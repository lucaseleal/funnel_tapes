create table credit_rejection_renewal_leads as
select 
	lucas.lead_id
from (select ltrl.lead_id,
		ltrl.lead_status
	from lead_tape_renewal_leads ltrl
	join(select t1.emprestimo_id,
			max(dp.cnpj) as cnpj,
			min(t1.pago_em::date) as dt_elegibilidade
		from (select fpp.emprestimo_id,
				(fp.pago_em + interval '1 millisecond' * fp.id) as pago_em,
				termo,
				rank() over (partition by fpp.emprestimo_id order by fp.pago_em + interval '1 millisecond' * fp.id)
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			join (select fpp.emprestimo_id, 
					count(*) as termo
				from financeiro_planopagamento fpp
				join financeiro_parcela fp on fp.plano_id = fpp.id
				where fp.status not in ('Cancelada','EmEspera')
				group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id) as t1
		join public.loan_requests lr on lr.loan_request_id = t1.emprestimo_id
		join public.offers o on o.offer_id = lr.offer_id
		join public.direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
		where rank/t1.termo::float >= 0.8
		group by 1) t1 on t1.cnpj = ltrl.lead_cnpj
	where ltrl.lead_opt_in_date > dt_elegibilidade 
	and ltrl.lead_status in ('Negado Renovação','Negado Comite','Negado Analise','Negado Faturamento','Negado SCR CPF','PedidoRejeitado','Negado Restricao','Negado Serasa','Negado Bizu','Negado SCR CNPJ')) as lucas
left join(select
		ltrl.lead_id,
		ltrl.lead_status
	from lead_tape_renewal_leads ltrl
	left join (select distinct dp.direct_prospect_id,dp.workflow
		from public.direct_prospects dp
		join lucas_leal.lead_tape_renewal_leads ltrl on ltrl.lead_id = dp.direct_prospect_id
		left join public.direct_prospect_contact dpc on dpc.direct_prospect_id = dp.direct_prospect_id
		where dpc.direct_prospect_id is null or (dp.workflow in ('Negado Renovação','Negado Comite') and dpc.notes not like '%em andamento%' and dpc.notes not like '%contrato recente%'and 
			(dpc.notes like '%não será%' 
			or upper(dpc.notes) like '%NEGADO%' 
			or upper(dpc.notes) like '%NÃO SERÁ POSSÍVEL%' 
			or upper(dpc.notes) like '%NÃO RENOVAMOS%' 
			or upper(dpc.notes) like '%SERASA%' 
			or upper(dpc.notes) like '%BIZU F%' 
			or upper(dpc.notes) like '%BLACKLIST%' 
			or upper(dpc.notes) like '%ATRASO%' 
			or upper(dpc.notes) like '%PEFIN%' 
			or upper(dpc.notes) like '%REFIN%' 
			or upper(dpc.notes) like '%RESTRITIVOS%' 
			or upper(dpc.notes) like '%PENDÊNCIA%' 
			or upper(dpc.notes) like '%SCORE%' 	
			or upper(dpc.notes) like '%ENROLAD%' 	
			or upper(dpc.notes) like '%NEGATIVAD%' 	
			or upper(dpc.notes) like '%RESTRIÇÃO%'))) as t1 on t1.direct_prospect_id = ltrl.lead_id
	where ltrl.lead_status in ('Negado Renovação','Negado Comite','Negado Analise','Negado Faturamento','Negado SCR CPF','PedidoRejeitado','Negado Restricao','Negado Serasa','Negado Bizu','Negado SCR CNPJ') and
		case when ltrl.lead_status in ('Negado Renovação','Negado Comite') then t1.direct_prospect_id is not null else true end) as cristiano on cristiano.lead_id = lucas.lead_id
where case when cristiano.lead_id is null then lucas.lead_status not in ('Negado Renovação','Negado Comite','Negado Analise') else true end 


--VARIAVEIS MODELO LUCAS
select
	t1.min_id,
	ltrl.lead_status,
	ltrl.lead_classe_bizu2,
	ltrl.scr_var_abs_12m_all_loans_pj_over_revenue as ld_scr_var_abs_12m_all_loans_pj_over_revenue,
	ltrl.scr_var_rel_12m_long_term_debt_pf_debt as ld_scr_var_rel_12m_long_term_debt_pf_debt,
	ltrl.scr_curr_overdraft_pj_over_long_term_debt as ld_scr_curr_overdraft_pj_over_long_term_debt,
	ltrl.scr_curr_all_loans_pj_over_long_term_debt as ld_scr_curr_all_loans_pj_over_long_term_debt,
	lt.collection_sum_payment_history_curve as ultimo_collection_sum_payment_history_curve,
	lt.scr_months_rise_long_term_debt_pf_over_total_months as ultimo_scr_months_rise_long_term_debt_pf_debt_over_total_months,
	lt.collection_count_pmt_paid_late as ultimo_collection_count_pmt_paid_late,
	case ltrl.scr_months_dive_other_credits_pj_debt_over_total_months when 0 then 0 else ltrl.scr_months_dive_other_credits_pj_debt_over_total_months / 
		regexp_replace(regexp_replace(lt.scr_months_dive_other_credits_pj_debt_over_total_months::text,'^0$','1'),'^-0$','1')::float - 1 end as delta_ultimo_scr_months_dive_other_credits_pj_debt_over_total_m,
--	as ld_scr_curr_lim_cred_pf_over_application_value,
	ltrl.loan_net_value,
	ltrl.offer_final_offer_max_value,
	ltrl.lead_application_amount,
	lt.loan_net_value as ticket_r0,
	lt.loan_final_net_interest_rate,
	lt.loan_original_final_term
from (select 
		ltrl.lead_cnpj,
		min(ltrl.lead_id) as min_id
	from lead_tape_renewal_leads ltrl
	join credit_rejection_renewal_leads crrl on crrl.lead_id = ltrl.lead_id
	where ltrl.loan_is_renewal = 1
	group by 1) as t1
join lead_tape_renewal_leads ltrl on ltrl.lead_id = t1.min_id
join loan_tape_full lt on lt.lead_cnpj = t1.lead_cnpj and lt.loan_is_renewal = 0



--VARIAVEIS MODELO VALESCA
select
	t1.min_id,
	coalesce(ltrl.loan_request_date,ltrl.offer_final_offer_date,ltrl.lead_opt_in_date)::date rejection_date,
	lt.lead_id,
	ltrl.lead_status,
	ltrl.lead_classe_bizu2,
	ltrl.scr_curr_lim_cred_pf_over_application_value as atual_scr_curr_lim_cred_pf_over_application_value,
	lt.collection_sum_payment_history_curve as ultimo_collection_sum_payment_history_curve,
	case ltrl.scr_avg_amortization_old_overdraft_pj_over_loan_pmt when 0 then 0 else ltrl.scr_avg_amortization_old_overdraft_pj_over_loan_pmt / 
		regexp_replace(regexp_replace(lt.scr_avg_amortization_old_overdraft_pj_over_loan_pmt::text,'^0$','1'),'^-0$','1')::float - 1 end as delta_ultimo_scr_avg_amort_old_overdraft_pj_over_loan_pmt,
	ltrl.lead_experian_score6 as atual_lead_experian_score6,
	ltrl.scr_var_abs_12m_all_loans_pj_over_revenue as atual_scr_var_abs_12m_all_loans_pj_over_revenue,
	case ltrl.scr_months_rise_long_term_debt_pf_debt_over_total_months when 0 then 0 else ltrl.scr_months_rise_long_term_debt_pf_debt_over_total_months / 
		regexp_replace(regexp_replace(lt.scr_months_rise_long_term_debt_pf_over_total_months::text,'^0$','1'),'^-0$','1')::float - 1 end as delta_ultimo_scr_months_rise_long_term_debt_pf_over_total_months,
	ltrl.loan_net_value,
	ltrl.offer_final_offer_max_value,
	ltrl.lead_application_amount,
	lt.loan_net_value as ticket_r0,
	lt.loan_final_net_interest_rate,
	lt.loan_original_final_term
from (select 
		ltrl.lead_cnpj,
		min(ltrl.lead_id) as min_id
	from lead_tape_renewal_leads ltrl
	join credit_rejection_renewal_leads crrl on crrl.lead_id = ltrl.lead_id
	where ltrl.loan_is_renewal = 1
	group by 1) as t1
join lead_tape_renewal_leads ltrl on ltrl.lead_id = t1.min_id
join loan_tape_full lt on lt.lead_cnpj = t1.lead_cnpj and lt.loan_is_renewal = 0



--VARIAVEIS MODELO VALESCA PARA ANTECIPAÇÃO DE LEADS QUE FICARÃO ELEGIVEIS A RENOVAÇÃO EM NOVEMBRO
select
	lt.lead_id,
	lt.lead_informed_month_revenue,
--	ltrl.lead_classe_bizu2,
--	ltrl.scr_curr_lim_cred_pf_over_application_value as atual_scr_curr_lim_cred_pf_over_application_value,
--	lt.collection_sum_payment_history_curve as ultimo_collection_sum_payment_history_curve,
--	case ltrl.scr_avg_amortization_old_overdraft_pj_over_loan_pmt when 0 then 0 else ltrl.scr_avg_amortization_old_overdraft_pj_over_loan_pmt / 
--		regexp_replace(regexp_replace(lt.scr_avg_amortization_old_overdraft_pj_over_loan_pmt::text,'^0$','1'),'^-0$','1')::float - 1 end as delta_ultimo_scr_avg_amort_old_overdraft_pj_over_loan_pmt,
--	ltrl.lead_experian_score6 as atual_lead_experian_score6,
--	ltrl.scr_var_abs_12m_all_loans_pj_over_revenue as atual_scr_var_abs_12m_all_loans_pj_over_revenue,
--	case ltrl.scr_months_rise_long_term_debt_pf_debt_over_total_months when 0 then 0 else ltrl.scr_months_rise_long_term_debt_pf_debt_over_total_months / 
--		regexp_replace(regexp_replace(lt.scr_months_rise_long_term_debt_pf_over_total_months::text,'^0$','1'),'^-0$','1')::float - 1 end as delta_ultimo_scr_months_rise_long_term_debt_pf_over_total_months,
--	ltrl.loan_net_value,
--	ltrl.offer_final_offer_max_value,
--	ltrl.lead_application_amount,
--	lt.loan_net_value as ticket_r0,
--	lt.loan_final_net_interest_rate,
--	lt.loan_original_final_term
from loan_tape_full lt
where lead_id in (87305,
180405,
165269,
184307,
179627,
227507,
172494,
223858,
181485,
168575,
172240,
170803,
186627,
174133,
179023,
301273,
192259,
178463,
185967,
180718,
188498,
193029,
187063,
197910,
194170,
184413,
169953,
245682,
174770,
188103,
356841,
186555,
340329,
81658,
185450,
196516,
89822,
82252,
172916,
170648,
191782,
197263,
184377,
202631,
183388,
170233,
183929,
202776,
253494,
174763,
198555,
374661,
195172,
175135,
219374,
234106,
203845,
182581,
174435,
273658,
178888,
180578,
186351,
178112,
286400,
290859,
197790,
177120,
295128,
316141,
368960,
181010,
184996,
185468,
185579,
164382,
361911,
185140,
172788,
197286,
185267,
182311,
184284,
202146,
183522,
261035,
168953,
183600,
178061)