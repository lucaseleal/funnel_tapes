select 
	left(cc.documento,8) as documento,
	string_agg(cc.id::text,',') as lista_coletas,
	(max(t1.documento) is null)::int as new_cnpj
from public.credito_coleta cc 
left join (select 
		documento, 
		max(coleta_id) as max_id 
	from lucas_leal.t_scr_pj_history_data
	group by 1) as t1 on t1.documento = left(cc.documento,8) 
where cc.documento_tipo::text = 'CNPJ'::text and cc.tipo::text = 'SCR'::text and coalesce(cc.documento,'') != '' and cc.id > coalesce(t1.max_id,1)
group by 1

select documento ,"data" ,count(distinct coleta_id)
from t_scr_pj_history_data
where data is not null
group by 1,2
order by 3 desc

select t1.*,dp.direct_prospect_id 
from t_scr_pj_history_data t1
join public.direct_prospects dp on left(dp.cnpj,8) = t1.documento 
where documento = '74536996'

grant select on data_science.reject_inference_tape to ricardo_filho

truncate t_scr_pj_history_data;
select t1.*
from(
	select 
		left(cc.documento,8) as documento,
		cc.id as coleta_id,
	    to_date(carteira_credito ->> 'data'::text, 'mm/yyyy'::text) AS data,
	    replace(replace(replace(carteira_credito ->> 'valor'::text, '-'::text, '0'::text), '.'::text, ''::text), ','::text, '.'::text)::double precision AS carteira_credito,
	    replace(replace(replace(vencido ->> 'valor'::text, '-'::text, '0'::text), '.'::text, ''::text), ','::text, '.'::text)::double precision AS vencidopj,
	    replace(replace(replace(prejuizo ->> 'valor'::text, '-'::text, '0'::text), '.'::text, ''::text), ','::text, '.'::text)::double precision AS prejuizopj,
	    replace(replace(replace(limite_credito ->> 'valor'::text, '-'::text, '0'::text), '.'::text, ''::text), ','::text, '.'::text)::double precision AS limcredpj,
		row_number() over (partition by cc.documento, to_date(carteira_credito ->> 'data'::text, 'mm/yyyy'::text) order by COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta) desc) as indice_valor_coleta,
		COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date as data_coleta,
		to_date(cc.data -> 'historico' -> 'Carteira de Cr�dito' -> 0 ->> 'data','mm/yyyy'::text) as data_base_coleta,
	    coalesce(cc."data"->> 'erro',cc."data"->> 'error','ok') as coleta_erro
	from credito_coleta cc
	left join jsonb_array_elements((cc.data -> 'historico'::text) -> 'Carteira de Cr�dito'::text) as carteira_credito on true
	left join jsonb_array_elements((cc.data -> 'historico'::text) -> 'Vencido'::text) as vencido on vencido ->> 'data' = carteira_credito ->> 'data'
	left join jsonb_array_elements((cc.data -> 'historico'::text) -> 'Preju�zo'::text) as prejuizo on prejuizo ->> 'data' = carteira_credito ->> 'data'
	left join jsonb_array_elements((cc.data -> 'historico'::text) -> 'Limite de Cr�dito'::text) as limite_credito on limite_credito ->> 'data' = carteira_credito ->> 'data'
	where cc.documento_tipo::text = 'CNPJ'::text and cc.tipo::text = 'SCR'::text 
	) as t1
left join lucas_leal.t_scr_pj_history_data t2 on t2.documento = t1.documento and t1.data = t2.data
where t1.indice_valor_coleta = 1 and t2.data is null


drop materialized view mv_reject_inferece_tape;
alter table mv_reject_inferece_tape2 rename to mv_reject_inferece_tape;
create unique index mv_reject_inferece_tape_lead_id on mv_reject_inferece_tape using btree (direct_prospect_id);
refresh materialized view mv_reject_inferece_tape with data;

create materialized view mv_reject_inferece_tape2 as 
select 
	dp.direct_prospect_id,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data <= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '12 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '12 months'::interval)) > 1)::int as ever_30_mob12,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '12 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '12 months'::interval)) > 1)::int as over_30_mob12,
	(sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 month' else '0' end::interval)::date + '12 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '12 months'::interval)) -
		sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 16 then '2 months' else '1 month' end::interval)::date or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '12 months'::interval)) > 1)::int as got_credit_mob12,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data <= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '11 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '11 months'::interval)) > 1)::int as ever_30_mob11,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '11 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '11 months'::interval)) > 1)::int as over_30_mob11,
	(sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 month' else '0' end::interval)::date + '11 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '11 months'::interval)) -
		sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 16 then '2 months' else '1 month' end::interval)::date or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '11 months'::interval)) > 1)::int as got_credit_mob11,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data <= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '10 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '10 months'::interval)) > 1)::int as ever_30_mob10,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '10 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '10 months'::interval)) > 1)::int as over_30_mob10,
	(sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 month' else '0' end::interval)::date + '10 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '10 months'::interval)) -
		sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 16 then '2 months' else '1 month' end::interval)::date or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '10 months'::interval)) > 1)::int as got_credit_mob10,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data <= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '9 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '9 months'::interval)) > 1)::int as ever_30_mob9,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '9 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '9 months'::interval)) > 1)::int as over_30_mob9,
	(sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 month' else '0' end::interval)::date + '9 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '9 months'::interval)) -
		sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 16 then '2 months' else '1 month' end::interval)::date or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '9 months'::interval)) > 1)::int as got_credit_mob9,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data <= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '8 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '8 months'::interval)) > 1)::int as ever_30_mob8,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '8 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '8 months'::interval)) > 1)::int as over_30_mob8,
	(sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 month' else '0' end::interval)::date + '8 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '8 months'::interval)) -
		sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 16 then '2 months' else '1 month' end::interval)::date or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '8 months'::interval)) > 1)::int as got_credit_mob8,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data <= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '7 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '7 months'::interval)) > 1)::int as ever_30_mob7,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '7 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '7 months'::interval)) > 1)::int as over_30_mob7,
	(sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 month' else '0' end::interval)::date + '7 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '7 months'::interval)) -
		sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 16 then '2 months' else '1 month' end::interval)::date or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '7 months'::interval)) > 1)::int as got_credit_mob7,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data <= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '6 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '6 months'::interval)) > 0)::int as ever_30_mob6,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '6 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '6 months'::interval)) > 0)::int as over_30_mob6,
	(sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 month' else '0' end::interval)::date + '6 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '6 months'::interval)) -
		sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 16 then '2 months' else '1 month' end::interval)::date or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '6 months'::interval)) > 1)::int as got_credit_mob6,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data <= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '5 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '5 months'::interval)) > 1)::int as ever_30_mob5,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '5 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '5 months'::interval)) > 1)::int as over_30_mob5,
	(sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 month' else '0' end::interval)::date + '5 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '5 months'::interval)) -
		sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 16 then '2 months' else '1 month' end::interval)::date or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '5 months'::interval)) > 1)::int as got_credit_mob5,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data <= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '4 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '4 months'::interval)) > 1)::int as ever_30_mob4,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '4 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '4 months'::interval)) > 1)::int as over_30_mob4,
	(sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 month' else '0' end::interval)::date + '4 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '4 months'::interval)) -
		sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 16 then '2 months' else '1 month' end::interval)::date or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '4 months'::interval)) > 1)::int as got_credit_mob4,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data <= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '3 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '3 months'::interval)) > 1)::int as ever_30_mob3,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '3 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '3 months'::interval)) > 1)::int as over_30_mob3,
	(sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 month' else '0' end::interval)::date + '3 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '3 months'::interval)) -
		sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 16 then '2 months' else '1 month' end::interval)::date or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '3 months'::interval)) > 1)::int as got_credit_mob3,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data <= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '2 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '2 months'::interval)) > 1)::int as ever_30_mob2,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '2 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '2 months'::interval)) > 1)::int as over_30_mob2,
	(sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 month' else '0' end::interval)::date + '2 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '2 months'::interval)) -
		sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 16 then '2 months' else '1 month' end::interval)::date or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '2 months'::interval)) > 1)::int as got_credit_mob2,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data <= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '1 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '1 months'::interval)) > 1)::int as ever_30_mob1,
	(sum(coalesce(vencidopj,0) + coalesce(prejuizopj,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '1 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '1 months'::interval)) > 1)::int as over_30_mob1,
	(sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 month' else '0' end::interval)::date + '1 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '1 months'::interval)) -
		sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 16 then '2 months' else '1 month' end::interval)::date or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '1 months'::interval)) > 1)::int as got_credit_mob1
from public.direct_prospects dp 
join lucas_leal.t_scr_pj_history_data t1 on left(t1.documento,8) = left(dp.cnpj,8)
where coalesce(data,date_trunc('month',t1.data_coleta)::date) >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 16 then '2 months' else '1 month' end::interval)::date
group by 1


select 
	sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 month' else '0' end::interval)::date + '12 months'::interval or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '12 months'::interval)) ,
		sum(coalesce(carteira_credito,0)) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 16 then '2 months' else '1 month' end::interval)::date or (t1.coleta_erro != 'ok' and date_trunc('month',t1.data_coleta)::date >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + '12 months'::interval)) ::int as got_credit_mob12
from public.direct_prospects dp 
join lucas_leal.t_scr_pj_history_data t1 on left(t1.documento,8) = left(dp.cnpj,8)
where coalesce(data,date_trunc('month',t1.data_coleta)::date) >= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 16 then '2 months' else '1 month' end::interval)::date
	and dp.direct_prospect_id = 266308


create or replace function lucas_leal.get_reject_inference_bad(lead_id bigint, metodo text default 'over', mob int default 12, got_credit_check bool default true, got_credit_threshold numeric default 1000, vencido_threshold numeric default 0) 
 RETURNS integer
 LANGUAGE sql
AS $function$
		select 
			(case when got_credit_check then
				(case when 
					sum(t1.carteira_credito) filter (where data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 month' else '0' end::interval)::date + (mob::text || ' months')::interval),-1) -
						sum(carteira_credito) filter (where data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 16 then '2 months' else '1 month' end::interval)::date),-1) > got_credit_threshold then
							(case metodo
								when 'ever' then 
									sum(t1.vencidopj) filter (where t1.data <= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + (mob::text || ' months')::interval) > vencido_threshold
								when 'over' then 
									sum(t1.vencidopj) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + (mob::text || ' months')::interval) > vencido_threshold
							end)::int
					else 0
				end)::int
			end)::int
		from public.direct_prospects dp 
		join lucas_leal.t_scr_pj_history_data t1 on left(t1.documento,8) = left(dp.cnpj,8)
		where dp.direct_prospect_id = lead_id
	;$function$
;


create or replace function lucas_leal.get_reject_inference_bad (lead_id bigint[], metodo text default 'over', mob int default 12, got_credit_check bool default true, got_credit_threshold numeric default 1000, vencido_threshold numeric default 0) 
	returns table (
		direct_prospect_id bigint,
		bad int
	) 
	language plpgsql
as $$
begin
	return query 
		select dp.direct_prospect_id,
			(case when got_credit_check then
				(case when 
					sum(t1.carteira_credito) filter (where data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 month' else '0' end::interval)::date + (mob::text || ' months')::interval),-1) -
						sum(carteira_credito) filter (where data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 16 then '2 months' else '1 month' end::interval)::date),-1) > got_credit_threshold then
							(case metodo
								when 'ever' then 
									sum(t1.vencidopj) filter (where t1.data <= date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + (mob::text || ' months')::interval) > vencido_threshold
								when 'over' then 
									sum(t1.vencidopj) filter (where t1.data = date_trunc('month',dp.opt_in_date - case when extract(day from dp.opt_in_date) < 6 then '1 months' else '0' end::interval)::date + (mob::text || ' months')::interval) > vencido_threshold
							end)::int
					else 0
				end)::int
			end)::int as bad
		from public.direct_prospects dp 
		join lucas_leal.t_scr_pj_history_data t1 on left(t1.documento,8) = left(dp.cnpj,8)
		where dp.direct_prospect_id in (lead_id)
;
end;$$
	