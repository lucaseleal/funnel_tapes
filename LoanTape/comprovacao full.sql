CREATE INDEX t_bizextratos_analysis_data_full_analysis_id ON lucas_leal.t_bizextratos_analysis_data_full USING btree (analysis_id);
CREATE INDEX t_bizextratos_analysis_data_full_loan_request_id ON lucas_leal.t_bizextratos_analysis_data_full USING btree (loan_request_id);
CREATE INDEX t_bizextratos_analysis_data_full_mes ON lucas_leal.t_bizextratos_analysis_data_full USING btree (mes);
--BIZEXTRATO - ANALISE
insert into lucas_leal.t_bizextratos_analysis_data_full
select 
	t1.id as analysis_id,
	t1.loan_request_id,
	(indicadores ->> 'id' || '-01')::date as mes,
	dense_rank() over (partition by t1.loan_request_id order by t1.id desc) as indice_analise,
	max(indicadores ->> 'totalDays')::smallint as qtd_dias_mapeados,
	max(indicadores ->> 'workDays')::smallint as qtd_dias_uteis,
	max(indicadores ->> 'totalIncome')::numeric as total_movimentado,
	max(indicadores ->> 'verifiedRevenue')::numeric as faturamento_comprovado,
	max(indicadores ->> 'totalIncomePercentage')::numeric / 100 as perc_total_movimentado,
	max(indicadores ->> 'verifiedRevenuePercentage')::numeric / 100 as perc_faturamento_comprovado,
	max(indicadores ->> 'averageTicket')::numeric as ticket_medio,
	max(indicadores ->> 'greaterTicket')::numeric as maior_entrada,
	max(indicadores ->> 'incomeCount')::smallint as qtd_entradas,
	max(indicadores ->> 'cashDespositsPercentage')::numeric / 100 as tipo_entrada_deposito_especie,
	max(indicadores ->> 'advancementPercentage')::numeric / 100 as tipo_entrada_antecipacao,
	@ max(indicadores ->> 'totalOutcome')::numeric as total_saida,
	@ max(indicadores ->> 'greaterOutcome')::numeric as maior_saida,
	@ max(indicadores ->> 'mainDrawPercentage')::numeric / 100 as tipo_saida_perc_principal_sacado,
	@ max(indicadores ->> 'atmWithdraws')::numeric as tipo_saida_saque_atm,
	@ max(indicadores ->> 'creditPMT')::numeric as tipo_saida_pgto_pmt_credito,
	@ max(indicadores ->> 'interest')::numeric as tipo_saida_pgto_juros_mora,
	@ max(indicadores ->> 'expenses')::numeric as tipo_saida_contas_despesas,
	@ max(indicadores ->> 'marketplacePercentage')::numeric / 100 as tipo_saida_marketplace,
	max(indicadores -> 'fluxoCaixa' ->> 'operacional')::numeric as fluxo_caixa_operacional,
	max(indicadores -> 'fluxoCaixa' ->> 'pior')::numeric as fluxo_caixa_pior,
	max(indicadores -> 'fluxoCaixa' ->> 'melhor')::numeric as fluxo_caixa_melhor,
	max(indicadores -> 'fluxoCaixa' ->> 'diaMelhor')::smallint as fluxo_caixa_dia_melhor,
	max(indicadores -> 'fluxoCaixa' ->> 'percentaulDiasNegativo')::numeric / 100 as fluxo_caixa_dias_negativo,
	max(indicadores -> 'fluxoCaixa' ->> 'diasNegativo')::smallint as fluxo_caixa_perc_dias_negativo,
	max(indicadores -> 'fluxoCaixa' ->> 'ateDiaLead')::numeric as fluxo_caixa_saldo_ate_dia_lead,
	max("data"->>'averageIncome')::numeric as faturamento_medio,
	max("data"->>'averageIncomePercentage')::numeric / 100 as perc_faturamento_medio
from biz_credit.analysis t1
left join json_array_elements(t1."data" -> 'monthlyIndicators') as indicadores on true
left join (select distinct loan_request_id 
	from lucas_leal.t_bizextratos_analysis_data_full) t2 on t2.loan_request_id = t1.loan_request_id 
left join lucas_leal.t_extratos t3 on t3.loan_request_id = t1.loan_request_id 
where created_by not in ('Eduardo Varella','Pedro Castro','Vanessa Medeiros')
	and last_updated_by not in ('Eduardo Varella','Pedro Castro','Vanessa Medeiros')
	and status not in ('IMPORTED','DELETED') 
	and t2.loan_request_id is null 
	and t3.loan_request_id is null 
group by 1,2,3
having sum((indicadores ->> 'totalIncome')::numeric) > 0
	

CREATE INDEX t_bizextrato_indicators_full_analysis_id ON lucas_leal.t_bizextrato_indicators_full USING btree (analysis_id);
CREATE unique INDEX t_bizextrato_indicators_full_loan_request_id ON lucas_leal.t_bizextrato_indicators_full USING btree (loan_request_id);
insert into lucas_leal.t_bizextrato_indicators_full
(
	(select
		q1.analysis_id,
		q1.loan_request_id ,
		sum(q1.faturamento_comprovado) filter (where q1.indice_mes_15du is not null) as ever_faturamento_comprovado_15du,
		max(q1.faturamento_medio) as faturamento_medio,
		max(q1.perc_faturamento_medio) as perc_faturamento_medio
	from(select 
			t1.analysis_id,
			t1.loan_request_id,
			t1.faturamento_comprovado,
			t1.perc_faturamento_comprovado,
			t1.qtd_dias_uteis,
			t1.mes,
			row_number() over (partition by t1.loan_request_id order by (t1.qtd_dias_uteis >= 20)::int desc, t1.mes) as indice_mes,
			case when t1.qtd_dias_uteis >= 5 then row_number() over (partition by t1.loan_request_id order by (t1.qtd_dias_uteis >= 5)::int desc, t1.mes) end as indice_mes_5du,
			case when t1.qtd_dias_uteis >= 10 then row_number() over (partition by t1.loan_request_id order by (t1.qtd_dias_uteis >= 10)::int desc, t1.mes) end as indice_mes_10du,
			case when t1.qtd_dias_uteis >= 15 then row_number() over (partition by t1.loan_request_id order by (t1.qtd_dias_uteis >= 15)::int desc, t1.mes) end as indice_mes_15du,
			case when t1.qtd_dias_uteis >= 20 then row_number() over (partition by t1.loan_request_id order by (t1.qtd_dias_uteis >= 20)::int desc, t1.mes) end as indice_mes_20du,
			t1.faturamento_medio,
			t1.perc_faturamento_medio
		from lucas_leal.t_bizextratos_analysis_data_full t1
		left join lucas_leal.t_extratos t2 on t2.loan_request_id = t1.loan_request_id
		left join lucas_leal.t_bizextrato_indicators_full t3 on t3.loan_request_id = t1.loan_request_id
		where t2.loan_request_id is null
			and t3.loan_request_id is null
			and indice_analise = 1) as q1
	group by 1,2)
--union all
--	(select
--		id as analysis_id,
--		loan_request_id,
--		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * faturamento_comprovado_mes1,0) +
--			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * faturamento_comprovado_mes2,0) +
--			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * faturamento_comprovado_mes3,0) +
--			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * faturamento_comprovado_mes4,0) +
--			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * faturamento_comprovado_mes5,0) as ever_faturamento_comprovado_15du,
--		faturamento_medio,
--		perc_faturamento_medio
--	from lucas_leal.t_extratos)
)

select *
from lucas_leal.t_bizextrato4_transactions_data_full
where value > 1000000 and date_trunc('month',docparser_processed_at)::date between '2021-04-01'::date and '2021-06-01'::date

select date_trunc('month',docparser_processed_at)::date,count(*),sum(value)
from lucas_leal.t_bizextrato4_transactions_data_full
where tag not in ('aplicacao','saldo') and  value < 1000000
group by 1

select loan_request_id ,transaction_id,count(*)
from lucas_leal.t_bizextrato4_transactions_data_full
group by 1,2
order by 3 desc

select loan_request_id ,count(*)
from lucas_leal.t_bizextrato4_transactions_data_full
where transaction_id = 1478482

select loan_request_id ,count(*)
from data_science.extratos_intermediarios ei 
where transaction_id is null
group by 1

update lucas_leal.t_bizextrato4_transactions_data_full
set account_number = ts.details ->> 'account'
from biz_credit.transactions t
left join biz_credit.transaction_sources ts on ts.id = t.source_id 
where t.id = t_bizextrato4_transactions_data_full.transaction_id 


CREATE INDEX t_bizextrato4_transactions_data_full_analysis_id ON lucas_leal.t_bizextrato4_transactions_data_full USING btree (analysis_id);
CREATE INDEX t_bizextrato4_transactions_data_full_loan_request_id ON lucas_leal.t_bizextrato4_transactions_data_full USING btree (loan_request_id);
CREATE INDEX t_bizextrato4_transactions_data_full_transaction_id ON lucas_leal.t_bizextrato4_transactions_data_full USING btree (transaction_id);
insert into lucas_leal.t_bizextrato4_transactions_data_full
	select
		t1.transaction_id,
		'bizextrato' as fonte,
		t1.loan_request_id,
		t1.value,
		t1.month_revenue,
		t1.sector,
		t1.chave_transacao,
		t1.intervalo,
		t1.periodo_mes,
		t1.movimentacao,
		t1.tratado,
		t1.tag_tratado_da_query,
		t1.tag_descricao,
		t1.tag,
		t4.analysis_id,
		(ts.details ->> 'processed_at')::timestamp as docparser_processed_at,
		ts.details ->> 'type' as tipo_doc,
		ts.details ->> 'branch' as agency_number,
		ts.details ->> 'account' as account_number,
		case ts.details ->> 'type'
			when 'Arbi' then 'ARBI'
			when 'BANCO AMAZONIA' then 'AMAZONIA'
			when 'BANCO AMAZONIA 2' then 'AMAZONIA'
			when 'BANCO BRASILIA' then 'BRASILIA'
			when 'BANCO DO BRASIL 1' then 'BRASIL'
			when 'BANCO DO BRASIL 2' then 'BRASIL'
			when 'BANCO DO BRASIL 3' then 'BRASIL'
			when 'BANCO DO BRASIL 4' then 'BRASIL'
			when 'BANCO DO BRASIL PF' then 'BRASIL'
			when 'Banco do Nordeste 2.0' then 'NORDESTE'
			when 'BANCO INTER' then 'INTER'
			when 'Banco Mercantil' then 'MERCANTIL'
			when 'BANCO NORDESTE' then 'NORDESTE'
			when 'BANCO NORDESTE 2' then 'NORDESTE'
			when 'Banco Original' then 'ORIGINAL'
			when 'Banco Pan' then 'PAN'
			when 'BANCO SAFRA 2' then 'SAFRA'
			when 'BANCO SAFRA 3' then 'SAFRA'
			when 'Bando Do Brasil PJ rodrigo' then 'BRASIL'
			when 'BANESE' then 'BANESE'
			when 'BANESTES' then 'BANESTES'
			when 'Banpar�' then 'Banpar�'
			when 'BANRISUL' then 'BANRISUL'
			when 'BizConta' then 'BizConta'
			when 'BMG' then 'BMG'
			when 'BMP' then 'BMP'
			when 'BRADESCO 2' then 'BRADESCO'
			when 'BRADESCO 3' then 'BRADESCO'
			when 'BRADESCO 4' then 'BRADESCO'
			when 'Bradesco Nota Impressa' then 'Bradesco'
			when 'BRADESCO PARALELO' then 'BRADESCO'
			when 'BRADESCO PF' then 'BRADESCO'
			when 'Bradesco Print' then 'Bradesco'
			when 'BRADESCO TESTE PEDRO' then 'BRADESCO'
			when 'BRADESCO TESTE VICTOR' then 'BRADESCO'
			when 'Bradesco.celular' then 'BRADESCO'
			when 'BRB' then 'BRB'
			when 'BS2' then 'BRB'
			when 'BTG+' then 'BRB'
			when 'C6' then 'BRB'
			when 'Caixa Print' then 'Caixa'
			when 'Caixa Scan' then 'Caixa'
			when 'CAIXA_PAPEL' then 'CEF'
			when 'Caso Takahashi' then 'TAKASHI'
			when 'CEF' then 'CEF'
			when 'CEF 2' then 'CEF'
			when 'CEF 3' then 'CEF'
			when 'CEF Celular' then 'CEF'
			when 'CEF NOVO' then 'CEF'
			when 'CEF Paralelo' then 'CEF'
			when 'CONTA SIMPLES' then 'SIMPLES'
			when 'Cooperativa Unilos' then 'UNILOS'
			when 'Cora' then 'CORA'
			when 'CREDICOOP' then 'CREDICOOP'
			when 'Credisan' then 'CREDISAN'
			when 'Credisis' then 'CREDISIS'
			when 'Cresol' then 'CRESOL'
			when 'Dividido' then 'DIVIDIDO'
			when 'Gambiarra' then 'GAMBIARRA'
			when 'Gerencianet' then 'GERENCIANET'
			when 'ITAU 2a VIA' then 'ITAU'
			when 'ITAU EMPRESAS 1' then 'ITAU'
			when 'ITAU EMPRESAS 2' then 'ITAU'
			when 'ITAU EMPRESAS 3' then 'ITAU'
			when 'ITAU EMPRESAS 4' then 'ITAU'
			when 'ITAU EMPRESAS 5' then 'ITAU'
			when 'ITAU EMPRESAS 6' then 'ITAU'
			when 'ITAU EMPRESAS PLUS' then 'ITAU'
			when 'ITAU PF' then 'ITAU'
			when 'ITAU PF 2' then 'ITAU'
			when 'ITAU SEM DATA' then 'ITAU'
			when 'Layout Maiko' then 'LAYOUT MAIKO'
			when 'Mercado Pago' then 'MERCADO PAGO'
			when 'MercadoPago Print' then 'MERCADOPAGO PRINT'
			when 'Neon' then 'NEON'
			when 'Next' then 'NEXT'
			when 'Nubank' then 'NUBANK'
			when 'PagSeguro 2' then 'PAGSEGURO'
			when 'PagSeguro 3' then 'PAGSEGURO'
			when 'PicPay' then 'PICPAY'
			when 'SANTANDER 1' then 'SANTANDER'
			when 'SANTANDER 2' then 'SANTANDER'
			when 'SANTANDER 3' then 'SANTANDER'
			when 'SANTANDER 4' then 'SANTANDER'
			when 'SANTANDER 5' then 'SANTANDER'
			when 'SANTANDER MASTER' then 'SANTANDER'
			when 'SANTANDER Paralelo' then 'SANTANDER'
			when 'SANTANDER PF' then 'SANTANDER'
			when 'SANTANDER PJ' then 'SANTANDER'
			when 'SANTANDER USO INTERNO' then 'SANTANDER'
			when 'Santander_PF' then 'SANTANDER'
			when 'Santander_PF_Rodrgo' then 'SANTANDER'
			when 'SICOOB 1' then 'SICOOB'
			when 'SICOOB 2' then 'SICOOB'
			when 'SICOOB 3' then 'SICOOB'
			when 'SICOOB NOVO' then 'SICOOB'
			when 'SICREDI 1' then 'SICREDI'
			when 'SICREDI 2' then 'SICREDI'
			when 'SICREDI 3' then 'SICREDI'
			when 'SICREDI 4' then 'SICREDI'
			when 'SICREDI TORTO' then 'SICREDI'
			when 'SICREDI USO INTERNO' then 'SICREDI'
			when 'Stone' then 'STONE'
			when 'Superdigital' then 'SUPERDIGITAL'
			when 'TAMP�O' then 'TAMP�O'
			when 'TRIBANCO' then 'TRIBANCO'
			when 'UNICRED' then 'UNICRED'
			when 'UNICRED 2' then 'UNICRED'
			when 'UNIPRIME' then 'UNIPRIME'
			when 'VIACREDI' then 'VIACREDI'
			when 'VIACREDI 2' then 'VIACREDI'
			when 'WOORI BANK' then 'WOORI'
			else 'OTHER'
		end as banco
	from data_science.extratos_intermediarios t1
--	left join (select distinct loan_request_id from lucas_leal.t_bizcredit_transactions_corrigida_full) t2 on t2.loan_request_id = t1.loan_request_id 
--	left join (select distinct loan_request_id from lucas_leal.t_bizextrato4_transactions_data_full) t3 on t3.loan_request_id = t1.loan_request_id 
	left join lucas_leal.t_bizextrato_indicators_full t4 on t4.loan_request_id = t1.loan_request_id 
	left join biz_credit.transactions t on t.id = t1.transaction_id 
	left join biz_credit.transaction_sources ts on ts.id = t.source_id 
	where /*t2.loan_request_id is null
		and t3.loan_request_id is null*/
	t1.loan_request_id in (select t1.loan_request_id
from (select distinct loan_request_id from data_science.extratos_intermediarios) t1
left join (select distinct loan_request_id from lucas_leal.t_bizcredit_transactions_corrigida_full) t2 on t2.loan_request_id = t1.loan_request_id 
left join (select distinct loan_request_id from lucas_leal.t_bizextrato4_transactions_data_full) t3 on t3.loan_request_id = t1.loan_request_id 
where coalesce(t2.loan_request_id ,t3.loan_request_id) is null)
--union all
--	select 
--		t1.id as transaction_id,
--		t1.fonte,
--		t1.loan_request_id,
--		t1.value,
--		t1.month_revenue,
--		t1.sector,
--		t1.chave_transacao,
--		t1.intervalo,
--		t1.periodo_mes,
--		t1.movimentacao,
--		t1.tratado,
--		t1.tag_tratado_da_query,
--		t1.tag_descricao,
--		t1.tag,
--		t2.analysis_id
--	from lucas_leal.t_bizcredit_transactions_corrigida_full t1
--	join lucas_leal.t_bizextrato_indicators_full t2 on t2.loan_request_id = t1.loan_request_id 



select t1.loan_request_id
from (select distinct loan_request_id from data_science.extratos_intermediarios) t1
left join (select distinct loan_request_id from lucas_leal.t_bizcredit_transactions_corrigida_full) t2 on t2.loan_request_id = t1.loan_request_id 
left join (select distinct loan_request_id from lucas_leal.t_bizextrato4_transactions_data_full) t3 on t3.loan_request_id = t1.loan_request_id 
where coalesce(t2.loan_request_id ,t3.loan_request_id) is null
--	andt1.loan_request_id not in (select distinct loan_request_id from lucas_leal.t_bizextrato4_transactions_data_full)
--limit 10
		


		
		
CREATE INDEX t_bizextrato4_transactions_indicators_full_analysis_id ON lucas_leal.t_bizextrato4_transactions_indicators_full USING btree (analysis_id);
CREATE unique INDEX t_bizextrato4_transactions_indicators_full_loan_request_id ON lucas_leal.t_bizextrato4_transactions_indicators_full USING btree (loan_request_id);
insert into lucas_leal.t_bizextrato4_transactions_indicators_full 
select
	t1.loan_request_id,
	t1.analysis_id,
	sum(t1.value) filter (where t1.tag in ('boletos','tbi_ted','dinheiro','sacado_governo','adquirencia','marketplace','input_manual') and movimentacao = 'positivo')::numeric as ever_faturamento_comprovado,
	sum(t1.value) filter (where t1.tag in ('boletos','tbi_ted','dinheiro','sacado_governo','adquirencia','marketplace','input_manual') and movimentacao = 'positivo')::numeric / 3 as faturamento_comprovado_medio,
	sum(t1.value) filter (where t1.tag in ('boletos','tbi_ted','dinheiro','sacado_governo','adquirencia','marketplace','input_manual') and movimentacao = 'positivo')::numeric / 3 / greatest(max(month_revenue),1) as perc_faturamento_comprovado_medio
from lucas_leal.t_bizextrato4_transactions_data_full t1
left join lucas_leal.t_bizextrato4_transactions_indicators_full t2 on t2.loan_request_id = t1.loan_request_id
where t2.loan_request_id is null
group by 1,2


select count(distinct loan_request_id) 
from lucas_leal.t_bizextrato4_transactions_data_full 
where loan_request_id in ()

