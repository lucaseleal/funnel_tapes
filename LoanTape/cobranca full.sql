select 
--	legacy_target_id as loan_request_id,
	jsonb_object_keys(data_output -> 'Treated') ,count(*)
from public.sync_collection_bars scb 
group by 1

select legacy_target_id,count(*)
from public.sync_collection_bars scb 
where data_output -> 'Loan' ->> 'SuspectedFraud' is not null
group by 1


data_output -> 'Loan' -> 'Id' as loan_id,
(data_output -> 'Loan' -> 'OpenValue')::numeric as loan_open_value,
data_output -> 'Loan' -> 'NotarizedClaim' as loan_notarized_claim,
data_output -> 'Loan' -> 'Borrower' -> 'CompanyName' as loan_borrower_company_name,
data_output -> 'Loan' -> 'Borrower' -> 'CompanyDocument' as loan_borrower_company_document,
data_output -> 'Loan' -> 'Product'->'BizId' as loan_product,
data_output -> 'Loan' -> 'SuspectedFraud' as loan_suspected_fraud,
(data_output -> 'Loan' -> 'Amount')::numeric as loan_amount,
data_output -> 'Loan' -> 'ExternalCollection' as loan_external_collection


data_output -> 'Id' as id,
data_output -> 'Type' as "type",
data_output -> 'Status' as status,
(data_output -> 'Spoiled')::int as spoiled,
(data_output -> 'CreatedAt')::timestamp as created_at,
(data_output -> 'DaysOfDelay')::int as days_of_delay,
(data_output -> 'LastUpdated')::timestamp as last_updated,
data_output -> 'LegacyRelationship' as legacy_relationship,
data_output -> 'LastContactNegotiation' as last_contact_negotiation,
data_output -> 'LastScheduledContactNegotiation' as last_scheduled_contact_negotiation,
data_output -> 'LastAgreementNegotiation' as last_agreement_negotiation,
data_output -> 'LastPaymentPromiseNegotiation' as last_payment_promise_negotiation,
data_output -> 'LastConciliationDate' as last_conciliation_date,
data_output -> 'NegotiationInfo' ->> 'Negotiated' as as negotiation_negotiated,
data_output -> 'NegotiationInfo' ->> 'LastUpdated' as as negotiation_last_updated,
data_output -> 'Responsibles'
data_output -> 'Treated'
data_output -> 'Priority'
data_output -> 'CurrentResponsible'
data_output -> 'DistributionScore'
data_output -> 'Negotiations'
data_output -> 'LastUpdatedTreated'
data_output -> 'PaymentPlan'
data_output -> 'DaysLate'


select 
	scb.id as collection_bar_id,
	fli.legacy_target_id as loan_request_id,
	(scb.data_output -> 'Loan' -> 'Borrower' ->> 'CompanyName') as cliente,
	(scb.data_output -> 'Loan' -> 'Borrower' ->> 'CompanyDocument') as cnpj,
	(scb.data_output -> 'Loan' ->> 'Amount')::numeric as valor_aprovado,
	(scb.data_output -> 'Loan' ->> 'OpenValue')::numeric as valor_em_aberto,
	(scb.data_output -> 'PaymentPlan' ->> 'ExposureAtDefault')::numeric as ead,
	--(scb.data_output -> 'LegacyRelationship' ->> 'CollectionBarId') as regua,
	case when scb.id = last_value(scb.id) over (partition by fli.legacy_target_id  order by fli.legacy_target_id , (scb.data_output ->> 'CreatedAt')::timestamp  RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) then true else false end isCurrent,
	(scb.data_output ->> 'CreatedAt')::timestamp Created_At, 
	(scb.data_output ->> 'LastUpdated')::timestamp Updated_At,
	(scb.data_output ->> 'Type')::text "type",
	(scb.data_output ->> 'Status')::text "status",
	(scb.data_output ->> 'DaysOfDelay')::integer daysLate,
	(scb.data_output ->> 'Spoiled')::boolean isSpoiled,
	(scb.data_output -> 'Tags'::text) ? 'FPD'::text AS isfpd,
	(scb.data_output -> 'Tags'::text) ? 'SPD'::text AS isspd,
	(scb.data_output -> 'Tags'::text) ? 'NOTARIZED_CLAIM'::text AS isnotarized,
	(scb.data_output -> 'Tags'::text) ? 'FIRST_TIME_IN_COLLECTION'::text AS isfirsttime,
	case when (scb.data_output ->> 'CurrentExternalCollection') is null then false else not (scb.data_output -> 'CurrentExternalCollection' ->> 'JudicialCollection')::boolean end as isAdmCollection, 
	case when (scb.data_output ->> 'CurrentExternalCollection') is null then false else (scb.data_output -> 'CurrentExternalCollection' ->> 'JudicialCollection')::boolean end as isJudCollection, 
	case when (scb.data_output ->> 'CurrentExternalCollection') is null then null else (scb.data_output -> 'CurrentExternalCollection'  ->> 'CollectionOfficeType') end as escritorio_cobranca, 
	case when (scb.data_output ->> 'CurrentNotarizedClaim') is null then null else (scb.data_output -> 'CurrentNotarizedClaim'  ->> 'SentDate')::date end as envio_protesto,
	case when (scb.data_output ->> 'CurrentNotarizedClaim') is null then null else (scb.data_output -> 'CurrentNotarizedClaim'  ->> 'RemovalDate')::date end as baixa_protesto,
	case when (scb.data_output ->> 'CurrentNotarizedClaim') is null then null else jsonb_array_length(scb.data_output -> 'CurrentNotarizedClaim' -> 'InstallmentIds') end as quantidade_parcelas_protesto,
	spp.legacy_target_id as plano_id,
	(scb.data_output -> 'CurrentResponsible' ->> 'Name') currentresponsible,
	(scb.data_output ->> 'LastUpdatedTreated')::timestamp Last_Updated_Treated,
	scb.data_output,
	date_trunc('second', current_timestamp)::timestamp as last_refresh
from sync_collection_bars scb 
left join sync_payment_plans spp on spp.related_collection_id = (scb.data_output -> 'PaymentPlan' ->> 'Id') 
-- left join sync_users su on su.related_collection_id = (scb.data_output -> 'CurrentResponsibleId' ->> 'Id')
inner join public.sync_financial_loan_information fli on (scb.data_output -> 'Loan' ->> 'Id') = fli.related_collection_id
-- where scb.legacy_target_id = 29742
order by 2,3;


