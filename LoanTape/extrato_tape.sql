grant select on v_extratos_full to giulia_killer;
grant select on v_extratos_full to valesca_jannis;
grant select on v_extratos_full to pedro_meirelles;
grant select on v_extratos_full to wellerson_silva;
grant select on v_extratos_full to cristiano_rocha;
grant select on v_extratos_full to rodrigo_pinho;


create materialized view v_extratos_full as
(select
	t.*,
	dp.direct_prospect_id,
	lr.status as status_pedido,
	(agg.over_pmt1 > 30)::int as collection_over_30_pmt1_aggressive,
	(agg.over_pmt2 > 30)::int as collection_over_30_pmt2_aggressive,
	(agg.over_pmt3 > 30)::int as collection_over_30_pmt3_aggressive,
	(agg.over_pmt4 > 30)::int as collection_over_30_pmt4_aggressive,
	(agg.over_pmt5 > 30)::int as collection_over_30_pmt5_aggressive,
	(agg.over_pmt6 > 30)::int as collection_over_30_pmt6_aggressive,
	(agg.ever_pmt1 > 30)::int as collection_ever_30_pmt1_aggressive,
	(agg.ever_pmt2 > 30)::int as collection_ever_30_pmt2_aggressive,
	(agg.ever_pmt3 > 30)::int as collection_ever_30_pmt3_aggressive,
	(agg.ever_pmt4 > 30)::int as collection_ever_30_pmt4_aggressive,
	(agg.ever_pmt5 > 30)::int as collection_ever_30_pmt5_aggressive,
	(agg.ever_pmt6 > 30)::int as collection_ever_30_pmt6_aggressive,
	dp.activity_sector,
	dp.sector,
	dp.cnae,
	dp.cnae_code,
	dp.month_revenue,
	dp.previous_loans_count,
	agg.ever_pmt1 as collection_ever_by_pmt1_aggressive,
	agg.ever_pmt2 as collection_ever_by_pmt2_aggressive,
	agg.ever_pmt3 as collection_ever_by_pmt3_aggressive,
	agg.ever_pmt4 as collection_ever_by_pmt4_aggressive,
	agg.ever_pmt5 as collection_ever_by_pmt5_aggressive,
	agg.ever_pmt6 as collection_ever_by_pmt6_aggressive
from t_extrato_tape as t
join public.loan_requests lr on lr.loan_request_id = t.loan_request_id
join public.offers o on o.offer_id = lr.offer_id
join public.direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
left join (select
		agg.loan_request_id,
		max(agg.atraso) filter (where case when foto_pmt1 < max_dia then dia = foto_pmt1 else agg.atraso = 0 end) as over_pmt1,
		max(agg.atraso) filter (where case when foto_pmt2 < max_dia then dia = foto_pmt2 else agg.atraso = 0 end) as over_pmt2,
		max(agg.atraso) filter (where case when foto_pmt3 < max_dia then dia = foto_pmt3 else agg.atraso = 0 end) as over_pmt3,
		max(agg.atraso) filter (where case when foto_pmt4 < max_dia then dia = foto_pmt4 else agg.atraso = 0 end) as over_pmt4,
		max(agg.atraso) filter (where case when foto_pmt5 < max_dia then dia = foto_pmt5 else agg.atraso = 0 end) as over_pmt5,
		max(agg.atraso) filter (where case when foto_pmt6 < max_dia then dia = foto_pmt6 else agg.atraso = 0 end) as over_pmt6,
		max(agg.atraso_maximo) filter (where dia = least(foto_pmt1,max_dia)) as ever_pmt1,
		max(agg.atraso_maximo) filter (where dia = least(foto_pmt2,max_dia)) as ever_pmt2,
		max(agg.atraso_maximo) filter (where dia = least(foto_pmt3,max_dia)) as ever_pmt3,
		max(agg.atraso_maximo) filter (where dia = least(foto_pmt4,max_dia)) as ever_pmt4,
		max(agg.atraso_maximo) filter (where dia = least(foto_pmt5,max_dia)) as ever_pmt5,
		max(agg.atraso_maximo) filter (where dia = least(foto_pmt6,max_dia)) as ever_pmt6
	from t_collection_aggressive as agg
	join (select
			fpp.emprestimo_id,
			coalesce(max(fp.vencimento_em) filter (where fp.numero = 1),max(lr.loan_date) + '1 month'::interval)::date as foto_pmt1,
			coalesce(max(fp.vencimento_em) filter (where fp.numero = 2),max(lr.loan_date) + '2 month'::interval)::date as foto_pmt2,
			coalesce(max(fp.vencimento_em) filter (where fp.numero = 3),max(lr.loan_date) + '3 month'::interval)::date as foto_pmt3,
			coalesce(max(fp.vencimento_em) filter (where fp.numero = 4),max(lr.loan_date) + '4 month'::interval)::date as foto_pmt4,
			coalesce(max(fp.vencimento_em) filter (where fp.numero = 5),max(lr.loan_date) + '5 month'::interval)::date as foto_pmt5,
			coalesce(max(fp.vencimento_em) filter (where fp.numero = 6),max(lr.loan_date) + '6 month'::interval)::date as foto_pmt6
		from public.financeiro_planopagamento fpp
		join public.loan_requests lr on lr.loan_request_id = fpp.emprestimo_id
		join public.financeiro_parcela fp on fp.plano_id = fpp.id
		where fpp."type" = 'ORIGINAL'
		group by 1) as foto on foto.emprestimo_id = agg.loan_request_id
	left join (select
			loan_request_id,
			max(dia) as max_dia
		from t_collection_aggressive t
		join public.financeiro_planopagamento fpp on fpp.emprestimo_id = t.loan_request_id
		where fpp.status = 'Pago'
		group by 1) as max_agg on max_agg.loan_request_id = agg.loan_request_id
	group by 1) as agg on agg.loan_request_id = t.loan_request_id)
	


select
	t.direct_prospect_id,
	lr.loan_request_id,
	o.client_id,
	lr.status,
	t.financeiro_faturamento_comprovado,
	t.financeiro_comportamento_faturamento,
	ofertas_substituidas,
	pedidos_substituidos
from t_approval_tape_2 t
join public.direct_prospects dp on dp.direct_prospect_id = t.direct_prospect_id
join(select
		o.direct_prospect_id,
		max(o.offer_id) filter (where lr.status = 'ACCEPTED') as ultima_oferta_aprovada,
		max(o.offer_id) filter (where lr.offer_id is not null and lr.status != 'Substituido') as ultima_oferta_com_pedido_nao_substituido,
		max(o.offer_id) filter (where lr.offer_id is not null) as ultima_oferta_com_pedido,
		max(o.offer_id) as ultima_oferta,
		count(*) filter (where o.status = 'Substituido') as ofertas_substituidas,
		count(*) filter (where lr.status = 'Substituido') as pedidos_substituidos
	from public.offers o
	left join public.loan_requests lr on lr.offer_id = o.offer_id
	group by 1) as filtro_oferta on filtro_oferta.direct_prospect_id = t.direct_prospect_id
join public.offers o on o.offer_id = coalesce(ultima_oferta_aprovada,ultima_oferta_com_pedido_nao_substituido,ultima_oferta_com_pedido,ultima_oferta)
join(select
		offer_id,
		max(loan_request_id) filter (where status = 'ACCEPTED') as ultimo_pedido_aprovado,
		max(loan_request_id) filter (where status != 'Substituido') as ultimo_pedido_nao_substituido,
		max(loan_request_id) as ultimo_pedido
	from public.loan_requests
	group by 1) as filtro_pedido on filtro_pedido.offer_id = coalesce(ultima_oferta_aprovada,ultima_oferta_com_pedido_nao_substituido,ultima_oferta_com_pedido,ultima_oferta)
join public.loan_requests lr on lr.loan_request_id = coalesce(ultimo_pedido_aprovado,ultimo_pedido_nao_substituido,ultimo_pedido)
left join t_extratos e on e.loan_request_id = lr.loan_request_id
where e.faturamento_medio is null and t.financeiro_faturamento_comprovado is not null and (ofertas_substituidas + pedidos_substituidos) > 0 --and lr.status in ('ACCEPTED','REJECTED','LOST') and dp.utm_source not in ('sebrae','estimulo2020')
order by 2 desc

			
insert into lucas_leal.t_extratos
(select extrato.*
from(select 
			id,
			"lead" as lead_id,
			loan_request_id,
			status as bizextrato_status,
			null::text as excel_model,
			null::int as document_id,
			null::numeric as faturamento_informado,
			null::numeric as serasa,
			("data"->'serasa'->>'consultadoFactoring')::bool::int as consulta_factoring,
			("data"->'monthlyIndicators'->0->>'id' || '-01')::date as mes_1,
			("data"->'monthlyIndicators'->1->>'id' || '-01')::date as mes_2,
			("data"->'monthlyIndicators'->2->>'id' || '-01')::date as mes_3,
			("data"->'monthlyIndicators'->3->>'id' || '-01')::date as mes_4,
			("data"->'monthlyIndicators'->4->>'id' || '-01')::date as mes_5,
			("data"->'monthlyIndicators'->0->>'workDays')::smallint as quantidade_dias_uteis_mes1,
			("data"->'monthlyIndicators'->1->>'workDays')::smallint as quantidade_dias_uteis_mes2,
			("data"->'monthlyIndicators'->2->>'workDays')::smallint as quantidade_dias_uteis_mes3,
			("data"->'monthlyIndicators'->3->>'workDays')::smallint as quantidade_dias_uteis_mes4,
			("data"->'monthlyIndicators'->4->>'workDays')::smallint as quantidade_dias_uteis_mes5,
			("data"->'monthlyIndicators'->0->>'totalDays')::smallint as quantidade_dias_mapeados_mes1,
			("data"->'monthlyIndicators'->1->>'totalDays')::smallint as quantidade_dias_mapeados_mes2,
			("data"->'monthlyIndicators'->2->>'totalDays')::smallint as quantidade_dias_mapeados_mes3,
			("data"->'monthlyIndicators'->3->>'totalDays')::smallint as quantidade_dias_mapeados_mes4,
			("data"->'monthlyIndicators'->4->>'totalDays')::smallint as quantidade_dias_mapeados_mes5,
			("data"->'monthlyIndicators'->0->>'verifiedRevenue')::numeric as faturamento_comprovado_mes1,
			("data"->'monthlyIndicators'->1->>'verifiedRevenue')::numeric as faturamento_comprovado_mes2,
			("data"->'monthlyIndicators'->2->>'verifiedRevenue')::numeric as faturamento_comprovado_mes3,
			("data"->'monthlyIndicators'->3->>'verifiedRevenue')::numeric as faturamento_comprovado_mes4,
			("data"->'monthlyIndicators'->4->>'verifiedRevenue')::numeric as faturamento_comprovado_mes5,
			("data"->'monthlyIndicators'->0->>'verifiedRevenuePercentage')::numeric / 100 as perc_faturamento_comprovado_mes1,
			("data"->'monthlyIndicators'->1->>'verifiedRevenuePercentage')::numeric / 100 as perc_faturamento_comprovado_mes2,
			("data"->'monthlyIndicators'->2->>'verifiedRevenuePercentage')::numeric / 100 as perc_faturamento_comprovado_mes3,
			("data"->'monthlyIndicators'->3->>'verifiedRevenuePercentage')::numeric / 100 as perc_faturamento_comprovado_mes4,
			("data"->'monthlyIndicators'->4->>'verifiedRevenuePercentage')::numeric / 100 as perc_faturamento_comprovado_mes5,
			("data"->'monthlyIndicators'->0->>'totalIncome')::numeric as total_movimentado_mes1,
			("data"->'monthlyIndicators'->1->>'totalIncome')::numeric as total_movimentado_mes2,
			("data"->'monthlyIndicators'->2->>'totalIncome')::numeric as total_movimentado_mes3,
			("data"->'monthlyIndicators'->3->>'totalIncome')::numeric as total_movimentado_mes4,
			("data"->'monthlyIndicators'->4->>'totalIncome')::numeric as total_movimentado_mes5,
			("data"->'monthlyIndicators'->0->>'totalIncomePercentage')::numeric / 100 as perc_total_movimentado_mes1,
			("data"->'monthlyIndicators'->1->>'totalIncomePercentage')::numeric / 100 as perc_total_movimentado_mes2,
			("data"->'monthlyIndicators'->2->>'totalIncomePercentage')::numeric / 100 as perc_total_movimentado_mes3,
			("data"->'monthlyIndicators'->3->>'totalIncomePercentage')::numeric / 100 as perc_total_movimentado_mes4,
			("data"->'monthlyIndicators'->4->>'totalIncomePercentage')::numeric / 100 as perc_total_movimentado_mes5,
			("data"->>'averageIncome')::numeric as faturamento_medio,
			("data"->>'averageIncomePercentage')::numeric / 100 as perc_faturamento_medio,
			("data"->'monthlyIndicators'->0->>'incomeCount')::smallint as quantidade_entradas_mes1,
			("data"->'monthlyIndicators'->1->>'incomeCount')::smallint as quantidade_entradas_mes2,
			("data"->'monthlyIndicators'->2->>'incomeCount')::smallint as quantidade_entradas_mes3,
			("data"->'monthlyIndicators'->3->>'incomeCount')::smallint as quantidade_entradas_mes4,
			("data"->'monthlyIndicators'->4->>'incomeCount')::smallint as quantidade_entradas_mes5,
			("data"->'monthlyIndicators'->0->>'averageTicket')::numeric as ticket_medio_mes1,
			("data"->'monthlyIndicators'->1->>'averageTicket')::numeric as ticket_medio_mes2,
			("data"->'monthlyIndicators'->2->>'averageTicket')::numeric as ticket_medio_mes3,
			("data"->'monthlyIndicators'->3->>'averageTicket')::numeric as ticket_medio_mes4,
			("data"->'monthlyIndicators'->4->>'averageTicket')::numeric as ticket_medio_mes5,
			("data"->'monthlyIndicators'->0->>'greaterTicket')::numeric as maior_entrada_mes1,
			("data"->'monthlyIndicators'->1->>'greaterTicket')::numeric as maior_entrada_mes2,
			("data"->'monthlyIndicators'->2->>'greaterTicket')::numeric as maior_entrada_mes3,
			("data"->'monthlyIndicators'->3->>'greaterTicket')::numeric as maior_entrada_mes4,
			("data"->'monthlyIndicators'->4->>'greaterTicket')::numeric as maior_entrada_mes5,
			("data"->'monthlyIndicators'->0->>'cashDespositsPercentage')::numeric / 100 as deposito_especie_mes1,
			("data"->'monthlyIndicators'->1->>'cashDespositsPercentage')::numeric / 100 as deposito_especie_mes2,
			("data"->'monthlyIndicators'->2->>'cashDespositsPercentage')::numeric / 100 as deposito_especie_mes3,
			("data"->'monthlyIndicators'->3->>'cashDespositsPercentage')::numeric / 100 as deposito_especie_mes4,
			("data"->'monthlyIndicators'->4->>'cashDespositsPercentage')::numeric / 100 as deposito_especie_mes5,
			("data"->'monthlyIndicators'->0->>'advancementPercentage')::numeric / 100 as antecipacao_mes1,
			("data"->'monthlyIndicators'->1->>'advancementPercentage')::numeric / 100 as antecipacao_mes2,
			("data"->'monthlyIndicators'->2->>'advancementPercentage')::numeric / 100 as antecipacao_mes3,
			("data"->'monthlyIndicators'->3->>'advancementPercentage')::numeric / 100 as antecipacao_mes4,
			("data"->'monthlyIndicators'->4->>'advancementPercentage')::numeric / 100 as antecipacao_mes5,
			("data"->'monthlyIndicators'->0->>'marketplacePercentage')::numeric / 100 as marketplace_mes1,
			("data"->'monthlyIndicators'->1->>'marketplacePercentage')::numeric / 100 as marketplace_mes2,
			("data"->'monthlyIndicators'->2->>'marketplacePercentage')::numeric / 100 as marketplace_mes3,
			("data"->'monthlyIndicators'->3->>'marketplacePercentage')::numeric / 100 as marketplace_mes4,
			("data"->'monthlyIndicators'->4->>'marketplacePercentage')::numeric / 100 as marketplace_mes5,
			("data"->'monthlyIndicators'->0->>'mainDrawPercentage')::numeric / 100 as principal_sacado_mes1,
			("data"->'monthlyIndicators'->1->>'mainDrawPercentage')::numeric / 100 as principal_sacado_mes2,
			("data"->'monthlyIndicators'->2->>'mainDrawPercentage')::numeric / 100 as principal_sacado_mes3,
			("data"->'monthlyIndicators'->3->>'mainDrawPercentage')::numeric / 100 as principal_sacado_mes4,
			("data"->'monthlyIndicators'->4->>'mainDrawPercentage')::numeric / 100 as principal_sacado_mes5,
			("data"->'monthlyIndicators'->0->>'atmWithdraws')::numeric as saque_atm_mes1,
			("data"->'monthlyIndicators'->1->>'atmWithdraws')::numeric as saque_atm_mes2,
			("data"->'monthlyIndicators'->2->>'atmWithdraws')::numeric as saque_atm_mes3,
			("data"->'monthlyIndicators'->3->>'atmWithdraws')::numeric as saque_atm_mes4,
			("data"->'monthlyIndicators'->4->>'atmWithdraws')::numeric as saque_atm_mes5,
			("data"->'monthlyIndicators'->0->>'expenses')::numeric as contas_despesas_mes1,
			("data"->'monthlyIndicators'->1->>'expenses')::numeric as contas_despesas_mes2,
			("data"->'monthlyIndicators'->2->>'expenses')::numeric as contas_despesas_mes3,
			("data"->'monthlyIndicators'->3->>'expenses')::numeric as contas_despesas_mes4,
			("data"->'monthlyIndicators'->4->>'expenses')::numeric as contas_despesas_mes5,
			("data"->'monthlyIndicators'->0->>'creditPMT')::numeric as pgto_pmt_credito_mes1,
			("data"->'monthlyIndicators'->1->>'creditPMT')::numeric as pgto_pmt_credito_mes2,
			("data"->'monthlyIndicators'->2->>'creditPMT')::numeric as pgto_pmt_credito_mes3,
			("data"->'monthlyIndicators'->3->>'creditPMT')::numeric as pgto_pmt_credito_mes4,
			("data"->'monthlyIndicators'->4->>'creditPMT')::numeric as pgto_pmt_credito_mes5,
			("data"->'monthlyIndicators'->0->>'interest')::numeric as pgto_juros_mora_mes1,
			("data"->'monthlyIndicators'->1->>'interest')::numeric as pgto_juros_mora_mes2,
			("data"->'monthlyIndicators'->2->>'interest')::numeric as pgto_juros_mora_mes3,
			("data"->'monthlyIndicators'->3->>'interest')::numeric as pgto_juros_mora_mes4,
			("data"->'monthlyIndicators'->4->>'interest')::numeric as pgto_juros_mora_mes5,
			("data"->'monthlyIndicators'->0->>'greaterOutcome')::numeric as maior_saida_mes1,
			("data"->'monthlyIndicators'->1->>'greaterOutcome')::numeric as maior_saida_mes2,
			("data"->'monthlyIndicators'->2->>'greaterOutcome')::numeric as maior_saida_mes3,
			("data"->'monthlyIndicators'->3->>'greaterOutcome')::numeric as maior_saida_mes4,
			("data"->'monthlyIndicators'->4->>'greaterOutcome')::numeric as maior_saida_mes5,
			("data"->'monthlyIndicators'->0->>'totalOutcome')::numeric as total_saida_mes1,
			("data"->'monthlyIndicators'->1->>'totalOutcome')::numeric as total_saida_mes2,
			("data"->'monthlyIndicators'->2->>'totalOutcome')::numeric as total_saida_mes3,
			("data"->'monthlyIndicators'->3->>'totalOutcome')::numeric as total_saida_mes4,
			("data"->'monthlyIndicators'->4->>'totalOutcome')::numeric as total_saida_mes5,
			("data"->'monthlyIndicators'->0->'fluxoCaixa'->>'pior')::numeric as fluxo_caixa_pior_mes1,
			("data"->'monthlyIndicators'->1->'fluxoCaixa'->>'pior')::numeric as fluxo_caixa_pior_mes2,
			("data"->'monthlyIndicators'->2->'fluxoCaixa'->>'pior')::numeric as fluxo_caixa_pior_mes3,
			("data"->'monthlyIndicators'->3->'fluxoCaixa'->>'pior')::numeric as fluxo_caixa_pior_mes4,
			("data"->'monthlyIndicators'->4->'fluxoCaixa'->>'pior')::numeric as fluxo_caixa_pior_mes5,
			("data"->'monthlyIndicators'->0->'fluxoCaixa'->>'melhor')::numeric as fluxo_caixa_melhor_mes1,
			("data"->'monthlyIndicators'->1->'fluxoCaixa'->>'melhor')::numeric as fluxo_caixa_melhor_mes2,
			("data"->'monthlyIndicators'->2->'fluxoCaixa'->>'melhor')::numeric as fluxo_caixa_melhor_mes3,
			("data"->'monthlyIndicators'->3->'fluxoCaixa'->>'melhor')::numeric as fluxo_caixa_melhor_mes4,
			("data"->'monthlyIndicators'->4->'fluxoCaixa'->>'melhor')::numeric as fluxo_caixa_melhor_mes5,
			("data"->'monthlyIndicators'->0->'fluxoCaixa'->>'diaMelhor')::smallint as fluxo_caixa_dia_melhor_mes1,
			("data"->'monthlyIndicators'->1->'fluxoCaixa'->>'diaMelhor')::smallint as fluxo_caixa_dia_melhor_mes2,
			("data"->'monthlyIndicators'->2->'fluxoCaixa'->>'diaMelhor')::smallint as fluxo_caixa_dia_melhor_mes3,
			("data"->'monthlyIndicators'->3->'fluxoCaixa'->>'diaMelhor')::smallint as fluxo_caixa_dia_melhor_mes4,
			("data"->'monthlyIndicators'->4->'fluxoCaixa'->>'diaMelhor')::smallint as fluxo_caixa_dia_melhor_mes5,
			("data"->'monthlyIndicators'->0->'fluxoCaixa'->>'diasNegativo')::smallint as fluxo_caixa_dias_negativo_mes1,
			("data"->'monthlyIndicators'->1->'fluxoCaixa'->>'diasNegativo')::smallint as fluxo_caixa_dias_negativo_mes2,
			("data"->'monthlyIndicators'->2->'fluxoCaixa'->>'diasNegativo')::smallint as fluxo_caixa_dias_negativo_mes3,
			("data"->'monthlyIndicators'->3->'fluxoCaixa'->>'diasNegativo')::smallint as fluxo_caixa_dias_negativo_mes4,
			("data"->'monthlyIndicators'->4->'fluxoCaixa'->>'diasNegativo')::smallint as fluxo_caixa_dias_negativo_mes5,
			("data"->'monthlyIndicators'->0->'fluxoCaixa'->>'percentaulDiasNegativo')::numeric / 100 as fluxo_caixa_perc_dias_negativo_mes1,
			("data"->'monthlyIndicators'->1->'fluxoCaixa'->>'percentaulDiasNegativo')::numeric / 100 as fluxo_caixa_perc_dias_negativo_mes2,
			("data"->'monthlyIndicators'->2->'fluxoCaixa'->>'percentaulDiasNegativo')::numeric / 100 as fluxo_caixa_perc_dias_negativo_mes3,
			("data"->'monthlyIndicators'->3->'fluxoCaixa'->>'percentaulDiasNegativo')::numeric / 100 as fluxo_caixa_perc_dias_negativo_mes4,
			("data"->'monthlyIndicators'->4->'fluxoCaixa'->>'percentaulDiasNegativo')::numeric / 100 as fluxo_caixa_perc_dias_negativo_mes5,
			("data"->'monthlyIndicators'->0->'fluxoCaixa'->>'operacional')::numeric as fluxo_caixa_saldo_operacional_mes1,
			("data"->'monthlyIndicators'->1->'fluxoCaixa'->>'operacional')::numeric as fluxo_caixa_saldo_operacional_mes2,
			("data"->'monthlyIndicators'->2->'fluxoCaixa'->>'operacional')::numeric as fluxo_caixa_saldo_operacional_mes3,
			("data"->'monthlyIndicators'->3->'fluxoCaixa'->>'operacional')::numeric as fluxo_caixa_saldo_operacional_mes4,
			("data"->'monthlyIndicators'->4->'fluxoCaixa'->>'operacional')::numeric as fluxo_caixa_saldo_operacional_mes5,
			("data"->'monthlyIndicators'->0->'fluxoCaixa'->>'ateDiaLead')::numeric as fluxo_caixa_saldo_ate_dia_lead_mes1,
			("data"->'monthlyIndicators'->1->'fluxoCaixa'->>'ateDiaLead')::numeric as fluxo_caixa_saldo_ate_dia_lead_mes2,
			("data"->'monthlyIndicators'->2->'fluxoCaixa'->>'ateDiaLead')::numeric as fluxo_caixa_saldo_ate_dia_lead_mes3,
			("data"->'monthlyIndicators'->3->'fluxoCaixa'->>'ateDiaLead')::numeric as fluxo_caixa_saldo_ate_dia_lead_mes4,
			("data"->'monthlyIndicators'->4->'fluxoCaixa'->>'ateDiaLead')::numeric as fluxo_caixa_saldo_ate_dia_lead_mes5,
			("data"->'monthlyIndicators'->0->'scrPF'->>'creditoPJ')::numeric as scr_pf_credito_pj_mes1,
			("data"->'monthlyIndicators'->1->'scrPF'->>'creditoPJ')::numeric as scr_pf_credito_pj_mes2,
			("data"->'monthlyIndicators'->2->'scrPF'->>'creditoPJ')::numeric as scr_pf_credito_pj_mes3,
			("data"->'monthlyIndicators'->3->'scrPF'->>'creditoPJ')::numeric as scr_pf_credito_pj_mes4,
			("data"->'monthlyIndicators'->4->'scrPF'->>'creditoPJ')::numeric as scr_pf_credito_pj_mes5,
			("data"->'monthlyIndicators'->0->'scrPF'->>'limiteCredito')::numeric as scr_pf_limite_credito_mes1,
			("data"->'monthlyIndicators'->1->'scrPF'->>'limiteCredito')::numeric as scr_pf_limite_credito_mes2,
			("data"->'monthlyIndicators'->2->'scrPF'->>'limiteCredito')::numeric as scr_pf_limite_credito_mes3,
			("data"->'monthlyIndicators'->3->'scrPF'->>'limiteCredito')::numeric as scr_pf_limite_credito_mes4,
			("data"->'monthlyIndicators'->4->'scrPF'->>'limiteCredito')::numeric as scr_pf_limite_credito_mes5,
			("data"->'monthlyIndicators'->0->'scrPJ'->>'contaGarantida')::numeric as scr_pj_conta_garantida_mes1,
			("data"->'monthlyIndicators'->1->'scrPJ'->>'contaGarantida')::numeric as scr_pj_conta_garantida_mes2,
			("data"->'monthlyIndicators'->2->'scrPJ'->>'contaGarantida')::numeric as scr_pj_conta_garantida_mes3,
			("data"->'monthlyIndicators'->3->'scrPJ'->>'contaGarantida')::numeric as scr_pj_conta_garantida_mes4,
			("data"->'monthlyIndicators'->4->'scrPJ'->>'contaGarantida')::numeric as scr_pj_conta_garantida_mes5,
			("data"->'monthlyIndicators'->0->'scrPJ'->>'chequeEspecial')::numeric as scr_pj_cheque_especial_mes1,
			("data"->'monthlyIndicators'->1->'scrPJ'->>'chequeEspecial')::numeric as scr_pj_cheque_especial_mes2,
			("data"->'monthlyIndicators'->2->'scrPJ'->>'chequeEspecial')::numeric as scr_pj_cheque_especial_mes3,
			("data"->'monthlyIndicators'->3->'scrPJ'->>'chequeEspecial')::numeric as scr_pj_cheque_especial_mes4,
			("data"->'monthlyIndicators'->4->'scrPJ'->>'chequeEspecial')::numeric as scr_pj_cheque_especial_mes5,
			("data"->'monthlyIndicators'->0->'scrPJ'->>'direitoCreditorio')::numeric as scr_pj_direito_creditorio_mes1,
			("data"->'monthlyIndicators'->1->'scrPJ'->>'direitoCreditorio')::numeric as scr_pj_direito_creditorio_mes2,
			("data"->'monthlyIndicators'->2->'scrPJ'->>'direitoCreditorio')::numeric as scr_pj_direito_creditorio_mes3,
			("data"->'monthlyIndicators'->3->'scrPJ'->>'direitoCreditorio')::numeric as scr_pj_direito_creditorio_mes4,
			("data"->'monthlyIndicators'->4->'scrPJ'->>'direitoCreditorio')::numeric as scr_pj_direito_creditorio_mes5,
			("data"->'monthlyIndicators'->0->'scrPJ'->>'limiteCredito')::numeric as scr_pj_limite_credito_mes1,
			("data"->'monthlyIndicators'->1->'scrPJ'->>'limiteCredito')::numeric as scr_pj_limite_credito_mes2,
			("data"->'monthlyIndicators'->2->'scrPJ'->>'limiteCredito')::numeric as scr_pj_limite_credito_mes3,
			("data"->'monthlyIndicators'->3->'scrPJ'->>'limiteCredito')::numeric as scr_pj_limite_credito_mes4,
			("data"->'monthlyIndicators'->4->'scrPJ'->>'limiteCredito')::numeric as scr_pj_limite_credito_mes5,
			("data"->'monthlyIndicators'->0->'scrPJ'->>'dividaTotal')::numeric as scr_pj_divida_total_mes1,
			("data"->'monthlyIndicators'->1->'scrPJ'->>'dividaTotal')::numeric as scr_pj_divida_total_mes2,
			("data"->'monthlyIndicators'->2->'scrPJ'->>'dividaTotal')::numeric as scr_pj_divida_total_mes3,
			("data"->'monthlyIndicators'->3->'scrPJ'->>'dividaTotal')::numeric as scr_pj_divida_total_mes4,
			("data"->'monthlyIndicators'->4->'scrPJ'->>'dividaTotal')::numeric as scr_pj_divida_total_mes5,
			("data"->'complexidade'->>'score')::int as complex_score,
			("data"->'complexidade'->>'cnpjAte3Anos')::bool::int as complex_menos_3_anos,
			("data"->'complexidade'->>'fazParteGrupoEconomico')::bool::int as complex_grupo_economico,
			("data"->'complexidade'->>'outrosMembrosPossuemEmpresas')::bool::int as complex_familiares_ramo,
			("data"->'complexidade'->>'houveTrocaCNPJ')::bool::int as complex_baixa_troca_cnpj,
			("data"->'complexidade'->>'administradFamiliarNaoSocio')::bool::int as complex_admin_nao_socio,
			("data"->'complexidade'->>'criouEmpresaComMenos24Anos')::bool::int as complex_socio_menos_24,
			("data"->'complexidade'->>'contatoNaoSocio')::bool::int as complex_contato_nao_socio,
			("data"->'complexidade'->>'receitaDesproporcional')::bool::int as complex_receita_tempo_desprop,
			("data"->'complexidade'->>'alteracaoSocial12Meses')::bool::int as complex_alt_ult_12_meses,
			("data"->'complexidade'->>'outroCNPJEmDefault')::bool::int as complex_outro_cnpj_default,
			("data"->'complexidade'->>'historicoAnteriorCNPJ')::bool::int as complex_historico_anterior_cnpj,
			("data"->'complexidade'->>'faturamentoMaior100kESocioSemRisco')::bool::int as complex_fat_socio_sem_risco,
			("data"->'comportamento'->'entradas'->>'valorContratado')::numeric as comportamento_entradas_valor_contratado,
			("data"->'comportamento'->'entradas'->>'registrosDiversificados')::bool::int as comportamento_entradas_registro_diversificado,
			case when greatest(("data"->'monthlyIndicators'->0->>'marketplacePercentage')::numeric,("data"->'monthlyIndicators'->1->>'marketplacePercentage')::numeric,("data"->'monthlyIndicators'->2->>'marketplacePercentage')::numeric,("data"->'monthlyIndicators'->3->>'marketplacePercentage')::numeric,("data"->'monthlyIndicators'->4->>'marketplacePercentage')::numeric)	> 0 then 1
				when greatest(("data"->'monthlyIndicators'->0->>'marketplacePercentage')::numeric,("data"->'monthlyIndicators'->1->>'marketplacePercentage')::numeric,("data"->'monthlyIndicators'->2->>'marketplacePercentage')::numeric,("data"->'monthlyIndicators'->3->>'marketplacePercentage')::numeric,("data"->'monthlyIndicators'->4->>'marketplacePercentage')::numeric) = 0 then 0 end as comportamento_entradas_presenca_marketplace,
			("data"->'comportamento'->'entradas'->>'chequeSemFundoEstorno')::bool::int as comportamento_entrada_ch_est_semfundo,
			case when greatest(("data"->'monthlyIndicators'->0->>'advancementPercentage')::numeric,("data"->'monthlyIndicators'->1->>'advancementPercentage')::numeric,("data"->'monthlyIndicators'->2->>'advancementPercentage')::numeric,("data"->'monthlyIndicators'->3->>'advancementPercentage')::numeric,("data"->'monthlyIndicators'->4->>'advancementPercentage')::numeric) > 0 then 1
				when greatest(("data"->'monthlyIndicators'->0->>'advancementPercentage')::numeric,("data"->'monthlyIndicators'->1->>'advancementPercentage')::numeric,("data"->'monthlyIndicators'->2->>'advancementPercentage')::numeric,("data"->'monthlyIndicators'->3->>'advancementPercentage')::numeric,("data"->'monthlyIndicators'->4->>'advancementPercentage')::numeric) = 0 then 0 end as comportamento_entradas_antecipacao,
			("data"->'comportamento'->'entradas'->>'movimentacaoPJPF')::bool::int as comportamento_entradas_movimento_pj_pf,
			case when ("data"->'comportamento'->'entradas'->>'valorContratado')::numeric > 0 then 1 when ("data"->'comportamento'->'entradas'->>'valorContratado')::numeric = 0 then 0 end as comportamento_entradas_contraiu_credito,
			("data"->'comportamento'->'saidas'->>'pagamentoImpostos')::bool::int as comportamento_saidas_pagamento_imposto,
			case when greatest(("data"->'monthlyIndicators'->0->>'expenses')::numeric,("data"->'monthlyIndicators'->1->>'expenses')::numeric,("data"->'monthlyIndicators'->2->>'expenses')::numeric,("data"->'monthlyIndicators'->3->>'expenses')::numeric,("data"->'monthlyIndicators'->4->>'expenses')::numeric) > 0 then 1 
				when greatest(("data"->'monthlyIndicators'->0->>'expenses')::numeric,("data"->'monthlyIndicators'->1->>'expenses')::numeric,("data"->'monthlyIndicators'->2->>'expenses')::numeric,("data"->'monthlyIndicators'->3->>'expenses')::numeric,("data"->'monthlyIndicators'->4->>'expenses')::numeric) = 0 then 0 end as comportamento_saidas_pagamento_custo_fixo,
			("data"->'comportamento'->'saidas'->>'pagamentoSalarios')::bool::int as comportamento_saida_pagamento_salario,
			("data"->'comportamento'->'saidas'->>'emissaoChequeSemFundo')::bool::int as comportamento_saidas_ch_est_semfundo,
			null::int as comportamento_saidas_movimento_pj_pf,
			("data"->'comportamento'->'saidas'->>'debitosRegulares')::bool::int as comportamento_saidas_debitos_regulares,
			("data"->'passivoFinanceiro'->>'aPagar30DiasEVencido14Dias')::numeric as scr_pj_a_pagar_curto_prazo,
			("data"->>'divida_atual')::numeric as scr_pj_divida_atual,
			("data"->>'alavancagem_real')::numeric as scr_pj_alavancagem_real,
			("data"->>'divida_total_media')::numeric as scr_pj_divida_total_media,
			("data"->>'pmtMedio')::numeric as scr_pj_pmt_medio,
			false::bool as amostra_extrato_score
		from biz_credit.analysis
		where created_by not in ('Eduardo Varella','Pedro Castro','Vanessa Medeiros')
			and last_updated_by not in ('Eduardo Varella','Pedro Castro','Vanessa Medeiros')
			and status not in ('IMPORTED','DELETED') 
			and loan_request_id not in (select loan_request_id from lucas_leal.t_extratos)) as extrato
	join public.loan_requests lr on lr.loan_request_id = extrato.loan_request_id
	where (coalesce(total_movimentado_mes1,0) + coalesce(total_movimentado_mes2,0) + coalesce(total_movimentado_mes3,0) + coalesce(total_movimentado_mes4,0) + coalesce(total_movimentado_mes5,0)) > 0
		and lr.status in ('ACCEPTED','REJECTED','LOST','Expirado','Cancelado','Substituido'))		
/*
union
	(select
		id,
		"lead" as lead_id,
		loan_request_id,
		status as bizextrato_status,
		"source"->>'model' as excel_model,
		("source"->>'documentId')::int as document_id,
		faturamento_informado,
		serasa,
		null::int as consulta_factoring,
		(left(mes_1::text,4) || '-' || right(mes_1::text,2) || '-01')::date as mes_1,
		(left(mes_2::text,4) || '-' || right(mes_2::text,2) || '-01')::date as mes_2,
		(left(mes_3::text,4) || '-' || right(mes_3::text,2) || '-01')::date as mes_3,
		(left(mes_4::text,4) || '-' || right(mes_4::text,2) || '-01')::date as mes_4,
		(left(mes_5::text,4) || '-' || right(mes_5::text,2) || '-01')::date as mes_5,
		quantidade_dias_mapeados_mes1 as quantidade_dias_uteis_mes1,
		quantidade_dias_mapeados_mes2 as quantidade_dias_uteis_mes2,
		quantidade_dias_mapeados_mes3 as quantidade_dias_uteis_mes3,
		quantidade_dias_mapeados_mes4 as quantidade_dias_uteis_mes4,
		quantidade_dias_mapeados_mes5 as quantidade_dias_uteis_mes5,
		null::smallint as quantidade_dias_mapeados_mes1,
		null::smallint as quantidade_dias_mapeados_mes2,
		null::smallint as quantidade_dias_mapeados_mes3,
		null::smallint as quantidade_dias_mapeados_mes4,
		null::smallint as quantidade_dias_mapeados_mes5,
		faturamento_comprovado_mes1,
		faturamento_comprovado_mes2,
		faturamento_comprovado_mes3,
		faturamento_comprovado_mes4,
		faturamento_comprovado_mes5,
		faturamento_comprovado_mes1 / faturamento_informado as perc_faturamento_comprovado_mes1,
		faturamento_comprovado_mes2 / faturamento_informado as perc_faturamento_comprovado_mes2,
		faturamento_comprovado_mes3 / faturamento_informado as perc_faturamento_comprovado_mes3,
		faturamento_comprovado_mes4 / faturamento_informado as perc_faturamento_comprovado_mes4,
		faturamento_comprovado_mes5 / faturamento_informado as perc_faturamento_comprovado_mes5,
		total_movimentado_mes1,
		total_movimentado_mes2,
		total_movimentado_mes3,
		total_movimentado_mes4,
		total_movimentado_mes5,
		total_movimentado_mes1 / faturamento_informado as perc_total_movimentado_mes1,
		total_movimentado_mes2 / faturamento_informado as perc_total_movimentado_mes2,
		total_movimentado_mes3 / faturamento_informado as perc_total_movimentado_mes3,
		total_movimentado_mes4 / faturamento_informado as perc_total_movimentado_mes4,
		total_movimentado_mes5 / faturamento_informado as perc_total_movimentado_mes5,
		(case when quantidade_dias_uteis_mes1 >= 15 then faturamento_comprovado_mes1 else 0 end +
			case when quantidade_dias_uteis_mes2 >= 15 then faturamento_comprovado_mes2 else 0 end +
			case when quantidade_dias_uteis_mes3 >= 15 then faturamento_comprovado_mes3 else 0 end +
			case when quantidade_dias_uteis_mes4 >= 15 then faturamento_comprovado_mes4 else 0 end +
			case when quantidade_dias_uteis_mes5 >= 15 then faturamento_comprovado_mes5 else 0 end)::numeric / 
			greatest(case when quantidade_dias_uteis_mes1 >= 15 then 1 else 0 end +
				case when quantidade_dias_uteis_mes2 >= 15 then 1 else 0 end +
				case when quantidade_dias_uteis_mes3 >= 15 then 1 else 0 end +
				case when quantidade_dias_uteis_mes4 >= 15 then 1 else 0 end +
				case when quantidade_dias_uteis_mes5 >= 15 then 1 else 0 end,1) as faturamento_medio,
		(case when quantidade_dias_uteis_mes1 >= 15 then faturamento_comprovado_mes1 else 0 end +
			case when quantidade_dias_uteis_mes2 >= 15 then faturamento_comprovado_mes2 else 0 end +
			case when quantidade_dias_uteis_mes3 >= 15 then faturamento_comprovado_mes3 else 0 end +
			case when quantidade_dias_uteis_mes4 >= 15 then faturamento_comprovado_mes4 else 0 end +
			case when quantidade_dias_uteis_mes5 >= 15 then faturamento_comprovado_mes5 else 0 end)::numeric / 
			greatest(case when quantidade_dias_uteis_mes1 >= 15 then 1 else 0 end +
				case when quantidade_dias_uteis_mes2 >= 15 then 1 else 0 end +
				case when quantidade_dias_uteis_mes3 >= 15 then 1 else 0 end +
				case when quantidade_dias_uteis_mes4 >= 15 then 1 else 0 end +
				case when quantidade_dias_uteis_mes5 >= 15 then 1 else 0 end,1) / faturamento_informado as perc_faturamento_medio,
		quantidade_entradas_mes1,
		quantidade_entradas_mes2,
		quantidade_entradas_mes3,
		quantidade_entradas_mes4,
		quantidade_entradas_mes5,
		faturamento_comprovado_mes1 / quantidade_entradas_mes1 as ticket_medio_mes1,
		faturamento_comprovado_mes2 / quantidade_entradas_mes2 as ticket_medio_mes2,
		faturamento_comprovado_mes3 / quantidade_entradas_mes3 as ticket_medio_mes3,
		faturamento_comprovado_mes4 / quantidade_entradas_mes4 as ticket_medio_mes4,
		faturamento_comprovado_mes5 / quantidade_entradas_mes5 as ticket_medio_mes5,
		null::numeric as maior_entrada_mes1,
		null::numeric as maior_entrada_mes2,
		null::numeric as maior_entrada_mes3,
		null::numeric as maior_entrada_mes4,
		null::numeric as maior_entrada_mes5,
		deposito_especie_mes1,
		deposito_especie_mes2,
		deposito_especie_mes3,
		deposito_especie_mes4,
		deposito_especie_mes5,
		antecipacao_mes1,
		antecipacao_mes2,
		antecipacao_mes3,
		antecipacao_mes4,
		antecipacao_mes5,
		marketplace_mes1,
		marketplace_mes2,
		marketplace_mes3,
		marketplace_mes4,
		marketplace_mes5,
		principal_sacado_mes1,
		principal_sacado_mes2,
		principal_sacado_mes3,
		principal_sacado_mes4,
		principal_sacado_mes5,
		pgtos_saque_mes1 as saque_atm_mes1,
		pgtos_saque_mes2 as saque_atm_mes2,
		pgtos_saque_mes3 as saque_atm_mes3,
		pgtos_saque_mes4 as saque_atm_mes4,
		pgtos_saque_mes5 as saque_atm_mes5,
		pgtos_contas_desp_mes1 as contas_despesas_mes1,
		pgtos_contas_desp_mes2 as contas_despesas_mes2,
		pgtos_contas_desp_mes3 as contas_despesas_mes3,
		pgtos_contas_desp_mes4 as contas_despesas_mes4,
		pgtos_contas_desp_mes5 as contas_despesas_mes5,
		pgtos_pmt_credito_mes1 as pgto_pmt_credito_mes1,
		pgtos_pmt_credito_mes2 as pgto_pmt_credito_mes2,
		pgtos_pmt_credito_mes3 as pgto_pmt_credito_mes3,
		pgtos_pmt_credito_mes4 as pgto_pmt_credito_mes4,
		pgtos_pmt_credito_mes5 as pgto_pmt_credito_mes5,
		pgtos_moras_juros_mes1 as pgto_juros_mora_mes1,
		pgtos_moras_juros_mes2 as pgto_juros_mora_mes2,
		pgtos_moras_juros_mes3 as pgto_juros_mora_mes3,
		pgtos_moras_juros_mes4 as pgto_juros_mora_mes4,
		pgtos_moras_juros_mes5 as pgto_juros_mora_mes5,
		pgtos_maior_saida_mes1 as maior_saida_mes1,
		pgtos_maior_saida_mes2 as maior_saida_mes2,
		pgtos_maior_saida_mes3 as maior_saida_mes3,
		pgtos_maior_saida_mes4 as maior_saida_mes4,
		pgtos_maior_saida_mes5 as maior_saida_mes5,
		pgtos_total_mes1 as total_saida_mes1,
		pgtos_total_mes2 as total_saida_mes2,
		pgtos_total_mes3 as total_saida_mes3,
		pgtos_total_mes4 as total_saida_mes4,
		pgtos_total_mes5 as total_saida_mes5,
		pior_saldo_mes1 as fluxo_caixa_pior_mes1,
		pior_saldo_mes2 as fluxo_caixa_pior_mes2,
		pior_saldo_mes3 as fluxo_caixa_pior_mes3,
		pior_saldo_mes4 as fluxo_caixa_pior_mes4,
		pior_saldo_mes5 as fluxo_caixa_pior_mes5,
		melhor_saldo_mes1 as fluxo_caixa_melhor_mes1,
		melhor_saldo_mes2 as fluxo_caixa_melhor_mes2,
		melhor_saldo_mes3 as fluxo_caixa_melhor_mes3,
		melhor_saldo_mes4 as fluxo_caixa_melhor_mes4,
		melhor_saldo_mes5 as fluxo_caixa_melhor_mes5,
		melhor_saldo_mes1 as fluxo_caixa_dia_melhor_mes1,
		melhor_saldo_mes2 as fluxo_caixa_dia_melhor_mes2,
		melhor_saldo_mes3 as fluxo_caixa_dia_melhor_mes3,
		melhor_saldo_mes4 as fluxo_caixa_dia_melhor_mes4,
		melhor_saldo_mes5 as fluxo_caixa_dia_melhor_mes5,
		dias_negativo_mes1 as fluxo_caixa_dias_negativo_mes1,
		dias_negativo_mes2 as fluxo_caixa_dias_negativo_mes2,
		dias_negativo_mes3 as fluxo_caixa_dias_negativo_mes3,
		dias_negativo_mes4 as fluxo_caixa_dias_negativo_mes4,
		dias_negativo_mes5 as fluxo_caixa_dias_negativo_mes5,
		dias_negativo_mes1 / quantidade_dias_mapeados_mes1 as fluxo_caixa_perc_dias_negativo_mes1,
		dias_negativo_mes2 / quantidade_dias_mapeados_mes2 as fluxo_caixa_perc_dias_negativo_mes2,
		dias_negativo_mes3 / quantidade_dias_mapeados_mes3 as fluxo_caixa_perc_dias_negativo_mes3,
		dias_negativo_mes4 / quantidade_dias_mapeados_mes4 as fluxo_caixa_perc_dias_negativo_mes4,
		dias_negativo_mes5 / quantidade_dias_mapeados_mes5 as fluxo_caixa_perc_dias_negativo_mes5,
		saldo_fim_mes1 as fluxo_caixa_saldo_operacional_mes1,
		saldo_fim_mes2 as fluxo_caixa_saldo_operacional_mes2,
		saldo_fim_mes3 as fluxo_caixa_saldo_operacional_mes3,
		saldo_fim_mes4 as fluxo_caixa_saldo_operacional_mes4,
		saldo_fim_mes5 as fluxo_caixa_saldo_operacional_mes5,
		saldo_dia_lead_mes1 as fluxo_caixa_saldo_ate_dia_lead_mes1,
		saldo_dia_lead_mes2 as fluxo_caixa_saldo_ate_dia_lead_mes2,
		saldo_dia_lead_mes3 as fluxo_caixa_saldo_ate_dia_lead_mes3,
		saldo_dia_lead_mes4 as fluxo_caixa_saldo_ate_dia_lead_mes4,
		saldo_dia_lead_mes5 as fluxo_caixa_saldo_ate_dia_lead_mes5,
		null::numeric as scr_pf_credito_pj_mes1,
		null::numeric as scr_pf_credito_pj_mes2,
		null::numeric as scr_pf_credito_pj_mes3,
		null::numeric as scr_pf_credito_pj_mes4,
		null::numeric as scr_pf_credito_pj_mes5,
		null::numeric as scr_pf_limite_credito_mes1,
		null::numeric as scr_pf_limite_credito_mes2,
		null::numeric as scr_pf_limite_credito_mes3,
		null::numeric as scr_pf_limite_credito_mes4,
		null::numeric as scr_pf_limite_credito_mes5,
		null::numeric as scr_pj_conta_garantida_mes1,
		null::numeric as scr_pj_conta_garantida_mes2,
		null::numeric as scr_pj_conta_garantida_mes3,
		null::numeric as scr_pj_conta_garantida_mes4,
		null::numeric as scr_pj_conta_garantida_mes5,
		null::numeric as scr_pj_cheque_especial_mes1,
		null::numeric as scr_pj_cheque_especial_mes2,
		null::numeric as scr_pj_cheque_especial_mes3,
		null::numeric as scr_pj_cheque_especial_mes4,
		null::numeric as scr_pj_cheque_especial_mes5,
		null::numeric as scr_pj_direito_creditorio_mes1,
		null::numeric as scr_pj_direito_creditorio_mes2,
		null::numeric as scr_pj_direito_creditorio_mes3,
		null::numeric as scr_pj_direito_creditorio_mes4,
		null::numeric as scr_pj_direito_creditorio_mes5,
		null::numeric as scr_pj_limite_credito_mes1,
		null::numeric as scr_pj_limite_credito_mes2,
		null::numeric as scr_pj_limite_credito_mes3,
		null::numeric as scr_pj_limite_credito_mes4,
		null::numeric as scr_pj_limite_credito_mes5,
		null::numeric as scr_pj_divida_total_mes1,
		null::numeric as scr_pj_divida_total_mes2,
		null::numeric as scr_pj_divida_total_mes3,
		null::numeric as scr_pj_divida_total_mes4,
		null::numeric as scr_pj_divida_total_mes5,
		case when ceiling((complex_menos_3_anos::int * 2 + complex_grupo_economico::int * 2 + complex_familiares_ramo::int * 2 + complex_baixa_troca_cnpj::int * 3 + complex_admin_nao_socio::int * 3 + complex_socio_menos_24::int * 4 + complex_contato_nao_socio::int * 1 + complex_receita_tempo_desprop::int * 9 + complex_alt_ult_12_meses::int * 1 +complex_outro_cnpj_default::int * 6 + complex_historico_anterior_cnpj::int * 10 + complex_fat_socio_sem_risco::int * 7) + (complex_menos_3_anos::int + complex_grupo_economico::int + complex_familiares_ramo::int + complex_baixa_troca_cnpj::int + complex_admin_nao_socio::int + complex_socio_menos_24::int + complex_contato_nao_socio::int + complex_receita_tempo_desprop::int + complex_alt_ult_12_meses::int + complex_outro_cnpj_default::int + complex_historico_anterior_cnpj::int + complex_fat_socio_sem_risco::int) / 2) >= 20 then 5
			when ceiling((complex_menos_3_anos::int * 2 + complex_grupo_economico::int * 2 + complex_familiares_ramo::int * 2 + complex_baixa_troca_cnpj::int * 3 + complex_admin_nao_socio::int * 3 + complex_socio_menos_24::int * 4 + complex_contato_nao_socio::int * 1 + complex_receita_tempo_desprop::int * 9 + complex_alt_ult_12_meses::int * 1 +complex_outro_cnpj_default::int * 6 + complex_historico_anterior_cnpj::int * 10 + complex_fat_socio_sem_risco::int * 7) + (complex_menos_3_anos::int + complex_grupo_economico::int + complex_familiares_ramo::int + complex_baixa_troca_cnpj::int + complex_admin_nao_socio::int + complex_socio_menos_24::int + complex_contato_nao_socio::int + complex_receita_tempo_desprop::int + complex_alt_ult_12_meses::int + complex_outro_cnpj_default::int + complex_historico_anterior_cnpj::int + complex_fat_socio_sem_risco::int) / 2) > 14 then 4
			when ceiling((complex_menos_3_anos::int * 2 + complex_grupo_economico::int * 2 + complex_familiares_ramo::int * 2 + complex_baixa_troca_cnpj::int * 3 + complex_admin_nao_socio::int * 3 + complex_socio_menos_24::int * 4 + complex_contato_nao_socio::int * 1 + complex_receita_tempo_desprop::int * 9 + complex_alt_ult_12_meses::int * 1 +complex_outro_cnpj_default::int * 6 + complex_historico_anterior_cnpj::int * 10 + complex_fat_socio_sem_risco::int * 7) + (complex_menos_3_anos::int + complex_grupo_economico::int + complex_familiares_ramo::int + complex_baixa_troca_cnpj::int + complex_admin_nao_socio::int + complex_socio_menos_24::int + complex_contato_nao_socio::int + complex_receita_tempo_desprop::int + complex_alt_ult_12_meses::int + complex_outro_cnpj_default::int + complex_historico_anterior_cnpj::int + complex_fat_socio_sem_risco::int) / 2) > 8 then 3
			when ceiling((complex_menos_3_anos::int * 2 + complex_grupo_economico::int * 2 + complex_familiares_ramo::int * 2 + complex_baixa_troca_cnpj::int * 3 + complex_admin_nao_socio::int * 3 + complex_socio_menos_24::int * 4 + complex_contato_nao_socio::int * 1 + complex_receita_tempo_desprop::int * 9 + complex_alt_ult_12_meses::int * 1 +complex_outro_cnpj_default::int * 6 + complex_historico_anterior_cnpj::int * 10 + complex_fat_socio_sem_risco::int * 7) + (complex_menos_3_anos::int + complex_grupo_economico::int + complex_familiares_ramo::int + complex_baixa_troca_cnpj::int + complex_admin_nao_socio::int + complex_socio_menos_24::int + complex_contato_nao_socio::int + complex_receita_tempo_desprop::int + complex_alt_ult_12_meses::int + complex_outro_cnpj_default::int + complex_historico_anterior_cnpj::int + complex_fat_socio_sem_risco::int) / 2) > 3 then 2
			when ceiling((complex_menos_3_anos::int * 2 + complex_grupo_economico::int * 2 + complex_familiares_ramo::int * 2 + complex_baixa_troca_cnpj::int * 3 + complex_admin_nao_socio::int * 3 + complex_socio_menos_24::int * 4 + complex_contato_nao_socio::int * 1 + complex_receita_tempo_desprop::int * 9 + complex_alt_ult_12_meses::int * 1 +complex_outro_cnpj_default::int * 6 + complex_historico_anterior_cnpj::int * 10 + complex_fat_socio_sem_risco::int * 7) + (complex_menos_3_anos::int + complex_grupo_economico::int + complex_familiares_ramo::int + complex_baixa_troca_cnpj::int + complex_admin_nao_socio::int + complex_socio_menos_24::int + complex_contato_nao_socio::int + complex_receita_tempo_desprop::int + complex_alt_ult_12_meses::int + complex_outro_cnpj_default::int + complex_historico_anterior_cnpj::int + complex_fat_socio_sem_risco::int) / 2) > 0 then 1
		else 0 end as complex_score,
		complex_menos_3_anos::int,
		complex_grupo_economico::int,
		complex_familiares_ramo::int,
		complex_baixa_troca_cnpj::int,
		complex_admin_nao_socio::int,
		complex_socio_menos_24::int,
		complex_contato_nao_socio::int,
		complex_receita_tempo_desprop::int,
		complex_alt_ult_12_meses::int,
		complex_outro_cnpj_default::int,
		complex_historico_anterior_cnpj::int,
		complex_fat_socio_sem_risco::int,
		null::numeric as comportamento_entradas_valor_contratado,
		concentrado_sacado::int as comportamento_entradas_registro_diversificado,
		presenca_mktplace::int as comportamento_entradas_presenca_marketplace,
		entrada_ch_est_semfundo::int as comportamento_entrada_ch_est_semfundo,
		antecipacao::int as comportamento_entradas_antecipacao,
		inflow_pf_pj::int as comportamento_entradas_movimento_pj_pf,
		contraiu_credito::int as comportamento_entradas_contraiu_credito,
		pagamento_imposto::int as comportamento_saidas_pagamento_imposto,
		pagamento_custo_fixo::int as comportamento_saidas_pagamento_custo_fixo,
		pagamento_salario::int as comportamento_saida_pagamento_salario,
		saida_ch_est_semfundo::int as comportamento_saidas_ch_est_semfundo,
		outflow_pj_pf::int as comportamento_saidas_movimento_pj_pf,
		null::int as comportamento_saidas_debitos_regulares,
		null::numeric as scr_pj_a_pagar_curto_prazo,
		null::numeric as scr_pj_divida_atual,
		null::numeric as scr_pj_alavancagem_real,
		null::numeric as scr_pj_divida_total_media,
		null::numeric as scr_pj_pmt_medio
	from biz_credit.analysis
	where created_by not in ('Eduardo Varella','Pedro Castro','Vanessa Medeiros') 
		and last_updated_by not in ('Eduardo Varella','Pedro Castro','Vanessa Medeiros') 
		and status = 'IMPORTED' and loan_request_id not in (select loan_request_id from lucas_leal.t_extratos)
		and (coalesce(total_movimentado_mes1,0) + coalesce(total_movimentado_mes2,0) + coalesce(total_movimentado_mes3,0) + coalesce(total_movimentado_mes4,0) + coalesce(total_movimentado_mes5,0))  > 0)
*/
		

	
create table t_extrato_tape as
select
	t.*,
	t1.*,
	t.faturamento_medio / greatest(dp.month_revenue,1) as perc_faturamento_medio_admin,
	months_dive(vl_faturamento_comprovado_m1_15_du,vl_faturamento_comprovado_m2_15_du,vl_faturamento_comprovado_m3_15_du,vl_faturamento_comprovado_m4_15_du,vl_faturamento_comprovado_m5_15_du) as n_meses_queda_faturamento_comprovado_15_du,
	months_dive(vl_faturamento_comprovado_m1_20_du,vl_faturamento_comprovado_m2_20_du,vl_faturamento_comprovado_m3_20_du,vl_faturamento_comprovado_m4_20_du,vl_faturamento_comprovado_m5_20_du) as n_meses_queda_faturamento_comprovado_20_du,
	months_dive(vl_faturamento_comprovado_m1_10_du,vl_faturamento_comprovado_m2_10_du,vl_faturamento_comprovado_m3_10_du,vl_faturamento_comprovado_m4_10_du,vl_faturamento_comprovado_m5_10_du) as n_meses_queda_faturamento_comprovado_10_du,
	months_dive(vl_faturamento_comprovado_m1_15_du,vl_faturamento_comprovado_m2_15_du,vl_faturamento_comprovado_m3_15_du,vl_faturamento_comprovado_m4_15_du,vl_faturamento_comprovado_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_faturamento_comprovado_15_du,
	months_dive(vl_faturamento_comprovado_m1_20_du,vl_faturamento_comprovado_m2_20_du,vl_faturamento_comprovado_m3_20_du,vl_faturamento_comprovado_m4_20_du,vl_faturamento_comprovado_m5_20_du) / greatest(n_meses_20_du - 1,1) as perc_meses_queda_faturamento_comprovado_20_du,
	months_dive(vl_faturamento_comprovado_m1_10_du,vl_faturamento_comprovado_m2_10_du,vl_faturamento_comprovado_m3_10_du,vl_faturamento_comprovado_m4_10_du,vl_faturamento_comprovado_m5_10_du) / greatest(n_meses_10_du - 1,1) as perc_meses_queda_faturamento_comprovado_10_du,
	months_rise(vl_faturamento_comprovado_m1_15_du,vl_faturamento_comprovado_m2_15_du,vl_faturamento_comprovado_m3_15_du,vl_faturamento_comprovado_m4_15_du,vl_faturamento_comprovado_m5_15_du) as n_meses_alta_faturamento_comprovado_15_du,
	months_rise(vl_faturamento_comprovado_m1_20_du,vl_faturamento_comprovado_m2_20_du,vl_faturamento_comprovado_m3_20_du,vl_faturamento_comprovado_m4_20_du,vl_faturamento_comprovado_m5_20_du) as n_meses_alta_faturamento_comprovado_20_du,
	months_rise(vl_faturamento_comprovado_m1_10_du,vl_faturamento_comprovado_m2_10_du,vl_faturamento_comprovado_m3_10_du,vl_faturamento_comprovado_m4_10_du,vl_faturamento_comprovado_m5_10_du) as n_meses_alta_faturamento_comprovado_10_du,
	months_rise(vl_faturamento_comprovado_m1_15_du,vl_faturamento_comprovado_m2_15_du,vl_faturamento_comprovado_m3_15_du,vl_faturamento_comprovado_m4_15_du,vl_faturamento_comprovado_m5_15_du) / greatest(n_meses_15_du,1) as perc_meses_alta_faturamento_comprovado_15_du,
	months_rise(vl_faturamento_comprovado_m1_20_du,vl_faturamento_comprovado_m2_20_du,vl_faturamento_comprovado_m3_20_du,vl_faturamento_comprovado_m4_20_du,vl_faturamento_comprovado_m5_20_du) / greatest(n_meses_20_du,1) as perc_meses_alta_faturamento_comprovado_20_du,
	months_rise(vl_faturamento_comprovado_m1_10_du,vl_faturamento_comprovado_m2_10_du,vl_faturamento_comprovado_m3_10_du,vl_faturamento_comprovado_m4_10_du,vl_faturamento_comprovado_m5_10_du) / greatest(n_meses_10_du,1) as perc_meses_alta_faturamento_comprovado_10_du,
	vl_faturamento_comprovado_m1_15_du / greatest(vl_max_faturamento_comprovado_15_du,1) as r_curr_to_max_faturamento_comprovado_15_du,
	vl_faturamento_comprovado_m1_15_du / greatest(vl_avg_faturamento_comprovado_15_du,1) as r_curr_to_avg_faturamento_comprovado_15_du,
	vl_faturamento_comprovado_m1_15_du / greatest(vl_min_faturamento_comprovado_15_du,1) as r_curr_to_min_faturamento_comprovado_15_du,
	vl_faturamento_comprovado_m1_15_du / greatest(vl_ever_faturamento_comprovado_15_du,1) as r_curr_to_ever_faturamento_comprovado_15_du,
	vl_faturamento_comprovado_m1_20_du / greatest(vl_max_faturamento_comprovado_20_du,1) as r_curr_to_max_faturamento_comprovado_20_du,
	vl_faturamento_comprovado_m1_20_du / greatest(vl_avg_faturamento_comprovado_20_du,1) as r_curr_to_avg_faturamento_comprovado_20_du,
	vl_faturamento_comprovado_m1_20_du / greatest(vl_min_faturamento_comprovado_20_du,1) as r_curr_to_min_faturamento_comprovado_20_du,
	vl_faturamento_comprovado_m1_20_du / greatest(vl_ever_faturamento_comprovado_20_du,1) as r_curr_to_ever_faturamento_comprovado_20_du,
	vl_faturamento_comprovado_m1_10_du / greatest(vl_max_faturamento_comprovado_10_du,1) as r_curr_to_max_faturamento_comprovado_10_du,
	vl_faturamento_comprovado_m1_10_du / greatest(vl_avg_faturamento_comprovado_10_du,1) as r_curr_to_avg_faturamento_comprovado_10_du,
	vl_faturamento_comprovado_m1_10_du / greatest(vl_min_faturamento_comprovado_10_du,1) as r_curr_to_min_faturamento_comprovado_10_du,
	vl_faturamento_comprovado_m1_10_du / greatest(vl_ever_faturamento_comprovado_10_du,1) as r_curr_to_ever_faturamento_comprovado_10_du,
	vl_max_faturamento_comprovado_15_du / greatest(dp.month_revenue,1) as r_max_faturamento_comprovado_to_faturamento_admin_15_du,
	vl_avg_faturamento_comprovado_15_du / greatest(dp.month_revenue,1) as r_avg_faturamento_comprovado_to_faturamento_admin_15_du,
	vl_min_faturamento_comprovado_15_du / greatest(dp.month_revenue,1) as r_min_faturamento_comprovado_to_faturamento_admin_15_du,
	vl_ever_faturamento_comprovado_15_du / greatest(dp.month_revenue,1) as r_ever_faturamento_comprovado_to_faturamento_admin_15_du,
	vl_max_faturamento_comprovado_20_du / greatest(dp.month_revenue,1) as r_max_faturamento_comprovado_to_faturamento_admin_20_du,
	vl_avg_faturamento_comprovado_20_du / greatest(dp.month_revenue,1) as r_avg_faturamento_comprovado_to_faturamento_admin_20_du,
	vl_min_faturamento_comprovado_20_du / greatest(dp.month_revenue,1) as r_min_faturamento_comprovado_to_faturamento_admin_20_du,
	vl_ever_faturamento_comprovado_20_du / greatest(dp.month_revenue,1) as r_ever_faturamento_comprovado_to_faturamento_admin_20_du,
	vl_max_faturamento_comprovado_10_du / greatest(dp.month_revenue,1) as r_max_faturamento_comprovado_to_faturamento_admin_10_du,
	vl_avg_faturamento_comprovado_10_du / greatest(dp.month_revenue,1) as r_avg_faturamento_comprovado_to_faturamento_admin_10_du,
	vl_min_faturamento_comprovado_10_du / greatest(dp.month_revenue,1) as r_min_faturamento_comprovado_to_faturamento_admin_10_du,
	vl_ever_faturamento_comprovado_10_du / greatest(dp.month_revenue,1) as r_ever_faturamento_comprovado_to_faturamento_admin_10_du,
	months_dive(vl_total_movimentado_m1_15_du,vl_total_movimentado_m2_15_du,vl_total_movimentado_m3_15_du,vl_total_movimentado_m4_15_du,vl_total_movimentado_m5_15_du) as n_meses_queda_total_movimentado_15_du,
	months_dive(vl_total_movimentado_m1_20_du,vl_total_movimentado_m2_20_du,vl_total_movimentado_m3_20_du,vl_total_movimentado_m4_20_du,vl_total_movimentado_m5_20_du) as n_meses_queda_total_movimentado_20_du,
	months_dive(vl_total_movimentado_m1_10_du,vl_total_movimentado_m2_10_du,vl_total_movimentado_m3_10_du,vl_total_movimentado_m4_10_du,vl_total_movimentado_m5_10_du) as n_meses_queda_total_movimentado_10_du,
	months_dive(vl_total_movimentado_m1_15_du,vl_total_movimentado_m2_15_du,vl_total_movimentado_m3_15_du,vl_total_movimentado_m4_15_du,vl_total_movimentado_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_total_movimentado_15_du,
	months_dive(vl_total_movimentado_m1_20_du,vl_total_movimentado_m2_20_du,vl_total_movimentado_m3_20_du,vl_total_movimentado_m4_20_du,vl_total_movimentado_m5_20_du) / greatest(n_meses_20_du - 1,1) as perc_meses_queda_total_movimentado_20_du,
	months_dive(vl_total_movimentado_m1_10_du,vl_total_movimentado_m2_10_du,vl_total_movimentado_m3_10_du,vl_total_movimentado_m4_10_du,vl_total_movimentado_m5_10_du) / greatest(n_meses_10_du - 1,1) as perc_meses_queda_total_movimentado_10_du,
	months_rise(vl_total_movimentado_m1_15_du,vl_total_movimentado_m2_15_du,vl_total_movimentado_m3_15_du,vl_total_movimentado_m4_15_du,vl_total_movimentado_m5_15_du) as n_meses_alta_total_movimentado_15_du,
	months_rise(vl_total_movimentado_m1_20_du,vl_total_movimentado_m2_20_du,vl_total_movimentado_m3_20_du,vl_total_movimentado_m4_20_du,vl_total_movimentado_m5_20_du) as n_meses_alta_total_movimentado_20_du,
	months_rise(vl_total_movimentado_m1_10_du,vl_total_movimentado_m2_10_du,vl_total_movimentado_m3_10_du,vl_total_movimentado_m4_10_du,vl_total_movimentado_m5_10_du) as n_meses_alta_total_movimentado_10_du,
	months_rise(vl_total_movimentado_m1_15_du,vl_total_movimentado_m2_15_du,vl_total_movimentado_m3_15_du,vl_total_movimentado_m4_15_du,vl_total_movimentado_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_total_movimentado_15_du,
	months_rise(vl_total_movimentado_m1_20_du,vl_total_movimentado_m2_20_du,vl_total_movimentado_m3_20_du,vl_total_movimentado_m4_20_du,vl_total_movimentado_m5_20_du) / greatest(n_meses_20_du - 1,1) as perc_meses_alta_total_movimentado_20_du,
	months_rise(vl_total_movimentado_m1_10_du,vl_total_movimentado_m2_10_du,vl_total_movimentado_m3_10_du,vl_total_movimentado_m4_10_du,vl_total_movimentado_m5_10_du) / greatest(n_meses_10_du - 1,1) as perc_meses_alta_total_movimentado_10_du,
	vl_total_movimentado_m1_15_du / greatest(vl_max_total_movimentado_15_du,1) as r_curr_to_max_total_movimentado_15_du,
	vl_total_movimentado_m1_15_du / greatest(vl_avg_total_movimentado_15_du,1) as r_curr_to_avg_total_movimentado_15_du,
	vl_total_movimentado_m1_15_du / greatest(vl_min_total_movimentado_15_du,1) as r_curr_to_min_total_movimentado_15_du,
	vl_total_movimentado_m1_15_du / greatest(vl_ever_total_movimentado_15_du,1) as r_curr_to_ever_total_movimentado_15_du,
	vl_total_movimentado_m1_20_du / greatest(vl_max_total_movimentado_20_du,1) as r_curr_to_max_total_movimentado_20_du,
	vl_total_movimentado_m1_20_du / greatest(vl_avg_total_movimentado_20_du,1) as r_curr_to_avg_total_movimentado_20_du,
	vl_total_movimentado_m1_20_du / greatest(vl_min_total_movimentado_20_du,1) as r_curr_to_min_total_movimentado_20_du,
	vl_total_movimentado_m1_20_du / greatest(vl_ever_total_movimentado_20_du,1) as r_curr_to_ever_total_movimentado_20_du,
	vl_total_movimentado_m1_10_du / greatest(vl_max_total_movimentado_10_du,1) as r_curr_to_max_total_movimentado_10_du,
	vl_total_movimentado_m1_10_du / greatest(vl_avg_total_movimentado_10_du,1) as r_curr_to_avg_total_movimentado_10_du,
	vl_total_movimentado_m1_10_du / greatest(vl_min_total_movimentado_10_du,1) as r_curr_to_min_total_movimentado_10_du,
	vl_total_movimentado_m1_10_du / greatest(vl_ever_total_movimentado_10_du,1) as r_curr_to_ever_total_movimentado_10_du,
	vl_max_total_movimentado_15_du / greatest(dp.month_revenue,1) as r_max_total_movimentado_to_faturamento_admin_15_du,
	vl_avg_total_movimentado_15_du / greatest(dp.month_revenue,1) as r_avg_total_movimentado_to_faturamento_admin_15_du,
	vl_min_total_movimentado_15_du / greatest(dp.month_revenue,1) as r_min_total_movimentado_to_faturamento_admin_15_du,
	vl_ever_total_movimentado_15_du / greatest(dp.month_revenue,1) as r_ever_total_movimentado_to_faturamento_admin_15_du,
	vl_max_total_movimentado_20_du / greatest(dp.month_revenue,1) as r_max_total_movimentado_to_faturamento_admin_20_du,
	vl_avg_total_movimentado_20_du / greatest(dp.month_revenue,1) as r_avg_total_movimentado_to_faturamento_admin_20_du,
	vl_min_total_movimentado_20_du / greatest(dp.month_revenue,1) as r_min_total_movimentado_to_faturamento_admin_20_du,
	vl_ever_total_movimentado_20_du / greatest(dp.month_revenue,1) as r_ever_total_movimentado_to_faturamento_admin_20_du,
	vl_max_total_movimentado_10_du / greatest(dp.month_revenue,1) as r_max_total_movimentado_to_faturamento_admin_10_du,
	vl_avg_total_movimentado_10_du / greatest(dp.month_revenue,1) as r_avg_total_movimentado_to_faturamento_admin_10_du,
	vl_min_total_movimentado_10_du / greatest(dp.month_revenue,1) as r_min_total_movimentado_to_faturamento_admin_10_du,
	vl_ever_total_movimentado_10_du / greatest(dp.month_revenue,1) as r_ever_total_movimentado_to_faturamento_admin_10_du,
	months_dive(vl_quantidade_entradas_m1_15_du,vl_quantidade_entradas_m2_15_du,vl_quantidade_entradas_m3_15_du,vl_quantidade_entradas_m4_15_du,vl_quantidade_entradas_m5_15_du) as n_meses_queda_quantidade_entradas_15_du,
	months_dive(vl_quantidade_entradas_m1_20_du,vl_quantidade_entradas_m2_20_du,vl_quantidade_entradas_m3_20_du,vl_quantidade_entradas_m4_20_du,vl_quantidade_entradas_m5_20_du) as n_meses_queda_quantidade_entradas_20_du,
	months_dive(vl_quantidade_entradas_m1_10_du,vl_quantidade_entradas_m2_10_du,vl_quantidade_entradas_m3_10_du,vl_quantidade_entradas_m4_10_du,vl_quantidade_entradas_m5_10_du) as n_meses_queda_quantidade_entradas_10_du,
	months_dive(vl_quantidade_entradas_m1_15_du,vl_quantidade_entradas_m2_15_du,vl_quantidade_entradas_m3_15_du,vl_quantidade_entradas_m4_15_du,vl_quantidade_entradas_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_quantidade_entradas_15_du,
	months_dive(vl_quantidade_entradas_m1_20_du,vl_quantidade_entradas_m2_20_du,vl_quantidade_entradas_m3_20_du,vl_quantidade_entradas_m4_20_du,vl_quantidade_entradas_m5_20_du) / greatest(n_meses_20_du - 1,1) as perc_meses_queda_quantidade_entradas_20_du,
	months_dive(vl_quantidade_entradas_m1_10_du,vl_quantidade_entradas_m2_10_du,vl_quantidade_entradas_m3_10_du,vl_quantidade_entradas_m4_10_du,vl_quantidade_entradas_m5_10_du) / greatest(n_meses_10_du - 1,1) as perc_meses_queda_quantidade_entradas_10_du,
	months_rise(vl_quantidade_entradas_m1_15_du,vl_quantidade_entradas_m2_15_du,vl_quantidade_entradas_m3_15_du,vl_quantidade_entradas_m4_15_du,vl_quantidade_entradas_m5_15_du) as n_meses_alta_quantidade_entradas_15_du,
	months_rise(vl_quantidade_entradas_m1_20_du,vl_quantidade_entradas_m2_20_du,vl_quantidade_entradas_m3_20_du,vl_quantidade_entradas_m4_20_du,vl_quantidade_entradas_m5_20_du) as n_meses_alta_quantidade_entradas_20_du,
	months_rise(vl_quantidade_entradas_m1_10_du,vl_quantidade_entradas_m2_10_du,vl_quantidade_entradas_m3_10_du,vl_quantidade_entradas_m4_10_du,vl_quantidade_entradas_m5_10_du) as n_meses_alta_quantidade_entradas_10_du,
	months_rise(vl_quantidade_entradas_m1_15_du,vl_quantidade_entradas_m2_15_du,vl_quantidade_entradas_m3_15_du,vl_quantidade_entradas_m4_15_du,vl_quantidade_entradas_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_quantidade_entradas_15_du,
	months_rise(vl_quantidade_entradas_m1_20_du,vl_quantidade_entradas_m2_20_du,vl_quantidade_entradas_m3_20_du,vl_quantidade_entradas_m4_20_du,vl_quantidade_entradas_m5_20_du) / greatest(n_meses_20_du - 1,1) as perc_meses_alta_quantidade_entradas_20_du,
	months_rise(vl_quantidade_entradas_m1_10_du,vl_quantidade_entradas_m2_10_du,vl_quantidade_entradas_m3_10_du,vl_quantidade_entradas_m4_10_du,vl_quantidade_entradas_m5_10_du) / greatest(n_meses_10_du - 1,1) as perc_meses_alta_quantidade_entradas_10_du,
	vl_quantidade_entradas_m1_15_du::numeric / greatest(vl_max_quantidade_entradas_15_du,1) as r_curr_to_max_quantidade_entradas_15_du,
	vl_quantidade_entradas_m1_15_du::numeric / greatest(vl_avg_quantidade_entradas_15_du,1) as r_curr_to_avg_quantidade_entradas_15_du,
	vl_quantidade_entradas_m1_15_du::numeric / greatest(vl_min_quantidade_entradas_15_du,1) as r_curr_to_min_quantidade_entradas_15_du,
	vl_quantidade_entradas_m1_15_du::numeric / greatest(vl_ever_quantidade_entradas_15_du,1) as r_curr_to_ever_quantidade_entradas_15_du,
	vl_quantidade_entradas_m1_20_du::numeric / greatest(vl_max_quantidade_entradas_20_du,1) as r_curr_to_max_quantidade_entradas_20_du,
	vl_quantidade_entradas_m1_20_du::numeric / greatest(vl_avg_quantidade_entradas_20_du,1) as r_curr_to_avg_quantidade_entradas_20_du,
	vl_quantidade_entradas_m1_20_du::numeric / greatest(vl_min_quantidade_entradas_20_du,1) as r_curr_to_min_quantidade_entradas_20_du,
	vl_quantidade_entradas_m1_20_du::numeric / greatest(vl_ever_quantidade_entradas_20_du,1) as r_curr_to_ever_quantidade_entradas_20_du,
	vl_quantidade_entradas_m1_10_du::numeric / greatest(vl_max_quantidade_entradas_10_du,1) as r_curr_to_max_quantidade_entradas_10_du,
	vl_quantidade_entradas_m1_10_du::numeric / greatest(vl_avg_quantidade_entradas_10_du,1) as r_curr_to_avg_quantidade_entradas_10_du,
	vl_quantidade_entradas_m1_10_du::numeric / greatest(vl_min_quantidade_entradas_10_du,1) as r_curr_to_min_quantidade_entradas_10_du,
	vl_quantidade_entradas_m1_10_du::numeric / greatest(vl_ever_quantidade_entradas_10_du,1) as r_curr_to_ever_quantidade_entradas_10_du,
	months_dive(vl_ticket_medio_m1_15_du,vl_ticket_medio_m2_15_du,vl_ticket_medio_m3_15_du,vl_ticket_medio_m4_15_du,vl_ticket_medio_m5_15_du) as n_meses_queda_ticket_medio_15_du,
	months_dive(vl_ticket_medio_m1_15_du,vl_ticket_medio_m2_15_du,vl_ticket_medio_m3_15_du,vl_ticket_medio_m4_15_du,vl_ticket_medio_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_ticket_medio_15_du,
	months_rise(vl_ticket_medio_m1_15_du,vl_ticket_medio_m2_15_du,vl_ticket_medio_m3_15_du,vl_ticket_medio_m4_15_du,vl_ticket_medio_m5_15_du) as n_meses_alta_ticket_medio_15_du,
	months_rise(vl_ticket_medio_m1_15_du,vl_ticket_medio_m2_15_du,vl_ticket_medio_m3_15_du,vl_ticket_medio_m4_15_du,vl_ticket_medio_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_ticket_medio_15_du,
	vl_ticket_medio_m1_15_du::numeric / greatest(vl_max_ticket_medio_15_du,1) as r_curr_to_max_ticket_medio_15_du,
	vl_ticket_medio_m1_15_du::numeric / greatest(vl_avg_ticket_medio_15_du,1) as r_curr_to_avg_ticket_medio_15_du,
	vl_ticket_medio_m1_15_du::numeric / greatest(vl_min_ticket_medio_15_du,1) as r_curr_to_min_ticket_medio_15_du,
	vl_ticket_medio_m1_15_du::numeric / greatest(vl_ever_ticket_medio_15_du,1) as r_curr_to_ever_ticket_medio_15_du,
	months_dive(vl_maior_entrada_m1_15_du,vl_maior_entrada_m2_15_du,vl_maior_entrada_m3_15_du,vl_maior_entrada_m4_15_du,vl_maior_entrada_m5_15_du) as n_meses_queda_maior_entrada_15_du,
	months_dive(vl_maior_entrada_m1_15_du,vl_maior_entrada_m2_15_du,vl_maior_entrada_m3_15_du,vl_maior_entrada_m4_15_du,vl_maior_entrada_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_maior_entrada_15_du,
	months_rise(vl_maior_entrada_m1_15_du,vl_maior_entrada_m2_15_du,vl_maior_entrada_m3_15_du,vl_maior_entrada_m4_15_du,vl_maior_entrada_m5_15_du) as n_meses_alta_maior_entrada_15_du,
	months_rise(vl_maior_entrada_m1_15_du,vl_maior_entrada_m2_15_du,vl_maior_entrada_m3_15_du,vl_maior_entrada_m4_15_du,vl_maior_entrada_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_maior_entrada_15_du,
	vl_maior_entrada_m1_15_du::numeric / greatest(vl_max_maior_entrada_15_du,1) as r_curr_to_max_maior_entrada_15_du,
	vl_maior_entrada_m1_15_du::numeric / greatest(vl_avg_maior_entrada_15_du,1) as r_curr_to_avg_maior_entrada_15_du,
	vl_maior_entrada_m1_15_du::numeric / greatest(vl_min_maior_entrada_15_du,1) as r_curr_to_min_maior_entrada_15_du,
	vl_maior_entrada_m1_15_du::numeric / greatest(vl_ever_maior_entrada_15_du,1) as r_curr_to_ever_maior_entrada_15_du,
	vl_max_maior_entrada_15_du::numeric / greatest(vl_max_ticket_medio_15_du,1) as r_max_maior_entrada_to_max_ticket_medio_15_du,
	vl_avg_maior_entrada_15_du::numeric / greatest(vl_avg_ticket_medio_15_du,1) as r_avg_maior_entrada_to_avg_ticket_medio_15_du,
	vl_min_maior_entrada_15_du::numeric / greatest(vl_min_ticket_medio_15_du,1) as r_min_maior_entrada_to_min_ticket_medio_15_du,
	vl_ever_maior_entrada_15_du::numeric / greatest(vl_ever_ticket_medio_15_du,1) as r_ever_maior_entrada_to_ever_ticket_medio_15_du,
	months_dive(vl_deposito_especie_m1_15_du,vl_deposito_especie_m2_15_du,vl_deposito_especie_m3_15_du,vl_deposito_especie_m4_15_du,vl_deposito_especie_m5_15_du) as n_meses_queda_deposito_especie_15_du,
	months_dive(vl_deposito_especie_m1_15_du,vl_deposito_especie_m2_15_du,vl_deposito_especie_m3_15_du,vl_deposito_especie_m4_15_du,vl_deposito_especie_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_deposito_especie_15_du,
	months_rise(vl_deposito_especie_m1_15_du,vl_deposito_especie_m2_15_du,vl_deposito_especie_m3_15_du,vl_deposito_especie_m4_15_du,vl_deposito_especie_m5_15_du) as n_meses_alta_deposito_especie_15_du,
	months_rise(vl_deposito_especie_m1_15_du,vl_deposito_especie_m2_15_du,vl_deposito_especie_m3_15_du,vl_deposito_especie_m4_15_du,vl_deposito_especie_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_deposito_especie_15_du,
	vl_deposito_especie_m1_15_du::numeric / greatest(vl_max_deposito_especie_15_du,1) as r_curr_to_max_deposito_especie_15_du,
	vl_deposito_especie_m1_15_du::numeric / greatest(vl_avg_deposito_especie_15_du,1) as r_curr_to_avg_deposito_especie_15_du,
	vl_deposito_especie_m1_15_du::numeric / greatest(vl_min_deposito_especie_15_du,1) as r_curr_to_min_deposito_especie_15_du,
	vl_deposito_especie_m1_15_du::numeric / greatest(vl_ever_deposito_especie_15_du,1) as r_curr_to_ever_deposito_especie_15_du,
	months_dive(vl_antecipacao_m1_15_du,vl_antecipacao_m2_15_du,vl_antecipacao_m3_15_du,vl_antecipacao_m4_15_du,vl_antecipacao_m5_15_du) as n_meses_queda_antecipacao_15_du,
	months_dive(vl_antecipacao_m1_15_du,vl_antecipacao_m2_15_du,vl_antecipacao_m3_15_du,vl_antecipacao_m4_15_du,vl_antecipacao_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_antecipacao_15_du,
	months_rise(vl_antecipacao_m1_15_du,vl_antecipacao_m2_15_du,vl_antecipacao_m3_15_du,vl_antecipacao_m4_15_du,vl_antecipacao_m5_15_du) as n_meses_alta_antecipacao_15_du,
	months_rise(vl_antecipacao_m1_15_du,vl_antecipacao_m2_15_du,vl_antecipacao_m3_15_du,vl_antecipacao_m4_15_du,vl_antecipacao_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_antecipacao_15_du,
	vl_antecipacao_m1_15_du::numeric / greatest(vl_max_antecipacao_15_du,1) as r_curr_to_max_antecipacao_15_du,
	vl_antecipacao_m1_15_du::numeric / greatest(vl_avg_antecipacao_15_du,1) as r_curr_to_avg_antecipacao_15_du,
	vl_antecipacao_m1_15_du::numeric / greatest(vl_min_antecipacao_15_du,1) as r_curr_to_min_antecipacao_15_du,
	vl_antecipacao_m1_15_du::numeric / greatest(vl_ever_antecipacao_15_du,1) as r_curr_to_ever_antecipacao_15_du,
	months_dive(vl_marketplace_m1_15_du,vl_marketplace_m2_15_du,vl_marketplace_m3_15_du,vl_marketplace_m4_15_du,vl_marketplace_m5_15_du) as n_meses_queda_marketplace_15_du,
	months_dive(vl_marketplace_m1_15_du,vl_marketplace_m2_15_du,vl_marketplace_m3_15_du,vl_marketplace_m4_15_du,vl_marketplace_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_marketplace_15_du,
	months_rise(vl_marketplace_m1_15_du,vl_marketplace_m2_15_du,vl_marketplace_m3_15_du,vl_marketplace_m4_15_du,vl_marketplace_m5_15_du) as n_meses_alta_marketplace_15_du,
	months_rise(vl_marketplace_m1_15_du,vl_marketplace_m2_15_du,vl_marketplace_m3_15_du,vl_marketplace_m4_15_du,vl_marketplace_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_marketplace_15_du,
	vl_marketplace_m1_15_du::numeric / greatest(vl_max_marketplace_15_du,1) as r_curr_to_max_marketplace_15_du,
	vl_marketplace_m1_15_du::numeric / greatest(vl_avg_marketplace_15_du,1) as r_curr_to_avg_marketplace_15_du,
	vl_marketplace_m1_15_du::numeric / greatest(vl_min_marketplace_15_du,1) as r_curr_to_min_marketplace_15_du,
	vl_marketplace_m1_15_du::numeric / greatest(vl_ever_marketplace_15_du,1) as r_curr_to_ever_marketplace_15_du,
	months_dive(vl_principal_sacado_m1_15_du,vl_principal_sacado_m2_15_du,vl_principal_sacado_m3_15_du,vl_principal_sacado_m4_15_du,vl_principal_sacado_m5_15_du) as n_meses_queda_principal_sacado_15_du,
	months_dive(vl_principal_sacado_m1_15_du,vl_principal_sacado_m2_15_du,vl_principal_sacado_m3_15_du,vl_principal_sacado_m4_15_du,vl_principal_sacado_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_principal_sacado_15_du,
	months_rise(vl_principal_sacado_m1_15_du,vl_principal_sacado_m2_15_du,vl_principal_sacado_m3_15_du,vl_principal_sacado_m4_15_du,vl_principal_sacado_m5_15_du) as n_meses_alta_principal_sacado_15_du,
	months_rise(vl_principal_sacado_m1_15_du,vl_principal_sacado_m2_15_du,vl_principal_sacado_m3_15_du,vl_principal_sacado_m4_15_du,vl_principal_sacado_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_principal_sacado_15_du,
	vl_principal_sacado_m1_15_du::numeric / greatest(vl_max_principal_sacado_15_du,1) as r_curr_to_max_principal_sacado_15_du,
	vl_principal_sacado_m1_15_du::numeric / greatest(vl_avg_principal_sacado_15_du,1) as r_curr_to_avg_principal_sacado_15_du,
	vl_principal_sacado_m1_15_du::numeric / greatest(vl_min_principal_sacado_15_du,1) as r_curr_to_min_principal_sacado_15_du,
	vl_principal_sacado_m1_15_du::numeric / greatest(vl_ever_principal_sacado_15_du,1) as r_curr_to_ever_principal_sacado_15_du,
	months_dive(vl_saque_atm_m1_15_du,vl_saque_atm_m2_15_du,vl_saque_atm_m3_15_du,vl_saque_atm_m4_15_du,vl_saque_atm_m5_15_du) as n_meses_queda_saque_atm_15_du,
	months_dive(vl_saque_atm_m1_15_du,vl_saque_atm_m2_15_du,vl_saque_atm_m3_15_du,vl_saque_atm_m4_15_du,vl_saque_atm_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_saque_atm_15_du,
	months_rise(vl_saque_atm_m1_15_du,vl_saque_atm_m2_15_du,vl_saque_atm_m3_15_du,vl_saque_atm_m4_15_du,vl_saque_atm_m5_15_du) as n_meses_alta_saque_atm_15_du,
	months_rise(vl_saque_atm_m1_15_du,vl_saque_atm_m2_15_du,vl_saque_atm_m3_15_du,vl_saque_atm_m4_15_du,vl_saque_atm_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_saque_atm_15_du,
	vl_saque_atm_m1_15_du::numeric / greatest(vl_max_saque_atm_15_du,1) as r_curr_to_max_saque_atm_15_du,
	vl_saque_atm_m1_15_du::numeric / greatest(vl_avg_saque_atm_15_du,1) as r_curr_to_avg_saque_atm_15_du,
	vl_saque_atm_m1_15_du::numeric / greatest(vl_min_saque_atm_15_du,1) as r_curr_to_min_saque_atm_15_du,
	vl_saque_atm_m1_15_du::numeric / greatest(vl_ever_saque_atm_15_du,1) as r_curr_to_ever_saque_atm_15_du,
	months_dive(vl_contas_despesas_m1_15_du,vl_contas_despesas_m2_15_du,vl_contas_despesas_m3_15_du,vl_contas_despesas_m4_15_du,vl_contas_despesas_m5_15_du) as n_meses_queda_contas_despesas_15_du,
	months_dive(vl_contas_despesas_m1_15_du,vl_contas_despesas_m2_15_du,vl_contas_despesas_m3_15_du,vl_contas_despesas_m4_15_du,vl_contas_despesas_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_contas_despesas_15_du,
	months_rise(vl_contas_despesas_m1_15_du,vl_contas_despesas_m2_15_du,vl_contas_despesas_m3_15_du,vl_contas_despesas_m4_15_du,vl_contas_despesas_m5_15_du) as n_meses_alta_contas_despesas_15_du,
	months_rise(vl_contas_despesas_m1_15_du,vl_contas_despesas_m2_15_du,vl_contas_despesas_m3_15_du,vl_contas_despesas_m4_15_du,vl_contas_despesas_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_contas_despesas_15_du,
	vl_contas_despesas_m1_15_du::numeric / greatest(vl_max_contas_despesas_15_du,1) as r_curr_to_max_contas_despesas_15_du,
	vl_contas_despesas_m1_15_du::numeric / greatest(vl_avg_contas_despesas_15_du,1) as r_curr_to_avg_contas_despesas_15_du,
	vl_contas_despesas_m1_15_du::numeric / greatest(vl_min_contas_despesas_15_du,1) as r_curr_to_min_contas_despesas_15_du,
	vl_contas_despesas_m1_15_du::numeric / greatest(vl_ever_contas_despesas_15_du,1) as r_curr_to_ever_contas_despesas_15_du,
	months_dive(vl_pgto_pmt_credito_m1_15_du,vl_pgto_pmt_credito_m2_15_du,vl_pgto_pmt_credito_m3_15_du,vl_pgto_pmt_credito_m4_15_du,vl_pgto_pmt_credito_m5_15_du) as n_meses_queda_pgto_pmt_credito_15_du,
	months_dive(vl_pgto_pmt_credito_m1_15_du,vl_pgto_pmt_credito_m2_15_du,vl_pgto_pmt_credito_m3_15_du,vl_pgto_pmt_credito_m4_15_du,vl_pgto_pmt_credito_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_pgto_pmt_credito_15_du,
	months_rise(vl_pgto_pmt_credito_m1_15_du,vl_pgto_pmt_credito_m2_15_du,vl_pgto_pmt_credito_m3_15_du,vl_pgto_pmt_credito_m4_15_du,vl_pgto_pmt_credito_m5_15_du) as n_meses_alta_pgto_pmt_credito_15_du,
	months_rise(vl_pgto_pmt_credito_m1_15_du,vl_pgto_pmt_credito_m2_15_du,vl_pgto_pmt_credito_m3_15_du,vl_pgto_pmt_credito_m4_15_du,vl_pgto_pmt_credito_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_pgto_pmt_credito_15_du,
	vl_pgto_pmt_credito_m1_15_du::numeric / greatest(vl_max_pgto_pmt_credito_15_du,1) as r_curr_to_max_pgto_pmt_credito_15_du,
	vl_pgto_pmt_credito_m1_15_du::numeric / greatest(vl_avg_pgto_pmt_credito_15_du,1) as r_curr_to_avg_pgto_pmt_credito_15_du,
	vl_pgto_pmt_credito_m1_15_du::numeric / greatest(vl_min_pgto_pmt_credito_15_du,1) as r_curr_to_min_pgto_pmt_credito_15_du,
	vl_pgto_pmt_credito_m1_15_du::numeric / greatest(vl_ever_pgto_pmt_credito_15_du,1) as r_curr_to_ever_pgto_pmt_credito_15_du,
	months_dive(vl_pgto_juros_mora_m1_15_du,vl_pgto_juros_mora_m2_15_du,vl_pgto_juros_mora_m3_15_du,vl_pgto_juros_mora_m4_15_du,vl_pgto_juros_mora_m5_15_du) as n_meses_queda_pgto_juros_mora_15_du,
	months_dive(vl_pgto_juros_mora_m1_15_du,vl_pgto_juros_mora_m2_15_du,vl_pgto_juros_mora_m3_15_du,vl_pgto_juros_mora_m4_15_du,vl_pgto_juros_mora_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_pgto_juros_mora_15_du,
	months_rise(vl_pgto_juros_mora_m1_15_du,vl_pgto_juros_mora_m2_15_du,vl_pgto_juros_mora_m3_15_du,vl_pgto_juros_mora_m4_15_du,vl_pgto_juros_mora_m5_15_du) as n_meses_alta_pgto_juros_mora_15_du,
	months_rise(vl_pgto_juros_mora_m1_15_du,vl_pgto_juros_mora_m2_15_du,vl_pgto_juros_mora_m3_15_du,vl_pgto_juros_mora_m4_15_du,vl_pgto_juros_mora_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_pgto_juros_mora_15_du,
	vl_pgto_juros_mora_m1_15_du::numeric / greatest(vl_max_pgto_juros_mora_15_du,1) as r_curr_to_max_pgto_juros_mora_15_du,
	vl_pgto_juros_mora_m1_15_du::numeric / greatest(vl_avg_pgto_juros_mora_15_du,1) as r_curr_to_avg_pgto_juros_mora_15_du,
	vl_pgto_juros_mora_m1_15_du::numeric / greatest(vl_min_pgto_juros_mora_15_du,1) as r_curr_to_min_pgto_juros_mora_15_du,
	vl_pgto_juros_mora_m1_15_du::numeric / greatest(vl_ever_pgto_juros_mora_15_du,1) as r_curr_to_ever_pgto_juros_mora_15_du,
	months_dive(vl_perc_saque_atm_m1_15_du,vl_perc_saque_atm_m2_15_du,vl_perc_saque_atm_m3_15_du,vl_perc_saque_atm_m4_15_du,vl_perc_saque_atm_m5_15_du) as n_meses_queda_perc_saque_atm_15_du,
	months_dive(vl_perc_saque_atm_m1_15_du,vl_perc_saque_atm_m2_15_du,vl_perc_saque_atm_m3_15_du,vl_perc_saque_atm_m4_15_du,vl_perc_saque_atm_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_perc_saque_atm_15_du,
	months_rise(vl_perc_saque_atm_m1_15_du,vl_perc_saque_atm_m2_15_du,vl_perc_saque_atm_m3_15_du,vl_perc_saque_atm_m4_15_du,vl_perc_saque_atm_m5_15_du) as n_meses_alta_perc_saque_atm_15_du,
	months_rise(vl_perc_saque_atm_m1_15_du,vl_perc_saque_atm_m2_15_du,vl_perc_saque_atm_m3_15_du,vl_perc_saque_atm_m4_15_du,vl_perc_saque_atm_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_perc_saque_atm_15_du,
	vl_perc_saque_atm_m1_15_du::numeric / case vl_max_perc_saque_atm_15_du when 0 then 1 else vl_max_perc_saque_atm_15_du end as r_curr_to_max_perc_saque_atm_15_du,
	vl_perc_saque_atm_m1_15_du::numeric / case vl_avg_perc_saque_atm_15_du when 0 then 1 else vl_avg_perc_saque_atm_15_du end as r_curr_to_avg_perc_saque_atm_15_du,
	vl_perc_saque_atm_m1_15_du::numeric / case vl_min_perc_saque_atm_15_du when 0 then 1 else vl_min_perc_saque_atm_15_du end as r_curr_to_min_perc_saque_atm_15_du,
	vl_perc_saque_atm_m1_15_du::numeric / case vl_ever_perc_saque_atm_15_du when 0 then 1 else vl_ever_perc_saque_atm_15_du end as r_curr_to_ever_perc_saque_atm_15_du,
	months_dive(vl_perc_contas_despesas_m1_15_du,vl_perc_contas_despesas_m2_15_du,vl_perc_contas_despesas_m3_15_du,vl_perc_contas_despesas_m4_15_du,vl_perc_contas_despesas_m5_15_du) as n_meses_queda_perc_contas_despesas_15_du,
	months_dive(vl_perc_contas_despesas_m1_15_du,vl_perc_contas_despesas_m2_15_du,vl_perc_contas_despesas_m3_15_du,vl_perc_contas_despesas_m4_15_du,vl_perc_contas_despesas_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_perc_contas_despesas_15_du,
	months_rise(vl_perc_contas_despesas_m1_15_du,vl_perc_contas_despesas_m2_15_du,vl_perc_contas_despesas_m3_15_du,vl_perc_contas_despesas_m4_15_du,vl_perc_contas_despesas_m5_15_du) as n_meses_alta_perc_contas_despesas_15_du,
	months_rise(vl_perc_contas_despesas_m1_15_du,vl_perc_contas_despesas_m2_15_du,vl_perc_contas_despesas_m3_15_du,vl_perc_contas_despesas_m4_15_du,vl_perc_contas_despesas_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_perc_contas_despesas_15_du,
	vl_perc_contas_despesas_m1_15_du::numeric / case vl_max_perc_contas_despesas_15_du when 0 then 1 else vl_max_perc_contas_despesas_15_du end as r_curr_to_max_perc_contas_despesas_15_du,
	vl_perc_contas_despesas_m1_15_du::numeric / case vl_avg_perc_contas_despesas_15_du when 0 then 1 else vl_avg_perc_contas_despesas_15_du end as r_curr_to_avg_perc_contas_despesas_15_du,
	vl_perc_contas_despesas_m1_15_du::numeric / case vl_min_perc_contas_despesas_15_du when 0 then 1 else vl_min_perc_contas_despesas_15_du end as r_curr_to_min_perc_contas_despesas_15_du,
	vl_perc_contas_despesas_m1_15_du::numeric / case vl_ever_perc_contas_despesas_15_du when 0 then 1 else vl_ever_perc_contas_despesas_15_du end as r_curr_to_ever_perc_contas_despesas_15_du,
	months_dive(vl_perc_pgto_pmt_credito_m1_15_du,vl_perc_pgto_pmt_credito_m2_15_du,vl_perc_pgto_pmt_credito_m3_15_du,vl_perc_pgto_pmt_credito_m4_15_du,vl_perc_pgto_pmt_credito_m5_15_du) as n_meses_queda_perc_pgto_pmt_credito_15_du,
	months_dive(vl_perc_pgto_pmt_credito_m1_15_du,vl_perc_pgto_pmt_credito_m2_15_du,vl_perc_pgto_pmt_credito_m3_15_du,vl_perc_pgto_pmt_credito_m4_15_du,vl_perc_pgto_pmt_credito_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_perc_pgto_pmt_credito_15_du,
	months_rise(vl_perc_pgto_pmt_credito_m1_15_du,vl_perc_pgto_pmt_credito_m2_15_du,vl_perc_pgto_pmt_credito_m3_15_du,vl_perc_pgto_pmt_credito_m4_15_du,vl_perc_pgto_pmt_credito_m5_15_du) as n_meses_alta_perc_pgto_pmt_credito_15_du,
	months_rise(vl_perc_pgto_pmt_credito_m1_15_du,vl_perc_pgto_pmt_credito_m2_15_du,vl_perc_pgto_pmt_credito_m3_15_du,vl_perc_pgto_pmt_credito_m4_15_du,vl_perc_pgto_pmt_credito_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_perc_pgto_pmt_credito_15_du,
	vl_perc_pgto_pmt_credito_m1_15_du::numeric / case vl_max_perc_pgto_pmt_credito_15_du when 0 then 1 else vl_max_perc_pgto_pmt_credito_15_du end as r_curr_to_max_perc_pgto_pmt_credito_15_du,
	vl_perc_pgto_pmt_credito_m1_15_du::numeric / case vl_avg_perc_pgto_pmt_credito_15_du when 0 then 1 else vl_avg_perc_pgto_pmt_credito_15_du end as r_curr_to_avg_perc_pgto_pmt_credito_15_du,
	vl_perc_pgto_pmt_credito_m1_15_du::numeric / case vl_min_perc_pgto_pmt_credito_15_du when 0 then 1 else vl_min_perc_pgto_pmt_credito_15_du end as r_curr_to_min_perc_pgto_pmt_credito_15_du,
	vl_perc_pgto_pmt_credito_m1_15_du::numeric / case vl_ever_perc_pgto_pmt_credito_15_du when 0 then 1 else vl_ever_perc_pgto_pmt_credito_15_du end as r_curr_to_ever_perc_pgto_pmt_credito_15_du,
	months_dive(vl_perc_pgto_juros_mora_m1_15_du,vl_perc_pgto_juros_mora_m2_15_du,vl_perc_pgto_juros_mora_m3_15_du,vl_perc_pgto_juros_mora_m4_15_du,vl_perc_pgto_juros_mora_m5_15_du) as n_meses_queda_perc_pgto_juros_mora_15_du,
	months_dive(vl_perc_pgto_juros_mora_m1_15_du,vl_perc_pgto_juros_mora_m2_15_du,vl_perc_pgto_juros_mora_m3_15_du,vl_perc_pgto_juros_mora_m4_15_du,vl_perc_pgto_juros_mora_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_perc_pgto_juros_mora_15_du,
	months_rise(vl_perc_pgto_juros_mora_m1_15_du,vl_perc_pgto_juros_mora_m2_15_du,vl_perc_pgto_juros_mora_m3_15_du,vl_perc_pgto_juros_mora_m4_15_du,vl_perc_pgto_juros_mora_m5_15_du) as n_meses_alta_perc_pgto_juros_mora_15_du,
	months_rise(vl_perc_pgto_juros_mora_m1_15_du,vl_perc_pgto_juros_mora_m2_15_du,vl_perc_pgto_juros_mora_m3_15_du,vl_perc_pgto_juros_mora_m4_15_du,vl_perc_pgto_juros_mora_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_perc_pgto_juros_mora_15_du,
	vl_perc_pgto_juros_mora_m1_15_du::numeric / case vl_max_perc_pgto_juros_mora_15_du when 0 then 1 else vl_max_perc_pgto_juros_mora_15_du end as r_curr_to_max_perc_pgto_juros_mora_15_du,
	vl_perc_pgto_juros_mora_m1_15_du::numeric / case vl_avg_perc_pgto_juros_mora_15_du when 0 then 1 else vl_avg_perc_pgto_juros_mora_15_du end as r_curr_to_avg_perc_pgto_juros_mora_15_du,
	vl_perc_pgto_juros_mora_m1_15_du::numeric / case vl_min_perc_pgto_juros_mora_15_du when 0 then 1 else vl_min_perc_pgto_juros_mora_15_du end as r_curr_to_min_perc_pgto_juros_mora_15_du,
	vl_perc_pgto_juros_mora_m1_15_du::numeric / case vl_ever_perc_pgto_juros_mora_15_du when 0 then 1 else vl_ever_perc_pgto_juros_mora_15_du end as r_curr_to_ever_perc_pgto_juros_mora_15_du,
	months_dive(vl_maior_saida_m1_15_du,vl_maior_saida_m2_15_du,vl_maior_saida_m3_15_du,vl_maior_saida_m4_15_du,vl_maior_saida_m5_15_du) as n_meses_queda_maior_saida_15_du,
	months_dive(vl_maior_saida_m1_15_du,vl_maior_saida_m2_15_du,vl_maior_saida_m3_15_du,vl_maior_saida_m4_15_du,vl_maior_saida_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_maior_saida_15_du,
	months_rise(vl_maior_saida_m1_15_du,vl_maior_saida_m2_15_du,vl_maior_saida_m3_15_du,vl_maior_saida_m4_15_du,vl_maior_saida_m5_15_du) as n_meses_alta_maior_saida_15_du,
	months_rise(vl_maior_saida_m1_15_du,vl_maior_saida_m2_15_du,vl_maior_saida_m3_15_du,vl_maior_saida_m4_15_du,vl_maior_saida_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_maior_saida_15_du,
	vl_maior_saida_m1_15_du::numeric / greatest(vl_max_maior_saida_15_du,1) as r_curr_to_max_maior_saida_15_du,
	vl_maior_saida_m1_15_du::numeric / greatest(vl_avg_maior_saida_15_du,1) as r_curr_to_avg_maior_saida_15_du,
	vl_maior_saida_m1_15_du::numeric / greatest(vl_min_maior_saida_15_du,1) as r_curr_to_min_maior_saida_15_du,
	vl_maior_saida_m1_15_du::numeric / greatest(vl_ever_maior_saida_15_du,1) as r_curr_to_ever_maior_saida_15_du,
	months_dive(vl_total_saida_m1_15_du,vl_total_saida_m2_15_du,vl_total_saida_m3_15_du,vl_total_saida_m4_15_du,vl_total_saida_m5_15_du) as n_meses_queda_total_saida_15_du,
	months_dive(vl_total_saida_m1_15_du,vl_total_saida_m2_15_du,vl_total_saida_m3_15_du,vl_total_saida_m4_15_du,vl_total_saida_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_total_saida_15_du,
	months_rise(vl_total_saida_m1_15_du,vl_total_saida_m2_15_du,vl_total_saida_m3_15_du,vl_total_saida_m4_15_du,vl_total_saida_m5_15_du) as n_meses_alta_total_saida_15_du,
	months_rise(vl_total_saida_m1_15_du,vl_total_saida_m2_15_du,vl_total_saida_m3_15_du,vl_total_saida_m4_15_du,vl_total_saida_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_total_saida_15_du,
	vl_total_saida_m1_15_du::numeric / greatest(vl_max_total_saida_15_du,1) as r_curr_to_max_total_saida_15_du,
	vl_total_saida_m1_15_du::numeric / greatest(vl_avg_total_saida_15_du,1) as r_curr_to_avg_total_saida_15_du,
	vl_total_saida_m1_15_du::numeric / greatest(vl_min_total_saida_15_du,1) as r_curr_to_min_total_saida_15_du,
	vl_total_saida_m1_15_du::numeric / greatest(vl_ever_total_saida_15_du,1) as r_curr_to_ever_total_saida_15_du,
	vl_max_total_saida_15_du::numeric / greatest(dp.month_revenue,1) as r_max_total_saida_to_faturamento_admin_15_du,
	vl_avg_total_saida_15_du::numeric / greatest(dp.month_revenue,1) as r_avg_total_saida_to_faturamento_admin_15_du,
	vl_min_total_saida_15_du::numeric / greatest(dp.month_revenue,1) as r_min_total_saida_to_faturamento_admin_15_du,
	vl_ever_total_saida_15_du::numeric / greatest(dp.month_revenue,1) as r_ever_total_saida_to_faturamento_admin_15_du,
	months_dive(vl_fluxo_caixa_pior_m1_15_du,vl_fluxo_caixa_pior_m2_15_du,vl_fluxo_caixa_pior_m3_15_du,vl_fluxo_caixa_pior_m4_15_du,vl_fluxo_caixa_pior_m5_15_du) as n_meses_queda_fluxo_caixa_pior_15_du,
	months_dive(vl_fluxo_caixa_pior_m1_15_du,vl_fluxo_caixa_pior_m2_15_du,vl_fluxo_caixa_pior_m3_15_du,vl_fluxo_caixa_pior_m4_15_du,vl_fluxo_caixa_pior_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_fluxo_caixa_pior_15_du,
	months_rise(vl_fluxo_caixa_pior_m1_15_du,vl_fluxo_caixa_pior_m2_15_du,vl_fluxo_caixa_pior_m3_15_du,vl_fluxo_caixa_pior_m4_15_du,vl_fluxo_caixa_pior_m5_15_du) as n_meses_alta_fluxo_caixa_pior_15_du,
	months_rise(vl_fluxo_caixa_pior_m1_15_du,vl_fluxo_caixa_pior_m2_15_du,vl_fluxo_caixa_pior_m3_15_du,vl_fluxo_caixa_pior_m4_15_du,vl_fluxo_caixa_pior_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_fluxo_caixa_pior_15_du,
	vl_fluxo_caixa_pior_m1_15_du::numeric / case vl_max_fluxo_caixa_pior_15_du when 0 then 1 else vl_max_fluxo_caixa_pior_15_du end as r_curr_to_max_fluxo_caixa_pior_15_du,
	vl_fluxo_caixa_pior_m1_15_du::numeric / case vl_avg_fluxo_caixa_pior_15_du when 0 then 1 else vl_avg_fluxo_caixa_pior_15_du end as r_curr_to_avg_fluxo_caixa_pior_15_du,
	vl_fluxo_caixa_pior_m1_15_du::numeric / case vl_min_fluxo_caixa_pior_15_du when 0 then 1 else vl_min_fluxo_caixa_pior_15_du end as r_curr_to_min_fluxo_caixa_pior_15_du,
	vl_fluxo_caixa_pior_m1_15_du::numeric / case vl_ever_fluxo_caixa_pior_15_du when 0 then 1 else vl_ever_fluxo_caixa_pior_15_du end as r_curr_to_ever_fluxo_caixa_pior_15_du,
	vl_max_fluxo_caixa_pior_15_du::numeric / greatest(dp.month_revenue,1) as r_max_fluxo_caixa_pior_to_faturamento_admin_15_du,
	vl_avg_fluxo_caixa_pior_15_du::numeric / greatest(dp.month_revenue,1) as r_avg_fluxo_caixa_pior_to_faturamento_admin_15_du,
	vl_min_fluxo_caixa_pior_15_du::numeric / greatest(dp.month_revenue,1) as r_min_fluxo_caixa_pior_to_faturamento_admin_15_du,
	vl_ever_fluxo_caixa_pior_15_du::numeric / greatest(dp.month_revenue,1) as r_ever_fluxo_caixa_pior_to_faturamento_admin_15_du,
	months_dive(vl_fluxo_caixa_melhor_m1_15_du,vl_fluxo_caixa_melhor_m2_15_du,vl_fluxo_caixa_melhor_m3_15_du,vl_fluxo_caixa_melhor_m4_15_du,vl_fluxo_caixa_melhor_m5_15_du) as n_meses_queda_fluxo_caixa_melhor_15_du,
	months_dive(vl_fluxo_caixa_melhor_m1_15_du,vl_fluxo_caixa_melhor_m2_15_du,vl_fluxo_caixa_melhor_m3_15_du,vl_fluxo_caixa_melhor_m4_15_du,vl_fluxo_caixa_melhor_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_fluxo_caixa_melhor_15_du,
	months_rise(vl_fluxo_caixa_melhor_m1_15_du,vl_fluxo_caixa_melhor_m2_15_du,vl_fluxo_caixa_melhor_m3_15_du,vl_fluxo_caixa_melhor_m4_15_du,vl_fluxo_caixa_melhor_m5_15_du) as n_meses_alta_fluxo_caixa_melhor_15_du,
	months_rise(vl_fluxo_caixa_melhor_m1_15_du,vl_fluxo_caixa_melhor_m2_15_du,vl_fluxo_caixa_melhor_m3_15_du,vl_fluxo_caixa_melhor_m4_15_du,vl_fluxo_caixa_melhor_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_fluxo_caixa_melhor_15_du,
	vl_fluxo_caixa_melhor_m1_15_du::numeric / case vl_max_fluxo_caixa_melhor_15_du when 0 then 1 else vl_max_fluxo_caixa_melhor_15_du end as r_curr_to_max_fluxo_caixa_melhor_15_du,
	vl_fluxo_caixa_melhor_m1_15_du::numeric / case vl_avg_fluxo_caixa_melhor_15_du when 0 then 1 else vl_avg_fluxo_caixa_melhor_15_du end as r_curr_to_avg_fluxo_caixa_melhor_15_du,
	vl_fluxo_caixa_melhor_m1_15_du::numeric / case vl_min_fluxo_caixa_melhor_15_du when 0 then 1 else vl_min_fluxo_caixa_melhor_15_du end as r_curr_to_min_fluxo_caixa_melhor_15_du,
	vl_fluxo_caixa_melhor_m1_15_du::numeric / case vl_ever_fluxo_caixa_melhor_15_du when 0 then 1 else vl_ever_fluxo_caixa_melhor_15_du end as r_curr_to_ever_fluxo_caixa_melhor_15_du,
	vl_max_fluxo_caixa_melhor_15_du::numeric / greatest(dp.month_revenue,1) as r_max_fluxo_caixa_melhor_to_faturamento_admin_15_du,
	vl_avg_fluxo_caixa_melhor_15_du::numeric / greatest(dp.month_revenue,1) as r_avg_fluxo_caixa_melhor_to_faturamento_admin_15_du,
	vl_min_fluxo_caixa_melhor_15_du::numeric / greatest(dp.month_revenue,1) as r_min_fluxo_caixa_melhor_to_faturamento_admin_15_du,
	vl_ever_fluxo_caixa_melhor_15_du::numeric / greatest(dp.month_revenue,1) as r_ever_fluxo_caixa_melhor_to_faturamento_admin_15_du,
	months_dive(vl_fluxo_caixa_dias_negativo_m1_15_du,vl_fluxo_caixa_dias_negativo_m2_15_du,vl_fluxo_caixa_dias_negativo_m3_15_du,vl_fluxo_caixa_dias_negativo_m4_15_du,vl_fluxo_caixa_dias_negativo_m5_15_du) as n_meses_queda_fluxo_caixa_dias_negativo_15_du,
	months_dive(vl_fluxo_caixa_dias_negativo_m1_15_du,vl_fluxo_caixa_dias_negativo_m2_15_du,vl_fluxo_caixa_dias_negativo_m3_15_du,vl_fluxo_caixa_dias_negativo_m4_15_du,vl_fluxo_caixa_dias_negativo_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_fluxo_caixa_dias_negativo_15_du,
	months_rise(vl_fluxo_caixa_dias_negativo_m1_15_du,vl_fluxo_caixa_dias_negativo_m2_15_du,vl_fluxo_caixa_dias_negativo_m3_15_du,vl_fluxo_caixa_dias_negativo_m4_15_du,vl_fluxo_caixa_dias_negativo_m5_15_du) as n_meses_alta_fluxo_caixa_dias_negativo_15_du,
	months_rise(vl_fluxo_caixa_dias_negativo_m1_15_du,vl_fluxo_caixa_dias_negativo_m2_15_du,vl_fluxo_caixa_dias_negativo_m3_15_du,vl_fluxo_caixa_dias_negativo_m4_15_du,vl_fluxo_caixa_dias_negativo_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_fluxo_caixa_dias_negativo_15_du,
	vl_fluxo_caixa_dias_negativo_m1_15_du::numeric / greatest(vl_max_fluxo_caixa_dias_negativo_15_du,1) as r_curr_to_max_fluxo_caixa_dias_negativo_15_du,
	vl_fluxo_caixa_dias_negativo_m1_15_du::numeric / greatest(vl_avg_fluxo_caixa_dias_negativo_15_du,1) as r_curr_to_avg_fluxo_caixa_dias_negativo_15_du,
	vl_fluxo_caixa_dias_negativo_m1_15_du::numeric / greatest(vl_min_fluxo_caixa_dias_negativo_15_du,1) as r_curr_to_min_fluxo_caixa_dias_negativo_15_du,
	vl_fluxo_caixa_dias_negativo_m1_15_du::numeric / greatest(vl_ever_fluxo_caixa_dias_negativo_15_du,1) as r_curr_to_ever_fluxo_caixa_dias_negativo_15_du,
	months_dive(vl_fluxo_caixa_perc_dias_negativo_m1_15_du,vl_fluxo_caixa_perc_dias_negativo_m2_15_du,vl_fluxo_caixa_perc_dias_negativo_m3_15_du,vl_fluxo_caixa_perc_dias_negativo_m4_15_du,vl_fluxo_caixa_perc_dias_negativo_m5_15_du) as n_meses_queda_fluxo_caixa_perc_dias_negativo_15_du,
	months_dive(vl_fluxo_caixa_perc_dias_negativo_m1_15_du,vl_fluxo_caixa_perc_dias_negativo_m2_15_du,vl_fluxo_caixa_perc_dias_negativo_m3_15_du,vl_fluxo_caixa_perc_dias_negativo_m4_15_du,vl_fluxo_caixa_perc_dias_negativo_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_fluxo_caixa_perc_dias_negativo_15_du,
	months_rise(vl_fluxo_caixa_perc_dias_negativo_m1_15_du,vl_fluxo_caixa_perc_dias_negativo_m2_15_du,vl_fluxo_caixa_perc_dias_negativo_m3_15_du,vl_fluxo_caixa_perc_dias_negativo_m4_15_du,vl_fluxo_caixa_perc_dias_negativo_m5_15_du) as n_meses_alta_fluxo_caixa_perc_dias_negativo_15_du,
	months_rise(vl_fluxo_caixa_perc_dias_negativo_m1_15_du,vl_fluxo_caixa_perc_dias_negativo_m2_15_du,vl_fluxo_caixa_perc_dias_negativo_m3_15_du,vl_fluxo_caixa_perc_dias_negativo_m4_15_du,vl_fluxo_caixa_perc_dias_negativo_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_fluxo_caixa_perc_dias_negativo_15_du,
	vl_fluxo_caixa_perc_dias_negativo_m1_15_du::numeric / greatest(vl_max_fluxo_caixa_perc_dias_negativo_15_du,1) as r_curr_to_max_fluxo_caixa_perc_dias_negativo_15_du,
	vl_fluxo_caixa_perc_dias_negativo_m1_15_du::numeric / greatest(vl_avg_fluxo_caixa_perc_dias_negativo_15_du,1) as r_curr_to_avg_fluxo_caixa_perc_dias_negativo_15_du,
	vl_fluxo_caixa_perc_dias_negativo_m1_15_du::numeric / greatest(vl_min_fluxo_caixa_perc_dias_negativo_15_du,1) as r_curr_to_min_fluxo_caixa_perc_dias_negativo_15_du,
	vl_fluxo_caixa_perc_dias_negativo_m1_15_du::numeric / greatest(vl_ever_fluxo_caixa_perc_dias_negativo_15_du,1) as r_curr_to_ever_fluxo_caixa_perc_dias_negativo_15_du,
	months_dive(vl_fluxo_caixa_saldo_operacional_m1_15_du,vl_fluxo_caixa_saldo_operacional_m2_15_du,vl_fluxo_caixa_saldo_operacional_m3_15_du,vl_fluxo_caixa_saldo_operacional_m4_15_du,vl_fluxo_caixa_saldo_operacional_m5_15_du) as n_meses_queda_fluxo_caixa_saldo_operacional_15_du,
	months_dive(vl_fluxo_caixa_saldo_operacional_m1_15_du,vl_fluxo_caixa_saldo_operacional_m2_15_du,vl_fluxo_caixa_saldo_operacional_m3_15_du,vl_fluxo_caixa_saldo_operacional_m4_15_du,vl_fluxo_caixa_saldo_operacional_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_fluxo_caixa_saldo_operacional_15_du,
	months_rise(vl_fluxo_caixa_saldo_operacional_m1_15_du,vl_fluxo_caixa_saldo_operacional_m2_15_du,vl_fluxo_caixa_saldo_operacional_m3_15_du,vl_fluxo_caixa_saldo_operacional_m4_15_du,vl_fluxo_caixa_saldo_operacional_m5_15_du) as n_meses_alta_fluxo_caixa_saldo_operacional_15_du,
	months_rise(vl_fluxo_caixa_saldo_operacional_m1_15_du,vl_fluxo_caixa_saldo_operacional_m2_15_du,vl_fluxo_caixa_saldo_operacional_m3_15_du,vl_fluxo_caixa_saldo_operacional_m4_15_du,vl_fluxo_caixa_saldo_operacional_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_fluxo_caixa_saldo_operacional_15_du,
	vl_fluxo_caixa_saldo_operacional_m1_15_du::numeric / case vl_max_fluxo_caixa_saldo_operacional_15_du when 0 then 1 else vl_max_fluxo_caixa_saldo_operacional_15_du end as r_curr_to_max_fluxo_caixa_saldo_operacional_15_du,
	vl_fluxo_caixa_saldo_operacional_m1_15_du::numeric / case vl_avg_fluxo_caixa_saldo_operacional_15_du when 0 then 1 else vl_avg_fluxo_caixa_saldo_operacional_15_du end as r_curr_to_avg_fluxo_caixa_saldo_operacional_15_du,
	vl_fluxo_caixa_saldo_operacional_m1_15_du::numeric / case vl_min_fluxo_caixa_saldo_operacional_15_du when 0 then 1 else vl_min_fluxo_caixa_saldo_operacional_15_du end as r_curr_to_min_fluxo_caixa_saldo_operacional_15_du,
	vl_fluxo_caixa_saldo_operacional_m1_15_du::numeric / case vl_ever_fluxo_caixa_saldo_operacional_15_du when 0 then 1 else vl_ever_fluxo_caixa_saldo_operacional_15_du end as r_curr_to_ever_fluxo_caixa_saldo_operacional_15_du,
	vl_max_fluxo_caixa_saldo_operacional_15_du::numeric / greatest(dp.month_revenue,1) as r_max_fluxo_caixa_saldo_operacional_to_faturamento_admin_15_du,
	vl_avg_fluxo_caixa_saldo_operacional_15_du::numeric / greatest(dp.month_revenue,1) as r_avg_fluxo_caixa_saldo_operacional_to_faturamento_admin_15_du,
	vl_min_fluxo_caixa_saldo_operacional_15_du::numeric / greatest(dp.month_revenue,1) as r_min_fluxo_caixa_saldo_operacional_to_faturamento_admin_15_du,
	vl_ever_fluxo_caixa_saldo_operacional_15_du::numeric / greatest(dp.month_revenue,1) as r_ever_fluxo_caixa_saldo_operacional_to_faturamento_admin_15_du,
	months_dive(vl_fluxo_caixa_saldo_ate_dia_lead_m1_15_du,vl_fluxo_caixa_saldo_ate_dia_lead_m2_15_du,vl_fluxo_caixa_saldo_ate_dia_lead_m3_15_du,vl_fluxo_caixa_saldo_ate_dia_lead_m4_15_du,vl_fluxo_caixa_saldo_ate_dia_lead_m5_15_du) as n_meses_queda_fluxo_caixa_saldo_ate_dia_lead_15_du,
	months_dive(vl_fluxo_caixa_saldo_ate_dia_lead_m1_15_du,vl_fluxo_caixa_saldo_ate_dia_lead_m2_15_du,vl_fluxo_caixa_saldo_ate_dia_lead_m3_15_du,vl_fluxo_caixa_saldo_ate_dia_lead_m4_15_du,vl_fluxo_caixa_saldo_ate_dia_lead_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_queda_fluxo_caixa_saldo_ate_dia_lead_15_du,
	months_rise(vl_fluxo_caixa_saldo_ate_dia_lead_m1_15_du,vl_fluxo_caixa_saldo_ate_dia_lead_m2_15_du,vl_fluxo_caixa_saldo_ate_dia_lead_m3_15_du,vl_fluxo_caixa_saldo_ate_dia_lead_m4_15_du,vl_fluxo_caixa_saldo_ate_dia_lead_m5_15_du) as n_meses_alta_fluxo_caixa_saldo_ate_dia_lead_15_du,
	months_rise(vl_fluxo_caixa_saldo_ate_dia_lead_m1_15_du,vl_fluxo_caixa_saldo_ate_dia_lead_m2_15_du,vl_fluxo_caixa_saldo_ate_dia_lead_m3_15_du,vl_fluxo_caixa_saldo_ate_dia_lead_m4_15_du,vl_fluxo_caixa_saldo_ate_dia_lead_m5_15_du) / greatest(n_meses_15_du - 1,1) as perc_meses_alta_fluxo_caixa_saldo_ate_dia_lead_15_du,
	vl_fluxo_caixa_saldo_ate_dia_lead_m1_15_du::numeric / case vl_max_fluxo_caixa_saldo_ate_dia_lead_15_du when 0 then 1 else vl_max_fluxo_caixa_saldo_ate_dia_lead_15_du end as r_curr_to_max_fluxo_caixa_saldo_ate_dia_lead_15_du,
	vl_fluxo_caixa_saldo_ate_dia_lead_m1_15_du::numeric / case vl_avg_fluxo_caixa_saldo_ate_dia_lead_15_du when 0 then 1 else vl_avg_fluxo_caixa_saldo_ate_dia_lead_15_du end as r_curr_to_avg_fluxo_caixa_saldo_ate_dia_lead_15_du,
	vl_fluxo_caixa_saldo_ate_dia_lead_m1_15_du::numeric / case vl_min_fluxo_caixa_saldo_ate_dia_lead_15_du when 0 then 1 else vl_min_fluxo_caixa_saldo_ate_dia_lead_15_du end as r_curr_to_min_fluxo_caixa_saldo_ate_dia_lead_15_du,
	vl_fluxo_caixa_saldo_ate_dia_lead_m1_15_du::numeric / case vl_ever_fluxo_caixa_saldo_ate_dia_lead_15_du when 0 then 1 else vl_ever_fluxo_caixa_saldo_ate_dia_lead_15_du end as r_curr_to_ever_fluxo_caixa_saldo_ate_dia_lead_15_du,
	vl_max_fluxo_caixa_saldo_ate_dia_lead_15_du::numeric / greatest(dp.month_revenue,1) as r_max_fluxo_caixa_saldo_ate_dia_lead_to_faturamento_admin_15_du,
	vl_avg_fluxo_caixa_saldo_ate_dia_lead_15_du::numeric / greatest(dp.month_revenue,1) as r_avg_fluxo_caixa_saldo_ate_dia_lead_to_faturamento_admin_15_du,
	vl_min_fluxo_caixa_saldo_ate_dia_lead_15_du::numeric / greatest(dp.month_revenue,1) as r_min_fluxo_caixa_saldo_ate_dia_lead_to_faturamento_admin_15_du,
	vl_ever_fluxo_caixa_saldo_ate_dia_lead_15_du::numeric / greatest(dp.month_revenue,1) as r_ever_fluxo_caixa_saldo_ate_dia_lead_to_faturamento_admin_15_du
from t_extratos t
join public.loan_requests lr on lr.loan_request_id = t.loan_request_id
join public.offers o on o.offer_id = lr.offer_id
join public.direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
join(select 
		id as extrato_id,
	--PERC FATURAMENTO COMPROVADO - GERAL
		(coalesce((perc_faturamento_comprovado_mes1 > 1)::int,0) + coalesce((perc_faturamento_comprovado_mes2 > 1)::int,0) + coalesce((perc_faturamento_comprovado_mes3 > 1)::int,0) + coalesce((perc_faturamento_comprovado_mes4 > 1)::int,0) + coalesce((perc_faturamento_comprovado_mes5 > 1)::int,0)) as n_meses_faturamento_comprovado_acima_informado,
		(coalesce((perc_faturamento_comprovado_mes1 < 1)::int,0) + coalesce((perc_faturamento_comprovado_mes2 < 1)::int,0) + coalesce((perc_faturamento_comprovado_mes3 < 1)::int,0) + coalesce((perc_faturamento_comprovado_mes4 < 1)::int,0) + coalesce((perc_faturamento_comprovado_mes5 < 1)::int,0)) as n_meses_faturamento_comprovado_abaixo_informado,
	--PERC FATURAMENTO COMPROVADO - 15 DU
		(coalesce((perc_faturamento_comprovado_mes1 > 1)::int * (quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((perc_faturamento_comprovado_mes2 > 1)::int * (quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((perc_faturamento_comprovado_mes3 > 1)::int * (quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((perc_faturamento_comprovado_mes4 > 1)::int * (quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((perc_faturamento_comprovado_mes5 > 1)::int * (quantidade_dias_uteis_mes5 >= 15)::int,0)) as n_meses_faturamento_comprovado_acima_informado_15_du,
		(coalesce((perc_faturamento_comprovado_mes1 < 1)::int * (quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((perc_faturamento_comprovado_mes2 < 1)::int * (quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((perc_faturamento_comprovado_mes3 < 1)::int * (quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((perc_faturamento_comprovado_mes4 < 1)::int * (quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((perc_faturamento_comprovado_mes5 < 1)::int * (quantidade_dias_uteis_mes5 >= 15)::int,0)) as n_meses_faturamento_comprovado_abaixo_informado_15_du,
	--FATURAMENTO COMPROVADO - GERAL
		coalesce(faturamento_comprovado_mes1,0) + coalesce(faturamento_comprovado_mes2,0) + coalesce(faturamento_comprovado_mes3,0) + coalesce(faturamento_comprovado_mes4,0) + coalesce(faturamento_comprovado_mes5,0) as vl_ever_faturamento_comprovado,
		greatest(faturamento_comprovado_mes1 , faturamento_comprovado_mes2 , faturamento_comprovado_mes3 , faturamento_comprovado_mes4 , faturamento_comprovado_mes5) as vl_max_faturamento_comprovado,
	--FATURAMENTO COMPROVADO - 10 DU
		(coalesce((quantidade_dias_uteis_mes1 >= 10)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 10)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 10)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 10)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 10)::int,0)) as n_meses_10_du,
		case 
			when quantidade_dias_uteis_mes1 >= 10 then faturamento_comprovado_mes1
			when quantidade_dias_uteis_mes2 >= 10 then faturamento_comprovado_mes2
			when quantidade_dias_uteis_mes3 >= 10 then faturamento_comprovado_mes3
			when quantidade_dias_uteis_mes4 >= 10 then faturamento_comprovado_mes4
			when quantidade_dias_uteis_mes5 >= 10 then faturamento_comprovado_mes5
		end as vl_faturamento_comprovado_m1_10_du,
		case 
			when quantidade_dias_uteis_mes1 >= 10 then faturamento_comprovado_mes2 * case when quantidade_dias_uteis_mes2 >= 10 then 1 end
			when quantidade_dias_uteis_mes2 >= 10 then faturamento_comprovado_mes3 * case when quantidade_dias_uteis_mes3 >= 10 then 1 end
			when quantidade_dias_uteis_mes3 >= 10 then faturamento_comprovado_mes4 * case when quantidade_dias_uteis_mes4 >= 10 then 1 end
			when quantidade_dias_uteis_mes4 >= 10 then faturamento_comprovado_mes5 * case when quantidade_dias_uteis_mes5 >= 10 then 1 end
		end as vl_faturamento_comprovado_m2_10_du,
		case 
			when quantidade_dias_uteis_mes1 >= 10 then faturamento_comprovado_mes3 * case when quantidade_dias_uteis_mes3 >= 10 then 1 end
			when quantidade_dias_uteis_mes2 >= 10 then faturamento_comprovado_mes4 * case when quantidade_dias_uteis_mes4 >= 10 then 1 end
			when quantidade_dias_uteis_mes3 >= 10 then faturamento_comprovado_mes5 * case when quantidade_dias_uteis_mes5 >= 10 then 1 end
		end as vl_faturamento_comprovado_m3_10_du,
		case 
			when quantidade_dias_uteis_mes1 >= 10 then faturamento_comprovado_mes4 * case when quantidade_dias_uteis_mes4 >= 10 then 1 end
			when quantidade_dias_uteis_mes2 >= 10 then faturamento_comprovado_mes5 * case when quantidade_dias_uteis_mes5 >= 10 then 1 end
		end as vl_faturamento_comprovado_m4_10_du,
		case 
			when quantidade_dias_uteis_mes1 >= 10 then faturamento_comprovado_mes5 * case when quantidade_dias_uteis_mes5 >= 10 then 1 end
		end as vl_faturamento_comprovado_m5_10_du,
		coalesce((quantidade_dias_uteis_mes1 >= 10)::int * faturamento_comprovado_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 10)::int * faturamento_comprovado_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 10)::int * faturamento_comprovado_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 10)::int * faturamento_comprovado_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 10)::int * faturamento_comprovado_mes5,0) as vl_ever_faturamento_comprovado_10_du,
		greatest((quantidade_dias_uteis_mes1 >= 10)::int * faturamento_comprovado_mes1 ,
			(quantidade_dias_uteis_mes2 >= 10)::int * faturamento_comprovado_mes2 ,
			(quantidade_dias_uteis_mes3 >= 10)::int * faturamento_comprovado_mes3 ,
			(quantidade_dias_uteis_mes4 >= 10)::int * faturamento_comprovado_mes4 ,
			(quantidade_dias_uteis_mes5 >= 10)::int * faturamento_comprovado_mes5) as vl_max_faturamento_comprovado_10_du,
		least((quantidade_dias_uteis_mes1 >= 10)::int * faturamento_comprovado_mes1 ,
			(quantidade_dias_uteis_mes2 >= 10)::int * faturamento_comprovado_mes2 ,
			(quantidade_dias_uteis_mes3 >= 10)::int * faturamento_comprovado_mes3 ,
			(quantidade_dias_uteis_mes4 >= 10)::int * faturamento_comprovado_mes4 ,
			(quantidade_dias_uteis_mes5 >= 10)::int * faturamento_comprovado_mes5) as vl_min_faturamento_comprovado_10_du,
		coalesce((quantidade_dias_uteis_mes1 >= 10)::int * faturamento_comprovado_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 10)::int * faturamento_comprovado_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 10)::int * faturamento_comprovado_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 10)::int * faturamento_comprovado_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 10)::int * faturamento_comprovado_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 10)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 10)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 10)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 10)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 10)::int,0)),1) as vl_avg_faturamento_comprovado_10_du,
	--FATURAMENTO COMPROVADO - 15 DU
		(coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)) as n_meses_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then faturamento_comprovado_mes1
			when quantidade_dias_uteis_mes2 >= 15 then faturamento_comprovado_mes2
			when quantidade_dias_uteis_mes3 >= 15 then faturamento_comprovado_mes3
			when quantidade_dias_uteis_mes4 >= 15 then faturamento_comprovado_mes4
			when quantidade_dias_uteis_mes5 >= 15 then faturamento_comprovado_mes5
		end as vl_faturamento_comprovado_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then faturamento_comprovado_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then faturamento_comprovado_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then faturamento_comprovado_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then faturamento_comprovado_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_faturamento_comprovado_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then faturamento_comprovado_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then faturamento_comprovado_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then faturamento_comprovado_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_faturamento_comprovado_m3_15_du,
		case 
			when quantida  >= 15 then faturamento_comprovado_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then faturamento_comprovado_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_faturamento_comprovado_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then faturamento_comprovado_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_faturamento_comprovado_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * faturamento_comprovado_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * faturamento_comprovado_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * faturamento_comprovado_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * faturamento_comprovado_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * faturamento_comprovado_mes5,0) as vl_ever_faturamento_comprovado_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * faturamento_comprovado_mes1,
			(quantidade_dias_uteis_mes2 >= 15)::int * faturamento_comprovado_mes2,
			(quantidade_dias_uteis_mes3 >= 15)::int * faturamento_comprovado_mes3,
			(quantidade_dias_uteis_mes4 >= 15)::int * faturamento_comprovado_mes4,
			(quantidade_dias_uteis_mes5 >= 15)::int * faturamento_comprovado_mes5) as vl_max_faturamento_comprovado_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * faturamento_comprovado_mes1,
			(quantidade_dias_uteis_mes2 >= 15)::int * faturamento_comprovado_mes2,
			(quantidade_dias_uteis_mes3 >= 15)::int * faturamento_comprovado_mes3,
			(quantidade_dias_uteis_mes4 >= 15)::int * faturamento_comprovado_mes4,
			(quantidade_dias_uteis_mes5 >= 15)::int * faturamento_comprovado_mes5) as vl_min_faturamento_comprovado_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * faturamento_comprovado_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * faturamento_comprovado_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * faturamento_comprovado_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * faturamento_comprovado_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * faturamento_comprovado_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_faturamento_comprovado_15_du,
	--FATURAMENTO COMPROVADO - 20 DU
		(coalesce((quantidade_dias_uteis_mes1 >= 20)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 20)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 20)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 20)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 20)::int,0)) as n_meses_20_du,
		case 
			when quantidade_dias_uteis_mes1 >= 20 then faturamento_comprovado_mes1
			when quantidade_dias_uteis_mes2 >= 20 then faturamento_comprovado_mes2
			when quantidade_dias_uteis_mes3 >= 20 then faturamento_comprovado_mes3
			when quantidade_dias_uteis_mes4 >= 20 then faturamento_comprovado_mes4
			when quantidade_dias_uteis_mes5 >= 20 then faturamento_comprovado_mes5
		end as vl_faturamento_comprovado_m1_20_du,
		case 
			when quantidade_dias_uteis_mes1 >= 20 then faturamento_comprovado_mes2 * case when quantidade_dias_uteis_mes2 >= 20 then 1 end
			when quantidade_dias_uteis_mes2 >= 20 then faturamento_comprovado_mes3 * case when quantidade_dias_uteis_mes3 >= 20 then 1 end
			when quantidade_dias_uteis_mes3 >= 20 then faturamento_comprovado_mes4 * case when quantidade_dias_uteis_mes4 >= 20 then 1 end
			when quantidade_dias_uteis_mes4 >= 20 then faturamento_comprovado_mes5 * case when quantidade_dias_uteis_mes5 >= 20 then 1 end
		end as vl_faturamento_comprovado_m2_20_du,
		case 
			when quantidade_dias_uteis_mes1 >= 20 then faturamento_comprovado_mes3 * case when quantidade_dias_uteis_mes3 >= 20 then 1 end
			when quantidade_dias_uteis_mes2 >= 20 then faturamento_comprovado_mes4 * case when quantidade_dias_uteis_mes4 >= 20 then 1 end
			when quantidade_dias_uteis_mes3 >= 20 then faturamento_comprovado_mes5 * case when quantidade_dias_uteis_mes5 >= 20 then 1 end
		end as vl_faturamento_comprovado_m3_20_du,
		case 
			when quantidade_dias_uteis_mes1 >= 20 then faturamento_comprovado_mes4 * case when quantidade_dias_uteis_mes4 >= 20 then 1 end
			when quantidade_dias_uteis_mes2 >= 20 then faturamento_comprovado_mes5 * case when quantidade_dias_uteis_mes5 >= 20 then 1 end
		end as vl_faturamento_comprovado_m4_20_du,
		case 
			when quantidade_dias_uteis_mes1 >= 20 then faturamento_comprovado_mes5 * case when quantidade_dias_uteis_mes5 >= 20 then 1 end
		end as vl_faturamento_comprovado_m5_20_du,
		coalesce((quantidade_dias_uteis_mes1 >= 20)::int * faturamento_comprovado_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 20)::int * faturamento_comprovado_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 20)::int * faturamento_comprovado_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 20)::int * faturamento_comprovado_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 20)::int * faturamento_comprovado_mes5,0) as vl_ever_faturamento_comprovado_20_du,
		greatest((quantidade_dias_uteis_mes1 >= 20)::int * faturamento_comprovado_mes1 ,
			(quantidade_dias_uteis_mes2 >= 20)::int * faturamento_comprovado_mes2 ,
			(quantidade_dias_uteis_mes3 >= 20)::int * faturamento_comprovado_mes3 ,
			(quantidade_dias_uteis_mes4 >= 20)::int * faturamento_comprovado_mes4 ,
			(quantidade_dias_uteis_mes5 >= 20)::int * faturamento_comprovado_mes5) as vl_max_faturamento_comprovado_20_du,
		least((quantidade_dias_uteis_mes1 >= 20)::int * faturamento_comprovado_mes1 ,
			(quantidade_dias_uteis_mes2 >= 20)::int * faturamento_comprovado_mes2 ,
			(quantidade_dias_uteis_mes3 >= 20)::int * faturamento_comprovado_mes3 ,
			(quantidade_dias_uteis_mes4 >= 20)::int * faturamento_comprovado_mes4 ,
			(quantidade_dias_uteis_mes5 >= 20)::int * faturamento_comprovado_mes5) as vl_min_faturamento_comprovado_20_du,
		coalesce((quantidade_dias_uteis_mes1 >= 20)::int * faturamento_comprovado_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 20)::int * faturamento_comprovado_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 20)::int * faturamento_comprovado_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 20)::int * faturamento_comprovado_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 20)::int * faturamento_comprovado_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 20)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 20)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 20)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 20)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 20)::int,0)),1) as vl_avg_faturamento_comprovado_20_du,
	--TOTAL MOVIMENTADO - GERAL
		coalesce(total_movimentado_mes1,0) + coalesce(total_movimentado_mes2,0) + coalesce(total_movimentado_mes3,0) + coalesce(total_movimentado_mes4,0) + coalesce(total_movimentado_mes5,0) as vl_ever_total_movimentado,
		greatest(total_movimentado_mes1 , total_movimentado_mes2 , total_movimentado_mes3 , total_movimentado_mes4 , total_movimentado_mes5) as vl_max_total_movimentado,
	--TOTAL MOVIMENTADO - 10 DU
		case 
			when quantidade_dias_uteis_mes1 >= 10 then total_movimentado_mes1
			when quantidade_dias_uteis_mes2 >= 10 then total_movimentado_mes2
			when quantidade_dias_uteis_mes3 >= 10 then total_movimentado_mes3
			when quantidade_dias_uteis_mes4 >= 10 then total_movimentado_mes4
			when quantidade_dias_uteis_mes5 >= 10 then total_movimentado_mes5
		end as vl_total_movimentado_m1_10_du,
		case 
			when quantidade_dias_uteis_mes1 >= 10 then total_movimentado_mes2 * case when quantidade_dias_uteis_mes2 >= 10 then 1 end
			when quantidade_dias_uteis_mes2 >= 10 then total_movimentado_mes3 * case when quantidade_dias_uteis_mes3 >= 10 then 1 end
			when quantidade_dias_uteis_mes3 >= 10 then total_movimentado_mes4 * case when quantidade_dias_uteis_mes4 >= 10 then 1 end
			when quantidade_dias_uteis_mes4 >= 10 then total_movimentado_mes5 * case when quantidade_dias_uteis_mes5 >= 10 then 1 end
		end as vl_total_movimentado_m2_10_du,
		case 
			when quantidade_dias_uteis_mes1 >= 10 then total_movimentado_mes3 * case when quantidade_dias_uteis_mes3 >= 10 then 1 end
			when quantidade_dias_uteis_mes2 >= 10 then total_movimentado_mes4 * case when quantidade_dias_uteis_mes4 >= 10 then 1 end
			when quantidade_dias_uteis_mes3 >= 10 then total_movimentado_mes5 * case when quantidade_dias_uteis_mes5 >= 10 then 1 end
		end as vl_total_movimentado_m3_10_du,
		case 
			when quantidade_dias_uteis_mes1 >= 10 then total_movimentado_mes4 * case when quantidade_dias_uteis_mes4 >= 10 then 1 end
			when quantidade_dias_uteis_mes2 >= 10 then total_movimentado_mes5 * case when quantidade_dias_uteis_mes5 >= 10 then 1 end
		end as vl_total_movimentado_m4_10_du,
		case 
			when quantidade_dias_uteis_mes1 >= 10 then total_movimentado_mes5 * case when quantidade_dias_uteis_mes5 >= 10 then 1 end
		end as vl_total_movimentado_m5_10_du,
		coalesce((quantidade_dias_uteis_mes1 >= 10)::int * total_movimentado_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 10)::int * total_movimentado_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 10)::int * total_movimentado_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 10)::int * total_movimentado_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 10)::int * total_movimentado_mes5,0) as vl_ever_total_movimentado_10_du,
		greatest((quantidade_dias_uteis_mes1 >= 10)::int * total_movimentado_mes1 ,
			(quantidade_dias_uteis_mes2 >= 10)::int * total_movimentado_mes2 ,
			(quantidade_dias_uteis_mes3 >= 10)::int * total_movimentado_mes3 ,
			(quantidade_dias_uteis_mes4 >= 10)::int * total_movimentado_mes4 ,
			(quantidade_dias_uteis_mes5 >= 10)::int * total_movimentado_mes5) as vl_max_total_movimentado_10_du,
		least((quantidade_dias_uteis_mes1 >= 10)::int * total_movimentado_mes1 ,
			(quantidade_dias_uteis_mes2 >= 10)::int * total_movimentado_mes2 ,
			(quantidade_dias_uteis_mes3 >= 10)::int * total_movimentado_mes3 ,
			(quantidade_dias_uteis_mes4 >= 10)::int * total_movimentado_mes4 ,
			(quantidade_dias_uteis_mes5 >= 10)::int * total_movimentado_mes5) as vl_min_total_movimentado_10_du,
		coalesce((quantidade_dias_uteis_mes1 >= 10)::int * total_movimentado_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 10)::int * total_movimentado_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 10)::int * total_movimentado_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 10)::int * total_movimentado_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 10)::int * total_movimentado_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 10)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 10)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 10)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 10)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 10)::int,0)),1) as vl_avg_total_movimentado_10_du,
	--TOTAL MOVIMENTADO - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then total_movimentado_mes1
			when quantidade_dias_uteis_mes2 >= 15 then total_movimentado_mes2
			when quantidade_dias_uteis_mes3 >= 15 then total_movimentado_mes3
			when quantidade_dias_uteis_mes4 >= 15 then total_movimentado_mes4
			when quantidade_dias_uteis_mes5 >= 15 then total_movimentado_mes5
		end as vl_total_movimentado_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then total_movimentado_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then total_movimentado_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then total_movimentado_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then total_movimentado_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_total_movimentado_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then total_movimentado_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then total_movimentado_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then total_movimentado_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_total_movimentado_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then total_movimentado_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then total_movimentado_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_total_movimentado_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then total_movimentado_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_total_movimentado_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * total_movimentado_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * total_movimentado_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * total_movimentado_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * total_movimentado_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * total_movimentado_mes5,0) as vl_ever_total_movimentado_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * total_movimentado_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * total_movimentado_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * total_movimentado_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * total_movimentado_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * total_movimentado_mes5) as vl_max_total_movimentado_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * total_movimentado_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * total_movimentado_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * total_movimentado_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * total_movimentado_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * total_movimentado_mes5) as vl_min_total_movimentado_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * total_movimentado_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * total_movimentado_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * total_movimentado_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * total_movimentado_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * total_movimentado_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_total_movimentado_15_du,
	--TOTAL MOVIMENTADO - 20 DU
		case 
			when quantidade_dias_uteis_mes1 >= 20 then total_movimentado_mes1
			when quantidade_dias_uteis_mes2 >= 20 then total_movimentado_mes2
			when quantidade_dias_uteis_mes3 >= 20 then total_movimentado_mes3
			when quantidade_dias_uteis_mes4 >= 20 then total_movimentado_mes4
			when quantidade_dias_uteis_mes5 >= 20 then total_movimentado_mes5
		end as vl_total_movimentado_m1_20_du,
		case 
			when quantidade_dias_uteis_mes1 >= 20 then total_movimentado_mes2 * case when quantidade_dias_uteis_mes2 >= 20 then 1 end
			when quantidade_dias_uteis_mes2 >= 20 then total_movimentado_mes3 * case when quantidade_dias_uteis_mes3 >= 20 then 1 end
			when quantidade_dias_uteis_mes3 >= 20 then total_movimentado_mes4 * case when quantidade_dias_uteis_mes4 >= 20 then 1 end
			when quantidade_dias_uteis_mes4 >= 20 then total_movimentado_mes5 * case when quantidade_dias_uteis_mes5 >= 20 then 1 end
		end as vl_total_movimentado_m2_20_du,
		case 
			when quantidade_dias_uteis_mes1 >= 20 then total_movimentado_mes3 * case when quantidade_dias_uteis_mes3 >= 20 then 1 end
			when quantidade_dias_uteis_mes2 >= 20 then total_movimentado_mes4 * case when quantidade_dias_uteis_mes4 >= 20 then 1 end
			when quantidade_dias_uteis_mes3 >= 20 then total_movimentado_mes5 * case when quantidade_dias_uteis_mes5 >= 20 then 1 end
		end as vl_total_movimentado_m3_20_du,
		case 
			when quantidade_dias_uteis_mes1 >= 20 then total_movimentado_mes4 * case when quantidade_dias_uteis_mes4 >= 20 then 1 end
			when quantidade_dias_uteis_mes2 >= 20 then total_movimentado_mes5 * case when quantidade_dias_uteis_mes5 >= 20 then 1 end
		end as vl_total_movimentado_m4_20_du,
		case 
			when quantidade_dias_uteis_mes1 >= 20 then total_movimentado_mes5 * case when quantidade_dias_uteis_mes5 >= 20 then 1 end
		end as vl_total_movimentado_m5_20_du,
		coalesce((quantidade_dias_uteis_mes1 >= 20)::int * total_movimentado_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 20)::int * total_movimentado_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 20)::int * total_movimentado_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 20)::int * total_movimentado_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 20)::int * total_movimentado_mes5,0) as vl_ever_total_movimentado_20_du,
		greatest((quantidade_dias_uteis_mes1 >= 20)::int * total_movimentado_mes1 ,
			(quantidade_dias_uteis_mes2 >= 20)::int * total_movimentado_mes2 ,
			(quantidade_dias_uteis_mes3 >= 20)::int * total_movimentado_mes3 ,
			(quantidade_dias_uteis_mes4 >= 20)::int * total_movimentado_mes4 ,
			(quantidade_dias_uteis_mes5 >= 20)::int * total_movimentado_mes5) as vl_max_total_movimentado_20_du,
		least((quantidade_dias_uteis_mes1 >= 20)::int * total_movimentado_mes1 ,
			(quantidade_dias_uteis_mes2 >= 20)::int * total_movimentado_mes2 ,
			(quantidade_dias_uteis_mes3 >= 20)::int * total_movimentado_mes3 ,
			(quantidade_dias_uteis_mes4 >= 20)::int * total_movimentado_mes4 ,
			(quantidade_dias_uteis_mes5 >= 20)::int * total_movimentado_mes5) as vl_min_total_movimentado_20_du,
		coalesce((quantidade_dias_uteis_mes1 >= 20)::int * total_movimentado_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 20)::int * total_movimentado_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 20)::int * total_movimentado_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 20)::int * total_movimentado_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 20)::int * total_movimentado_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 20)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 20)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 20)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 20)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 20)::int,0)),1) as vl_avg_total_movimentado_20_du,
	--QUANTIDADE DE ENTRADAS - GERAL
		coalesce(quantidade_entradas_mes1,0) + coalesce(quantidade_entradas_mes2,0) + coalesce(quantidade_entradas_mes3,0) + coalesce(quantidade_entradas_mes4,0) + coalesce(quantidade_entradas_mes5,0) as vl_ever_quantidade_entradas,
		greatest(quantidade_entradas_mes1 , quantidade_entradas_mes2 , quantidade_entradas_mes3 , quantidade_entradas_mes4 , quantidade_entradas_mes5) as vl_max_quantidade_entradas,
	--QUANTIDADE DE ENTRADAS - 10 DU
		case 
			when quantidade_dias_uteis_mes1 >= 10 then quantidade_entradas_mes1
			when quantidade_dias_uteis_mes2 >= 10 then quantidade_entradas_mes2
			when quantidade_dias_uteis_mes3 >= 10 then quantidade_entradas_mes3
			when quantidade_dias_uteis_mes4 >= 10 then quantidade_entradas_mes4
			when quantidade_dias_uteis_mes5 >= 10 then quantidade_entradas_mes5
		end as vl_quantidade_entradas_m1_10_du,
		case 
			when quantidade_dias_uteis_mes1 >= 10 then quantidade_entradas_mes2 * case when quantidade_dias_uteis_mes2 >= 10 then 1 end
			when quantidade_dias_uteis_mes2 >= 10 then quantidade_entradas_mes3 * case when quantidade_dias_uteis_mes3 >= 10 then 1 end
			when quantidade_dias_uteis_mes3 >= 10 then quantidade_entradas_mes4 * case when quantidade_dias_uteis_mes4 >= 10 then 1 end
			when quantidade_dias_uteis_mes4 >= 10 then quantidade_entradas_mes5 * case when quantidade_dias_uteis_mes5 >= 10 then 1 end
		end as vl_quantidade_entradas_m2_10_du,
		case 
			when quantidade_dias_uteis_mes1 >= 10 then quantidade_entradas_mes3 * case when quantidade_dias_uteis_mes3 >= 10 then 1 end
			when quantidade_dias_uteis_mes2 >= 10 then quantidade_entradas_mes4 * case when quantidade_dias_uteis_mes4 >= 10 then 1 end
			when quantidade_dias_uteis_mes3 >= 10 then quantidade_entradas_mes5 * case when quantidade_dias_uteis_mes5 >= 10 then 1 end
		end as vl_quantidade_entradas_m3_10_du,
		case 
			when quantidade_dias_uteis_mes1 >= 10 then quantidade_entradas_mes4 * case when quantidade_dias_uteis_mes4 >= 10 then 1 end
			when quantidade_dias_uteis_mes2 >= 10 then quantidade_entradas_mes5 * case when quantidade_dias_uteis_mes5 >= 10 then 1 end
		end as vl_quantidade_entradas_m4_10_du,
		case 
			when quantidade_dias_uteis_mes1 >= 10 then quantidade_entradas_mes5 * case when quantidade_dias_uteis_mes5 >= 10 then 1 end
		end as vl_quantidade_entradas_m5_10_du,
		coalesce((quantidade_dias_uteis_mes1 >= 10)::int * quantidade_entradas_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 10)::int * quantidade_entradas_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 10)::int * quantidade_entradas_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 10)::int * quantidade_entradas_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 10)::int * quantidade_entradas_mes5,0) as vl_ever_quantidade_entradas_10_du,
		greatest((quantidade_dias_uteis_mes1 >= 10)::int * quantidade_entradas_mes1 ,
			(quantidade_dias_uteis_mes2 >= 10)::int * quantidade_entradas_mes2 ,
			(quantidade_dias_uteis_mes3 >= 10)::int * quantidade_entradas_mes3 ,
			(quantidade_dias_uteis_mes4 >= 10)::int * quantidade_entradas_mes4 ,
			(quantidade_dias_uteis_mes5 >= 10)::int * quantidade_entradas_mes5) as vl_max_quantidade_entradas_10_du,
		least((quantidade_dias_uteis_mes1 >= 10)::int * quantidade_entradas_mes1 ,
			(quantidade_dias_uteis_mes2 >= 10)::int * quantidade_entradas_mes2 ,
			(quantidade_dias_uteis_mes3 >= 10)::int * quantidade_entradas_mes3 ,
			(quantidade_dias_uteis_mes4 >= 10)::int * quantidade_entradas_mes4 ,
			(quantidade_dias_uteis_mes5 >= 10)::int * quantidade_entradas_mes5) as vl_min_quantidade_entradas_10_du,
		coalesce((quantidade_dias_uteis_mes1 >= 10)::int * quantidade_entradas_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 10)::int * quantidade_entradas_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 10)::int * quantidade_entradas_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 10)::int * quantidade_entradas_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 10)::int * quantidade_entradas_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 10)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 10)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 10)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 10)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 10)::int,0)),1) as vl_avg_quantidade_entradas_10_du,
	--QUANTIDADE DE ENTRADAS - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then quantidade_entradas_mes1
			when quantidade_dias_uteis_mes2 >= 15 then quantidade_entradas_mes2
			when quantidade_dias_uteis_mes3 >= 15 then quantidade_entradas_mes3
			when quantidade_dias_uteis_mes4 >= 15 then quantidade_entradas_mes4
			when quantidade_dias_uteis_mes5 >= 15 then quantidade_entradas_mes5
		end as vl_quantidade_entradas_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then quantidade_entradas_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then quantidade_entradas_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then quantidade_entradas_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then quantidade_entradas_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_quantidade_entradas_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then quantidade_entradas_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then quantidade_entradas_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then quantidade_entradas_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_quantidade_entradas_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then quantidade_entradas_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then quantidade_entradas_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_quantidade_entradas_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then quantidade_entradas_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_quantidade_entradas_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * quantidade_entradas_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * quantidade_entradas_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * quantidade_entradas_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * quantidade_entradas_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * quantidade_entradas_mes5,0) as vl_ever_quantidade_entradas_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * quantidade_entradas_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * quantidade_entradas_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * quantidade_entradas_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * quantidade_entradas_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * quantidade_entradas_mes5) as vl_max_quantidade_entradas_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * quantidade_entradas_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * quantidade_entradas_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * quantidade_entradas_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * quantidade_entradas_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * quantidade_entradas_mes5) as vl_min_quantidade_entradas_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * quantidade_entradas_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * quantidade_entradas_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * quantidade_entradas_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * quantidade_entradas_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * quantidade_entradas_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_quantidade_entradas_15_du,
	--QUANTIDADE DE ENTRADAS - 20 DU
		case 
			when quantidade_dias_uteis_mes1 >= 20 then quantidade_entradas_mes1
			when quantidade_dias_uteis_mes2 >= 20 then quantidade_entradas_mes2
			when quantidade_dias_uteis_mes3 >= 20 then quantidade_entradas_mes3
			when quantidade_dias_uteis_mes4 >= 20 then quantidade_entradas_mes4
			when quantidade_dias_uteis_mes5 >= 20 then quantidade_entradas_mes5
		end as vl_quantidade_entradas_m1_20_du,
		case 
			when quantidade_dias_uteis_mes1 >= 20 then quantidade_entradas_mes2 * case when quantidade_dias_uteis_mes2 >= 20 then 1 end
			when quantidade_dias_uteis_mes2 >= 20 then quantidade_entradas_mes3 * case when quantidade_dias_uteis_mes3 >= 20 then 1 end
			when quantidade_dias_uteis_mes3 >= 20 then quantidade_entradas_mes4 * case when quantidade_dias_uteis_mes4 >= 20 then 1 end
			when quantidade_dias_uteis_mes4 >= 20 then quantidade_entradas_mes5 * case when quantidade_dias_uteis_mes5 >= 20 then 1 end
		end as vl_quantidade_entradas_m2_20_du,
		case 
			when quantidade_dias_uteis_mes1 >= 20 then quantidade_entradas_mes3 * case when quantidade_dias_uteis_mes3 >= 20 then 1 end
			when quantidade_dias_uteis_mes2 >= 20 then quantidade_entradas_mes4 * case when quantidade_dias_uteis_mes4 >= 20 then 1 end
			when quantidade_dias_uteis_mes3 >= 20 then quantidade_entradas_mes5 * case when quantidade_dias_uteis_mes5 >= 20 then 1 end
		end as vl_quantidade_entradas_m3_20_du,
		case 
			when quantidade_dias_uteis_mes1 >= 20 then quantidade_entradas_mes4 * case when quantidade_dias_uteis_mes4 >= 20 then 1 end
			when quantidade_dias_uteis_mes2 >= 20 then quantidade_entradas_mes5 * case when quantidade_dias_uteis_mes5 >= 20 then 1 end
		end as vl_quantidade_entradas_m4_20_du,
		case 
			when quantidade_dias_uteis_mes1 >= 20 then quantidade_entradas_mes5 * case when quantidade_dias_uteis_mes5 >= 20 then 1 end
		end as vl_quantidade_entradas_m5_20_du,
		coalesce((quantidade_dias_uteis_mes1 >= 20)::int * quantidade_entradas_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 20)::int * quantidade_entradas_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 20)::int * quantidade_entradas_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 20)::int * quantidade_entradas_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 20)::int * quantidade_entradas_mes5,0) as vl_ever_quantidade_entradas_20_du,
		greatest((quantidade_dias_uteis_mes1 >= 20)::int * quantidade_entradas_mes1 ,
			(quantidade_dias_uteis_mes2 >= 20)::int * quantidade_entradas_mes2 ,
			(quantidade_dias_uteis_mes3 >= 20)::int * quantidade_entradas_mes3 ,
			(quantidade_dias_uteis_mes4 >= 20)::int * quantidade_entradas_mes4 ,
			(quantidade_dias_uteis_mes5 >= 20)::int * quantidade_entradas_mes5) as vl_max_quantidade_entradas_20_du,
		least((quantidade_dias_uteis_mes1 >= 20)::int * quantidade_entradas_mes1 ,
			(quantidade_dias_uteis_mes2 >= 20)::int * quantidade_entradas_mes2 ,
			(quantidade_dias_uteis_mes3 >= 20)::int * quantidade_entradas_mes3 ,
			(quantidade_dias_uteis_mes4 >= 20)::int * quantidade_entradas_mes4 ,
			(quantidade_dias_uteis_mes5 >= 20)::int * quantidade_entradas_mes5) as vl_min_quantidade_entradas_20_du,
		coalesce((quantidade_dias_uteis_mes1 >= 20)::int * quantidade_entradas_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 20)::int * quantidade_entradas_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 20)::int * quantidade_entradas_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 20)::int * quantidade_entradas_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 20)::int * quantidade_entradas_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 20)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 20)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 20)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 20)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 20)::int,0)),1) as vl_avg_quantidade_entradas_20_du,
	--TICKET MEDIO - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then ticket_medio_mes1
			when quantidade_dias_uteis_mes2 >= 15 then ticket_medio_mes2
			when quantidade_dias_uteis_mes3 >= 15 then ticket_medio_mes3
			when quantidade_dias_uteis_mes4 >= 15 then ticket_medio_mes4
			when quantidade_dias_uteis_mes5 >= 15 then ticket_medio_mes5
		end as vl_ticket_medio_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then ticket_medio_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then ticket_medio_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then ticket_medio_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then ticket_medio_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_ticket_medio_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then ticket_medio_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then ticket_medio_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then ticket_medio_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_ticket_medio_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then ticket_medio_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then ticket_medio_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_ticket_medio_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then ticket_medio_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_ticket_medio_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * ticket_medio_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * ticket_medio_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * ticket_medio_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * ticket_medio_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * ticket_medio_mes5,0) as vl_ever_ticket_medio_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * ticket_medio_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * ticket_medio_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * ticket_medio_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * ticket_medio_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * ticket_medio_mes5) as vl_max_ticket_medio_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * ticket_medio_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * ticket_medio_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * ticket_medio_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * ticket_medio_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * ticket_medio_mes5) as vl_min_ticket_medio_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * ticket_medio_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * ticket_medio_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * ticket_medio_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * ticket_medio_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * ticket_medio_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_ticket_medio_15_du,
	--MAIOR ENTRADA - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then maior_entrada_mes1
			when quantidade_dias_uteis_mes2 >= 15 then maior_entrada_mes2
			when quantidade_dias_uteis_mes3 >= 15 then maior_entrada_mes3
			when quantidade_dias_uteis_mes4 >= 15 then maior_entrada_mes4
			when quantidade_dias_uteis_mes5 >= 15 then maior_entrada_mes5
		end as vl_maior_entrada_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then maior_entrada_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then maior_entrada_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then maior_entrada_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then maior_entrada_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_maior_entrada_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then maior_entrada_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then maior_entrada_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then maior_entrada_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_maior_entrada_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then maior_entrada_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then maior_entrada_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_maior_entrada_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then maior_entrada_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_maior_entrada_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * maior_entrada_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * maior_entrada_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * maior_entrada_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * maior_entrada_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * maior_entrada_mes5,0) as vl_ever_maior_entrada_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * maior_entrada_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * maior_entrada_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * maior_entrada_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * maior_entrada_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * maior_entrada_mes5) as vl_max_maior_entrada_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * maior_entrada_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * maior_entrada_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * maior_entrada_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * maior_entrada_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * maior_entrada_mes5) as vl_min_maior_entrada_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * maior_entrada_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * maior_entrada_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * maior_entrada_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * maior_entrada_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * maior_entrada_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_maior_entrada_15_du,
	--DEPOSITO ESPECIE - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then deposito_especie_mes1
			when quantidade_dias_uteis_mes2 >= 15 then deposito_especie_mes2
			when quantidade_dias_uteis_mes3 >= 15 then deposito_especie_mes3
			when quantidade_dias_uteis_mes4 >= 15 then deposito_especie_mes4
			when quantidade_dias_uteis_mes5 >= 15 then deposito_especie_mes5
		end as vl_deposito_especie_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then deposito_especie_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then deposito_especie_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then deposito_especie_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then deposito_especie_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_deposito_especie_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then deposito_especie_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then deposito_especie_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then deposito_especie_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_deposito_especie_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then deposito_especie_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then deposito_especie_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_deposito_especie_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then deposito_especie_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_deposito_especie_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * deposito_especie_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * deposito_especie_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * deposito_especie_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * deposito_especie_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * deposito_especie_mes5,0) as vl_ever_deposito_especie_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * deposito_especie_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * deposito_especie_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * deposito_especie_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * deposito_especie_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * deposito_especie_mes5) as vl_max_deposito_especie_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * deposito_especie_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * deposito_especie_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * deposito_especie_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * deposito_especie_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * deposito_especie_mes5) as vl_min_deposito_especie_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * deposito_especie_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * deposito_especie_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * deposito_especie_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * deposito_especie_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * deposito_especie_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_deposito_especie_15_du,
	--ANTECIPACAO - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then antecipacao_mes1
			when quantidade_dias_uteis_mes2 >= 15 then antecipacao_mes2
			when quantidade_dias_uteis_mes3 >= 15 then antecipacao_mes3
			when quantidade_dias_uteis_mes4 >= 15 then antecipacao_mes4
			when quantidade_dias_uteis_mes5 >= 15 then antecipacao_mes5
		end as vl_antecipacao_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then antecipacao_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then antecipacao_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then antecipacao_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then antecipacao_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_antecipacao_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then antecipacao_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then antecipacao_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then antecipacao_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_antecipacao_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then antecipacao_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then antecipacao_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_antecipacao_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then antecipacao_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_antecipacao_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * antecipacao_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * antecipacao_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * antecipacao_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * antecipacao_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * antecipacao_mes5,0) as vl_ever_antecipacao_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * antecipacao_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * antecipacao_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * antecipacao_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * antecipacao_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * antecipacao_mes5) as vl_max_antecipacao_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * antecipacao_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * antecipacao_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * antecipacao_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * antecipacao_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * antecipacao_mes5) as vl_min_antecipacao_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * antecipacao_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * antecipacao_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * antecipacao_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * antecipacao_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * antecipacao_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_antecipacao_15_du,
	--MARKETPLACE - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then marketplace_mes1
			when quantidade_dias_uteis_mes2 >= 15 then marketplace_mes2
			when quantidade_dias_uteis_mes3 >= 15 then marketplace_mes3
			when quantidade_dias_uteis_mes4 >= 15 then marketplace_mes4
			when quantidade_dias_uteis_mes5 >= 15 then marketplace_mes5
		end as vl_marketplace_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then marketplace_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then marketplace_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then marketplace_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then marketplace_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_marketplace_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then marketplace_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then marketplace_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then marketplace_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_marketplace_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then marketplace_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then marketplace_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_marketplace_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then marketplace_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_marketplace_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * marketplace_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * marketplace_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * marketplace_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * marketplace_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * marketplace_mes5,0) as vl_ever_marketplace_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * marketplace_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * marketplace_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * marketplace_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * marketplace_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * marketplace_mes5) as vl_max_marketplace_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * marketplace_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * marketplace_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * marketplace_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * marketplace_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * marketplace_mes5) as vl_min_marketplace_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * marketplace_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * marketplace_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * marketplace_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * marketplace_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * marketplace_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_marketplace_15_du,
	--PRINCIPAL SACADO - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then principal_sacado_mes1
			when quantidade_dias_uteis_mes2 >= 15 then principal_sacado_mes2
			when quantidade_dias_uteis_mes3 >= 15 then principal_sacado_mes3
			when quantidade_dias_uteis_mes4 >= 15 then principal_sacado_mes4
			when quantidade_dias_uteis_mes5 >= 15 then principal_sacado_mes5
		end as vl_principal_sacado_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then principal_sacado_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then principal_sacado_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then principal_sacado_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then principal_sacado_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_principal_sacado_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then principal_sacado_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then principal_sacado_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then principal_sacado_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_principal_sacado_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then principal_sacado_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then principal_sacado_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_principal_sacado_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then principal_sacado_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_principal_sacado_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * principal_sacado_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * principal_sacado_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * principal_sacado_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * principal_sacado_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * principal_sacado_mes5,0) as vl_ever_principal_sacado_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * principal_sacado_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * principal_sacado_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * principal_sacado_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * principal_sacado_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * principal_sacado_mes5) as vl_max_principal_sacado_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * principal_sacado_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * principal_sacado_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * principal_sacado_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * principal_sacado_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * principal_sacado_mes5) as vl_min_principal_sacado_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * principal_sacado_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * principal_sacado_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * principal_sacado_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * principal_sacado_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * principal_sacado_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_principal_sacado_15_du,
	--SAQUE ATM - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * saque_atm_mes1
			when quantidade_dias_uteis_mes2 >= 15 then -1 * saque_atm_mes2
			when quantidade_dias_uteis_mes3 >= 15 then -1 * saque_atm_mes3
			when quantidade_dias_uteis_mes4 >= 15 then -1 * saque_atm_mes4
			when quantidade_dias_uteis_mes5 >= 15 then -1 * saque_atm_mes5
		end as vl_saque_atm_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * saque_atm_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * saque_atm_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * saque_atm_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then -1 * saque_atm_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_saque_atm_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * saque_atm_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * saque_atm_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * saque_atm_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_saque_atm_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * saque_atm_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * saque_atm_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_saque_atm_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * saque_atm_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_saque_atm_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * saque_atm_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * saque_atm_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * saque_atm_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * saque_atm_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * saque_atm_mes5,0) as vl_ever_saque_atm_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * -1 * saque_atm_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * saque_atm_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * saque_atm_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * saque_atm_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * saque_atm_mes5) as vl_max_saque_atm_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * -1 * saque_atm_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * saque_atm_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * saque_atm_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * saque_atm_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * saque_atm_mes5) as vl_min_saque_atm_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * saque_atm_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * saque_atm_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * saque_atm_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * saque_atm_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * saque_atm_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_saque_atm_15_du,
	--SAQUE ATM / TOTAL PAGAMENTO - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * saque_atm_mes1 / greatest(-1 * total_saida_mes1,1)
			when quantidade_dias_uteis_mes2 >= 15 then -1 * saque_atm_mes2 / greatest(-1 * total_saida_mes2,1)
			when quantidade_dias_uteis_mes3 >= 15 then -1 * saque_atm_mes3 / greatest(-1 * total_saida_mes3,1)
			when quantidade_dias_uteis_mes4 >= 15 then -1 * saque_atm_mes4 / greatest(-1 * total_saida_mes4,1)
			when quantidade_dias_uteis_mes5 >= 15 then -1 * saque_atm_mes5 / greatest(-1 * total_saida_mes5,1)
		end as vl_perc_saque_atm_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * saque_atm_mes2 / greatest(-1 * total_saida_mes2,1) * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * saque_atm_mes3 / greatest(-1 * total_saida_mes3,1) * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * saque_atm_mes4 / greatest(-1 * total_saida_mes4,1) * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then -1 * saque_atm_mes5 / greatest(-1 * total_saida_mes5,1) * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_perc_saque_atm_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * saque_atm_mes3 / greatest(-1 * total_saida_mes3,1) * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * saque_atm_mes4 / greatest(-1 * total_saida_mes4,1) * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * saque_atm_mes5 / greatest(-1 * total_saida_mes5,1) * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_perc_saque_atm_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * saque_atm_mes4 / greatest(-1 * total_saida_mes4,1) * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * saque_atm_mes5 / greatest(-1 * total_saida_mes5,1) * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_perc_saque_atm_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * saque_atm_mes5 / greatest(-1 * total_saida_mes5,1) * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_perc_saque_atm_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * saque_atm_mes1 / greatest(-1 * total_saida_mes1,1),0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * saque_atm_mes2 / greatest(-1 * total_saida_mes2,1),0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * saque_atm_mes3 / greatest(-1 * total_saida_mes3,1),0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * saque_atm_mes4 / greatest(-1 * total_saida_mes4,1),0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * saque_atm_mes5 / greatest(-1 * total_saida_mes5,1),0) as vl_ever_perc_saque_atm_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * -1 * saque_atm_mes1 / greatest(-1 * total_saida_mes1,1),
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * saque_atm_mes2 / greatest(-1 * total_saida_mes2,1),
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * saque_atm_mes3 / greatest(-1 * total_saida_mes3,1),
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * saque_atm_mes4 / greatest(-1 * total_saida_mes4,1),
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * saque_atm_mes5 / greatest(-1 * total_saida_mes5,1)) as vl_max_perc_saque_atm_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * -1 * saque_atm_mes1 / greatest(-1 * total_saida_mes1,1),
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * saque_atm_mes2 / greatest(-1 * total_saida_mes2,1),
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * saque_atm_mes3 / greatest(-1 * total_saida_mes3,1),
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * saque_atm_mes4 / greatest(-1 * total_saida_mes4,1),
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * saque_atm_mes5 / greatest(-1 * total_saida_mes5,1)) as vl_min_perc_saque_atm_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * saque_atm_mes1 / greatest(-1 * total_saida_mes1,1),0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * saque_atm_mes2 / greatest(-1 * total_saida_mes2,1),0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * saque_atm_mes3 / greatest(-1 * total_saida_mes3,1),0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * saque_atm_mes4 / greatest(-1 * total_saida_mes4,1),0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * saque_atm_mes5 / greatest(-1 * total_saida_mes5,1),0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_perc_saque_atm_15_du,
	--CONTAS E DESPESAS - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * contas_despesas_mes1
			when quantidade_dias_uteis_mes2 >= 15 then -1 * contas_despesas_mes2
			when quantidade_dias_uteis_mes3 >= 15 then -1 * contas_despesas_mes3
			when quantidade_dias_uteis_mes4 >= 15 then -1 * contas_despesas_mes4
			when quantidade_dias_uteis_mes5 >= 15 then -1 * contas_despesas_mes5
		end as vl_contas_despesas_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * contas_despesas_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * contas_despesas_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * contas_despesas_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then -1 * contas_despesas_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_contas_despesas_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * contas_despesas_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * contas_despesas_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * contas_despesas_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_contas_despesas_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * contas_despesas_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * contas_despesas_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_contas_despesas_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * contas_despesas_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_contas_despesas_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * contas_despesas_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * contas_despesas_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * contas_despesas_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * contas_despesas_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * contas_despesas_mes5,0) as vl_ever_contas_despesas_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * -1 * contas_despesas_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * contas_despesas_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * contas_despesas_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * contas_despesas_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * contas_despesas_mes5) as vl_max_contas_despesas_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * -1 * contas_despesas_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * contas_despesas_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * contas_despesas_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * contas_despesas_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * contas_despesas_mes5) as vl_min_contas_despesas_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * contas_despesas_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * contas_despesas_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * contas_despesas_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * contas_despesas_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * contas_despesas_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_contas_despesas_15_du,
	--CONTAS DESPESA / TOTAL PAGAMENTO - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * contas_despesas_mes1 / greatest(-1 * total_saida_mes1,1)
			when quantidade_dias_uteis_mes2 >= 15 then -1 * contas_despesas_mes2 / greatest(-1 * total_saida_mes2,1)
			when quantidade_dias_uteis_mes3 >= 15 then -1 * contas_despesas_mes3 / greatest(-1 * total_saida_mes3,1)
			when quantidade_dias_uteis_mes4 >= 15 then -1 * contas_despesas_mes4 / greatest(-1 * total_saida_mes4,1)
			when quantidade_dias_uteis_mes5 >= 15 then -1 * contas_despesas_mes5 / greatest(-1 * total_saida_mes5,1)
		end as vl_perc_contas_despesas_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * contas_despesas_mes2 / greatest(-1 * total_saida_mes2,1) * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * contas_despesas_mes3 / greatest(-1 * total_saida_mes3,1) * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * contas_despesas_mes4 / greatest(-1 * total_saida_mes4,1) * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then -1 * contas_despesas_mes5 / greatest(-1 * total_saida_mes5,1) * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_perc_contas_despesas_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * contas_despesas_mes3 / greatest(-1 * total_saida_mes3,1) * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * contas_despesas_mes4 / greatest(-1 * total_saida_mes4,1) * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * contas_despesas_mes5 / greatest(-1 * total_saida_mes5,1) * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_perc_contas_despesas_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * contas_despesas_mes4 / greatest(-1 * total_saida_mes4,1) * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * contas_despesas_mes5 / greatest(-1 * total_saida_mes5,1) * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_perc_contas_despesas_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * contas_despesas_mes5 / greatest(-1 * total_saida_mes5,1) * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_perc_contas_despesas_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * contas_despesas_mes1 / greatest(-1 * total_saida_mes1,1),0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * contas_despesas_mes2 / greatest(-1 * total_saida_mes2,1),0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * contas_despesas_mes3 / greatest(-1 * total_saida_mes3,1),0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * contas_despesas_mes4 / greatest(-1 * total_saida_mes4,1),0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * contas_despesas_mes5 / greatest(-1 * total_saida_mes5,1),0) as vl_ever_perc_contas_despesas_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * -1 * contas_despesas_mes1 / greatest(-1 * total_saida_mes1,1) ,
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * contas_despesas_mes2 / greatest(-1 * total_saida_mes2,1) ,
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * contas_despesas_mes3 / greatest(-1 * total_saida_mes3,1) ,
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * contas_despesas_mes4 / greatest(-1 * total_saida_mes4,1) ,
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * contas_despesas_mes5 / greatest(-1 * total_saida_mes5,1)) as vl_max_perc_contas_despesas_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * -1 * contas_despesas_mes1 / greatest(-1 * total_saida_mes1,1) ,
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * contas_despesas_mes2 / greatest(-1 * total_saida_mes2,1) ,
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * contas_despesas_mes3 / greatest(-1 * total_saida_mes3,1) ,
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * contas_despesas_mes4 / greatest(-1 * total_saida_mes4,1) ,
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * contas_despesas_mes5 / greatest(-1 * total_saida_mes5,1)) as vl_min_perc_contas_despesas_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * contas_despesas_mes1 / greatest(-1 * total_saida_mes1,1),0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * contas_despesas_mes2 / greatest(-1 * total_saida_mes2,1),0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * contas_despesas_mes3 / greatest(-1 * total_saida_mes3,1),0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * contas_despesas_mes4 / greatest(-1 * total_saida_mes4,1),0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * contas_despesas_mes5 / greatest(-1 * total_saida_mes5,1),0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_perc_contas_despesas_15_du,
	--PAGAMENTO PMT - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * pgto_pmt_credito_mes1
			when quantidade_dias_uteis_mes2 >= 15 then -1 * pgto_pmt_credito_mes2
			when quantidade_dias_uteis_mes3 >= 15 then -1 * pgto_pmt_credito_mes3
			when quantidade_dias_uteis_mes4 >= 15 then -1 * pgto_pmt_credito_mes4
			when quantidade_dias_uteis_mes5 >= 15 then -1 * pgto_pmt_credito_mes5
		end as vl_pgto_pmt_credito_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * pgto_pmt_credito_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * pgto_pmt_credito_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * pgto_pmt_credito_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then -1 * pgto_pmt_credito_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_pgto_pmt_credito_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * pgto_pmt_credito_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * pgto_pmt_credito_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * pgto_pmt_credito_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_pgto_pmt_credito_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * pgto_pmt_credito_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * pgto_pmt_credito_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_pgto_pmt_credito_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * pgto_pmt_credito_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_pgto_pmt_credito_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * pgto_pmt_credito_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * pgto_pmt_credito_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * pgto_pmt_credito_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * pgto_pmt_credito_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * pgto_pmt_credito_mes5,0) as vl_ever_pgto_pmt_credito_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * -1 * pgto_pmt_credito_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * pgto_pmt_credito_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * pgto_pmt_credito_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * pgto_pmt_credito_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * pgto_pmt_credito_mes5) as vl_max_pgto_pmt_credito_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * -1 * pgto_pmt_credito_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * pgto_pmt_credito_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * pgto_pmt_credito_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * pgto_pmt_credito_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * pgto_pmt_credito_mes5) as vl_min_pgto_pmt_credito_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * pgto_pmt_credito_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * pgto_pmt_credito_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * pgto_pmt_credito_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * pgto_pmt_credito_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * pgto_pmt_credito_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_pgto_pmt_credito_15_du,
	--PAGAMENTO PMT / TOTAL PAGAMENTO - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * pgto_pmt_credito_mes1 / greatest(-1 * total_saida_mes1,1)
			when quantidade_dias_uteis_mes2 >= 15 then -1 * pgto_pmt_credito_mes2 / greatest(-1 * total_saida_mes2,1)
			when quantidade_dias_uteis_mes3 >= 15 then -1 * pgto_pmt_credito_mes3 / greatest(-1 * total_saida_mes3,1)
			when quantidade_dias_uteis_mes4 >= 15 then -1 * pgto_pmt_credito_mes4 / greatest(-1 * total_saida_mes4,1)
			when quantidade_dias_uteis_mes5 >= 15 then -1 * pgto_pmt_credito_mes5 / greatest(-1 * total_saida_mes5,1)
		end as vl_perc_pgto_pmt_credito_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * pgto_pmt_credito_mes2 / greatest(-1 * total_saida_mes2,1) * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * pgto_pmt_credito_mes3 / greatest(-1 * total_saida_mes3,1) * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * pgto_pmt_credito_mes4 / greatest(-1 * total_saida_mes4,1) * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then -1 * pgto_pmt_credito_mes5 / greatest(-1 * total_saida_mes5,1) * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_perc_pgto_pmt_credito_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * pgto_pmt_credito_mes3 / greatest(-1 * total_saida_mes3,1) * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * pgto_pmt_credito_mes4 / greatest(-1 * total_saida_mes4,1) * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * pgto_pmt_credito_mes5 / greatest(-1 * total_saida_mes5,1) * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_perc_pgto_pmt_credito_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * pgto_pmt_credito_mes4 / greatest(-1 * total_saida_mes4,1) * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * pgto_pmt_credito_mes5 / greatest(-1 * total_saida_mes5,1) * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_perc_pgto_pmt_credito_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * pgto_pmt_credito_mes5 / greatest(-1 * total_saida_mes5,1) * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_perc_pgto_pmt_credito_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * pgto_pmt_credito_mes1 / greatest(-1 * total_saida_mes1,1),0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * pgto_pmt_credito_mes2 / greatest(-1 * total_saida_mes2,1),0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * pgto_pmt_credito_mes3 / greatest(-1 * total_saida_mes3,1),0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * pgto_pmt_credito_mes4 / greatest(-1 * total_saida_mes4,1),0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * pgto_pmt_credito_mes5 / greatest(-1 * total_saida_mes5,1),0) as vl_ever_perc_pgto_pmt_credito_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * -1 * pgto_pmt_credito_mes1 / greatest(-1 * total_saida_mes1,1) ,
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * pgto_pmt_credito_mes2 / greatest(-1 * total_saida_mes2,1) ,
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * pgto_pmt_credito_mes3 / greatest(-1 * total_saida_mes3,1) ,
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * pgto_pmt_credito_mes4 / greatest(-1 * total_saida_mes4,1) ,
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * pgto_pmt_credito_mes5 / greatest(-1 * total_saida_mes5,1)) as vl_max_perc_pgto_pmt_credito_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * -1 * pgto_pmt_credito_mes1 / greatest(-1 * total_saida_mes1,1) ,
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * pgto_pmt_credito_mes2 / greatest(-1 * total_saida_mes2,1) ,
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * pgto_pmt_credito_mes3 / greatest(-1 * total_saida_mes3,1) ,
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * pgto_pmt_credito_mes4 / greatest(-1 * total_saida_mes4,1) ,
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * pgto_pmt_credito_mes5 / greatest(-1 * total_saida_mes5,1)) as vl_min_perc_pgto_pmt_credito_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * pgto_pmt_credito_mes1 / greatest(-1 * total_saida_mes1,1),0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * pgto_pmt_credito_mes2 / greatest(-1 * total_saida_mes2,1),0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * pgto_pmt_credito_mes3 / greatest(-1 * total_saida_mes3,1),0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * pgto_pmt_credito_mes4 / greatest(-1 * total_saida_mes4,1),0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * pgto_pmt_credito_mes5 / greatest(-1 * total_saida_mes5,1),0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_perc_pgto_pmt_credito_15_du,
	--PAGAMENTO JUROS E MORA - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * pgto_juros_mora_mes1
			when quantidade_dias_uteis_mes2 >= 15 then -1 * pgto_juros_mora_mes2
			when quantidade_dias_uteis_mes3 >= 15 then -1 * pgto_juros_mora_mes3
			when quantidade_dias_uteis_mes4 >= 15 then -1 * pgto_juros_mora_mes4
			when quantidade_dias_uteis_mes5 >= 15 then -1 * pgto_juros_mora_mes5
		end as vl_pgto_juros_mora_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * pgto_juros_mora_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * pgto_juros_mora_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * pgto_juros_mora_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then -1 * pgto_juros_mora_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_pgto_juros_mora_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * pgto_juros_mora_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * pgto_juros_mora_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * pgto_juros_mora_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_pgto_juros_mora_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * pgto_juros_mora_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * pgto_juros_mora_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_pgto_juros_mora_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * pgto_juros_mora_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_pgto_juros_mora_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * pgto_juros_mora_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * pgto_juros_mora_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * pgto_juros_mora_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * pgto_juros_mora_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * pgto_juros_mora_mes5,0) as vl_ever_pgto_juros_mora_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * -1 * pgto_juros_mora_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * pgto_juros_mora_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * pgto_juros_mora_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * pgto_juros_mora_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * pgto_juros_mora_mes5) as vl_max_pgto_juros_mora_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * -1 * pgto_juros_mora_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * pgto_juros_mora_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * pgto_juros_mora_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * pgto_juros_mora_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * pgto_juros_mora_mes5) as vl_min_pgto_juros_mora_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * pgto_juros_mora_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * pgto_juros_mora_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * pgto_juros_mora_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * pgto_juros_mora_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * pgto_juros_mora_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_pgto_juros_mora_15_du,
	--PAGAMENTO JUROS MORA / TOTAL PAGAMENTO - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * pgto_juros_mora_mes1 / greatest(-1 * total_saida_mes1,1)
			when quantidade_dias_uteis_mes2 >= 15 then -1 * pgto_juros_mora_mes2 / greatest(-1 * total_saida_mes2,1)
			when quantidade_dias_uteis_mes3 >= 15 then -1 * pgto_juros_mora_mes3 / greatest(-1 * total_saida_mes3,1)
			when quantidade_dias_uteis_mes4 >= 15 then -1 * pgto_juros_mora_mes4 / greatest(-1 * total_saida_mes4,1)
			when quantidade_dias_uteis_mes5 >= 15 then -1 * pgto_juros_mora_mes5 / greatest(-1 * total_saida_mes5,1)
		end as vl_perc_pgto_juros_mora_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * pgto_juros_mora_mes2 / greatest(-1 * total_saida_mes2,1) * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * pgto_juros_mora_mes3 / greatest(-1 * total_saida_mes3,1) * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * pgto_juros_mora_mes4 / greatest(-1 * total_saida_mes4,1) * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then -1 * pgto_juros_mora_mes5 / greatest(-1 * total_saida_mes5,1) * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_perc_pgto_juros_mora_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * pgto_juros_mora_mes3 / greatest(-1 * total_saida_mes3,1) * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * pgto_juros_mora_mes4 / greatest(-1 * total_saida_mes4,1) * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * pgto_juros_mora_mes5 / greatest(-1 * total_saida_mes5,1) * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_perc_pgto_juros_mora_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * pgto_juros_mora_mes4 / greatest(-1 * total_saida_mes4,1) * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * pgto_juros_mora_mes5 / greatest(-1 * total_saida_mes5,1) * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_perc_pgto_juros_mora_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * pgto_juros_mora_mes5 / greatest(-1 * total_saida_mes5,1) * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_perc_pgto_juros_mora_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * pgto_juros_mora_mes1 / greatest(-1 * total_saida_mes1,1),0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * pgto_juros_mora_mes2 / greatest(-1 * total_saida_mes2,1),0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * pgto_juros_mora_mes3 / greatest(-1 * total_saida_mes3,1),0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * pgto_juros_mora_mes4 / greatest(-1 * total_saida_mes4,1),0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * pgto_juros_mora_mes5 / greatest(-1 * total_saida_mes5,1),0) as vl_ever_perc_pgto_juros_mora_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * -1 * pgto_juros_mora_mes1 / greatest(-1 * total_saida_mes1,1) ,
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * pgto_juros_mora_mes2 / greatest(-1 * total_saida_mes2,1) ,
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * pgto_juros_mora_mes3 / greatest(-1 * total_saida_mes3,1) ,
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * pgto_juros_mora_mes4 / greatest(-1 * total_saida_mes4,1) ,
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * pgto_juros_mora_mes5 / greatest(-1 * total_saida_mes5,1)) as vl_max_perc_pgto_juros_mora_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * -1 * pgto_juros_mora_mes1 / greatest(-1 * total_saida_mes1,1) ,
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * pgto_juros_mora_mes2 / greatest(-1 * total_saida_mes2,1) ,
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * pgto_juros_mora_mes3 / greatest(-1 * total_saida_mes3,1) ,
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * pgto_juros_mora_mes4 / greatest(-1 * total_saida_mes4,1) ,
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * pgto_juros_mora_mes5 / greatest(-1 * total_saida_mes5,1)) as vl_min_perc_pgto_juros_mora_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * pgto_juros_mora_mes1 / greatest(-1 * total_saida_mes1,1),0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * pgto_juros_mora_mes2 / greatest(-1 * total_saida_mes2,1),0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * pgto_juros_mora_mes3 / greatest(-1 * total_saida_mes3,1),0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * pgto_juros_mora_mes4 / greatest(-1 * total_saida_mes4,1),0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * pgto_juros_mora_mes5 / greatest(-1 * total_saida_mes5,1),0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_perc_pgto_juros_mora_15_du,
	--MAIOR SAIDA - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * maior_saida_mes1
			when quantidade_dias_uteis_mes2 >= 15 then -1 * maior_saida_mes2
			when quantidade_dias_uteis_mes3 >= 15 then -1 * maior_saida_mes3
			when quantidade_dias_uteis_mes4 >= 15 then -1 * maior_saida_mes4
			when quantidade_dias_uteis_mes5 >= 15 then -1 * maior_saida_mes5
		end as vl_maior_saida_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * maior_saida_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * maior_saida_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * maior_saida_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then -1 * maior_saida_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_maior_saida_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * maior_saida_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * maior_saida_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * maior_saida_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_maior_saida_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * maior_saida_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * maior_saida_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_maior_saida_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * maior_saida_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_maior_saida_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * maior_saida_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * maior_saida_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * maior_saida_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * maior_saida_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * maior_saida_mes5,0) as vl_ever_maior_saida_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * -1 * maior_saida_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * maior_saida_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * maior_saida_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * maior_saida_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * maior_saida_mes5) as vl_max_maior_saida_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * -1 * maior_saida_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * maior_saida_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * maior_saida_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * maior_saida_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * maior_saida_mes5) as vl_min_maior_saida_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * maior_saida_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * maior_saida_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * maior_saida_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * maior_saida_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * maior_saida_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_maior_saida_15_du,
	--TOTAL SAIDA - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * total_saida_mes1
			when quantidade_dias_uteis_mes2 >= 15 then -1 * total_saida_mes2
			when quantidade_dias_uteis_mes3 >= 15 then -1 * total_saida_mes3
			when quantidade_dias_uteis_mes4 >= 15 then -1 * total_saida_mes4
			when quantidade_dias_uteis_mes5 >= 15 then -1 * total_saida_mes5
		end as vl_total_saida_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * total_saida_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * total_saida_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * total_saida_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then -1 * total_saida_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_total_saida_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * total_saida_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * total_saida_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * total_saida_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_total_saida_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * total_saida_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * total_saida_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_total_saida_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * total_saida_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_total_saida_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * total_saida_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * total_saida_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * total_saida_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * total_saida_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * total_saida_mes5,0) as vl_ever_total_saida_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * -1 * total_saida_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * total_saida_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * total_saida_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * total_saida_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * total_saida_mes5) as vl_max_total_saida_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * -1 * total_saida_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * total_saida_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * total_saida_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * total_saida_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * total_saida_mes5) as vl_min_total_saida_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * total_saida_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * total_saida_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * total_saida_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * total_saida_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * total_saida_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_total_saida_15_du,
	--TICKET MEDIO SAIDA - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * total_saida_mes1 / greatest(quantidade_dias_uteis_mes1,1)
			when quantidade_dias_uteis_mes2 >= 15 then -1 * total_saida_mes2 / greatest(quantidade_dias_uteis_mes2,1)
			when quantidade_dias_uteis_mes3 >= 15 then -1 * total_saida_mes3 / greatest(quantidade_dias_uteis_mes3,1)
			when quantidade_dias_uteis_mes4 >= 15 then -1 * total_saida_mes4 / greatest(quantidade_dias_uteis_mes4,1)
			when quantidade_dias_uteis_mes5 >= 15 then -1 * total_saida_mes5 / greatest(quantidade_dias_uteis_mes5,1)
		end as vl_ticket_medio_saida_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * total_saida_mes2 / greatest(quantidade_dias_uteis_mes2,1) * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * total_saida_mes3 / greatest(quantidade_dias_uteis_mes3,1) * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * total_saida_mes4 / greatest(quantidade_dias_uteis_mes4,1) * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then -1 * total_saida_mes5 / greatest(quantidade_dias_uteis_mes5,1) * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_ticket_medio_saida_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * total_saida_mes3 / greatest(quantidade_dias_uteis_mes3,1) * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * total_saida_mes4 / greatest(quantidade_dias_uteis_mes4,1) * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then -1 * total_saida_mes5 / greatest(quantidade_dias_uteis_mes5,1) * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_ticket_medio_saida_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * total_saida_mes4 / greatest(quantidade_dias_uteis_mes4,1) * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then -1 * total_saida_mes5 / greatest(quantidade_dias_uteis_mes5,1) * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_ticket_medio_saida_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then -1 * total_saida_mes5 / greatest(quantidade_dias_uteis_mes5,1) * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_ticket_medio_saida_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * total_saida_mes1 / greatest(quantidade_dias_uteis_mes1,1),0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * total_saida_mes2 / greatest(quantidade_dias_uteis_mes2,1),0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * total_saida_mes3 / greatest(quantidade_dias_uteis_mes3,1),0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * total_saida_mes4 / greatest(quantidade_dias_uteis_mes4,1),0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * total_saida_mes5 / greatest(quantidade_dias_uteis_mes5,1),0) as vl_ever_ticket_medio_saida_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * -1 * total_saida_mes1 / greatest(quantidade_dias_uteis_mes1,1) ,
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * total_saida_mes2 / greatest(quantidade_dias_uteis_mes2,1) ,
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * total_saida_mes3 / greatest(quantidade_dias_uteis_mes3,1) ,
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * total_saida_mes4 / greatest(quantidade_dias_uteis_mes4,1) ,
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * total_saida_mes5 / greatest(quantidade_dias_uteis_mes5,1)) as vl_max_ticket_medio_saida_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * -1 * total_saida_mes1 / greatest(quantidade_dias_uteis_mes1,1) ,
			(quantidade_dias_uteis_mes2 >= 15)::int * -1 * total_saida_mes2 / greatest(quantidade_dias_uteis_mes2,1) ,
			(quantidade_dias_uteis_mes3 >= 15)::int * -1 * total_saida_mes3 / greatest(quantidade_dias_uteis_mes3,1) ,
			(quantidade_dias_uteis_mes4 >= 15)::int * -1 * total_saida_mes4 / greatest(quantidade_dias_uteis_mes4,1) ,
			(quantidade_dias_uteis_mes5 >= 15)::int * -1 * total_saida_mes5 / greatest(quantidade_dias_uteis_mes5,1)) as vl_min_ticket_medio_saida_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * -1 * total_saida_mes1 / greatest(quantidade_dias_uteis_mes1,1),0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * -1 * total_saida_mes2 / greatest(quantidade_dias_uteis_mes2,1),0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * -1 * total_saida_mes3 / greatest(quantidade_dias_uteis_mes3,1),0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * -1 * total_saida_mes4 / greatest(quantidade_dias_uteis_mes4,1),0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * -1 * total_saida_mes5 / greatest(quantidade_dias_uteis_mes5,1),0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_ticket_medio_saida_15_du,
	--FLUXO CAIXA PIOR - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_pior_mes1
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_pior_mes2
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_pior_mes3
			when quantidade_dias_uteis_mes4 >= 15 then fluxo_caixa_pior_mes4
			when quantidade_dias_uteis_mes5 >= 15 then fluxo_caixa_pior_mes5
		end as vl_fluxo_caixa_pior_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_pior_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_pior_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_pior_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then fluxo_caixa_pior_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_pior_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_pior_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_pior_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_pior_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_pior_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_pior_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_pior_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_pior_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_pior_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_pior_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_pior_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_pior_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_pior_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_pior_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_pior_mes5,0) as vl_ever_fluxo_caixa_pior_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_pior_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_pior_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_pior_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_pior_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_pior_mes5) as vl_max_fluxo_caixa_pior_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_pior_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_pior_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_pior_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_pior_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_pior_mes5) as vl_min_fluxo_caixa_pior_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_pior_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_pior_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_pior_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_pior_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_pior_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_fluxo_caixa_pior_15_du,
	--FLUXO CAIXA MELHOR - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_melhor_mes1
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_melhor_mes2
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_melhor_mes3
			when quantidade_dias_uteis_mes4 >= 15 then fluxo_caixa_melhor_mes4
			when quantidade_dias_uteis_mes5 >= 15 then fluxo_caixa_melhor_mes5
		end as vl_fluxo_caixa_melhor_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_melhor_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_melhor_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_melhor_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then fluxo_caixa_melhor_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_melhor_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_melhor_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_melhor_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_melhor_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_melhor_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_melhor_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_melhor_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_melhor_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_melhor_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_melhor_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_melhor_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_melhor_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_melhor_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_melhor_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_melhor_mes5,0) as vl_ever_fluxo_caixa_melhor_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_melhor_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_melhor_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_melhor_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_melhor_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_melhor_mes5) as vl_max_fluxo_caixa_melhor_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_melhor_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_melhor_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_melhor_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_melhor_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_melhor_mes5) as vl_min_fluxo_caixa_melhor_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_melhor_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_melhor_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_melhor_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_melhor_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_melhor_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_fluxo_caixa_melhor_15_du,
	--FLUXO DIA MELHOR - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_dia_melhor_mes1
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_dia_melhor_mes2
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_dia_melhor_mes3
			when quantidade_dias_uteis_mes4 >= 15 then fluxo_caixa_dia_melhor_mes4
			when quantidade_dias_uteis_mes5 >= 15 then fluxo_caixa_dia_melhor_mes5
		end as vl_fluxo_caixa_dia_melhor_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_dia_melhor_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_dia_melhor_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_dia_melhor_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then fluxo_caixa_dia_melhor_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_dia_melhor_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_dia_melhor_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_dia_melhor_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_dia_melhor_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_dia_melhor_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_dia_melhor_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_dia_melhor_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_dia_melhor_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_dia_melhor_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_dia_melhor_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_dia_melhor_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_dia_melhor_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_dia_melhor_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_dia_melhor_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_dia_melhor_mes5,0) as vl_ever_fluxo_caixa_dia_melhor_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_dia_melhor_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_dia_melhor_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_dia_melhor_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_dia_melhor_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_dia_melhor_mes5) as vl_max_fluxo_caixa_dia_melhor_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_dia_melhor_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_dia_melhor_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_dia_melhor_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_dia_melhor_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_dia_melhor_mes5) as vl_min_fluxo_caixa_dia_melhor_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_dia_melhor_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_dia_melhor_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_dia_melhor_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_dia_melhor_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_dia_melhor_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_fluxo_caixa_dia_melhor_15_du,
	--FLUXO DIAS NEGATIVO - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_dias_negativo_mes1
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_dias_negativo_mes2
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_dias_negativo_mes3
			when quantidade_dias_uteis_mes4 >= 15 then fluxo_caixa_dias_negativo_mes4
			when quantidade_dias_uteis_mes5 >= 15 then fluxo_caixa_dias_negativo_mes5
		end as vl_fluxo_caixa_dias_negativo_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_dias_negativo_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_dias_negativo_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_dias_negativo_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then fluxo_caixa_dias_negativo_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_dias_negativo_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_dias_negativo_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_dias_negativo_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_dias_negativo_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_dias_negativo_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_dias_negativo_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_dias_negativo_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_dias_negativo_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_dias_negativo_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_dias_negativo_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_dias_negativo_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_dias_negativo_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_dias_negativo_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_dias_negativo_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_dias_negativo_mes5,0) as vl_ever_fluxo_caixa_dias_negativo_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_dias_negativo_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_dias_negativo_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_dias_negativo_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_dias_negativo_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_dias_negativo_mes5) as vl_max_fluxo_caixa_dias_negativo_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_dias_negativo_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_dias_negativo_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_dias_negativo_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_dias_negativo_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_dias_negativo_mes5) as vl_min_fluxo_caixa_dias_negativo_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_dias_negativo_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_dias_negativo_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_dias_negativo_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_dias_negativo_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_dias_negativo_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_fluxo_caixa_dias_negativo_15_du,
	--PERC DIAS NEGATIVO - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_perc_dias_negativo_mes1
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_perc_dias_negativo_mes2
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_perc_dias_negativo_mes3
			when quantidade_dias_uteis_mes4 >= 15 then fluxo_caixa_perc_dias_negativo_mes4
			when quantidade_dias_uteis_mes5 >= 15 then fluxo_caixa_perc_dias_negativo_mes5
		end as vl_fluxo_caixa_perc_dias_negativo_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_perc_dias_negativo_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_perc_dias_negativo_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_perc_dias_negativo_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then fluxo_caixa_perc_dias_negativo_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_perc_dias_negativo_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_perc_dias_negativo_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_perc_dias_negativo_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_perc_dias_negativo_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_perc_dias_negativo_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_perc_dias_negativo_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_perc_dias_negativo_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_perc_dias_negativo_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_perc_dias_negativo_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_perc_dias_negativo_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_perc_dias_negativo_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_perc_dias_negativo_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_perc_dias_negativo_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_perc_dias_negativo_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_perc_dias_negativo_mes5,0) as vl_ever_fluxo_caixa_perc_dias_negativo_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_perc_dias_negativo_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_perc_dias_negativo_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_perc_dias_negativo_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_perc_dias_negativo_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_perc_dias_negativo_mes5) as vl_max_fluxo_caixa_perc_dias_negativo_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_perc_dias_negativo_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_perc_dias_negativo_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_perc_dias_negativo_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_perc_dias_negativo_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_perc_dias_negativo_mes5) as vl_min_fluxo_caixa_perc_dias_negativo_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_perc_dias_negativo_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_perc_dias_negativo_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_perc_dias_negativo_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_perc_dias_negativo_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_perc_dias_negativo_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_fluxo_caixa_perc_dias_negativo_15_du,
	--FLUXO CAIXA SALDO OPERACIONAL - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_saldo_operacional_mes1
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_saldo_operacional_mes2
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_saldo_operacional_mes3
			when quantidade_dias_uteis_mes4 >= 15 then fluxo_caixa_saldo_operacional_mes4
			when quantidade_dias_uteis_mes5 >= 15 then fluxo_caixa_saldo_operacional_mes5
		end as vl_fluxo_caixa_saldo_operacional_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_saldo_operacional_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_saldo_operacional_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_saldo_operacional_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then fluxo_caixa_saldo_operacional_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_saldo_operacional_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_saldo_operacional_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_saldo_operacional_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_saldo_operacional_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_saldo_operacional_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_saldo_operacional_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_saldo_operacional_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_saldo_operacional_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_saldo_operacional_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_saldo_operacional_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_saldo_operacional_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_saldo_operacional_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_saldo_operacional_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_saldo_operacional_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_saldo_operacional_mes5,0) as vl_ever_fluxo_caixa_saldo_operacional_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_saldo_operacional_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_saldo_operacional_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_saldo_operacional_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_saldo_operacional_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_saldo_operacional_mes5) as vl_max_fluxo_caixa_saldo_operacional_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_saldo_operacional_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_saldo_operacional_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_saldo_operacional_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_saldo_operacional_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_saldo_operacional_mes5) as vl_min_fluxo_caixa_saldo_operacional_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_saldo_operacional_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_saldo_operacional_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_saldo_operacional_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_saldo_operacional_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_saldo_operacional_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_fluxo_caixa_saldo_operacional_15_du,
	--FLUXO CAIXA SALDO ATE DIA LEAD - 15 DU
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_saldo_ate_dia_lead_mes1
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_saldo_ate_dia_lead_mes2
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_saldo_ate_dia_lead_mes3
			when quantidade_dias_uteis_mes4 >= 15 then fluxo_caixa_saldo_ate_dia_lead_mes4
			when quantidade_dias_uteis_mes5 >= 15 then fluxo_caixa_saldo_ate_dia_lead_mes5
		end as vl_fluxo_caixa_saldo_ate_dia_lead_m1_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_saldo_ate_dia_lead_mes2 * case when quantidade_dias_uteis_mes2 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_saldo_ate_dia_lead_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_saldo_ate_dia_lead_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes4 >= 15 then fluxo_caixa_saldo_ate_dia_lead_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_saldo_ate_dia_lead_m2_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_saldo_ate_dia_lead_mes3 * case when quantidade_dias_uteis_mes3 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_saldo_ate_dia_lead_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes3 >= 15 then fluxo_caixa_saldo_ate_dia_lead_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_saldo_ate_dia_lead_m3_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_saldo_ate_dia_lead_mes4 * case when quantidade_dias_uteis_mes4 >= 15 then 1 end
			when quantidade_dias_uteis_mes2 >= 15 then fluxo_caixa_saldo_ate_dia_lead_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_saldo_ate_dia_lead_m4_15_du,
		case 
			when quantidade_dias_uteis_mes1 >= 15 then fluxo_caixa_saldo_ate_dia_lead_mes5 * case when quantidade_dias_uteis_mes5 >= 15 then 1 end
		end as vl_fluxo_caixa_saldo_ate_dia_lead_m5_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_saldo_ate_dia_lead_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_saldo_ate_dia_lead_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_saldo_ate_dia_lead_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_saldo_ate_dia_lead_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_saldo_ate_dia_lead_mes5,0) as vl_ever_fluxo_caixa_saldo_ate_dia_lead_15_du,
		greatest((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_saldo_ate_dia_lead_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_saldo_ate_dia_lead_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_saldo_ate_dia_lead_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_saldo_ate_dia_lead_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_saldo_ate_dia_lead_mes5) as vl_max_fluxo_caixa_saldo_ate_dia_lead_15_du,
		least((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_saldo_ate_dia_lead_mes1 ,
			(quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_saldo_ate_dia_lead_mes2 ,
			(quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_saldo_ate_dia_lead_mes3 ,
			(quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_saldo_ate_dia_lead_mes4 ,
			(quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_saldo_ate_dia_lead_mes5) as vl_min_fluxo_caixa_saldo_ate_dia_lead_15_du,
		coalesce((quantidade_dias_uteis_mes1 >= 15)::int * fluxo_caixa_saldo_ate_dia_lead_mes1,0) +
			coalesce((quantidade_dias_uteis_mes2 >= 15)::int * fluxo_caixa_saldo_ate_dia_lead_mes2,0) +
			coalesce((quantidade_dias_uteis_mes3 >= 15)::int * fluxo_caixa_saldo_ate_dia_lead_mes3,0) +
			coalesce((quantidade_dias_uteis_mes4 >= 15)::int * fluxo_caixa_saldo_ate_dia_lead_mes4,0) +
			coalesce((quantidade_dias_uteis_mes5 >= 15)::int * fluxo_caixa_saldo_ate_dia_lead_mes5,0) /
			greatest((coalesce((quantidade_dias_uteis_mes1 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes2 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes3 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes4 >= 15)::int,0) + coalesce((quantidade_dias_uteis_mes5 >= 15)::int,0)),1) as vl_avg_fluxo_caixa_saldo_ate_dia_lead_15_du
	from t_extratos) as t1 on t1.extrato_id = t.id
