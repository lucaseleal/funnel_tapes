--t_get_credit_underwrite_data
insert into lucas_leal.t_get_credit_underwrite_data
(SELECT cp.emprestimo_id,
    count(*) FILTER (WHERE cp.criado_por = 67) AS bizbot,
    count(*) FILTER (WHERE cp.criado_por <> 67) AS nao_bizbot,
    min(cp.criado_em) FILTER (WHERE cp.indice = 1)::date AS data_a1,
    max(regexp_replace(au.first_name::text || '_'::text || au.last_name::text,'\_$','')) FILTER (WHERE cp.indice = 1) AS analista_a1,
    max(cp.status::text) FILTER (WHERE cp.indice = 1) AS status_a1,
    max(cp.confianca) FILTER (WHERE cp.indice = 1) AS nota_a1,
    max(cp.limite) FILTER (WHERE cp.indice = 1) AS valor_a1,
    max(cp.juros) FILTER (WHERE cp.indice = 1)::numeric(10,2) AS taxa_a1,
    max(cp.prazo) FILTER (WHERE cp.indice = 1)::numeric(10,2) AS prazo_a1,
    min(cp.criado_em) FILTER (WHERE cp.indice = 2)::date AS data_a2,
    max((au.first_name::text || '_'::text) || au.last_name::text) FILTER (WHERE cp.indice = 2) AS analista_a2,
    max(cp.status::text) FILTER (WHERE cp.indice = 2) AS status_a2,
    max(cp.confianca) FILTER (WHERE cp.indice = 2) AS nota_a2,
    max(cp.limite) FILTER (WHERE cp.indice = 2) AS valor_a2,
    max(cp.juros) FILTER (WHERE cp.indice = 2)::numeric(10,2) AS taxa_a2,
    max(cp.prazo) FILTER (WHERE cp.indice = 2)::numeric(10,2) AS prazo_a2,
    min(cp.criado_em) FILTER (WHERE cp.indice = 3)::date AS data_a3,
    max((au.first_name::text || '_'::text) || au.last_name::text) FILTER (WHERE cp.indice = 3) AS analista_a3,
    max(cp.status::text) FILTER (WHERE cp.indice = 3) AS status_a3,
    max(cp.confianca) FILTER (WHERE cp.indice = 3) AS nota_a3,
    max(cp.limite) FILTER (WHERE cp.indice = 3) AS valor_a3,
    max(cp.juros) FILTER (WHERE cp.indice = 3)::numeric(10,2) AS taxa_a3,
    max(cp.prazo) FILTER (WHERE cp.indice = 3)::numeric(10,2) AS prazo_a3
   FROM ( SELECT credito_parecer.id,
            credito_parecer.confianca,
            credito_parecer.status,
            credito_parecer.tipo,
            credito_parecer.texto,
            credito_parecer.juros,
            credito_parecer.prazo,
            credito_parecer.criado_em,
            credito_parecer.criado_por,
            credito_parecer.modificado_em,
            credito_parecer.modificado_por,
            credito_parecer.emprestimo_id,
            credito_parecer.limite,
            dense_rank() OVER (PARTITION BY credito_parecer.emprestimo_id ORDER BY (to_timestamp(to_char(credito_parecer.criado_em, 'yyyy-mm-dd HH24:MI:SS'::text), 'yyyy-mm-dd HH24:MI:SS'::text)), credito_parecer.tipo) AS indice
           FROM credito_parecer
           where credito_parecer.emprestimo_id not in (select emprestimo_id from t_get_credit_underwrite_data)) cp
     JOIN loan_requests lr ON lr.loan_request_id = cp.emprestimo_id
     JOIN offers o ON o.offer_id = lr.offer_id
     LEFT JOIN sync_credit_analysis sc ON sc.legacy_target_id = o.direct_prospect_id
     LEFT JOIN public_new.auth_user au ON au.id = cp.criado_por
  where lr.status::text = 'ACCEPTED'::text and lr.contrato_assinado_url is not null AND sc.legacy_target_id IS null and lr.loan_request_id not in (select emprestimo_id from lucas_leal.t_get_credit_underwrite_data)
  group by cp.emprestimo_id
union
	select lr.loan_request_id AS emprestimo_id,
    	0 as bizbot,
    	(((c.data_output -> 'CreditOpinions'::text) -> 0) IS NOT NULL)::integer + (((c.data_output -> 'CreditOpinions'::text) -> 1) IS NOT NULL)::integer + (((c.data_output -> 'CreditOpinions'::text) -> 2) IS NOT NULL)::integer AS nao_bizbot,
        CASE
            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 0) -> 'LastChange'::text) ->> 'Date'::text)::date
            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 1) -> 'LastChange'::text) ->> 'Date'::text)::date
            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 2) -> 'LastChange'::text) ->> 'Date'::text)::date
            ELSE NULL::date
        END AS data_a1,
        CASE
            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'Username'::text
            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'Username'::text
            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'Username'::text
            ELSE NULL::text
        END AS analista_a1,
	    replace(replace(replace(
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'CreditOpinionSuggestionType'::text
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'CreditOpinionSuggestionType'::text
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'CreditOpinionSuggestionType'::text
	            ELSE NULL::text
	        END, 'APPROVE_WITH_CHANGES'::text, 'AprovarAlteracao'::text), 'APPROVE'::text, 'Aprovar'::text), 'REPROVE'::text, 'Rejeitar'::text) AS status_a1,
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'TrustLevel'::text)::integer
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'TrustLevel'::text)::integer
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'TrustLevel'::text)::integer
	            ELSE NULL::integer
	        END AS nota_a1,
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'NewAmount'::text))::numeric(100,2)
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'NewAmount'::text))::numeric(100,2)
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'NewAmount'::text))::numeric(100,2)
	            ELSE NULL::numeric(100,2)
	        END AS valor_a1,
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'NewInterestRate'::text))::numeric(10,2)
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'NewInterestRate'::text))::numeric(10,2)
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'NewInterestRate'::text))::numeric(10,2)
	            ELSE NULL::numeric(10,2)
	        END AS taxa_a1,
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'NewTenure'::text)::integer
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'NewTenure'::text)::integer
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'NewTenure'::text)::integer
	            ELSE NULL::integer
	        END AS prazo_a1,
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 0) -> 'LastChange'::text) ->> 'Date'::text)::date
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 1) -> 'LastChange'::text) ->> 'Date'::text)::date
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 2) -> 'LastChange'::text) ->> 'Date'::text)::date
	            ELSE NULL::date
	        END AS data_a2,
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'Username'::text
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'Username'::text
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'Username'::text
	            ELSE NULL::text
	        END AS analista_a2,
	    replace(replace(replace(
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'CreditOpinionSuggestionType'::text
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'CreditOpinionSuggestionType'::text
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'CreditOpinionSuggestionType'::text
	            ELSE NULL::text
	        END, 'APPROVE_WITH_CHANGES'::text, 'AprovarAlteracao'::text), 'APPROVE'::text, 'Aprovar'::text), 'REPROVE'::text, 'Rejeitar'::text) AS status_a2,
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'TrustLevel'::text)::integer
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'TrustLevel'::text)::integer
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'TrustLevel'::text)::integer
	            ELSE NULL::integer
	        END AS nota_a2,
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'NewAmount'::text))::numeric(100,2)
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'NewAmount'::text))::numeric(100,2)
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'NewAmount'::text))::numeric(100,2)
	            ELSE NULL::numeric(100,2)
	        END AS valor_a2,
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'NewInterestRate'::text))::numeric(10,2)
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'NewInterestRate'::text))::numeric(10,2)
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'NewInterestRate'::text))::numeric(10,2)
	            ELSE NULL::numeric(10,2)
	        END AS taxa_a2,
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'NewTenure'::text)::integer
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'NewTenure'::text)::integer
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'NewTenure'::text)::integer
	            ELSE NULL::integer
	        END AS prazo_a2,
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 0) -> 'LastChange'::text) ->> 'Date'::text)::date
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 1) -> 'LastChange'::text) ->> 'Date'::text)::date
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 2) -> 'LastChange'::text) ->> 'Date'::text)::date
	            ELSE NULL::date
	        END AS data_a3,
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'Username'::text
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'Username'::text
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'Username'::text
	            ELSE NULL::text
	        END AS analista_a3,
	    replace(replace(replace(
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'CreditOpinionSuggestionType'::text
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'CreditOpinionSuggestionType'::text
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'CreditOpinionSuggestionType'::text
	            ELSE NULL::text
	        END, 'APPROVE_WITH_CHANGES'::text, 'AprovarAlteracao'::text), 'APPROVE'::text, 'Aprovar'::text), 'REPROVE'::text, 'Rejeitar'::text) AS status_a3,
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'TrustLevel'::text)::integer
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'TrustLevel'::text)::integer
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'TrustLevel'::text)::integer
	            ELSE NULL::integer
	        END AS nota_a3,
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'NewAmount'::text))::numeric(100,2)
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'NewAmount'::text))::numeric(100,2)
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'NewAmount'::text))::numeric(100,2)
	            ELSE NULL::numeric(100,2)
	        END AS valor_a3,
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'NewInterestRate'::text))::numeric(10,2)
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'NewInterestRate'::text))::numeric(10,2)
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'NewInterestRate'::text))::numeric(10,2)
	            ELSE NULL::numeric(10,2)
	        END AS taxa_a3,
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'NewTenure'::text)::integer
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'NewTenure'::text)::integer
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'NewTenure'::text)::integer
	            ELSE NULL::integer
	        END AS prazo_a3
	FROM (select 
			legacy_target_id,
			data_output,
			row_number() over (partition by legacy_target_id order by id desc) as indice_analise
		from sync_credit_analysis) c
	JOIN offers o ON o.direct_prospect_id = c.legacy_target_id
	JOIN loan_requests lr ON lr.offer_id = o.offer_id
	LEFT JOIN 
	WHERE lr.status::text = 'ACCEPTED'::text and lr.contrato_assinado_url is not null and c.indice_analise = 1 and lr.loan_request_id not in (select emprestimo_id from lucas_leal.t_get_credit_underwrite_data))


--t_get_old_rejection_reasons_data
insert into t_get_old_rejection_reasons_data 
(SELECT lr.loan_request_id,
    count(*) FILTER (WHERE mr.nome::text = 'Complexidade Estrutural'::text) AS "Structure complexity",
    count(*) FILTER (WHERE mr.nome::text = 'Mau Pagamento ao Mercado (Anotações Restritivas na PF e/ou PJ e/ou SCR)'::text) AS "Bad payment record (Experian/SCR)",
    count(*) FILTER (WHERE mr.nome::text = 'Baixo Faturamento Comprovado'::text) AS "Low income (bank stmts)",
    count(*) FILTER (WHERE mr.nome::text = 'Mau Pagamento ao Mercado (Extrato)'::text) AS "Bad payment record (bank stmts)",
    count(*) FILTER (WHERE mr.nome::text = ANY (ARRAY['Política de Crédito (Score Serasa Baixo)'::character varying, 'Política de Crédito (Score Serasa Baixo na análise de Documentação )'::character varying]::text[])) AS "Policy: low Experian score",
    count(*) FILTER (WHERE mr.nome::text = 'Queda do Faturamento'::text) AS "Income descrease",
    count(*) FILTER (WHERE mr.nome::text = 'Suspeita de Fraude'::text) AS "Fraud suspicion",
    count(*) FILTER (WHERE mr.nome::text = 'Política de Crédito (Alteração no Contrato Social)'::text) AS "Policy: social contract change",
    count(*) FILTER (WHERE mr.nome::text = 'Falta de Capacidade de Pagamento'::text) AS "Low margin",
    count(*) FILTER (WHERE mr.nome::text = 'Política de Crédito (Atividade Fim da Empresa)'::text) AS "Policy: company activity",
    count(*) FILTER (WHERE mr.nome::text = 'Política de Crédito (Objetivo do Empréstimo)'::text) AS "Policy: money use",
    count(*) FILTER (WHERE mr.nome::text = 'Negou Informações/Anuência'::text) AS "Info/consent denial",
    count(*) FILTER (WHERE mr.nome::text = 'Endividamento Elevado (Extrato/Crédito Recente)'::text) AS "High indebtness due to recent borrow (bank stmts)",
    count(*) FILTER (WHERE mr.nome::text = 'LT Baixo'::text) AS "Low LT score",
    count(*) FILTER (WHERE mr.nome::text = 'Política de Crédito (Quadro Societário)'::text) AS "Policy: shareholder change",
    count(*) FILTER (WHERE mr.nome::text = 'Endividamento Elevado (Comprovado Baixo Faturamento)'::text) AS "High indebtness due to low income (bank stmts)",
    count(*) FILTER (WHERE mr.nome::text = 'Problemas Com a Justiça'::text) AS "Justice issues",
    count(*) FILTER (WHERE mr.nome::text = 'Empresa não possui conta PJ'::text) AS "No bank account for company",
    count(*) FILTER (WHERE mr.nome::text = 'Documentação Irregular'::text) AS "Documentation issues",
    count(*) FILTER (WHERE mr.nome::text = 'Política de Crédito (Empresa com menos de 1 ano de operação)'::text) AS "Policy: less than 1 yo"
   FROM loan_requests lr
     JOIN loan_requests_motivos_rejeicao lrm ON lrm.loanrequest_id = lr.loan_request_id
     JOIN motivos_rejeicao mr ON mr.id = lrm.motivosrejeicao_id
  WHERE lr.status::text = 'ACCEPTED'::text and lr.loan_request_id not in (select loan_request_id from t_get_old_rejection_reasons_data)
  GROUP BY lr.loan_request_id
)


--t_get_old_checklist_data
insert into t_get_old_checklist_data 
(SELECT t2.emprestimo_id,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Extrato'::text AND t2.campo = 'valor_pj'::text) AS largest_cashflow_company_account,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Extrato'::text AND t2.campo = 'faturamento'::text) AS proved_informed_revenue,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Extrato'::text AND t2.campo = 'empreza_azul'::text) AS positive_balance_account,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Extrato'::text AND t2.campo = 'fluxo_entradas'::text) AS continuous_income,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Extrato'::text AND t2.campo = 'saldo_negativo'::text) AS low_negative_balance,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Extrato'::text AND t2.campo = 'entradas_desvinculadas'::text) AS income_unrelated_government,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Extrato'::text AND t2.campo = 'diversificacao_clientes'::text) AS diversified_clients,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Extrato'::text AND t2.campo = 'somente_despesas_empresa'::text) AS only_company_expenses,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Outros'::text AND t2.campo = 'endereco'::text) AS different_personal_company_address,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Outros'::text AND t2.campo = 'socio_bens'::text) AS partner_has_assets,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Outros'::text AND t2.campo = 'empresa_credito'::text) AS company_usedto_credit,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Outros'::text AND t2.campo = 'solicitou_menos'::text) AS requested_lessthan_offered,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Outros'::text AND t2.campo = 'endividamento_baixo'::text) AS low_personal_debt,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Outros'::text AND t2.campo = 'simulacao_diligente'::text) AS dilligent_credit_simulation,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Empresa'::text AND t2.campo = 'conta_pj'::text) AS company_account_confirmed,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Empresa'::text AND t2.campo = 'contrato_social'::text) AS social_contract_confirmed,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Empresa'::text AND t2.campo = 'extratos_bancarios'::text) AS bank_statements_confirmed,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Empresa'::text AND t2.campo = 'comprovante_endereço'::text) AS company_address_confirmed,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Física'::text AND t2.campo = 'cpf'::text) / min(t1.num_socios)::double precision AS personal_cpf,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Física'::text AND t2.campo = 'identidade'::text) / min(t1.num_socios)::double precision AS personal_rg,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Física'::text AND t2.campo = 'conjuge_info'::text) / min(t1.num_socios)::double precision AS spouse_info,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Física'::text AND t2.campo = 'comprovante_renda'::text) / min(t1.num_socios)::double precision AS proof_income,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = 'Física'::text AND t2.campo = 'comprovante_endereço'::text) / min(t1.num_socios)::double precision AS proof_personal_address,
    sum(t2.pontuacao) FILTER (WHERE t2.tipo::text = ANY (ARRAY['Extrato'::character varying, 'Outros'::character varying]::text[])) AS score_documentation,
    sum(t2.pontuacao) FILTER (WHERE (t2.tipo::text = ANY (ARRAY['Física'::character varying, 'Empresa'::character varying]::text[])) AND (t2.campo <> ALL (ARRAY['anuencia'::text, 'email'::text]))) / (4 + 5 * min(t1.num_socios))::double precision AS percent_documentation
   FROM ( SELECT t1_1.emprestimo_id,
            t1_1.tipo,
            t1_1.campo,
                CASE
                    WHEN t1_1.pontuacao = ''::text THEN NULL::text
                    ELSE t1_1.pontuacao
                END::double precision AS pontuacao
           FROM ( SELECT cc.emprestimo_id,
                    cc.tipo,
                    jsonb_object_keys(cc.formulario) AS campo,
                    (cc.formulario -> jsonb_object_keys(cc.formulario)) ->> 'pontos'::text AS pontuacao
                   FROM credito_checklist cc
                     JOIN loan_requests lr ON lr.loan_request_id = cc.emprestimo_id
                  WHERE cc.emprestimo_id not in (select emprestimo_id from t_get_old_checklist_data)) t1_1) t2 --lr.status::text = 'ACCEPTED'::text and 
     LEFT JOIN ( SELECT cc.emprestimo_id,
            count(*) AS num_socios
           FROM credito_checklist cc
             JOIN loan_requests lr ON lr.loan_request_id = cc.emprestimo_id
          WHERE cc.tipo::text = 'Física'::text and cc.emprestimo_id not in (select emprestimo_id from t_get_old_checklist_data) --(lr.status::text = ANY (ARRAY['ACCEPTED'::character varying, 'REJECTED'::character varying]::text[])) AND 
          GROUP BY cc.emprestimo_id) t1 ON t1.emprestimo_id = t2.emprestimo_id
  GROUP BY t2.emprestimo_id
)
 