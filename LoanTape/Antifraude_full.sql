
insert into t_antifraude_modelo_neuroid_prop
select
	t3.*,
	case 
		when prob > 0.82380 then 'Vermelho'
		when prob > 0.5715279 then 'Amarelo'
		when prob <= 0.5715279 then 'Verde'
	end as cor
from(select 
		t2.*,
		1 / (1 + exp(-total)) as prob
	from(select 
		t1.lead_id,
		t1.prob_neuro_id,
		t1.prob_prop,
		t1.woe_neuro_id,
		t1.woe_prop,
		-0.44598 + 0.87250234 * woe_neuro_id + 1.37227295 * woe_prop as total
	from (select 
			t1.origem_id as lead_id,
			(t1.dados -> 'valores' ->> 0)::numeric as prob_neuro_id,
			(t1.dados -> 'valores' ->> 1)::numeric as prob_prop,
			(t1.dados -> 'valores_bin' ->> 0)::numeric as woe_neuro_id,
			(t1.dados -> 'valores_bin' ->> 1)::numeric as woe_prop,
			row_number() over (partition by origem_id order by id desc) as indice
		from backtests.modelos_log t1
--		left join t_antifraude_modelo_neuroid_prop t2 on t2.lead_id = t1.origem_id 
		where t1.modelo = 'neuroid_prop'
--			and t2.lead_id is null
			) as t1
	where indice = 1) as t2) as t3

alter table t_antifraude_modelo_neuroid_prop add column id serial primary key
	

