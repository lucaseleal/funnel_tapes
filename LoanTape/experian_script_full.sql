CREATE INDEX t_experian_choose_report_full_lead_id ON lucas_leal.t_experian_choose_report_full USING btree (direct_prospect_id);
CREATE INDEX t_experian_choose_report_full_coleta_id ON lucas_leal.t_experian_choose_report_full USING btree (id);
--t_experian_choose_report_full
insert into lucas_leal.t_experian_choose_report_full
(select dp.direct_prospect_id,
    	cc.id,
    	dp.cnpj,
    	dp.cpf,
		row_number() over (partition by dp.direct_prospect_id order by to_timestamp((((cc.data -> 'datetime'::text) ->> 'data'::text) || ' '::text) || ((cc.data -> 'datetime'::text) ->> 'hora'::text), 'yyyymmdd HH24:MI:SS'::text) desc, cc.id desc) as indice_ultimo_antes_data_referencia,
    	cc.tipo
	from credito_coleta cc
	join direct_prospects dp ON "left"(cc.documento::text, 8) = "left"(dp.cnpj::text, 8)
	join lucas_leal.v_funil_completo_sem_duplicacao vfcsd on vfcsd.direct_prospect_id = dp.direct_prospect_id 
    left join offers o on o.offer_id = vfcsd.offer_id 
    left join loan_requests lr on lr.loan_request_id = vfcsd.loan_request_id
	left join (select distinct direct_prospect_id from lucas_leal.t_experian_choose_report_full) tspcrf on tspcrf.direct_prospect_id = dp.direct_prospect_id 
	where cc.documento_tipo::text = 'CNPJ'::text 
		and cc.tipo::text in ('Serasa','RelatoSocios','RelatoBasico')
		and to_date((cc.data -> 'datetime'::text) ->> 'data'::text, 'yyyymmdd'::text) <= coalesce(lr.loan_date,coalesce(lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '10 days')::date
		and tspcrf.direct_prospect_id is null
	)
	
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
CREATE unique INDEX t_experian_get_company_negative_public_record_data_full_lead_id ON lucas_leal.t_experian_get_company_negative_public_record_data_full USING btree (direct_prospect_id);
--t_experian_get_company_negative_public_record_data_full
insert into lucas_leal.t_experian_get_company_negative_public_record_data_full
(select
	coalesce(t1.direct_prospect_id,t3.direct_prospect_id) as direct_prospect_id,
	coalesce(t3.empresa_divida_valor,0) as empresa_divida_valor,
	coalesce(t3.empresa_ccf_valor,0) as empresa_ccf_valor,
	coalesce(t3.empresa_protesto_valor,0) as empresa_protesto_valor,
	coalesce(t3.empresa_acao_valor,0) as empresa_acao_valor,
	coalesce(t3.empresa_pefin_valor,0) as empresa_pefin_valor,
	coalesce(t3.empresa_refin_valor,0) as empresa_refin_valor,
	coalesce(t3.empresa_spc_valor,0) as empresa_spc_valor,
	coalesce(t3.empresa_divida_unit,0) as empresa_divida_unit,
	coalesce(t3.empresa_ccf_unit,0) as empresa_ccf_unit,
	coalesce(t3.empresa_protesto_unit,0) as empresa_protesto_unit,
	coalesce(t3.empresa_acao_unit,0) as empresa_acao_unit,
	coalesce(t3.empresa_pefin_unit,0) as empresa_pefin_unit,
	coalesce(t3.empresa_refin_unit,0) as empresa_refin_unit,
	coalesce(t3.empresa_spc_unit,0) as empresa_spc_unit,
	coalesce(t3.socio_divida_valor,0) as socio_divida_valor,
	coalesce(t3.socio_ccf_valor,0) as socio_ccf_valor,
	coalesce(t3.socio_acao_valor,0) as socio_acao_valor,
	coalesce(t3.socio_pefin_valor,0) as socio_pefin_valor,
	coalesce(t3.socio_protesto_valor,0) as socio_protesto_valor,
	coalesce(t3.socio_refin_valor,0) as socio_refin_valor,
	coalesce(t3.socio_spc_valor,0) as socio_spc_valor,
	coalesce(t3.socio_divida_unit,0) as socio_divida_unit,
	coalesce(t3.socio_ccf_unit,0) as socio_ccf_unit,
	coalesce(t3.socio_protesto_unit,0) as socio_protesto_unit,
	coalesce(t3.socio_acao_unit,0) as socio_acao_unit,
	coalesce(t3.socio_pefin_unit,0) as socio_pefin_unit,
	coalesce(t3.socio_refin_unit,0) as socio_refin_unit,
	coalesce(t3.socio_spc_unit,0) as socio_spc_unit,
	coalesce(t3.empresa_divida_valor + t3.empresa_ccf_valor + t3.empresa_protesto_valor + t3.empresa_acao_valor + t3.empresa_pefin_valor + t3.empresa_refin_valor + t3.empresa_spc_valor,0) AS empresa_total_valor,
    coalesce(t3.empresa_divida_unit + t3.empresa_ccf_unit + t3.empresa_protesto_unit + t3.empresa_acao_unit + t3.empresa_pefin_unit + t3.empresa_refin_unit + t3.empresa_spc_unit,0) AS empresa_total_unit,
	coalesce(t3.socio_divida_valor + t3.socio_ccf_valor + t3.socio_protesto_valor + t3.socio_acao_valor + t3.socio_pefin_valor + t3.socio_refin_valor + t3.socio_spc_valor,0) AS socios_total_valor,
    coalesce(t3.socio_divida_unit + t3.socio_ccf_unit + t3.socio_protesto_unit + t3.socio_acao_unit + t3.socio_pefin_unit + t3.socio_refin_unit + t3.socio_spc_unit,0) AS socios_total_unit,
	coalesce((t3.empresa_divida_valor + t3.empresa_ccf_valor + t3.empresa_protesto_valor + t3.empresa_acao_valor + t3.empresa_pefin_valor + t3.empresa_refin_valor + t3.empresa_spc_valor) + (t3.socio_divida_valor + t3.socio_ccf_valor + t3.socio_protesto_valor + t3.socio_acao_valor + t3.socio_pefin_valor + t3.socio_refin_valor + t3.socio_spc_valor),0) AS all_valor,
    coalesce((t3.empresa_divida_unit + t3.empresa_ccf_unit + t3.empresa_protesto_unit + t3.empresa_acao_unit + t3.empresa_pefin_unit + t3.empresa_refin_unit + t3.empresa_spc_unit) + (t3.socio_divida_unit + t3.socio_ccf_unit + t3.socio_protesto_unit + t3.socio_acao_unit + t3.socio_pefin_unit + t3.socio_refin_unit + t3.socio_spc_unit),0) AS all_unit
from lucas_leal.t_experian_choose_report_full t1
left join (select distinct direct_prospect_id from lucas_leal.t_experian_get_company_negative_public_record_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
full outer join(select coalesce(spc.direct_prospect_id,serasa.direct_prospect_id) as direct_prospect_id,
		    coalesce(sum(serasa.divida_valor) filter (where spc.nome = 'empresa'::text), 0::double precision) as empresa_divida_valor,
		    coalesce(sum(serasa.ccf_valor) filter (where spc.nome = 'empresa'::text), 0::double precision) as empresa_ccf_valor,
		    coalesce(sum(serasa.protesto_valor) filter (where spc.nome = 'empresa'::text), 0::double precision) as empresa_protesto_valor,
		    coalesce(sum(serasa.acao_valor) filter (where spc.nome = 'empresa'::text), 0::double precision) as empresa_acao_valor,
		    coalesce(sum(serasa.pefin_valor) filter (where spc.nome = 'empresa'::text), 0::double precision) as empresa_pefin_valor,
		    coalesce(sum(serasa.refin_valor) filter (where spc.nome = 'empresa'::text), 0::double precision) as empresa_refin_valor,
		    coalesce(sum(spc.spc_valor) filter (where spc.nome = 'empresa'::text), 0::double precision) as empresa_spc_valor,
		    coalesce(sum(serasa.divida_unit) filter (where spc.nome = 'empresa'::text), 0::double precision) as empresa_divida_unit,
		    coalesce(sum(serasa.ccf_unit) filter (where spc.nome = 'empresa'::text), 0::double precision) as empresa_ccf_unit,
		    coalesce(sum(serasa.protesto_unit) filter (where spc.nome = 'empresa'::text), 0::double precision) as empresa_protesto_unit,
		    coalesce(sum(serasa.acao_unit) filter (where spc.nome = 'empresa'::text), 0::double precision) as empresa_acao_unit,
		    coalesce(sum(serasa.pefin_unit) filter (where spc.nome = 'empresa'::text), 0::double precision) as empresa_pefin_unit,
		    coalesce(sum(serasa.refin_unit) filter (where spc.nome = 'empresa'::text), 0::double precision) as empresa_refin_unit,
		    coalesce(sum(spc.spc_unit) filter (where spc.nome = 'empresa'::text), 0::double precision) as empresa_spc_unit,
		    coalesce(sum(serasa.divida_valor) filter (where spc.spc_share is not null), 0::double precision) as socio_divida_valor,
		    coalesce(sum(serasa.ccf_valor) filter (where spc.spc_share is not null), 0::double precision) as socio_ccf_valor,
		    coalesce(sum(serasa.acao_valor) filter (where spc.spc_share is not null), 0::double precision) as socio_acao_valor,
		    coalesce(sum(serasa.pefin_valor) filter (where spc.spc_share is not null), 0::double precision) as socio_pefin_valor,
		    coalesce(sum(serasa.protesto_valor) filter (where spc.spc_share is not null), 0::double precision) as socio_protesto_valor,
		    coalesce(sum(serasa.refin_valor) filter (where spc.spc_share is not null), 0::double precision) as socio_refin_valor,
		    coalesce(sum(spc.spc_valor) filter (where spc.spc_share is not null), 0::double precision) as socio_spc_valor,
		    coalesce(sum(serasa.divida_unit) filter (where spc.spc_share is not null), 0::double precision) as socio_divida_unit,
		    coalesce(sum(serasa.ccf_unit) filter (where spc.spc_share is not null), 0::double precision) as socio_ccf_unit,
		    coalesce(sum(serasa.protesto_unit) filter (where spc.spc_share is not null), 0::double precision) as socio_protesto_unit,
		    coalesce(sum(serasa.acao_unit) filter (where spc.spc_share is not null), 0::double precision) as socio_acao_unit,
		    coalesce(sum(serasa.pefin_unit) filter (where spc.spc_share is not null), 0::double precision) as socio_pefin_unit,
		    coalesce(sum(serasa.refin_unit) filter (where spc.spc_share is not null), 0::double precision) as socio_refin_unit,
		    coalesce(sum(spc.spc_unit) filter (where spc.spc_share is not null), 0::double precision) as socio_spc_unit
		from ( select socios_share.direct_prospect_id,
		        socios_share.nome,
		        socios_share.spc_share,
		        case
		            when coalesce(spc_socios.spc_socios_valor, '0'::text) = ''::text then '0'::text
		            else spc_socios.spc_socios_valor
		        end::double precision as spc_valor,
		        case
		            when coalesce(spc_socios.spc_socios_unit, '0'::text) = ''::text then '0'::text
		            else spc_socios.spc_socios_unit
		        end::integer as spc_unit
			from ( select t1.direct_prospect_id,
					jsonb_object_keys(cc.data -> 'acionistas'::text) as nome,
					((((cc.data -> 'acionistas'::text) -> jsonb_object_keys(cc.data -> 'acionistas'::text)) ->> 'percentual'::text)::double precision) / 100::double precision as spc_share
				from credito_coleta cc
				join lucas_leal.t_experian_choose_report_full t1 on cc.id = t1.id 
				left join (select distinct direct_prospect_id from lucas_leal.t_experian_get_company_negative_public_record_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
				where t1.indice_ultimo_antes_data_referencia = 1
					and t2.direct_prospect_id is null 
	 				) socios_share
			left join ( select t1.direct_prospect_id,
		        	((cc.data -> 'spc'::text) -> jsonb_object_keys(cc.data -> 'spc'::text)) ->> 'nome'::text as spc_nome,
		            ((cc.data -> 'spc'::text) -> jsonb_object_keys(cc.data -> 'spc'::text)) ->> 'valor'::text as spc_socios_valor,
		            ((cc.data -> 'spc'::text) -> jsonb_object_keys(cc.data -> 'spc'::text)) ->> 'quantidade'::text as spc_socios_unit
	            from credito_coleta cc
				join lucas_leal.t_experian_choose_report_full t1 on cc.id = t1.id
				left join (select distinct direct_prospect_id from lucas_leal.t_experian_get_company_negative_public_record_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
	            where t1.indice_ultimo_antes_data_referencia = 1
	            	and t2.direct_prospect_id is null
	            ) spc_socios ON spc_socios.direct_prospect_id = socios_share.direct_prospect_id AND spc_socios.spc_nome = socios_share.nome
	UNION
	    SELECT t2.direct_prospect_id,
				'empresa'::text AS nome,
				NULL::double precision AS spc_share,
		        sum(COALESCE(t2.spc_valor, 0::double precision)) AS spc_valor,
		        sum(COALESCE(t2.spc_unit, 0::double precision)) AS spc_unit
		FROM ( SELECT t1.direct_prospect_id,
				((cc.data -> 'anotacao_spc'::text) ->> 'total_debito'::text)::double precision AS spc_valor,
	            ((cc.data -> 'anotacao_spc'::text) ->> 'total_ocr'::text)::double precision AS spc_unit
	    	FROM credito_coleta cc
			JOIN lucas_leal.t_experian_choose_report_full t1 on cc.id = t1.id
			left join (select distinct direct_prospect_id from lucas_leal.t_experian_get_company_negative_public_record_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
	        where t1.indice_ultimo_antes_data_referencia = 1
	        	and t2.direct_prospect_id is null
	        ) t2 
		GROUP BY t2.direct_prospect_id) spc
	full outer JOIN ( SELECT t1.direct_prospect_id,
	            jsonb_object_keys(cc.data -> 'anotacoes'::text) AS serasa_nome,
	            (((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Resumo'::text) -> 'DIVIDA_VENCIDA'::text) ->> 'valor'::text)::double precision AS divida_valor,
	            (((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Resumo'::text) -> 'CHEQUE'::text) ->> 'valor'::text)::double precision AS ccf_valor,
	            (((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Resumo'::text) -> 'PROTESTO'::text) ->> 'valor'::text)::double precision AS protesto_valor,
	            (((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Resumo'::text) -> 'ACAO_JUDICIAL'::text) ->> 'valor'::text)::double precision AS acao_valor,
	            ((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Pefin'::text) ->> 'valor_total'::text)::double precision AS pefin_valor,
	            ((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Refin'::text) ->> 'valor_total'::text)::double precision AS refin_valor,
	            (((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Resumo'::text) -> 'DIVIDA_VENCIDA'::text) ->> 'quantidade'::text)::double precision AS divida_unit,
	            (((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Resumo'::text) -> 'CHEQUE'::text) ->> 'quantidade'::text)::double precision AS ccf_unit,
	            (((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Resumo'::text) -> 'PROTESTO'::text) ->> 'quantidade'::text)::double precision AS protesto_unit,
	            (((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Resumo'::text) -> 'ACAO_JUDICIAL'::text) ->> 'quantidade'::text)::double precision AS acao_unit,
	            ((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Pefin'::text) ->> 'quantidade'::text)::double precision AS pefin_unit,
	            ((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Refin'::text) ->> 'quantidade'::text)::double precision AS refin_unit
			FROM credito_coleta cc
	        JOIN lucas_leal.t_experian_choose_report_full t1 ON t1.id = cc.id
			left join (select distinct direct_prospect_id from lucas_leal.t_experian_get_company_negative_public_record_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
	        where t1.indice_ultimo_antes_data_referencia = 1
	        	and t2.direct_prospect_id is null
	    UNION
			SELECT lead_id AS direct_prospect_id,
					'empresa'::text AS serasa_nome,
		            experian_debt_overdue_value AS divida_valor,
		            experian_bad_check_value AS ccf_valor,
		            experian_registry_protest_value AS protesto_valor,
		            experian_legal_action_value AS acao_valor,
		            0 AS pefin_valor,
		            0 AS pefin_valor,
		            experian_debt_overdue_unit AS divida_unit,
		            experian_bad_check_unit AS ccf_unit,
		            experian_registry_protest_unit AS protesto_unit,
		            experian_legal_action_unit AS acao_unit,
		            0 AS pefin_unit,
		            0 AS refin_unit
	           FROM lucas_leal.serasas_antigos_parseados t1
				left join (select distinct direct_prospect_id from lucas_leal.t_experian_get_company_negative_public_record_data_full) t2 on t2.direct_prospect_id = t1.lead_id 
	           where t2.direct_prospect_id is null
	           ) serasa ON serasa.direct_prospect_id = spc.direct_prospect_id AND spc.nome = serasa.serasa_nome
	GROUP BY 1) as t3 on t3.direct_prospect_id = t1.direct_prospect_id
where t1.indice_ultimo_antes_data_referencia = 1
	and t2.direct_prospect_id is null
)


select count(*),count(distinct t1.direct_prospect_id)
from lucas_leal.t_experian_choose_report_full t1 
left join (select distinct direct_prospect_id from lucas_leal.t_experian_get_company_negative_public_record_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and t2.direct_prospect_id is null

select count(*),count(distinct direct_prospect_id)
from lucas_leal.t_experian_get_company_negative_public_record_data_full

select t1.direct_prospect_id 
from lucas_leal.t_experian_choose_report_full t1 
left join (select distinct direct_prospect_id from lucas_leal.t_experian_get_company_negative_public_record_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and t2.direct_prospect_id is null


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE INDEX t_experian_coligadas_full_cnpj ON lucas_leal.t_experian_coligadas_full USING btree (coligadas_cnpj);
--t_experian_coligadas_full
insert into lucas_leal.t_experian_coligadas_full
select t1.direct_prospect_id,
	((cc.data -> 'participacoes'::text) -> jsonb_object_keys(cc.data -> 'participacoes'::text)) ->> 'cnpj'::text AS coligadas_cnpj,
	((cc.data -> 'participacoes'::text) -> jsonb_object_keys(cc.data -> 'participacoes'::text)) ->> 'restricao'::text AS coligadas_restricao
from credito_coleta cc
join t_experian_choose_report_full t1 ON t1.id = cc.id
left join (select distinct direct_prospect_id from lucas_leal.t_experian_coligadas_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and t2.direct_prospect_id is null


select count(*),count(distinct t1.direct_prospect_id)
from lucas_leal.t_experian_choose_report_full t1 
left join (select distinct direct_prospect_id from lucas_leal.t_experian_coligadas_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and t2.direct_prospect_id is null

select count(*),count(distinct direct_prospect_id)
from lucas_leal.t_experian_coligadas_full

select t1.direct_prospect_id 
from lucas_leal.t_experian_choose_report_full t1 
left join (select distinct direct_prospect_id from lucas_leal.t_experian_coligadas_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and t2.direct_prospect_id is null

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE unique INDEX t_experian_get_related_companies_data_full_lead_id ON lucas_leal.t_experian_get_related_companies_data_full USING btree (direct_prospect_id);
--t_experian_get_related_companies_data_full
insert into t_experian_get_related_companies_data_full

(SELECT t1.direct_prospect_id,
		count(*) FILTER (WHERE t2.coligadas_restricao = 'S'::text) AS empresas_problema
	from t_experian_choose_report_full t1
	left join lucas_leal.t_experian_coligadas_full t2 on t2.direct_prospect_id = t1.direct_prospect_id
	left join lucas_leal.t_experian_get_related_companies_data_full t3 on t3.direct_prospect_id = t1.direct_prospect_id 
	where t1.indice_ultimo_antes_data_referencia = 1
		and t3.direct_prospect_id is null
	GROUP BY t1.direct_prospect_id)


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
CREATE unique INDEX t_experian_get_score_full_lead_id ON lucas_leal.t_experian_get_score_full USING btree (direct_prospect_id);
--t_experian_get_score_full
insert into t_experian_get_score_full
(SELECT t1.direct_prospect_id,
		(cc.data ->> 'score'::text)::double precision AS serasa_coleta,
	    (cc.data ->> 'pd'::text)::double precision AS pd_serasa
	FROM credito_coleta cc
    JOIN t_experian_choose_report_full t1 ON cc.id = t1.id
	left join t_experian_get_score_full t2 on t2.direct_prospect_id = t1.direct_prospect_id 
	where t1.indice_ultimo_antes_data_referencia = 1
		and t2.direct_prospect_id is null
	)	
	

select t1.direct_prospect_id 
from lucas_leal.t_experian_choose_report_full t1 
left join t_experian_get_score_full t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and t2.direct_prospect_id is null
	
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


CREATE INDEX t_experian_get_shareholders_data_full_lead_id ON lucas_leal.t_experian_get_shareholders_data_full USING btree (direct_prospect_id);
--t_experian_get_shareholders_data
insert into lucas_leal.t_experian_get_shareholders_data_full
(select t1.direct_prospect_id,
		t1.cpf,
		((cc.data -> 'acionistas'::text) -> jsonb_object_keys(cc.data -> 'acionistas'::text)) ->> 'cpf'::text as cpf_socios,
	    jsonb_object_keys(cc.data -> 'acionistas'::text) as nome,
	    (((cc.data -> 'acionistas'::text) -> jsonb_object_keys(cc.data -> 'acionistas'::text)) ->> 'percentual'::text)::numeric as share
	from credito_coleta cc
	join lucas_leal.t_experian_choose_report_full t1 on t1.id = cc.id
	left join (select distinct direct_prospect_id from lucas_leal.t_experian_get_shareholders_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
	where t1.indice_ultimo_antes_data_referencia = 1
		and t2.direct_prospect_id is null
	)

select count(*),count(distinct direct_prospect_id)
from t_experian_get_shareholders_data_full



---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE unique INDEX t_experian_get_serasa_inquiries_data_full_lead_id ON lucas_leal.t_experian_get_serasa_inquiries_data_full USING btree (direct_prospect_id);
--t_experian_get_serasa_inquiries_data
insert into lucas_leal.t_experian_get_serasa_inquiries_data_full
(select
	coalesce(t1.direct_prospect_id,serasa.direct_prospect_id,spc.direct_prospect_id) as direct_prospect_id,
	coalesce(serasa.consultas_serasa_atual,0) + coalesce(spc.consultas_spc_atual,0) as consultas_serasa_atual,
	coalesce(serasa.consultas_serasa_1m,0) + coalesce(spc.consultas_spc_1m,0) as consultas_1m,
	coalesce(serasa.consultas_serasa_2m,0) + coalesce(spc.consultas_spc_2m,0) as consultas_2m,
	coalesce(serasa.consultas_serasa_3m,0) + coalesce(spc.consultas_spc_3m,0) as consultas_3m,
	coalesce(serasa.consultas_serasa_4m,0) + coalesce(spc.consultas_spc_4m,0) as consultas_4m,
	coalesce(serasa.consultas_serasa_5m,0) + coalesce(spc.consultas_spc_5m,0) as consultas_5m,
	coalesce(serasa.consultas_serasa_6m,0) + coalesce(spc.consultas_spc_6m,0) as consultas_6m,
	coalesce(serasa.consultas_serasa_7m,0) + coalesce(spc.consultas_spc_7m,0) as consultas_7m,
	coalesce(serasa.consultas_serasa_8m,0) + coalesce(spc.consultas_spc_8m,0) as consultas_8m,
	coalesce(serasa.consultas_serasa_9m,0) + coalesce(spc.consultas_spc_9m,0) as consultas_9m,
	coalesce(serasa.consultas_serasa_10m,0) + coalesce(spc.consultas_spc_10m,0) as consultas_10m,
	coalesce(serasa.consultas_serasa_11m,0) + coalesce(spc.consultas_spc_11m,0) as consultas_11m
from lucas_leal.t_experian_choose_report_full t1
left join lucas_leal.t_experian_get_serasa_inquiries_data_full t2 on t2.direct_prospect_id = t1.direct_prospect_id 
full outer join(SELECT t2.direct_prospect_id,
	    sum(CASE
	            WHEN t2.serasa = ''::text THEN '0'::text
	            ELSE COALESCE(t2.serasa, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = t2.data_base) AS consultas_serasa_atual,
	    sum(
	        CASE
	            WHEN t2.serasa = ''::text THEN '0'::text
	            ELSE COALESCE(t2.serasa, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '1 mon'::interval)) AS consultas_serasa_1m,
	    sum(
	        CASE
	            WHEN t2.serasa = ''::text THEN '0'::text
	            ELSE COALESCE(t2.serasa, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '2 mons'::interval)) AS consultas_serasa_2m,
	    sum(
	        CASE
	            WHEN t2.serasa = ''::text THEN '0'::text
	            ELSE COALESCE(t2.serasa, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '3 mons'::interval)) AS consultas_serasa_3m,
	    sum(
	        CASE
	            WHEN t2.serasa = ''::text THEN '0'::text
	            ELSE COALESCE(t2.serasa, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '4 mons'::interval)) AS consultas_serasa_4m,
	    sum(
	        CASE
	            WHEN t2.serasa = ''::text THEN '0'::text
	            ELSE COALESCE(t2.serasa, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '5 mons'::interval)) AS consultas_serasa_5m,
	    sum(
	        CASE
	            WHEN t2.serasa = ''::text THEN '0'::text
	            ELSE COALESCE(t2.serasa, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '6 mons'::interval)) AS consultas_serasa_6m,
	    sum(
	        CASE
	            WHEN t2.serasa = ''::text THEN '0'::text
	            ELSE COALESCE(t2.serasa, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '7 mons'::interval)) AS consultas_serasa_7m,
	    sum(
	        CASE
	            WHEN t2.serasa = ''::text THEN '0'::text
	            ELSE COALESCE(t2.serasa, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '8 mons'::interval)) AS consultas_serasa_8m,
	    sum(
	        CASE
	            WHEN t2.serasa = ''::text THEN '0'::text
	            ELSE COALESCE(t2.serasa, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '9 mons'::interval)) AS consultas_serasa_9m,
	    sum(
	        CASE
	            WHEN t2.serasa = ''::text THEN '0'::text
	            ELSE COALESCE(t2.serasa, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '10 mons'::interval)) AS consultas_serasa_10m,
	    sum(
	        CASE
	            WHEN t2.serasa = ''::text THEN '0'::text
	            ELSE COALESCE(t2.serasa, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '11 mons'::interval)) AS consultas_serasa_11m
	FROM ( SELECT t1.direct_prospect_id,
            to_date(to_char(to_date((cc.data -> 'datetime'::text) ->> 'data'::text, 'yyyymmdd'::text)::timestamp with time zone, 'Mon/yy'::text), 'Mon/yy'::text) AS data_base,
            jsonb_object_keys(cc.data -> 'consultas'::text) AS mes,
            ((cc.data -> 'consultas'::text) -> jsonb_object_keys(cc.data -> 'consultas'::text)) ->> 'pesquisaBancos'::text AS serasa
		FROM credito_coleta cc
        JOIN t_experian_choose_report_full t1 ON t1.id = cc.id
		left join t_experian_get_serasa_inquiries_data_full t2 on t2.direct_prospect_id = t1.direct_prospect_id 
        where t1.indice_ultimo_antes_data_referencia = 1
        	and t2.direct_prospect_id is null
        ) as t2
	GROUP BY t2.direct_prospect_id
	) as serasa on serasa.direct_prospect_id = t1.direct_prospect_id
full outer join (SELECT t2.direct_prospect_id,
	    sum(
	        CASE
	            WHEN t2.spc = ''::text THEN '0'::text
	            ELSE COALESCE(t2.spc, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = t2.data_base) AS consultas_spc_atual,
	    sum(
	        CASE
	            WHEN t2.spc = ''::text THEN '0'::text
	            ELSE COALESCE(t2.spc, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '1 mon'::interval)) AS consultas_spc_1m,
	    sum(
	        CASE
	            WHEN t2.spc = ''::text THEN '0'::text
	            ELSE COALESCE(t2.spc, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '2 mons'::interval)) AS consultas_spc_2m,
	    sum(
	        CASE
	            WHEN t2.spc = ''::text THEN '0'::text
	            ELSE COALESCE(t2.spc, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '3 mons'::interval)) AS consultas_spc_3m,
	    sum(
	        CASE
	            WHEN t2.spc = ''::text THEN '0'::text
	            ELSE COALESCE(t2.spc, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '4 mons'::interval)) AS consultas_spc_4m,
	    sum(
	        CASE
	            WHEN t2.spc = ''::text THEN '0'::text
	            ELSE COALESCE(t2.spc, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '5 mons'::interval)) AS consultas_spc_5m,
	    sum(
	        CASE
	            WHEN t2.spc = ''::text THEN '0'::text
	            ELSE COALESCE(t2.spc, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '6 mons'::interval)) AS consultas_spc_6m,
	    sum(
	        CASE
	            WHEN t2.spc = ''::text THEN '0'::text
	            ELSE COALESCE(t2.spc, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '7 mons'::interval)) AS consultas_spc_7m,
	    sum(
	        CASE
	            WHEN t2.spc = ''::text THEN '0'::text
	            ELSE COALESCE(t2.spc, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '8 mons'::interval)) AS consultas_spc_8m,
	    sum(
	        CASE
	            WHEN t2.spc = ''::text THEN '0'::text
	            ELSE COALESCE(t2.spc, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '9 mons'::interval)) AS consultas_spc_9m,
	    sum(
	        CASE
	            WHEN t2.spc = ''::text THEN '0'::text
	            ELSE COALESCE(t2.spc, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '10 mons'::interval)) AS consultas_spc_10m,
	    sum(
	        CASE
	            WHEN t2.spc = ''::text THEN '0'::text
	            ELSE COALESCE(t2.spc, '0'::text)
	        END::integer) FILTER (WHERE to_date(t2.mes,case when right(t2.mes,2)::int > 12 then 'mm/yy' else 'yy/mm' end) = (t2.data_base - '11 mons'::interval)) AS consultas_spc_11m
	FROM ( SELECT t1.direct_prospect_id,
			to_date(to_char(to_date((cc.data -> 'datetime'::text) ->> 'data'::text, 'yyyymmdd'::text)::timestamp with time zone, 'Mon/yy'::text), 'Mon/yy'::text) AS data_base,
	        jsonb_object_keys(cc.data -> 'consultas'::text) AS mes,
	        replace(((cc.data -> 'registro_spc'::text) -> jsonb_object_keys(cc.data -> 'registro_spc'::text)) ->> 'quantidade'::text, ' '::text, ''::text) AS spc
        FROM credito_coleta cc
		JOIN t_experian_choose_report_full t1 ON cc.id = t1.id
		left join t_experian_get_serasa_inquiries_data_full t2 on t2.direct_prospect_id = t1.direct_prospect_id 
        where t1.indice_ultimo_antes_data_referencia = 1
        	and t2.direct_prospect_id is null
        	) t2 
	GROUP BY t2.direct_prospect_id
--UNION
--	SELECT 
--	 	lead_id AS direct_prospect_id,
--	    experian_inquiries_curr AS consultas_spc_atual,
--	    experian_inquiries_2m AS consultas_spc_1m,
--	    experian_inquiries_3m AS consultas_spc_2m,
--	    experian_inquiries_4m AS consultas_spc_3m,
--	    experian_inquiries_5m AS consultas_spc_4m,
--	    experian_inquiries_6m AS consultas_spc_5m,
--	    experian_inquiries_7m AS consultas_spc_6m,
--	    experian_inquiries_8m AS consultas_spc_7m,
--	    experian_inquiries_9m AS consultas_spc_8m,
--	    experian_inquiries_10m AS consultas_spc_9m,
--	    experian_inquiries_11m AS consultas_spc_10m,
--	    experian_inquiries_12m AS consultas_spc_11m
--	FROM serasas_antigos_parseados t1
--	left join t_experian_get_serasa_inquiries_data_full t2 on t2.direct_prospect_id = t1.lead_id 
--	where t2.lead_id is null
	) as spc on spc.direct_prospect_id = t1.direct_prospect_id
where t1.indice_ultimo_antes_data_referencia = 1
	and t2.direct_prospect_id is null
)

select count(*),count(distinct direct_prospect_id)
from t_experian_get_serasa_inquiries_data_full

select count(*),count(distinct t1.direct_prospect_id)
from t_experian_choose_report_full t1 
left join t_experian_get_serasa_inquiries_data_full t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and t2.direct_prospect_id is null

	
select t1.direct_prospect_id 
from lucas_leal.t_experian_choose_report_full t1 
left join t_experian_get_serasa_inquiries_data_full t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and t2.direct_prospect_id is null	
	
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


CREATE INDEX t_experian_controle_adm_full_controle_adm_cpf ON lucas_leal.t_experian_controle_adm_full USING btree (controle_adm_cpf);
--t_experian_controle_adm_full
insert into lucas_leal.t_experian_controle_adm_full
select t1.direct_prospect_id,
	((cc.data -> 'controle_adm'::text -> 'adms') -> jsonb_object_keys(cc.data -> 'controle_adm'::text -> 'adms')) ->> 'cpf'::text AS controle_adm_cpf,
	((cc.data -> 'controle_adm'::text -> 'adms') -> jsonb_object_keys(cc.data -> 'controle_adm'::text -> 'adms')) ->> 'pais'::text AS controle_adm_pais,
	((cc.data -> 'controle_adm'::text -> 'adms') -> jsonb_object_keys(cc.data -> 'controle_adm'::text -> 'adms')) ->> 'cargo'::text AS controle_adm_cargo,
	((cc.data -> 'controle_adm'::text -> 'adms') -> jsonb_object_keys(cc.data -> 'controle_adm'::text -> 'adms')) ->> 'vinculo'::text AS controle_adm_vinculo,
	((cc.data -> 'controle_adm'::text -> 'adms') -> jsonb_object_keys(cc.data -> 'controle_adm'::text -> 'adms')) ->> 'data_entrada'::text AS controle_adm_data_entrada,
	((cc.data -> 'controle_adm'::text -> 'adms') -> jsonb_object_keys(cc.data -> 'controle_adm'::text -> 'adms')) ->> 'estado_civil'::text AS controle_adm_estado_civil,
	((cc.data -> 'controle_adm'::text -> 'adms') -> jsonb_object_keys(cc.data -> 'controle_adm'::text -> 'adms')) ->> 'identificacao'::text AS controle_adm_identificacao
from public.credito_coleta cc
join lucas_leal.t_experian_choose_report_full t1 ON t1.id = cc.id
left join (select distinct direct_prospect_id from lucas_leal.t_experian_controle_adm_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and t2.direct_prospect_id is null



CREATE INDEX t_experian_tipo_consultas_serasa_full_lead_id ON lucas_leal.t_experian_tipo_consultas_serasa_full USING btree (direct_prospect_id);
--TIPO CONSULTAS SERASA
insert into lucas_leal.t_experian_tipo_consultas_serasa_full
(select 
	q1.direct_prospect_id,
	q2.consultas,
	q2.consulta_factoring,
	q2.consulta_seguradora,
	q2.consulta_cobranca,
	q2.consulta_provedor_dado,
	q2.consulta_financeira,
	q2.consulta_fornecedores
from lucas_leal.t_experian_choose_report_full q1
left join(SELECT 
		t2.direct_prospect_id,
		t2.consultas,
		lucas_leal.consulta_serasa_dicionario_factoring(t2.consultas) AS consulta_factoring,
		lucas_leal.consulta_serasa_dicionario_seguradora(t2.consultas) AS consulta_seguradora,
		lucas_leal.consulta_serasa_dicionario_cobranca(t2.consultas) AS consulta_cobranca,
		lucas_leal.consulta_serasa_dicionario_provedor_dado(t2.consultas) AS consulta_provedor_dado,
		lucas_leal.consulta_serasa_dicionario_financeira(t2.consultas) AS consulta_financeira,
		lucas_leal.consulta_serasa_dicionario_fornecedores(t2.consultas) AS consulta_fornecedores,
	    CASE
	        WHEN ("position"(t2.razao_social,t2.consultas) > 0) THEN 1
	        ELSE 0
	    END AS consulta_proprio
	from ( SELECT t1.direct_prospect_id,
				((cc.data -> 'consulta_empresas'::text) -> jsonb_object_keys(cc.data -> 'consulta_empresas'::text)) ->> 'nome'::text AS consultas,
		        dp."name" as razao_social
		    FROM public.credito_coleta cc
		    join lucas_leal.t_experian_choose_report_full t1 on t1.id = cc.id
		    join public.direct_prospects dp on dp.direct_prospect_id = t1.direct_prospect_id 
		    left join (select distinct direct_prospect_id from lucas_leal.t_experian_tipo_consultas_serasa_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
		    where t1.indice_ultimo_antes_data_referencia = 1
		    	and t2.direct_prospect_id is null
	    union all
			SELECT t1.direct_prospect_id,
				((cc.data -> 'consulta_spc'::text) -> jsonb_object_keys(cc.data -> 'consulta_spc'::text)) ->> 'nome'::text AS consultas,
		        dp."name" as razao_social
		    FROM public.credito_coleta cc
		    join lucas_leal.t_experian_choose_report_full t1 on t1.id = cc.id
		    join public.direct_prospects dp on dp.direct_prospect_id = t1.direct_prospect_id 
		    left join (select distinct direct_prospect_id from lucas_leal.t_experian_tipo_consultas_serasa_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
		    where t1.indice_ultimo_antes_data_referencia = 1
		    	and t2.direct_prospect_id is null
	            ) t2
	where t2.consultas != 'CREDITLOOP CORRESPONDENTE BANCARIO' and t2.consultas != ''
	) as q2 on q2.direct_prospect_id = q1.direct_prospect_id 
left join (select distinct direct_prospect_id from lucas_leal.t_experian_tipo_consultas_serasa_full) as q3 on q3.direct_prospect_id = q1.direct_prospect_id 
where q1.indice_ultimo_antes_data_referencia = 1
	and q3.direct_prospect_id is null
)


select count(*),count(distinct direct_prospect_id)
from t_experian_tipo_consultas_serasa_full


select count(*),count(distinct t1.direct_prospect_id)
from t_experian_choose_report_full t1 
left join (select distinct direct_prospect_id from lucas_leal.t_experian_tipo_consultas_serasa_full) as t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and t2.direct_prospect_id is null

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	

CREATE unique INDEX t_experian_tipo_consultas_serasa_indicador_full_lead_id ON lucas_leal.t_experian_tipo_consultas_serasa_indicador_full USING btree (direct_prospect_id);
--TIPO CONSULTAS SERASA INDICADOR
insert into lucas_leal.t_experian_tipo_consultas_serasa_indicador_full 
(select
	t1.direct_prospect_id,
	coalesce(count(*) filter (where t1.consulta_proprio = 1),0) as consulta_proprio,
	coalesce(count(*) filter (where t1.consulta_factoring = 1 and t1.consulta_proprio = 0),0) as consulta_factoring,
	coalesce(count(*) filter (where t1.consulta_seguradora = 1 and greatest(t1.consulta_proprio,t1.consulta_factoring) = 0),0) as consulta_seguradora,
	coalesce(count(*) filter (where t1.consulta_financeira = 1 and greatest(t1.consulta_proprio,t1.consulta_factoring,t1.consulta_seguradora) = 0),0) as consulta_financeira,	
	coalesce(count(*) filter (where t1.consulta_cobranca = 1 and greatest(t1.consulta_proprio,t1.consulta_factoring,t1.consulta_seguradora,t1.consulta_financeira) = 0),0) as consulta_cobranca,
	coalesce(count(*) filter (where t1.consulta_provedor_dado = 1 and greatest(t1.consulta_proprio,t1.consulta_factoring,t1.consulta_seguradora,t1.consulta_financeira,t1.consulta_cobranca) = 0),0) as consulta_provedor_dado,
	coalesce(count(*) filter (where t1.consulta_fornecedores = 1 and greatest(t1.consulta_proprio,t1.consulta_factoring,t1.consulta_seguradora,t1.consulta_financeira,t1.consulta_cobranca,t1.consulta_provedor_dado) = 0),0) as consulta_fornecedores,
	coalesce(count(*) filter (where greatest(t1.consulta_proprio,t1.consulta_factoring,t1.consulta_seguradora,t1.consulta_financeira,t1.consulta_cobranca,t1.consulta_provedor_dado,t1.consulta_fornecedores) = 0),0) as consulta_indefinido
from lucas_leal.t_experian_tipo_consultas_serasa_full t1
left join lucas_leal.t_experian_tipo_consultas_serasa_indicador_full as t2 on t2.direct_prospect_id = t1.direct_prospect_id
where t2.direct_prospect_id is null
group by 1)



