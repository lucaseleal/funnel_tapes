-------------------------------------------------------------------------------------------------------
----------------------------------------------T TRANSACTIONS
-------------------------------------------------------------------------------------------------------

create table t_extratos_upload_lucas_oot as 
select 
	t.id as transaction_id,
	e.direct_prospect_id,
	t.details -> 'financial_connector_transaction' ->> 'statement_id' as docparser_document_id,
	s.details ->> 'filename' as filename,
	t3.uploaded_at as docparser_received_at,
	s.details ->> 'processed_at' as docparser_processed_at,
	s.details ->> 'type' as tipo_doc,
	t3.account_holder,
	s.details ->> 'branch' as agency_number,
	s.details ->> 'account' as account_number,
	t3.data_de_emisso as data_emissao,
	case 
		when regexp_replace(coalesce(t.details -> 'financial_connector_transaction' ->> 'data',t."date"::text),'\d{2}\/\d{2}\/\d{4}','lucas_leal') = 'lucas_leal' then
			to_date(coalesce(t.details -> 'financial_connector_transaction' ->> 'data',t."date"::text),'dd/mm/yyyy')::text
		else coalesce(t.details -> 'financial_connector_transaction' ->> 'data',t."date"::text)
	end::date as data_transacao,
	coalesce(t.details -> 'financial_connector_transaction' ->> 'chave',t.description) as chave_transacao,
	t.details -> 'financial_connector_transaction' ->> 'detalhes' as detalhe_transacao,
	case coalesce(t.details -> 'financial_connector_transaction' ->> 'valor_r',t.value::text) 
		when '' then null 
		else replace(replace(coalesce(t.details -> 'financial_connector_transaction' ->> 'valor_r',t.value::text),'.',''),',','.') 
	end::numeric as valor_transacao,
	(case
		when t.details -> 'financial_connector_transaction' ->> 'saldo_r' = '' then null
		when regexp_replace(replace(replace(t.details -> 'financial_connector_transaction' ->> 'saldo_r' ,'.',''),',','.'),'\d{1,10}\.\d{2}|\-\d{1,10}\.\d{2}','lucas_leal') = 'lucas_leal' then replace(replace(t.details -> 'financial_connector_transaction' ->> 'saldo_r','.',''),',','.')
		else null
	end)::numeric as saldo_transacao
from v_extratos_full e
join biz_credit.analysis_transactions a on a.analysis_id = e.id
join biz_credit.transactions t on t.id = a.transaction_id
join biz_credit.transaction_sources s on s.id = t.source_id
left join financial_connectors.docparser_statement_transactions t2 on t2.id = (t.details -> 'financial_connector_transaction' ->> 'id')::bigint
left join financial_connectors.docparser_statements t3 on t2.statement_id = t3.id
join public.loan_requests lr on lr.loan_request_id = e.loan_request_id
where to_date(to_char(lr.date_inserted,'Mon/yy'),'Mon/yy') between '2020-01-01' and '2020-03-01'
	and regexp_replace(case 
			when regexp_replace(coalesce(t.details -> 'financial_connector_transaction' ->> 'data',t."date"::text),'\d{2}\/\d{2}\/\d{4}','lucas_leal') = 'lucas_leal' then
				to_date(coalesce(t.details -> 'financial_connector_transaction' ->> 'data',t."date"::text),'dd/mm/yyyy')::text
			else coalesce(t.details -> 'financial_connector_transaction' ->> 'data',t."date"::text)
		end,'\d{4}\-\d{2}\-\d{2}','lucas_leal') = 'lucas_leal'
	and (regexp_replace(replace(replace(coalesce(t.details -> 'financial_connector_transaction' ->> 'valor_r',t.value::text),'.',''),',','.'),'\d{1,10}\.\d{2}|\-\d{1,10}\.\d{2}','lucas_leal') = 'lucas_leal' or 
		coalesce(t.details -> 'financial_connector_transaction' ->> 'valor_r',t.value::text) = '')

		
create table t_transactions as
(select 
		id,
		lead_id,
		docparser_document_id,
		docparser_remote_id,
		filename,
		docparser_received_at,
		docparser_processed_at,
		tipo_doc,
		account_holder,
		agency_number,
		account_number,
		data_emissao,
		data_transacao,
		chave_transacao,
		detalhe_transacao,
		valor_transacao,
		saldo_transacao
	from(select 
		l.id,
		lead_id,
		docparser_document_id,
		case docparser_remote_id when '' then null end::bigint as docparser_remote_id,
		filename,
		docparser_received_at,
		docparser_processed_at,
		tipo_doc,
		account_holder,
		agency_number,
		account_number,
		data_emissao,
		data_transacao,
		chave_transacao,
		detalhe_transacao,
		valor_transacao,
		saldo_transacao,
		row_number() over (partition by l.id order by l.id) as flag_dupli
	from t_extratos_upload_lucas_new l
	join(select id,count(*) units
		from t_extratos_upload_lucas_new
		group by 1) as t1 on t1.id = l.id) as t2
	where t2.flag_dupli = 1)
union all
	(select 
		row_number() over (order by transaction_id) + 2878146 as id,
		direct_prospect_id as lead_id,
		docparser_document_id,
		transaction_id as docparser_remote_id,
		filename,
		docparser_received_at,
		docparser_processed_at,
		tipo_doc,
		account_holder,
		agency_number,
		account_number,
		data_emissao,
		data_transacao,
		chave_transacao,
		detalhe_transacao,
		valor_transacao,
		saldo_transacao
	from t_extratos_upload_lucas_oot)
		
		


update t_transactions t
set loan_request_id = t1.loan_request_id
from(
	select
		t.lead_id,
		lr.loan_request_id
	from (select distinct lead_id from t_transactions) as t
	left join(select
			o.direct_prospect_id,
			max(o.offer_id) filter (where lr.status = 'ACCEPTED') as ultima_oferta_aprovada,
			max(o.offer_id) filter (where lr.offer_id is not null and lr.status != 'Substituido') as ultima_oferta_com_pedido_nao_substituido,
			max(o.offer_id) filter (where lr.offer_id is not null) as ultima_oferta_com_pedido,
			max(o.offer_id) as ultima_oferta
		from public.offers o
		left join public.loan_requests lr on lr.offer_id = o.offer_id
		group by 1) as filtro_oferta on filtro_oferta.direct_prospect_id = t.lead_id
	left join public.offers o on o.offer_id = coalesce(ultima_oferta_aprovada,ultima_oferta_com_pedido_nao_substituido,ultima_oferta_com_pedido,ultima_oferta)
	left join(select
			offer_id,
			max(loan_request_id) filter (where status = 'ACCEPTED') as ultimo_pedido_aprovado,
			max(loan_request_id) filter (where status != 'Substituido') as ultimo_pedido_nao_substituido,
			max(loan_request_id) as ultimo_pedido
		from public.loan_requests
		group by 1) as filtro_pedido on filtro_pedido.offer_id = coalesce(ultima_oferta_aprovada,ultima_oferta_com_pedido_nao_substituido,ultima_oferta_com_pedido,ultima_oferta)
	left join public.loan_requests lr on lr.loan_request_id = coalesce(ultimo_pedido_aprovado,ultimo_pedido_nao_substituido,ultimo_pedido)) as t1
where t1.lead_id = t.lead_id



-------------------------------------------------------------------------------------------------------
----------------------------------------------TRANSACTIONS TAPE
-------------------------------------------------------------------------------------------------------

refresh materialized view mv_transactions_tape

grant all on mv_transactions_tape to giulia_killer;
grant all on mv_transactions_tape to pedro_meirelles;
grant select on mv_transactions_tape to valesca_jannis;
grant select on mv_transactions_tape to wellerson_silva;
grant select on mv_transactions_tape to rodrigo_pinho;
grant select on mv_transactions_tape to cristiano_rocha;
grant select on mv_transactions_tape to marcelo_prado;

select
	lead_id,
	lead_bizu3_prob_comb1 as bizu3
from loan_tape_full
union
select
	lead_id,
	bizu3
from mv_transactions_tape


drop materialized view mv_transactions_tape

drop table mv_transactions_tape;

alter table mv_transactions_tape_2 rename to mv_transactions_tape;

create table mv_transactions_tape_2 as 
(select	
	t.lead_id,
	t.loan_request_id,
	coalesce(lr.status,o.status,dp.workflow) as status,
	to_date(to_char(coalesce(lr.date_inserted,o.date_inserted,dp.opt_in_date),'Mon/yy'),'Mon/yy') as cohort_pedido,
	dp.month_revenue,
	case coalesce(lr.status,o.status,dp.workflow) when 'ACCEPTED' then coalesce(lt.collection_ever_30_pmt6_aggressive,lt.collection_ever_30_pmt5_aggressive,lt.collection_ever_30_pmt4_aggressive) else 1 end as bad_oficial,
	(g.loan_request_id is not null)::int as flag_amostra,
	coalesce(lr.value, lr.valor_solicitado,o.max_value,dp.amount_requested) as vl_valor_pedido,
	lucas_leal.is_city_capital(dp.city,dp.state) as neoway_is_city_capital,
	gd.populacao_2017 as geofusion_city_population_2017,
	gd.hierarquia as geofusion_city_hierarchy,
	gd.segmento_municipal as geofusion_city_segmentation,
	n_transactions,
	vl_transactions,
	dp.activity_sector as neoway_activity_sector,
	dp.sector as neoway_sector,
	dp.cnae_code as neoway_economic_activity_national_registration_code,
	dp.cnae as neoway_economic_activity_national_registration_name,
	gc.secao_ibge_denominacao,
	gc.divisao_ibge,
	gc.divisao_ibge_denominacao,
	gc.grupo_ibge,
	gc.grupo_ibge_denominacao,
	gc.classe_ibge,
	gc.classe_ibge_denominacao,
	gc.atividade_ibge,
	banco.banco as banco_principal,
	banco.units as banco_principal_n_transactions,
	banco.units / n_transactions as r_banco_principal_n_txns_to_n_txns,
	banco.valor as r_banco_principal_vl_transactions,
	banco.valor / vl_transactions as r_banco_principal_vl_txns_to_vl_txns,
	n_templates,
	n_extratos,
	n_bancos,
	entradas_final_5_zeros,
	saidas_final_5_zeros,
/*
	n_transacoes_segunda / n_transactions as r_segunda_feira_n_txns_to_n_txns,
	n_transacoes_terca / n_transactions as r_terca_feira_n_txns_to_n_txns,
	n_transacoes_quarta / n_transactions as r_quarta_feira_n_txns_to_n_txns,
	n_transacoes_quinta / n_transactions as r_quinta_feira_n_txns_to_n_txns,
	n_transacoes_sexta / n_transactions as r_sexta_feira_n_txns_to_n_txns,
	n_transacoes_sabado / n_transactions as r_sabado_feira_n_txns_to_n_txns,
	n_transacoes_domingo / n_transactions as r_domingo_feira_n_txns_to_n_txns,
	vl_transacoes_segunda / vl_transactions as r_segunda_feira_vl_txns_to_vl_txns,
	vl_transacoes_terca / vl_transactions as r_terca_feira_vl_txns_to_vl_txns,
	vl_transacoes_quarta / vl_transactions as r_quarta_feira_vl_txns_to_vl_txns,
	vl_transacoes_quinta / vl_transactions as r_quinta_feira_vl_txns_to_vl_txns,
	vl_transacoes_sexta / vl_transactions as r_sexta_feira_vl_txns_to_vl_txns,
	vl_transacoes_sabado / vl_transactions as r_sabado_vl_txns_to_vl_txns,
	vl_transacoes_domingo / vl_transactions as r_domingo_vl_txns_to_vl_txns,
*/
	case 
		when (sum_entradas_31_60_dias between sum_entradas_ate_30_dias * 0.9 and sum_entradas_ate_30_dias * 1.1) and (sum_entradas_over60_dias between sum_entradas_31_60_dias * 0.9 and sum_entradas_31_60_dias * 1.1) then 'Estavel'
		when (sum_entradas_31_60_dias > sum_entradas_ate_30_dias * 1.1) and (sum_entradas_over60_dias > sum_entradas_31_60_dias * 1.1) then 'Crescente'
		when (sum_entradas_31_60_dias < sum_entradas_ate_30_dias * 0.9) and (sum_entradas_over60_dias < sum_entradas_31_60_dias * 0.9) then 'Decrescente'
		else 'Oscilante'
	end as comportamento_faturamento,
	dp.employees,
	case 
		when dp.month_revenue <= 40000 then bz3.lt_bizu3
		when dp.month_revenue <= 350000 then bz3.mt_bizu3
		when dp.month_revenue > 350000 then bz3.ht_bizu3
	end as bizu3,
	r_vl_transactions_to_revenue,
	sum_entradas_ate_30_dias,
	sum_entradas_31_60_dias,
	sum_entradas_over60_dias,
	sum_transacoes_ate_30_dias,
	sum_transacoes_31_60_dias,
	sum_transacoes_over60_dias,
	avg_count_transacoes_por_dia,
	perc_count_dias_com_transacao,
	std_transacoes,
	std_transacoes_ate_30_dias,
	std_transacoes_31_60_dias,
	std_transacoes_over60_dias,
	count_dias_extrato,
	coalesce(lt.lead_experian_score6,rt.lead_experian_score6) as lead_experian_score6,
	coalesce(lt.lead_experian_score4,rt.lead_experian_score4) as lead_experian_score4	
/*
	n_extratos_conta_pf,
	n_extratos_conta_pf::numeric / n_extratos as perc_n_extratos_conta_pf,
	n_transactions_conta_pf::numeric / n_transactions as perc_n_transactions_conta_pf,
	n_transactions_entrada_conta_pf::numeric / n_entradas as perc_n_transactions_entrada_conta_pf,
	n_transactions_saidas_conta_pf::numeric / n_saidas as perc_n_transactions_saidas_conta_pf,
	vl_transactions_conta_pf::numeric / vl_transactions / dp.month_revenue as perc_vl_transactions_conta_pf,
	vl_entradas_conta_pf::numeric / (coalesce(sum_transacoes_ate_30_dias,0) + coalesce(sum_transacoes_31_60_dias,0) + coalesce(sum_transacoes_over60_dias,0)) / dp.month_revenue as perc_vl_entradas_conta_pf,
	vl_saidas_conta_pf::numeric / sum_saidas / dp.month_revenue as perc_vl_saidas_conta_pf
*/
from(select
		t.lead_id,
		t.loan_request_id,
		count(*) filter (where valor_transacao > 0) n_entradas,
		count(*) filter (where valor_transacao < 0) n_saidas,
		count(*) n_transactions,
		sum(valor_transacao) filter (where valor_transacao < 0) as sum_saidas,
		sum(abs(valor_transacao)) filter (where abs(valor_transacao) > 0) as vl_transactions,
		sum(abs(valor_transacao)) filter (where abs(valor_transacao) > 0)::numeric / greatest(max(dp.month_revenue),1) as r_vl_transactions_to_revenue,
		sum(valor_transacao) filter (where coalesce(lr.date_inserted::date,o.date_inserted::date,dp.opt_in_date::date) - data_transacao <= 30 and valor_transacao > 0) as sum_entradas_ate_30_dias,
		sum(valor_transacao) filter (where coalesce(lr.date_inserted::date,o.date_inserted::date,dp.opt_in_date::date) - data_transacao between 31 and 60 and valor_transacao > 0) as sum_entradas_31_60_dias,
		sum(valor_transacao) filter (where coalesce(lr.date_inserted::date,o.date_inserted::date,dp.opt_in_date::date) - data_transacao > 60 and valor_transacao > 0) as sum_entradas_over60_dias,
		sum(abs(valor_transacao)) filter (where coalesce(lr.date_inserted::date,o.date_inserted::date,dp.opt_in_date::date) - data_transacao <= 30) as sum_transacoes_ate_30_dias,
		sum(abs(valor_transacao)) filter (where coalesce(lr.date_inserted::date,o.date_inserted::date,dp.opt_in_date::date) - data_transacao between 31 and 60) as sum_transacoes_31_60_dias,
		sum(abs(valor_transacao)) filter (where coalesce(lr.date_inserted::date,o.date_inserted::date,dp.opt_in_date::date) - data_transacao > 60) as sum_transacoes_over60_dias,
		count(*)::numeric / greatest((max(data_transacao) - min(data_transacao)),1) as avg_count_transacoes_por_dia,
		count(distinct data_transacao)::numeric / greatest((max(data_transacao) - min(data_transacao)),1) as perc_count_dias_com_transacao,
		stddev(abs(valor_transacao)) as std_transacoes,
		stddev(abs(valor_transacao)) filter (where coalesce(lr.date_inserted::date,o.date_inserted::date,dp.opt_in_date::date) - data_transacao <= 30) as std_transacoes_ate_30_dias,
		stddev(abs(valor_transacao)) filter (where coalesce(lr.date_inserted::date,o.date_inserted::date,dp.opt_in_date::date) - data_transacao between 31 and 60) as std_transacoes_31_60_dias,
		stddev(abs(valor_transacao)) filter (where coalesce(lr.date_inserted::date,o.date_inserted::date,dp.opt_in_date::date) - data_transacao > 60) as std_transacoes_over60_dias,
		max(data_transacao) - min(data_transacao) as count_dias_extrato
	from data_science.extratos t
	left join public.loan_requests lr on lr.loan_request_id = t.loan_request_id
	left join public.offers o on o.offer_id = lr.offer_id
	join public.direct_prospects dp on dp.direct_prospect_id = coalesce(o.direct_prospect_id,t.lead_id)
	where t.tag_dict not in ('aplicacao','saldo','ignorado')
	group by 1,2) as t
left join public.loan_requests lr on lr.loan_request_id = t.loan_request_id
left join public.offers o on o.offer_id = lr.offer_id
join public.direct_prospects dp on dp.direct_prospect_id = coalesce(o.direct_prospect_id,t.lead_id)
LEFT JOIN mv_loan_tape_new lt on lt.loan_request_id = t.loan_request_id
left join giulia.aux_amostra_extrato_bruto g on g.loan_request_id = t.loan_request_id
left join lucas_leal.geofusion_data gd on gd.pk = dp.state || dp.city
left join (select
		loan_request_id,
		max(banco) filter (where indice_units = 1) as banco,
		sum(units) filter (where indice_units = 1) as units,
		sum(valor) filter (where indice_units = 1) as valor,
		sum(n_templates) as n_templates,
		sum(n_extratos) as n_extratos,
		max(indice_units) as n_bancos,
		sum(entradas_final_5_zeros) as entradas_final_5_zeros,
		sum(saidas_final_5_zeros) as saidas_final_5_zeros
	from(select
			*,	
			row_number() over (partition by loan_request_id order by units) as indice_units,
			row_number() over (partition by loan_request_id order by valor) as indice_valor
		from(select
				loan_request_id,
				case 
					when position('AMAZONIA' in tipo_doc) > 0 then 'AMAZONIA'
					when position('BANESE' in tipo_doc) > 0 then 'BANESE'
					when position('BRASILIA' in tipo_doc) > 0 then 'BRASILIA'
					when position('BRASIL' in tipo_doc) > 0 then 'BRASIL'
					when position('INTER' in tipo_doc) > 0 then 'INTER'
					when position('NORDESTE' in tipo_doc) > 0 then 'NORDESTE'
					when position('SAFRA' in tipo_doc) > 0 then 'SAFRA'
					when position('TOPAZIO' in tipo_doc) > 0 then 'TOPAZIO'
					when position('BANESTES' in tipo_doc) > 0 then 'BANESTES'
					when position('BANPARA' in tipo_doc) > 0 then 'BANPARA'
					when position('BANRISUL' in tipo_doc) > 0 then 'BANRISUL'
					when position('BRADESCO' in tipo_doc) > 0 then 'BRADESCO'
					when position('CEF' in tipo_doc) > 0 then 'CEF'
					when position('ITAU' in tipo_doc) > 0 then 'ITAU'
					when position('SANTANDER' in tipo_doc) > 0 then 'SANTANDER'
					when position('SICOOB' in tipo_doc) > 0 then 'SICOOB'
					when position('SICREDI' in tipo_doc) > 0 then 'SICREDI'
					when position('TRIBANCO' in tipo_doc) > 0 then 'TRIBANCO'
					when position('UNICRED' in tipo_doc) > 0 then 'UNICRED'
					when position('UNIPRIME' in tipo_doc) > 0 then 'UNIPRIME'
					when position('VIACREDI' in tipo_doc) > 0 then 'VIACREDI'
					when position('WOORI' in tipo_doc) > 0 then 'WOORI'
					else 'OTHER'
				end as banco,
				count(*) filter (where abs(valor_transacao) > 0) as units,
				sum(abs(valor_transacao)) filter (where abs(valor_transacao) > 0) as valor,
				count(distinct tipo_doc) as n_templates,
				count(distinct filename) as n_extratos,
				count(*) filter (where valor_transacao > 0 and right(replace(valor_transacao::text,'.',''),5) like '%00000') as entradas_final_5_zeros,
				count(*) filter (where valor_transacao < 0 and right(replace(valor_transacao::text,'.',''),5) like '%00000') as saidas_final_5_zeros
			from t_transactions t
			join public.direct_prospects dp on dp.direct_prospect_id = t.lead_id
			group by 1,2) as t1) as t2
	group by 1) as banco on banco.loan_request_id = t.loan_request_id
left join giulia.categorias_cnae gc on gc.cnae_code = dp.cnae_code
left join temp_bizu3 bz3 on bz3.lead_id = t.lead_id
left join lucas_leal.rejeitados_transactions as rt on rt.lead_id = t.lead_id)
	
left join (select
		loan_request_id,
		count(*) filter (where valor_transacao > 0 and extract (isodow from data_transacao)::int = 1) as n_transacoes_segunda,
		count(*) filter (where valor_transacao > 0 and extract (isodow from data_transacao)::int = 2) as n_transacoes_terca,
		count(*) filter (where valor_transacao > 0 and extract (isodow from data_transacao)::int = 3) as n_transacoes_quarta,
		count(*) filter (where valor_transacao > 0 and extract (isodow from data_transacao)::int = 4) as n_transacoes_quinta,
		count(*) filter (where valor_transacao > 0 and extract (isodow from data_transacao)::int = 5) as n_transacoes_sexta,
		count(*) filter (where valor_transacao > 0 and extract (isodow from data_transacao)::int = 6) as n_transacoes_sabado,
		count(*) filter (where valor_transacao > 0 and extract (isodow from data_transacao)::int = 7) as n_transacoes_domingo,
		sum(valor_transacao) filter (where valor_transacao > 0 and extract (isodow from data_transacao)::int = 1) as vl_transacoes_segunda,
		sum(valor_transacao) filter (where valor_transacao > 0 and extract (isodow from data_transacao)::int = 2) as vl_transacoes_terca,
		sum(valor_transacao) filter (where valor_transacao > 0 and extract (isodow from data_transacao)::int = 3) as vl_transacoes_quarta,
		sum(valor_transacao) filter (where valor_transacao > 0 and extract (isodow from data_transacao)::int = 4) as vl_transacoes_quinta,
		sum(valor_transacao) filter (where valor_transacao > 0 and extract (isodow from data_transacao)::int = 5) as vl_transacoes_sexta,
		sum(valor_transacao) filter (where valor_transacao > 0 and extract (isodow from data_transacao)::int = 6) as vl_transacoes_sabado,
		sum(valor_transacao) filter (where valor_transacao > 0 and extract (isodow from data_transacao)::int = 7) as vl_transacoes_domingo
	from t_transactions
	group by 1)	as dow on dow.loan_request_id = t.loan_request_id

left join (select
		t.lead_id,
		count(*) filter (where t1.conta_pf = 1) n_transactions_conta_pf,
		count(*) filter (where t.valor_transacao > 0 and t1.conta_pf = 1) as n_transactions_entrada_conta_pf,
		count(*) filter (where t.valor_transacao < 0 and t1.conta_pf = 1) as n_transactions_saidas_conta_pf,
		sum(abs(t.valor_transacao)) filter (where abs(t.valor_transacao) > 0 and conta_pf = 1) as vl_transactions_conta_pf,
		sum(t.valor_transacao) filter (where t.valor_transacao > 0 and t1.conta_pf = 1) as vl_entradas_conta_pf,
		sum(t.valor_transacao) filter (where t.valor_transacao < 0 and t1.conta_pf = 1) as vl_saidas_conta_pf,
		count(distinct t.filename) filter (where t1.conta_pf = 1) as n_extratos_conta_pf
	from(select t.lead_id,
				t.id,
				((position(left(dp."name",position(' ' in dp."name") - 1) in t.account_holder) > 0 
					and greatest(position('EIRELI' in t.account_holder),
						position(' ME' in t.account_holder),
						position(' MEI' in t.account_holder),
						position('CENTRO' in t.account_holder),
						position('IMPORT' in t.account_holder),
						position('EXPORT' in t.account_holder),
						position('ODONTO' in t.account_holder),
						position('MATERIA' in t.account_holder),
						position('LTDA' in t.account_holder),
						position('EPP' in t.account_holder),
						position('S/A' in t.account_holder),
						position('ASSOCIADOS' in t.account_holder),
						position('COMERC' in t.account_holder),
						position('SISTEM' in t.account_holder),
						position('CAPITAL' in t.account_holder),
						position('INFANT' in t.account_holder),
						position('CIRURG' in t.account_holder),
						position('BAR' in t.account_holder),
						position('TELECOM' in t.account_holder),
						position('RESTAURANTE' in t.account_holder),
						position('ESCOLA' in t.account_holder),
						position('SERV' in t.account_holder),
						position('&' in t.account_holder),
						position('FRANQUIA' in t.account_holder),
						position('EQUIP' in t.account_holder),
						position('EDUC' in t.account_holder),
						position('GIGALINK' in t.account_holder),
						position('LAVAND' in t.account_holder),
						position('PUBL' in t.account_holder),
						position('REPR' in t.account_holder),
						position('ADM' in t.account_holder),
						position('ESQUADR' in t.account_holder),
						position(' IND ' in t.account_holder),
						position('ESPORT' in t.account_holder),
						position('CLINICA' in t.account_holder),
						position('CRECHE' in t.account_holder),
						position('MARKETING' in t.account_holder),
						position('INSTALA' in t.account_holder),
						position('ETIQUETA' in t.account_holder),
						position(' EMP' in t.account_holder),
						position('MINIMERC' in t.account_holder),
						position('PRODU' in t.account_holder),
						position('PARTNER' in t.account_holder),
						position('LOGISTICA' in t.account_holder),
						position('PRESENT' in t.account_holder),
						position('RECICLA' in t.account_holder),
						position('UNIVERSO' in t.account_holder),
						position('PRESTAC' in t.account_holder),
						position('ATIVIDAD' in t.account_holder),
						position('Consult' in t.account_holder),
						position('AGROPEC' in t.account_holder),
						position('VEICUL' in t.account_holder),
						position('MAQUIN' in t.account_holder),
						position('ALIMENT' in t.account_holder),
						position('TRATAM' in t.account_holder),
						position('SUPER' in t.account_holder),
						position('TRADICAO' in t.account_holder),
						position('DESENV' in t.account_holder),
						position('FABRIC' in t.account_holder),
						position('ESTR ' in t.account_holder),
						position('RUA ' in t.account_holder),
						position('ADVO' in t.account_holder),
						position('PECAS' in t.account_holder),
						position('ENSINO' in t.account_holder),
						position('ARTIGOS' in t.account_holder),
						position('DROGARIA' in t.account_holder),
						position('TERCEIR' in t.account_holder),
						position('SOLUTIONS' in t.account_holder),
						position('TERAPIA' in t.account_holder),
						position('ESPACO' in t.account_holder),
						position('HOTEL' in t.account_holder),
						position('LABORAT' in t.account_holder),
						position('POUSADA' in t.account_holder),
						position('ELETR' in t.account_holder),
						position('MOVEIS' in t.account_holder),
						position('ACESSORIOS' in t.account_holder),
						position('CNPJ' in t.account_holder),
						position('ADMINIST' in t.account_holder),
						position('EFLUENTE' in t.account_holder),
						position('RECAUCHUT' in t.account_holder),
						position('PARTICIPA' in t.account_holder),
						position('CENTRAL' in t.account_holder),
						position('CALCADO' in t.account_holder),
						position('MANUTEN' in t.account_holder),
						position('PROJET' in t.account_holder),
						position('ORGANIC' in t.account_holder),
						position('ATACAD' in t.account_holder),
						position('SEGUR' in t.account_holder),
						position('PLANEJ' in t.account_holder),
						position('PROMOTORA' in t.account_holder),
						position('TECNOLOGIA' in t.account_holder),
						position('ALUGUEL' in t.account_holder),
						position('VIDRO' in t.account_holder),
						position('TRANSP' in t.account_holder),
						position('MAOZINHAS' in t.account_holder),
						position('MARAITT' in t.account_holder),
						position('INFORMATICA' in t.account_holder),
						position('VIAGE' in t.account_holder),
						position('LANCHONETE' in t.account_holder),
						position('LOCACOES' in t.account_holder),
						position('NEGOCIO' in t.account_holder),
						position('DECORACOES' in t.account_holder),
						position('PROFISS' in t.account_holder),
						position('REPRESENT' in t.account_holder),
						position('EVENTOS' in t.account_holder),
						position('COMUNICACAO' in t.account_holder),
						position('CONFEITARIA' in t.account_holder),
						position('PADARIA' in t.account_holder),
						position('BELEZA' in t.account_holder),
						position('MONTAGEM' in t.account_holder),
						position('CONSULT' in t.account_holder),
						position('CONTAB' in t.account_holder),
						position('ARTESANATO' in t.account_holder),
						position('FUNILARIA' in t.account_holder),
						position('PIZZARIA' in t.account_holder),
						position('INVEST' in t.account_holder),
						position('CONTROLE' in t.account_holder),
						position('INTERNET' in t.account_holder),
						position('SOLUCO' in t.account_holder),
						position('ESTACION' in t.account_holder),
						position('ESTETICA' in t.account_holder),
						position('MARMO' in t.account_holder),
						position('STUDIO' in t.account_holder),
						position('CONSTR' in t.account_holder),
						position('FARMACIA' in t.account_holder),
						position('DENTAL' in t.account_holder),
						position('PASTELARIA' in t.account_holder),
						position('AUTO' in t.account_holder),
						position('INDUSTR' in t.account_holder),
						position('DISTRIBUIDORA' in t.account_holder),
						position('TURISMO' in t.account_holder),
						position(' SALAO ' in t.account_holder)) = 0
					and regexp_replace(translate(t.account_holder,'/-.',''),'.*\d{14,16}.*','lucas') != 'lucas')
					or regexp_replace(translate(t.account_holder,'/-.',''),'.*\d{9,12}[a-zA-Z].*|.*\d{9,12}\s.*|.*\d{9,12}$','edler') = 'edler')::int as conta_pf
			from t_transactions t
			join public.direct_prospects dp on dp.direct_prospect_id = t.lead_id) as t1
		join data_science.extratos t on t.id = t1.id
		where t.tag_dict not in ('aplicacao','saldo','ignorado')
		group by 1) as conta_pj on conta_pj.lead_id = t.lead_id)

-------------------------------------------------------------------------------------------------------
----------------------------------------------NOVAS VARIAVEIS
-------------------------------------------------------------------------------------------------------

		

select 
	t.loan_request_id,
	(t1.prob_b2b >= .6)::int as b2b,
	t.neoway_sector,
	(t1.prob_b2c >= .5)::int as b2c,
	bad_oficial
from mv_transactions_tape t
join t_anexo_b2bc t1 on t1.loan_request_id = t.loan_request_id
where flag_amostra = 1

select
	sum(abs(t.valor_transacao)) filter (where t.tag_dict like )
from data_science.extratos t
join public.direct_prospects dp on dp.direct_prospect_id = coalesce(o.direct_prospect_id,t.lead_id)
join mv_transactions_tape tt on tt.lead_id = t.lead_id
where t.tag_dict not in ('aplicacao','saldo','ignorado') and tt.flag_amostra = 1


select 
	replace(replace(replace(tratado,'tarifas',''),'tarifa',''),'tar','') as tipo_tarifa,
	count(*) as units,
	avg(abs(valor_transacao)) filter (where abs(valor_transacao) > 0) as valor
from data_science.extratos t
where tag_dict = 'servico_bancario' and t.tratado like 'tar%'
group by 1
order by 2 desc



select 
	case
		when t.tratado like '%serv%' or t.tratado like '%banca%' then 'serviços bancarios'
		when t.tratado like '%cheq%' or t.tratado like '%chq%' then 'cheque'
		when t.tratado like '%tit%' or t.tratado like '%liq%' then 'liquidação de titulos'
		when t.tratado like '%dep%' then 'deposito'
		when t.tratado like '%dev%' then 'devolução'
		when t.tratado like '%recibo%' then 'emissão de recibo'
		when t.tratado like '%sispag%' then 'pagamentos -sispag'
		when t.tratado like '%cred%' then 'liberação de crédito'
		when t.tratado like '%saldo%' then 'verificação de saldo'
		when t.tratado like '%ted%' or t.tratado like '%doc' or t.tratado like '%trans%' then 'ted-doc'
		when t.tratado like '%cobr%' then 'cobrança em geral'
		else 'outros'
	end tipo_tarifa,
	count(*) as units,
	sum(abs(valor_transacao)) filter (where abs(valor_transacao) > 0)::text as valor,
	avg(abs(valor_transacao)) filter (where abs(valor_transacao) > 0) / 100 as avg_valor
from data_science.extratos t
where tag_dict = 'servico_bancario' and t.tratado like 'tar%' and abs(valor_transacao) < 100
group by 1
order by 2 desc
		


select
	dp.direct_prospect_id,
	lr.loan_request_id,
	lr.status,
	dp.cnae,
	dp.sector,
	dp.activity_sector,
	gc.secao_ibge_denominacao,
	gc.divisao_ibge::bigint,
	gc.divisao_ibge_denominacao,
	gc.grupo_ibge::bigint,
	gc.grupo_ibge_denominacao,
	gc.classe_ibge::bigint,
	gc.classe_ibge_denominacao,
	gc.atividade_ibge,
	tipo_negocio.tipo_empresa_comercio_online,
	tipo_negocio.tipo_empresa_industria,
	tipo_negocio.tipo_empresa_servico,
	tipo_negocio.tipo_empresa_comercio,
	tipo_cliente.b2c,
	tipo_cliente.b2b,
	tipo_cliente.b2g,
	dp.month_revenue,
	dp.amount_requested,
	dp.employees,
	dp.age,
	dp.mei::int,
	dp.is_simples::int,
	dp.social_capital,
	dp.level_activity,
	dp.number_branches,
	gd.populacao_2017 as geofusion_city_population_2017,
	gd.hierarquia as geofusion_city_hierarchy,
	gd.segmento_municipal as geofusion_city_segmentation,
	dp.bizu21_score,
	dp.serasa_6,
	ba.bank_id::text loan_bank_id,
	dp.number_partners,
	dp.number_coligadas,
	dp.cnae_code::bigint,
	coalesce(et.faturamento_comprovado_mes1,0) +
	coalesce(et.faturamento_comprovado_mes2,0) +
	coalesce(et.faturamento_comprovado_mes3,0) +
	coalesce(et.faturamento_comprovado_mes4,0) +
	coalesce(et.faturamento_comprovado_mes5,0) as faturamento_comprovado,
	et.faturamento_medio as faturamento_medio,
	et.perc_faturamento_medio as perc_faturamento_medio,
	coalesce(et.quantidade_entradas_mes1,0) +
	coalesce(et.quantidade_entradas_mes2,0) +
	coalesce(et.quantidade_entradas_mes3,0) +
	coalesce(et.quantidade_entradas_mes4,0) +
	coalesce(et.quantidade_entradas_mes5,0) as quantidade_entradas,	
	(coalesce(et.faturamento_comprovado_mes1,0) +
	coalesce(et.faturamento_comprovado_mes2,0) +
	coalesce(et.faturamento_comprovado_mes3,0) +
	coalesce(et.faturamento_comprovado_mes4,0) +
	coalesce(et.faturamento_comprovado_mes5,0)) /
		greatest(coalesce(et.quantidade_entradas_mes1,0) +
		coalesce(et.quantidade_entradas_mes2,0) +
		coalesce(et.quantidade_entradas_mes3,0) +
		coalesce(et.quantidade_entradas_mes4,0) +
		coalesce(et.quantidade_entradas_mes5,0),1) as ticket_medio,	
	greatest(et.maior_entrada_mes1,
		et.maior_entrada_mes2,
		et.maior_entrada_mes3,
		et.maior_entrada_mes4,
		et.maior_entrada_mes5) as maior_entrada,
	greatest(et.maior_entrada_mes1,
		et.maior_entrada_mes2,
		et.maior_entrada_mes3,
		et.maior_entrada_mes4,
		et.maior_entrada_mes5) / 
		greatest(coalesce(et.faturamento_comprovado_mes1,0) +
		coalesce(et.faturamento_comprovado_mes2,0) +
		coalesce(et.faturamento_comprovado_mes3,0) +
		coalesce(et.faturamento_comprovado_mes4,0) +
		coalesce(et.faturamento_comprovado_mes5,0),1)as maior_entrada_ponderada
from public.direct_prospects dp
join(select
		o.direct_prospect_id,
		max(o.offer_id) filter (where lr.status = 'ACCEPTED') as ultima_oferta_aprovada,
		max(o.offer_id) filter (where lr.offer_id is not null and lr.status != 'Substituido') as ultima_oferta_com_pedido_nao_substituido,
		max(o.offer_id) filter (where lr.offer_id is not null) as ultima_oferta_com_pedido,
		max(o.offer_id) as ultima_oferta
	from public.offers o
	left join public.loan_requests lr on lr.offer_id = o.offer_id
	group by 1) as filtro_oferta on filtro_oferta.direct_prospect_id = dp.direct_prospect_id
join public.offers o on o.offer_id = coalesce(ultima_oferta_aprovada,ultima_oferta_com_pedido_nao_substituido,ultima_oferta_com_pedido,ultima_oferta)
join(select
		offer_id,
		max(loan_request_id) filter (where status = 'ACCEPTED') as ultimo_pedido_aprovado,
		max(loan_request_id) filter (where status != 'Substituido') as ultimo_pedido_nao_substituido,
		max(loan_request_id) as ultimo_pedido
	from public.loan_requests
	group by 1) as filtro_pedido on filtro_pedido.offer_id = coalesce(ultima_oferta_aprovada,ultima_oferta_com_pedido_nao_substituido,ultima_oferta_com_pedido,ultima_oferta)
join public.loan_requests lr on lr.loan_request_id = coalesce(ultimo_pedido_aprovado,ultimo_pedido_nao_substituido,ultimo_pedido)
left join (select
		 legacy_target_id,	
		 (((data_output -> 'CreditChecklist'-> 4 -> 'Answers' -> 0 -> 'BooleanReply')::varchar)::bool)::integer  as tipo_empresa_comercio,
		 (((data_output -> 'CreditChecklist'-> 4 -> 'Answers' -> 1 -> 'BooleanReply')::varchar)::bool)::integer  as tipo_empresa_comercio_online,
		 (((data_output -> 'CreditChecklist'-> 4 -> 'Answers' -> 2 -> 'BooleanReply')::varchar)::bool)::integer  as tipo_empresa_industria,
		 (((data_output -> 'CreditChecklist'-> 4 -> 'Answers' -> 3 -> 'BooleanReply')::varchar)::bool)::integer  as tipo_empresa_servico
	from public.sync_credit_analysis) as tipo_negocio on tipo_negocio.legacy_target_id = dp.direct_prospect_id
left join (select
		 legacy_target_id,	
		 (((data_output -> 'CreditChecklist'-> 5 -> 'Answers' -> 0 -> 'BooleanReply')::varchar)::bool)::integer  as b2c,
		 (((data_output -> 'CreditChecklist'-> 5 -> 'Answers' -> 1 -> 'BooleanReply')::varchar)::bool)::integer  as b2b,
		 (((data_output -> 'CreditChecklist'-> 5 -> 'Answers' -> 2 -> 'BooleanReply')::varchar)::bool)::integer  as b2g
	from public.sync_credit_analysis) as tipo_cliente on tipo_cliente.legacy_target_id = dp.direct_prospect_id
left join giulia.categorias_cnae gc on gc.cnae_code = dp.cnae_code
left join lucas_leal.geofusion_data gd on gd.pk = dp.state || dp.city
left join bank_accounts ba on ba.bank_account_id = lr.bank_account_id
left join t_extrato_tape et on et.loan_request_id = lr.loan_request_id
where coalesce(tipo_negocio.legacy_target_id,tipo_cliente.legacy_target_id) is not null



select
	dp.direct_prospect_id,
	lr.loan_request_id,
	lr.status,
	dp.cnae,
	dp.sector,
	dp.activity_sector,
	gc.secao_ibge_denominacao,
	gc.divisao_ibge::bigint,
	gc.divisao_ibge_denominacao,
	gc.grupo_ibge::bigint,
	gc.grupo_ibge_denominacao,
	gc.classe_ibge::bigint,
	gc.classe_ibge_denominacao,
	gc.atividade_ibge,
	dp.month_revenue,
	dp.amount_requested,
	dp.employees,
	dp.age,
	dp.mei::int,
	dp.is_simples::int,
	dp.social_capital,
	dp.level_activity,
	dp.number_branches,
	gd.populacao_2017 as geofusion_city_population_2017,
	gd.hierarquia as geofusion_city_hierarchy,
	gd.segmento_municipal as geofusion_city_segmentation,
	dp.bizu21_score,
	dp.serasa_6,
	ba.bank_id::text loan_bank_id,
	dp.number_partners,
	dp.number_coligadas,
	dp.cnae_code::bigint,
	coalesce(et.faturamento_comprovado_mes1,0) +
	coalesce(et.faturamento_comprovado_mes2,0) +
	coalesce(et.faturamento_comprovado_mes3,0) +
	coalesce(et.faturamento_comprovado_mes4,0) +
	coalesce(et.faturamento_comprovado_mes5,0) as faturamento_comprovado,
	et.faturamento_medio as faturamento_medio,
	et.perc_faturamento_medio as perc_faturamento_medio,
	coalesce(et.quantidade_entradas_mes1,0) +
	coalesce(et.quantidade_entradas_mes2,0) +
	coalesce(et.quantidade_entradas_mes3,0) +
	coalesce(et.quantidade_entradas_mes4,0) +
	coalesce(et.quantidade_entradas_mes5,0) as quantidade_entradas,	
	(coalesce(et.faturamento_comprovado_mes1,0) +
	coalesce(et.faturamento_comprovado_mes2,0) +
	coalesce(et.faturamento_comprovado_mes3,0) +
	coalesce(et.faturamento_comprovado_mes4,0) +
	coalesce(et.faturamento_comprovado_mes5,0)) /
		greatest(coalesce(et.quantidade_entradas_mes1,0) +
		coalesce(et.quantidade_entradas_mes2,0) +
		coalesce(et.quantidade_entradas_mes3,0) +
		coalesce(et.quantidade_entradas_mes4,0) +
		coalesce(et.quantidade_entradas_mes5,0),1) as ticket_medio,	
	greatest(et.maior_entrada_mes1,
		et.maior_entrada_mes2,
		et.maior_entrada_mes3,
		et.maior_entrada_mes4,
		et.maior_entrada_mes5) as maior_entrada,
	greatest(et.maior_entrada_mes1,
		et.maior_entrada_mes2,
		et.maior_entrada_mes3,
		et.maior_entrada_mes4,
		et.maior_entrada_mes5) / 
		greatest(coalesce(et.faturamento_comprovado_mes1,0) +
		coalesce(et.faturamento_comprovado_mes2,0) +
		coalesce(et.faturamento_comprovado_mes3,0) +
		coalesce(et.faturamento_comprovado_mes4,0) +
		coalesce(et.faturamento_comprovado_mes5,0),1)as maior_entrada_ponderada
from public.direct_prospects dp
join(select
		o.direct_prospect_id,
		max(o.offer_id) filter (where lr.status = 'ACCEPTED') as ultima_oferta_aprovada,
		max(o.offer_id) filter (where lr.offer_id is not null and lr.status != 'Substituido') as ultima_oferta_com_pedido_nao_substituido,
		max(o.offer_id) filter (where lr.offer_id is not null) as ultima_oferta_com_pedido,
		max(o.offer_id) as ultima_oferta
	from public.offers o
	left join public.loan_requests lr on lr.offer_id = o.offer_id
	group by 1) as filtro_oferta on filtro_oferta.direct_prospect_id = dp.direct_prospect_id
join public.offers o on o.offer_id = coalesce(ultima_oferta_aprovada,ultima_oferta_com_pedido_nao_substituido,ultima_oferta_com_pedido,ultima_oferta)
join(select
		offer_id,
		max(loan_request_id) filter (where status = 'ACCEPTED') as ultimo_pedido_aprovado,
		max(loan_request_id) filter (where status != 'Substituido') as ultimo_pedido_nao_substituido,
		max(loan_request_id) as ultimo_pedido
	from public.loan_requests
	group by 1) as filtro_pedido on filtro_pedido.offer_id = coalesce(ultima_oferta_aprovada,ultima_oferta_com_pedido_nao_substituido,ultima_oferta_com_pedido,ultima_oferta)
join public.loan_requests lr on lr.loan_request_id = coalesce(ultimo_pedido_aprovado,ultimo_pedido_nao_substituido,ultimo_pedido)
left join giulia.categorias_cnae gc on gc.cnae_code = dp.cnae_code
left join lucas_leal.geofusion_data gd on gd.pk = dp.state || dp.city
left join bank_accounts ba on ba.bank_account_id = lr.bank_account_id
join t_extrato_tape et on et.loan_request_id = lr.loan_request_id
join mv_transactions_tape tt on tt.loan_request_id = lr.loan_request_id
where tt.flag_amostra = 1

select
*
from(select 
	loan_request_id,
	coalesce((data->'monthlyIndicators'->0->>'verifiedRevenue')::numeric,0) +
	coalesce((data->'monthlyIndicators'->1->>'verifiedRevenue')::numeric,0) +
	coalesce((data->'monthlyIndicators'->2->>'verifiedRevenue')::numeric,0) +
	coalesce((data->'monthlyIndicators'->3->>'verifiedRevenue')::numeric,0) +
	coalesce((data->'monthlyIndicators'->4->>'verifiedRevenue')::numeric,0) as faturamento_comprovado,
	(data->>'averageIncome')::numeric as faturamento_medio,
	(data->>'averageIncomePercentage')::numeric / 100 as perc_faturamento_medio,
	coalesce((data->'monthlyIndicators'->0->>'incomeCount')::smallint,0) +
	coalesce((data->'monthlyIndicators'->1->>'incomeCount')::smallint,0) +
	coalesce((data->'monthlyIndicators'->2->>'incomeCount')::smallint,0) +
	coalesce((data->'monthlyIndicators'->3->>'incomeCount')::smallint,0) +
	coalesce((data->'monthlyIndicators'->4->>'incomeCount')::smallint,0) as quantidade_entradas,	
	(coalesce((data->'monthlyIndicators'->0->>'verifiedRevenue')::numeric,0) +
	coalesce((data->'monthlyIndicators'->1->>'verifiedRevenue')::numeric,0) +
	coalesce((data->'monthlyIndicators'->2->>'verifiedRevenue')::numeric,0) +
	coalesce((data->'monthlyIndicators'->3->>'verifiedRevenue')::numeric,0) +
	coalesce((data->'monthlyIndicators'->4->>'verifiedRevenue')::numeric,0)) /
		greatest(coalesce((data->'monthlyIndicators'->0->>'incomeCount')::smallint,0) +
			coalesce((data->'monthlyIndicators'->1->>'incomeCount')::smallint,0) +
			coalesce((data->'monthlyIndicators'->2->>'incomeCount')::smallint,0) +
			coalesce((data->'monthlyIndicators'->3->>'incomeCount')::smallint,0) +
			coalesce((data->'monthlyIndicators'->4->>'incomeCount')::smallint,0),1) as ticket_medio,	
	greatest((data->'monthlyIndicators'->0->>'greaterTicket')::numeric,
		(data->'monthlyIndicators'->1->>'greaterTicket')::numeric,
		(data->'monthlyIndicators'->2->>'greaterTicket')::numeric,
		(data->'monthlyIndicators'->3->>'greaterTicket')::numeric,
		(data->'monthlyIndicators'->4->>'greaterTicket')::numeric) as maior_entrada,
	greatest((data->'monthlyIndicators'->0->>'greaterTicket')::numeric,
		(data->'monthlyIndicators'->1->>'greaterTicket')::numeric,
		(data->'monthlyIndicators'->2->>'greaterTicket')::numeric,
		(data->'monthlyIndicators'->3->>'greaterTicket')::numeric,
		(data->'monthlyIndicators'->4->>'greaterTicket')::numeric) / 
		greatest(coalesce((data->'monthlyIndicators'->0->>'verifiedRevenue')::numeric,0) +
			coalesce((data->'monthlyIndicators'->1->>'verifiedRevenue')::numeric,0) +
			coalesce((data->'monthlyIndicators'->2->>'verifiedRevenue')::numeric,0) +
			coalesce((data->'monthlyIndicators'->3->>'verifiedRevenue')::numeric,0) +
			coalesce((data->'monthlyIndicators'->4->>'verifiedRevenue')::numeric,0),1)as maior_entrada_ponderada,
	row_number() over (partition by loan_request_id order by created_on desc) as indice_data
from biz_credit.analysis
where created_by not in ('Eduardo Varella','Pedro Castro','Vanessa Medeiros')
	and last_updated_by not in ('Eduardo Varella','Pedro Castro','Vanessa Medeiros')
	and status not in ('IMPORTED','DELETED')) as t1
where indice_data = 1
	

from(select
		loan_request_id,
		avg(daily_balance) as avg_daily_balance,
		avg(daily_running_balance) as avg_daily_running_balance,
		sum(daily_balance) filter (where indice_dia_asc = 1) as begin_balance,
		sum(daily_running_balance) filter (where indice_dia_asc = 1) as begin_running_balance,
		sum(daily_balance) filter (where indice_dia_desc = 1) as end_balance,
		sum(daily_running_balance) filter (where indice_dia_desc = 1) as end_running_balance,
		min(data_transacao) as first_transaction_date,
		max(data_transacao) as last_transaction_date,
		min(daily_balance) as min_daily_balance,
		max(daily_balance) as max_daily_balance,
		min(daily_running_balance) as min_daily_running_balance,
		max(daily_running_balance) as max_daily_running_balance,
		count(*) filter (where daily_balance < 0) as count_negative_daily_balance,
		array_agg(data_transacao) filter (where daily_balance < 0) as list_of_days_negative_balance,
		avg(daily_ignored_credit) as avg_daily_credit,
		avg(daily_ignored_debit) as avg_daily_debit,
		avg(daily_ignored_running_balance) as avg_daily_running_balance,
		sum(daily_ignored_debit) as avg_daily_debit,
		sum(daily_ignored_running_balance) as avg_daily_running_balance,
		count(*) as count_days_with_transactions
	from(select
			t.loan_request_id,
			t.data_transacao,
			row_number() over (partition by t.loan_request_id order by t.data_transacao) as indice_dia_asc,
			row_number() over (partition by t.loan_request_id order by t.data_transacao desc) as indice_dia_desc,
			sum(valor_transacao) filter (where t.tag_dict not in ('aplicacao','saldo','ignorado')) as daily_balance,
			sum(abs(valor_transacao)) filter (where t.tag_dict not in ('aplicacao','saldo','ignorado')) as daily_running_balance,
			sum(valor_transacao) filter (where t.tag_dict = 'ignorado' and valor_transacao > 0) as daily_ignored_credit,
			sum(valor_transacao) filter (where t.tag_dict = 'ignorado' and valor_transacao < 0) as daily_ignored_debit,
			sum(abs(valor_transacao)) filter (where t.tag_dict = 'ignorado') as daily_ignored_running_balance
		from data_science.extratos t
		group by 1,2) as t1
	group by 1) as dia
join(select
		loan_request_id,
		avg(valor_transacao) filter (where t.tag_dict not in ('aplicacao','saldo','ignorado') and indice_dia_asc <= 15) as balance_0_15,
		avg(valor_transacao) filter (where t.tag_dict not in ('aplicacao','saldo','ignorado') and indice_dia_asc between 16 and 30) as balance_16_30,
		stddev(valor_transacao) filter (where t.tag_dict not in ('aplicacao','saldo','ignorado') and indice_dia_asc between 16 and 30) as balance_16_30,
		avg(valor_transacao) filter (where t.tag_dict not in ('aplicacao','saldo','ignorado') and indice_dia_asc <= 30) as balance_0_30,
		avg(valor_transacao) filter (where t.tag_dict not in ('aplicacao','saldo','ignorado') and indice_dia_asc between 31 and 60) as balance_31_60,
		avg(valor_transacao) filter (where t.tag_dict not in ('aplicacao','saldo','ignorado') and indice_dia_asc <= 45) as balance_0_45,
		avg(valor_transacao) filter (where t.tag_dict not in ('aplicacao','saldo','ignorado') and indice_dia_asc between 46 and 90) as balance_46_90,
	from(select
			*,
			dense_rank() over (partition by loan_request_id order by data_transacao) as indice_dia_asc
		from data_science.extratos) as t1
	group by 1) as quinze

select
	loan_request_id,
	sum(valor_transacao) filter(where dense_rank() over (partition by loan_request_id order by data_transacao) <= 30) --as indice_dia_asc
from data_science.extratos
where loan_request_id in (843,852)

select tag_dict,count(*)
from data_science.extratos
group by 1


select
	loan_request_id,
	count(*) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 1) as n_entradas_janeiro,
	count(*) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 2) as n_entradas_fevereiro,
	count(*) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 3) as n_entradas_marco,
	count(*) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 4) as n_entradas_abril,
	count(*) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 5) as n_entradas_maio,
	count(*) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 6) as n_entradas_junho,
	count(*) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 7) as n_entradas_julho,
	count(*) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 8) as n_entradas_agosto,
	count(*) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 9) as n_entradas_setembro,
	count(*) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 10) as n_entradas_outubro,
	count(*) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 11) as n_entradas_novembro,
	count(*) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 12) as n_entradas_dezembro,
	count(*) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 1) as n_saidas_janeiro,
	count(*) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 2) as n_saidas_fevereiro,
	count(*) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 3) as n_saidas_marco,
	count(*) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 4) as n_saidas_abril,
	count(*) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 5) as n_saidas_maio,
	count(*) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 6) as n_saidas_junho,
	count(*) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 7) as n_saidas_julho,
	count(*) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 8) as n_saidas_agosto,
	count(*) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 9) as n_saidas_setembro,
	count(*) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 10) as n_saidas_outubro,
	count(*) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 11) as n_saidas_novembro,
	count(*) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 12) as n_saidas_dezembro,
	sum(valor_transacao) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 1) as vl_entradas_janeiro,
	sum(valor_transacao) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 2) as vl_entradas_fevereiro,
	sum(valor_transacao) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 3) as vl_entradas_marco,
	sum(valor_transacao) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 4) as vl_entradas_abril,
	sum(valor_transacao) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 5) as vl_entradas_maio,
	sum(valor_transacao) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 6) as vl_entradas_junho,
	sum(valor_transacao) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 7) as vl_entradas_julho,
	sum(valor_transacao) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 8) as vl_entradas_agosto,
	sum(valor_transacao) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 9) as vl_entradas_setembro,
	sum(valor_transacao) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 10) as vl_entradas_outubro,
	sum(valor_transacao) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 11) as vl_entradas_novembro,
	sum(valor_transacao) filter (where valor_transacao > 0 and to_char(data_transacao,'MM')::int = 12) as vl_entradas_dezembro,
	sum(valor_transacao) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 1) as vl_saidas_janeiro,
	sum(valor_transacao) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 2) as vl_saidas_fevereiro,
	sum(valor_transacao) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 3) as vl_saidas_marco,
	sum(valor_transacao) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 4) as vl_saidas_abril,
	sum(valor_transacao) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 5) as vl_saidas_maio,
	sum(valor_transacao) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 6) as vl_saidas_junho,
	sum(valor_transacao) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 7) as vl_saidas_julho,
	sum(valor_transacao) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 8) as vl_saidas_agosto,
	sum(valor_transacao) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 9) as vl_saidas_setembro,
	sum(valor_transacao) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 10) as vl_saidas_outubro,
	sum(valor_transacao) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 11) as vl_saidas_novembro,
	sum(valor_transacao) filter (where valor_transacao < 0 and to_char(data_transacao,'MM')::int = 12) as vl_saidas_dezembro
from t_transactions
group by 1


select 
	--(t1.prob_b2b >= .6)::int as b2b,
	(t1.prob_b2c >= .5)::int as b2c,
	count(*) filter (where bad_oficial = 1) bad,
	count(*) filter (where bad_oficial = 0) good
from mv_transactions_tape t
join t_anexo_b2bc t1 on t1.loan_request_id = t.loan_request_id
group by 1--,2
order by 3


