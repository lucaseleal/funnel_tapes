CREATE INDEX mvsigners_data_full_lead_id ON lucas_leal.mv_signers_data_full USING btree (direct_prospect_id);
CREATE INDEX mv_signers_data_full_loan_request_id ON lucas_leal.mv_signers_data_full USING btree (loan_request_id);
CREATE INDEX mv_signers_data_full_signer_id ON lucas_leal.mv_signers_data_full USING btree (signer_id);
CREATE INDEX mv_signers_data_full_signer_client_id ON lucas_leal.mv_signers_data_full USING btree (signer_client_id);
--insert into lucas_leal.t_signers_data_full2
create materialized view lucas_leal.mv_signers_data_full as
(select 
--		dp.direct_prospect_id || '-' || coalesce(s.name,es.nome,ser.nome) as pk,
		dp.direct_prospect_id,
		lr.loan_request_id,
		coalesce(s.share_percentage,es.participacao,ser.share) as signer_share_percentage,
		to_char(age(dp.opt_in_date::date, s.date_of_birth::date), 'yyyy'::text)::integer AS signer_age,
		s.revenue::numeric as signer_revenue,
		s.gender::text::character varying(50) as signer_gender,
		s.guarantor as signer_is_avalista,
		case when coalesce(s.cpf,es.cpf,ser.cpf_socios) != '' then coalesce(s.cpf,es.cpf,ser.cpf_socios) end as signer_cpf,
		coalesce(s.cpf,es.cpf,ser.cpf_socios) = dp.cpf as signer_is_solicitante,
		s.civil_status as signer_civil_status,
		coalesce(s.name,es.nome,ser.nome) as signer_name,
		s.email as signer_email,
		s.profession as signer_profession,
		s.rg as signer_rg,
		s.issuing_body as signer_issuing_body,
		s.country_of_birth as signer_country_of_birth,
		s.city_of_birth as signer_city_of_birth,
		s.date_of_birth as signer_date_of_bith,
		sa.street as signer_street_address,
		sa."number" as signer_number_address,
		sa.complement as signer_complement_address,
		sa.zip_code as signer_zip_code_address,
		c."name" as signer_city_address,
		ss."name" as signer_state_address,
		sa.country as signer_country_address,
		sa.neighbourhood as signer_neighbourhood_address,
		s.sign_spouse as signer_sign_spouse,
		coalesce(s.shareholder,es.participacao > 0,ser.share > 0) as signer_is_shareholder,
		s.administrator as signer_is_administrator,
		s.spouse->>'cpf' as signer_spouse_cpf,
		s.spouse->>'name' as signer_spouse_name,
		s.spouse->>'email' as signer_spouse_email,
		s.signer_id as signer_id,
		s.client_id as signer_client_id,
		es_info.qualificacao as signer_neoway_qualificao,
		es_info.nivel_pep as signer_neoway_nivel_pep,
		es_info.ultima_atualizacao::date as signer_neoway_ultima_atualizacao,
		es_info.falecido as siger_neoway_falecido,
		row_number() over (partition by dp.direct_prospect_id order by case when s.administrator then 1 else 2 end, s.date_of_birth) as indice_administrador,
		row_number() over (partition by dp.direct_prospect_id order by coalesce(s.share_percentage,es.participacao,ser.share,0) desc, s.date_of_birth) as indice_socio_majoritario,
		row_number() over (partition by dp.direct_prospect_id order by case coalesce(s.share_percentage,es.participacao,ser.share,0) when 0 then null else s.date_of_birth end, coalesce(s.share_percentage,es.participacao,ser.share,0) desc) as indice_socio_mais_velho
	from public.direct_prospects dp
	join data_science.v_funil_completo_sem_duplicacao vfcsd on vfcsd.direct_prospect_id = dp.direct_prospect_id 
	left join public.loan_requests lr on lr.loan_request_id = vfcsd.loan_request_id 
	left join public.loan_requests_signers lrs on lrs.loan_request_id = lr.loan_request_id
	left join public.signers s on s.signer_id = lrs.signer_id
	left join public.signer_addresses sa on sa.address_id = s.address_id
	left join public.cities c on c.city_id = sa.city_id
	left join public.states ss on ss.state_id = c.state_id
	left join (select
			es.cnpj,
			es.cpf,
			es.nome,
			es.participacao,
			row_number() over (partition by es.cnpj,es.cpf order by es.id) as indice
		from public.empresas_socios es
		left join (select distinct dp.cnpj from public.signers s join (select distinct cnpj,client_id from public.direct_prospects) dp on dp.client_id = s.client_id) as s on s.cnpj = es.cnpj
		where s.cnpj is null) es on es.cnpj = dp.cnpj and es.indice = 1
	left join (select t1.*
		from lucas_leal.t_experian_get_shareholders_data_full t1
		join public.direct_prospects dp on dp.direct_prospect_id = t1.direct_prospect_id 
		left join (select distinct cnpj from public.empresas_socios) es on es.cnpj = dp.cnpj 
		left join (select distinct client_id from public.signers) s on s.client_id = dp.client_id 
		where es.cnpj is null 
			and s.client_id is null
		) as ser on ser.direct_prospect_id = dp.direct_prospect_id
	left join (select
			cnpj,
			cpf,
			nome,
			nivel_pep,
			qualificacao,
			participacao,
			ultima_atualizacao,
			falecido,
			row_number() over (partition by cnpj,cpf order by id) as indice
		from public.empresas_socios
		where participacao > 0) es_info on es_info.cnpj = dp.cnpj and es_info.cpf = coalesce(s.cpf,es.cpf,ser.cpf_socios) and es_info.indice = 1
	where coalesce(s.name,es.nome,ser.nome) is not null 
)

CREATE UNIQUE INDEX mv_signers_indicators_full_lead_id ON lucas_leal.mv_signers_indicators_full USING btree (direct_prospect_id);
create materialized view lucas_leal.mv_signers_indicators_full as
(select
	t1.direct_prospect_id,
	max(t1.signer_age) filter (where t1.indice_administrador = 1 and signer_is_administrator) as administrator_age,
	max(t1.signer_revenue) filter (where t1.indice_administrador = 1 and signer_is_administrator) as administrator_revenue,
	max(t1.signer_civil_status) filter (where t1.indice_administrador = 1 and signer_is_administrator) as administrator_civil_status,
	max(t1.signer_gender) filter (where t1.indice_administrador = 1 and signer_is_administrator) as administrator_gender,
    avg(t1.signer_age) FILTER (where t1.signer_is_shareholder) AS avg_age_partners,
    avg(t1.signer_revenue) FILTER (where t1.signer_is_shareholder) AS avg_revenue_partners,
    count(*) FILTER (where t1.signer_gender = 'MALE' AND signer_is_shareholder) AS male_partners,
    count(*) FILTER (where t1.signer_gender = 'FEMALE' AND signer_is_shareholder) AS female_partners,
    count(*) FILTER (where t1.signer_is_shareholder) AS count_socios,
    count(*) FILTER (where t1.signer_is_avalista) AS count_avalistas,
    count(*) filter (where t1.signer_is_avalista and coalesce(t1.signer_share_percentage,0) = 0) as count_avalistas_nao_socios,
    count(*) AS count_total,
    count(*) filter (where t1.signer_is_shareholder and signer_is_solicitante) as is_shareholder,
	max(t1.signer_age) filter (where t1.indice_socio_majoritario = 1 and signer_is_shareholder) as majorsigner_age,
	max(t1.signer_revenue) filter (where t1.indice_socio_majoritario = 1 and signer_is_shareholder) as majorsigner_revenue,
	max(t1.signer_civil_status) filter (where t1.indice_socio_majoritario = 1 and signer_is_shareholder) as majorsigner_civil_status,
	max(t1.signer_gender) filter (where t1.indice_socio_majoritario = 1 and signer_is_shareholder) as majorsigner_gender,
	max(t1.signer_age) filter (where t1.indice_socio_mais_velho = 1 and signer_is_shareholder) as oldestsh_age,
	max(t1.signer_revenue) filter (where t1.indice_socio_mais_velho = 1 and signer_is_shareholder) as oldestsh_revenue,
	max(t1.signer_civil_status) filter (where t1.indice_socio_mais_velho = 1 and signer_is_shareholder) as oldestsh_civil_status,
	max(t1.signer_gender) filter (where t1.indice_socio_mais_velho = 1 and signer_is_shareholder) as oldestsh_gender,
	max(t1.signer_age) filter (where t1.signer_is_solicitante) as solicsignerscpf_age,
	max(t1.signer_revenue) filter (where t1.signer_is_solicitante) as solicsignerscpf_revenue,
	max(t1.signer_civil_status) filter (where t1.signer_is_solicitante) as solicsignerscpf_civil_status,
	max(t1.signer_gender) filter (where t1.signer_is_solicitante) as solicsignerscpf_gender
from lucas_leal.t_signers_data_full as t1
group by 1);


create unique index t_signers_zip_code_data_lead_id ON lucas_leal.t_signers_zip_code_data USING btree (direct_prospect_id);
--t_signers_zip_code_data
insert into t_signers_zip_code_data 
(SELECT dp.direct_prospect_id,
	max(replace(sa.zip_code::text, '-'::text, ''::text)) AS signer_zipcode
	FROM public.direct_prospects dp
	join lucas_leal.v_funil_completo_sem_duplicacao t1 on t1.direct_prospect_id = dp.direct_prospect_id 
	JOIN loan_requests_signers lrs ON lrs.loan_request_id = t1.loan_request_id
	JOIN signers s ON s.signer_id = lrs.signer_id
	JOIN signer_addresses sa ON sa.address_id = s.address_id
  WHERE (dp.zip_code::text = ''::text OR dp.zip_code IS null) and dp.direct_prospect_id not in (select distinct direct_prospect_id from t_signers_zip_code_data)
  GROUP BY dp.direct_prospect_id)

