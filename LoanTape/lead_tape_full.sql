create table lucas_leal.temp_ids as
select t1.*
from lucas_leal.v_funil_completo_sem_duplicacao t1
where t1.direct_prospect_id in ()

select count(*)
from lucas_leal.temp_ids
	
select
	lead_id,
	crednet_total_derogatory_mark_units,
	crednet_total_derogatory_mark_value_byrevenue,
	coalesce(lead_experian_score6,lead_experian_score4) as lead_experian_score,
	scr_months_since_last_overdue_pj,
	scr_months_since_last_default_pj,
	scr_months_since_last_overdue_pf,
	scr_months_since_last_default_pf
from lucas_leal.t_reject_inferece_negado_renovacao_21_03_24



create table lucas_leal.t_reject_inferece_projeto_fraude_21_03_30 as 
--------------------------DIRETO
select 
	dp.direct_prospect_id as lead_id,
	dp.workflow as lead_status,
	dp.opt_in_date as lead_opt_in_date,
	extract(hour from dp.opt_in_date)::int as lead_opt_in_hour,
	extract(day from dp.opt_in_date)::int as lead_opt_in_day,
	extract(dow from dp.opt_in_date)::int as lead_opt_in_dow,
	(extract('day' from date_trunc('week', case when extract(isodow from dp.opt_in_date::date) = 7 then dp.opt_in_date::date::date + 1 when extract(isodow from dp.opt_in_date::date) = 6 then dp.opt_in_date::date::date - 1 else dp.opt_in_date::date::date end) - date_trunc('week', case when extract(isodow from date_trunc('month', dp.opt_in_date::date)) = 7 then date_trunc('month', dp.opt_in_date::date)::date + 1 when extract(isodow from date_trunc('month', dp.opt_in_date::date)) = 6 then date_trunc('month', dp.opt_in_date::date)::date - 1 else date_trunc('month', dp.opt_in_date::date)::date end)) / 7 + 1)::int as lead_opt_in_wom,
	extract(month from dp.opt_in_date)::int as lead_opt_in_month,
	extract(year from dp.opt_in_date)::int as lead_opt_in_year,
	case when amount_requested < 1000 then o.max_value else amount_requested end / case month_revenue when 0 then null else month_revenue end / 12 as lead_admin_incremental_leverage,
	(case when amount_requested < 1000 then o.max_value else amount_requested end + case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null then 0 else divida_atual_pj end) / case month_revenue when 0 then null else month_revenue end / 12 as lead_admin_total_leverage,	
	o.max_value/case when amount_requested < 1000 then o.max_value else amount_requested end as lead_offer_over_application_value,
	lr.valor_solicitado/o.max_value::float as lead_requested_over_offer_value,
	lr.value/lr.valor_solicitado::float as lead_loan_over_requested_value,
	lr.prazo_solicitado/o.max_number_of_installments::double precision as lead_requested_over_offer_term,
	lr.number_of_installments/lr.prazo_solicitado::float as lead_loan_over_requested_term,
	case when lr.value/o.max_value::float = lr.number_of_installments/o.max_number_of_installments::float then 1 else (@li.pmt) / ((o.interest_rate / 100 + 0.009) / (1 - ( 1 + o.interest_rate / 100 + 0.009) ^ (- o.max_number_of_installments)) * o.max_value) end as lead_loan_over_offer_pmt,
	case when lr.value / lr.valor_solicitado::float = lr.number_of_installments / lr.prazo_solicitado::float then 1 else (@li.pmt) / ((o.interest_rate / 100 + 0.009) / (1 - ( 1 + o.interest_rate / 100 + 0.009) ^ (- lr.prazo_solicitado)) * lr.valor_solicitado) end as lead_loan_over_requested_pmt,
	case when lr.loan_date < dp.opt_in_date then 0 else extract(day from lr.loan_date - dp.opt_in_date) * 24 + extract(hour from lr.loan_date - dp.opt_in_date) + extract(minute from lr.loan_date - dp.opt_in_date) / 60 end as lead_full_track_time,
	replace(replace(replace(replace(replace(replace(replace(replace(replace(dp.reason_for_loan,'Capital de Giro','Working Capital'),'ExpansÃ£o','Expansion'),'Compra de Estoque','Inventory'),'Outros','Others'),'ConsolidaÃ§Ã£o de DÃ­vidas','Debt Consolidation'),'Marketing e Vendas','Sales and Marketing'),'Uso Pessoal','Personal Use'),'Reforma','Refurbishment'),'Compra de Equipamentos','Machinery') as lead_reason_for_loan,
	replace(replace(replace(replace(replace(replace(replace(replace(replace(dp.vinculo,'Administrador','Administrator'),'Outros','Other'),'Procurador','Attorney'),'SÃ³cio','Shareholder'),'Administradora','Administrator'),'SÃ³cia','Shareholder'),'Diretor','Director'),'Gerente Financeiro','CFO'),'Presidente','CEO') as lead_requester_relationship,
	dp.month_revenue as lead_informed_month_revenue,
	dp.amount_requested as lead_application_amount,
	lucas_leal.treat_source_name(lucas_leal.translate_char_encode_uft8(dp.utm_source)) as lead_marketing_channel,
	coalesce(case upper(lucas_leal.treat_source_name(lucas_leal.translate_char_encode_uft8(dp.utm_source))) 
		when 'ADWORDS' then 'adwords'
		when 'AGENT' then 'agent'
		when 'BIDU' then 'bidu'
		when 'BING' then 'Bing'
		when 'CREDITA' then 'credita'
		when 'FACEBOOK' then 'facebook'
		when 'FACEBOOK_ORG' then 'facebook'
		when 'FINANZERO' then 'finanzero'
		when 'FINPASS' then 'finpass'
		when 'GERU' then 'geru'
		when 'INSTAGRAM' then 'instagram'
		when 'JUROSBAIXOS' then 'jurosbaixos'
		when 'KONKERO' then 'konkero'
		when 'LINKEDIN' then 'linkedin'
		when 'MGM' then 'member-get-member'
		when 'ORGANICO' then 'organico'
		when 'BLOG' then 'organico'
		when 'SITE' then 'organico'
		when 'DIRECT_CHANNEL' then 'organico'
		when 'ANDROID' then 'organico'
		when 'RENEWAL' then 'organico'
		when 'EMAIL' then 'organico'
		when 'RD' then 'organico'
		when 'RD#/' then 'organico'
		when 'RDSTATION' then 'organico'
		when 'RD+STATION' then 'organico'
		when 'AMERICANAS' then 'other' 
		when 'IFOOD' then 'other' 
		when 'PEIXE-URBANO' then 'other' 
		when 'OUTBRAIN' then 'outbrain'
		when 'TABOOLA' then 'taboola'
		when 'CHARLES/' then 'agent'
		when 'MARCELOROMERA' then 'agent'
		when 'PBCONSULTORIA' then 'agent'
		when 'HMSEGUROS' then 'agent'
		when 'COMPARAONLINE' then 'agent'
		when 'CREDEXPRESS#/' then 'agent'
		else case when pp.identificacao is not null then 'agent' else dp.utm_source end
	end,'other') as lead_marketing_channel_group,
	lucas_leal.translate_char_encode_uft8(utm_medium)  as lead_marketing_medium,
	lucas_leal.treat_campaign_name(lucas_leal.translate_char_encode_uft8(utm_source)) as lead_marketing_campaign,
	dp.client_id as lead_client_id,
	dp.opt_in_ip as lead_opt_in_ip,
	case when dp.n_company_applied_before is null then 0 else dp.n_company_applied_before end as lead_times_company_applied_before,
	case when dp.n_individual_applied_before is null then 0 when lucas_leal.treat_source_name(lucas_leal.translate_char_encode_uft8(dp.utm_source)) in ('fdex','capitalemp','finpass','IDEA','IMPERIO','ACESSE','andreozzi','ARAMAYO','AZUL','BONUS','CHARLES','CREDRAPIDO','CREDEXPRESS','DIGNESS','DUOCAPITAL','ISF','JEITONOVO','MONETAE','MRS','PLANOCAPITAL','R2A','VIETTO','vipac','VIRGINIA','ALMIRGUEDES','CREDPRIME','HMSEGUROS','CFC','REALCRED','ESPACOLIMA','FRIGO') then null else dp.n_individual_applied_before end as lead_times_individual_applied_before,
	dp.facebook::int lead_has_facebook_page,
	dp.likes lead_number_likes_facebook,
	dp.streetview::int lead_has_google_streetview,
	dp.site::int lead_has_website,
	case when upper(left(right(dp.email, length(dp.email)-position('@' in dp.email)),position('.' in right(dp.email, length(dp.email)-position('@' in dp.email)-1))))  in ('GMAIL','HOTMAIL','YAHOO','OUTLOOK','UOL','TERRA','BOL','LIVE','ICLOUD','IG','MSN','GLOBO') then replace(upper(left(right(dp.email, length(dp.email)-position('@' in dp.email)),position('.' in right(dp.email, length(dp.email)-position('@' in dp.email)-1)))),'GMAIIL','GMAIL') else 'OTHER' end as lead_email_server,
	case when (month_revenue >= 100000 and age < 2) or (month_revenue >= 200000 and age < 3) or (month_revenue >= 300000 and age < 4) then 1 else 0 end as lead_Age_Revenue_Discrepancy,
	case when dp.phone = '' then null else substring(dp.phone,2,2) end as lead_phone_area_code,
--NEOWAY
	dp.revenue::double precision as neoway_estimated_annual_revenue,
	to_date(dp.date_created,'yyyy-mm-dd') as neoway_date_company_creation,
	dp.age as neoway_company_age,
	dp.employees as neoway_estimate_number_of_employees,
	dp.mei::int as neoway_is_mei,
	dp.is_simples::int as neoway_simple_tribute_method_applied,
	dp.cnae_code as neoway_economic_activity_national_registration_code,
	dp.cnae as neoway_economic_activity_national_registration_name,
	dp.social_capital::float as neoway_social_capital,
	dp.number_partners as neoway_estimated_number_of_shareholders,
	dp.number_coligadas as neoway_estimated_number_of_related_companies,
	dp.number_branches as neoway_estimated_number_of_branches,
	case when dp.zip_code = '' then null else dp.zip_code end as neoway_company_zip_code,
	lucas_leal.get_postal_code_lucas(dp.state::text,case when dp.zip_code = '' then null else dp.zip_code end::text) as neoway_company_zip_code_class,
	dp.neighbourhood as neoway_company_neighbourhood,
	dp.city as neoway_company_city,
	dp.state as neoway_company_state,
	lucas_leal.is_city_capital(dp.city,dp.state) as neoway_is_city_capital,
	lucas_leal.state_region(dp.state) as neoway_state_region,
	dp.tax_health as neoway_tax_health,
	dp.level_activity as neoway_level_activity,
	dp.activity_sector as neoway_activity_sector,
	dp.sector as neoway_sector,
	dp.pgfn_debt as neoway_government_debt,
--CREDNET
	dp.number_protests as crednet_notary_registry_protests_unit,
	dp.protests_amount crednet_notary_registry_protests_value,
	dp.protests_amount / case month_revenue when 0 then null else month_revenue end / 12 crednet_notary_registry_protests_value_byrevenue,
	dp.number_spc as crednet_company_negative_mark_by_non_fin_companies_unit,
	dp.spc_amount as crednet_company_negative_mark_by_non_fin_companies_value,
	dp.spc_amount / case month_revenue when 0 then null else month_revenue end / 12 as crednet_company_negative_mark_by_non_fin_companies_value_byrevenue,
	dp.number_acoes as crednet_company_legal_action_unit,
	dp.acoes_amount as crednet_company_legal_action_value,
	dp.acoes_amount / case month_revenue when 0 then null else month_revenue end / 12 crednet_legal_action_value_byrevenue,
	dp.number_refins as crednet_company_negative_mark_by_fin_companies_unit,
	dp.refins_amount as crednet_company_negative_mark_by_fin_companies_value,
	dp.refins_amount / case month_revenue when 0 then null else month_revenue end / 12 crednet_company_negative_mark_by_fin_companies_value_byrevenue,
	dp.number_protests + dp.number_spc + dp.number_acoes + dp.number_refins as crednet_total_derogatory_mark_units,
	dp.protests_amount + dp.spc_amount + dp.acoes_amount + dp.refins_amount as crednet_total_derogatory_mark_value,
	(dp.protests_amount + dp.spc_amount + dp.acoes_amount + dp.refins_amount) / case month_revenue when 0 then null else month_revenue end / 12 as crednet_total_derogatory_mark_value_byrevenue,
--SCORES
	lead_score::float as lead_biz_lead_score,
	bizu_score::float as lead_biz_bizu1_score,
	case when bizu_score >= 850 then 'A+' when bizu_score >= 700 then 'A-' when bizu_score >= 650 then 'B+' when bizu_score >= 600 then 'B-' when bizu_score >= 500 then 'C+'when bizu_score >= 400 then 'C-' when bizu_score >= 300 then 'D' when bizu_score < 300 then 'E' else 'No Bizu1' end as lead_rating_class_bizu1,
	case when dp.opt_in_date < '2019-10-16' then coalesce(serasa_coleta,o.rating,dp.serasa_4) else dp.serasa_4 end as lead_experian_score4,
	lucas_leal.serasa_classe(case when dp.opt_in_date < '2019-10-16' then coalesce(serasa_coleta,o.rating,dp.serasa_4) else dp.serasa_4 end::int) as lead_class_experian_score4,
	case when dp.opt_in_date < '2019-10-16' then pd_serasa end as lead_experian_score4_associated_pd,
	case when dp.opt_in_date < '2019-10-16' then coalesce(bktest.s6,bktest2.score6,dp.serasa_6) else coalesce(serasa_coleta,dp.serasa_6) end as lead_experian_score6,
	lucas_leal.serasa_classe(case when dp.opt_in_date < '2019-10-16' then coalesce(bktest.s6,bktest2.score6,dp.serasa_6) else coalesce(serasa_coleta,dp.serasa_6) end::int) as lead_class_experian_score6,
	dp.proposal_score as lead_proposal_score,
	dp.proposal_amount as lead_proposal_limit,
	dp.proposal_interest as lead_proposal_interest_rate,
	dp.proposal_term as lead_proposal_term,
	dp.low_ticket_score as lead_low_ticket_score,
	dp.bizu2_score::float as lead_biz_bizu2_score,
	dp.pd_bizu2::float as lead_biz_bizu2_associated_pd,
	case when dp.bizu2_score >= 1100 then 'A+' when dp.bizu2_score >= 850 then 'A-' when dp.bizu2_score >= 720 then 'B+' when dp.bizu2_score >= 600 then 'B-' when dp.bizu2_score >= 500 then 'C+' when dp.bizu2_score >= 400 then 'C-' when dp.bizu2_score >= 300 then 'D' when dp.bizu2_score >= 200 then 'E' when dp.bizu2_score < 200 then 'F' else 'No Bizu2' end as lead_classe_Bizu2,
	dp.bizu21_score::float as lead_biz_bizu21_score,
	dp.pd_bizu21::float as lead_biz_bizu21_associated_pd,
	dp.bizu21_class as lead_classe_Bizu21,
	case when dp.opt_in_date < '2018-03-13' then dp.bizu_score when dp.opt_in_date < '2019-10-16' then dp.bizu2_score else dp.bizu21_score end as lead_biz_bizux_score,
	case when dp.opt_in_date < '2018-03-13' then 
			case when bizu_score >= 850 then 'A+' when bizu_score >= 700 then 'A-' when bizu_score >= 650 then 'B+' when bizu_score >= 600 then 'B-' when bizu_score >= 500 then 'C+'when bizu_score >= 400 then 'C-' when bizu_score >= 300 then 'D' when bizu_score < 300 then 'E' else 'No Bizu1' end
		when dp.opt_in_date < '2019-10-16' then 
			case when dp.bizu2_score >= 1100 then 'A+' when dp.bizu2_score >= 850 then 'A-' when dp.bizu2_score >= 720 then 'B+' when dp.bizu2_score >= 600 then 'B-' when dp.bizu2_score >= 500 then 'C+' when dp.bizu2_score >= 400 then 'C-' when dp.bizu2_score >= 300 then 'D' when dp.bizu2_score >= 200 then 'E' when dp.bizu2_score < 200 then 'F' else 'No Bizu2' end
		when dp.direct_prospect_id < 503019 then
			case when dp.bizu21_score >= 1200 then 'A+' when dp.bizu21_score >= 950 then 'A-' when dp.bizu21_score >= 750 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score <200 then 'F' else 'No Bizu21' end
		else
			case when dp.bizu21_score >= 1100 then 'A+' when dp.bizu21_score >= 850 then 'A-' when dp.bizu21_score >= 720 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score <200 then 'F' else 'No Bizu21' end
		end	as lead_classe_Bizux,
--GEOFUSION
	microrregiao as geofusion_city_microregion,
	mesorregiao as geofusion_city_mesoregion,
	regiao_geografica geofusion_city_region,
	ric_imediata as geofusion_city_nearest_center,
	ric_intermediaria as geofusion_city_nearest_big_center,
	ric_ampliada as geofusion_city_nearest_capital,
	hierarquia as geofusion_city_hierarchy,
	"area" as geofusion_city_area_size,
	populacao_2017 as geofusion_city_population_2017,
	densidade_pop_2017 as geofusion_city_density_2017,
	domicilios_2017 as geofusion_city_number_of_houses_2017,
	pib_2015 as geofusion_city_gdp_2015_2015,
	pib_per_capta_2015 as geofusion_city_GDP_per_capta_2015,
	idh_2010 as geofusion_city_hdi_2010,
	idh_faixa_2010 as geofusion_city_hdi_2010_class,
	renda_media_2017 as geofusion_city_avg_revenue_2017,
	renda_nominal_2017 as geofusion_city_nominal_revenue_2017,
	pea_2016 as geofusion_city_economicaly_active_population_2016,
	faixa_pea_2016 as geofusion_city_economicaly_active_population_2016_class,
	populacao_ativa_2016 as geofusion_city_active_population_2016,
	idh_educacao_2010 as geofusion_city_hdi_education_2010,
	idh_educacao_faixa_2010 as geofusion_city_hdi_education_2010_class,
	idh_longevidade_2010 as geofusion_city_hdi_longevity_2010,
	idh_longevidade_faixa_2010 as geofusion_city_hdi_longevity_2010_class,
	idh_renda_2010 as geofusion_city_hdi_revenue_2010,
	idh_renda_faixa_2010 as geofusion_city_hdi_revenue_2010_class,
	segmento_municipal as geofusion_city_segmentation,
--BACEN
	total_agencias_uf as bacen_total_bank_branches_in_state,
	total_agencias_municipio as bacen_total_bank_branches_in_city,
	total_agencias_bairro as bacen_total_bank_branches_in_neighbourhood,
	total_agencias_postal as bacen_total_bank_branches_in_zip_code_class,
--------------------------INITIAL OFFER
	initialoff.offer_id as initial_offer_id,
	initialoff.status as initial_offer_status,
	initialoff.date_inserted as initial_offer_date,
	initialoff.interest_rate as initial_offer_interest_rate,
	initialoff.max_number_of_installments as initial_offer_max_term,
	initialoff.max_value as initial_offer_max_value,
	initialoff.count_neg - 1 as count_negotiations,
	initialoff.data_ultimo_contato as initial_offer_last_contact_date,
	replace(initialoff.follow_up,'Carlos Saraiva','Cadu Saraiva') as initial_offer_analista_is,
--------------------------FINAL OFFER
	o.offer_id as final_offer_id,
	o.status as final_offer_status,
	o.date_inserted as final_offer_date,
	o.interest_rate as final_offer_interest_rate,
	o.max_number_of_installments as final_offer_max_term,
	o.max_value as final_offer_max_value,
	o.data_ultimo_contato as final_offer_last_contact_date,
	replace(o.follow_up,'Carlos Saraiva','Cadu Saraiva') as final_offer_analista_is,
--------------------------LOAN
	lr.loan_request_id as loan_request_id,
	lr.status as loan_request_status,
	lr.date_inserted as loan_request_date,
	lr.valor_solicitado as loan_requested_value,
	lr.prazo_solicitado as loan_requested_term,
	dp.previous_loans_count as loan_is_renewal,
	lr.value as loan_net_value,
	li.total_payment as loan_gross_value,
	lr.taxa_final as loan_final_net_interest_rate,
	li.monthly_cet as loan_final_gross_interest_rate,
	lr.number_of_installments as loan_original_final_term,
	(@li.pmt) as loan_original_final_pmt,
	(@li.pmt)/case dp.month_revenue when 0 then null else dp.month_revenue end as loan_original_final_pmt_leverage,
	li.commission as loan_commission_fee,
	lr.loan_date as loan_contract_date,
	extract (year from age(current_date,lr.loan_date)) * 12 + extract(month from age(current_date,lr.loan_date)) as loan_months_on_book,
	lr.portfolio_id::text as loan_portfolio_id,
	ba.bank_id::text as loan_bank_id,
	lr.data_ultimo_contato as loan_last_contact_date,
	lr.analista_responsavel as loan_analyst_responsible,
	case when cp.bizbot > 0 and cp.nao_bizbot = 0 then 1 else 0 end as loan_approved_by_bizbot,
	case when cp.emprestimo_id is not null or lr.status in ('ACCEPTED','Cancelado','REJECTED') or ad.loan_request_id is not null then 1 else 0 end as loan_request_is_all_docs,
	case when lr.status = 'ACCEPTED' then 1 else 0 end as loan_is_loan,
--------------------------SCR HISTORICO PJ
--Divida longo prazo
	case when teve_scr_tirado = 0 then null when coalesce(Ever_long_term_debt_PJ,Ever_CarteiraCredito_PJ) > 0 then 1 else 0 end as scr_Has_Debt_History_PJ, 
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 else coalesce(Ever_long_term_debt_PJ,Ever_CarteiraCredito_PJ) * 1000 end as scr_ever_long_term_debt_PJ,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 else coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) * 1000 end as scr_curr_long_term_debt_PJ,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 else coalesce(long_term_debt_PJ_5M,CarteiraCredito_PJ_5M) * 1000 end as scr_6M_long_term_debt_PJ,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 else coalesce(long_term_debt_PJ_11M,CarteiraCredito_PJ_11M) * 1000 end as scr_12M_long_term_debt_PJ,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 else coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Curr_long_term_debt_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 else coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) / case when amount_requested < 1000 then o.max_value else amount_requested end * 1000 end as scr_Curr_long_term_debt_PJ_over_application_value,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr,0) = 0 then 0 when divida_atual_PJ = 0 then 2019 else coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) / divida_atual_PJ end as scr_curr_long_term_debt_PJ_over_short_term_debt,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 else coalesce(max_long_term_debt_PJ,max_CarteiraCredito_PJ) * 1000 end as scr_max_long_term_debt_pj,
	case when teve_scr_tirado = 0 then null when coalesce(max_long_term_debt_PJ,max_CarteiraCredito_PJ,0) = 0 then 0 else coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) / coalesce(max_long_term_debt_PJ,max_CarteiraCredito_PJ,0) end as scr_curr_over_max_long_term_debt_pj,
	case when teve_scr_tirado = 0 then null when coalesce(max_long_term_debt_PJ,max_CarteiraCredito_PJ,0) = 0 then 0 else case when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) = coalesce(max_long_term_debt_PJ,max_CarteiraCredito_PJ,0) then 1 else 0 end end as scr_All_TimeHigh_long_term_debt_pj,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 else (coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) - coalesce(long_term_debt_PJ_5M,CarteiraCredito_PJ_5M)) / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_var_abs_6M_long_term_debt_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 else (coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) - coalesce(long_term_debt_PJ_11M,CarteiraCredito_PJ_11M))/case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_var_abs_12M_long_term_debt_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr,0) = 0 then 0 when coalesce(long_term_debt_PJ_5M,CarteiraCredito_PJ_5M) = 0 then 2019 else coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) / coalesce(long_term_debt_PJ_5M,CarteiraCredito_PJ_5M) - 1 end as scr_var_rel_6M_long_term_debt_pj,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr,0) = 0 then 0 when coalesce(long_term_debt_PJ_11M,CarteiraCredito_PJ_11M) = 0 then 2019 else coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) / coalesce(long_term_debt_PJ_11M,CarteiraCredito_PJ_11M) - 1 end as scr_var_rel_12M_long_term_debt_pj,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 else coalesce(Meses_Aumento_DividaPJ,Meses_Aumento_CarteiraCredito_PJ) / (case when coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) <= 1 then 1 else coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) - 1 end) end as scr_Months_Rise_long_term_debt_pj_over_total_months,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 else coalesce(Meses_Reducao_DividaPJ,Meses_Reducao_CarteiraCredito_PJ) / (case when coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) <= 1 then 1 else coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) - 1 end) end as scr_Months_Dive_long_term_debt_pj_over_total_months,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 else coalesce(months_sth_PJ,months_sth_CarteiraCredito_PJ) / (case when coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) <= 1 then 1 else coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) - 1 end) end as scr_month_consecutive_dive_long_term_debt_pj_over_total_months,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 else coalesce(months_hth_PJ,months_hth_CarteiraCredito_PJ) / (case when coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) <= 1 then 1 else coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) - 1 end) end as scr_month_consecutive_rise_long_term_debt_pj_over_total_months,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 else coalesce(Saldo_Amort_DividaPJ,Saldo_Amort_CarteiraCredito_PJ) * 1000 end as scr_long_term_debt_amortization_PJ,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 when coalesce(Saldo_Amort_DividaPJ,Saldo_Amort_CarteiraCredito_PJ) = 0 then 0 else coalesce(Saldo_Amort_DividaPJ,Saldo_Amort_CarteiraCredito_PJ) / coalesce(Meses_Reducao_DividaPJ,Meses_Reducao_CarteiraCredito_PJ) / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_LTDebt_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 when coalesce(Saldo_Amort_DividaPJ,Saldo_Amort_CarteiraCredito_PJ) = 0 then 0 else coalesce(Saldo_Amort_DividaPJ,Saldo_Amort_CarteiraCredito_PJ) / coalesce(Meses_Reducao_DividaPJ,Meses_Reducao_CarteiraCredito_PJ) / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_LTDebt_PJ_over_loan_PMT,
--Vencido
	case when teve_scr_tirado = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Overdue_PJ_Curr * 1000 end as scr_curr_overdue_debt_PJ,
	case when teve_scr_tirado = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Max_Overdue_PJ * 1000 end as scr_max_overdue_debt_PJ,
	case when teve_scr_tirado = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Months_Overdue_PJ / Qtd_meses_escopo_PJ end as scr_Months_Overdue_PJ_over_total_months,
	case when teve_scr_tirado = 0 then null when Long_Term_Debt_PJ_Curr is null then -1 when Months_Since_Last_Overdue_PJ is null then - 1 else Months_Since_Last_Overdue_PJ end as scr_Months_since_last_Overdue_PJ,
	case when teve_scr_tirado = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Max_Overdue_PJ / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Max_Overdue_PJ_over_Revenue,
	case when teve_scr_tirado = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Overdue_PJ_Curr / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Curr_Overdue_PJ_over_Revenue,
	case when teve_scr_tirado = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 when Overdue_PJ_Curr = 0 then 0 when Long_Term_Debt_PJ_Curr = 0 then 2019 else Overdue_PJ_Curr / Long_Term_Debt_PJ_Curr end as scr_Curr_Overdue_PJ_long_term_debt,
--Prejuizo
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 else coalesce(Default_PJ_Curr,Prejuizo_PJ_Curr) * 1000 end as scr_curr_default_debt_PJ,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 else coalesce(Max_Default_PJ,Max_Prejuizo_PJ) * 1000 end as scr_max_default_debt_PJ,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 else coalesce(Months_Default_PJ,Meses_Prejuizo_PJ) / coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) end as scr_Months_Default_PJ_over_total_months,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr,Months_Since_Last_Default_PJ,Meses_Desde_Ultimo_Prejuizo_PJ) is null then -1 else Months_Since_Last_Default_PJ end as scr_Months_since_last_Default_PJ,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 else coalesce(Max_Default_PJ,Max_Prejuizo_PJ) / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Max_Default_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) is null then 0 else coalesce(Default_PJ_Curr,Prejuizo_PJ_Curr) / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Curr_Default_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when coalesce(Default_PJ_Curr,Prejuizo_PJ_Curr,0) = 0 then 0 when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) = 0 then 2019 else coalesce(Default_PJ_Curr,Prejuizo_PJ_Curr)/coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) end as scr_Curr_Default_PJ_over_long_term_debt,
--Limite
	case when teve_scr_tirado = 0 then null when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) is null then 0 else coalesce(Ever_Lim_Cred_PJ,Ever_LimiteCredito_PJ) * 1000 end as scr_ever_Lim_Cred_PJ,
	case when teve_scr_tirado = 0 then null when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) is null then 0 else coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) * 1000 end as scr_curr_Lim_Cred_PJ,
	case when teve_scr_tirado = 0 then null when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) is null then 0 else coalesce(Lim_Cred_PJ_5M,LimiteCredito_PJ_5M) * 1000 end as scr_6M_Lim_Cred_PJ,
	case when teve_scr_tirado = 0 then null when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) is null then 0 else coalesce(Lim_Cred_PJ_11M,LimiteCredito_PJ_11M) * 1000 end as scr_12M_Lim_Cred_PJ,
	case when teve_scr_tirado = 0 then null when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) is null then 0 else coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Curr_Lim_Cred_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) is null then 0 else coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) / case when amount_requested < 1000 then o.max_value else amount_requested end * 1000 end as scr_Curr_Lim_Cred_PJ_over_application_value,
	case when teve_scr_tirado = 0 then null when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr,0) = 0 then 0 when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) > coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) then 1 when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) = 0 then 2019 else coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr)/coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) end as scr_curr_Lim_Cred_PJ_over_long_term_debt,
	case when teve_scr_tirado = 0 then null when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr,0) = 0 then 0 when divida_atual_PJ = 0 then 2019 else coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) / divida_atual_PJ end as scr_curr_Lim_Cred_PJ_over_short_term_debt,
	case when teve_scr_tirado = 0 then null when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) is null then 0 else coalesce(max_Lim_Cred_PJ,max_LimiteCredito_PJ) * 1000 end as scr_max_Lim_Cred_PJ_debt,
	case when teve_scr_tirado = 0 then null when coalesce(max_Lim_Cred_PJ,max_LimiteCredito_PJ,0) = 0 then 0 else coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) / coalesce(max_Lim_Cred_PJ,max_LimiteCredito_PJ,0) end as scr_curr_over_max_Lim_Cred_PJ_debt,
	case when teve_scr_tirado = 0 then null when coalesce(max_Lim_Cred_PJ,max_LimiteCredito_PJ,0) = 0 then 0 else case when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) = coalesce(max_Lim_Cred_PJ,max_LimiteCredito_PJ,0) then 1 else 0 end end as scr_All_TimeHigh_Lim_Cred_PJ_debt,
	case when teve_scr_tirado = 0 then null when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) is null then 0 else (coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) - coalesce(Lim_Cred_PJ_5M,LimiteCredito_PJ_5M))/case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_var_abs_6M_Lim_Cred_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) is null then 0 else (coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) - coalesce(Lim_Cred_PJ_11M,LimiteCredito_PJ_11M))/case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_var_abs_12M_Lim_Cred_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr,0) = 0 then 0 when coalesce(Lim_Cred_PJ_5M,LimiteCredito_PJ_5M) = 0 then 2019 else coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) / coalesce(Lim_Cred_PJ_5M,LimiteCredito_PJ_5M) - 1 end as scr_var_rel_6M_Lim_Cred_PJ_debt,
	case when teve_scr_tirado = 0 then null when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr,0) = 0 then 0 when coalesce(Lim_Cred_PJ_11M,LimiteCredito_PJ_11M) = 0 then 2019 else coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) / coalesce(Lim_Cred_PJ_11M,LimiteCredito_PJ_11M) - 1 end as scr_var_rel_12M_Lim_Cred_PJ_debt,
	case when teve_scr_tirado = 0 then null when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) is null then 0 else coalesce(Meses_Aumento_Lim_Cred_PJ,Meses_Aumento_LimiteCredito_PJ) / (case when coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) <= 1 then 1 else coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) - 1 end) end as scr_Months_Rise_Lim_Cred_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) is null then 0 else coalesce(Meses_Reducao_Lim_Cred_PJ,Meses_Reducao_LimiteCredito_PJ) / (case when coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) <= 1 then 1 else coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) - 1 end) end as scr_Months_Dive_Lim_Cred_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) is null then 0 else coalesce(months_sth_lim_cred_PJ,months_sth_LimiteCredito_PJ) / (case when coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) <= 1 then 1 else coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) - 1 end) end as scr_month_consecutive_dive_Lim_Cred_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) is null then 0 else coalesce(months_hth_lim_cred_PJ,months_hth_LimiteCredito_PJ) / (case when coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) <= 1 then 1 else coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) - 1 end) end as scr_month_consecutive_rise_Lim_Cred_PJ_debt_over_total_months,
--Operacoes
	case when teve_scr_tirado = 0 then null else case when Num_Ops_PJ is null then 0 else Num_Ops_PJ end end as scr_Num_Fin_Ops_PJ,
	case when teve_scr_tirado = 0 then null else case when Num_FIs_PJ is null then 0 else Num_FIs_PJ end end as scr_Num_Fin_Inst_PJ,
	case when teve_scr_tirado = 0 then null else case when First_Relation_FI_PJ is null then 0 else First_Relation_FI_PJ end end as scr_First_Relation_Fin_Inst_PJ,
--------------------------SCR HISTORICO PF
--Divida longo prazo
	case when teve_scr_pf_tirado = 0 then null when coalesce(Ever_long_term_debt_PF,Ever_CarteiraCredito_PF) > 0 then 1 else 0 end as scr_Has_Debt_History_PF, 
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 else coalesce(Ever_long_term_debt_PF,Ever_CarteiraCredito_PF) * 1000 end as scr_ever_long_term_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 else coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) * 1000 end as scr_curr_long_term_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 else coalesce(long_term_debt_PF_5M,CarteiraCredito_PF_5M) * 1000 end as scr_6M_long_term_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 else coalesce(long_term_debt_PF_11M,CarteiraCredito_PF_11M) * 1000 end as scr_12M_long_term_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 else coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Curr_long_term_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 else coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) / case when amount_requested < 1000 then o.max_value else amount_requested end * 1000 end as scr_Curr_long_term_debt_PF_over_application_value,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr,0) = 0 then 0 when divida_atual_PF = 0 then 2019 else coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) / divida_atual_PF end as scr_curr_long_term_debt_PF_over_short_term_debt,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 else coalesce(max_long_term_debt_PF,max_CarteiraCredito_PF) * 1000 end as scr_max_long_term_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when coalesce(max_long_term_debt_PF,max_CarteiraCredito_PF,0) = 0 then 0 else coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) / coalesce(max_long_term_debt_PF,max_CarteiraCredito_PF,0) end as scr_curr_over_max_long_term_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when coalesce(max_long_term_debt_PF,max_CarteiraCredito_PF,0) = 0 then 0 else case when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) = coalesce(max_long_term_debt_PF,max_CarteiraCredito_PF,0) then 1 else 0 end end as scr_All_TimeHigh_long_term_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 else (coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) - coalesce(long_term_debt_PF_5M,CarteiraCredito_PF_5M)) / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_var_abs_6M_long_term_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 else (coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) - coalesce(long_term_debt_PF_11M,CarteiraCredito_PF_11M))/case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_var_abs_12M_long_term_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr,0) = 0 then 0 when coalesce(long_term_debt_PF_5M,CarteiraCredito_PF_5M) = 0 then 2019 else coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) / coalesce(long_term_debt_PF_5M,CarteiraCredito_PF_5M) - 1 end as scr_var_rel_6M_long_term_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr,0) = 0 then 0 when coalesce(long_term_debt_PF_11M,CarteiraCredito_PF_11M) = 0 then 2019 else coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) / coalesce(long_term_debt_PF_11M,CarteiraCredito_PF_11M) - 1 end as scr_var_rel_12M_long_term_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 else coalesce(Meses_Aumento_DividaPF,Meses_Aumento_CarteiraCredito_PF) / (case when coalesce(qtd_meses_escopo_PF,qtd_meses_modalidade_PF) <= 1 then 1 else coalesce(qtd_meses_escopo_PF,qtd_meses_modalidade_PF) - 1 end) end as scr_Months_Rise_long_term_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 else coalesce(Meses_Reducao_DividaPF,Meses_Reducao_CarteiraCredito_PF) / (case when coalesce(qtd_meses_escopo_PF,qtd_meses_modalidade_PF) <= 1 then 1 else coalesce(qtd_meses_escopo_PF,qtd_meses_modalidade_PF) - 1 end) end as scr_Months_Dive_long_term_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 else coalesce(months_sth_PF,months_sth_CarteiraCredito_PF) / (case when coalesce(qtd_meses_escopo_PF,qtd_meses_modalidade_PF) <= 1 then 1 else coalesce(qtd_meses_escopo_PF,qtd_meses_modalidade_PF) - 1 end) end as scr_month_consecutive_dive_long_term_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 else coalesce(months_hth_PF,months_hth_CarteiraCredito_PF) / (case when coalesce(qtd_meses_escopo_PF,qtd_meses_modalidade_PF) <= 1 then 1 else coalesce(qtd_meses_escopo_PF,qtd_meses_modalidade_PF) - 1 end) end as scr_month_consecutive_rise_long_term_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 else coalesce(Saldo_Amort_DividaPF,Saldo_Amort_CarteiraCredito_PF) * 1000 end as scr_long_term_debt_amortization_PF,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 when coalesce(Saldo_Amort_DividaPF,Saldo_Amort_CarteiraCredito_PF) = 0 then 0 else coalesce(Saldo_Amort_DividaPF,Saldo_Amort_CarteiraCredito_PF) / coalesce(Meses_Reducao_DividaPF,Meses_Reducao_CarteiraCredito_PF) / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_LTDebt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 when coalesce(Saldo_Amort_DividaPF,Saldo_Amort_CarteiraCredito_PF) = 0 then 0 else coalesce(Saldo_Amort_DividaPF,Saldo_Amort_CarteiraCredito_PF) / coalesce(Meses_Reducao_DividaPF,Meses_Reducao_CarteiraCredito_PF) / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_LTDebt_PF_over_loan_PMT,
--Vencido
	case when teve_scr_pf_tirado = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Overdue_PF_Curr * 1000 end as scr_curr_overdue_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Max_Overdue_PF * 1000 end as scr_max_overdue_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Months_Overdue_PF / Qtd_meses_escopo_PF end as scr_Months_Overdue_PF_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when Long_Term_Debt_PF_Curr is null then -1 when Months_Since_Last_Overdue_PF is null then - 1 else Months_Since_Last_Overdue_PF end as scr_Months_since_last_Overdue_PF,
	case when teve_scr_pf_tirado = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Max_Overdue_PF / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Max_Overdue_PF_over_Revenue,
	case when teve_scr_pf_tirado = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Overdue_PF_Curr / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Curr_Overdue_PF_over_Revenue,
	case when teve_scr_pf_tirado = 0 then null when Long_Term_Debt_PF_Curr is null then 0 when Overdue_PF_Curr = 0 then 0 when Long_Term_Debt_PF_Curr = 0 then 2019 else Overdue_PF_Curr / Long_Term_Debt_PF_Curr end as scr_Curr_Overdue_PF_long_term_debt,
--Prejuizo
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 else coalesce(Default_PF_Curr,Prejuizo_PF_Curr) * 1000 end as scr_curr_default_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 else coalesce(Max_Default_PF,Max_Prejuizo_PF) * 1000 end as scr_max_default_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 else coalesce(Months_Default_PF,Meses_Prejuizo_PF) / coalesce(qtd_meses_escopo_PF,qtd_meses_modalidade_PF) end as scr_Months_Default_PF_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr,Months_Since_Last_Default_PF,Meses_Desde_Ultimo_Prejuizo_PF) is null then -1 else Months_Since_Last_Default_PF end as scr_Months_since_last_Default_PF,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 else coalesce(Max_Default_PF,Max_Prejuizo_PF) / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Max_Default_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) is null then 0 else coalesce(Default_PF_Curr,Prejuizo_PF_Curr) / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Curr_Default_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when coalesce(Default_PF_Curr,Prejuizo_PF_Curr,0) = 0 then 0 when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) = 0 then 2019 else coalesce(Default_PF_Curr,Prejuizo_PF_Curr)/coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) end as scr_Curr_Default_PF_over_long_term_debt,
--Limite
	case when teve_scr_pf_tirado = 0 then null when coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) is null then 0 else coalesce(Ever_Lim_Cred_PF,Ever_LimiteCredito_PF) * 1000 end as scr_ever_Lim_Cred_PF,
	case when teve_scr_pf_tirado = 0 then null when coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) is null then 0 else coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) * 1000 end as scr_curr_Lim_Cred_PF,
	case when teve_scr_pf_tirado = 0 then null when coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) is null then 0 else coalesce(Lim_Cred_PF_5M,LimiteCredito_PF_5M) * 1000 end as scr_6M_Lim_Cred_PF,
	case when teve_scr_pf_tirado = 0 then null when coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) is null then 0 else coalesce(Lim_Cred_PF_11M,LimiteCredito_PF_11M) * 1000 end as scr_12M_Lim_Cred_PF,
	case when teve_scr_pf_tirado = 0 then null when coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) is null then 0 else coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Curr_Lim_Cred_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) is null then 0 else coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) / case when amount_requested < 1000 then o.max_value else amount_requested end * 1000 end as scr_Curr_Lim_Cred_PF_over_application_value,
	case when teve_scr_pf_tirado = 0 then null when coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr,0) = 0 then 0 when coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) > coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) then 1 when coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) = 0 then 2019 else coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr)/coalesce(long_term_debt_PF_Curr,CarteiraCredito_PF_Curr) end as scr_curr_Lim_Cred_PF_over_long_term_debt,
	case when teve_scr_pf_tirado = 0 then null when coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr,0) = 0 then 0 when divida_atual_PF = 0 then 2019 else coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) / divida_atual_PF end as scr_curr_Lim_Cred_PF_over_short_term_debt,
	case when teve_scr_pf_tirado = 0 then null when coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) is null then 0 else coalesce(max_Lim_Cred_PF,max_LimiteCredito_PF) * 1000 end as scr_max_Lim_Cred_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when coalesce(max_Lim_Cred_PF,max_LimiteCredito_PF,0) = 0 then 0 else coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) / coalesce(max_Lim_Cred_PF,max_LimiteCredito_PF,0) end as scr_curr_over_max_Lim_Cred_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when coalesce(max_Lim_Cred_PF,max_LimiteCredito_PF,0) = 0 then 0 else case when coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) = coalesce(max_Lim_Cred_PF,max_LimiteCredito_PF,0) then 1 else 0 end end as scr_All_TimeHigh_Lim_Cred_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) is null then 0 else (coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) - coalesce(Lim_Cred_PF_5M,LimiteCredito_PF_5M))/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_Lim_Cred_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) is null then 0 else (coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) - coalesce(Lim_Cred_PF_11M,LimiteCredito_PF_11M))/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_Lim_Cred_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr,0) = 0 then 0 when coalesce(Lim_Cred_PF_5M,LimiteCredito_PF_5M) = 0 then 2019 else coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) / coalesce(Lim_Cred_PF_5M,LimiteCredito_PF_5M) - 1 end as scr_var_rel_6M_Lim_Cred_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr,0) = 0 then 0 when coalesce(Lim_Cred_PF_11M,LimiteCredito_PF_11M) = 0 then 2019 else coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) / coalesce(Lim_Cred_PF_11M,LimiteCredito_PF_11M) - 1 end as scr_var_rel_12M_Lim_Cred_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) is null then 0 else coalesce(Meses_Aumento_Lim_Cred_PF,Meses_Aumento_LimiteCredito_PF) / (case when coalesce(qtd_meses_escopo_PF,qtd_meses_modalidade_PF) <= 1 then 1 else coalesce(qtd_meses_escopo_PF,qtd_meses_modalidade_PF) - 1 end) end as scr_Months_Rise_Lim_Cred_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) is null then 0 else coalesce(Meses_Reducao_Lim_Cred_PF,Meses_Reducao_LimiteCredito_PF) / (case when coalesce(qtd_meses_escopo_PF,qtd_meses_modalidade_PF) <= 1 then 1 else coalesce(qtd_meses_escopo_PF,qtd_meses_modalidade_PF) - 1 end) end as scr_Months_Dive_Lim_Cred_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) is null then 0 else coalesce(months_sth_lim_cred_PF,months_sth_LimiteCredito_PF) / (case when coalesce(qtd_meses_escopo_PF,qtd_meses_modalidade_PF) <= 1 then 1 else coalesce(qtd_meses_escopo_PF,qtd_meses_modalidade_PF) - 1 end) end as scr_month_consecutive_dive_Lim_Cred_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when coalesce(Lim_Cred_PF_Curr,LimiteCredito_PF_Curr) is null then 0 else coalesce(months_hth_lim_cred_PF,months_hth_LimiteCredito_PF) / (case when coalesce(qtd_meses_escopo_PF,qtd_meses_modalidade_PF) <= 1 then 1 else coalesce(qtd_meses_escopo_PF,qtd_meses_modalidade_PF) - 1 end) end as scr_month_consecutive_rise_Lim_Cred_PF_debt_over_total_months,
--Operacoes
	case when teve_scr_pf_tirado = 0 then null else case when Num_Ops_PF is null then 0 else Num_Ops_PF end end as scr_Num_Fin_Ops_PF,
	case when teve_scr_pf_tirado = 0 then null else case when Num_FIs_PF is null then 0 else Num_FIs_PF end end as scr_Num_Fin_Inst_PF,
	case when teve_scr_pf_tirado = 0 then null else case when First_Relation_FI_PF is null then 0 else First_Relation_FI_PF end end as scr_First_Relation_Fin_Inst_PF,
--------------------------SCR PJ MODALIDADE: INFO GERAL
	case when teve_scr_tirado = 0 then null else divida_atual_pj end as scr_short_term_debt_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null then 0 else qtd_meses_modalidade_pj end as scr_number_of_months_in_report_pj,
--------------------------SCR PJ MODALIDADE: EMPRESTIMOS TOTAL
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null then 0 else count_emprestimos_PJ end as scr_count_all_loans_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null then 0 else Ever_emprestimos_PJ * 1000 end as scr_ever_all_loans_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null then 0 else emprestimos_PJ_Curr * 1000 end as scr_curr_all_loans_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null then 0 else emprestimos_PJ_5M * 1000 end as scr_6M_all_loans_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null then 0 else emprestimos_PJ_11M * 1000 end as scr_12M_all_loans_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null then 0 else emprestimos_PJ_Curr / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Curr_all_loans_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null or emprestimos_PJ_Curr = 0 then 0 when emprestimos_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 when Long_Term_Debt_PJ_Curr = 0 then 2019 else emprestimos_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_curr_all_loans_PJ_over_long_term_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null or emprestimos_PJ_Curr = 0 then 0 when divida_atual_PJ = 0 then 2019 else emprestimos_PJ_Curr/divida_atual_PJ end as scr_curr_all_loans_PJ_over_short_term_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null then 0 else max_emprestimos_PJ * 1000 end as scr_max_all_loans_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null or max_emprestimos_PJ = 0 then 0 else emprestimos_PJ_Curr / max_emprestimos_PJ end as scr_curr_over_max_all_loans_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null or max_emprestimos_PJ = 0 then 0 else case when emprestimos_PJ_Curr = max_emprestimos_PJ then 1 else 0 end end as scr_All_TimeHigh_all_loans_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null then 0 else (emprestimos_PJ_Curr - emprestimos_PJ_5M) / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_var_abs_6M_all_loans_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null then 0 else (emprestimos_PJ_Curr - emprestimos_PJ_11M) / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_var_abs_12M_all_loans_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null or emprestimos_PJ_Curr = 0 then 0 when emprestimos_PJ_5M = 0 then 2019 else emprestimos_PJ_Curr / emprestimos_PJ_5M - 1 end as scr_var_rel_6M_all_loans_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null or emprestimos_PJ_Curr = 0 then 0 when emprestimos_PJ_11M = 0 then 2019 else emprestimos_PJ_Curr / emprestimos_PJ_11M - 1 end as scr_var_rel_12M_all_loans_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null then 0 else Meses_Aumento_emprestimos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ - 1 end) end as scr_Months_Rise_all_loans_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null then 0 else Meses_Reducao_emprestimos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ - 1 end) end as scr_Months_Dive_all_loans_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null then 0 else months_sth_emprestimos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ - 1 end) end as scr_month_consecutive_dive_all_loans_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null then 0 else months_hth_emprestimos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ - 1 end) end as scr_month_consecutive_rise_all_loans_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null then 0 else Saldo_Amort_emprestimos_PJ * 1000 end as scr_all_loans_amortization_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null then 0 when Saldo_Amort_emprestimos_PJ = 0 then 0 else Saldo_Amort_emprestimos_PJ / Meses_Reducao_emprestimos_PJ / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_all_loans_pj_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_PJ_Curr is null then 0 when Saldo_Amort_emprestimos_PJ = 0 then 0 else Saldo_Amort_emprestimos_PJ / Meses_Reducao_emprestimos_PJ / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_all_loans_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: EMPRESTIMOS CHEQUE ESPECIAL
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null then 0 else count_emprestimos_cheque_especial_PJ end as scr_count_overdraft_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null then 0 else Ever_emprestimos_cheque_especial_PJ * 1000 end as scr_ever_overdraft_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null then 0 else emprestimos_cheque_especial_PJ_Curr * 1000 end as scr_curr_overdraft_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null then 0 else emprestimos_cheque_especial_PJ_5M * 1000 end as scr_6M_overdraft_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null then 0 else emprestimos_cheque_especial_PJ_11M * 1000 end as scr_12M_overdraft_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null then 0 else emprestimos_cheque_especial_PJ_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_overdraft_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null or emprestimos_cheque_especial_PJ_Curr = 0 then 0 when emprestimos_cheque_especial_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 when Long_Term_Debt_PJ_Curr = 0 then 2019 else emprestimos_cheque_especial_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_curr_overdraft_PJ_over_long_term_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null or emprestimos_cheque_especial_PJ_Curr = 0 then 0 when divida_atual_PJ = 0 then 2019 else emprestimos_cheque_especial_PJ_Curr/divida_atual_PJ end as scr_curr_overdraft_PJ_over_short_term_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null then 0 else max_emprestimos_cheque_especial_PJ * 1000 end as scr_max_overdraft_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null or max_emprestimos_cheque_especial_PJ = 0 then 0 else emprestimos_cheque_especial_PJ_Curr / max_emprestimos_cheque_especial_PJ end as scr_curr_over_max_overdraft_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null or max_emprestimos_cheque_especial_PJ = 0 then 0 else case when emprestimos_cheque_especial_PJ_Curr = max_emprestimos_cheque_especial_PJ then 1 else 0 end end as scr_All_TimeHigh_overdraft_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null then 0 else (emprestimos_cheque_especial_PJ_Curr - emprestimos_cheque_especial_PJ_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_overdraft_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null then 0 else (emprestimos_cheque_especial_PJ_Curr - emprestimos_cheque_especial_PJ_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_overdraft_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null or emprestimos_cheque_especial_PJ_Curr = 0 then 0 when emprestimos_cheque_especial_PJ_5M = 0 then 2019 else emprestimos_cheque_especial_PJ_Curr / emprestimos_cheque_especial_PJ_5M - 1 end as scr_var_rel_6M_overdraft_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null or emprestimos_cheque_especial_PJ_Curr = 0 then 0 when emprestimos_cheque_especial_PJ_11M = 0 then 2019 else emprestimos_cheque_especial_PJ_Curr / emprestimos_cheque_especial_PJ_11M - 1 end as scr_var_rel_12M_overdraft_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null then 0 else Meses_Aumento_emprestimos_cheque_especial_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Rise_overdraft_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null then 0 else Meses_Reducao_emprestimos_cheque_especial_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Dive_overdraft_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null then 0 else months_sth_emprestimos_cheque_especial_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_dive_overdraft_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null then 0 else months_hth_emprestimos_cheque_especial_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_rise_overdraft_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null then 0 else Saldo_Amort_emprestimos_cheque_especial_PJ * 1000 end as scr_overdraft_amortization_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null then 0 when Saldo_Amort_emprestimos_cheque_especial_PJ = 0 then 0 else Saldo_Amort_emprestimos_cheque_especial_PJ / Meses_Reducao_emprestimos_cheque_especial_PJ / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_overdraft_pj_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_cheque_especial_PJ_Curr is null then 0 when Saldo_Amort_emprestimos_cheque_especial_PJ = 0 then 0 else Saldo_Amort_emprestimos_cheque_especial_PJ / Meses_Reducao_emprestimos_cheque_especial_PJ / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_overdraft_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: EMPRESTIMOS CAIPTAL DE GIRO LONGO PRAZO
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null then 0 else count_emprestimos_giro_longo_PJ end as scr_count_long_term_work_capital_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null then 0 else Ever_emprestimos_giro_longo_PJ * 1000 end as scr_ever_long_term_work_capital_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null then 0 else emprestimos_giro_longo_PJ_Curr * 1000 end as scr_curr_long_term_work_capital_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null then 0 else emprestimos_giro_longo_PJ_5M * 1000 end as scr_6M_long_term_work_capital_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null then 0 else emprestimos_giro_longo_PJ_11M * 1000 end as scr_12M_long_term_work_capital_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null then 0 else emprestimos_giro_longo_PJ_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_long_term_work_capital_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null or emprestimos_giro_longo_PJ_Curr = 0 then 0 when emprestimos_giro_longo_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 when Long_Term_Debt_PJ_Curr = 0 then 2019 else emprestimos_giro_longo_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_curr_long_term_work_capital_PJ_over_long_term_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null or emprestimos_giro_longo_PJ_Curr = 0 then 0 when divida_atual_PJ = 0 then 2019 else emprestimos_giro_longo_PJ_Curr/divida_atual_PJ end as scr_curr_long_term_work_capital_PJ_over_short_term_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null then 0 else max_emprestimos_giro_longo_PJ * 1000 end as scr_max_long_term_work_capital_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null or max_emprestimos_giro_longo_PJ = 0 then 0 else emprestimos_giro_longo_PJ_Curr / max_emprestimos_giro_longo_PJ end as scr_curr_over_max_long_term_work_capital_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null or max_emprestimos_giro_longo_PJ = 0 then 0 else case when emprestimos_giro_longo_PJ_Curr = max_emprestimos_giro_longo_PJ then 1 else 0 end end as scr_All_TimeHigh_long_term_work_capital_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null then 0 else (emprestimos_giro_longo_PJ_Curr - emprestimos_giro_longo_PJ_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_long_term_work_capital_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null then 0 else (emprestimos_giro_longo_PJ_Curr - emprestimos_giro_longo_PJ_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_long_term_work_capital_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null or emprestimos_giro_longo_PJ_Curr = 0 then 0 when emprestimos_giro_longo_PJ_5M = 0 then 2019 else emprestimos_giro_longo_PJ_Curr / emprestimos_giro_longo_PJ_5M - 1 end as scr_var_rel_6M_long_term_work_capital_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null or emprestimos_giro_longo_PJ_Curr = 0 then 0 when emprestimos_giro_longo_PJ_11M = 0 then 2019 else emprestimos_giro_longo_PJ_Curr / emprestimos_giro_longo_PJ_11M - 1 end as scr_var_rel_12M_long_term_work_capital_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null then 0 else Meses_Aumento_emprestimos_giro_longo_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Rise_long_term_work_capital_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null then 0 else Meses_Reducao_emprestimos_giro_longo_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Dive_long_term_work_capital_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null then 0 else months_sth_emprestimos_giro_longo_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_dive_long_term_work_capital_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null then 0 else months_hth_emprestimos_giro_longo_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_rise_long_term_work_capital_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null then 0 else Saldo_Amort_emprestimos_giro_longo_PJ * 1000 end as scr_long_term_work_capital_amortization_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null then 0 when Saldo_Amort_emprestimos_giro_longo_PJ = 0 then 0 else Saldo_Amort_emprestimos_giro_longo_PJ / Meses_Reducao_emprestimos_giro_longo_PJ / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_long_term_work_capital_pj_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_longo_PJ_Curr is null then 0 when Saldo_Amort_emprestimos_giro_longo_PJ = 0 then 0 else Saldo_Amort_emprestimos_giro_longo_PJ / Meses_Reducao_emprestimos_giro_longo_PJ / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_long_term_work_capital_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: EMPRESTIMOS CAPITAL DE GIRO CURTO PRAZO
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null then 0 else count_emprestimos_giro_curto_PJ end as scr_count_short_term_work_capital_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null then 0 else Ever_emprestimos_giro_curto_PJ * 1000 end as scr_ever_short_term_work_capital_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null then 0 else emprestimos_giro_curto_PJ_Curr * 1000 end as scr_curr_short_term_work_capital_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null then 0 else emprestimos_giro_curto_PJ_5M * 1000 end as scr_6M_short_term_work_capital_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null then 0 else emprestimos_giro_curto_PJ_11M * 1000 end as scr_12M_short_term_work_capital_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null then 0 else emprestimos_giro_curto_PJ_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_short_term_work_capital_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null or emprestimos_giro_curto_PJ_Curr = 0 then 0 when emprestimos_giro_curto_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 when Long_Term_Debt_PJ_Curr = 0 then 2019 else emprestimos_giro_curto_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_curr_short_term_work_capital_PJ_over_long_term_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null or emprestimos_giro_curto_PJ_Curr = 0 then 0 when divida_atual_PJ = 0 then 2019 else emprestimos_giro_curto_PJ_Curr/divida_atual_PJ end as scr_curr_short_term_work_capital_PJ_over_short_term_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null then 0 else max_emprestimos_giro_curto_PJ * 1000 end as scr_max_short_term_work_capital_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null or max_emprestimos_giro_curto_PJ = 0 then 0 else emprestimos_giro_curto_PJ_Curr / max_emprestimos_giro_curto_PJ end as scr_curr_over_max_short_term_work_capital_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null or max_emprestimos_giro_curto_PJ = 0 then 0 else case when emprestimos_giro_curto_PJ_Curr = max_emprestimos_giro_curto_PJ then 1 else 0 end end as scr_All_TimeHigh_short_term_work_capital_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null then 0 else (emprestimos_giro_curto_PJ_Curr - emprestimos_giro_curto_PJ_5M) / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_var_abs_6M_short_term_work_capital_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null then 0 else (emprestimos_giro_curto_PJ_Curr - emprestimos_giro_curto_PJ_11M) / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_var_abs_12M_short_term_work_capital_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null or emprestimos_giro_curto_PJ_Curr = 0 then 0 when emprestimos_giro_curto_PJ_5M = 0 then 2019 else emprestimos_giro_curto_PJ_Curr / emprestimos_giro_curto_PJ_5M - 1 end as scr_var_rel_6M_short_term_work_capital_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null or emprestimos_giro_curto_PJ_Curr = 0 then 0 when emprestimos_giro_curto_PJ_11M = 0 then 2019 else emprestimos_giro_curto_PJ_Curr / emprestimos_giro_curto_PJ_11M - 1 end as scr_var_rel_12M_short_term_work_capital_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null then 0 else Meses_Aumento_emprestimos_giro_curto_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ - 1 end) end as scr_Months_Rise_short_term_work_capital_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null then 0 else Meses_Reducao_emprestimos_giro_curto_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ - 1 end) end as scr_Months_Dive_short_term_work_capital_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null then 0 else months_sth_emprestimos_giro_curto_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ - 1 end) end as scr_month_consecutive_dive_short_term_work_capital_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null then 0 else months_hth_emprestimos_giro_curto_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ - 1 end) end as scr_month_consecutive_rise_short_term_work_capital_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null then 0 else Saldo_Amort_emprestimos_giro_curto_PJ * 1000 end as scr_short_term_work_capital_amortization_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null then 0 when Saldo_Amort_emprestimos_giro_curto_PJ = 0 then 0 else Saldo_Amort_emprestimos_giro_curto_PJ / Meses_Reducao_emprestimos_giro_curto_PJ / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_short_term_work_capital_pj_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_curto_PJ_Curr is null then 0 when Saldo_Amort_emprestimos_giro_curto_PJ = 0 then 0 else Saldo_Amort_emprestimos_giro_curto_PJ / Meses_Reducao_emprestimos_giro_curto_PJ / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_short_term_work_capital_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: EMPRESTIMOS CARTÃƒO DE CRÃ‰DITO
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null then 0 else count_emprestimos_cartao_credito_PJ end as scr_count_credit_card_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null then 0 else Ever_emprestimos_cartao_credito_PJ * 1000 end as scr_ever_credit_card_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null then 0 else emprestimos_cartao_credito_PJ_Curr * 1000 end as scr_curr_credit_card_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null then 0 else emprestimos_cartao_credito_PJ_5M * 1000 end as scr_6M_credit_card_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null then 0 else emprestimos_cartao_credito_PJ_11M * 1000 end as scr_12M_credit_card_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null then 0 else emprestimos_cartao_credito_PJ_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_credit_card_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null or emprestimos_cartao_credito_PJ_Curr = 0 then 0 when emprestimos_cartao_credito_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 when Long_Term_Debt_PJ_Curr = 0 then 2019 else emprestimos_cartao_credito_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_curr_credit_card_PJ_over_long_term_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null or emprestimos_cartao_credito_PJ_Curr = 0 then 0 when divida_atual_PJ = 0 then 2019 else emprestimos_cartao_credito_PJ_Curr/divida_atual_PJ end as scr_curr_credit_card_PJ_over_short_term_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null then 0 else max_emprestimos_cartao_credito_PJ * 1000 end as scr_max_credit_card_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null or max_emprestimos_cartao_credito_PJ = 0 then 0 else emprestimos_cartao_credito_PJ_Curr / max_emprestimos_cartao_credito_PJ end as scr_curr_over_max_credit_card_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null or max_emprestimos_cartao_credito_PJ = 0 then 0 else case when emprestimos_cartao_credito_PJ_Curr = max_emprestimos_cartao_credito_PJ then 1 else 0 end end as scr_All_TimeHigh_credit_card_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null then 0 else (emprestimos_cartao_credito_PJ_Curr - emprestimos_cartao_credito_PJ_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_credit_card_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null then 0 else (emprestimos_cartao_credito_PJ_Curr - emprestimos_cartao_credito_PJ_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_credit_card_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null or emprestimos_cartao_credito_PJ_Curr = 0 then 0 when emprestimos_cartao_credito_PJ_5M = 0 then 2019 else emprestimos_cartao_credito_PJ_Curr / emprestimos_cartao_credito_PJ_5M - 1 end as scr_var_rel_6M_credit_card_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null or emprestimos_cartao_credito_PJ_Curr = 0 then 0 when emprestimos_cartao_credito_PJ_11M = 0 then 2019 else emprestimos_cartao_credito_PJ_Curr / emprestimos_cartao_credito_PJ_11M - 1 end as scr_var_rel_12M_credit_card_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null then 0 else Meses_Aumento_emprestimos_cartao_credito_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Rise_credit_card_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null then 0 else Meses_Reducao_emprestimos_cartao_credito_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Dive_credit_card_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null then 0 else months_sth_emprestimos_cartao_credito_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_dive_credit_card_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null then 0 else months_hth_emprestimos_cartao_credito_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_rise_credit_card_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null then 0 else Saldo_Amort_emprestimos_cartao_credito_PJ * 1000 end as scr_credit_card_amortization_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null then 0 when Saldo_Amort_emprestimos_cartao_credito_PJ = 0 then 0 else Saldo_Amort_emprestimos_cartao_credito_PJ / Meses_Reducao_emprestimos_cartao_credito_PJ / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_credit_card_pj_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_cartao_credito_PJ_Curr is null then 0 when Saldo_Amort_emprestimos_cartao_credito_PJ = 0 then 0 else Saldo_Amort_emprestimos_cartao_credito_PJ / Meses_Reducao_emprestimos_cartao_credito_PJ / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_credit_card_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: EMPRESTIMOS CONTA GARANTIDA
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null then 0 else count_emprestimos_conta_garantida_PJ end as scr_count_light_overdraft_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null then 0 else Ever_emprestimos_conta_garantida_PJ * 1000 end as scr_ever_light_overdraft_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null then 0 else emprestimos_conta_garantida_PJ_Curr * 1000 end as scr_curr_light_overdraft_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null then 0 else emprestimos_conta_garantida_PJ_5M * 1000 end as scr_6M_light_overdraft_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null then 0 else emprestimos_conta_garantida_PJ_11M * 1000 end as scr_12M_light_overdraft_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null then 0 else emprestimos_conta_garantida_PJ_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_light_overdraft_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null or emprestimos_conta_garantida_PJ_Curr = 0 then 0 when emprestimos_conta_garantida_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 when Long_Term_Debt_PJ_Curr = 0 then 2019 else emprestimos_conta_garantida_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_curr_light_overdraft_PJ_over_long_term_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null or emprestimos_conta_garantida_PJ_Curr = 0 then 0 when divida_atual_PJ = 0 then 2019 else emprestimos_conta_garantida_PJ_Curr/divida_atual_PJ end as scr_curr_light_overdraft_PJ_over_short_term_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null then 0 else max_emprestimos_conta_garantida_PJ * 1000 end as scr_max_light_overdraft_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null or max_emprestimos_conta_garantida_PJ = 0 then 0 else emprestimos_conta_garantida_PJ_Curr / max_emprestimos_conta_garantida_PJ end as scr_curr_over_max_light_overdraft_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null or max_emprestimos_conta_garantida_PJ = 0 then 0 else case when emprestimos_conta_garantida_PJ_Curr = max_emprestimos_conta_garantida_PJ then 1 else 0 end end as scr_All_TimeHigh_light_overdraft_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null then 0 else (emprestimos_conta_garantida_PJ_Curr - emprestimos_conta_garantida_PJ_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_light_overdraft_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null then 0 else (emprestimos_conta_garantida_PJ_Curr - emprestimos_conta_garantida_PJ_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_light_overdraft_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null or emprestimos_conta_garantida_PJ_Curr = 0 then 0 when emprestimos_conta_garantida_PJ_5M = 0 then 2019 else emprestimos_conta_garantida_PJ_Curr / emprestimos_conta_garantida_PJ_5M - 1 end as scr_var_rel_6M_light_overdraft_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null or emprestimos_conta_garantida_PJ_Curr = 0 then 0 when emprestimos_conta_garantida_PJ_11M = 0 then 2019 else emprestimos_conta_garantida_PJ_Curr / emprestimos_conta_garantida_PJ_11M - 1 end as scr_var_rel_12M_light_overdraft_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null then 0 else Meses_Aumento_emprestimos_conta_garantida_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Rise_light_overdraft_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null then 0 else Meses_Reducao_emprestimos_conta_garantida_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Dive_light_overdraft_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null then 0 else months_sth_emprestimos_conta_garantida_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_dive_light_overdraft_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null then 0 else months_hth_emprestimos_conta_garantida_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_rise_light_overdraft_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null then 0 else Saldo_Amort_emprestimos_conta_garantida_PJ * 1000 end as scr_light_overdraft_amortization_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null then 0 when Saldo_Amort_emprestimos_conta_garantida_PJ = 0 then 0 else Saldo_Amort_emprestimos_conta_garantida_PJ / Meses_Reducao_emprestimos_conta_garantida_PJ / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_light_overdraft_pj_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_PJ_Curr is null then 0 when Saldo_Amort_emprestimos_conta_garantida_PJ = 0 then 0 else Saldo_Amort_emprestimos_conta_garantida_PJ / Meses_Reducao_emprestimos_conta_garantida_PJ / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_light_overdraft_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: EMPRESTIMOS OUTROS EMPRÃ‰STIMOS
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null then 0 else count_emprestimos_outros_emprestimos_PJ end as scr_count_other_loans_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null then 0 else Ever_emprestimos_outros_emprestimos_PJ * 1000 end as scr_ever_other_loans_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null then 0 else emprestimos_outros_emprestimos_PJ_Curr * 1000 end as scr_curr_other_loans_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null then 0 else emprestimos_outros_emprestimos_PJ_5M * 1000 end as scr_6M_other_loans_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null then 0 else emprestimos_outros_emprestimos_PJ_11M * 1000 end as scr_12M_other_loans_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null then 0 else emprestimos_outros_emprestimos_PJ_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_other_loans_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null or emprestimos_outros_emprestimos_PJ_Curr = 0 then 0 when emprestimos_outros_emprestimos_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 when Long_Term_Debt_PJ_Curr = 0 then 2019 else emprestimos_outros_emprestimos_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_curr_other_loans_PJ_over_long_term_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null or emprestimos_outros_emprestimos_PJ_Curr = 0 then 0 when divida_atual_PJ = 0 then 2019 else emprestimos_outros_emprestimos_PJ_Curr/divida_atual_PJ end as scr_curr_other_loans_PJ_over_short_term_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null then 0 else max_emprestimos_outros_emprestimos_PJ * 1000 end as scr_max_other_loans_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null or max_emprestimos_outros_emprestimos_PJ = 0 then 0 else emprestimos_outros_emprestimos_PJ_Curr / max_emprestimos_outros_emprestimos_PJ end as scr_curr_over_max_other_loans_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null or max_emprestimos_outros_emprestimos_PJ = 0 then 0 else case when emprestimos_outros_emprestimos_PJ_Curr = max_emprestimos_outros_emprestimos_PJ then 1 else 0 end end as scr_All_TimeHigh_other_loans_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null then 0 else (emprestimos_outros_emprestimos_PJ_Curr - emprestimos_outros_emprestimos_PJ_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_other_loans_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null then 0 else (emprestimos_outros_emprestimos_PJ_Curr - emprestimos_outros_emprestimos_PJ_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_other_loans_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null or emprestimos_outros_emprestimos_PJ_Curr = 0 then 0 when emprestimos_outros_emprestimos_PJ_5M = 0 then 2019 else emprestimos_outros_emprestimos_PJ_Curr / emprestimos_outros_emprestimos_PJ_5M - 1 end as scr_var_rel_6M_other_loans_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null or emprestimos_outros_emprestimos_PJ_Curr = 0 then 0 when emprestimos_outros_emprestimos_PJ_11M = 0 then 2019 else emprestimos_outros_emprestimos_PJ_Curr / emprestimos_outros_emprestimos_PJ_11M - 1 end as scr_var_rel_12M_other_loans_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null then 0 else Meses_Aumento_emprestimos_outros_emprestimos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Rise_other_loans_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null then 0 else Meses_Reducao_emprestimos_outros_emprestimos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Dive_other_loans_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null then 0 else months_sth_emprestimos_outros_emprestimos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_dive_other_loans_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null then 0 else months_hth_emprestimos_outros_emprestimos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_rise_other_loans_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null then 0 else Saldo_Amort_emprestimos_outros_emprestimos_PJ * 1000 end as scr_other_loans_amortization_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null then 0 when Saldo_Amort_emprestimos_outros_emprestimos_PJ = 0 then 0 else Saldo_Amort_emprestimos_outros_emprestimos_PJ / Meses_Reducao_emprestimos_outros_emprestimos_PJ / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_other_loans_pj_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_outros_emprestimos_PJ_Curr is null then 0 when Saldo_Amort_emprestimos_outros_emprestimos_PJ = 0 then 0 else Saldo_Amort_emprestimos_outros_emprestimos_PJ / Meses_Reducao_emprestimos_outros_emprestimos_PJ / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_other_loans_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: EMPRESTIMOS CAPITAL DE GIRO ROTATIVO
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null then 0 else count_emprestimos_giro_rotativo_PJ end as scr_count_overdraft_work_capital_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null then 0 else Ever_emprestimos_giro_rotativo_PJ * 1000 end as scr_ever_overdraft_work_capital_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null then 0 else emprestimos_giro_rotativo_PJ_Curr * 1000 end as scr_curr_overdraft_work_capital_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null then 0 else emprestimos_giro_rotativo_PJ_5M * 1000 end as scr_6M_overdraft_work_capital_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null then 0 else emprestimos_giro_rotativo_PJ_11M * 1000 end as scr_12M_overdraft_work_capital_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null then 0 else emprestimos_giro_rotativo_PJ_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_overdraft_work_capital_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null or emprestimos_giro_rotativo_PJ_Curr = 0 then 0 when emprestimos_giro_rotativo_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 when Long_Term_Debt_PJ_Curr = 0 then 2019 else emprestimos_giro_rotativo_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_curr_overdraft_work_capital_PJ_over_long_term_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null or emprestimos_giro_rotativo_PJ_Curr = 0 then 0 when divida_atual_PJ = 0 then 2019 else emprestimos_giro_rotativo_PJ_Curr/divida_atual_PJ end as scr_curr_overdraft_work_capital_PJ_over_short_term_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null then 0 else max_emprestimos_giro_rotativo_PJ * 1000 end as scr_max_overdraft_work_capital_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null or max_emprestimos_giro_rotativo_PJ = 0 then 0 else emprestimos_giro_rotativo_PJ_Curr / max_emprestimos_giro_rotativo_PJ end as scr_curr_over_max_overdraft_work_capital_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null or max_emprestimos_giro_rotativo_PJ = 0 then 0 else case when emprestimos_giro_rotativo_PJ_Curr = max_emprestimos_giro_rotativo_PJ then 1 else 0 end end as scr_All_TimeHigh_overdraft_work_capital_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null then 0 else (emprestimos_giro_rotativo_PJ_Curr - emprestimos_giro_rotativo_PJ_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_overdraft_work_capital_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null then 0 else (emprestimos_giro_rotativo_PJ_Curr - emprestimos_giro_rotativo_PJ_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_overdraft_work_capital_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null or emprestimos_giro_rotativo_PJ_Curr = 0 then 0 when emprestimos_giro_rotativo_PJ_5M = 0 then 2019 else emprestimos_giro_rotativo_PJ_Curr / emprestimos_giro_rotativo_PJ_5M - 1 end as scr_var_rel_6M_overdraft_work_capital_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null or emprestimos_giro_rotativo_PJ_Curr = 0 then 0 when emprestimos_giro_rotativo_PJ_11M = 0 then 2019 else emprestimos_giro_rotativo_PJ_Curr / emprestimos_giro_rotativo_PJ_11M - 1 end as scr_var_rel_12M_overdraft_work_capital_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null then 0 else Meses_Aumento_emprestimos_giro_rotativo_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Rise_overdraft_work_capital_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null then 0 else Meses_Reducao_emprestimos_giro_rotativo_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Dive_overdraft_work_capital_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null then 0 else months_sth_emprestimos_giro_rotativo_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_dive_overdraft_work_capital_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null then 0 else months_hth_emprestimos_giro_rotativo_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_rise_overdraft_work_capital_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null then 0 else Saldo_Amort_emprestimos_giro_rotativo_PJ * 1000 end as scr_overdraft_work_capital_amortization_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null then 0 when Saldo_Amort_emprestimos_giro_rotativo_PJ = 0 then 0 else Saldo_Amort_emprestimos_giro_rotativo_PJ / Meses_Reducao_emprestimos_giro_rotativo_PJ / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_overdraft_work_capital_pj_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_giro_rotativo_PJ_Curr is null then 0 when Saldo_Amort_emprestimos_giro_rotativo_PJ = 0 then 0 else Saldo_Amort_emprestimos_giro_rotativo_PJ / Meses_Reducao_emprestimos_giro_rotativo_PJ / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_overdraft_work_capital_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: EMPRESTIMOS CONTA GARANTIDA + CHEQUE ESPECIAL
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null then 0 else count_emprestimos_conta_garantida_cheque_especial_PJ end as scr_count_old_overdraft_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null then 0 else Ever_emprestimos_conta_garantida_cheque_especial_PJ * 1000 end as scr_ever_old_overdraft_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null then 0 else emprestimos_conta_garantida_cheque_especial_PJ_Curr * 1000 end as scr_curr_old_overdraft_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null then 0 else emprestimos_conta_garantida_cheque_especial_PJ_5M * 1000 end as scr_6M_old_overdraft_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null then 0 else emprestimos_conta_garantida_cheque_especial_PJ_11M * 1000 end as scr_12M_old_overdraft_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null then 0 else emprestimos_conta_garantida_cheque_especial_PJ_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_old_overdraft_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null or emprestimos_conta_garantida_cheque_especial_PJ_Curr = 0 then 0 when emprestimos_conta_garantida_cheque_especial_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 when Long_Term_Debt_PJ_Curr = 0 then 2019 else emprestimos_conta_garantida_cheque_especial_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_curr_old_overdraft_PJ_over_long_term_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null or emprestimos_conta_garantida_cheque_especial_PJ_Curr = 0 then 0 when divida_atual_PJ = 0 then 2019 else emprestimos_conta_garantida_cheque_especial_PJ_Curr/divida_atual_PJ end as scr_curr_old_overdraft_PJ_over_short_term_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null then 0 else max_emprestimos_conta_garantida_cheque_especial_PJ * 1000 end as scr_max_old_overdraft_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null or max_emprestimos_conta_garantida_cheque_especial_PJ = 0 then 0 else emprestimos_conta_garantida_cheque_especial_PJ_Curr / max_emprestimos_conta_garantida_cheque_especial_PJ end as scr_curr_over_max_old_overdraft_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null or max_emprestimos_conta_garantida_cheque_especial_PJ = 0 then 0 else case when emprestimos_conta_garantida_cheque_especial_PJ_Curr = max_emprestimos_conta_garantida_cheque_especial_PJ then 1 else 0 end end as scr_All_TimeHigh_old_overdraft_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null then 0 else (emprestimos_conta_garantida_cheque_especial_PJ_Curr - emprestimos_conta_garantida_cheque_especial_PJ_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_old_overdraft_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null then 0 else (emprestimos_conta_garantida_cheque_especial_PJ_Curr - emprestimos_conta_garantida_cheque_especial_PJ_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_old_overdraft_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null or emprestimos_conta_garantida_cheque_especial_PJ_Curr = 0 then 0 when emprestimos_conta_garantida_cheque_especial_PJ_5M = 0 then 2019 else emprestimos_conta_garantida_cheque_especial_PJ_Curr / emprestimos_conta_garantida_cheque_especial_PJ_5M - 1 end as scr_var_rel_6M_old_overdraft_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null or emprestimos_conta_garantida_cheque_especial_PJ_Curr = 0 then 0 when emprestimos_conta_garantida_cheque_especial_PJ_11M = 0 then 2019 else emprestimos_conta_garantida_cheque_especial_PJ_Curr / emprestimos_conta_garantida_cheque_especial_PJ_11M - 1 end as scr_var_rel_12M_old_overdraft_PJ_debt,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null then 0 else Meses_Aumento_emprestimos_conta_garantida_cheque_especial_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Rise_old_overdraft_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null then 0 else Meses_Reducao_emprestimos_conta_garantida_cheque_especial_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Dive_old_overdraft_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null then 0 else months_sth_emprestimos_conta_garantida_cheque_especial_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_dive_old_overdraft_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null then 0 else months_hth_emprestimos_conta_garantida_cheque_especial_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_rise_old_overdraft_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null then 0 else Saldo_Amort_emprestimos_conta_garantida_cheque_especial_PJ * 1000 end as scr_old_overdraft_amortization_PJ,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null then 0 when Saldo_Amort_emprestimos_conta_garantida_cheque_especial_PJ = 0 then 0 else Saldo_Amort_emprestimos_conta_garantida_cheque_especial_PJ / Meses_Reducao_emprestimos_conta_garantida_cheque_especial_PJ / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_old_overdraft_pj_over_revenue,
	case when teve_scr_tirado = 0 then null when emprestimos_conta_garantida_cheque_especial_PJ_Curr is null then 0 when Saldo_Amort_emprestimos_conta_garantida_cheque_especial_PJ = 0 then 0 else Saldo_Amort_emprestimos_conta_garantida_cheque_especial_PJ / Meses_Reducao_emprestimos_conta_garantida_cheque_especial_PJ / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_old_overdraft_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: OUTROS CREDITOS
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null then 0 else count_outroscreditos_PJ end as scr_count_other_credits_PJ,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null then 0 else Ever_outroscreditos_PJ * 1000 end as scr_ever_other_credits_PJ,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null then 0 else outroscreditos_PJ_Curr * 1000 end as scr_curr_other_credits_PJ,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null then 0 else outroscreditos_PJ_5M * 1000 end as scr_6M_other_credits_PJ,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null then 0 else outroscreditos_PJ_11M * 1000 end as scr_12M_other_credits_PJ,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null then 0 else outroscreditos_PJ_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_other_credits_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null or outroscreditos_PJ_Curr = 0 then 0 when outroscreditos_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 when Long_Term_Debt_PJ_Curr = 0 then 2019 else outroscreditos_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_curr_other_credits_PJ_over_long_term_debt,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null or outroscreditos_PJ_Curr = 0 then 0 when divida_atual_PJ = 0 then 2019 else outroscreditos_PJ_Curr/divida_atual_PJ end as scr_curr_other_credits_PJ_over_short_term_debt,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null then 0 else max_outroscreditos_PJ * 1000 end as scr_max_other_credits_PJ_debt,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null or max_outroscreditos_PJ = 0 then 0 else outroscreditos_PJ_Curr / max_outroscreditos_PJ end as scr_curr_over_max_other_credits_PJ_debt,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null or max_outroscreditos_PJ = 0 then 0 else case when outroscreditos_PJ_Curr = max_outroscreditos_PJ then 1 else 0 end end as scr_All_TimeHigh_other_credits_PJ_debt,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null then 0 else (outroscreditos_PJ_Curr - outroscreditos_PJ_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_other_credits_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null then 0 else (outroscreditos_PJ_Curr - outroscreditos_PJ_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_other_credits_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null or outroscreditos_PJ_Curr = 0 then 0 when outroscreditos_PJ_5M = 0 then 2019 else outroscreditos_PJ_Curr / outroscreditos_PJ_5M - 1 end as scr_var_rel_6M_other_credits_PJ_debt,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null or outroscreditos_PJ_Curr = 0 then 0 when outroscreditos_PJ_11M = 0 then 2019 else outroscreditos_PJ_Curr / outroscreditos_PJ_11M - 1 end as scr_var_rel_12M_other_credits_PJ_debt,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null then 0 else Meses_Aumento_outroscreditos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Rise_other_credits_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null then 0 else Meses_Reducao_outroscreditos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Dive_other_credits_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null then 0 else months_sth_outroscreditos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_dive_other_credits_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null then 0 else months_hth_outroscreditos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_rise_other_credits_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null then 0 else Saldo_Amort_outroscreditos_PJ * 1000 end as scr_other_credits_amortization_PJ,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null then 0 when Saldo_Amort_outroscreditos_PJ = 0 then 0 else Saldo_Amort_outroscreditos_PJ / Meses_Reducao_outroscreditos_PJ / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_other_credits_pj_over_revenue,
	case when teve_scr_tirado = 0 then null when outroscreditos_PJ_Curr is null then 0 when Saldo_Amort_outroscreditos_PJ = 0 then 0 else Saldo_Amort_outroscreditos_PJ / Meses_Reducao_outroscreditos_PJ / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_other_credits_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: DESCONTO DE DIREITOS CREDITORIOS
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null then 0 else count_desconto_PJ end as scr_count_cash_advance_PJ,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null then 0 else Ever_desconto_PJ * 1000 end as scr_ever_cash_advance_PJ,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null then 0 else desconto_PJ_Curr * 1000 end as scr_curr_cash_advance_PJ,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null then 0 else desconto_PJ_5M * 1000 end as scr_6M_cash_advance_PJ,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null then 0 else desconto_PJ_11M * 1000 end as scr_12M_cash_advance_PJ,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null then 0 else desconto_PJ_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_cash_advance_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null or desconto_PJ_Curr = 0 then 0 when desconto_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 when Long_Term_Debt_PJ_Curr = 0 then 2019 else desconto_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_curr_cash_advance_PJ_over_long_term_debt,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null or desconto_PJ_Curr = 0 then 0 when divida_atual_PJ = 0 then 2019 else desconto_PJ_Curr/divida_atual_PJ end as scr_curr_cash_advance_PJ_over_short_term_debt,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null then 0 else max_desconto_PJ * 1000 end as scr_max_cash_advance_PJ_debt,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null or max_desconto_PJ = 0 then 0 else desconto_PJ_Curr / max_desconto_PJ end as scr_curr_over_max_cash_advance_PJ_debt,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null or max_desconto_PJ = 0 then 0 else case when desconto_PJ_Curr = max_desconto_PJ then 1 else 0 end end as scr_All_TimeHigh_cash_advance_PJ_debt,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null then 0 else (desconto_PJ_Curr - desconto_PJ_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_cash_advance_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null then 0 else (desconto_PJ_Curr - desconto_PJ_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_cash_advance_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null or desconto_PJ_Curr = 0 then 0 when desconto_PJ_5M = 0 then 2019 else desconto_PJ_Curr / desconto_PJ_5M - 1 end as scr_var_rel_6M_cash_advance_PJ_debt,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null or desconto_PJ_Curr = 0 then 0 when desconto_PJ_11M = 0 then 2019 else desconto_PJ_Curr / desconto_PJ_11M - 1 end as scr_var_rel_12M_cash_advance_PJ_debt,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null then 0 else Meses_Aumento_desconto_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Rise_cash_advance_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null then 0 else Meses_Reducao_desconto_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Dive_cash_advance_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null then 0 else months_sth_desconto_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_dive_cash_advance_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null then 0 else months_hth_desconto_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_rise_cash_advance_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null then 0 else Saldo_Amort_desconto_PJ * 1000 end as scr_cash_advance_amortization_PJ,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null then 0 when Saldo_Amort_desconto_PJ = 0 then 0 else Saldo_Amort_desconto_PJ / Meses_Reducao_desconto_PJ / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_cash_advance_pj_over_revenue,
	case when teve_scr_tirado = 0 then null when desconto_PJ_Curr is null then 0 when Saldo_Amort_desconto_PJ = 0 then 0 else Saldo_Amort_desconto_PJ / Meses_Reducao_desconto_PJ / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_cash_advance_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: FINANCIAMENTOS
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null then 0 else count_financiamentos_PJ end as scr_count_all_financings_PJ,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null then 0 else Ever_financiamentos_PJ * 1000 end as scr_ever_all_financings_PJ,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null then 0 else financiamentos_PJ_Curr * 1000 end as scr_curr_all_financings_PJ,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null then 0 else financiamentos_PJ_5M * 1000 end as scr_6M_all_financings_PJ,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null then 0 else financiamentos_PJ_11M * 1000 end as scr_12M_all_financings_PJ,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null then 0 else financiamentos_PJ_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_all_financings_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null or financiamentos_PJ_Curr = 0 then 0 when financiamentos_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 when Long_Term_Debt_PJ_Curr = 0 then 2019 else financiamentos_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_curr_all_financings_PJ_over_long_term_debt,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null or financiamentos_PJ_Curr = 0 then 0 when divida_atual_PJ = 0 then 2019 else financiamentos_PJ_Curr/divida_atual_PJ end as scr_curr_all_financings_PJ_over_short_term_debt,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null then 0 else max_financiamentos_PJ * 1000 end as scr_max_all_financings_PJ_debt,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null or max_financiamentos_PJ = 0 then 0 else financiamentos_PJ_Curr / max_financiamentos_PJ end as scr_curr_over_max_all_financings_PJ_debt,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null or max_financiamentos_PJ = 0 then 0 else case when financiamentos_PJ_Curr = max_financiamentos_PJ then 1 else 0 end end as scr_All_TimeHigh_all_financings_PJ_debt,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null then 0 else (financiamentos_PJ_Curr - financiamentos_PJ_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_all_financings_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null then 0 else (financiamentos_PJ_Curr - financiamentos_PJ_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_all_financings_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null or financiamentos_PJ_Curr = 0 then 0 when financiamentos_PJ_5M = 0 then 2019 else financiamentos_PJ_Curr / financiamentos_PJ_5M - 1 end as scr_var_rel_6M_all_financings_PJ_debt,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null or financiamentos_PJ_Curr = 0 then 0 when financiamentos_PJ_11M = 0 then 2019 else financiamentos_PJ_Curr / financiamentos_PJ_11M - 1 end as scr_var_rel_12M_all_financings_PJ_debt,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null then 0 else Meses_Aumento_financiamentos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Rise_all_financings_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null then 0 else Meses_Reducao_financiamentos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Dive_all_financings_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null then 0 else months_sth_financiamentos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_dive_all_financings_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null then 0 else months_hth_financiamentos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_rise_all_financings_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null then 0 else Saldo_Amort_financiamentos_PJ * 1000 end as scr_all_financings_amortization_PJ,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null then 0 when Saldo_Amort_financiamentos_PJ = 0 then 0 else Saldo_Amort_financiamentos_PJ / Meses_Reducao_financiamentos_PJ / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_all_financings_pj_over_revenue,
	case when teve_scr_tirado = 0 then null when financiamentos_PJ_Curr is null then 0 when Saldo_Amort_financiamentos_PJ = 0 then 0 else Saldo_Amort_financiamentos_PJ / Meses_Reducao_financiamentos_PJ / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_all_financings_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: ADIANTAMENTO A DEPOSITANTES
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null then 0 else count_adiantamentos_PJ end as scr_count_deposit_advance_PJ,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null then 0 else Ever_adiantamentos_PJ * 1000 end as scr_ever_deposit_advance_PJ,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null then 0 else adiantamentos_PJ_Curr * 1000 end as scr_curr_deposit_advance_PJ,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null then 0 else adiantamentos_PJ_5M * 1000 end as scr_6M_deposit_advance_PJ,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null then 0 else adiantamentos_PJ_11M * 1000 end as scr_12M_deposit_advance_PJ,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null then 0 else adiantamentos_PJ_Curr / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Curr_deposit_advance_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null or adiantamentos_PJ_Curr = 0 then 0 when adiantamentos_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 when Long_Term_Debt_PJ_Curr = 0 then 2019 else adiantamentos_PJ_Curr / Long_Term_Debt_PJ_Curr end as scr_curr_deposit_advance_PJ_over_long_term_debt,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null or adiantamentos_PJ_Curr = 0 then 0 when divida_atual_PJ = 0 then 2019 else adiantamentos_PJ_Curr / divida_atual_PJ end as scr_curr_deposit_advance_PJ_over_short_term_debt,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null then 0 else max_adiantamentos_PJ * 1000 end as scr_max_deposit_advance_PJ_debt,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null or max_adiantamentos_PJ = 0 then 0 else adiantamentos_PJ_Curr / max_adiantamentos_PJ end as scr_curr_over_max_deposit_advance_PJ_debt,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null or max_adiantamentos_PJ = 0 then 0 else case when adiantamentos_PJ_Curr = max_adiantamentos_PJ then 1 else 0 end end as scr_All_TimeHigh_deposit_advance_PJ_debt,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null then 0 else (adiantamentos_PJ_Curr - adiantamentos_PJ_5M) / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_var_abs_6M_deposit_advance_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null then 0 else (adiantamentos_PJ_Curr - adiantamentos_PJ_11M) / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_var_abs_12M_deposit_advance_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null or adiantamentos_PJ_Curr = 0 then 0 when adiantamentos_PJ_5M = 0 then 2019 else adiantamentos_PJ_Curr / adiantamentos_PJ_5M - 1 end as scr_var_rel_6M_deposit_advance_PJ_debt,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null or adiantamentos_PJ_Curr = 0 then 0 when adiantamentos_PJ_11M = 0 then 2019 else adiantamentos_PJ_Curr / adiantamentos_PJ_11M - 1 end as scr_var_rel_12M_deposit_advance_PJ_debt,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null then 0 else Meses_Aumento_adiantamentos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ - 1 end) end as scr_Months_Rise_deposit_advance_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null then 0 else Meses_Reducao_adiantamentos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ - 1 end) end as scr_Months_Dive_deposit_advance_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null then 0 else months_sth_adiantamentos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ - 1 end) end as scr_month_consecutive_dive_deposit_advance_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null then 0 else months_hth_adiantamentos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ - 1 end) end as scr_month_consecutive_rise_deposit_advance_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null then 0 else Saldo_Amort_adiantamentos_PJ * 1000 end as scr_deposit_advance_amortization_PJ,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null then 0 when Saldo_Amort_adiantamentos_PJ = 0 then 0 else Saldo_Amort_adiantamentos_PJ / Meses_Reducao_adiantamentos_PJ / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_deposit_advance_pj_over_revenue,
	case when teve_scr_tirado = 0 then null when adiantamentos_PJ_Curr is null then 0 when Saldo_Amort_adiantamentos_PJ = 0 then 0 else Saldo_Amort_adiantamentos_PJ / Meses_Reducao_adiantamentos_PJ / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_deposit_advance_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: OUTROS FINANCIAMENTOS
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null then 0 else count_outrosfinanciamentos_PJ end as scr_count_other_financings_PJ,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null then 0 else Ever_outrosfinanciamentos_PJ * 1000 end as scr_ever_other_financings_PJ,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null then 0 else outrosfinanciamentos_PJ_Curr * 1000 end as scr_curr_other_financings_PJ,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null then 0 else outrosfinanciamentos_PJ_5M * 1000 end as scr_6M_other_financings_PJ,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null then 0 else outrosfinanciamentos_PJ_11M * 1000 end as scr_12M_other_financings_PJ,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null then 0 else outrosfinanciamentos_PJ_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_other_financings_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null or outrosfinanciamentos_PJ_Curr = 0 then 0 when outrosfinanciamentos_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 when Long_Term_Debt_PJ_Curr = 0 then 2019 else outrosfinanciamentos_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_curr_other_financings_PJ_over_long_term_debt,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null or outrosfinanciamentos_PJ_Curr = 0 then 0 when divida_atual_PJ = 0 then 2019 else outrosfinanciamentos_PJ_Curr/divida_atual_PJ end as scr_curr_other_financings_PJ_over_short_term_debt,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null then 0 else max_outrosfinanciamentos_PJ * 1000 end as scr_max_other_financings_PJ_debt,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null or max_outrosfinanciamentos_PJ = 0 then 0 else outrosfinanciamentos_PJ_Curr / max_outrosfinanciamentos_PJ end as scr_curr_over_max_other_financings_PJ_debt,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null or max_outrosfinanciamentos_PJ = 0 then 0 else case when outrosfinanciamentos_PJ_Curr = max_outrosfinanciamentos_PJ then 1 else 0 end end as scr_All_TimeHigh_other_financings_PJ_debt,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null then 0 else (outrosfinanciamentos_PJ_Curr - outrosfinanciamentos_PJ_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_other_financings_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null then 0 else (outrosfinanciamentos_PJ_Curr - outrosfinanciamentos_PJ_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_other_financings_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null or outrosfinanciamentos_PJ_Curr = 0 then 0 when outrosfinanciamentos_PJ_5M = 0 then 2019 else outrosfinanciamentos_PJ_Curr / outrosfinanciamentos_PJ_5M - 1 end as scr_var_rel_6M_other_financings_PJ_debt,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null or outrosfinanciamentos_PJ_Curr = 0 then 0 when outrosfinanciamentos_PJ_11M = 0 then 2019 else outrosfinanciamentos_PJ_Curr / outrosfinanciamentos_PJ_11M - 1 end as scr_var_rel_12M_other_financings_PJ_debt,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null then 0 else Meses_Aumento_outrosfinanciamentos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Rise_other_financings_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null then 0 else Meses_Reducao_outrosfinanciamentos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Dive_other_financings_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null then 0 else months_sth_outrosfinanciamentos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_dive_other_financings_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null then 0 else months_hth_outrosfinanciamentos_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_rise_other_financings_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null then 0 else Saldo_Amort_outrosfinanciamentos_PJ * 1000 end as scr_other_financings_amortization_PJ,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null then 0 when Saldo_Amort_outrosfinanciamentos_PJ = 0 then 0 else Saldo_Amort_outrosfinanciamentos_PJ / Meses_Reducao_outrosfinanciamentos_PJ / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_other_financings_pj_over_revenue,
	case when teve_scr_tirado = 0 then null when outrosfinanciamentos_PJ_Curr is null then 0 when Saldo_Amort_outrosfinanciamentos_PJ = 0 then 0 else Saldo_Amort_outrosfinanciamentos_PJ / Meses_Reducao_outrosfinanciamentos_PJ / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_other_financings_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: LIMITE DE CREDITO CURTO PRAZO
	case when teve_scr_tirado = 0 then null when limitecredito_curto_PJ_Curr is null then 0 else count_limitecredito_curto_PJ end as scr_count_st_credit_limit_PJ,
	case when teve_scr_tirado = 0 then null when limitecredito_curto_PJ_Curr is null then 0 else Ever_limitecredito_curto_PJ * 1000 end as scr_ever_st_credit_limit_PJ,
	case when teve_scr_tirado = 0 then null when limitecredito_curto_PJ_Curr is null then 0 else limitecredito_curto_PJ_Curr * 1000 end as scr_curr_st_credit_limit_PJ,
	case when teve_scr_tirado = 0 then null when limitecredito_curto_PJ_Curr is null then 0 else limitecredito_curto_PJ_5M * 1000 end as scr_6M_st_credit_limit_PJ,
	case when teve_scr_tirado = 0 then null when limitecredito_curto_PJ_Curr is null then 0 else limitecredito_curto_PJ_11M * 1000 end as scr_12M_st_credit_limit_PJ,
	case when teve_scr_tirado = 0 then null when limitecredito_curto_PJ_Curr is null then 0 else limitecredito_curto_PJ_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_st_credit_limit_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when limitecredito_curto_PJ_Curr is null or limitecredito_curto_PJ_Curr = 0 then 0 when limitecredito_curto_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 when Long_Term_Debt_PJ_Curr = 0 then 2019 else limitecredito_curto_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_curr_st_credit_limit_PJ_over_long_term_debt,
	case when teve_scr_tirado = 0 then null when limitecredito_curto_PJ_Curr is null or limitecredito_curto_PJ_Curr = 0 then 0 when divida_atual_PJ = 0 then 2019 else limitecredito_curto_PJ_Curr/divida_atual_PJ end as scr_curr_st_credit_limit_PJ_over_short_term_debt,
	case when teve_scr_tirado = 0 then null when limitecredito_curto_PJ_Curr is null then 0 else max_limitecredito_curto_PJ * 1000 end as scr_max_st_credit_limit_PJ_debt,
	case when teve_scr_tirado = 0 then null when limitecredito_curto_PJ_Curr is null or max_limitecredito_curto_PJ = 0 then 0 else limitecredito_curto_PJ_Curr / max_limitecredito_curto_PJ end as scr_curr_over_max_st_credit_limit_PJ_debt,
	case when teve_scr_tirado = 0 then null when limitecredito_curto_PJ_Curr is null or max_limitecredito_curto_PJ = 0 then 0 else case when limitecredito_curto_PJ_Curr = max_limitecredito_curto_PJ then 1 else 0 end end as scr_All_TimeHigh_st_credit_limit_PJ_debt,
	case when teve_scr_tirado = 0 then null when limitecredito_curto_PJ_Curr is null then 0 else (limitecredito_curto_PJ_Curr - limitecredito_curto_PJ_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_st_credit_limit_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when limitecredito_curto_PJ_Curr is null then 0 else (limitecredito_curto_PJ_Curr - limitecredito_curto_PJ_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_st_credit_limit_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when limitecredito_curto_PJ_Curr is null or limitecredito_curto_PJ_Curr = 0 then 0 when limitecredito_curto_PJ_5M = 0 then 2019 else limitecredito_curto_PJ_Curr / limitecredito_curto_PJ_5M - 1 end as scr_var_rel_6M_st_credit_limit_PJ_debt,
	case when teve_scr_tirado = 0 then null when limitecredito_curto_PJ_Curr is null or limitecredito_curto_PJ_Curr = 0 then 0 when limitecredito_curto_PJ_11M = 0 then 2019 else limitecredito_curto_PJ_Curr / limitecredito_curto_PJ_11M - 1 end as scr_var_rel_12M_st_credit_limit_PJ_debt,
	case when teve_scr_tirado = 0 then null when limitecredito_curto_PJ_Curr is null then 0 else Meses_Aumento_limitecredito_curto_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Rise_st_credit_limit_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when limitecredito_curto_PJ_Curr is null then 0 else Meses_Reducao_limitecredito_curto_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Dive_st_credit_limit_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when limitecredito_curto_PJ_Curr is null then 0 else months_sth_limitecredito_curto_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_dive_st_credit_limit_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when limitecredito_curto_PJ_Curr is null then 0 else months_hth_limitecredito_curto_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_rise_st_credit_limit_PJ_debt_over_total_months,
--------------------------SCR PJ MODALIDADE: LIMITE DE CREDITO LONGO PRAZO
	case when teve_scr_tirado = 0 then null when limitecredito_longo_PJ_Curr is null then 0 else count_limitecredito_longo_PJ end as scr_count_lt_credit_limit_PJ,
	case when teve_scr_tirado = 0 then null when limitecredito_longo_PJ_Curr is null then 0 else Ever_limitecredito_longo_PJ * 1000 end as scr_ever_lt_credit_limit_PJ,
	case when teve_scr_tirado = 0 then null when limitecredito_longo_PJ_Curr is null then 0 else limitecredito_longo_PJ_Curr * 1000 end as scr_curr_lt_credit_limit_PJ,
	case when teve_scr_tirado = 0 then null when limitecredito_longo_PJ_Curr is null then 0 else limitecredito_longo_PJ_5M * 1000 end as scr_6M_lt_credit_limit_PJ,
	case when teve_scr_tirado = 0 then null when limitecredito_longo_PJ_Curr is null then 0 else limitecredito_longo_PJ_11M * 1000 end as scr_12M_lt_credit_limit_PJ,
	case when teve_scr_tirado = 0 then null when limitecredito_longo_PJ_Curr is null then 0 else limitecredito_longo_PJ_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_lt_credit_limit_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when limitecredito_longo_PJ_Curr is null or limitecredito_longo_PJ_Curr = 0 then 0 when limitecredito_longo_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 when Long_Term_Debt_PJ_Curr = 0 then 2019 else limitecredito_longo_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_curr_lt_credit_limit_PJ_over_long_term_debt,
	case when teve_scr_tirado = 0 then null when limitecredito_longo_PJ_Curr is null or limitecredito_longo_PJ_Curr = 0 then 0 when divida_atual_PJ = 0 then 2019 else limitecredito_longo_PJ_Curr/divida_atual_PJ end as scr_curr_lt_credit_limit_PJ_over_short_term_debt,
	case when teve_scr_tirado = 0 then null when limitecredito_longo_PJ_Curr is null then 0 else max_limitecredito_longo_PJ * 1000 end as scr_max_lt_credit_limit_PJ_debt,
	case when teve_scr_tirado = 0 then null when limitecredito_longo_PJ_Curr is null or max_limitecredito_longo_PJ = 0 then 0 else limitecredito_longo_PJ_Curr / max_limitecredito_longo_PJ end as scr_curr_over_max_lt_credit_limit_PJ_debt,
	case when teve_scr_tirado = 0 then null when limitecredito_longo_PJ_Curr is null or max_limitecredito_longo_PJ = 0 then 0 else case when limitecredito_longo_PJ_Curr = max_limitecredito_longo_PJ then 1 else 0 end end as scr_All_TimeHigh_lt_credit_limit_PJ_debt,
	case when teve_scr_tirado = 0 then null when limitecredito_longo_PJ_Curr is null then 0 else (limitecredito_longo_PJ_Curr - limitecredito_longo_PJ_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_lt_credit_limit_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when limitecredito_longo_PJ_Curr is null then 0 else (limitecredito_longo_PJ_Curr - limitecredito_longo_PJ_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_lt_credit_limit_PJ_over_revenue,
	case when teve_scr_tirado = 0 then null when limitecredito_longo_PJ_Curr is null or limitecredito_longo_PJ_Curr = 0 then 0 when limitecredito_longo_PJ_5M = 0 then 2019 else limitecredito_longo_PJ_Curr / limitecredito_longo_PJ_5M - 1 end as scr_var_rel_6M_lt_credit_limit_PJ_debt,
	case when teve_scr_tirado = 0 then null when limitecredito_longo_PJ_Curr is null or limitecredito_longo_PJ_Curr = 0 then 0 when limitecredito_longo_PJ_11M = 0 then 2019 else limitecredito_longo_PJ_Curr / limitecredito_longo_PJ_11M - 1 end as scr_var_rel_12M_lt_credit_limit_PJ_debt,
	case when teve_scr_tirado = 0 then null when limitecredito_longo_PJ_Curr is null then 0 else Meses_Aumento_limitecredito_longo_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Rise_lt_credit_limit_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when limitecredito_longo_PJ_Curr is null then 0 else Meses_Reducao_limitecredito_longo_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_Months_Dive_lt_credit_limit_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when limitecredito_longo_PJ_Curr is null then 0 else months_sth_limitecredito_longo_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_dive_lt_credit_limit_PJ_debt_over_total_months,
	case when teve_scr_tirado = 0 then null when limitecredito_longo_PJ_Curr is null then 0 else months_hth_limitecredito_longo_PJ / (case when qtd_meses_modalidade_PJ <= 1 then 1 else qtd_meses_modalidade_PJ-1 end) end as scr_month_consecutive_rise_lt_credit_limit_PJ_debt_over_total_months,
--------------------------SCR PF MODALIDADE: INFO GERAL
	case when teve_scr_pf_tirado = 0 then null else divida_atual_pf end as scr_short_term_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_pf_Curr is null then 0 else qtd_meses_modalidade_pf end as scr_number_of_months_in_report_modal_pf,
--------------------------SCR PF MODALIDADE: EMPRESTIMOS TOTAL
	case when teve_scr_pf_tirado = 0 then null when emprestimos_PF_Curr is null then 0 else Ever_emprestimos_PF * 1000 end as scr_ever_all_loans_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_PF_Curr is null then 0 else emprestimos_PF_Curr * 1000 end as scr_curr_all_loans_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_PF_Curr is null then 0 else emprestimos_PF_5M * 1000 end as scr_6M_all_loans_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_PF_Curr is null then 0 else emprestimos_PF_11M * 1000 end as scr_12M_all_loans_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_PF_Curr is null then 0 else emprestimos_PF_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_all_loans_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_PF_Curr is null or emprestimos_PF_Curr = 0 then 0 when emprestimos_PF_Curr > Long_Term_Debt_PF_Curr then 1 when Long_Term_Debt_PF_Curr = 0 then 2019 else emprestimos_PF_Curr/Long_Term_Debt_PF_Curr end as scr_curr_all_loans_PF_over_long_term_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_PF_Curr is null or emprestimos_PF_Curr = 0 then 0 when divida_atual_PF = 0 then 2019 else emprestimos_PF_Curr/divida_atual_PF end as scr_curr_all_loans_PF_over_short_term_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_PF_Curr is null then 0 else max_emprestimos_PF * 1000 end as scr_max_all_loans_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_PF_Curr is null or max_emprestimos_PF = 0 then 0 else emprestimos_PF_Curr / max_emprestimos_PF end as scr_curr_over_max_all_loans_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_PF_Curr is null or max_emprestimos_PF = 0 then 0 else case when emprestimos_PF_Curr = max_emprestimos_PF then 1 else 0 end end as scr_All_TimeHigh_all_loans_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_PF_Curr is null then 0 else (emprestimos_PF_Curr - emprestimos_PF_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_all_loans_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_PF_Curr is null then 0 else (emprestimos_PF_Curr - emprestimos_PF_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_all_loans_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_PF_Curr is null or emprestimos_PF_Curr = 0 then 0 when emprestimos_PF_5M = 0 then 2019 else emprestimos_PF_Curr / emprestimos_PF_5M - 1 end as scr_var_rel_6M_all_loans_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_PF_Curr is null or emprestimos_PF_Curr = 0 then 0 when emprestimos_PF_11M = 0 then 2019 else emprestimos_PF_Curr / emprestimos_PF_11M - 1 end as scr_var_rel_12M_all_loans_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_PF_Curr is null then 0 else Meses_Aumento_emprestimos_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Rise_all_loans_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_PF_Curr is null then 0 else Meses_Reducao_emprestimos_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Dive_all_loans_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_PF_Curr is null then 0 else months_sth_emprestimos_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_dive_all_loans_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_PF_Curr is null then 0 else months_hth_emprestimos_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_rise_all_loans_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_pf_Curr is null then 0 else Saldo_Amort_emprestimos_pf * 1000 end as scr_all_loans_amortization_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_pf_Curr is null then 0 when Saldo_Amort_emprestimos_pf = 0 then 0 else Saldo_Amort_emprestimos_pf / Meses_Reducao_emprestimos_pf / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_all_loans_pf_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_pf_Curr is null then 0 when Saldo_Amort_emprestimos_pf = 0 then 0 else Saldo_Amort_emprestimos_pf / Meses_Reducao_emprestimos_pf / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_all_loans_pf_over_loan_PMT,
--------------------------SCR PF MODALIDADE: EMPRESTIMOS CHEQUE ESPECIAL
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_PF_Curr is null then 0 else Ever_emprestimos_cheque_especial_PF * 1000 end as scr_ever_overdraft_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_PF_Curr is null then 0 else emprestimos_cheque_especial_PF_Curr * 1000 end as scr_curr_overdraft_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_PF_Curr is null then 0 else emprestimos_cheque_especial_PF_5M * 1000 end as scr_6M_overdraft_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_PF_Curr is null then 0 else emprestimos_cheque_especial_PF_11M * 1000 end as scr_12M_overdraft_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_PF_Curr is null then 0 else emprestimos_cheque_especial_PF_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_overdraft_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_PF_Curr is null or emprestimos_cheque_especial_PF_Curr = 0 then 0 when emprestimos_cheque_especial_PF_Curr > Long_Term_Debt_PF_Curr then 1 when Long_Term_Debt_PF_Curr = 0 then 2019 else emprestimos_cheque_especial_PF_Curr/Long_Term_Debt_PF_Curr end as scr_curr_overdraft_PF_over_long_term_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_PF_Curr is null or emprestimos_cheque_especial_PF_Curr = 0 then 0 when divida_atual_PF = 0 then 2019 else emprestimos_cheque_especial_PF_Curr/divida_atual_PF end as scr_curr_overdraft_PF_over_short_term_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_PF_Curr is null then 0 else max_emprestimos_cheque_especial_PF * 1000 end as scr_max_overdraft_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_PF_Curr is null or max_emprestimos_cheque_especial_PF = 0 then 0 else emprestimos_cheque_especial_PF_Curr / max_emprestimos_cheque_especial_PF end as scr_curr_over_max_overdraft_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_PF_Curr is null or max_emprestimos_cheque_especial_PF = 0 then 0 else case when emprestimos_cheque_especial_PF_Curr = max_emprestimos_cheque_especial_PF then 1 else 0 end end as scr_All_TimeHigh_overdraft_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_PF_Curr is null then 0 else (emprestimos_cheque_especial_PF_Curr - emprestimos_cheque_especial_PF_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_overdraft_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_PF_Curr is null then 0 else (emprestimos_cheque_especial_PF_Curr - emprestimos_cheque_especial_PF_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_overdraft_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_PF_Curr is null or emprestimos_cheque_especial_PF_Curr = 0 then 0 when emprestimos_cheque_especial_PF_5M = 0 then 2019 else emprestimos_cheque_especial_PF_Curr / emprestimos_cheque_especial_PF_5M - 1 end as scr_var_rel_6M_overdraft_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_PF_Curr is null or emprestimos_cheque_especial_PF_Curr = 0 then 0 when emprestimos_cheque_especial_PF_11M = 0 then 2019 else emprestimos_cheque_especial_PF_Curr / emprestimos_cheque_especial_PF_11M - 1 end as scr_var_rel_12M_overdraft_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_PF_Curr is null then 0 else Meses_Aumento_emprestimos_cheque_especial_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Rise_overdraft_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_PF_Curr is null then 0 else Meses_Reducao_emprestimos_cheque_especial_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Dive_overdraft_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_PF_Curr is null then 0 else months_sth_emprestimos_cheque_especial_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_dive_overdraft_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_PF_Curr is null then 0 else months_hth_emprestimos_cheque_especial_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_rise_overdraft_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_pf_Curr is null then 0 else Saldo_Amort_emprestimos_cheque_especial_pf * 1000 end as scr_overdraft_amortization_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_pf_Curr is null then 0 when Saldo_Amort_emprestimos_cheque_especial_pf = 0 then 0 else Saldo_Amort_emprestimos_cheque_especial_pf / Meses_Reducao_emprestimos_cheque_especial_pf / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_overdraft_pf_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cheque_especial_pf_Curr is null then 0 when Saldo_Amort_emprestimos_cheque_especial_pf = 0 then 0 else Saldo_Amort_emprestimos_cheque_especial_pf / Meses_Reducao_emprestimos_cheque_especial_pf / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_overdraft_pf_over_loan_PMT,
--------------------------SCR PF MODALIDADE: EMPRESTIMOS CARTÃƒO DE CRÃ‰DITO
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_PF_Curr is null then 0 else Ever_emprestimos_cartao_credito_PF * 1000 end as scr_ever_credit_card_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_PF_Curr is null then 0 else emprestimos_cartao_credito_PF_Curr * 1000 end as scr_curr_credit_card_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_PF_Curr is null then 0 else emprestimos_cartao_credito_PF_5M * 1000 end as scr_6M_credit_card_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_PF_Curr is null then 0 else emprestimos_cartao_credito_PF_11M * 1000 end as scr_12M_credit_card_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_PF_Curr is null then 0 else emprestimos_cartao_credito_PF_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_credit_card_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_PF_Curr is null or emprestimos_cartao_credito_PF_Curr = 0 then 0 when emprestimos_cartao_credito_PF_Curr > Long_Term_Debt_PF_Curr then 1 when Long_Term_Debt_PF_Curr = 0 then 2019 else emprestimos_cartao_credito_PF_Curr/Long_Term_Debt_PF_Curr end as scr_curr_credit_card_PF_over_long_term_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_PF_Curr is null or emprestimos_cartao_credito_PF_Curr = 0 then 0 when divida_atual_PF = 0 then 2019 else emprestimos_cartao_credito_PF_Curr/divida_atual_PF end as scr_curr_credit_card_PF_over_short_term_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_PF_Curr is null then 0 else max_emprestimos_cartao_credito_PF * 1000 end as scr_max_credit_card_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_PF_Curr is null or max_emprestimos_cartao_credito_PF = 0 then 0 else emprestimos_cartao_credito_PF_Curr / max_emprestimos_cartao_credito_PF end as scr_curr_over_max_credit_card_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_PF_Curr is null or max_emprestimos_cartao_credito_PF = 0 then 0 else case when emprestimos_cartao_credito_PF_Curr = max_emprestimos_cartao_credito_PF then 1 else 0 end end as scr_All_TimeHigh_credit_card_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_PF_Curr is null then 0 else (emprestimos_cartao_credito_PF_Curr - emprestimos_cartao_credito_PF_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_credit_card_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_PF_Curr is null then 0 else (emprestimos_cartao_credito_PF_Curr - emprestimos_cartao_credito_PF_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_credit_card_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_PF_Curr is null or emprestimos_cartao_credito_PF_Curr = 0 then 0 when emprestimos_cartao_credito_PF_5M = 0 then 2019 else emprestimos_cartao_credito_PF_Curr / emprestimos_cartao_credito_PF_5M - 1 end as scr_var_rel_6M_credit_card_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_PF_Curr is null or emprestimos_cartao_credito_PF_Curr = 0 then 0 when emprestimos_cartao_credito_PF_11M = 0 then 2019 else emprestimos_cartao_credito_PF_Curr / emprestimos_cartao_credito_PF_11M - 1 end as scr_var_rel_12M_credit_card_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_PF_Curr is null then 0 else Meses_Aumento_emprestimos_cartao_credito_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Rise_credit_card_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_PF_Curr is null then 0 else Meses_Reducao_emprestimos_cartao_credito_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Dive_credit_card_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_PF_Curr is null then 0 else months_sth_emprestimos_cartao_credito_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_dive_credit_card_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_PF_Curr is null then 0 else months_hth_emprestimos_cartao_credito_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_rise_credit_card_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_pf_Curr is null then 0 else Saldo_Amort_emprestimos_cartao_credito_pf * 1000 end as scr_credit_card_amortization_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_pf_Curr is null then 0 when Saldo_Amort_emprestimos_cartao_credito_pf = 0 then 0 else Saldo_Amort_emprestimos_cartao_credito_pf / Meses_Reducao_emprestimos_cartao_credito_pf / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_credit_card_pf_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_cartao_credito_pf_Curr is null then 0 when Saldo_Amort_emprestimos_cartao_credito_pf = 0 then 0 else Saldo_Amort_emprestimos_cartao_credito_pf / Meses_Reducao_emprestimos_cartao_credito_pf / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_credit_card_pf_over_loan_PMT,
--------------------------SCR PF MODALIDADE: EMPRESTIMOS CRÃ‰DITO PRA PJ
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_PF_Curr is null then 0 else Ever_emprestimos_credito_pPJ_PF * 1000 end as scr_ever_credit_for_company_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_PF_Curr is null then 0 else emprestimos_credito_pPJ_PF_Curr * 1000 end as scr_curr_credit_for_company_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_PF_Curr is null then 0 else emprestimos_credito_pPJ_PF_5M * 1000 end as scr_6M_credit_for_company_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_PF_Curr is null then 0 else emprestimos_credito_pPJ_PF_11M * 1000 end as scr_12M_credit_for_company_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_PF_Curr is null then 0 else emprestimos_credito_pPJ_PF_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_credit_for_company_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_PF_Curr is null or emprestimos_credito_pPJ_PF_Curr = 0 then 0 when emprestimos_credito_pPJ_PF_Curr > Long_Term_Debt_PF_Curr then 1 when Long_Term_Debt_PF_Curr = 0 then 2019 else emprestimos_credito_pPJ_PF_Curr/Long_Term_Debt_PF_Curr end as scr_curr_credit_for_company_PF_over_long_term_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_PF_Curr is null or emprestimos_credito_pPJ_PF_Curr = 0 then 0 when divida_atual_PF = 0 then 2019 else emprestimos_credito_pPJ_PF_Curr/divida_atual_PF end as scr_curr_credit_for_company_PF_over_short_term_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_PF_Curr is null then 0 else max_emprestimos_credito_pPJ_PF * 1000 end as scr_max_credit_for_company_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_PF_Curr is null or max_emprestimos_credito_pPJ_PF = 0 then 0 else emprestimos_credito_pPJ_PF_Curr / max_emprestimos_credito_pPJ_PF end as scr_curr_over_max_credit_for_company_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_PF_Curr is null or max_emprestimos_credito_pPJ_PF = 0 then 0 else case when emprestimos_credito_pPJ_PF_Curr = max_emprestimos_credito_pPJ_PF then 1 else 0 end end as scr_All_TimeHigh_credit_for_company_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_PF_Curr is null then 0 else (emprestimos_credito_pPJ_PF_Curr - emprestimos_credito_pPJ_PF_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_credit_for_company_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_PF_Curr is null then 0 else (emprestimos_credito_pPJ_PF_Curr - emprestimos_credito_pPJ_PF_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_credit_for_company_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_PF_Curr is null or emprestimos_credito_pPJ_PF_Curr = 0 then 0 when emprestimos_credito_pPJ_PF_5M = 0 then 2019 else emprestimos_credito_pPJ_PF_Curr / emprestimos_credito_pPJ_PF_5M - 1 end as scr_var_rel_6M_credit_for_company_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_PF_Curr is null or emprestimos_credito_pPJ_PF_Curr = 0 then 0 when emprestimos_credito_pPJ_PF_11M = 0 then 2019 else emprestimos_credito_pPJ_PF_Curr / emprestimos_credito_pPJ_PF_11M - 1 end as scr_var_rel_12M_credit_for_company_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_PF_Curr is null then 0 else Meses_Aumento_emprestimos_credito_pPJ_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Rise_credit_for_company_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_PF_Curr is null then 0 else Meses_Reducao_emprestimos_credito_pPJ_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Dive_credit_for_company_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_PF_Curr is null then 0 else months_sth_emprestimos_credito_pPJ_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_dive_credit_for_company_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_PF_Curr is null then 0 else months_hth_emprestimos_credito_pPJ_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_rise_credit_for_company_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_pf_Curr is null then 0 else Saldo_Amort_emprestimos_credito_pPJ_pf * 1000 end as scr_credit_for_company_amortization_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_pf_Curr is null then 0 when Saldo_Amort_emprestimos_credito_pPJ_pf = 0 then 0 else Saldo_Amort_emprestimos_credito_pPJ_pf / Meses_Reducao_emprestimos_credito_pPJ_pf / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_credit_for_company_pf_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_pPJ_pf_Curr is null then 0 when Saldo_Amort_emprestimos_credito_pPJ_pf = 0 then 0 else Saldo_Amort_emprestimos_credito_pPJ_pf / Meses_Reducao_emprestimos_credito_pPJ_pf / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_credit_for_company_pf_over_loan_PMT,
--------------------------SCR PF MODALIDADE: EMPRESTIMOS CREDITO CONSIGNADO
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_PF_Curr is null then 0 else Ever_emprestimos_credito_consignado_PF * 1000 end as scr_ever_credit_payroll_granted_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_PF_Curr is null then 0 else emprestimos_credito_consignado_PF_Curr * 1000 end as scr_curr_credit_payroll_granted_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_PF_Curr is null then 0 else emprestimos_credito_consignado_PF_5M * 1000 end as scr_6M_credit_payroll_granted_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_PF_Curr is null then 0 else emprestimos_credito_consignado_PF_11M * 1000 end as scr_12M_credit_payroll_granted_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_PF_Curr is null then 0 else emprestimos_credito_consignado_PF_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_credit_payroll_granted_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_PF_Curr is null or emprestimos_credito_consignado_PF_Curr = 0 then 0 when emprestimos_credito_consignado_PF_Curr > Long_Term_Debt_PF_Curr then 1 when Long_Term_Debt_PF_Curr = 0 then 2019 else emprestimos_credito_consignado_PF_Curr/Long_Term_Debt_PF_Curr end as scr_curr_credit_payroll_granted_PF_over_long_term_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_PF_Curr is null or emprestimos_credito_consignado_PF_Curr = 0 then 0 when divida_atual_PF = 0 then 2019 else emprestimos_credito_consignado_PF_Curr/divida_atual_PF end as scr_curr_credit_payroll_granted_PF_over_short_term_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_PF_Curr is null then 0 else max_emprestimos_credito_consignado_PF * 1000 end as scr_max_credit_payroll_granted_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_PF_Curr is null or max_emprestimos_credito_consignado_PF = 0 then 0 else emprestimos_credito_consignado_PF_Curr / max_emprestimos_credito_consignado_PF end as scr_curr_over_max_credit_payroll_granted_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_PF_Curr is null or max_emprestimos_credito_consignado_PF = 0 then 0 else case when emprestimos_credito_consignado_PF_Curr = max_emprestimos_credito_consignado_PF then 1 else 0 end end as scr_All_TimeHigh_credit_payroll_granted_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_PF_Curr is null then 0 else (emprestimos_credito_consignado_PF_Curr - emprestimos_credito_consignado_PF_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_credit_payroll_granted_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_PF_Curr is null then 0 else (emprestimos_credito_consignado_PF_Curr - emprestimos_credito_consignado_PF_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_credit_payroll_granted_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_PF_Curr is null or emprestimos_credito_consignado_PF_Curr = 0 then 0 when emprestimos_credito_consignado_PF_5M = 0 then 2019 else emprestimos_credito_consignado_PF_Curr / emprestimos_credito_consignado_PF_5M - 1 end as scr_var_rel_6M_credit_payroll_granted_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_PF_Curr is null or emprestimos_credito_consignado_PF_Curr = 0 then 0 when emprestimos_credito_consignado_PF_11M = 0 then 2019 else emprestimos_credito_consignado_PF_Curr / emprestimos_credito_consignado_PF_11M - 1 end as scr_var_rel_12M_credit_payroll_granted_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_PF_Curr is null then 0 else Meses_Aumento_emprestimos_credito_consignado_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Rise_credit_payroll_granted_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_PF_Curr is null then 0 else Meses_Reducao_emprestimos_credito_consignado_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Dive_credit_payroll_granted_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_PF_Curr is null then 0 else months_sth_emprestimos_credito_consignado_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_dive_credit_payroll_granted_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_PF_Curr is null then 0 else months_hth_emprestimos_credito_consignado_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_rise_credit_payroll_granted_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_pf_Curr is null then 0 else Saldo_Amort_emprestimos_credito_consignado_pf * 1000 end as scr_credit_payroll_granted_amortization_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_pf_Curr is null then 0 when Saldo_Amort_emprestimos_credito_consignado_pf = 0 then 0 else Saldo_Amort_emprestimos_credito_consignado_pf / Meses_Reducao_emprestimos_credito_consignado_pf / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_credit_payroll_granted_pf_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_credito_consignado_pf_Curr is null then 0 when Saldo_Amort_emprestimos_credito_consignado_pf = 0 then 0 else Saldo_Amort_emprestimos_credito_consignado_pf / Meses_Reducao_emprestimos_credito_consignado_pf / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_credit_payroll_granted_pf_over_loan_PMT,
--------------------------SCR PF MODALIDADE: EMPRESTIMOS OUTROS EMPRÃ‰STIMOS
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_PF_Curr is null then 0 else Ever_emprestimos_outros_emprestimos_PF * 1000 end as scr_ever_other_loans_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_PF_Curr is null then 0 else emprestimos_outros_emprestimos_PF_Curr * 1000 end as scr_curr_other_loans_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_PF_Curr is null then 0 else emprestimos_outros_emprestimos_PF_5M * 1000 end as scr_6M_other_loans_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_PF_Curr is null then 0 else emprestimos_outros_emprestimos_PF_11M * 1000 end as scr_12M_other_loans_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_PF_Curr is null then 0 else emprestimos_outros_emprestimos_PF_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_other_loans_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_PF_Curr is null or emprestimos_outros_emprestimos_PF_Curr = 0 then 0 when emprestimos_outros_emprestimos_PF_Curr > Long_Term_Debt_PF_Curr then 1 when Long_Term_Debt_PF_Curr = 0 then 2019 else emprestimos_outros_emprestimos_PF_Curr/Long_Term_Debt_PF_Curr end as scr_curr_other_loans_PF_over_long_term_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_PF_Curr is null or emprestimos_outros_emprestimos_PF_Curr = 0 then 0 when divida_atual_PF = 0 then 2019 else emprestimos_outros_emprestimos_PF_Curr/divida_atual_PF end as scr_curr_other_loans_PF_over_short_term_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_PF_Curr is null then 0 else max_emprestimos_outros_emprestimos_PF * 1000 end as scr_max_other_loans_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_PF_Curr is null or max_emprestimos_outros_emprestimos_PF = 0 then 0 else emprestimos_outros_emprestimos_PF_Curr / max_emprestimos_outros_emprestimos_PF end as scr_curr_over_max_other_loans_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_PF_Curr is null or max_emprestimos_outros_emprestimos_PF = 0 then 0 else case when emprestimos_outros_emprestimos_PF_Curr = max_emprestimos_outros_emprestimos_PF then 1 else 0 end end as scr_All_TimeHigh_other_loans_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_PF_Curr is null then 0 else (emprestimos_outros_emprestimos_PF_Curr - emprestimos_outros_emprestimos_PF_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_other_loans_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_PF_Curr is null then 0 else (emprestimos_outros_emprestimos_PF_Curr - emprestimos_outros_emprestimos_PF_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_other_loans_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_PF_Curr is null or emprestimos_outros_emprestimos_PF_Curr = 0 then 0 when emprestimos_outros_emprestimos_PF_5M = 0 then 2019 else emprestimos_outros_emprestimos_PF_Curr / emprestimos_outros_emprestimos_PF_5M - 1 end as scr_var_rel_6M_other_loans_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_PF_Curr is null or emprestimos_outros_emprestimos_PF_Curr = 0 then 0 when emprestimos_outros_emprestimos_PF_11M = 0 then 2019 else emprestimos_outros_emprestimos_PF_Curr / emprestimos_outros_emprestimos_PF_11M - 1 end as scr_var_rel_12M_other_loans_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_PF_Curr is null then 0 else Meses_Aumento_emprestimos_outros_emprestimos_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Rise_other_loans_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_PF_Curr is null then 0 else Meses_Reducao_emprestimos_outros_emprestimos_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Dive_other_loans_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_PF_Curr is null then 0 else months_sth_emprestimos_outros_emprestimos_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_dive_other_loans_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_PF_Curr is null then 0 else months_hth_emprestimos_outros_emprestimos_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_rise_other_loans_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_pf_Curr is null then 0 else Saldo_Amort_emprestimos_outros_emprestimos_pf * 1000 end as scr_other_loans_amortization_pf,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_pf_Curr is null then 0 when Saldo_Amort_emprestimos_outros_emprestimos_pf = 0 then 0 else Saldo_Amort_emprestimos_outros_emprestimos_pf / Meses_Reducao_emprestimos_outros_emprestimos_pf / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_other_loans_pf_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when emprestimos_outros_emprestimos_pf_Curr is null then 0 when Saldo_Amort_emprestimos_outros_emprestimos_pf = 0 then 0 else Saldo_Amort_emprestimos_outros_emprestimos_pf / Meses_Reducao_emprestimos_outros_emprestimos_pf / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_other_loans_pf_over_loan_PMT,
--------------------------SCR PF MODALIDADE: FINANCIAMENTO DE VEÃ�CULOS
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_PF_Curr is null then 0 else Ever_financiamento_veiculo_PF * 1000 end as scr_ever_vehicle_financing_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_PF_Curr is null then 0 else financiamento_veiculo_PF_Curr * 1000 end as scr_curr_vehicle_financing_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_PF_Curr is null then 0 else financiamento_veiculo_PF_5M * 1000 end as scr_6M_vehicle_financing_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_PF_Curr is null then 0 else financiamento_veiculo_PF_11M * 1000 end as scr_12M_vehicle_financing_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_PF_Curr is null then 0 else financiamento_veiculo_PF_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_vehicle_financing_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_PF_Curr is null or financiamento_veiculo_PF_Curr = 0 then 0 when financiamento_veiculo_PF_Curr > Long_Term_Debt_PF_Curr then 1 when Long_Term_Debt_PF_Curr = 0 then 2019 else financiamento_veiculo_PF_Curr/Long_Term_Debt_PF_Curr end as scr_curr_vehicle_financing_PF_over_long_term_debt,
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_PF_Curr is null or financiamento_veiculo_PF_Curr = 0 then 0 when divida_atual_PF = 0 then 2019 else financiamento_veiculo_PF_Curr/divida_atual_PF end as scr_curr_vehicle_financing_PF_over_short_term_debt,
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_PF_Curr is null then 0 else max_financiamento_veiculo_PF * 1000 end as scr_max_vehicle_financing_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_PF_Curr is null or max_financiamento_veiculo_PF = 0 then 0 else financiamento_veiculo_PF_Curr / max_financiamento_veiculo_PF end as scr_curr_over_max_vehicle_financing_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_PF_Curr is null or max_financiamento_veiculo_PF = 0 then 0 else case when financiamento_veiculo_PF_Curr = max_financiamento_veiculo_PF then 1 else 0 end end as scr_All_TimeHigh_vehicle_financing_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_PF_Curr is null then 0 else (financiamento_veiculo_PF_Curr - financiamento_veiculo_PF_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_vehicle_financing_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_PF_Curr is null then 0 else (financiamento_veiculo_PF_Curr - financiamento_veiculo_PF_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_vehicle_financing_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_PF_Curr is null or financiamento_veiculo_PF_Curr = 0 then 0 when financiamento_veiculo_PF_5M = 0 then 2019 else financiamento_veiculo_PF_Curr / financiamento_veiculo_PF_5M - 1 end as scr_var_rel_6M_vehicle_financing_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_PF_Curr is null or financiamento_veiculo_PF_Curr = 0 then 0 when financiamento_veiculo_PF_11M = 0 then 2019 else financiamento_veiculo_PF_Curr / financiamento_veiculo_PF_11M - 1 end as scr_var_rel_12M_vehicle_financing_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_PF_Curr is null then 0 else Meses_Aumento_financiamento_veiculo_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Rise_vehicle_financing_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_PF_Curr is null then 0 else Meses_Reducao_financiamento_veiculo_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Dive_vehicle_financing_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_PF_Curr is null then 0 else months_sth_financiamento_veiculo_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_dive_vehicle_financing_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_PF_Curr is null then 0 else months_hth_financiamento_veiculo_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_rise_vehicle_financing_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_pf_Curr is null then 0 else Saldo_Amort_financiamento_veiculo_pf * 1000 end as scr_vehicle_financing_amortization_pf,
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_pf_Curr is null then 0 when Saldo_Amort_financiamento_veiculo_pf = 0 then 0 else Saldo_Amort_financiamento_veiculo_pf / Meses_Reducao_financiamento_veiculo_pf / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_vehicle_financing_pf_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when financiamento_veiculo_pf_Curr is null then 0 when Saldo_Amort_financiamento_veiculo_pf = 0 then 0 else Saldo_Amort_financiamento_veiculo_pf / Meses_Reducao_financiamento_veiculo_pf / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_vehicle_financing_pf_over_loan_PMT,
--------------------------SCR PF MODALIDADE: FINANCIAMENTO IMOBILIÃ�RIO
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_PF_Curr is null then 0 else Ever_financiamento_imobiliario_PF * 1000 end as scr_ever_realty_financing_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_PF_Curr is null then 0 else financiamento_imobiliario_PF_Curr * 1000 end as scr_curr_realty_financing_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_PF_Curr is null then 0 else financiamento_imobiliario_PF_5M * 1000 end as scr_6M_realty_financing_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_PF_Curr is null then 0 else financiamento_imobiliario_PF_11M * 1000 end as scr_12M_realty_financing_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_PF_Curr is null then 0 else financiamento_imobiliario_PF_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_realty_financing_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_PF_Curr is null or financiamento_imobiliario_PF_Curr = 0 then 0 when financiamento_imobiliario_PF_Curr > Long_Term_Debt_PF_Curr then 1 when Long_Term_Debt_PF_Curr = 0 then 2019 else financiamento_imobiliario_PF_Curr/Long_Term_Debt_PF_Curr end as scr_curr_realty_financing_PF_over_long_term_debt,
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_PF_Curr is null or financiamento_imobiliario_PF_Curr = 0 then 0 when divida_atual_PF = 0 then 2019 else financiamento_imobiliario_PF_Curr/divida_atual_PF end as scr_curr_realty_financing_PF_over_short_term_debt,
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_PF_Curr is null then 0 else max_financiamento_imobiliario_PF * 1000 end as scr_max_realty_financing_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_PF_Curr is null or max_financiamento_imobiliario_PF = 0 then 0 else financiamento_imobiliario_PF_Curr / max_financiamento_imobiliario_PF end as scr_curr_over_max_realty_financing_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_PF_Curr is null or max_financiamento_imobiliario_PF = 0 then 0 else case when financiamento_imobiliario_PF_Curr = max_financiamento_imobiliario_PF then 1 else 0 end end as scr_All_TimeHigh_realty_financing_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_PF_Curr is null then 0 else (financiamento_imobiliario_PF_Curr - financiamento_imobiliario_PF_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_realty_financing_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_PF_Curr is null then 0 else (financiamento_imobiliario_PF_Curr - financiamento_imobiliario_PF_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_realty_financing_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_PF_Curr is null or financiamento_imobiliario_PF_Curr = 0 then 0 when financiamento_imobiliario_PF_5M = 0 then 2019 else financiamento_imobiliario_PF_Curr / financiamento_imobiliario_PF_5M - 1 end as scr_var_rel_6M_realty_financing_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_PF_Curr is null or financiamento_imobiliario_PF_Curr = 0 then 0 when financiamento_imobiliario_PF_11M = 0 then 2019 else financiamento_imobiliario_PF_Curr / financiamento_imobiliario_PF_11M - 1 end as scr_var_rel_12M_realty_financing_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_PF_Curr is null then 0 else Meses_Aumento_financiamento_imobiliario_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Rise_realty_financing_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_PF_Curr is null then 0 else Meses_Reducao_financiamento_imobiliario_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Dive_realty_financing_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_PF_Curr is null then 0 else months_sth_financiamento_imobiliario_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_dive_realty_financing_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_PF_Curr is null then 0 else months_hth_financiamento_imobiliario_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_rise_realty_financing_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_pf_Curr is null then 0 else Saldo_Amort_financiamento_imobiliario_pf * 1000 end as scr_realty_financing_amortization_pf,
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_pf_Curr is null then 0 when Saldo_Amort_financiamento_imobiliario_pf = 0 then 0 else Saldo_Amort_financiamento_imobiliario_pf / Meses_Reducao_financiamento_imobiliario_pf / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_realty_financing_pf_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when financiamento_imobiliario_pf_Curr is null then 0 when Saldo_Amort_financiamento_imobiliario_pf = 0 then 0 else Saldo_Amort_financiamento_imobiliario_pf / Meses_Reducao_financiamento_imobiliario_pf / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_realty_financing_pf_over_loan_PMT,
--------------------------SCR PF MODALIDADE: ADIANTAMENTOS E DESCONTOS DE DIREITOS CREDITORIOS
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_PF_Curr is null then 0 else Ever_adiantamentosdescontos_PF * 1000 end as scr_ever_cash_deposit_advance_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_PF_Curr is null then 0 else adiantamentosdescontos_PF_Curr * 1000 end as scr_curr_cash_deposit_advance_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_PF_Curr is null then 0 else adiantamentosdescontos_PF_5M * 1000 end as scr_6M_cash_deposit_advance_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_PF_Curr is null then 0 else adiantamentosdescontos_PF_11M * 1000 end as scr_12M_cash_deposit_advance_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_PF_Curr is null then 0 else adiantamentosdescontos_PF_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_cash_deposit_advance_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_PF_Curr is null or adiantamentosdescontos_PF_Curr = 0 then 0 when adiantamentosdescontos_PF_Curr > Long_Term_Debt_PF_Curr then 1 when Long_Term_Debt_PF_Curr = 0 then 2019 else adiantamentosdescontos_PF_Curr/Long_Term_Debt_PF_Curr end as scr_curr_cash_deposit_advance_PF_over_long_term_debt,
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_PF_Curr is null or adiantamentosdescontos_PF_Curr = 0 then 0 when divida_atual_PF = 0 then 2019 else adiantamentosdescontos_PF_Curr/divida_atual_PF end as scr_curr_cash_deposit_advance_PF_over_short_term_debt,
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_PF_Curr is null then 0 else max_adiantamentosdescontos_PF * 1000 end as scr_max_cash_deposit_advance_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_PF_Curr is null or max_adiantamentosdescontos_PF = 0 then 0 else adiantamentosdescontos_PF_Curr / max_adiantamentosdescontos_PF end as scr_curr_over_max_cash_deposit_advance_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_PF_Curr is null or max_adiantamentosdescontos_PF = 0 then 0 else case when adiantamentosdescontos_PF_Curr = max_adiantamentosdescontos_PF then 1 else 0 end end as scr_All_TimeHigh_cash_deposit_advance_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_PF_Curr is null then 0 else (adiantamentosdescontos_PF_Curr - adiantamentosdescontos_PF_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_cash_deposit_advance_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_PF_Curr is null then 0 else (adiantamentosdescontos_PF_Curr - adiantamentosdescontos_PF_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_cash_deposit_advance_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_PF_Curr is null or adiantamentosdescontos_PF_Curr = 0 then 0 when adiantamentosdescontos_PF_5M = 0 then 2019 else adiantamentosdescontos_PF_Curr / adiantamentosdescontos_PF_5M - 1 end as scr_var_rel_6M_cash_deposit_advance_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_PF_Curr is null or adiantamentosdescontos_PF_Curr = 0 then 0 when adiantamentosdescontos_PF_11M = 0 then 2019 else adiantamentosdescontos_PF_Curr / adiantamentosdescontos_PF_11M - 1 end as scr_var_rel_12M_cash_deposit_advance_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_PF_Curr is null then 0 else Meses_Aumento_adiantamentosdescontos_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Rise_cash_deposit_advance_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_PF_Curr is null then 0 else Meses_Reducao_adiantamentosdescontos_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Dive_cash_deposit_advance_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_PF_Curr is null then 0 else months_sth_adiantamentosdescontos_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_dive_cash_deposit_advance_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_PF_Curr is null then 0 else months_hth_adiantamentosdescontos_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_rise_cash_deposit_advance_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_pf_Curr is null then 0 else Saldo_Amort_adiantamentosdescontos_pf * 1000 end as scr_cash_deposit_advance_amortization_pf,
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_pf_Curr is null then 0 when Saldo_Amort_adiantamentosdescontos_pf = 0 then 0 else Saldo_Amort_adiantamentosdescontos_pf / Meses_Reducao_adiantamentosdescontos_pf / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_cash_deposit_advance_pf_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when adiantamentosdescontos_pf_Curr is null then 0 when Saldo_Amort_adiantamentosdescontos_pf = 0 then 0 else Saldo_Amort_adiantamentosdescontos_pf / Meses_Reducao_adiantamentosdescontos_pf / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_cash_deposit_advance_pf_over_loan_PMT,
--------------------------SCR PF MODALIDADE: OUTROS FINANCIAMENTOS
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_PF_Curr is null then 0 else Ever_outrosfinanciamentos_PF * 1000 end as scr_ever_other_financings_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_PF_Curr is null then 0 else outrosfinanciamentos_PF_Curr * 1000 end as scr_curr_other_financings_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_PF_Curr is null then 0 else outrosfinanciamentos_PF_5M * 1000 end as scr_6M_other_financings_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_PF_Curr is null then 0 else outrosfinanciamentos_PF_11M * 1000 end as scr_12M_other_financings_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_PF_Curr is null then 0 else outrosfinanciamentos_PF_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_other_financings_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_PF_Curr is null or outrosfinanciamentos_PF_Curr = 0 then 0 when outrosfinanciamentos_PF_Curr > Long_Term_Debt_PF_Curr then 1 when Long_Term_Debt_PF_Curr = 0 then 2019 else outrosfinanciamentos_PF_Curr/Long_Term_Debt_PF_Curr end as scr_curr_other_financings_PF_over_long_term_debt,
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_PF_Curr is null or outrosfinanciamentos_PF_Curr = 0 then 0 when divida_atual_PF = 0 then 2019 else outrosfinanciamentos_PF_Curr/divida_atual_PF end as scr_curr_other_financings_PF_over_short_term_debt,
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_PF_Curr is null then 0 else max_outrosfinanciamentos_PF * 1000 end as scr_max_other_financings_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_PF_Curr is null or max_outrosfinanciamentos_PF = 0 then 0 else outrosfinanciamentos_PF_Curr / max_outrosfinanciamentos_PF end as scr_curr_over_max_other_financings_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_PF_Curr is null or max_outrosfinanciamentos_PF = 0 then 0 else case when outrosfinanciamentos_PF_Curr = max_outrosfinanciamentos_PF then 1 else 0 end end as scr_All_TimeHigh_other_financings_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_PF_Curr is null then 0 else (outrosfinanciamentos_PF_Curr - outrosfinanciamentos_PF_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_other_financings_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_PF_Curr is null then 0 else (outrosfinanciamentos_PF_Curr - outrosfinanciamentos_PF_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_other_financings_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_PF_Curr is null or outrosfinanciamentos_PF_Curr = 0 then 0 when outrosfinanciamentos_PF_5M = 0 then 2019 else outrosfinanciamentos_PF_Curr / outrosfinanciamentos_PF_5M - 1 end as scr_var_rel_6M_other_financings_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_PF_Curr is null or outrosfinanciamentos_PF_Curr = 0 then 0 when outrosfinanciamentos_PF_11M = 0 then 2019 else outrosfinanciamentos_PF_Curr / outrosfinanciamentos_PF_11M - 1 end as scr_var_rel_12M_other_financings_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_PF_Curr is null then 0 else Meses_Aumento_outrosfinanciamentos_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Rise_other_financings_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_PF_Curr is null then 0 else Meses_Reducao_outrosfinanciamentos_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Dive_other_financings_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_PF_Curr is null then 0 else months_sth_outrosfinanciamentos_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_dive_other_financings_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_PF_Curr is null then 0 else months_hth_outrosfinanciamentos_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_rise_other_financings_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_pf_Curr is null then 0 else Saldo_Amort_outrosfinanciamentos_pf * 1000 end as scr_other_financings_amortization_pf,
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_pf_Curr is null then 0 when Saldo_Amort_outrosfinanciamentos_pf = 0 then 0 else Saldo_Amort_outrosfinanciamentos_pf / Meses_Reducao_outrosfinanciamentos_pf / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_other_financings_pf_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when outrosfinanciamentos_pf_Curr is null then 0 when Saldo_Amort_outrosfinanciamentos_pf = 0 then 0 else Saldo_Amort_outrosfinanciamentos_pf / Meses_Reducao_outrosfinanciamentos_pf / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_other_financings_pf_over_loan_PMT,
--------------------------SCR PF MODALIDADE: HOME EQUITY
	case when teve_scr_pf_tirado = 0 then null when homeequity_PF_Curr is null then 0 else Ever_homeequity_PF * 1000 end as scr_ever_home_equity_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when homeequity_PF_Curr is null then 0 else homeequity_PF_Curr * 1000 end as scr_curr_home_equity_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when homeequity_PF_Curr is null then 0 else homeequity_PF_5M * 1000 end as scr_6M_home_equity_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when homeequity_PF_Curr is null then 0 else homeequity_PF_11M * 1000 end as scr_12M_home_equity_debt_PF,
	case when teve_scr_pf_tirado = 0 then null when homeequity_PF_Curr is null then 0 else homeequity_PF_Curr/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_Curr_home_equity_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when homeequity_PF_Curr is null or homeequity_PF_Curr = 0 then 0 when homeequity_PF_Curr > Long_Term_Debt_PF_Curr then 1 when Long_Term_Debt_PF_Curr = 0 then 2019 else homeequity_PF_Curr/Long_Term_Debt_PF_Curr end as scr_curr_home_equity_PF_over_long_term_debt,
	case when teve_scr_pf_tirado = 0 then null when homeequity_PF_Curr is null or homeequity_PF_Curr = 0 then 0 when divida_atual_PF = 0 then 2019 else homeequity_PF_Curr/divida_atual_PF end as scr_curr_home_equity_PF_over_short_term_debt,
	case when teve_scr_pf_tirado = 0 then null when homeequity_PF_Curr is null then 0 else max_homeequity_PF * 1000 end as scr_max_home_equity_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when homeequity_PF_Curr is null or max_homeequity_PF = 0 then 0 else homeequity_PF_Curr / max_homeequity_PF end as scr_curr_over_max_home_equity_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when homeequity_PF_Curr is null or max_homeequity_PF = 0 then 0 else case when homeequity_PF_Curr = max_homeequity_PF then 1 else 0 end end as scr_All_TimeHigh_home_equity_PF_debt,
	case when teve_scr_pf_tirado = 0 then null when homeequity_PF_Curr is null then 0 else (homeequity_PF_Curr - homeequity_PF_5M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_6M_home_equity_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when homeequity_PF_Curr is null then 0 else (homeequity_PF_Curr - homeequity_PF_11M)/case month_revenue when 0 then null else month_revenue end/12*1000 end as scr_var_abs_12M_home_equity_debt_PF_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when homeequity_PF_Curr is null or homeequity_PF_Curr = 0 then 0 when homeequity_PF_5M = 0 then 2019 else homeequity_PF_Curr / homeequity_PF_5M - 1 end as scr_var_rel_6M_home_equity_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when homeequity_PF_Curr is null or homeequity_PF_Curr = 0 then 0 when homeequity_PF_11M = 0 then 2019 else homeequity_PF_Curr / homeequity_PF_11M - 1 end as scr_var_rel_12M_home_equity_debt_pf,
	case when teve_scr_pf_tirado = 0 then null when homeequity_PF_Curr is null then 0 else Meses_Aumento_homeequity_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Rise_home_equity_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when homeequity_PF_Curr is null then 0 else Meses_Reducao_homeequity_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_Months_Dive_home_equity_debt_pf_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when homeequity_PF_Curr is null then 0 else months_sth_homeequity_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_dive_home_equity_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when homeequity_PF_Curr is null then 0 else months_hth_homeequity_PF / (case when qtd_meses_modalidade_PF <= 1 then 1 else qtd_meses_modalidade_PF-1 end) end as scr_month_consecutive_rise_home_equity_PF_debt_over_total_months,
	case when teve_scr_pf_tirado = 0 then null when homeequity_pf_Curr is null then 0 else Saldo_Amort_homeequity_pf * 1000 end as scr_home_equity_amortization_pf,
	case when teve_scr_pf_tirado = 0 then null when homeequity_pf_Curr is null then 0 when Saldo_Amort_homeequity_pf = 0 then 0 else Saldo_Amort_homeequity_pf / Meses_Reducao_homeequity_pf / case month_revenue when 0 then null else month_revenue end / 12 * 1000 end as scr_Avg_Amortization_home_equity_pf_over_revenue,
	case when teve_scr_pf_tirado = 0 then null when homeequity_pf_Curr is null then 0 when Saldo_Amort_homeequity_pf = 0 then 0 else Saldo_Amort_homeequity_pf / Meses_Reducao_homeequity_pf / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_home_equity_pf_over_loan_PMT,
--------------------------SERASA
	n_cnpjs_relacionados as experian_number_of_related_companies,
	n_cpfs_relacionados as experian_number_of_related_shareholders,
	is_shareholder as experian_is_shareholder,
	count_socios as experian_count_shareholders,	
--PROBLEMAS DA EMPRESA
	empresas_problema as experian_derogatory_marked_related_companies,
	empresa_divida_valor as experian_company_overdue_debt_value,
	empresa_divida_valor / case month_revenue when 0 then null else month_revenue end / 12 as experian_company_overdue_debt_value_byrevenue,
	empresa_divida_unit as experian_company_overdue_debt_unit,
	empresa_ccf_valor as experian_company_bad_check_value,
	empresa_ccf_valor / case month_revenue when 0 then null else month_revenue end / 12 as experian_company_bad_check_value_byrevenue,
	empresa_ccf_unit as experian_company_bad_check_unit,
	empresa_protesto_valor as experian_company_notary_registry_protests_value,
	empresa_protesto_valor / case month_revenue when 0 then null else month_revenue end / 12 as experian_company_notary_registry_protests_value_byrevenue,
	empresa_protesto_unit as experian_company_notary_registry_protests_unit,
	empresa_acao_valor as experian_company_legal_action_value,
	empresa_acao_valor / case month_revenue when 0 then null else month_revenue end / 12 as experian_company_legal_action_value_byrevenue,
	empresa_acao_unit as experian_company_legal_action_unit,
	empresa_pefin_valor as experian_company_negative_mark_by_non_fin_companies_value,
	empresa_pefin_valor / case month_revenue when 0 then null else month_revenue end / 12 as experian_company_negative_mark_by_non_fin_companies_value_byrevenue,
	empresa_pefin_unit as experian_company_negative_mark_by_non_fin_companies_unit,
	empresa_refin_valor as experian_company_negative_mark_by_fin_companies_value,
	empresa_refin_valor / case month_revenue when 0 then null else month_revenue end / 12 as experian_company_negative_mark_by_fin_companies_value_byrevenue,
	empresa_refin_unit as experian_company_negative_mark_by_fin_companies_unit,
	empresa_spc_valor as experian_company_bureau_derogatory_value,
	empresa_spc_valor / case month_revenue when 0 then null else month_revenue end / 12 as experian_company_bureau_derogatory_value_byrevenue,
	empresa_spc_unit as experian_company_bureau_derogatory_unit,
	empresa_total_valor as experian_company_derogatory_mark_total_value,
	empresa_total_valor / case month_revenue when 0 then null else month_revenue end / 12 as experian_company_derogatory_mark_total_value_byrevenue,
	empresa_total_unit as experian_company_derogatory_mark_total_unit,
--PROBLEMAS DOS SOCIOS
	socio_divida_valor as experian_sh_overdue_debt_value,
	socio_divida_valor / case month_revenue when 0 then null else month_revenue end / 12 as experian_sh_overdue_debt_value_byrevenue,
	socio_divida_unit as experian_sh_overdue_debt_unit,
	socio_ccf_valor as experian_sh_bad_check_value,
	socio_ccf_valor / case month_revenue when 0 then null else month_revenue end / 12 as experian_sh_bad_check_value_byrevenue,
	socio_ccf_unit as experian_sh_bad_check_unit,
	socio_protesto_valor as experian_sh_notary_registry_protests_value,
	socio_protesto_valor / case month_revenue when 0 then null else month_revenue end / 12 as experian_sh_notary_registry_protests_value_byrevenue,
	socio_protesto_unit as experian_sh_notary_registry_protests_unit,
	socio_acao_valor as experian_sh_legal_action_value,
	socio_acao_valor / case month_revenue when 0 then null else month_revenue end / 12 as experian_sh_legal_action_value_byrevenue,
	socio_acao_unit as experian_sh_legal_action_unit,
	socio_pefin_valor as experian_sh_negative_mark_by_non_fin_companies_value,
	socio_pefin_valor / case month_revenue when 0 then null else month_revenue end / 12 as experian_sh_negative_mark_by_non_fin_companies_value_byrevenue,
	socio_pefin_unit as experian_sh_negative_mark_by_non_fin_companies_unit,
	socio_refin_valor as experian_sh_negative_mark_by_fin_companies_value,
	socio_refin_valor / case month_revenue when 0 then null else month_revenue end / 12 as experian_sh_negative_mark_by_fin_companies_value_byrevenue,
	socio_refin_unit as experian_sh_negative_mark_by_fin_companies_unit,
	socio_spc_valor as experian_sh_bureau_derogatory_value,
	socio_spc_valor / case month_revenue when 0 then null else month_revenue end / 12 as experian_sh_bureau_derogatory_value_byrevenue,
	socio_spc_unit as experian_sh_bureau_derogatory_unit,
	socios_total_valor as experian_sh_derogatory_mark_total_value,
	socios_total_valor / case month_revenue when 0 then null else month_revenue end / 12 as experian_sh_derogatory_mark_total_value_byrevenue,
	socios_total_unit as experian_sh_derogatory_mark_total_unit,
--PROBLEMAS SOMADOS
	all_valor as experian_company_shareholders_derogatory_mark_total_value,
	all_valor / case month_revenue when 0 then null else month_revenue end / 12 as experian_company_shareholders_derogatory_mark_total_value_byrevenue,
	all_unit as experian_company_shareholders_derogatory_mark_total_unit,
--CONSULTAS SERASA + SPC
	consulta_factoring as experian_report_recently_taken_by_factoring,
	case when serasa_consultas.direct_prospect_id is null then null else consultas_serasa_atual + consultas_serasa_1m + consultas_serasa_2m + consultas_serasa_3m + consultas_serasa_4m + consultas_serasa_5m + consultas_serasa_6m + consultas_serasa_7m + consultas_serasa_8m + consultas_serasa_9m + consultas_serasa_10m + consultas_serasa_11m + coalesce(consultas_spc_atual,0) + coalesce(consultas_spc_1m,0) + coalesce(consultas_spc_2m,0) + coalesce(consultas_spc_3m,0) + coalesce(consultas_spc_4m,0) + coalesce(consultas_spc_5m,0) + coalesce(consultas_spc_6m,0) + coalesce(consultas_spc_7m,0) + coalesce(consultas_spc_8m,0) + coalesce(consultas_spc_9m,0) + coalesce(consultas_spc_10m,0) + coalesce(consultas_spc_11m,0) end as experian_all_inquiries_ever,
	case when serasa_consultas.direct_prospect_id is null then null else consultas_serasa_atual + coalesce(consultas_spc_atual,0) end as experian_all_inquiries_curr,
	case when serasa_consultas.direct_prospect_id is null then null else consultas_serasa_5m + coalesce(consultas_spc_5m,0) end as experian_all_inquiries_6m,
	case when serasa_consultas.direct_prospect_id is null then null else consultas_serasa_11m + coalesce(consultas_spc_11m,0) end as experian_all_inquiries_12m,
	case when serasa_consultas.direct_prospect_id is null then null else greatest(consultas_serasa_atual + coalesce(consultas_spc_atual,0), consultas_serasa_1m + coalesce(consultas_spc_1m,0), consultas_serasa_2m + coalesce(consultas_spc_2m,0), consultas_serasa_3m + coalesce(consultas_spc_3m,0), consultas_serasa_4m + coalesce(consultas_spc_4m,0), consultas_serasa_5m + coalesce(consultas_spc_5m,0), consultas_serasa_6m + coalesce(consultas_spc_6m,0), consultas_serasa_7m + coalesce(consultas_spc_7m,0), consultas_serasa_8m + coalesce(consultas_spc_8m,0), consultas_serasa_9m + coalesce(consultas_spc_9m,0), consultas_serasa_10m + coalesce(consultas_spc_10m,0), consultas_serasa_11m + coalesce(consultas_spc_11m,0)) end as experian_all_inquiries_max,
	case when serasa_consultas.direct_prospect_id is null then null else (consultas_serasa_atual + coalesce(consultas_spc_atual,0)) / greatest(consultas_serasa_atual + coalesce(consultas_spc_atual,0), consultas_serasa_1m + coalesce(consultas_spc_1m,0), consultas_serasa_2m + coalesce(consultas_spc_2m,0), consultas_serasa_3m + coalesce(consultas_spc_3m,0), consultas_serasa_4m + coalesce(consultas_spc_4m,0), consultas_serasa_5m + coalesce(consultas_spc_5m,0), consultas_serasa_6m + coalesce(consultas_spc_6m,0), consultas_serasa_7m + coalesce(consultas_spc_7m,0), consultas_serasa_8m + coalesce(consultas_spc_8m,0), consultas_serasa_9m + coalesce(consultas_spc_9m,0), consultas_serasa_10m + coalesce(consultas_spc_10m,0), consultas_serasa_11m + coalesce(consultas_spc_11m,0),1) end as experian_all_inquiries_atual_over_max,
	case when serasa_consultas.direct_prospect_id is null then null else (consultas_serasa_atual + coalesce(consultas_spc_atual,0)) / case (consultas_serasa_5m + coalesce(consultas_spc_5m,0)) when 0 then 2019 else (consultas_serasa_5m + coalesce(consultas_spc_5m,0)) end - 1 end as experian_all_inquiries_atual_over_6m,
	case when serasa_consultas.direct_prospect_id is null then null else (consultas_serasa_atual + coalesce(consultas_spc_atual,0)) / case (consultas_serasa_11m + coalesce(consultas_spc_11m,0)) when 0 then 2019 else (consultas_serasa_11m + coalesce(consultas_spc_11m,0)) end - 1 end as experian_all_inquiries_atual_over_12m,
	case when serasa_consultas.direct_prospect_id is null then null else 
		case when consultas_serasa_10m + coalesce(consultas_spc_10m,0) < consultas_serasa_11m + coalesce(consultas_spc_11m,0) then 1 else 0 end +
		case when consultas_serasa_9m + coalesce(consultas_spc_9m,0) < consultas_serasa_10m + coalesce(consultas_spc_10m,0) then 1 else 0 end +
		case when consultas_serasa_8m + coalesce(consultas_spc_8m,0) < consultas_serasa_9m + coalesce(consultas_spc_9m,0) then 1 else 0 end +
		case when consultas_serasa_7m + coalesce(consultas_spc_7m,0) < consultas_serasa_8m + coalesce(consultas_spc_8m,0) then 1 else 0 end +
		case when consultas_serasa_6m + coalesce(consultas_spc_6m,0) < consultas_serasa_7m + coalesce(consultas_spc_7m,0) then 1 else 0 end +
		case when consultas_serasa_5m + coalesce(consultas_spc_5m,0) < consultas_serasa_6m + coalesce(consultas_spc_6m,0) then 1 else 0 end +
		case when consultas_serasa_4m + coalesce(consultas_spc_4m,0) < consultas_serasa_5m + coalesce(consultas_spc_5m,0) then 1 else 0 end +
		case when consultas_serasa_3m + coalesce(consultas_spc_3m,0) < consultas_serasa_4m + coalesce(consultas_spc_4m,0) then 1 else 0 end +
		case when consultas_serasa_2m + coalesce(consultas_spc_2m,0) < consultas_serasa_3m + coalesce(consultas_spc_3m,0) then 1 else 0 end +
		case when consultas_serasa_1m + coalesce(consultas_spc_1m,0) < consultas_serasa_2m + coalesce(consultas_spc_2m,0) then 1 else 0 end +
		case when consultas_serasa_atual + coalesce(consultas_spc_atual,0) < consultas_serasa_1m + coalesce(consultas_spc_1m,0) then 1 else 0 end::float end as experian_all_inquiries_months_dive,
	case when serasa_consultas.direct_prospect_id is null then null else 
		case when consultas_serasa_10m + coalesce(consultas_spc_10m,0) > consultas_serasa_11m + coalesce(consultas_spc_11m,0) then 1 else 0 end +
		case when consultas_serasa_9m + coalesce(consultas_spc_9m,0) > consultas_serasa_10m + coalesce(consultas_spc_10m,0) then 1 else 0 end +
		case when consultas_serasa_8m + coalesce(consultas_spc_8m,0) > consultas_serasa_9m + coalesce(consultas_spc_9m,0) then 1 else 0 end +
		case when consultas_serasa_7m + coalesce(consultas_spc_7m,0) > consultas_serasa_8m + coalesce(consultas_spc_8m,0) then 1 else 0 end +
		case when consultas_serasa_6m + coalesce(consultas_spc_6m,0) > consultas_serasa_7m + coalesce(consultas_spc_7m,0) then 1 else 0 end +
		case when consultas_serasa_5m + coalesce(consultas_spc_5m,0) > consultas_serasa_6m + coalesce(consultas_spc_6m,0) then 1 else 0 end +
		case when consultas_serasa_4m + coalesce(consultas_spc_4m,0) > consultas_serasa_5m + coalesce(consultas_spc_5m,0) then 1 else 0 end +
		case when consultas_serasa_3m + coalesce(consultas_spc_3m,0) > consultas_serasa_4m + coalesce(consultas_spc_4m,0) then 1 else 0 end +
		case when consultas_serasa_2m + coalesce(consultas_spc_2m,0) > consultas_serasa_3m + coalesce(consultas_spc_3m,0) then 1 else 0 end +
		case when consultas_serasa_1m + coalesce(consultas_spc_1m,0) > consultas_serasa_2m + coalesce(consultas_spc_2m,0) then 1 else 0 end +
		case when consultas_serasa_atual + coalesce(consultas_spc_atual,0) > consultas_serasa_1m + coalesce(consultas_spc_1m,0) then 1 else 0 end::float end as experian_all_inquiries_months_rise,
	case when serasa_consultas.direct_prospect_id is null then null else 
		case when consultas_serasa_atual + coalesce(consultas_spc_atual,0) >= consultas_serasa_1m + coalesce(consultas_spc_1m,0) then 0 else
		case when consultas_serasa_1m + coalesce(consultas_spc_1m,0) >= consultas_serasa_2m + coalesce(consultas_spc_2m,0) then 1 else
		case when consultas_serasa_2m + coalesce(consultas_spc_2m,0) >= consultas_serasa_3m + coalesce(consultas_spc_3m,0) then 2 else
		case when consultas_serasa_3m + coalesce(consultas_spc_3m,0) >= consultas_serasa_4m + coalesce(consultas_spc_4m,0) then 3 else
		case when consultas_serasa_4m + coalesce(consultas_spc_4m,0) >= consultas_serasa_5m + coalesce(consultas_spc_5m,0) then 4 else
		case when consultas_serasa_5m + coalesce(consultas_spc_5m,0) >= consultas_serasa_6m + coalesce(consultas_spc_6m,0) then 5 else
		case when consultas_serasa_6m + coalesce(consultas_spc_6m,0) >= consultas_serasa_7m + coalesce(consultas_spc_7m,0) then 6 else
		case when consultas_serasa_7m + coalesce(consultas_spc_7m,0) >= consultas_serasa_8m + coalesce(consultas_spc_8m,0) then 7 else
		case when consultas_serasa_8m + coalesce(consultas_spc_8m,0) >= consultas_serasa_9m + coalesce(consultas_spc_9m,0) then 8 else
		case when consultas_serasa_9m + coalesce(consultas_spc_9m,0) >= consultas_serasa_10m + coalesce(consultas_spc_10m,0) then 9 else
		case when consultas_serasa_10m + coalesce(consultas_spc_10m,0) >= consultas_serasa_11m + coalesce(consultas_spc_11m,0) then 10 else 11 end end end end end end end end end end end::float end as experian_all_inquiries_consectuive_months_dive,
	case when serasa_consultas.direct_prospect_id is null then null else 
		case when consultas_serasa_atual + coalesce(consultas_spc_atual,0) <= consultas_serasa_1m + coalesce(consultas_spc_1m,0) then 0 else
		case when consultas_serasa_1m + coalesce(consultas_spc_1m,0) <= consultas_serasa_2m + coalesce(consultas_spc_2m,0) then 1 else
		case when consultas_serasa_2m + coalesce(consultas_spc_2m,0) <= consultas_serasa_3m + coalesce(consultas_spc_3m,0) then 2 else
		case when consultas_serasa_3m + coalesce(consultas_spc_3m,0) <= consultas_serasa_4m + coalesce(consultas_spc_4m,0) then 3 else
		case when consultas_serasa_4m + coalesce(consultas_spc_4m,0) <= consultas_serasa_5m + coalesce(consultas_spc_5m,0) then 4 else
		case when consultas_serasa_5m + coalesce(consultas_spc_5m,0) <= consultas_serasa_6m + coalesce(consultas_spc_6m,0) then 5 else
		case when consultas_serasa_6m + coalesce(consultas_spc_6m,0) <= consultas_serasa_7m + coalesce(consultas_spc_7m,0) then 6 else
		case when consultas_serasa_7m + coalesce(consultas_spc_7m,0) <= consultas_serasa_8m + coalesce(consultas_spc_8m,0) then 7 else
		case when consultas_serasa_8m + coalesce(consultas_spc_8m,0) <= consultas_serasa_9m + coalesce(consultas_spc_9m,0) then 8 else
		case when consultas_serasa_9m + coalesce(consultas_spc_9m,0) <= consultas_serasa_10m + coalesce(consultas_spc_10m,0) then 9 else
		case when consultas_serasa_10m + coalesce(consultas_spc_10m,0) <= consultas_serasa_11m + coalesce(consultas_spc_11m,0) then 10 else 11 end end end end end end end end end end end::float end as experian_all_inquiries_consectuive_months_rise
--------------------------
from direct_prospects dp
join temp_ids as aux on aux.direct_prospect_id = dp.direct_prospect_id
join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join (select o.* ,count_neg from offers o join (select direct_prospect_id,min(offer_id) as min_id,count(*) as count_neg from offers group by 1) as initial_off on initial_off.min_id = o.offer_id) as initialoff on initialoff.direct_prospect_id = dp.direct_prospect_id
left join offers o on o.offer_id = coalesce(acc_offer_id,max_offer_id,other_offer_id)
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on coalesce(max_id,min_id) = lr.loan_request_id) lr on lr.offer_id = coalesce(acc_offer_id,max_offer_id,other_offer_id)
left join loan_infos li on li.loan_info_id = lr.loan_request_id
left join bank_accounts ba on ba.bank_account_id = lr.bank_account_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
left join lucas_leal.geofusion_data gd on gd.pk = dp.state || dp.city
left join backtests.s4_s6_bizu_20190923 as bktest on bktest.lead_id = dp.direct_prospect_id
left join lucas_leal.backtest_serasa_jan2020 as bktest2 on bktest2.lead_id = dp.direct_prospect_id
left join (select distinct identificacao from parceiros_parceiro) pp on pp.identificacao = replace(dp.utm_source,'#/','')
--------------------------TABLE SCR HISTORICO PJ
left join(select direct_prospect_id,
	teve_scr_tirado,
	Num_Ops_PJ::float,
	Num_FIs_PJ::float,
	First_Relation_FI_PJ::float,
	Long_Term_Debt_PJ_Curr::float,
	Long_Term_Debt_PJ_1M::float,
	Long_Term_Debt_PJ_2M::float,
	Long_Term_Debt_PJ_3M::float,
	Long_Term_Debt_PJ_4M::float,
	Long_Term_Debt_PJ_5M::float,
	Long_Term_Debt_PJ_6M::float,
	Long_Term_Debt_PJ_7M::float,
	Long_Term_Debt_PJ_8M::float,
	Long_Term_Debt_PJ_9M::float,
	Long_Term_Debt_PJ_10M::float,
	Long_Term_Debt_PJ_11M::float,
	Ever_long_term_debt_PJ::float,
	Max_Long_Term_Debt_PJ::float,
	Overdue_PJ_Curr::float,
	Months_Since_Last_Overdue_PJ::float,
	Months_Overdue_PJ::float,
	Max_Overdue_PJ::float,
	Default_PJ_Curr::float,
	Months_Since_Last_Default_PJ::float,
	Months_Default_PJ::float,
	Max_Default_PJ::float,
	Qtd_meses_escopo_pj::float,
	case when Long_Term_Debt_PJ_22M > Long_Term_Debt_PJ_23M then 1 else 0 end +
	case when Long_Term_Debt_PJ_21M > Long_Term_Debt_PJ_22M then 1 else 0 end +
	case when Long_Term_Debt_PJ_20M > Long_Term_Debt_PJ_21M then 1 else 0 end +
	case when Long_Term_Debt_PJ_19M > Long_Term_Debt_PJ_20M then 1 else 0 end +
	case when Long_Term_Debt_PJ_18M > Long_Term_Debt_PJ_19M then 1 else 0 end +
	case when Long_Term_Debt_PJ_17M > Long_Term_Debt_PJ_18M then 1 else 0 end +
	case when Long_Term_Debt_PJ_16M > Long_Term_Debt_PJ_17M then 1 else 0 end +
	case when Long_Term_Debt_PJ_15M > Long_Term_Debt_PJ_16M then 1 else 0 end +
	case when Long_Term_Debt_PJ_14M > Long_Term_Debt_PJ_15M then 1 else 0 end +
	case when Long_Term_Debt_PJ_13M > Long_Term_Debt_PJ_14M then 1 else 0 end +
	case when Long_Term_Debt_PJ_12M > Long_Term_Debt_PJ_13M then 1 else 0 end +
	case when Long_Term_Debt_PJ_11M > Long_Term_Debt_PJ_12M then 1 else 0 end +
	case when Long_Term_Debt_PJ_10M > Long_Term_Debt_PJ_11M then 1 else 0 end +
	case when Long_Term_Debt_PJ_9M > Long_Term_Debt_PJ_10M then 1 else 0 end +
	case when Long_Term_Debt_PJ_8M > Long_Term_Debt_PJ_9M then 1 else 0 end +
	case when Long_Term_Debt_PJ_7M > Long_Term_Debt_PJ_8M then 1 else 0 end +
	case when Long_Term_Debt_PJ_6M > Long_Term_Debt_PJ_7M then 1 else 0 end +
	case when Long_Term_Debt_PJ_5M > Long_Term_Debt_PJ_6M then 1 else 0 end +
	case when Long_Term_Debt_PJ_4M > Long_Term_Debt_PJ_5M then 1 else 0 end +
	case when Long_Term_Debt_PJ_3M > Long_Term_Debt_PJ_4M then 1 else 0 end +
	case when Long_Term_Debt_PJ_2M > Long_Term_Debt_PJ_3M then 1 else 0 end +
	case when Long_Term_Debt_PJ_1M > Long_Term_Debt_PJ_2M then 1 else 0 end +
	case when Long_Term_Debt_PJ_Curr > Long_Term_Debt_PJ_1M then 1 else 0 end::float as Meses_Aumento_DividaPJ,
	case when Long_Term_Debt_PJ_22M < Long_Term_Debt_PJ_23M then 1 else 0 end +
	case when Long_Term_Debt_PJ_21M < Long_Term_Debt_PJ_22M then 1 else 0 end +
	case when Long_Term_Debt_PJ_20M < Long_Term_Debt_PJ_21M then 1 else 0 end +
	case when Long_Term_Debt_PJ_19M < Long_Term_Debt_PJ_20M then 1 else 0 end +
	case when Long_Term_Debt_PJ_18M < Long_Term_Debt_PJ_19M then 1 else 0 end +
	case when Long_Term_Debt_PJ_17M < Long_Term_Debt_PJ_18M then 1 else 0 end +
	case when Long_Term_Debt_PJ_16M < Long_Term_Debt_PJ_17M then 1 else 0 end +
	case when Long_Term_Debt_PJ_15M < Long_Term_Debt_PJ_16M then 1 else 0 end +
	case when Long_Term_Debt_PJ_14M < Long_Term_Debt_PJ_15M then 1 else 0 end +
	case when Long_Term_Debt_PJ_13M < Long_Term_Debt_PJ_14M then 1 else 0 end +
	case when Long_Term_Debt_PJ_12M < Long_Term_Debt_PJ_13M then 1 else 0 end +
	case when Long_Term_Debt_PJ_11M < Long_Term_Debt_PJ_12M then 1 else 0 end +
	case when Long_Term_Debt_PJ_10M < Long_Term_Debt_PJ_11M then 1 else 0 end +
	case when Long_Term_Debt_PJ_9M < Long_Term_Debt_PJ_10M then 1 else 0 end +
	case when Long_Term_Debt_PJ_8M < Long_Term_Debt_PJ_9M then 1 else 0 end +
	case when Long_Term_Debt_PJ_7M < Long_Term_Debt_PJ_8M then 1 else 0 end +
	case when Long_Term_Debt_PJ_6M < Long_Term_Debt_PJ_7M then 1 else 0 end +
	case when Long_Term_Debt_PJ_5M < Long_Term_Debt_PJ_6M then 1 else 0 end +
	case when Long_Term_Debt_PJ_4M < Long_Term_Debt_PJ_5M then 1 else 0 end +
	case when Long_Term_Debt_PJ_3M < Long_Term_Debt_PJ_4M then 1 else 0 end +
	case when Long_Term_Debt_PJ_2M < Long_Term_Debt_PJ_3M then 1 else 0 end +
	case when Long_Term_Debt_PJ_1M < Long_Term_Debt_PJ_2M then 1 else 0 end +
	case when Long_Term_Debt_PJ_Curr < Long_Term_Debt_PJ_1M then 1 else 0 end::float as Meses_Reducao_DividaPJ,
	(case when Long_Term_Debt_PJ_22M < Long_Term_Debt_PJ_23M then Long_Term_Debt_PJ_22M - Long_Term_Debt_PJ_23M else 0 end +
	case when Long_Term_Debt_PJ_21M < Long_Term_Debt_PJ_22M then Long_Term_Debt_PJ_21M - Long_Term_Debt_PJ_22M else 0 end +
	case when Long_Term_Debt_PJ_20M < Long_Term_Debt_PJ_21M then Long_Term_Debt_PJ_20M - Long_Term_Debt_PJ_21M else 0 end +
	case when Long_Term_Debt_PJ_19M < Long_Term_Debt_PJ_20M then Long_Term_Debt_PJ_19M - Long_Term_Debt_PJ_20M else 0 end +
	case when Long_Term_Debt_PJ_18M < Long_Term_Debt_PJ_19M then Long_Term_Debt_PJ_18M - Long_Term_Debt_PJ_19M else 0 end +
	case when Long_Term_Debt_PJ_17M < Long_Term_Debt_PJ_18M then Long_Term_Debt_PJ_17M - Long_Term_Debt_PJ_18M else 0 end +
	case when Long_Term_Debt_PJ_16M < Long_Term_Debt_PJ_17M then Long_Term_Debt_PJ_16M - Long_Term_Debt_PJ_17M else 0 end +
	case when Long_Term_Debt_PJ_15M < Long_Term_Debt_PJ_16M then Long_Term_Debt_PJ_15M - Long_Term_Debt_PJ_16M else 0 end +
	case when Long_Term_Debt_PJ_14M < Long_Term_Debt_PJ_15M then Long_Term_Debt_PJ_14M - Long_Term_Debt_PJ_15M else 0 end +
	case when Long_Term_Debt_PJ_13M < Long_Term_Debt_PJ_14M then Long_Term_Debt_PJ_13M - Long_Term_Debt_PJ_14M else 0 end +
	case when Long_Term_Debt_PJ_12M < Long_Term_Debt_PJ_13M then Long_Term_Debt_PJ_12M - Long_Term_Debt_PJ_13M else 0 end +
	case when Long_Term_Debt_PJ_11M < Long_Term_Debt_PJ_12M then Long_Term_Debt_PJ_11M - Long_Term_Debt_PJ_12M else 0 end +
	case when Long_Term_Debt_PJ_10M < Long_Term_Debt_PJ_11M then Long_Term_Debt_PJ_10M - Long_Term_Debt_PJ_11M else 0 end +
	case when Long_Term_Debt_PJ_9M < Long_Term_Debt_PJ_10M then Long_Term_Debt_PJ_9M - Long_Term_Debt_PJ_10M else 0 end +
	case when Long_Term_Debt_PJ_8M < Long_Term_Debt_PJ_9M then Long_Term_Debt_PJ_8M - Long_Term_Debt_PJ_9M else 0 end +
	case when Long_Term_Debt_PJ_7M < Long_Term_Debt_PJ_8M then Long_Term_Debt_PJ_7M - Long_Term_Debt_PJ_8M else 0 end +
	case when Long_Term_Debt_PJ_6M < Long_Term_Debt_PJ_7M then Long_Term_Debt_PJ_6M - Long_Term_Debt_PJ_7M else 0 end +
	case when Long_Term_Debt_PJ_5M < Long_Term_Debt_PJ_6M then Long_Term_Debt_PJ_5M - Long_Term_Debt_PJ_6M else 0 end +
	case when Long_Term_Debt_PJ_4M < Long_Term_Debt_PJ_5M then Long_Term_Debt_PJ_4M - Long_Term_Debt_PJ_5M else 0 end +
	case when Long_Term_Debt_PJ_3M < Long_Term_Debt_PJ_4M then Long_Term_Debt_PJ_3M - Long_Term_Debt_PJ_4M else 0 end +
	case when Long_Term_Debt_PJ_2M < Long_Term_Debt_PJ_3M then Long_Term_Debt_PJ_2M - Long_Term_Debt_PJ_3M else 0 end +
	case when Long_Term_Debt_PJ_1M < Long_Term_Debt_PJ_2M then Long_Term_Debt_PJ_1M - Long_Term_Debt_PJ_2M else 0 end +
	case when Long_Term_Debt_PJ_Curr < Long_Term_Debt_PJ_1M then Long_Term_Debt_PJ_Curr - Long_Term_Debt_PJ_1M else 0 end)*(-1)::float as Saldo_Amort_DividaPJ,
	case when Long_Term_Debt_PJ_1M is null then null else
	case when Long_Term_Debt_PJ_Curr >= Long_Term_Debt_PJ_1M then 0 else
	case when Long_Term_Debt_PJ_1M >= Long_Term_Debt_PJ_2M or Long_Term_Debt_PJ_2M is null then 1 else
	case when Long_Term_Debt_PJ_2M >= Long_Term_Debt_PJ_3M or Long_Term_Debt_PJ_3M is null then 2 else
	case when Long_Term_Debt_PJ_3M >= Long_Term_Debt_PJ_4M or Long_Term_Debt_PJ_4M is null then 3 else
	case when Long_Term_Debt_PJ_4M >= Long_Term_Debt_PJ_5M or Long_Term_Debt_PJ_5M is null then 4 else
	case when Long_Term_Debt_PJ_5M >= Long_Term_Debt_PJ_6M or Long_Term_Debt_PJ_6M is null then 5 else
	case when Long_Term_Debt_PJ_6M >= Long_Term_Debt_PJ_7M or Long_Term_Debt_PJ_7M is null then 6 else
	case when Long_Term_Debt_PJ_7M >= Long_Term_Debt_PJ_8M or Long_Term_Debt_PJ_8M is null then 7 else
	case when Long_Term_Debt_PJ_8M >= Long_Term_Debt_PJ_9M or Long_Term_Debt_PJ_9M is null then 8 else
	case when Long_Term_Debt_PJ_9M >= Long_Term_Debt_PJ_10M or Long_Term_Debt_PJ_10M is null then 9 else
	case when Long_Term_Debt_PJ_10M >= Long_Term_Debt_PJ_11M or Long_Term_Debt_PJ_11M is null then 10 else
	case when Long_Term_Debt_PJ_11M >= Long_Term_Debt_PJ_12M or Long_Term_Debt_PJ_12M is null then 11 else
	case when Long_Term_Debt_PJ_12M >= Long_Term_Debt_PJ_13M or Long_Term_Debt_PJ_13M is null then 12 else
	case when Long_Term_Debt_PJ_13M >= Long_Term_Debt_PJ_14M or Long_Term_Debt_PJ_14M is null then 13 else
	case when Long_Term_Debt_PJ_14M >= Long_Term_Debt_PJ_15M or Long_Term_Debt_PJ_15M is null then 14 else
	case when Long_Term_Debt_PJ_15M >= Long_Term_Debt_PJ_16M or Long_Term_Debt_PJ_16M is null then 15 else
	case when Long_Term_Debt_PJ_16M >= Long_Term_Debt_PJ_17M or Long_Term_Debt_PJ_17M is null then 16 else
	case when Long_Term_Debt_PJ_17M >= Long_Term_Debt_PJ_18M or Long_Term_Debt_PJ_18M is null then 17 else
	case when Long_Term_Debt_PJ_18M >= Long_Term_Debt_PJ_19M or Long_Term_Debt_PJ_19M is null then 18 else
	case when Long_Term_Debt_PJ_19M >= Long_Term_Debt_PJ_20M or Long_Term_Debt_PJ_20M is null then 19 else
	case when Long_Term_Debt_PJ_20M >= Long_Term_Debt_PJ_21M or Long_Term_Debt_PJ_21M is null then 20 else
	case when Long_Term_Debt_PJ_21M >= Long_Term_Debt_PJ_22M or Long_Term_Debt_PJ_22M is null then 21 else
	case when Long_Term_Debt_PJ_22M >= Long_Term_Debt_PJ_23M or Long_Term_Debt_PJ_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_sth_pj,
	case when Long_Term_Debt_PJ_1M is null then null else
	case when Long_Term_Debt_PJ_Curr <= Long_Term_Debt_PJ_1M then 0 else
	case when Long_Term_Debt_PJ_1M <= Long_Term_Debt_PJ_2M or Long_Term_Debt_PJ_2M is null then 1 else
	case when Long_Term_Debt_PJ_2M <= Long_Term_Debt_PJ_3M or Long_Term_Debt_PJ_3M is null then 2 else
	case when Long_Term_Debt_PJ_3M <= Long_Term_Debt_PJ_4M or Long_Term_Debt_PJ_4M is null then 3 else
	case when Long_Term_Debt_PJ_4M <= Long_Term_Debt_PJ_5M or Long_Term_Debt_PJ_5M is null then 4 else
	case when Long_Term_Debt_PJ_5M <= Long_Term_Debt_PJ_6M or Long_Term_Debt_PJ_6M is null then 5 else
	case when Long_Term_Debt_PJ_6M <= Long_Term_Debt_PJ_7M or Long_Term_Debt_PJ_7M is null then 6 else
	case when Long_Term_Debt_PJ_7M <= Long_Term_Debt_PJ_8M or Long_Term_Debt_PJ_8M is null then 7 else
	case when Long_Term_Debt_PJ_8M <= Long_Term_Debt_PJ_9M or Long_Term_Debt_PJ_9M is null then 8 else
	case when Long_Term_Debt_PJ_9M <= Long_Term_Debt_PJ_10M or Long_Term_Debt_PJ_10M is null then 9 else
	case when Long_Term_Debt_PJ_10M <= Long_Term_Debt_PJ_11M or Long_Term_Debt_PJ_11M is null then 10 else
	case when Long_Term_Debt_PJ_11M <= Long_Term_Debt_PJ_12M or Long_Term_Debt_PJ_12M is null then 11 else
	case when Long_Term_Debt_PJ_12M <= Long_Term_Debt_PJ_13M or Long_Term_Debt_PJ_13M is null then 12 else
	case when Long_Term_Debt_PJ_13M <= Long_Term_Debt_PJ_14M or Long_Term_Debt_PJ_14M is null then 13 else
	case when Long_Term_Debt_PJ_14M <= Long_Term_Debt_PJ_15M or Long_Term_Debt_PJ_15M is null then 14 else
	case when Long_Term_Debt_PJ_15M <= Long_Term_Debt_PJ_16M or Long_Term_Debt_PJ_16M is null then 15 else
	case when Long_Term_Debt_PJ_16M <= Long_Term_Debt_PJ_17M or Long_Term_Debt_PJ_17M is null then 16 else
	case when Long_Term_Debt_PJ_17M <= Long_Term_Debt_PJ_18M or Long_Term_Debt_PJ_18M is null then 17 else
	case when Long_Term_Debt_PJ_18M <= Long_Term_Debt_PJ_19M or Long_Term_Debt_PJ_19M is null then 18 else
	case when Long_Term_Debt_PJ_19M <= Long_Term_Debt_PJ_20M or Long_Term_Debt_PJ_20M is null then 19 else
	case when Long_Term_Debt_PJ_20M <= Long_Term_Debt_PJ_21M or Long_Term_Debt_PJ_21M is null then 20 else
	case when Long_Term_Debt_PJ_21M <= Long_Term_Debt_PJ_22M or Long_Term_Debt_PJ_22M is null then 21 else
	case when Long_Term_Debt_PJ_22M <= Long_Term_Debt_PJ_23M or Long_Term_Debt_PJ_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_hth_pj,
	Lim_Cred_PJ_Curr::float,
	Lim_Cred_PJ_1M::float,
	Lim_Cred_PJ_2M::float,
	Lim_Cred_PJ_3M::float,
	Lim_Cred_PJ_4M::float,
	Lim_Cred_PJ_5M::float,
	Lim_Cred_PJ_6M::float,
	Lim_Cred_PJ_7M::float,
	Lim_Cred_PJ_8M::float,
	Lim_Cred_PJ_9M::float,
	Lim_Cred_PJ_10M::float,
	Lim_Cred_PJ_11M::float,
	Ever_Lim_Cred_PJ::float,
	Max_Lim_Cred_PJ::float,
	case when Lim_cred_PJ_22M > Lim_cred_PJ_23M then 1 else 0 end +
	case when Lim_cred_PJ_21M > Lim_cred_PJ_22M then 1 else 0 end +
	case when Lim_cred_PJ_20M > Lim_cred_PJ_21M then 1 else 0 end +
	case when Lim_cred_PJ_19M > Lim_cred_PJ_20M then 1 else 0 end +
	case when Lim_cred_PJ_18M > Lim_cred_PJ_19M then 1 else 0 end +
	case when Lim_cred_PJ_17M > Lim_cred_PJ_18M then 1 else 0 end +
	case when Lim_cred_PJ_16M > Lim_cred_PJ_17M then 1 else 0 end +
	case when Lim_cred_PJ_15M > Lim_cred_PJ_16M then 1 else 0 end +
	case when Lim_cred_PJ_14M > Lim_cred_PJ_15M then 1 else 0 end +
	case when Lim_cred_PJ_13M > Lim_cred_PJ_14M then 1 else 0 end +
	case when Lim_cred_PJ_12M > Lim_cred_PJ_13M then 1 else 0 end +
	case when Lim_cred_PJ_11M > Lim_cred_PJ_12M then 1 else 0 end +
	case when Lim_cred_PJ_10M > Lim_cred_PJ_11M then 1 else 0 end +
	case when Lim_cred_PJ_9M > Lim_cred_PJ_10M then 1 else 0 end +
	case when Lim_cred_PJ_8M > Lim_cred_PJ_9M then 1 else 0 end +
	case when Lim_cred_PJ_7M > Lim_cred_PJ_8M then 1 else 0 end +
	case when Lim_cred_PJ_6M > Lim_cred_PJ_7M then 1 else 0 end +
	case when Lim_cred_PJ_5M > Lim_cred_PJ_6M then 1 else 0 end +
	case when Lim_cred_PJ_4M > Lim_cred_PJ_5M then 1 else 0 end +
	case when Lim_cred_PJ_3M > Lim_cred_PJ_4M then 1 else 0 end +
	case when Lim_cred_PJ_2M > Lim_cred_PJ_3M then 1 else 0 end +
	case when Lim_cred_PJ_1M > Lim_cred_PJ_2M then 1 else 0 end +
	case when Lim_cred_PJ_Curr > Lim_cred_PJ_1M then 1 else 0 end::float as Meses_Aumento_Lim_Cred_PJ,
	case when Lim_cred_PJ_22M < Lim_cred_PJ_23M then 1 else 0 end +
	case when Lim_cred_PJ_21M < Lim_cred_PJ_22M then 1 else 0 end +
	case when Lim_cred_PJ_20M < Lim_cred_PJ_21M then 1 else 0 end +
	case when Lim_cred_PJ_19M < Lim_cred_PJ_20M then 1 else 0 end +
	case when Lim_cred_PJ_18M < Lim_cred_PJ_19M then 1 else 0 end +
	case when Lim_cred_PJ_17M < Lim_cred_PJ_18M then 1 else 0 end +
	case when Lim_cred_PJ_16M < Lim_cred_PJ_17M then 1 else 0 end +
	case when Lim_cred_PJ_15M < Lim_cred_PJ_16M then 1 else 0 end +
	case when Lim_cred_PJ_14M < Lim_cred_PJ_15M then 1 else 0 end +
	case when Lim_cred_PJ_13M < Lim_cred_PJ_14M then 1 else 0 end +
	case when Lim_cred_PJ_12M < Lim_cred_PJ_13M then 1 else 0 end +
	case when Lim_cred_PJ_11M < Lim_cred_PJ_12M then 1 else 0 end +
	case when Lim_cred_PJ_10M < Lim_cred_PJ_11M then 1 else 0 end +
	case when Lim_cred_PJ_9M < Lim_cred_PJ_10M then 1 else 0 end +
	case when Lim_cred_PJ_8M < Lim_cred_PJ_9M then 1 else 0 end +
	case when Lim_cred_PJ_7M < Lim_cred_PJ_8M then 1 else 0 end +
	case when Lim_cred_PJ_6M < Lim_cred_PJ_7M then 1 else 0 end +
	case when Lim_cred_PJ_5M < Lim_cred_PJ_6M then 1 else 0 end +
	case when Lim_cred_PJ_4M < Lim_cred_PJ_5M then 1 else 0 end +
	case when Lim_cred_PJ_3M < Lim_cred_PJ_4M then 1 else 0 end +
	case when Lim_cred_PJ_2M < Lim_cred_PJ_3M then 1 else 0 end +
	case when Lim_cred_PJ_1M < Lim_cred_PJ_2M then 1 else 0 end +
	case when Lim_cred_PJ_Curr < Lim_cred_PJ_1M then 1 else 0 end::float as Meses_Reducao_Lim_Cred_PJ,
	case when Lim_cred_PJ_1M is null then null else
	case when Lim_cred_PJ_Curr >= Lim_cred_PJ_1M or Lim_cred_PJ_1M is null then 0 else
	case when Lim_cred_PJ_1M >= Lim_cred_PJ_2M or Lim_cred_PJ_2M is null then 1 else
	case when Lim_cred_PJ_2M >= Lim_cred_PJ_3M or Lim_cred_PJ_3M is null then 2 else
	case when Lim_cred_PJ_3M >= Lim_cred_PJ_4M or Lim_cred_PJ_4M is null then 3 else
	case when Lim_cred_PJ_4M >= Lim_cred_PJ_5M or Lim_cred_PJ_5M is null then 4 else
	case when Lim_cred_PJ_5M >= Lim_cred_PJ_6M or Lim_cred_PJ_6M is null then 5 else
	case when Lim_cred_PJ_6M >= Lim_cred_PJ_7M or Lim_cred_PJ_7M is null then 6 else
	case when Lim_cred_PJ_7M >= Lim_cred_PJ_8M or Lim_cred_PJ_8M is null then 7 else
	case when Lim_cred_PJ_8M >= Lim_cred_PJ_9M or Lim_cred_PJ_9M is null then 8 else
	case when Lim_cred_PJ_9M >= Lim_cred_PJ_10M or Lim_cred_PJ_10M is null then 9 else
	case when Lim_cred_PJ_10M >= Lim_cred_PJ_11M or Lim_cred_PJ_11M is null then 10 else
	case when Lim_cred_PJ_11M >= Lim_cred_PJ_12M or Lim_cred_PJ_12M is null then 11 else
	case when Lim_cred_PJ_12M >= Lim_cred_PJ_13M or Lim_cred_PJ_13M is null then 12 else
	case when Lim_cred_PJ_13M >= Lim_cred_PJ_14M or Lim_cred_PJ_14M is null then 13 else
	case when Lim_cred_PJ_14M >= Lim_cred_PJ_15M or Lim_cred_PJ_15M is null then 14 else
	case when Lim_cred_PJ_15M >= Lim_cred_PJ_16M or Lim_cred_PJ_16M is null then 15 else
	case when Lim_cred_PJ_16M >= Lim_cred_PJ_17M or Lim_cred_PJ_17M is null then 16 else
	case when Lim_cred_PJ_17M >= Lim_cred_PJ_18M or Lim_cred_PJ_18M is null then 17 else
	case when Lim_cred_PJ_18M >= Lim_cred_PJ_19M or Lim_cred_PJ_19M is null then 18 else
	case when Lim_cred_PJ_19M >= Lim_cred_PJ_20M or Lim_cred_PJ_20M is null then 19 else
	case when Lim_cred_PJ_20M >= Lim_cred_PJ_21M or Lim_cred_PJ_21M is null then 20 else
	case when Lim_cred_PJ_21M >= Lim_cred_PJ_22M or Lim_cred_PJ_22M is null then 21 else
	case when Lim_cred_PJ_22M >= Lim_cred_PJ_23M or Lim_cred_PJ_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_sth_lim_cred_pj,
	case when Lim_cred_PJ_1M is null then null else
	case when Lim_cred_PJ_Curr <= Lim_cred_PJ_1M or Lim_cred_PJ_1M is null then 0 else
	case when Lim_cred_PJ_1M <= Lim_cred_PJ_2M or Lim_cred_PJ_2M is null then 1 else
	case when Lim_cred_PJ_2M <= Lim_cred_PJ_3M or Lim_cred_PJ_3M is null then 2 else
	case when Lim_cred_PJ_3M <= Lim_cred_PJ_4M or Lim_cred_PJ_4M is null then 3 else
	case when Lim_cred_PJ_4M <= Lim_cred_PJ_5M or Lim_cred_PJ_5M is null then 4 else
	case when Lim_cred_PJ_5M <= Lim_cred_PJ_6M or Lim_cred_PJ_6M is null then 5 else
	case when Lim_cred_PJ_6M <= Lim_cred_PJ_7M or Lim_cred_PJ_7M is null then 6 else
	case when Lim_cred_PJ_7M <= Lim_cred_PJ_8M or Lim_cred_PJ_8M is null then 7 else
	case when Lim_cred_PJ_8M <= Lim_cred_PJ_9M or Lim_cred_PJ_9M is null then 8 else
	case when Lim_cred_PJ_9M <= Lim_cred_PJ_10M or Lim_cred_PJ_10M is null then 9 else
	case when Lim_cred_PJ_10M <= Lim_cred_PJ_11M or Lim_cred_PJ_11M is null then 10 else
	case when Lim_cred_PJ_11M <= Lim_cred_PJ_12M or Lim_cred_PJ_12M is null then 11 else
	case when Lim_cred_PJ_12M <= Lim_cred_PJ_13M or Lim_cred_PJ_13M is null then 12 else
	case when Lim_cred_PJ_13M <= Lim_cred_PJ_14M or Lim_cred_PJ_14M is null then 13 else
	case when Lim_cred_PJ_14M <= Lim_cred_PJ_15M or Lim_cred_PJ_15M is null then 14 else
	case when Lim_cred_PJ_15M <= Lim_cred_PJ_16M or Lim_cred_PJ_16M is null then 15 else
	case when Lim_cred_PJ_16M <= Lim_cred_PJ_17M or Lim_cred_PJ_17M is null then 16 else
	case when Lim_cred_PJ_17M <= Lim_cred_PJ_18M or Lim_cred_PJ_18M is null then 17 else
	case when Lim_cred_PJ_18M <= Lim_cred_PJ_19M or Lim_cred_PJ_19M is null then 18 else
	case when Lim_cred_PJ_19M <= Lim_cred_PJ_20M or Lim_cred_PJ_20M is null then 19 else
	case when Lim_cred_PJ_20M <= Lim_cred_PJ_21M or Lim_cred_PJ_21M is null then 20 else
	case when Lim_cred_PJ_21M <= Lim_cred_PJ_22M or Lim_cred_PJ_22M is null then 21 else
	case when Lim_cred_PJ_22M <= Lim_cred_PJ_23M or Lim_cred_PJ_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_hth_lim_cred_pj
from(select direct_prospect_id,
	max(teve_scr_tirado) as teve_scr_tirado,
	max(Num_Ops_PJ) as Num_Ops_PJ,
	max(Num_FIs_PJ) as Num_FIs_PJ,
	max(First_Relation_FI_PJ) as First_Relation_FI_PJ,
	sum(DebtPJ) filter (where data = data_referencia) as Long_Term_Debt_PJ_Curr,
	sum(DebtPJ) filter (where data = data_referencia - interval '1 month') as Long_Term_Debt_PJ_1M,
	sum(DebtPJ) filter (where data = data_referencia - interval '2 months') as Long_Term_Debt_PJ_2M,
	sum(DebtPJ) filter (where data = data_referencia - interval '3 months') as Long_Term_Debt_PJ_3M,
	sum(DebtPJ) filter (where data = data_referencia - interval '4 months') as Long_Term_Debt_PJ_4M,
	sum(DebtPJ) filter (where data = data_referencia - interval '5 months') as Long_Term_Debt_PJ_5M,
	sum(DebtPJ) filter (where data = data_referencia - interval '6 months') as Long_Term_Debt_PJ_6M,
	sum(DebtPJ) filter (where data = data_referencia - interval '7 months') as Long_Term_Debt_PJ_7M,
	sum(DebtPJ) filter (where data = data_referencia - interval '8 months') as Long_Term_Debt_PJ_8M,
	sum(DebtPJ) filter (where data = data_referencia - interval '9 months') as Long_Term_Debt_PJ_9M,
	sum(DebtPJ) filter (where data = data_referencia - interval '10 months') as Long_Term_Debt_PJ_10M,
	sum(DebtPJ) filter (where data = data_referencia - interval '11 months') as Long_Term_Debt_PJ_11M,
	sum(DebtPJ) filter (where data = data_referencia - interval '12 months') as Long_Term_Debt_PJ_12M,
	sum(DebtPJ) filter (where data = data_referencia - interval '13 months') as Long_Term_Debt_PJ_13M,
	sum(DebtPJ) filter (where data = data_referencia - interval '14 months') as Long_Term_Debt_PJ_14M,
	sum(DebtPJ) filter (where data = data_referencia - interval '15 months') as Long_Term_Debt_PJ_15M,
	sum(DebtPJ) filter (where data = data_referencia - interval '16 months') as Long_Term_Debt_PJ_16M,
	sum(DebtPJ) filter (where data = data_referencia - interval '17 months') as Long_Term_Debt_PJ_17M,
	sum(DebtPJ) filter (where data = data_referencia - interval '18 months') as Long_Term_Debt_PJ_18M,
	sum(DebtPJ) filter (where data = data_referencia - interval '19 months') as Long_Term_Debt_PJ_19M,
	sum(DebtPJ) filter (where data = data_referencia - interval '20 months') as Long_Term_Debt_PJ_20M,
	sum(DebtPJ) filter (where data = data_referencia - interval '21 months') as Long_Term_Debt_PJ_21M,
	sum(DebtPJ) filter (where data = data_referencia - interval '22 months') as Long_Term_Debt_PJ_22M,
	sum(DebtPJ) filter (where data = data_referencia - interval '23 months') as Long_Term_Debt_PJ_23M,
	sum(DebtPJ) filter (where data <= data_referencia) as Ever_long_term_debt_PJ,
	max(DebtPJ) filter (where data <= data_referencia) as Max_Long_Term_Debt_PJ,
	sum(VencidoPJ) filter (where data = data_referencia) as Overdue_PJ_Curr,
	extract(year from age(min(data_referencia), max(data) filter (where data <= data_referencia and VencidoPJ > 0)))*12 + extract(month from age(min(data_referencia), max(data) filter (where data <= data_referencia and VencidoPJ > 0))) as Months_Since_Last_Overdue_PJ,
	count(VencidoPJ) filter (where data <= data_referencia and VencidoPJ > 0) as Months_Overdue_PJ,
	max(VencidoPJ) filter (where data <= data_referencia) as Max_Overdue_PJ,
	sum(PrejuizoPJ) filter (where data = data_referencia) as Default_PJ_Curr,
	extract(year from age(min(data_referencia), max(data) filter (where data <= data_referencia and PrejuizoPJ > 0)))*12 + extract(month from age(min(data_referencia), max(data) filter (where data <= data_referencia and PrejuizoPJ > 0)))  as Months_Since_Last_Default_PJ,
	count(PrejuizoPJ) filter (where data <= data_referencia and PrejuizoPJ > 0) as Months_Default_PJ,
	max(PrejuizoPJ) filter (where data <= data_referencia) as Max_Default_PJ,
	count(*) filter (where data <= data_referencia) as Qtd_meses_escopo_pj,
	sum(LimCredPJ) filter (where data = data_referencia) as Lim_Cred_PJ_Curr,
	sum(LimCredPJ) filter (where data = data_referencia - interval '1 month') as Lim_Cred_PJ_1M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '2 months') as Lim_Cred_PJ_2M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '3 months') as Lim_Cred_PJ_3M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '4 months') as Lim_Cred_PJ_4M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '5 months') as Lim_Cred_PJ_5M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '6 months') as Lim_Cred_PJ_6M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '7 months') as Lim_Cred_PJ_7M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '8 months') as Lim_Cred_PJ_8M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '9 months') as Lim_Cred_PJ_9M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '10 months') as Lim_Cred_PJ_10M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '11 months') as Lim_Cred_PJ_11M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '12 months') as Lim_Cred_PJ_12M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '13 months') as Lim_Cred_PJ_13M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '14 months') as Lim_Cred_PJ_14M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '15 months') as Lim_Cred_PJ_15M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '16 months') as Lim_Cred_PJ_16M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '17 months') as Lim_Cred_PJ_17M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '18 months') as Lim_Cred_PJ_18M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '19 months') as Lim_Cred_PJ_19M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '20 months') as Lim_Cred_PJ_20M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '21 months') as Lim_Cred_PJ_21M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '22 months') as Lim_Cred_PJ_22M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '23 months') as Lim_Cred_PJ_23M,
	sum(LimCredPJ) filter (where data <= data_referencia) as Ever_Lim_Cred_PJ,
	max(LimCredPJ) filter (where data <= data_referencia) as Max_Lim_Cred_PJ
	from(select direct_prospect_id,
			case when extract(day from ref_date) >= 16 then case when to_date(cc.data->'historico'->'Carteira de CrÃ©dito'->0->>'data','mm-yyyy') > to_date(to_char(ref_date - interval '1 month' ,'Mon/yy'),'Mon/yy') then to_date(to_char(ref_date - interval '1 month' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de CrÃ©dito'->0->>'data','mm-yyyy') end else case when to_date(cc.data->'historico'->'Carteira de CrÃ©dito'->0->>'data','mm-yyyy') > to_date(to_char(ref_date - interval '2 months' ,'Mon/yy'),'Mon/yy') then to_date(to_char(ref_date - interval '2 months' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de CrÃ©dito'->0->>'data','mm-yyyy') end end as data_referencia,
			to_date(jsonb_array_elements(cc.data->'historico'->'Carteira de CrÃ©dito')->>'data','mm/yyyy') as data,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Carteira de CrÃ©dito')->>'valor','-','0'),'.',''),',','.')::float DebtPJ,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Vencido')->>'valor','-','0'),'.',''),',','.')::float VencidoPJ,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'PrejuÃ­zo')->>'valor','-','0'),'.',''),',','.')::float PrejuizoPJ,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Limite de CrÃ©dito')->>'valor','-','0'),'.',''),',','.')::float LimCredPJ,
			case when max_id is null then null else replace(replace(case when position('Quantidade de OperaÃ§Ãµes' in cc.raw) > 0 then substring(cc.raw,position('Quantidade de OperaÃ§Ãµes' in cc.raw)+length(case when position('class="fu3" width="50%"' in raw) > 0 then 'Quantidade de OperaÃ§Ãµes</div></td><td class="fu3" width="50%"><b>' else 'Quantidade de OperaÃ§Ãµes</div>        </td>        <td width="50%" class="fu3">                    <b>' end),3) else null end,'<',''),'/','')::int end as Num_Ops_PJ,
			case when max_id is null then null else replace(replace(case when position('Quantidade de IFs em que o Cliente possui OperaÃ§Ãµes' in cc.raw) > 0 then substring(cc.raw,position('Quantidade de IFs em que o Cliente possui OperaÃ§Ãµes' in cc.raw)+ length(case when position('class="fu3" width="50%"' in raw) > 0 then 'Quantidade de IFs em que o Cliente possui OperaÃ§Ãµes</div></td><td class="fu3" width="50%"><b>' else 'Quantidade de IFs em que o Cliente possui OperaÃ§Ãµes</div>        </td>        <td width="50%" class="fu3">                    <b>' end),3) else null end,'<',''),'/','')::int end as Num_FIs_PJ,
			case when max_id is null then null else extract(year from age(ref_date,to_date(replace(replace(replace(case when position('Data de InÃ­cio de Relacionamento com a IF' in cc.raw) > 0 then substring(cc.raw,position('Data de InÃ­cio de Relacionamento com a IF' in cc.raw) + length(case when position('class="fu3" width="50%"' in raw) > 0 then 'Data de InÃ­cio de Relacionamento com a IF</div></td><td class="fu3" width="50%"><b>' else 'Data de InÃ­cio de Relacionamento com a IF</div>        </td>        <td width="50%" class="fu3">                         <b>' end),10) else null end,'<',''),'/',''),'-','0'),'ddmmyyyy')))::int end as First_Relation_FI_PJ,
			case when coalesce(max_id,max_id) is null then 0 else 1 end as teve_scr_tirado
		from credito_coleta cc
 		join(select 
 				dp.direct_prospect_id, 
 				max(coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date) + interval '1 day') as ref_date,
 				max(dp.cnpj) as cnpj,
				max(cc.id) filter (where coalesce(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '1 day') as max_id,
				min(cc.id) as min_id
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join temp_ids as aux on aux.direct_prospect_id = dp.direct_prospect_id
			left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
				from direct_prospects dp
				left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
				left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
				left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
				group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
			left join offers o on o.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
			left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on coalesce(max_id ,min_id) = lr.loan_request_id) lr on lr.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
			where documento_tipo = 'CNPJ' and tipo = 'SCR'
			group by 1) as t1 on cc.id = coalesce(t1.max_id,t1.min_id)) as t2
	group by 1) as t3) as SCRHistPJ on SCRHistPJ.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SCR HISTORICO PF
left join(select direct_prospect_id,
	teve_scr_pf_tirado,
	Num_Ops_pf::float,
	Num_FIs_pf::float,
	First_Relation_FI_pf::float,
	Long_Term_Debt_pf_Curr::float,
	Long_Term_Debt_pf_1M::float,
	Long_Term_Debt_pf_2M::float,
	Long_Term_Debt_pf_3M::float,
	Long_Term_Debt_pf_4M::float,
	Long_Term_Debt_pf_5M::float,
	Long_Term_Debt_pf_6M::float,
	Long_Term_Debt_pf_7M::float,
	Long_Term_Debt_pf_8M::float,
	Long_Term_Debt_pf_9M::float,
	Long_Term_Debt_pf_10M::float,
	Long_Term_Debt_pf_11M::float,
	Ever_long_term_debt_pf::float,
	Max_Long_Term_Debt_pf::float,
	Overdue_pf_Curr::float,
	Months_Since_Last_Overdue_pf::float,
	Months_Overdue_pf::float,
	Max_Overdue_pf::float,
	Default_pf_Curr::float,
	Months_Since_Last_Default_pf::float,
	Months_Default_pf::float,
	Max_Default_pf::float,
	Qtd_meses_escopo_pf::float,
	case when Long_Term_Debt_pf_22M > Long_Term_Debt_pf_23M then 1 else 0 end +
	case when Long_Term_Debt_pf_21M > Long_Term_Debt_pf_22M then 1 else 0 end +
	case when Long_Term_Debt_pf_20M > Long_Term_Debt_pf_21M then 1 else 0 end +
	case when Long_Term_Debt_pf_19M > Long_Term_Debt_pf_20M then 1 else 0 end +
	case when Long_Term_Debt_pf_18M > Long_Term_Debt_pf_19M then 1 else 0 end +
	case when Long_Term_Debt_pf_17M > Long_Term_Debt_pf_18M then 1 else 0 end +
	case when Long_Term_Debt_pf_16M > Long_Term_Debt_pf_17M then 1 else 0 end +
	case when Long_Term_Debt_pf_15M > Long_Term_Debt_pf_16M then 1 else 0 end +
	case when Long_Term_Debt_pf_14M > Long_Term_Debt_pf_15M then 1 else 0 end +
	case when Long_Term_Debt_pf_13M > Long_Term_Debt_pf_14M then 1 else 0 end +
	case when Long_Term_Debt_pf_12M > Long_Term_Debt_pf_13M then 1 else 0 end +
	case when Long_Term_Debt_pf_11M > Long_Term_Debt_pf_12M then 1 else 0 end +
	case when Long_Term_Debt_pf_10M > Long_Term_Debt_pf_11M then 1 else 0 end +
	case when Long_Term_Debt_pf_9M > Long_Term_Debt_pf_10M then 1 else 0 end +
	case when Long_Term_Debt_pf_8M > Long_Term_Debt_pf_9M then 1 else 0 end +
	case when Long_Term_Debt_pf_7M > Long_Term_Debt_pf_8M then 1 else 0 end +
	case when Long_Term_Debt_pf_6M > Long_Term_Debt_pf_7M then 1 else 0 end +
	case when Long_Term_Debt_pf_5M > Long_Term_Debt_pf_6M then 1 else 0 end +
	case when Long_Term_Debt_pf_4M > Long_Term_Debt_pf_5M then 1 else 0 end +
	case when Long_Term_Debt_pf_3M > Long_Term_Debt_pf_4M then 1 else 0 end +
	case when Long_Term_Debt_pf_2M > Long_Term_Debt_pf_3M then 1 else 0 end +
	case when Long_Term_Debt_pf_1M > Long_Term_Debt_pf_2M then 1 else 0 end +
	case when Long_Term_Debt_pf_Curr > Long_Term_Debt_pf_1M then 1 else 0 end::float as Meses_Aumento_Dividapf,
	case when Long_Term_Debt_pf_22M < Long_Term_Debt_pf_23M then 1 else 0 end +
	case when Long_Term_Debt_pf_21M < Long_Term_Debt_pf_22M then 1 else 0 end +
	case when Long_Term_Debt_pf_20M < Long_Term_Debt_pf_21M then 1 else 0 end +
	case when Long_Term_Debt_pf_19M < Long_Term_Debt_pf_20M then 1 else 0 end +
	case when Long_Term_Debt_pf_18M < Long_Term_Debt_pf_19M then 1 else 0 end +
	case when Long_Term_Debt_pf_17M < Long_Term_Debt_pf_18M then 1 else 0 end +
	case when Long_Term_Debt_pf_16M < Long_Term_Debt_pf_17M then 1 else 0 end +
	case when Long_Term_Debt_pf_15M < Long_Term_Debt_pf_16M then 1 else 0 end +
	case when Long_Term_Debt_pf_14M < Long_Term_Debt_pf_15M then 1 else 0 end +
	case when Long_Term_Debt_pf_13M < Long_Term_Debt_pf_14M then 1 else 0 end +
	case when Long_Term_Debt_pf_12M < Long_Term_Debt_pf_13M then 1 else 0 end +
	case when Long_Term_Debt_pf_11M < Long_Term_Debt_pf_12M then 1 else 0 end +
	case when Long_Term_Debt_pf_10M < Long_Term_Debt_pf_11M then 1 else 0 end +
	case when Long_Term_Debt_pf_9M < Long_Term_Debt_pf_10M then 1 else 0 end +
	case when Long_Term_Debt_pf_8M < Long_Term_Debt_pf_9M then 1 else 0 end +
	case when Long_Term_Debt_pf_7M < Long_Term_Debt_pf_8M then 1 else 0 end +
	case when Long_Term_Debt_pf_6M < Long_Term_Debt_pf_7M then 1 else 0 end +
	case when Long_Term_Debt_pf_5M < Long_Term_Debt_pf_6M then 1 else 0 end +
	case when Long_Term_Debt_pf_4M < Long_Term_Debt_pf_5M then 1 else 0 end +
	case when Long_Term_Debt_pf_3M < Long_Term_Debt_pf_4M then 1 else 0 end +
	case when Long_Term_Debt_pf_2M < Long_Term_Debt_pf_3M then 1 else 0 end +
	case when Long_Term_Debt_pf_1M < Long_Term_Debt_pf_2M then 1 else 0 end +
	case when Long_Term_Debt_pf_Curr < Long_Term_Debt_pf_1M then 1 else 0 end::float as Meses_Reducao_Dividapf,
	(case when Long_Term_Debt_pf_22M < Long_Term_Debt_pf_23M then Long_Term_Debt_pf_22M - Long_Term_Debt_pf_23M else 0 end +
	case when Long_Term_Debt_pf_21M < Long_Term_Debt_pf_22M then Long_Term_Debt_pf_21M - Long_Term_Debt_pf_22M else 0 end +
	case when Long_Term_Debt_pf_20M < Long_Term_Debt_pf_21M then Long_Term_Debt_pf_20M - Long_Term_Debt_pf_21M else 0 end +
	case when Long_Term_Debt_pf_19M < Long_Term_Debt_pf_20M then Long_Term_Debt_pf_19M - Long_Term_Debt_pf_20M else 0 end +
	case when Long_Term_Debt_pf_18M < Long_Term_Debt_pf_19M then Long_Term_Debt_pf_18M - Long_Term_Debt_pf_19M else 0 end +
	case when Long_Term_Debt_pf_17M < Long_Term_Debt_pf_18M then Long_Term_Debt_pf_17M - Long_Term_Debt_pf_18M else 0 end +
	case when Long_Term_Debt_pf_16M < Long_Term_Debt_pf_17M then Long_Term_Debt_pf_16M - Long_Term_Debt_pf_17M else 0 end +
	case when Long_Term_Debt_pf_15M < Long_Term_Debt_pf_16M then Long_Term_Debt_pf_15M - Long_Term_Debt_pf_16M else 0 end +
	case when Long_Term_Debt_pf_14M < Long_Term_Debt_pf_15M then Long_Term_Debt_pf_14M - Long_Term_Debt_pf_15M else 0 end +
	case when Long_Term_Debt_pf_13M < Long_Term_Debt_pf_14M then Long_Term_Debt_pf_13M - Long_Term_Debt_pf_14M else 0 end +
	case when Long_Term_Debt_pf_12M < Long_Term_Debt_pf_13M then Long_Term_Debt_pf_12M - Long_Term_Debt_pf_13M else 0 end +
	case when Long_Term_Debt_pf_11M < Long_Term_Debt_pf_12M then Long_Term_Debt_pf_11M - Long_Term_Debt_pf_12M else 0 end +
	case when Long_Term_Debt_pf_10M < Long_Term_Debt_pf_11M then Long_Term_Debt_pf_10M - Long_Term_Debt_pf_11M else 0 end +
	case when Long_Term_Debt_pf_9M < Long_Term_Debt_pf_10M then Long_Term_Debt_pf_9M - Long_Term_Debt_pf_10M else 0 end +
	case when Long_Term_Debt_pf_8M < Long_Term_Debt_pf_9M then Long_Term_Debt_pf_8M - Long_Term_Debt_pf_9M else 0 end +
	case when Long_Term_Debt_pf_7M < Long_Term_Debt_pf_8M then Long_Term_Debt_pf_7M - Long_Term_Debt_pf_8M else 0 end +
	case when Long_Term_Debt_pf_6M < Long_Term_Debt_pf_7M then Long_Term_Debt_pf_6M - Long_Term_Debt_pf_7M else 0 end +
	case when Long_Term_Debt_pf_5M < Long_Term_Debt_pf_6M then Long_Term_Debt_pf_5M - Long_Term_Debt_pf_6M else 0 end +
	case when Long_Term_Debt_pf_4M < Long_Term_Debt_pf_5M then Long_Term_Debt_pf_4M - Long_Term_Debt_pf_5M else 0 end +
	case when Long_Term_Debt_pf_3M < Long_Term_Debt_pf_4M then Long_Term_Debt_pf_3M - Long_Term_Debt_pf_4M else 0 end +
	case when Long_Term_Debt_pf_2M < Long_Term_Debt_pf_3M then Long_Term_Debt_pf_2M - Long_Term_Debt_pf_3M else 0 end +
	case when Long_Term_Debt_pf_1M < Long_Term_Debt_pf_2M then Long_Term_Debt_pf_1M - Long_Term_Debt_pf_2M else 0 end +
	case when Long_Term_Debt_pf_Curr < Long_Term_Debt_pf_1M then Long_Term_Debt_pf_Curr - Long_Term_Debt_pf_1M else 0 end)*(-1)::float as Saldo_Amort_Dividapf,
	case when Long_Term_Debt_pf_1M is null then null else
	case when Long_Term_Debt_pf_Curr >= Long_Term_Debt_pf_1M then 0 else
	case when Long_Term_Debt_pf_1M >= Long_Term_Debt_pf_2M or Long_Term_Debt_pf_2M is null then 1 else
	case when Long_Term_Debt_pf_2M >= Long_Term_Debt_pf_3M or Long_Term_Debt_pf_3M is null then 2 else
	case when Long_Term_Debt_pf_3M >= Long_Term_Debt_pf_4M or Long_Term_Debt_pf_4M is null then 3 else
	case when Long_Term_Debt_pf_4M >= Long_Term_Debt_pf_5M or Long_Term_Debt_pf_5M is null then 4 else
	case when Long_Term_Debt_pf_5M >= Long_Term_Debt_pf_6M or Long_Term_Debt_pf_6M is null then 5 else
	case when Long_Term_Debt_pf_6M >= Long_Term_Debt_pf_7M or Long_Term_Debt_pf_7M is null then 6 else
	case when Long_Term_Debt_pf_7M >= Long_Term_Debt_pf_8M or Long_Term_Debt_pf_8M is null then 7 else
	case when Long_Term_Debt_pf_8M >= Long_Term_Debt_pf_9M or Long_Term_Debt_pf_9M is null then 8 else
	case when Long_Term_Debt_pf_9M >= Long_Term_Debt_pf_10M or Long_Term_Debt_pf_10M is null then 9 else
	case when Long_Term_Debt_pf_10M >= Long_Term_Debt_pf_11M or Long_Term_Debt_pf_11M is null then 10 else
	case when Long_Term_Debt_pf_11M >= Long_Term_Debt_pf_12M or Long_Term_Debt_pf_12M is null then 11 else
	case when Long_Term_Debt_pf_12M >= Long_Term_Debt_pf_13M or Long_Term_Debt_pf_13M is null then 12 else
	case when Long_Term_Debt_pf_13M >= Long_Term_Debt_pf_14M or Long_Term_Debt_pf_14M is null then 13 else
	case when Long_Term_Debt_pf_14M >= Long_Term_Debt_pf_15M or Long_Term_Debt_pf_15M is null then 14 else
	case when Long_Term_Debt_pf_15M >= Long_Term_Debt_pf_16M or Long_Term_Debt_pf_16M is null then 15 else
	case when Long_Term_Debt_pf_16M >= Long_Term_Debt_pf_17M or Long_Term_Debt_pf_17M is null then 16 else
	case when Long_Term_Debt_pf_17M >= Long_Term_Debt_pf_18M or Long_Term_Debt_pf_18M is null then 17 else
	case when Long_Term_Debt_pf_18M >= Long_Term_Debt_pf_19M or Long_Term_Debt_pf_19M is null then 18 else
	case when Long_Term_Debt_pf_19M >= Long_Term_Debt_pf_20M or Long_Term_Debt_pf_20M is null then 19 else
	case when Long_Term_Debt_pf_20M >= Long_Term_Debt_pf_21M or Long_Term_Debt_pf_21M is null then 20 else
	case when Long_Term_Debt_pf_21M >= Long_Term_Debt_pf_22M or Long_Term_Debt_pf_22M is null then 21 else
	case when Long_Term_Debt_pf_22M >= Long_Term_Debt_pf_23M or Long_Term_Debt_pf_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_sth_pf,
	case when Long_Term_Debt_pf_1M is null then null else
	case when Long_Term_Debt_pf_Curr <= Long_Term_Debt_pf_1M then 0 else
	case when Long_Term_Debt_pf_1M <= Long_Term_Debt_pf_2M or Long_Term_Debt_pf_2M is null then 1 else
	case when Long_Term_Debt_pf_2M <= Long_Term_Debt_pf_3M or Long_Term_Debt_pf_3M is null then 2 else
	case when Long_Term_Debt_pf_3M <= Long_Term_Debt_pf_4M or Long_Term_Debt_pf_4M is null then 3 else
	case when Long_Term_Debt_pf_4M <= Long_Term_Debt_pf_5M or Long_Term_Debt_pf_5M is null then 4 else
	case when Long_Term_Debt_pf_5M <= Long_Term_Debt_pf_6M or Long_Term_Debt_pf_6M is null then 5 else
	case when Long_Term_Debt_pf_6M <= Long_Term_Debt_pf_7M or Long_Term_Debt_pf_7M is null then 6 else
	case when Long_Term_Debt_pf_7M <= Long_Term_Debt_pf_8M or Long_Term_Debt_pf_8M is null then 7 else
	case when Long_Term_Debt_pf_8M <= Long_Term_Debt_pf_9M or Long_Term_Debt_pf_9M is null then 8 else
	case when Long_Term_Debt_pf_9M <= Long_Term_Debt_pf_10M or Long_Term_Debt_pf_10M is null then 9 else
	case when Long_Term_Debt_pf_10M <= Long_Term_Debt_pf_11M or Long_Term_Debt_pf_11M is null then 10 else
	case when Long_Term_Debt_pf_11M <= Long_Term_Debt_pf_12M or Long_Term_Debt_pf_12M is null then 11 else
	case when Long_Term_Debt_pf_12M <= Long_Term_Debt_pf_13M or Long_Term_Debt_pf_13M is null then 12 else
	case when Long_Term_Debt_pf_13M <= Long_Term_Debt_pf_14M or Long_Term_Debt_pf_14M is null then 13 else
	case when Long_Term_Debt_pf_14M <= Long_Term_Debt_pf_15M or Long_Term_Debt_pf_15M is null then 14 else
	case when Long_Term_Debt_pf_15M <= Long_Term_Debt_pf_16M or Long_Term_Debt_pf_16M is null then 15 else
	case when Long_Term_Debt_pf_16M <= Long_Term_Debt_pf_17M or Long_Term_Debt_pf_17M is null then 16 else
	case when Long_Term_Debt_pf_17M <= Long_Term_Debt_pf_18M or Long_Term_Debt_pf_18M is null then 17 else
	case when Long_Term_Debt_pf_18M <= Long_Term_Debt_pf_19M or Long_Term_Debt_pf_19M is null then 18 else
	case when Long_Term_Debt_pf_19M <= Long_Term_Debt_pf_20M or Long_Term_Debt_pf_20M is null then 19 else
	case when Long_Term_Debt_pf_20M <= Long_Term_Debt_pf_21M or Long_Term_Debt_pf_21M is null then 20 else
	case when Long_Term_Debt_pf_21M <= Long_Term_Debt_pf_22M or Long_Term_Debt_pf_22M is null then 21 else
	case when Long_Term_Debt_pf_22M <= Long_Term_Debt_pf_23M or Long_Term_Debt_pf_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_hth_pf,
	Lim_Cred_pf_Curr::float,
	Lim_Cred_pf_1M::float,
	Lim_Cred_pf_2M::float,
	Lim_Cred_pf_3M::float,
	Lim_Cred_pf_4M::float,
	Lim_Cred_pf_5M::float,
	Lim_Cred_pf_6M::float,
	Lim_Cred_pf_7M::float,
	Lim_Cred_pf_8M::float,
	Lim_Cred_pf_9M::float,
	Lim_Cred_pf_10M::float,
	Lim_Cred_pf_11M::float,
	Ever_Lim_Cred_pf::float,
	Max_Lim_Cred_pf::float,
	case when Lim_cred_pf_22M > Lim_cred_pf_23M then 1 else 0 end +
	case when Lim_cred_pf_21M > Lim_cred_pf_22M then 1 else 0 end +
	case when Lim_cred_pf_20M > Lim_cred_pf_21M then 1 else 0 end +
	case when Lim_cred_pf_19M > Lim_cred_pf_20M then 1 else 0 end +
	case when Lim_cred_pf_18M > Lim_cred_pf_19M then 1 else 0 end +
	case when Lim_cred_pf_17M > Lim_cred_pf_18M then 1 else 0 end +
	case when Lim_cred_pf_16M > Lim_cred_pf_17M then 1 else 0 end +
	case when Lim_cred_pf_15M > Lim_cred_pf_16M then 1 else 0 end +
	case when Lim_cred_pf_14M > Lim_cred_pf_15M then 1 else 0 end +
	case when Lim_cred_pf_13M > Lim_cred_pf_14M then 1 else 0 end +
	case when Lim_cred_pf_12M > Lim_cred_pf_13M then 1 else 0 end +
	case when Lim_cred_pf_11M > Lim_cred_pf_12M then 1 else 0 end +
	case when Lim_cred_pf_10M > Lim_cred_pf_11M then 1 else 0 end +
	case when Lim_cred_pf_9M > Lim_cred_pf_10M then 1 else 0 end +
	case when Lim_cred_pf_8M > Lim_cred_pf_9M then 1 else 0 end +
	case when Lim_cred_pf_7M > Lim_cred_pf_8M then 1 else 0 end +
	case when Lim_cred_pf_6M > Lim_cred_pf_7M then 1 else 0 end +
	case when Lim_cred_pf_5M > Lim_cred_pf_6M then 1 else 0 end +
	case when Lim_cred_pf_4M > Lim_cred_pf_5M then 1 else 0 end +
	case when Lim_cred_pf_3M > Lim_cred_pf_4M then 1 else 0 end +
	case when Lim_cred_pf_2M > Lim_cred_pf_3M then 1 else 0 end +
	case when Lim_cred_pf_1M > Lim_cred_pf_2M then 1 else 0 end +
	case when Lim_cred_pf_Curr > Lim_cred_pf_1M then 1 else 0 end::float as Meses_Aumento_Lim_Cred_pf,
	case when Lim_cred_pf_22M < Lim_cred_pf_23M then 1 else 0 end +
	case when Lim_cred_pf_21M < Lim_cred_pf_22M then 1 else 0 end +
	case when Lim_cred_pf_20M < Lim_cred_pf_21M then 1 else 0 end +
	case when Lim_cred_pf_19M < Lim_cred_pf_20M then 1 else 0 end +
	case when Lim_cred_pf_18M < Lim_cred_pf_19M then 1 else 0 end +
	case when Lim_cred_pf_17M < Lim_cred_pf_18M then 1 else 0 end +
	case when Lim_cred_pf_16M < Lim_cred_pf_17M then 1 else 0 end +
	case when Lim_cred_pf_15M < Lim_cred_pf_16M then 1 else 0 end +
	case when Lim_cred_pf_14M < Lim_cred_pf_15M then 1 else 0 end +
	case when Lim_cred_pf_13M < Lim_cred_pf_14M then 1 else 0 end +
	case when Lim_cred_pf_12M < Lim_cred_pf_13M then 1 else 0 end +
	case when Lim_cred_pf_11M < Lim_cred_pf_12M then 1 else 0 end +
	case when Lim_cred_pf_10M < Lim_cred_pf_11M then 1 else 0 end +
	case when Lim_cred_pf_9M < Lim_cred_pf_10M then 1 else 0 end +
	case when Lim_cred_pf_8M < Lim_cred_pf_9M then 1 else 0 end +
	case when Lim_cred_pf_7M < Lim_cred_pf_8M then 1 else 0 end +
	case when Lim_cred_pf_6M < Lim_cred_pf_7M then 1 else 0 end +
	case when Lim_cred_pf_5M < Lim_cred_pf_6M then 1 else 0 end +
	case when Lim_cred_pf_4M < Lim_cred_pf_5M then 1 else 0 end +
	case when Lim_cred_pf_3M < Lim_cred_pf_4M then 1 else 0 end +
	case when Lim_cred_pf_2M < Lim_cred_pf_3M then 1 else 0 end +
	case when Lim_cred_pf_1M < Lim_cred_pf_2M then 1 else 0 end +
	case when Lim_cred_pf_Curr < Lim_cred_pf_1M then 1 else 0 end::float as Meses_Reducao_Lim_Cred_pf,
	case when Lim_cred_pf_1M is null then null else
	case when Lim_cred_pf_Curr >= Lim_cred_pf_1M or Lim_cred_pf_1M is null then 0 else
	case when Lim_cred_pf_1M >= Lim_cred_pf_2M or Lim_cred_pf_2M is null then 1 else
	case when Lim_cred_pf_2M >= Lim_cred_pf_3M or Lim_cred_pf_3M is null then 2 else
	case when Lim_cred_pf_3M >= Lim_cred_pf_4M or Lim_cred_pf_4M is null then 3 else
	case when Lim_cred_pf_4M >= Lim_cred_pf_5M or Lim_cred_pf_5M is null then 4 else
	case when Lim_cred_pf_5M >= Lim_cred_pf_6M or Lim_cred_pf_6M is null then 5 else
	case when Lim_cred_pf_6M >= Lim_cred_pf_7M or Lim_cred_pf_7M is null then 6 else
	case when Lim_cred_pf_7M >= Lim_cred_pf_8M or Lim_cred_pf_8M is null then 7 else
	case when Lim_cred_pf_8M >= Lim_cred_pf_9M or Lim_cred_pf_9M is null then 8 else
	case when Lim_cred_pf_9M >= Lim_cred_pf_10M or Lim_cred_pf_10M is null then 9 else
	case when Lim_cred_pf_10M >= Lim_cred_pf_11M or Lim_cred_pf_11M is null then 10 else
	case when Lim_cred_pf_11M >= Lim_cred_pf_12M or Lim_cred_pf_12M is null then 11 else
	case when Lim_cred_pf_12M >= Lim_cred_pf_13M or Lim_cred_pf_13M is null then 12 else
	case when Lim_cred_pf_13M >= Lim_cred_pf_14M or Lim_cred_pf_14M is null then 13 else
	case when Lim_cred_pf_14M >= Lim_cred_pf_15M or Lim_cred_pf_15M is null then 14 else
	case when Lim_cred_pf_15M >= Lim_cred_pf_16M or Lim_cred_pf_16M is null then 15 else
	case when Lim_cred_pf_16M >= Lim_cred_pf_17M or Lim_cred_pf_17M is null then 16 else
	case when Lim_cred_pf_17M >= Lim_cred_pf_18M or Lim_cred_pf_18M is null then 17 else
	case when Lim_cred_pf_18M >= Lim_cred_pf_19M or Lim_cred_pf_19M is null then 18 else
	case when Lim_cred_pf_19M >= Lim_cred_pf_20M or Lim_cred_pf_20M is null then 19 else
	case when Lim_cred_pf_20M >= Lim_cred_pf_21M or Lim_cred_pf_21M is null then 20 else
	case when Lim_cred_pf_21M >= Lim_cred_pf_22M or Lim_cred_pf_22M is null then 21 else
	case when Lim_cred_pf_22M >= Lim_cred_pf_23M or Lim_cred_pf_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_sth_lim_cred_pf,
	case when Lim_cred_pf_1M is null then null else
	case when Lim_cred_pf_Curr <= Lim_cred_pf_1M or Lim_cred_pf_1M is null then 0 else
	case when Lim_cred_pf_1M <= Lim_cred_pf_2M or Lim_cred_pf_2M is null then 1 else
	case when Lim_cred_pf_2M <= Lim_cred_pf_3M or Lim_cred_pf_3M is null then 2 else
	case when Lim_cred_pf_3M <= Lim_cred_pf_4M or Lim_cred_pf_4M is null then 3 else
	case when Lim_cred_pf_4M <= Lim_cred_pf_5M or Lim_cred_pf_5M is null then 4 else
	case when Lim_cred_pf_5M <= Lim_cred_pf_6M or Lim_cred_pf_6M is null then 5 else
	case when Lim_cred_pf_6M <= Lim_cred_pf_7M or Lim_cred_pf_7M is null then 6 else
	case when Lim_cred_pf_7M <= Lim_cred_pf_8M or Lim_cred_pf_8M is null then 7 else
	case when Lim_cred_pf_8M <= Lim_cred_pf_9M or Lim_cred_pf_9M is null then 8 else
	case when Lim_cred_pf_9M <= Lim_cred_pf_10M or Lim_cred_pf_10M is null then 9 else
	case when Lim_cred_pf_10M <= Lim_cred_pf_11M or Lim_cred_pf_11M is null then 10 else
	case when Lim_cred_pf_11M <= Lim_cred_pf_12M or Lim_cred_pf_12M is null then 11 else
	case when Lim_cred_pf_12M <= Lim_cred_pf_13M or Lim_cred_pf_13M is null then 12 else
	case when Lim_cred_pf_13M <= Lim_cred_pf_14M or Lim_cred_pf_14M is null then 13 else
	case when Lim_cred_pf_14M <= Lim_cred_pf_15M or Lim_cred_pf_15M is null then 14 else
	case when Lim_cred_pf_15M <= Lim_cred_pf_16M or Lim_cred_pf_16M is null then 15 else
	case when Lim_cred_pf_16M <= Lim_cred_pf_17M or Lim_cred_pf_17M is null then 16 else
	case when Lim_cred_pf_17M <= Lim_cred_pf_18M or Lim_cred_pf_18M is null then 17 else
	case when Lim_cred_pf_18M <= Lim_cred_pf_19M or Lim_cred_pf_19M is null then 18 else
	case when Lim_cred_pf_19M <= Lim_cred_pf_20M or Lim_cred_pf_20M is null then 19 else
	case when Lim_cred_pf_20M <= Lim_cred_pf_21M or Lim_cred_pf_21M is null then 20 else
	case when Lim_cred_pf_21M <= Lim_cred_pf_22M or Lim_cred_pf_22M is null then 21 else
	case when Lim_cred_pf_22M <= Lim_cred_pf_23M or Lim_cred_pf_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_hth_lim_cred_pf
from(select direct_prospect_id,
	max(teve_scr_pf_tirado) as teve_scr_pf_tirado,
	max(Num_Ops_pf) as Num_Ops_pf,
	max(Num_FIs_pf) as Num_FIs_pf,
	max(First_Relation_FI_pf) as First_Relation_FI_pf,
	sum(Debtpf) filter (where data = data_referencia) as Long_Term_Debt_pf_Curr,
	sum(Debtpf) filter (where data = data_referencia - interval '1 month') as Long_Term_Debt_pf_1M,
	sum(Debtpf) filter (where data = data_referencia - interval '2 months') as Long_Term_Debt_pf_2M,
	sum(Debtpf) filter (where data = data_referencia - interval '3 months') as Long_Term_Debt_pf_3M,
	sum(Debtpf) filter (where data = data_referencia - interval '4 months') as Long_Term_Debt_pf_4M,
	sum(Debtpf) filter (where data = data_referencia - interval '5 months') as Long_Term_Debt_pf_5M,
	sum(Debtpf) filter (where data = data_referencia - interval '6 months') as Long_Term_Debt_pf_6M,
	sum(Debtpf) filter (where data = data_referencia - interval '7 months') as Long_Term_Debt_pf_7M,
	sum(Debtpf) filter (where data = data_referencia - interval '8 months') as Long_Term_Debt_pf_8M,
	sum(Debtpf) filter (where data = data_referencia - interval '9 months') as Long_Term_Debt_pf_9M,
	sum(Debtpf) filter (where data = data_referencia - interval '10 months') as Long_Term_Debt_pf_10M,
	sum(Debtpf) filter (where data = data_referencia - interval '11 months') as Long_Term_Debt_pf_11M,
	sum(Debtpf) filter (where data = data_referencia - interval '12 months') as Long_Term_Debt_pf_12M,
	sum(Debtpf) filter (where data = data_referencia - interval '13 months') as Long_Term_Debt_pf_13M,
	sum(Debtpf) filter (where data = data_referencia - interval '14 months') as Long_Term_Debt_pf_14M,
	sum(Debtpf) filter (where data = data_referencia - interval '15 months') as Long_Term_Debt_pf_15M,
	sum(Debtpf) filter (where data = data_referencia - interval '16 months') as Long_Term_Debt_pf_16M,
	sum(Debtpf) filter (where data = data_referencia - interval '17 months') as Long_Term_Debt_pf_17M,
	sum(Debtpf) filter (where data = data_referencia - interval '18 months') as Long_Term_Debt_pf_18M,
	sum(Debtpf) filter (where data = data_referencia - interval '19 months') as Long_Term_Debt_pf_19M,
	sum(Debtpf) filter (where data = data_referencia - interval '20 months') as Long_Term_Debt_pf_20M,
	sum(Debtpf) filter (where data = data_referencia - interval '21 months') as Long_Term_Debt_pf_21M,
	sum(Debtpf) filter (where data = data_referencia - interval '22 months') as Long_Term_Debt_pf_22M,
	sum(Debtpf) filter (where data = data_referencia - interval '23 months') as Long_Term_Debt_pf_23M,
	sum(Debtpf) filter (where data <= data_referencia) as Ever_long_term_debt_pf,
	max(Debtpf) filter (where data <= data_referencia) as Max_Long_Term_Debt_pf,
	sum(Vencidopf) filter (where data = data_referencia) as Overdue_pf_Curr,
	extract(year from age(min(data_referencia), max(data) filter (where data <= data_referencia and Vencidopf > 0)))*12 + extract(month from age(min(data_referencia), max(data) filter (where data <= data_referencia and Vencidopf > 0))) as Months_Since_Last_Overdue_pf,
	count(Vencidopf) filter (where data <= data_referencia and Vencidopf > 0) as Months_Overdue_pf,
	max(Vencidopf) filter (where data <= data_referencia) as Max_Overdue_pf,
	sum(Prejuizopf) filter (where data = data_referencia) as Default_pf_Curr,
	extract(year from age(min(data_referencia), max(data) filter (where data <= data_referencia and Prejuizopf > 0)))*12 + extract(month from age(min(data_referencia), max(data) filter (where data <= data_referencia and Prejuizopf > 0)))  as Months_Since_Last_Default_pf,
	count(Prejuizopf) filter (where data <= data_referencia and Prejuizopf > 0) as Months_Default_pf,
	max(Prejuizopf) filter (where data <= data_referencia) as Max_Default_pf,
	count(*) filter (where data <= data_referencia) as Qtd_meses_escopo_pf,
	sum(LimCredpf) filter (where data = data_referencia) as Lim_Cred_pf_Curr,
	sum(LimCredpf) filter (where data = data_referencia - interval '1 month') as Lim_Cred_pf_1M,
	sum(LimCredpf) filter (where data = data_referencia - interval '2 months') as Lim_Cred_pf_2M,
	sum(LimCredpf) filter (where data = data_referencia - interval '3 months') as Lim_Cred_pf_3M,
	sum(LimCredpf) filter (where data = data_referencia - interval '4 months') as Lim_Cred_pf_4M,
	sum(LimCredpf) filter (where data = data_referencia - interval '5 months') as Lim_Cred_pf_5M,
	sum(LimCredpf) filter (where data = data_referencia - interval '6 months') as Lim_Cred_pf_6M,
	sum(LimCredpf) filter (where data = data_referencia - interval '7 months') as Lim_Cred_pf_7M,
	sum(LimCredpf) filter (where data = data_referencia - interval '8 months') as Lim_Cred_pf_8M,
	sum(LimCredpf) filter (where data = data_referencia - interval '9 months') as Lim_Cred_pf_9M,
	sum(LimCredpf) filter (where data = data_referencia - interval '10 months') as Lim_Cred_pf_10M,
	sum(LimCredpf) filter (where data = data_referencia - interval '11 months') as Lim_Cred_pf_11M,
	sum(LimCredpf) filter (where data = data_referencia - interval '12 months') as Lim_Cred_pf_12M,
	sum(LimCredpf) filter (where data = data_referencia - interval '13 months') as Lim_Cred_pf_13M,
	sum(LimCredpf) filter (where data = data_referencia - interval '14 months') as Lim_Cred_pf_14M,
	sum(LimCredpf) filter (where data = data_referencia - interval '15 months') as Lim_Cred_pf_15M,
	sum(LimCredpf) filter (where data = data_referencia - interval '16 months') as Lim_Cred_pf_16M,
	sum(LimCredpf) filter (where data = data_referencia - interval '17 months') as Lim_Cred_pf_17M,
	sum(LimCredpf) filter (where data = data_referencia - interval '18 months') as Lim_Cred_pf_18M,
	sum(LimCredpf) filter (where data = data_referencia - interval '19 months') as Lim_Cred_pf_19M,
	sum(LimCredpf) filter (where data = data_referencia - interval '20 months') as Lim_Cred_pf_20M,
	sum(LimCredpf) filter (where data = data_referencia - interval '21 months') as Lim_Cred_pf_21M,
	sum(LimCredpf) filter (where data = data_referencia - interval '22 months') as Lim_Cred_pf_22M,
	sum(LimCredpf) filter (where data = data_referencia - interval '23 months') as Lim_Cred_pf_23M,
	sum(LimCredpf) filter (where data <= data_referencia) as Ever_Lim_Cred_pf,
	max(LimCredpf) filter (where data <= data_referencia) as Max_Lim_Cred_pf
	from(select direct_prospect_id,
			case when extract(day from ref_date) >= 16 then case when to_date(cc.data->'historico'->'Carteira de CrÃ©dito'->0->>'data','mm-yyyy') > to_date(to_char(ref_date - interval '1 month' ,'Mon/yy'),'Mon/yy') then to_date(to_char(ref_date - interval '1 month' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de CrÃ©dito'->0->>'data','mm-yyyy') end else case when to_date(cc.data->'historico'->'Carteira de CrÃ©dito'->0->>'data','mm-yyyy') > to_date(to_char(ref_date - interval '2 months' ,'Mon/yy'),'Mon/yy') then to_date(to_char(ref_date - interval '2 months' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de CrÃ©dito'->0->>'data','mm-yyyy') end end as data_referencia,
			to_date(jsonb_array_elements(cc.data->'historico'->'Carteira de CrÃ©dito')->>'data','mm/yyyy') as data,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Carteira de CrÃ©dito')->>'valor','-','0'),'.',''),',','.')::float Debtpf,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Vencido')->>'valor','-','0'),'.',''),',','.')::float Vencidopf,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'PrejuÃ­zo')->>'valor','-','0'),'.',''),',','.')::float Prejuizopf,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Limite de CrÃ©dito')->>'valor','-','0'),'.',''),',','.')::float LimCredpf,
			case when max_id is null then null else replace(replace(case when position('Quantidade de OperaÃ§Ãµes' in cc.raw) > 0 then substring(cc.raw,position('Quantidade de OperaÃ§Ãµes' in cc.raw)+length(case when position('class="fu3" width="50%"' in raw) > 0 then 'Quantidade de OperaÃ§Ãµes</div></td><td class="fu3" width="50%"><b>' else 'Quantidade de OperaÃ§Ãµes</div>        </td>        <td width="50%" class="fu3">                    <b>' end),3) else null end,'<',''),'/','')::int end as Num_Ops_pf,
			case when max_id is null then null else replace(replace(case when position('Quantidade de IFs em que o Cliente possui OperaÃ§Ãµes' in cc.raw) > 0 then substring(cc.raw,position('Quantidade de IFs em que o Cliente possui OperaÃ§Ãµes' in cc.raw)+ length(case when position('class="fu3" width="50%"' in raw) > 0 then 'Quantidade de IFs em que o Cliente possui OperaÃ§Ãµes</div></td><td class="fu3" width="50%"><b>' else 'Quantidade de IFs em que o Cliente possui OperaÃ§Ãµes</div>        </td>        <td width="50%" class="fu3">                    <b>' end),3) else null end,'<',''),'/','')::int end as Num_FIs_pf,
			case when max_id is null then null else extract(year from age(ref_date,to_date(replace(replace(replace(case when position('Data de InÃ­cio de Relacionamento com a IF' in cc.raw) > 0 then substring(cc.raw,position('Data de InÃ­cio de Relacionamento com a IF' in cc.raw)+ length(case when position('class="fu3" width="50%"' in raw) > 0 then 'Data de InÃ­cio de Relacionamento com a IF</div></td><td class="fu3" width="50%"><b>' else 'Data de InÃ­cio de Relacionamento com a IF</div>        </td>        <td width="50%" class="fu3">                         <b>' end),10) else null end,'<',''),'/',''),'-','0'),'ddmmyyyy')))::int end as First_Relation_FI_pf,
			case when coalesce(max_id,min_id) is null then 0 else 1 end as teve_scr_pf_tirado
		from credito_coleta cc
		join(select dp.direct_prospect_id, 
				max(coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date) + interval '1 day') as ref_date,
				max(dp.cpf) as cpf,
				max(cc.id) filter (where coalesce(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '1 day') as max_id,
				min(cc.id) as min_id
			from credito_coleta cc
			join direct_prospects dp on cc.documento = dp.cpf
			join temp_ids as aux on aux.direct_prospect_id = dp.direct_prospect_id
			left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
				from direct_prospects dp
				left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
				left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
				left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
				group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
			left join offers o on o.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
			left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on coalesce(max_id ,min_id) = lr.loan_request_id) lr on lr.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
			where documento_tipo = 'CPF' and tipo = 'SCR'
			group by 1) as t1 on cc.id = coalesce(max_id,min_id)) as t2
	group by 1) as t3) as SCRHistPF on SCRHistPF.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SCR MODALIDADES PJ
left join(select direct_prospect_id, divida_atual_pj,qtd_meses_modalidade_pj,
	emprestimos_PJ_Curr::float,
	emprestimos_PJ_1M::float,
	emprestimos_PJ_2M::float,
	emprestimos_PJ_3M::float,
	emprestimos_PJ_4M::float,
	emprestimos_PJ_5M::float,
	emprestimos_PJ_6M::float,
	emprestimos_PJ_7M::float,
	emprestimos_PJ_8M::float,
	emprestimos_PJ_9M::float,
	emprestimos_PJ_10M::float,
	emprestimos_PJ_11M::float,
	greatest(emprestimos_PJ_Curr,emprestimos_PJ_1M,emprestimos_PJ_2M,emprestimos_PJ_3M,emprestimos_PJ_4M,emprestimos_PJ_5M,emprestimos_PJ_6M,emprestimos_PJ_7M,emprestimos_PJ_8M,emprestimos_PJ_9M,emprestimos_PJ_10M,emprestimos_PJ_11M)::float as max_emprestimos_pj,
	Ever_emprestimos_pj::float,
	count_emprestimos_pj::int,
	case when emprestimos_pj_10M < emprestimos_pj_11M then 1 else 0 end +
	case when emprestimos_pj_9M < emprestimos_pj_10M then 1 else 0 end +
	case when emprestimos_pj_8M < emprestimos_pj_9M then 1 else 0 end +
	case when emprestimos_pj_7M < emprestimos_pj_8M then 1 else 0 end +
	case when emprestimos_pj_6M < emprestimos_pj_7M then 1 else 0 end +
	case when emprestimos_pj_5M < emprestimos_pj_6M then 1 else 0 end +
	case when emprestimos_pj_4M < emprestimos_pj_5M then 1 else 0 end +
	case when emprestimos_pj_3M < emprestimos_pj_4M then 1 else 0 end +
	case when emprestimos_pj_2M < emprestimos_pj_3M then 1 else 0 end +
	case when emprestimos_pj_1M < emprestimos_pj_2M then 1 else 0 end +
	case when emprestimos_pj_Curr < emprestimos_pj_1M then 1 else 0 end::float as Meses_Reducao_emprestimos_pj,
	case when emprestimos_pj_10M > emprestimos_pj_11M then 1 else 0 end +
	case when emprestimos_pj_9M > emprestimos_pj_10M then 1 else 0 end +
	case when emprestimos_pj_8M > emprestimos_pj_9M then 1 else 0 end +
	case when emprestimos_pj_7M > emprestimos_pj_8M then 1 else 0 end +
	case when emprestimos_pj_6M > emprestimos_pj_7M then 1 else 0 end +
	case when emprestimos_pj_5M > emprestimos_pj_6M then 1 else 0 end +
	case when emprestimos_pj_4M > emprestimos_pj_5M then 1 else 0 end +
	case when emprestimos_pj_3M > emprestimos_pj_4M then 1 else 0 end +
	case when emprestimos_pj_2M > emprestimos_pj_3M then 1 else 0 end +
	case when emprestimos_pj_1M > emprestimos_pj_2M then 1 else 0 end +
	case when emprestimos_pj_Curr > emprestimos_pj_1M then 1 else 0 end::float as Meses_Aumento_emprestimos_pj,
	case when emprestimos_pj_1M is null then null else
	case when emprestimos_pj_Curr >= emprestimos_pj_1M or emprestimos_pj_1M is null then 0 else
	case when emprestimos_pj_1M >= emprestimos_pj_2M or emprestimos_pj_2M is null then 1 else
	case when emprestimos_pj_2M >= emprestimos_pj_3M or emprestimos_pj_3M is null then 2 else
	case when emprestimos_pj_3M >= emprestimos_pj_4M or emprestimos_pj_4M is null then 3 else
	case when emprestimos_pj_4M >= emprestimos_pj_5M or emprestimos_pj_5M is null then 4 else
	case when emprestimos_pj_5M >= emprestimos_pj_6M or emprestimos_pj_6M is null then 5 else
	case when emprestimos_pj_6M >= emprestimos_pj_7M or emprestimos_pj_7M is null then 6 else
	case when emprestimos_pj_7M >= emprestimos_pj_8M or emprestimos_pj_8M is null then 7 else
	case when emprestimos_pj_8M >= emprestimos_pj_9M or emprestimos_pj_9M is null then 8 else
	case when emprestimos_pj_9M >= emprestimos_pj_10M or emprestimos_pj_10M is null then 9 else
	case when emprestimos_pj_10M >= emprestimos_pj_11M or emprestimos_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_emprestimos_pj,
	case when emprestimos_pj_1M is null then null else
	case when emprestimos_pj_Curr <= emprestimos_pj_1M or emprestimos_pj_1M is null then 0 else
	case when emprestimos_pj_1M <= emprestimos_pj_2M or emprestimos_pj_2M is null then 1 else
	case when emprestimos_pj_2M <= emprestimos_pj_3M or emprestimos_pj_3M is null then 2 else
	case when emprestimos_pj_3M <= emprestimos_pj_4M or emprestimos_pj_4M is null then 3 else
	case when emprestimos_pj_4M <= emprestimos_pj_5M or emprestimos_pj_5M is null then 4 else
	case when emprestimos_pj_5M <= emprestimos_pj_6M or emprestimos_pj_6M is null then 5 else
	case when emprestimos_pj_6M <= emprestimos_pj_7M or emprestimos_pj_7M is null then 6 else
	case when emprestimos_pj_7M <= emprestimos_pj_8M or emprestimos_pj_8M is null then 7 else
	case when emprestimos_pj_8M <= emprestimos_pj_9M or emprestimos_pj_9M is null then 8 else
	case when emprestimos_pj_9M <= emprestimos_pj_10M or emprestimos_pj_10M is null then 9 else
	case when emprestimos_pj_10M <= emprestimos_pj_11M or emprestimos_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_emprestimos_pj,
	(case when emprestimos_PJ_10M < emprestimos_PJ_11M then emprestimos_PJ_10M - emprestimos_PJ_11M else 0 end +
	case when emprestimos_PJ_9M < emprestimos_PJ_10M then emprestimos_PJ_9M - emprestimos_PJ_10M else 0 end +
	case when emprestimos_PJ_8M < emprestimos_PJ_9M then emprestimos_PJ_8M - emprestimos_PJ_9M else 0 end +
	case when emprestimos_PJ_7M < emprestimos_PJ_8M then emprestimos_PJ_7M - emprestimos_PJ_8M else 0 end +
	case when emprestimos_PJ_6M < emprestimos_PJ_7M then emprestimos_PJ_6M - emprestimos_PJ_7M else 0 end +
	case when emprestimos_PJ_5M < emprestimos_PJ_6M then emprestimos_PJ_5M - emprestimos_PJ_6M else 0 end +
	case when emprestimos_PJ_4M < emprestimos_PJ_5M then emprestimos_PJ_4M - emprestimos_PJ_5M else 0 end +
	case when emprestimos_PJ_3M < emprestimos_PJ_4M then emprestimos_PJ_3M - emprestimos_PJ_4M else 0 end +
	case when emprestimos_PJ_2M < emprestimos_PJ_3M then emprestimos_PJ_2M - emprestimos_PJ_3M else 0 end +
	case when emprestimos_PJ_1M < emprestimos_PJ_2M then emprestimos_PJ_1M - emprestimos_PJ_2M else 0 end +
	case when emprestimos_PJ_Curr < emprestimos_PJ_1M then emprestimos_PJ_Curr - emprestimos_PJ_1M else 0 end)*(-1)::float as Saldo_Amort_emprestimos_PJ,
	emprestimos_conta_garantida_cheque_especial_PJ_Curr::float,
	emprestimos_conta_garantida_cheque_especial_PJ_1M::float,
	emprestimos_conta_garantida_cheque_especial_PJ_2M::float,
	emprestimos_conta_garantida_cheque_especial_PJ_3M::float,
	emprestimos_conta_garantida_cheque_especial_PJ_4M::float,
	emprestimos_conta_garantida_cheque_especial_PJ_5M::float,
	emprestimos_conta_garantida_cheque_especial_PJ_6M::float,
	emprestimos_conta_garantida_cheque_especial_PJ_7M::float,
	emprestimos_conta_garantida_cheque_especial_PJ_8M::float,
	emprestimos_conta_garantida_cheque_especial_PJ_9M::float,
	emprestimos_conta_garantida_cheque_especial_PJ_10M::float,
	emprestimos_conta_garantida_cheque_especial_PJ_11M::float,
	greatest(emprestimos_conta_garantida_cheque_especial_PJ_Curr,emprestimos_conta_garantida_cheque_especial_PJ_1M,emprestimos_conta_garantida_cheque_especial_PJ_2M,emprestimos_conta_garantida_cheque_especial_PJ_3M,emprestimos_conta_garantida_cheque_especial_PJ_4M,emprestimos_conta_garantida_cheque_especial_PJ_5M,emprestimos_conta_garantida_cheque_especial_PJ_6M,emprestimos_conta_garantida_cheque_especial_PJ_7M,emprestimos_conta_garantida_cheque_especial_PJ_8M,emprestimos_conta_garantida_cheque_especial_PJ_9M,emprestimos_conta_garantida_cheque_especial_PJ_10M,emprestimos_conta_garantida_cheque_especial_PJ_11M)::float as max_emprestimos_conta_garantida_cheque_especial_pj,
	Ever_emprestimos_conta_garantida_cheque_especial_pj::float,
	count_emprestimos_conta_garantida_cheque_especial_pj::int,
	case when emprestimos_conta_garantida_cheque_especial_pj_10M < emprestimos_conta_garantida_cheque_especial_pj_11M then 1 else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_pj_9M < emprestimos_conta_garantida_cheque_especial_pj_10M then 1 else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_pj_8M < emprestimos_conta_garantida_cheque_especial_pj_9M then 1 else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_pj_7M < emprestimos_conta_garantida_cheque_especial_pj_8M then 1 else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_pj_6M < emprestimos_conta_garantida_cheque_especial_pj_7M then 1 else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_pj_5M < emprestimos_conta_garantida_cheque_especial_pj_6M then 1 else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_pj_4M < emprestimos_conta_garantida_cheque_especial_pj_5M then 1 else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_pj_3M < emprestimos_conta_garantida_cheque_especial_pj_4M then 1 else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_pj_2M < emprestimos_conta_garantida_cheque_especial_pj_3M then 1 else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_pj_1M < emprestimos_conta_garantida_cheque_especial_pj_2M then 1 else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_pj_Curr < emprestimos_conta_garantida_cheque_especial_pj_1M then 1 else 0 end::float as Meses_Reducao_emprestimos_conta_garantida_cheque_especial_pj,
	case when emprestimos_conta_garantida_cheque_especial_pj_10M > emprestimos_conta_garantida_cheque_especial_pj_11M then 1 else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_pj_9M > emprestimos_conta_garantida_cheque_especial_pj_10M then 1 else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_pj_8M > emprestimos_conta_garantida_cheque_especial_pj_9M then 1 else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_pj_7M > emprestimos_conta_garantida_cheque_especial_pj_8M then 1 else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_pj_6M > emprestimos_conta_garantida_cheque_especial_pj_7M then 1 else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_pj_5M > emprestimos_conta_garantida_cheque_especial_pj_6M then 1 else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_pj_4M > emprestimos_conta_garantida_cheque_especial_pj_5M then 1 else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_pj_3M > emprestimos_conta_garantida_cheque_especial_pj_4M then 1 else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_pj_2M > emprestimos_conta_garantida_cheque_especial_pj_3M then 1 else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_pj_1M > emprestimos_conta_garantida_cheque_especial_pj_2M then 1 else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_pj_Curr > emprestimos_conta_garantida_cheque_especial_pj_1M then 1 else 0 end::float as Meses_Aumento_emprestimos_conta_garantida_cheque_especial_pj,
	case when emprestimos_conta_garantida_cheque_especial_pj_1M is null then null else
	case when emprestimos_conta_garantida_cheque_especial_pj_Curr >= emprestimos_conta_garantida_cheque_especial_pj_1M or emprestimos_conta_garantida_cheque_especial_pj_1M is null then 0 else
	case when emprestimos_conta_garantida_cheque_especial_pj_1M >= emprestimos_conta_garantida_cheque_especial_pj_2M or emprestimos_conta_garantida_cheque_especial_pj_2M is null then 1 else
	case when emprestimos_conta_garantida_cheque_especial_pj_2M >= emprestimos_conta_garantida_cheque_especial_pj_3M or emprestimos_conta_garantida_cheque_especial_pj_3M is null then 2 else
	case when emprestimos_conta_garantida_cheque_especial_pj_3M >= emprestimos_conta_garantida_cheque_especial_pj_4M or emprestimos_conta_garantida_cheque_especial_pj_4M is null then 3 else
	case when emprestimos_conta_garantida_cheque_especial_pj_4M >= emprestimos_conta_garantida_cheque_especial_pj_5M or emprestimos_conta_garantida_cheque_especial_pj_5M is null then 4 else
	case when emprestimos_conta_garantida_cheque_especial_pj_5M >= emprestimos_conta_garantida_cheque_especial_pj_6M or emprestimos_conta_garantida_cheque_especial_pj_6M is null then 5 else
	case when emprestimos_conta_garantida_cheque_especial_pj_6M >= emprestimos_conta_garantida_cheque_especial_pj_7M or emprestimos_conta_garantida_cheque_especial_pj_7M is null then 6 else
	case when emprestimos_conta_garantida_cheque_especial_pj_7M >= emprestimos_conta_garantida_cheque_especial_pj_8M or emprestimos_conta_garantida_cheque_especial_pj_8M is null then 7 else
	case when emprestimos_conta_garantida_cheque_especial_pj_8M >= emprestimos_conta_garantida_cheque_especial_pj_9M or emprestimos_conta_garantida_cheque_especial_pj_9M is null then 8 else
	case when emprestimos_conta_garantida_cheque_especial_pj_9M >= emprestimos_conta_garantida_cheque_especial_pj_10M or emprestimos_conta_garantida_cheque_especial_pj_10M is null then 9 else
	case when emprestimos_conta_garantida_cheque_especial_pj_10M >= emprestimos_conta_garantida_cheque_especial_pj_11M or emprestimos_conta_garantida_cheque_especial_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_emprestimos_conta_garantida_cheque_especial_pj,
	case when emprestimos_conta_garantida_cheque_especial_pj_1M is null then null else
	case when emprestimos_conta_garantida_cheque_especial_pj_Curr <= emprestimos_conta_garantida_cheque_especial_pj_1M or emprestimos_conta_garantida_cheque_especial_pj_1M is null then 0 else
	case when emprestimos_conta_garantida_cheque_especial_pj_1M <= emprestimos_conta_garantida_cheque_especial_pj_2M or emprestimos_conta_garantida_cheque_especial_pj_2M is null then 1 else
	case when emprestimos_conta_garantida_cheque_especial_pj_2M <= emprestimos_conta_garantida_cheque_especial_pj_3M or emprestimos_conta_garantida_cheque_especial_pj_3M is null then 2 else
	case when emprestimos_conta_garantida_cheque_especial_pj_3M <= emprestimos_conta_garantida_cheque_especial_pj_4M or emprestimos_conta_garantida_cheque_especial_pj_4M is null then 3 else
	case when emprestimos_conta_garantida_cheque_especial_pj_4M <= emprestimos_conta_garantida_cheque_especial_pj_5M or emprestimos_conta_garantida_cheque_especial_pj_5M is null then 4 else
	case when emprestimos_conta_garantida_cheque_especial_pj_5M <= emprestimos_conta_garantida_cheque_especial_pj_6M or emprestimos_conta_garantida_cheque_especial_pj_6M is null then 5 else
	case when emprestimos_conta_garantida_cheque_especial_pj_6M <= emprestimos_conta_garantida_cheque_especial_pj_7M or emprestimos_conta_garantida_cheque_especial_pj_7M is null then 6 else
	case when emprestimos_conta_garantida_cheque_especial_pj_7M <= emprestimos_conta_garantida_cheque_especial_pj_8M or emprestimos_conta_garantida_cheque_especial_pj_8M is null then 7 else
	case when emprestimos_conta_garantida_cheque_especial_pj_8M <= emprestimos_conta_garantida_cheque_especial_pj_9M or emprestimos_conta_garantida_cheque_especial_pj_9M is null then 8 else
	case when emprestimos_conta_garantida_cheque_especial_pj_9M <= emprestimos_conta_garantida_cheque_especial_pj_10M or emprestimos_conta_garantida_cheque_especial_pj_10M is null then 9 else
	case when emprestimos_conta_garantida_cheque_especial_pj_10M <= emprestimos_conta_garantida_cheque_especial_pj_11M or emprestimos_conta_garantida_cheque_especial_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_emprestimos_conta_garantida_cheque_especial_pj,
	(case when emprestimos_conta_garantida_cheque_especial_PJ_10M < emprestimos_conta_garantida_cheque_especial_PJ_11M then emprestimos_conta_garantida_cheque_especial_PJ_10M - emprestimos_conta_garantida_cheque_especial_PJ_11M else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_PJ_9M < emprestimos_conta_garantida_cheque_especial_PJ_10M then emprestimos_conta_garantida_cheque_especial_PJ_9M - emprestimos_conta_garantida_cheque_especial_PJ_10M else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_PJ_8M < emprestimos_conta_garantida_cheque_especial_PJ_9M then emprestimos_conta_garantida_cheque_especial_PJ_8M - emprestimos_conta_garantida_cheque_especial_PJ_9M else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_PJ_7M < emprestimos_conta_garantida_cheque_especial_PJ_8M then emprestimos_conta_garantida_cheque_especial_PJ_7M - emprestimos_conta_garantida_cheque_especial_PJ_8M else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_PJ_6M < emprestimos_conta_garantida_cheque_especial_PJ_7M then emprestimos_conta_garantida_cheque_especial_PJ_6M - emprestimos_conta_garantida_cheque_especial_PJ_7M else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_PJ_5M < emprestimos_conta_garantida_cheque_especial_PJ_6M then emprestimos_conta_garantida_cheque_especial_PJ_5M - emprestimos_conta_garantida_cheque_especial_PJ_6M else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_PJ_4M < emprestimos_conta_garantida_cheque_especial_PJ_5M then emprestimos_conta_garantida_cheque_especial_PJ_4M - emprestimos_conta_garantida_cheque_especial_PJ_5M else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_PJ_3M < emprestimos_conta_garantida_cheque_especial_PJ_4M then emprestimos_conta_garantida_cheque_especial_PJ_3M - emprestimos_conta_garantida_cheque_especial_PJ_4M else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_PJ_2M < emprestimos_conta_garantida_cheque_especial_PJ_3M then emprestimos_conta_garantida_cheque_especial_PJ_2M - emprestimos_conta_garantida_cheque_especial_PJ_3M else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_PJ_1M < emprestimos_conta_garantida_cheque_especial_PJ_2M then emprestimos_conta_garantida_cheque_especial_PJ_1M - emprestimos_conta_garantida_cheque_especial_PJ_2M else 0 end +
	case when emprestimos_conta_garantida_cheque_especial_PJ_Curr < emprestimos_conta_garantida_cheque_especial_PJ_1M then emprestimos_conta_garantida_cheque_especial_PJ_Curr - emprestimos_conta_garantida_cheque_especial_PJ_1M else 0 end)*(-1)::float as Saldo_Amort_emprestimos_conta_garantida_cheque_especial_PJ,
	emprestimos_cheque_especial_PJ_Curr::float,
	emprestimos_cheque_especial_PJ_1M::float,
	emprestimos_cheque_especial_PJ_2M::float,
	emprestimos_cheque_especial_PJ_3M::float,
	emprestimos_cheque_especial_PJ_4M::float,
	emprestimos_cheque_especial_PJ_5M::float,
	emprestimos_cheque_especial_PJ_6M::float,
	emprestimos_cheque_especial_PJ_7M::float,
	emprestimos_cheque_especial_PJ_8M::float,
	emprestimos_cheque_especial_PJ_9M::float,
	emprestimos_cheque_especial_PJ_10M::float,
	emprestimos_cheque_especial_PJ_11M::float,
	greatest(emprestimos_cheque_especial_PJ_Curr,emprestimos_cheque_especial_PJ_1M,emprestimos_cheque_especial_PJ_2M,emprestimos_cheque_especial_PJ_3M,emprestimos_cheque_especial_PJ_4M,emprestimos_cheque_especial_PJ_5M,emprestimos_cheque_especial_PJ_6M,emprestimos_cheque_especial_PJ_7M,emprestimos_cheque_especial_PJ_8M,emprestimos_cheque_especial_PJ_9M,emprestimos_cheque_especial_PJ_10M,emprestimos_cheque_especial_PJ_11M)::float as max_emprestimos_cheque_especial_pj,
	Ever_emprestimos_cheque_especial_pj::float,
	count_emprestimos_cheque_especial_pj::int,
	case when emprestimos_cheque_especial_pj_10M < emprestimos_cheque_especial_pj_11M then 1 else 0 end +
	case when emprestimos_cheque_especial_pj_9M < emprestimos_cheque_especial_pj_10M then 1 else 0 end +
	case when emprestimos_cheque_especial_pj_8M < emprestimos_cheque_especial_pj_9M then 1 else 0 end +
	case when emprestimos_cheque_especial_pj_7M < emprestimos_cheque_especial_pj_8M then 1 else 0 end +
	case when emprestimos_cheque_especial_pj_6M < emprestimos_cheque_especial_pj_7M then 1 else 0 end +
	case when emprestimos_cheque_especial_pj_5M < emprestimos_cheque_especial_pj_6M then 1 else 0 end +
	case when emprestimos_cheque_especial_pj_4M < emprestimos_cheque_especial_pj_5M then 1 else 0 end +
	case when emprestimos_cheque_especial_pj_3M < emprestimos_cheque_especial_pj_4M then 1 else 0 end +
	case when emprestimos_cheque_especial_pj_2M < emprestimos_cheque_especial_pj_3M then 1 else 0 end +
	case when emprestimos_cheque_especial_pj_1M < emprestimos_cheque_especial_pj_2M then 1 else 0 end +
	case when emprestimos_cheque_especial_pj_Curr < emprestimos_cheque_especial_pj_1M then 1 else 0 end::float as Meses_Reducao_emprestimos_cheque_especial_pj,
	case when emprestimos_cheque_especial_pj_10M > emprestimos_cheque_especial_pj_11M then 1 else 0 end +
	case when emprestimos_cheque_especial_pj_9M > emprestimos_cheque_especial_pj_10M then 1 else 0 end +
	case when emprestimos_cheque_especial_pj_8M > emprestimos_cheque_especial_pj_9M then 1 else 0 end +
	case when emprestimos_cheque_especial_pj_7M > emprestimos_cheque_especial_pj_8M then 1 else 0 end +
	case when emprestimos_cheque_especial_pj_6M > emprestimos_cheque_especial_pj_7M then 1 else 0 end +
	case when emprestimos_cheque_especial_pj_5M > emprestimos_cheque_especial_pj_6M then 1 else 0 end +
	case when emprestimos_cheque_especial_pj_4M > emprestimos_cheque_especial_pj_5M then 1 else 0 end +
	case when emprestimos_cheque_especial_pj_3M > emprestimos_cheque_especial_pj_4M then 1 else 0 end +
	case when emprestimos_cheque_especial_pj_2M > emprestimos_cheque_especial_pj_3M then 1 else 0 end +
	case when emprestimos_cheque_especial_pj_1M > emprestimos_cheque_especial_pj_2M then 1 else 0 end +
	case when emprestimos_cheque_especial_pj_Curr > emprestimos_cheque_especial_pj_1M then 1 else 0 end::float as Meses_Aumento_emprestimos_cheque_especial_pj,
	case when emprestimos_cheque_especial_pj_1M is null then null else
	case when emprestimos_cheque_especial_pj_Curr >= emprestimos_cheque_especial_pj_1M or emprestimos_cheque_especial_pj_1M is null then 0 else
	case when emprestimos_cheque_especial_pj_1M >= emprestimos_cheque_especial_pj_2M or emprestimos_cheque_especial_pj_2M is null then 1 else
	case when emprestimos_cheque_especial_pj_2M >= emprestimos_cheque_especial_pj_3M or emprestimos_cheque_especial_pj_3M is null then 2 else
	case when emprestimos_cheque_especial_pj_3M >= emprestimos_cheque_especial_pj_4M or emprestimos_cheque_especial_pj_4M is null then 3 else
	case when emprestimos_cheque_especial_pj_4M >= emprestimos_cheque_especial_pj_5M or emprestimos_cheque_especial_pj_5M is null then 4 else
	case when emprestimos_cheque_especial_pj_5M >= emprestimos_cheque_especial_pj_6M or emprestimos_cheque_especial_pj_6M is null then 5 else
	case when emprestimos_cheque_especial_pj_6M >= emprestimos_cheque_especial_pj_7M or emprestimos_cheque_especial_pj_7M is null then 6 else
	case when emprestimos_cheque_especial_pj_7M >= emprestimos_cheque_especial_pj_8M or emprestimos_cheque_especial_pj_8M is null then 7 else
	case when emprestimos_cheque_especial_pj_8M >= emprestimos_cheque_especial_pj_9M or emprestimos_cheque_especial_pj_9M is null then 8 else
	case when emprestimos_cheque_especial_pj_9M >= emprestimos_cheque_especial_pj_10M or emprestimos_cheque_especial_pj_10M is null then 9 else
	case when emprestimos_cheque_especial_pj_10M >= emprestimos_cheque_especial_pj_11M or emprestimos_cheque_especial_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_emprestimos_cheque_especial_pj,
	case when emprestimos_cheque_especial_pj_1M is null then null else
	case when emprestimos_cheque_especial_pj_Curr <= emprestimos_cheque_especial_pj_1M or emprestimos_cheque_especial_pj_1M is null then 0 else
	case when emprestimos_cheque_especial_pj_1M <= emprestimos_cheque_especial_pj_2M or emprestimos_cheque_especial_pj_2M is null then 1 else
	case when emprestimos_cheque_especial_pj_2M <= emprestimos_cheque_especial_pj_3M or emprestimos_cheque_especial_pj_3M is null then 2 else
	case when emprestimos_cheque_especial_pj_3M <= emprestimos_cheque_especial_pj_4M or emprestimos_cheque_especial_pj_4M is null then 3 else
	case when emprestimos_cheque_especial_pj_4M <= emprestimos_cheque_especial_pj_5M or emprestimos_cheque_especial_pj_5M is null then 4 else
	case when emprestimos_cheque_especial_pj_5M <= emprestimos_cheque_especial_pj_6M or emprestimos_cheque_especial_pj_6M is null then 5 else
	case when emprestimos_cheque_especial_pj_6M <= emprestimos_cheque_especial_pj_7M or emprestimos_cheque_especial_pj_7M is null then 6 else
	case when emprestimos_cheque_especial_pj_7M <= emprestimos_cheque_especial_pj_8M or emprestimos_cheque_especial_pj_8M is null then 7 else
	case when emprestimos_cheque_especial_pj_8M <= emprestimos_cheque_especial_pj_9M or emprestimos_cheque_especial_pj_9M is null then 8 else
	case when emprestimos_cheque_especial_pj_9M <= emprestimos_cheque_especial_pj_10M or emprestimos_cheque_especial_pj_10M is null then 9 else
	case when emprestimos_cheque_especial_pj_10M <= emprestimos_cheque_especial_pj_11M or emprestimos_cheque_especial_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_emprestimos_cheque_especial_pj,
	(case when emprestimos_cheque_especial_PJ_10M < emprestimos_cheque_especial_PJ_11M then emprestimos_cheque_especial_PJ_10M - emprestimos_cheque_especial_PJ_11M else 0 end +
	case when emprestimos_cheque_especial_PJ_9M < emprestimos_cheque_especial_PJ_10M then emprestimos_cheque_especial_PJ_9M - emprestimos_cheque_especial_PJ_10M else 0 end +
	case when emprestimos_cheque_especial_PJ_8M < emprestimos_cheque_especial_PJ_9M then emprestimos_cheque_especial_PJ_8M - emprestimos_cheque_especial_PJ_9M else 0 end +
	case when emprestimos_cheque_especial_PJ_7M < emprestimos_cheque_especial_PJ_8M then emprestimos_cheque_especial_PJ_7M - emprestimos_cheque_especial_PJ_8M else 0 end +
	case when emprestimos_cheque_especial_PJ_6M < emprestimos_cheque_especial_PJ_7M then emprestimos_cheque_especial_PJ_6M - emprestimos_cheque_especial_PJ_7M else 0 end +
	case when emprestimos_cheque_especial_PJ_5M < emprestimos_cheque_especial_PJ_6M then emprestimos_cheque_especial_PJ_5M - emprestimos_cheque_especial_PJ_6M else 0 end +
	case when emprestimos_cheque_especial_PJ_4M < emprestimos_cheque_especial_PJ_5M then emprestimos_cheque_especial_PJ_4M - emprestimos_cheque_especial_PJ_5M else 0 end +
	case when emprestimos_cheque_especial_PJ_3M < emprestimos_cheque_especial_PJ_4M then emprestimos_cheque_especial_PJ_3M - emprestimos_cheque_especial_PJ_4M else 0 end +
	case when emprestimos_cheque_especial_PJ_2M < emprestimos_cheque_especial_PJ_3M then emprestimos_cheque_especial_PJ_2M - emprestimos_cheque_especial_PJ_3M else 0 end +
	case when emprestimos_cheque_especial_PJ_1M < emprestimos_cheque_especial_PJ_2M then emprestimos_cheque_especial_PJ_1M - emprestimos_cheque_especial_PJ_2M else 0 end +
	case when emprestimos_cheque_especial_PJ_Curr < emprestimos_cheque_especial_PJ_1M then emprestimos_cheque_especial_PJ_Curr - emprestimos_cheque_especial_PJ_1M else 0 end)*(-1)::float as Saldo_Amort_emprestimos_cheque_especial_PJ,
	emprestimos_giro_longo_PJ_Curr::float,
	emprestimos_giro_longo_PJ_1M::float,
	emprestimos_giro_longo_PJ_2M::float,
	emprestimos_giro_longo_PJ_3M::float,
	emprestimos_giro_longo_PJ_4M::float,
	emprestimos_giro_longo_PJ_5M::float,
	emprestimos_giro_longo_PJ_6M::float,
	emprestimos_giro_longo_PJ_7M::float,
	emprestimos_giro_longo_PJ_8M::float,
	emprestimos_giro_longo_PJ_9M::float,
	emprestimos_giro_longo_PJ_10M::float,
	emprestimos_giro_longo_PJ_11M::float,
	greatest(emprestimos_giro_longo_PJ_Curr,emprestimos_giro_longo_PJ_1M,emprestimos_giro_longo_PJ_2M,emprestimos_giro_longo_PJ_3M,emprestimos_giro_longo_PJ_4M,emprestimos_giro_longo_PJ_5M,emprestimos_giro_longo_PJ_6M,emprestimos_giro_longo_PJ_7M,emprestimos_giro_longo_PJ_8M,emprestimos_giro_longo_PJ_9M,emprestimos_giro_longo_PJ_10M,emprestimos_giro_longo_PJ_11M)::float as max_emprestimos_giro_longo_pj,
	Ever_emprestimos_giro_longo_pj::float,
	count_emprestimos_giro_longo_pj::int,
	case when emprestimos_giro_longo_pj_10M < emprestimos_giro_longo_pj_11M then 1 else 0 end +
	case when emprestimos_giro_longo_pj_9M < emprestimos_giro_longo_pj_10M then 1 else 0 end +
	case when emprestimos_giro_longo_pj_8M < emprestimos_giro_longo_pj_9M then 1 else 0 end +
	case when emprestimos_giro_longo_pj_7M < emprestimos_giro_longo_pj_8M then 1 else 0 end +
	case when emprestimos_giro_longo_pj_6M < emprestimos_giro_longo_pj_7M then 1 else 0 end +
	case when emprestimos_giro_longo_pj_5M < emprestimos_giro_longo_pj_6M then 1 else 0 end +
	case when emprestimos_giro_longo_pj_4M < emprestimos_giro_longo_pj_5M then 1 else 0 end +
	case when emprestimos_giro_longo_pj_3M < emprestimos_giro_longo_pj_4M then 1 else 0 end +
	case when emprestimos_giro_longo_pj_2M < emprestimos_giro_longo_pj_3M then 1 else 0 end +
	case when emprestimos_giro_longo_pj_1M < emprestimos_giro_longo_pj_2M then 1 else 0 end +
	case when emprestimos_giro_longo_pj_Curr < emprestimos_giro_longo_pj_1M then 1 else 0 end::float as Meses_Reducao_emprestimos_giro_longo_pj,
	case when emprestimos_giro_longo_pj_10M > emprestimos_giro_longo_pj_11M then 1 else 0 end +
	case when emprestimos_giro_longo_pj_9M > emprestimos_giro_longo_pj_10M then 1 else 0 end +
	case when emprestimos_giro_longo_pj_8M > emprestimos_giro_longo_pj_9M then 1 else 0 end +
	case when emprestimos_giro_longo_pj_7M > emprestimos_giro_longo_pj_8M then 1 else 0 end +
	case when emprestimos_giro_longo_pj_6M > emprestimos_giro_longo_pj_7M then 1 else 0 end +
	case when emprestimos_giro_longo_pj_5M > emprestimos_giro_longo_pj_6M then 1 else 0 end +
	case when emprestimos_giro_longo_pj_4M > emprestimos_giro_longo_pj_5M then 1 else 0 end +
	case when emprestimos_giro_longo_pj_3M > emprestimos_giro_longo_pj_4M then 1 else 0 end +
	case when emprestimos_giro_longo_pj_2M > emprestimos_giro_longo_pj_3M then 1 else 0 end +
	case when emprestimos_giro_longo_pj_1M > emprestimos_giro_longo_pj_2M then 1 else 0 end +
	case when emprestimos_giro_longo_pj_Curr > emprestimos_giro_longo_pj_1M then 1 else 0 end::float as Meses_Aumento_emprestimos_giro_longo_pj,
	case when emprestimos_giro_longo_pj_1M is null then null else
	case when emprestimos_giro_longo_pj_Curr >= emprestimos_giro_longo_pj_1M or emprestimos_giro_longo_pj_1M is null then 0 else
	case when emprestimos_giro_longo_pj_1M >= emprestimos_giro_longo_pj_2M or emprestimos_giro_longo_pj_2M is null then 1 else
	case when emprestimos_giro_longo_pj_2M >= emprestimos_giro_longo_pj_3M or emprestimos_giro_longo_pj_3M is null then 2 else
	case when emprestimos_giro_longo_pj_3M >= emprestimos_giro_longo_pj_4M or emprestimos_giro_longo_pj_4M is null then 3 else
	case when emprestimos_giro_longo_pj_4M >= emprestimos_giro_longo_pj_5M or emprestimos_giro_longo_pj_5M is null then 4 else
	case when emprestimos_giro_longo_pj_5M >= emprestimos_giro_longo_pj_6M or emprestimos_giro_longo_pj_6M is null then 5 else
	case when emprestimos_giro_longo_pj_6M >= emprestimos_giro_longo_pj_7M or emprestimos_giro_longo_pj_7M is null then 6 else
	case when emprestimos_giro_longo_pj_7M >= emprestimos_giro_longo_pj_8M or emprestimos_giro_longo_pj_8M is null then 7 else
	case when emprestimos_giro_longo_pj_8M >= emprestimos_giro_longo_pj_9M or emprestimos_giro_longo_pj_9M is null then 8 else
	case when emprestimos_giro_longo_pj_9M >= emprestimos_giro_longo_pj_10M or emprestimos_giro_longo_pj_10M is null then 9 else
	case when emprestimos_giro_longo_pj_10M >= emprestimos_giro_longo_pj_11M or emprestimos_giro_longo_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_emprestimos_giro_longo_pj,
	case when emprestimos_giro_longo_pj_1M is null then null else
	case when emprestimos_giro_longo_pj_Curr <= emprestimos_giro_longo_pj_1M or emprestimos_giro_longo_pj_1M is null then 0 else
	case when emprestimos_giro_longo_pj_1M <= emprestimos_giro_longo_pj_2M or emprestimos_giro_longo_pj_2M is null then 1 else
	case when emprestimos_giro_longo_pj_2M <= emprestimos_giro_longo_pj_3M or emprestimos_giro_longo_pj_3M is null then 2 else
	case when emprestimos_giro_longo_pj_3M <= emprestimos_giro_longo_pj_4M or emprestimos_giro_longo_pj_4M is null then 3 else
	case when emprestimos_giro_longo_pj_4M <= emprestimos_giro_longo_pj_5M or emprestimos_giro_longo_pj_5M is null then 4 else
	case when emprestimos_giro_longo_pj_5M <= emprestimos_giro_longo_pj_6M or emprestimos_giro_longo_pj_6M is null then 5 else
	case when emprestimos_giro_longo_pj_6M <= emprestimos_giro_longo_pj_7M or emprestimos_giro_longo_pj_7M is null then 6 else
	case when emprestimos_giro_longo_pj_7M <= emprestimos_giro_longo_pj_8M or emprestimos_giro_longo_pj_8M is null then 7 else
	case when emprestimos_giro_longo_pj_8M <= emprestimos_giro_longo_pj_9M or emprestimos_giro_longo_pj_9M is null then 8 else
	case when emprestimos_giro_longo_pj_9M <= emprestimos_giro_longo_pj_10M or emprestimos_giro_longo_pj_10M is null then 9 else
	case when emprestimos_giro_longo_pj_10M <= emprestimos_giro_longo_pj_11M or emprestimos_giro_longo_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_emprestimos_giro_longo_pj,
	(case when emprestimos_giro_longo_PJ_10M < emprestimos_giro_longo_PJ_11M then emprestimos_giro_longo_PJ_10M - emprestimos_giro_longo_PJ_11M else 0 end +
	case when emprestimos_giro_longo_PJ_9M < emprestimos_giro_longo_PJ_10M then emprestimos_giro_longo_PJ_9M - emprestimos_giro_longo_PJ_10M else 0 end +
	case when emprestimos_giro_longo_PJ_8M < emprestimos_giro_longo_PJ_9M then emprestimos_giro_longo_PJ_8M - emprestimos_giro_longo_PJ_9M else 0 end +
	case when emprestimos_giro_longo_PJ_7M < emprestimos_giro_longo_PJ_8M then emprestimos_giro_longo_PJ_7M - emprestimos_giro_longo_PJ_8M else 0 end +
	case when emprestimos_giro_longo_PJ_6M < emprestimos_giro_longo_PJ_7M then emprestimos_giro_longo_PJ_6M - emprestimos_giro_longo_PJ_7M else 0 end +
	case when emprestimos_giro_longo_PJ_5M < emprestimos_giro_longo_PJ_6M then emprestimos_giro_longo_PJ_5M - emprestimos_giro_longo_PJ_6M else 0 end +
	case when emprestimos_giro_longo_PJ_4M < emprestimos_giro_longo_PJ_5M then emprestimos_giro_longo_PJ_4M - emprestimos_giro_longo_PJ_5M else 0 end +
	case when emprestimos_giro_longo_PJ_3M < emprestimos_giro_longo_PJ_4M then emprestimos_giro_longo_PJ_3M - emprestimos_giro_longo_PJ_4M else 0 end +
	case when emprestimos_giro_longo_PJ_2M < emprestimos_giro_longo_PJ_3M then emprestimos_giro_longo_PJ_2M - emprestimos_giro_longo_PJ_3M else 0 end +
	case when emprestimos_giro_longo_PJ_1M < emprestimos_giro_longo_PJ_2M then emprestimos_giro_longo_PJ_1M - emprestimos_giro_longo_PJ_2M else 0 end +
	case when emprestimos_giro_longo_PJ_Curr < emprestimos_giro_longo_PJ_1M then emprestimos_giro_longo_PJ_Curr - emprestimos_giro_longo_PJ_1M else 0 end)*(-1)::float as Saldo_Amort_emprestimos_giro_longo_PJ,
	emprestimos_giro_curto_PJ_Curr::float,
	emprestimos_giro_curto_PJ_1M::float,
	emprestimos_giro_curto_PJ_2M::float,
	emprestimos_giro_curto_PJ_3M::float,
	emprestimos_giro_curto_PJ_4M::float,
	emprestimos_giro_curto_PJ_5M::float,
	emprestimos_giro_curto_PJ_6M::float,
	emprestimos_giro_curto_PJ_7M::float,
	emprestimos_giro_curto_PJ_8M::float,
	emprestimos_giro_curto_PJ_9M::float,
	emprestimos_giro_curto_PJ_10M::float,
	emprestimos_giro_curto_PJ_11M::float,
	greatest(emprestimos_giro_curto_PJ_Curr,emprestimos_giro_curto_PJ_1M,emprestimos_giro_curto_PJ_2M,emprestimos_giro_curto_PJ_3M,emprestimos_giro_curto_PJ_4M,emprestimos_giro_curto_PJ_5M,emprestimos_giro_curto_PJ_6M,emprestimos_giro_curto_PJ_7M,emprestimos_giro_curto_PJ_8M,emprestimos_giro_curto_PJ_9M,emprestimos_giro_curto_PJ_10M,emprestimos_giro_curto_PJ_11M)::float as max_emprestimos_giro_curto_pj,
	Ever_emprestimos_giro_curto_pj::float,
	count_emprestimos_giro_curto_pj::int,
	case when emprestimos_giro_curto_pj_10M < emprestimos_giro_curto_pj_11M then 1 else 0 end +
	case when emprestimos_giro_curto_pj_9M < emprestimos_giro_curto_pj_10M then 1 else 0 end +
	case when emprestimos_giro_curto_pj_8M < emprestimos_giro_curto_pj_9M then 1 else 0 end +
	case when emprestimos_giro_curto_pj_7M < emprestimos_giro_curto_pj_8M then 1 else 0 end +
	case when emprestimos_giro_curto_pj_6M < emprestimos_giro_curto_pj_7M then 1 else 0 end +
	case when emprestimos_giro_curto_pj_5M < emprestimos_giro_curto_pj_6M then 1 else 0 end +
	case when emprestimos_giro_curto_pj_4M < emprestimos_giro_curto_pj_5M then 1 else 0 end +
	case when emprestimos_giro_curto_pj_3M < emprestimos_giro_curto_pj_4M then 1 else 0 end +
	case when emprestimos_giro_curto_pj_2M < emprestimos_giro_curto_pj_3M then 1 else 0 end +
	case when emprestimos_giro_curto_pj_1M < emprestimos_giro_curto_pj_2M then 1 else 0 end +
	case when emprestimos_giro_curto_pj_Curr < emprestimos_giro_curto_pj_1M then 1 else 0 end::float as Meses_Reducao_emprestimos_giro_curto_pj,
	case when emprestimos_giro_curto_pj_10M > emprestimos_giro_curto_pj_11M then 1 else 0 end +
	case when emprestimos_giro_curto_pj_9M > emprestimos_giro_curto_pj_10M then 1 else 0 end +
	case when emprestimos_giro_curto_pj_8M > emprestimos_giro_curto_pj_9M then 1 else 0 end +
	case when emprestimos_giro_curto_pj_7M > emprestimos_giro_curto_pj_8M then 1 else 0 end +
	case when emprestimos_giro_curto_pj_6M > emprestimos_giro_curto_pj_7M then 1 else 0 end +
	case when emprestimos_giro_curto_pj_5M > emprestimos_giro_curto_pj_6M then 1 else 0 end +
	case when emprestimos_giro_curto_pj_4M > emprestimos_giro_curto_pj_5M then 1 else 0 end +
	case when emprestimos_giro_curto_pj_3M > emprestimos_giro_curto_pj_4M then 1 else 0 end +
	case when emprestimos_giro_curto_pj_2M > emprestimos_giro_curto_pj_3M then 1 else 0 end +
	case when emprestimos_giro_curto_pj_1M > emprestimos_giro_curto_pj_2M then 1 else 0 end +
	case when emprestimos_giro_curto_pj_Curr > emprestimos_giro_curto_pj_1M then 1 else 0 end::float as Meses_Aumento_emprestimos_giro_curto_pj,
	case when emprestimos_giro_curto_pj_1M is null then null else
	case when emprestimos_giro_curto_pj_Curr >= emprestimos_giro_curto_pj_1M or emprestimos_giro_curto_pj_1M is null then 0 else
	case when emprestimos_giro_curto_pj_1M >= emprestimos_giro_curto_pj_2M or emprestimos_giro_curto_pj_2M is null then 1 else
	case when emprestimos_giro_curto_pj_2M >= emprestimos_giro_curto_pj_3M or emprestimos_giro_curto_pj_3M is null then 2 else
	case when emprestimos_giro_curto_pj_3M >= emprestimos_giro_curto_pj_4M or emprestimos_giro_curto_pj_4M is null then 3 else
	case when emprestimos_giro_curto_pj_4M >= emprestimos_giro_curto_pj_5M or emprestimos_giro_curto_pj_5M is null then 4 else
	case when emprestimos_giro_curto_pj_5M >= emprestimos_giro_curto_pj_6M or emprestimos_giro_curto_pj_6M is null then 5 else
	case when emprestimos_giro_curto_pj_6M >= emprestimos_giro_curto_pj_7M or emprestimos_giro_curto_pj_7M is null then 6 else
	case when emprestimos_giro_curto_pj_7M >= emprestimos_giro_curto_pj_8M or emprestimos_giro_curto_pj_8M is null then 7 else
	case when emprestimos_giro_curto_pj_8M >= emprestimos_giro_curto_pj_9M or emprestimos_giro_curto_pj_9M is null then 8 else
	case when emprestimos_giro_curto_pj_9M >= emprestimos_giro_curto_pj_10M or emprestimos_giro_curto_pj_10M is null then 9 else
	case when emprestimos_giro_curto_pj_10M >= emprestimos_giro_curto_pj_11M or emprestimos_giro_curto_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_emprestimos_giro_curto_pj,
	case when emprestimos_giro_curto_pj_1M is null then null else
	case when emprestimos_giro_curto_pj_Curr <= emprestimos_giro_curto_pj_1M or emprestimos_giro_curto_pj_1M is null then 0 else
	case when emprestimos_giro_curto_pj_1M <= emprestimos_giro_curto_pj_2M or emprestimos_giro_curto_pj_2M is null then 1 else
	case when emprestimos_giro_curto_pj_2M <= emprestimos_giro_curto_pj_3M or emprestimos_giro_curto_pj_3M is null then 2 else
	case when emprestimos_giro_curto_pj_3M <= emprestimos_giro_curto_pj_4M or emprestimos_giro_curto_pj_4M is null then 3 else
	case when emprestimos_giro_curto_pj_4M <= emprestimos_giro_curto_pj_5M or emprestimos_giro_curto_pj_5M is null then 4 else
	case when emprestimos_giro_curto_pj_5M <= emprestimos_giro_curto_pj_6M or emprestimos_giro_curto_pj_6M is null then 5 else
	case when emprestimos_giro_curto_pj_6M <= emprestimos_giro_curto_pj_7M or emprestimos_giro_curto_pj_7M is null then 6 else
	case when emprestimos_giro_curto_pj_7M <= emprestimos_giro_curto_pj_8M or emprestimos_giro_curto_pj_8M is null then 7 else
	case when emprestimos_giro_curto_pj_8M <= emprestimos_giro_curto_pj_9M or emprestimos_giro_curto_pj_9M is null then 8 else
	case when emprestimos_giro_curto_pj_9M <= emprestimos_giro_curto_pj_10M or emprestimos_giro_curto_pj_10M is null then 9 else
	case when emprestimos_giro_curto_pj_10M <= emprestimos_giro_curto_pj_11M or emprestimos_giro_curto_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_emprestimos_giro_curto_pj,
	(case when emprestimos_giro_curto_PJ_10M < emprestimos_giro_curto_PJ_11M then emprestimos_giro_curto_PJ_10M - emprestimos_giro_curto_PJ_11M else 0 end +
	case when emprestimos_giro_curto_PJ_9M < emprestimos_giro_curto_PJ_10M then emprestimos_giro_curto_PJ_9M - emprestimos_giro_curto_PJ_10M else 0 end +
	case when emprestimos_giro_curto_PJ_8M < emprestimos_giro_curto_PJ_9M then emprestimos_giro_curto_PJ_8M - emprestimos_giro_curto_PJ_9M else 0 end +
	case when emprestimos_giro_curto_PJ_7M < emprestimos_giro_curto_PJ_8M then emprestimos_giro_curto_PJ_7M - emprestimos_giro_curto_PJ_8M else 0 end +
	case when emprestimos_giro_curto_PJ_6M < emprestimos_giro_curto_PJ_7M then emprestimos_giro_curto_PJ_6M - emprestimos_giro_curto_PJ_7M else 0 end +
	case when emprestimos_giro_curto_PJ_5M < emprestimos_giro_curto_PJ_6M then emprestimos_giro_curto_PJ_5M - emprestimos_giro_curto_PJ_6M else 0 end +
	case when emprestimos_giro_curto_PJ_4M < emprestimos_giro_curto_PJ_5M then emprestimos_giro_curto_PJ_4M - emprestimos_giro_curto_PJ_5M else 0 end +
	case when emprestimos_giro_curto_PJ_3M < emprestimos_giro_curto_PJ_4M then emprestimos_giro_curto_PJ_3M - emprestimos_giro_curto_PJ_4M else 0 end +
	case when emprestimos_giro_curto_PJ_2M < emprestimos_giro_curto_PJ_3M then emprestimos_giro_curto_PJ_2M - emprestimos_giro_curto_PJ_3M else 0 end +
	case when emprestimos_giro_curto_PJ_1M < emprestimos_giro_curto_PJ_2M then emprestimos_giro_curto_PJ_1M - emprestimos_giro_curto_PJ_2M else 0 end +
	case when emprestimos_giro_curto_PJ_Curr < emprestimos_giro_curto_PJ_1M then emprestimos_giro_curto_PJ_Curr - emprestimos_giro_curto_PJ_1M else 0 end)*(-1)::float as Saldo_Amort_emprestimos_giro_curto_PJ,
	emprestimos_cartao_credito_PJ_Curr::float,
	emprestimos_cartao_credito_PJ_1M::float,
	emprestimos_cartao_credito_PJ_2M::float,
	emprestimos_cartao_credito_PJ_3M::float,
	emprestimos_cartao_credito_PJ_4M::float,
	emprestimos_cartao_credito_PJ_5M::float,
	emprestimos_cartao_credito_PJ_6M::float,
	emprestimos_cartao_credito_PJ_7M::float,
	emprestimos_cartao_credito_PJ_8M::float,
	emprestimos_cartao_credito_PJ_9M::float,
	emprestimos_cartao_credito_PJ_10M::float,
	emprestimos_cartao_credito_PJ_11M::float,
	greatest(emprestimos_cartao_credito_PJ_Curr,emprestimos_cartao_credito_PJ_1M,emprestimos_cartao_credito_PJ_2M,emprestimos_cartao_credito_PJ_3M,emprestimos_cartao_credito_PJ_4M,emprestimos_cartao_credito_PJ_5M,emprestimos_cartao_credito_PJ_6M,emprestimos_cartao_credito_PJ_7M,emprestimos_cartao_credito_PJ_8M,emprestimos_cartao_credito_PJ_9M,emprestimos_cartao_credito_PJ_10M,emprestimos_cartao_credito_PJ_11M)::float as max_emprestimos_cartao_credito_pj,
	Ever_emprestimos_cartao_credito_pj::float,
	count_emprestimos_cartao_credito_pj::int,
	case when emprestimos_cartao_credito_pj_10M < emprestimos_cartao_credito_pj_11M then 1 else 0 end +
	case when emprestimos_cartao_credito_pj_9M < emprestimos_cartao_credito_pj_10M then 1 else 0 end +
	case when emprestimos_cartao_credito_pj_8M < emprestimos_cartao_credito_pj_9M then 1 else 0 end +
	case when emprestimos_cartao_credito_pj_7M < emprestimos_cartao_credito_pj_8M then 1 else 0 end +
	case when emprestimos_cartao_credito_pj_6M < emprestimos_cartao_credito_pj_7M then 1 else 0 end +
	case when emprestimos_cartao_credito_pj_5M < emprestimos_cartao_credito_pj_6M then 1 else 0 end +
	case when emprestimos_cartao_credito_pj_4M < emprestimos_cartao_credito_pj_5M then 1 else 0 end +
	case when emprestimos_cartao_credito_pj_3M < emprestimos_cartao_credito_pj_4M then 1 else 0 end +
	case when emprestimos_cartao_credito_pj_2M < emprestimos_cartao_credito_pj_3M then 1 else 0 end +
	case when emprestimos_cartao_credito_pj_1M < emprestimos_cartao_credito_pj_2M then 1 else 0 end +
	case when emprestimos_cartao_credito_pj_Curr < emprestimos_cartao_credito_pj_1M then 1 else 0 end::float as Meses_Reducao_emprestimos_cartao_credito_pj,
	case when emprestimos_cartao_credito_pj_10M > emprestimos_cartao_credito_pj_11M then 1 else 0 end +
	case when emprestimos_cartao_credito_pj_9M > emprestimos_cartao_credito_pj_10M then 1 else 0 end +
	case when emprestimos_cartao_credito_pj_8M > emprestimos_cartao_credito_pj_9M then 1 else 0 end +
	case when emprestimos_cartao_credito_pj_7M > emprestimos_cartao_credito_pj_8M then 1 else 0 end +
	case when emprestimos_cartao_credito_pj_6M > emprestimos_cartao_credito_pj_7M then 1 else 0 end +
	case when emprestimos_cartao_credito_pj_5M > emprestimos_cartao_credito_pj_6M then 1 else 0 end +
	case when emprestimos_cartao_credito_pj_4M > emprestimos_cartao_credito_pj_5M then 1 else 0 end +
	case when emprestimos_cartao_credito_pj_3M > emprestimos_cartao_credito_pj_4M then 1 else 0 end +
	case when emprestimos_cartao_credito_pj_2M > emprestimos_cartao_credito_pj_3M then 1 else 0 end +
	case when emprestimos_cartao_credito_pj_1M > emprestimos_cartao_credito_pj_2M then 1 else 0 end +
	case when emprestimos_cartao_credito_pj_Curr > emprestimos_cartao_credito_pj_1M then 1 else 0 end::float as Meses_Aumento_emprestimos_cartao_credito_pj,
	case when emprestimos_cartao_credito_pj_1M is null then null else
	case when emprestimos_cartao_credito_pj_Curr >= emprestimos_cartao_credito_pj_1M or emprestimos_cartao_credito_pj_1M is null then 0 else
	case when emprestimos_cartao_credito_pj_1M >= emprestimos_cartao_credito_pj_2M or emprestimos_cartao_credito_pj_2M is null then 1 else
	case when emprestimos_cartao_credito_pj_2M >= emprestimos_cartao_credito_pj_3M or emprestimos_cartao_credito_pj_3M is null then 2 else
	case when emprestimos_cartao_credito_pj_3M >= emprestimos_cartao_credito_pj_4M or emprestimos_cartao_credito_pj_4M is null then 3 else
	case when emprestimos_cartao_credito_pj_4M >= emprestimos_cartao_credito_pj_5M or emprestimos_cartao_credito_pj_5M is null then 4 else
	case when emprestimos_cartao_credito_pj_5M >= emprestimos_cartao_credito_pj_6M or emprestimos_cartao_credito_pj_6M is null then 5 else
	case when emprestimos_cartao_credito_pj_6M >= emprestimos_cartao_credito_pj_7M or emprestimos_cartao_credito_pj_7M is null then 6 else
	case when emprestimos_cartao_credito_pj_7M >= emprestimos_cartao_credito_pj_8M or emprestimos_cartao_credito_pj_8M is null then 7 else
	case when emprestimos_cartao_credito_pj_8M >= emprestimos_cartao_credito_pj_9M or emprestimos_cartao_credito_pj_9M is null then 8 else
	case when emprestimos_cartao_credito_pj_9M >= emprestimos_cartao_credito_pj_10M or emprestimos_cartao_credito_pj_10M is null then 9 else
	case when emprestimos_cartao_credito_pj_10M >= emprestimos_cartao_credito_pj_11M or emprestimos_cartao_credito_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_emprestimos_cartao_credito_pj,
	case when emprestimos_cartao_credito_pj_1M is null then null else
	case when emprestimos_cartao_credito_pj_Curr <= emprestimos_cartao_credito_pj_1M or emprestimos_cartao_credito_pj_1M is null then 0 else
	case when emprestimos_cartao_credito_pj_1M <= emprestimos_cartao_credito_pj_2M or emprestimos_cartao_credito_pj_2M is null then 1 else
	case when emprestimos_cartao_credito_pj_2M <= emprestimos_cartao_credito_pj_3M or emprestimos_cartao_credito_pj_3M is null then 2 else
	case when emprestimos_cartao_credito_pj_3M <= emprestimos_cartao_credito_pj_4M or emprestimos_cartao_credito_pj_4M is null then 3 else
	case when emprestimos_cartao_credito_pj_4M <= emprestimos_cartao_credito_pj_5M or emprestimos_cartao_credito_pj_5M is null then 4 else
	case when emprestimos_cartao_credito_pj_5M <= emprestimos_cartao_credito_pj_6M or emprestimos_cartao_credito_pj_6M is null then 5 else
	case when emprestimos_cartao_credito_pj_6M <= emprestimos_cartao_credito_pj_7M or emprestimos_cartao_credito_pj_7M is null then 6 else
	case when emprestimos_cartao_credito_pj_7M <= emprestimos_cartao_credito_pj_8M or emprestimos_cartao_credito_pj_8M is null then 7 else
	case when emprestimos_cartao_credito_pj_8M <= emprestimos_cartao_credito_pj_9M or emprestimos_cartao_credito_pj_9M is null then 8 else
	case when emprestimos_cartao_credito_pj_9M <= emprestimos_cartao_credito_pj_10M or emprestimos_cartao_credito_pj_10M is null then 9 else
	case when emprestimos_cartao_credito_pj_10M <= emprestimos_cartao_credito_pj_11M or emprestimos_cartao_credito_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_emprestimos_cartao_credito_pj,
	(case when emprestimos_cartao_credito_PJ_10M < emprestimos_cartao_credito_PJ_11M then emprestimos_cartao_credito_PJ_10M - emprestimos_cartao_credito_PJ_11M else 0 end +
	case when emprestimos_cartao_credito_PJ_9M < emprestimos_cartao_credito_PJ_10M then emprestimos_cartao_credito_PJ_9M - emprestimos_cartao_credito_PJ_10M else 0 end +
	case when emprestimos_cartao_credito_PJ_8M < emprestimos_cartao_credito_PJ_9M then emprestimos_cartao_credito_PJ_8M - emprestimos_cartao_credito_PJ_9M else 0 end +
	case when emprestimos_cartao_credito_PJ_7M < emprestimos_cartao_credito_PJ_8M then emprestimos_cartao_credito_PJ_7M - emprestimos_cartao_credito_PJ_8M else 0 end +
	case when emprestimos_cartao_credito_PJ_6M < emprestimos_cartao_credito_PJ_7M then emprestimos_cartao_credito_PJ_6M - emprestimos_cartao_credito_PJ_7M else 0 end +
	case when emprestimos_cartao_credito_PJ_5M < emprestimos_cartao_credito_PJ_6M then emprestimos_cartao_credito_PJ_5M - emprestimos_cartao_credito_PJ_6M else 0 end +
	case when emprestimos_cartao_credito_PJ_4M < emprestimos_cartao_credito_PJ_5M then emprestimos_cartao_credito_PJ_4M - emprestimos_cartao_credito_PJ_5M else 0 end +
	case when emprestimos_cartao_credito_PJ_3M < emprestimos_cartao_credito_PJ_4M then emprestimos_cartao_credito_PJ_3M - emprestimos_cartao_credito_PJ_4M else 0 end +
	case when emprestimos_cartao_credito_PJ_2M < emprestimos_cartao_credito_PJ_3M then emprestimos_cartao_credito_PJ_2M - emprestimos_cartao_credito_PJ_3M else 0 end +
	case when emprestimos_cartao_credito_PJ_1M < emprestimos_cartao_credito_PJ_2M then emprestimos_cartao_credito_PJ_1M - emprestimos_cartao_credito_PJ_2M else 0 end +
	case when emprestimos_cartao_credito_PJ_Curr < emprestimos_cartao_credito_PJ_1M then emprestimos_cartao_credito_PJ_Curr - emprestimos_cartao_credito_PJ_1M else 0 end)*(-1)::float as Saldo_Amort_emprestimos_cartao_credito_PJ,
	emprestimos_conta_garantida_PJ_Curr::float,
	emprestimos_conta_garantida_PJ_1M::float,
	emprestimos_conta_garantida_PJ_2M::float,
	emprestimos_conta_garantida_PJ_3M::float,
	emprestimos_conta_garantida_PJ_4M::float,
	emprestimos_conta_garantida_PJ_5M::float,
	emprestimos_conta_garantida_PJ_6M::float,
	emprestimos_conta_garantida_PJ_7M::float,
	emprestimos_conta_garantida_PJ_8M::float,
	emprestimos_conta_garantida_PJ_9M::float,
	emprestimos_conta_garantida_PJ_10M::float,
	emprestimos_conta_garantida_PJ_11M::float,
	greatest(emprestimos_conta_garantida_PJ_Curr,emprestimos_conta_garantida_PJ_1M,emprestimos_conta_garantida_PJ_2M,emprestimos_conta_garantida_PJ_3M,emprestimos_conta_garantida_PJ_4M,emprestimos_conta_garantida_PJ_5M,emprestimos_conta_garantida_PJ_6M,emprestimos_conta_garantida_PJ_7M,emprestimos_conta_garantida_PJ_8M,emprestimos_conta_garantida_PJ_9M,emprestimos_conta_garantida_PJ_10M,emprestimos_conta_garantida_PJ_11M)::float as max_emprestimos_conta_garantida_pj,
	Ever_emprestimos_conta_garantida_pj::float,
	count_emprestimos_conta_garantida_pj::int,
	case when emprestimos_conta_garantida_pj_10M < emprestimos_conta_garantida_pj_11M then 1 else 0 end +
	case when emprestimos_conta_garantida_pj_9M < emprestimos_conta_garantida_pj_10M then 1 else 0 end +
	case when emprestimos_conta_garantida_pj_8M < emprestimos_conta_garantida_pj_9M then 1 else 0 end +
	case when emprestimos_conta_garantida_pj_7M < emprestimos_conta_garantida_pj_8M then 1 else 0 end +
	case when emprestimos_conta_garantida_pj_6M < emprestimos_conta_garantida_pj_7M then 1 else 0 end +
	case when emprestimos_conta_garantida_pj_5M < emprestimos_conta_garantida_pj_6M then 1 else 0 end +
	case when emprestimos_conta_garantida_pj_4M < emprestimos_conta_garantida_pj_5M then 1 else 0 end +
	case when emprestimos_conta_garantida_pj_3M < emprestimos_conta_garantida_pj_4M then 1 else 0 end +
	case when emprestimos_conta_garantida_pj_2M < emprestimos_conta_garantida_pj_3M then 1 else 0 end +
	case when emprestimos_conta_garantida_pj_1M < emprestimos_conta_garantida_pj_2M then 1 else 0 end +
	case when emprestimos_conta_garantida_pj_Curr < emprestimos_conta_garantida_pj_1M then 1 else 0 end::float as Meses_Reducao_emprestimos_conta_garantida_pj,
	case when emprestimos_conta_garantida_pj_10M > emprestimos_conta_garantida_pj_11M then 1 else 0 end +
	case when emprestimos_conta_garantida_pj_9M > emprestimos_conta_garantida_pj_10M then 1 else 0 end +
	case when emprestimos_conta_garantida_pj_8M > emprestimos_conta_garantida_pj_9M then 1 else 0 end +
	case when emprestimos_conta_garantida_pj_7M > emprestimos_conta_garantida_pj_8M then 1 else 0 end +
	case when emprestimos_conta_garantida_pj_6M > emprestimos_conta_garantida_pj_7M then 1 else 0 end +
	case when emprestimos_conta_garantida_pj_5M > emprestimos_conta_garantida_pj_6M then 1 else 0 end +
	case when emprestimos_conta_garantida_pj_4M > emprestimos_conta_garantida_pj_5M then 1 else 0 end +
	case when emprestimos_conta_garantida_pj_3M > emprestimos_conta_garantida_pj_4M then 1 else 0 end +
	case when emprestimos_conta_garantida_pj_2M > emprestimos_conta_garantida_pj_3M then 1 else 0 end +
	case when emprestimos_conta_garantida_pj_1M > emprestimos_conta_garantida_pj_2M then 1 else 0 end +
	case when emprestimos_conta_garantida_pj_Curr > emprestimos_conta_garantida_pj_1M then 1 else 0 end::float as Meses_Aumento_emprestimos_conta_garantida_pj,
	case when emprestimos_conta_garantida_pj_1M is null then null else
	case when emprestimos_conta_garantida_pj_Curr >= emprestimos_conta_garantida_pj_1M or emprestimos_conta_garantida_pj_1M is null then 0 else
	case when emprestimos_conta_garantida_pj_1M >= emprestimos_conta_garantida_pj_2M or emprestimos_conta_garantida_pj_2M is null then 1 else
	case when emprestimos_conta_garantida_pj_2M >= emprestimos_conta_garantida_pj_3M or emprestimos_conta_garantida_pj_3M is null then 2 else
	case when emprestimos_conta_garantida_pj_3M >= emprestimos_conta_garantida_pj_4M or emprestimos_conta_garantida_pj_4M is null then 3 else
	case when emprestimos_conta_garantida_pj_4M >= emprestimos_conta_garantida_pj_5M or emprestimos_conta_garantida_pj_5M is null then 4 else
	case when emprestimos_conta_garantida_pj_5M >= emprestimos_conta_garantida_pj_6M or emprestimos_conta_garantida_pj_6M is null then 5 else
	case when emprestimos_conta_garantida_pj_6M >= emprestimos_conta_garantida_pj_7M or emprestimos_conta_garantida_pj_7M is null then 6 else
	case when emprestimos_conta_garantida_pj_7M >= emprestimos_conta_garantida_pj_8M or emprestimos_conta_garantida_pj_8M is null then 7 else
	case when emprestimos_conta_garantida_pj_8M >= emprestimos_conta_garantida_pj_9M or emprestimos_conta_garantida_pj_9M is null then 8 else
	case when emprestimos_conta_garantida_pj_9M >= emprestimos_conta_garantida_pj_10M or emprestimos_conta_garantida_pj_10M is null then 9 else
	case when emprestimos_conta_garantida_pj_10M >= emprestimos_conta_garantida_pj_11M or emprestimos_conta_garantida_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_emprestimos_conta_garantida_pj,
	case when emprestimos_conta_garantida_pj_1M is null then null else
	case when emprestimos_conta_garantida_pj_Curr <= emprestimos_conta_garantida_pj_1M or emprestimos_conta_garantida_pj_1M is null then 0 else
	case when emprestimos_conta_garantida_pj_1M <= emprestimos_conta_garantida_pj_2M or emprestimos_conta_garantida_pj_2M is null then 1 else
	case when emprestimos_conta_garantida_pj_2M <= emprestimos_conta_garantida_pj_3M or emprestimos_conta_garantida_pj_3M is null then 2 else
	case when emprestimos_conta_garantida_pj_3M <= emprestimos_conta_garantida_pj_4M or emprestimos_conta_garantida_pj_4M is null then 3 else
	case when emprestimos_conta_garantida_pj_4M <= emprestimos_conta_garantida_pj_5M or emprestimos_conta_garantida_pj_5M is null then 4 else
	case when emprestimos_conta_garantida_pj_5M <= emprestimos_conta_garantida_pj_6M or emprestimos_conta_garantida_pj_6M is null then 5 else
	case when emprestimos_conta_garantida_pj_6M <= emprestimos_conta_garantida_pj_7M or emprestimos_conta_garantida_pj_7M is null then 6 else
	case when emprestimos_conta_garantida_pj_7M <= emprestimos_conta_garantida_pj_8M or emprestimos_conta_garantida_pj_8M is null then 7 else
	case when emprestimos_conta_garantida_pj_8M <= emprestimos_conta_garantida_pj_9M or emprestimos_conta_garantida_pj_9M is null then 8 else
	case when emprestimos_conta_garantida_pj_9M <= emprestimos_conta_garantida_pj_10M or emprestimos_conta_garantida_pj_10M is null then 9 else
	case when emprestimos_conta_garantida_pj_10M <= emprestimos_conta_garantida_pj_11M or emprestimos_conta_garantida_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_emprestimos_conta_garantida_pj,
	(case when emprestimos_conta_garantida_PJ_10M < emprestimos_conta_garantida_PJ_11M then emprestimos_conta_garantida_PJ_10M - emprestimos_conta_garantida_PJ_11M else 0 end +
	case when emprestimos_conta_garantida_PJ_9M < emprestimos_conta_garantida_PJ_10M then emprestimos_conta_garantida_PJ_9M - emprestimos_conta_garantida_PJ_10M else 0 end +
	case when emprestimos_conta_garantida_PJ_8M < emprestimos_conta_garantida_PJ_9M then emprestimos_conta_garantida_PJ_8M - emprestimos_conta_garantida_PJ_9M else 0 end +
	case when emprestimos_conta_garantida_PJ_7M < emprestimos_conta_garantida_PJ_8M then emprestimos_conta_garantida_PJ_7M - emprestimos_conta_garantida_PJ_8M else 0 end +
	case when emprestimos_conta_garantida_PJ_6M < emprestimos_conta_garantida_PJ_7M then emprestimos_conta_garantida_PJ_6M - emprestimos_conta_garantida_PJ_7M else 0 end +
	case when emprestimos_conta_garantida_PJ_5M < emprestimos_conta_garantida_PJ_6M then emprestimos_conta_garantida_PJ_5M - emprestimos_conta_garantida_PJ_6M else 0 end +
	case when emprestimos_conta_garantida_PJ_4M < emprestimos_conta_garantida_PJ_5M then emprestimos_conta_garantida_PJ_4M - emprestimos_conta_garantida_PJ_5M else 0 end +
	case when emprestimos_conta_garantida_PJ_3M < emprestimos_conta_garantida_PJ_4M then emprestimos_conta_garantida_PJ_3M - emprestimos_conta_garantida_PJ_4M else 0 end +
	case when emprestimos_conta_garantida_PJ_2M < emprestimos_conta_garantida_PJ_3M then emprestimos_conta_garantida_PJ_2M - emprestimos_conta_garantida_PJ_3M else 0 end +
	case when emprestimos_conta_garantida_PJ_1M < emprestimos_conta_garantida_PJ_2M then emprestimos_conta_garantida_PJ_1M - emprestimos_conta_garantida_PJ_2M else 0 end +
	case when emprestimos_conta_garantida_PJ_Curr < emprestimos_conta_garantida_PJ_1M then emprestimos_conta_garantida_PJ_Curr - emprestimos_conta_garantida_PJ_1M else 0 end)*(-1)::float as Saldo_Amort_emprestimos_conta_garantida_PJ,
	emprestimos_outros_emprestimos_PJ_Curr::float,
	emprestimos_outros_emprestimos_PJ_1M::float,
	emprestimos_outros_emprestimos_PJ_2M::float,
	emprestimos_outros_emprestimos_PJ_3M::float,
	emprestimos_outros_emprestimos_PJ_4M::float,
	emprestimos_outros_emprestimos_PJ_5M::float,
	emprestimos_outros_emprestimos_PJ_6M::float,
	emprestimos_outros_emprestimos_PJ_7M::float,
	emprestimos_outros_emprestimos_PJ_8M::float,
	emprestimos_outros_emprestimos_PJ_9M::float,
	emprestimos_outros_emprestimos_PJ_10M::float,
	emprestimos_outros_emprestimos_PJ_11M::float,
	greatest(emprestimos_outros_emprestimos_PJ_Curr,emprestimos_outros_emprestimos_PJ_1M,emprestimos_outros_emprestimos_PJ_2M,emprestimos_outros_emprestimos_PJ_3M,emprestimos_outros_emprestimos_PJ_4M,emprestimos_outros_emprestimos_PJ_5M,emprestimos_outros_emprestimos_PJ_6M,emprestimos_outros_emprestimos_PJ_7M,emprestimos_outros_emprestimos_PJ_8M,emprestimos_outros_emprestimos_PJ_9M,emprestimos_outros_emprestimos_PJ_10M,emprestimos_outros_emprestimos_PJ_11M)::float as max_emprestimos_outros_emprestimos_pj,
	Ever_emprestimos_outros_emprestimos_pj::float,
	count_emprestimos_outros_emprestimos_pj::int,
	case when emprestimos_outros_emprestimos_pj_10M < emprestimos_outros_emprestimos_pj_11M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pj_9M < emprestimos_outros_emprestimos_pj_10M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pj_8M < emprestimos_outros_emprestimos_pj_9M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pj_7M < emprestimos_outros_emprestimos_pj_8M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pj_6M < emprestimos_outros_emprestimos_pj_7M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pj_5M < emprestimos_outros_emprestimos_pj_6M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pj_4M < emprestimos_outros_emprestimos_pj_5M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pj_3M < emprestimos_outros_emprestimos_pj_4M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pj_2M < emprestimos_outros_emprestimos_pj_3M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pj_1M < emprestimos_outros_emprestimos_pj_2M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pj_Curr < emprestimos_outros_emprestimos_pj_1M then 1 else 0 end::float as Meses_Reducao_emprestimos_outros_emprestimos_pj,
	case when emprestimos_outros_emprestimos_pj_10M > emprestimos_outros_emprestimos_pj_11M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pj_9M > emprestimos_outros_emprestimos_pj_10M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pj_8M > emprestimos_outros_emprestimos_pj_9M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pj_7M > emprestimos_outros_emprestimos_pj_8M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pj_6M > emprestimos_outros_emprestimos_pj_7M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pj_5M > emprestimos_outros_emprestimos_pj_6M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pj_4M > emprestimos_outros_emprestimos_pj_5M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pj_3M > emprestimos_outros_emprestimos_pj_4M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pj_2M > emprestimos_outros_emprestimos_pj_3M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pj_1M > emprestimos_outros_emprestimos_pj_2M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pj_Curr > emprestimos_outros_emprestimos_pj_1M then 1 else 0 end::float as Meses_Aumento_emprestimos_outros_emprestimos_pj,
	case when emprestimos_outros_emprestimos_pj_1M is null then null else
	case when emprestimos_outros_emprestimos_pj_Curr >= emprestimos_outros_emprestimos_pj_1M or emprestimos_outros_emprestimos_pj_1M is null then 0 else
	case when emprestimos_outros_emprestimos_pj_1M >= emprestimos_outros_emprestimos_pj_2M or emprestimos_outros_emprestimos_pj_2M is null then 1 else
	case when emprestimos_outros_emprestimos_pj_2M >= emprestimos_outros_emprestimos_pj_3M or emprestimos_outros_emprestimos_pj_3M is null then 2 else
	case when emprestimos_outros_emprestimos_pj_3M >= emprestimos_outros_emprestimos_pj_4M or emprestimos_outros_emprestimos_pj_4M is null then 3 else
	case when emprestimos_outros_emprestimos_pj_4M >= emprestimos_outros_emprestimos_pj_5M or emprestimos_outros_emprestimos_pj_5M is null then 4 else
	case when emprestimos_outros_emprestimos_pj_5M >= emprestimos_outros_emprestimos_pj_6M or emprestimos_outros_emprestimos_pj_6M is null then 5 else
	case when emprestimos_outros_emprestimos_pj_6M >= emprestimos_outros_emprestimos_pj_7M or emprestimos_outros_emprestimos_pj_7M is null then 6 else
	case when emprestimos_outros_emprestimos_pj_7M >= emprestimos_outros_emprestimos_pj_8M or emprestimos_outros_emprestimos_pj_8M is null then 7 else
	case when emprestimos_outros_emprestimos_pj_8M >= emprestimos_outros_emprestimos_pj_9M or emprestimos_outros_emprestimos_pj_9M is null then 8 else
	case when emprestimos_outros_emprestimos_pj_9M >= emprestimos_outros_emprestimos_pj_10M or emprestimos_outros_emprestimos_pj_10M is null then 9 else
	case when emprestimos_outros_emprestimos_pj_10M >= emprestimos_outros_emprestimos_pj_11M or emprestimos_outros_emprestimos_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_emprestimos_outros_emprestimos_pj,
	case when emprestimos_outros_emprestimos_pj_1M is null then null else
	case when emprestimos_outros_emprestimos_pj_Curr <= emprestimos_outros_emprestimos_pj_1M or emprestimos_outros_emprestimos_pj_1M is null then 0 else
	case when emprestimos_outros_emprestimos_pj_1M <= emprestimos_outros_emprestimos_pj_2M or emprestimos_outros_emprestimos_pj_2M is null then 1 else
	case when emprestimos_outros_emprestimos_pj_2M <= emprestimos_outros_emprestimos_pj_3M or emprestimos_outros_emprestimos_pj_3M is null then 2 else
	case when emprestimos_outros_emprestimos_pj_3M <= emprestimos_outros_emprestimos_pj_4M or emprestimos_outros_emprestimos_pj_4M is null then 3 else
	case when emprestimos_outros_emprestimos_pj_4M <= emprestimos_outros_emprestimos_pj_5M or emprestimos_outros_emprestimos_pj_5M is null then 4 else
	case when emprestimos_outros_emprestimos_pj_5M <= emprestimos_outros_emprestimos_pj_6M or emprestimos_outros_emprestimos_pj_6M is null then 5 else
	case when emprestimos_outros_emprestimos_pj_6M <= emprestimos_outros_emprestimos_pj_7M or emprestimos_outros_emprestimos_pj_7M is null then 6 else
	case when emprestimos_outros_emprestimos_pj_7M <= emprestimos_outros_emprestimos_pj_8M or emprestimos_outros_emprestimos_pj_8M is null then 7 else
	case when emprestimos_outros_emprestimos_pj_8M <= emprestimos_outros_emprestimos_pj_9M or emprestimos_outros_emprestimos_pj_9M is null then 8 else
	case when emprestimos_outros_emprestimos_pj_9M <= emprestimos_outros_emprestimos_pj_10M or emprestimos_outros_emprestimos_pj_10M is null then 9 else
	case when emprestimos_outros_emprestimos_pj_10M <= emprestimos_outros_emprestimos_pj_11M or emprestimos_outros_emprestimos_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_emprestimos_outros_emprestimos_pj,
	(case when emprestimos_outros_emprestimos_PJ_10M < emprestimos_outros_emprestimos_PJ_11M then emprestimos_outros_emprestimos_PJ_10M - emprestimos_outros_emprestimos_PJ_11M else 0 end +
	case when emprestimos_outros_emprestimos_PJ_9M < emprestimos_outros_emprestimos_PJ_10M then emprestimos_outros_emprestimos_PJ_9M - emprestimos_outros_emprestimos_PJ_10M else 0 end +
	case when emprestimos_outros_emprestimos_PJ_8M < emprestimos_outros_emprestimos_PJ_9M then emprestimos_outros_emprestimos_PJ_8M - emprestimos_outros_emprestimos_PJ_9M else 0 end +
	case when emprestimos_outros_emprestimos_PJ_7M < emprestimos_outros_emprestimos_PJ_8M then emprestimos_outros_emprestimos_PJ_7M - emprestimos_outros_emprestimos_PJ_8M else 0 end +
	case when emprestimos_outros_emprestimos_PJ_6M < emprestimos_outros_emprestimos_PJ_7M then emprestimos_outros_emprestimos_PJ_6M - emprestimos_outros_emprestimos_PJ_7M else 0 end +
	case when emprestimos_outros_emprestimos_PJ_5M < emprestimos_outros_emprestimos_PJ_6M then emprestimos_outros_emprestimos_PJ_5M - emprestimos_outros_emprestimos_PJ_6M else 0 end +
	case when emprestimos_outros_emprestimos_PJ_4M < emprestimos_outros_emprestimos_PJ_5M then emprestimos_outros_emprestimos_PJ_4M - emprestimos_outros_emprestimos_PJ_5M else 0 end +
	case when emprestimos_outros_emprestimos_PJ_3M < emprestimos_outros_emprestimos_PJ_4M then emprestimos_outros_emprestimos_PJ_3M - emprestimos_outros_emprestimos_PJ_4M else 0 end +
	case when emprestimos_outros_emprestimos_PJ_2M < emprestimos_outros_emprestimos_PJ_3M then emprestimos_outros_emprestimos_PJ_2M - emprestimos_outros_emprestimos_PJ_3M else 0 end +
	case when emprestimos_outros_emprestimos_PJ_1M < emprestimos_outros_emprestimos_PJ_2M then emprestimos_outros_emprestimos_PJ_1M - emprestimos_outros_emprestimos_PJ_2M else 0 end +
	case when emprestimos_outros_emprestimos_PJ_Curr < emprestimos_outros_emprestimos_PJ_1M then emprestimos_outros_emprestimos_PJ_Curr - emprestimos_outros_emprestimos_PJ_1M else 0 end)*(-1)::float as Saldo_Amort_emprestimos_outros_emprestimos_PJ,
	emprestimos_giro_rotativo_PJ_Curr::float,
	emprestimos_giro_rotativo_PJ_1M::float,
	emprestimos_giro_rotativo_PJ_2M::float,
	emprestimos_giro_rotativo_PJ_3M::float,
	emprestimos_giro_rotativo_PJ_4M::float,
	emprestimos_giro_rotativo_PJ_5M::float,
	emprestimos_giro_rotativo_PJ_6M::float,
	emprestimos_giro_rotativo_PJ_7M::float,
	emprestimos_giro_rotativo_PJ_8M::float,
	emprestimos_giro_rotativo_PJ_9M::float,
	emprestimos_giro_rotativo_PJ_10M::float,
	emprestimos_giro_rotativo_PJ_11M::float,
	greatest(emprestimos_giro_rotativo_PJ_Curr,emprestimos_giro_rotativo_PJ_1M,emprestimos_giro_rotativo_PJ_2M,emprestimos_giro_rotativo_PJ_3M,emprestimos_giro_rotativo_PJ_4M,emprestimos_giro_rotativo_PJ_5M,emprestimos_giro_rotativo_PJ_6M,emprestimos_giro_rotativo_PJ_7M,emprestimos_giro_rotativo_PJ_8M,emprestimos_giro_rotativo_PJ_9M,emprestimos_giro_rotativo_PJ_10M,emprestimos_giro_rotativo_PJ_11M)::float as max_emprestimos_giro_rotativo_pj,
	Ever_emprestimos_giro_rotativo_pj::float,
	count_emprestimos_giro_rotativo_pj::int,
	case when emprestimos_giro_rotativo_pj_10M < emprestimos_giro_rotativo_pj_11M then 1 else 0 end +
	case when emprestimos_giro_rotativo_pj_9M < emprestimos_giro_rotativo_pj_10M then 1 else 0 end +
	case when emprestimos_giro_rotativo_pj_8M < emprestimos_giro_rotativo_pj_9M then 1 else 0 end +
	case when emprestimos_giro_rotativo_pj_7M < emprestimos_giro_rotativo_pj_8M then 1 else 0 end +
	case when emprestimos_giro_rotativo_pj_6M < emprestimos_giro_rotativo_pj_7M then 1 else 0 end +
	case when emprestimos_giro_rotativo_pj_5M < emprestimos_giro_rotativo_pj_6M then 1 else 0 end +
	case when emprestimos_giro_rotativo_pj_4M < emprestimos_giro_rotativo_pj_5M then 1 else 0 end +
	case when emprestimos_giro_rotativo_pj_3M < emprestimos_giro_rotativo_pj_4M then 1 else 0 end +
	case when emprestimos_giro_rotativo_pj_2M < emprestimos_giro_rotativo_pj_3M then 1 else 0 end +
	case when emprestimos_giro_rotativo_pj_1M < emprestimos_giro_rotativo_pj_2M then 1 else 0 end +
	case when emprestimos_giro_rotativo_pj_Curr < emprestimos_giro_rotativo_pj_1M then 1 else 0 end::float as Meses_Reducao_emprestimos_giro_rotativo_pj,
	case when emprestimos_giro_rotativo_pj_10M > emprestimos_giro_rotativo_pj_11M then 1 else 0 end +
	case when emprestimos_giro_rotativo_pj_9M > emprestimos_giro_rotativo_pj_10M then 1 else 0 end +
	case when emprestimos_giro_rotativo_pj_8M > emprestimos_giro_rotativo_pj_9M then 1 else 0 end +
	case when emprestimos_giro_rotativo_pj_7M > emprestimos_giro_rotativo_pj_8M then 1 else 0 end +
	case when emprestimos_giro_rotativo_pj_6M > emprestimos_giro_rotativo_pj_7M then 1 else 0 end +
	case when emprestimos_giro_rotativo_pj_5M > emprestimos_giro_rotativo_pj_6M then 1 else 0 end +
	case when emprestimos_giro_rotativo_pj_4M > emprestimos_giro_rotativo_pj_5M then 1 else 0 end +
	case when emprestimos_giro_rotativo_pj_3M > emprestimos_giro_rotativo_pj_4M then 1 else 0 end +
	case when emprestimos_giro_rotativo_pj_2M > emprestimos_giro_rotativo_pj_3M then 1 else 0 end +
	case when emprestimos_giro_rotativo_pj_1M > emprestimos_giro_rotativo_pj_2M then 1 else 0 end +
	case when emprestimos_giro_rotativo_pj_Curr > emprestimos_giro_rotativo_pj_1M then 1 else 0 end::float as Meses_Aumento_emprestimos_giro_rotativo_pj,
	case when emprestimos_giro_rotativo_pj_1M is null then null else
	case when emprestimos_giro_rotativo_pj_Curr >= emprestimos_giro_rotativo_pj_1M or emprestimos_giro_rotativo_pj_1M is null then 0 else
	case when emprestimos_giro_rotativo_pj_1M >= emprestimos_giro_rotativo_pj_2M or emprestimos_giro_rotativo_pj_2M is null then 1 else
	case when emprestimos_giro_rotativo_pj_2M >= emprestimos_giro_rotativo_pj_3M or emprestimos_giro_rotativo_pj_3M is null then 2 else
	case when emprestimos_giro_rotativo_pj_3M >= emprestimos_giro_rotativo_pj_4M or emprestimos_giro_rotativo_pj_4M is null then 3 else
	case when emprestimos_giro_rotativo_pj_4M >= emprestimos_giro_rotativo_pj_5M or emprestimos_giro_rotativo_pj_5M is null then 4 else
	case when emprestimos_giro_rotativo_pj_5M >= emprestimos_giro_rotativo_pj_6M or emprestimos_giro_rotativo_pj_6M is null then 5 else
	case when emprestimos_giro_rotativo_pj_6M >= emprestimos_giro_rotativo_pj_7M or emprestimos_giro_rotativo_pj_7M is null then 6 else
	case when emprestimos_giro_rotativo_pj_7M >= emprestimos_giro_rotativo_pj_8M or emprestimos_giro_rotativo_pj_8M is null then 7 else
	case when emprestimos_giro_rotativo_pj_8M >= emprestimos_giro_rotativo_pj_9M or emprestimos_giro_rotativo_pj_9M is null then 8 else
	case when emprestimos_giro_rotativo_pj_9M >= emprestimos_giro_rotativo_pj_10M or emprestimos_giro_rotativo_pj_10M is null then 9 else
	case when emprestimos_giro_rotativo_pj_10M >= emprestimos_giro_rotativo_pj_11M or emprestimos_giro_rotativo_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_emprestimos_giro_rotativo_pj,
	case when emprestimos_giro_rotativo_pj_1M is null then null else
	case when emprestimos_giro_rotativo_pj_Curr <= emprestimos_giro_rotativo_pj_1M or emprestimos_giro_rotativo_pj_1M is null then 0 else
	case when emprestimos_giro_rotativo_pj_1M <= emprestimos_giro_rotativo_pj_2M or emprestimos_giro_rotativo_pj_2M is null then 1 else
	case when emprestimos_giro_rotativo_pj_2M <= emprestimos_giro_rotativo_pj_3M or emprestimos_giro_rotativo_pj_3M is null then 2 else
	case when emprestimos_giro_rotativo_pj_3M <= emprestimos_giro_rotativo_pj_4M or emprestimos_giro_rotativo_pj_4M is null then 3 else
	case when emprestimos_giro_rotativo_pj_4M <= emprestimos_giro_rotativo_pj_5M or emprestimos_giro_rotativo_pj_5M is null then 4 else
	case when emprestimos_giro_rotativo_pj_5M <= emprestimos_giro_rotativo_pj_6M or emprestimos_giro_rotativo_pj_6M is null then 5 else
	case when emprestimos_giro_rotativo_pj_6M <= emprestimos_giro_rotativo_pj_7M or emprestimos_giro_rotativo_pj_7M is null then 6 else
	case when emprestimos_giro_rotativo_pj_7M <= emprestimos_giro_rotativo_pj_8M or emprestimos_giro_rotativo_pj_8M is null then 7 else
	case when emprestimos_giro_rotativo_pj_8M <= emprestimos_giro_rotativo_pj_9M or emprestimos_giro_rotativo_pj_9M is null then 8 else
	case when emprestimos_giro_rotativo_pj_9M <= emprestimos_giro_rotativo_pj_10M or emprestimos_giro_rotativo_pj_10M is null then 9 else
	case when emprestimos_giro_rotativo_pj_10M <= emprestimos_giro_rotativo_pj_11M or emprestimos_giro_rotativo_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_emprestimos_giro_rotativo_pj,
	(case when emprestimos_giro_rotativo_PJ_10M < emprestimos_giro_rotativo_PJ_11M then emprestimos_giro_rotativo_PJ_10M - emprestimos_giro_rotativo_PJ_11M else 0 end +
	case when emprestimos_giro_rotativo_PJ_9M < emprestimos_giro_rotativo_PJ_10M then emprestimos_giro_rotativo_PJ_9M - emprestimos_giro_rotativo_PJ_10M else 0 end +
	case when emprestimos_giro_rotativo_PJ_8M < emprestimos_giro_rotativo_PJ_9M then emprestimos_giro_rotativo_PJ_8M - emprestimos_giro_rotativo_PJ_9M else 0 end +
	case when emprestimos_giro_rotativo_PJ_7M < emprestimos_giro_rotativo_PJ_8M then emprestimos_giro_rotativo_PJ_7M - emprestimos_giro_rotativo_PJ_8M else 0 end +
	case when emprestimos_giro_rotativo_PJ_6M < emprestimos_giro_rotativo_PJ_7M then emprestimos_giro_rotativo_PJ_6M - emprestimos_giro_rotativo_PJ_7M else 0 end +
	case when emprestimos_giro_rotativo_PJ_5M < emprestimos_giro_rotativo_PJ_6M then emprestimos_giro_rotativo_PJ_5M - emprestimos_giro_rotativo_PJ_6M else 0 end +
	case when emprestimos_giro_rotativo_PJ_4M < emprestimos_giro_rotativo_PJ_5M then emprestimos_giro_rotativo_PJ_4M - emprestimos_giro_rotativo_PJ_5M else 0 end +
	case when emprestimos_giro_rotativo_PJ_3M < emprestimos_giro_rotativo_PJ_4M then emprestimos_giro_rotativo_PJ_3M - emprestimos_giro_rotativo_PJ_4M else 0 end +
	case when emprestimos_giro_rotativo_PJ_2M < emprestimos_giro_rotativo_PJ_3M then emprestimos_giro_rotativo_PJ_2M - emprestimos_giro_rotativo_PJ_3M else 0 end +
	case when emprestimos_giro_rotativo_PJ_1M < emprestimos_giro_rotativo_PJ_2M then emprestimos_giro_rotativo_PJ_1M - emprestimos_giro_rotativo_PJ_2M else 0 end +
	case when emprestimos_giro_rotativo_PJ_Curr < emprestimos_giro_rotativo_PJ_1M then emprestimos_giro_rotativo_PJ_Curr - emprestimos_giro_rotativo_PJ_1M else 0 end)*(-1)::float as Saldo_Amort_emprestimos_giro_rotativo_PJ,
	OutrosCreditos_PJ_Curr::float,
	OutrosCreditos_PJ_1M::float,
	OutrosCreditos_PJ_2M::float,
	OutrosCreditos_PJ_3M::float,
	OutrosCreditos_PJ_4M::float,
	OutrosCreditos_PJ_5M::float,
	OutrosCreditos_PJ_6M::float,
	OutrosCreditos_PJ_7M::float,
	OutrosCreditos_PJ_8M::float,
	OutrosCreditos_PJ_9M::float,
	OutrosCreditos_PJ_10M::float,
	OutrosCreditos_PJ_11M::float,
	greatest(OutrosCreditos_PJ_Curr,OutrosCreditos_PJ_1M,OutrosCreditos_PJ_2M,OutrosCreditos_PJ_3M,OutrosCreditos_PJ_4M,OutrosCreditos_PJ_5M,OutrosCreditos_PJ_6M,OutrosCreditos_PJ_7M,OutrosCreditos_PJ_8M,OutrosCreditos_PJ_9M,OutrosCreditos_PJ_10M,OutrosCreditos_PJ_11M)::float as max_OutrosCreditos_pj,
	Ever_OutrosCreditos_pj::float,
	count_OutrosCreditos_pj::int,
	case when OutrosCreditos_pj_10M < OutrosCreditos_pj_11M then 1 else 0 end +
	case when OutrosCreditos_pj_9M < OutrosCreditos_pj_10M then 1 else 0 end +
	case when OutrosCreditos_pj_8M < OutrosCreditos_pj_9M then 1 else 0 end +
	case when OutrosCreditos_pj_7M < OutrosCreditos_pj_8M then 1 else 0 end +
	case when OutrosCreditos_pj_6M < OutrosCreditos_pj_7M then 1 else 0 end +
	case when OutrosCreditos_pj_5M < OutrosCreditos_pj_6M then 1 else 0 end +
	case when OutrosCreditos_pj_4M < OutrosCreditos_pj_5M then 1 else 0 end +
	case when OutrosCreditos_pj_3M < OutrosCreditos_pj_4M then 1 else 0 end +
	case when OutrosCreditos_pj_2M < OutrosCreditos_pj_3M then 1 else 0 end +
	case when OutrosCreditos_pj_1M < OutrosCreditos_pj_2M then 1 else 0 end +
	case when OutrosCreditos_pj_Curr < OutrosCreditos_pj_1M then 1 else 0 end::float as Meses_Reducao_OutrosCreditos_pj,
	case when OutrosCreditos_pj_10M > OutrosCreditos_pj_11M then 1 else 0 end +
	case when OutrosCreditos_pj_9M > OutrosCreditos_pj_10M then 1 else 0 end +
	case when OutrosCreditos_pj_8M > OutrosCreditos_pj_9M then 1 else 0 end +
	case when OutrosCreditos_pj_7M > OutrosCreditos_pj_8M then 1 else 0 end +
	case when OutrosCreditos_pj_6M > OutrosCreditos_pj_7M then 1 else 0 end +
	case when OutrosCreditos_pj_5M > OutrosCreditos_pj_6M then 1 else 0 end +
	case when OutrosCreditos_pj_4M > OutrosCreditos_pj_5M then 1 else 0 end +
	case when OutrosCreditos_pj_3M > OutrosCreditos_pj_4M then 1 else 0 end +
	case when OutrosCreditos_pj_2M > OutrosCreditos_pj_3M then 1 else 0 end +
	case when OutrosCreditos_pj_1M > OutrosCreditos_pj_2M then 1 else 0 end +
	case when OutrosCreditos_pj_Curr > OutrosCreditos_pj_1M then 1 else 0 end::float as Meses_Aumento_OutrosCreditos_pj,
	case when OutrosCreditos_pj_1M is null then null else
	case when OutrosCreditos_pj_Curr >= OutrosCreditos_pj_1M or OutrosCreditos_pj_1M is null then 0 else
	case when OutrosCreditos_pj_1M >= OutrosCreditos_pj_2M or OutrosCreditos_pj_2M is null then 1 else
	case when OutrosCreditos_pj_2M >= OutrosCreditos_pj_3M or OutrosCreditos_pj_3M is null then 2 else
	case when OutrosCreditos_pj_3M >= OutrosCreditos_pj_4M or OutrosCreditos_pj_4M is null then 3 else
	case when OutrosCreditos_pj_4M >= OutrosCreditos_pj_5M or OutrosCreditos_pj_5M is null then 4 else
	case when OutrosCreditos_pj_5M >= OutrosCreditos_pj_6M or OutrosCreditos_pj_6M is null then 5 else
	case when OutrosCreditos_pj_6M >= OutrosCreditos_pj_7M or OutrosCreditos_pj_7M is null then 6 else
	case when OutrosCreditos_pj_7M >= OutrosCreditos_pj_8M or OutrosCreditos_pj_8M is null then 7 else
	case when OutrosCreditos_pj_8M >= OutrosCreditos_pj_9M or OutrosCreditos_pj_9M is null then 8 else
	case when OutrosCreditos_pj_9M >= OutrosCreditos_pj_10M or OutrosCreditos_pj_10M is null then 9 else
	case when OutrosCreditos_pj_10M >= OutrosCreditos_pj_11M or OutrosCreditos_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_OutrosCreditos_pj,
	case when OutrosCreditos_pj_1M is null then null else
	case when OutrosCreditos_pj_Curr <= OutrosCreditos_pj_1M or OutrosCreditos_pj_1M is null then 0 else
	case when OutrosCreditos_pj_1M <= OutrosCreditos_pj_2M or OutrosCreditos_pj_2M is null then 1 else
	case when OutrosCreditos_pj_2M <= OutrosCreditos_pj_3M or OutrosCreditos_pj_3M is null then 2 else
	case when OutrosCreditos_pj_3M <= OutrosCreditos_pj_4M or OutrosCreditos_pj_4M is null then 3 else
	case when OutrosCreditos_pj_4M <= OutrosCreditos_pj_5M or OutrosCreditos_pj_5M is null then 4 else
	case when OutrosCreditos_pj_5M <= OutrosCreditos_pj_6M or OutrosCreditos_pj_6M is null then 5 else
	case when OutrosCreditos_pj_6M <= OutrosCreditos_pj_7M or OutrosCreditos_pj_7M is null then 6 else
	case when OutrosCreditos_pj_7M <= OutrosCreditos_pj_8M or OutrosCreditos_pj_8M is null then 7 else
	case when OutrosCreditos_pj_8M <= OutrosCreditos_pj_9M or OutrosCreditos_pj_9M is null then 8 else
	case when OutrosCreditos_pj_9M <= OutrosCreditos_pj_10M or OutrosCreditos_pj_10M is null then 9 else
	case when OutrosCreditos_pj_10M <= OutrosCreditos_pj_11M or OutrosCreditos_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_OutrosCreditos_pj,
	(case when OutrosCreditos_PJ_10M < OutrosCreditos_PJ_11M then OutrosCreditos_PJ_10M - OutrosCreditos_PJ_11M else 0 end +
	case when OutrosCreditos_PJ_9M < OutrosCreditos_PJ_10M then OutrosCreditos_PJ_9M - OutrosCreditos_PJ_10M else 0 end +
	case when OutrosCreditos_PJ_8M < OutrosCreditos_PJ_9M then OutrosCreditos_PJ_8M - OutrosCreditos_PJ_9M else 0 end +
	case when OutrosCreditos_PJ_7M < OutrosCreditos_PJ_8M then OutrosCreditos_PJ_7M - OutrosCreditos_PJ_8M else 0 end +
	case when OutrosCreditos_PJ_6M < OutrosCreditos_PJ_7M then OutrosCreditos_PJ_6M - OutrosCreditos_PJ_7M else 0 end +
	case when OutrosCreditos_PJ_5M < OutrosCreditos_PJ_6M then OutrosCreditos_PJ_5M - OutrosCreditos_PJ_6M else 0 end +
	case when OutrosCreditos_PJ_4M < OutrosCreditos_PJ_5M then OutrosCreditos_PJ_4M - OutrosCreditos_PJ_5M else 0 end +
	case when OutrosCreditos_PJ_3M < OutrosCreditos_PJ_4M then OutrosCreditos_PJ_3M - OutrosCreditos_PJ_4M else 0 end +
	case when OutrosCreditos_PJ_2M < OutrosCreditos_PJ_3M then OutrosCreditos_PJ_2M - OutrosCreditos_PJ_3M else 0 end +
	case when OutrosCreditos_PJ_1M < OutrosCreditos_PJ_2M then OutrosCreditos_PJ_1M - OutrosCreditos_PJ_2M else 0 end +
	case when OutrosCreditos_PJ_Curr < OutrosCreditos_PJ_1M then OutrosCreditos_PJ_Curr - OutrosCreditos_PJ_1M else 0 end)*(-1)::float as Saldo_Amort_OutrosCreditos_PJ,
	Desconto_PJ_Curr::float,
	Desconto_PJ_1M::float,
	Desconto_PJ_2M::float,
	Desconto_PJ_3M::float,
	Desconto_PJ_4M::float,
	Desconto_PJ_5M::float,
	Desconto_PJ_6M::float,
	Desconto_PJ_7M::float,
	Desconto_PJ_8M::float,
	Desconto_PJ_9M::float,
	Desconto_PJ_10M::float,
	Desconto_PJ_11M::float,
	greatest(Desconto_PJ_Curr,Desconto_PJ_1M,Desconto_PJ_2M,Desconto_PJ_3M,Desconto_PJ_4M,Desconto_PJ_5M,Desconto_PJ_6M,Desconto_PJ_7M,Desconto_PJ_8M,Desconto_PJ_9M,Desconto_PJ_10M,Desconto_PJ_11M)::float as max_Desconto_pj,
	Ever_Desconto_pj::float,
	count_Desconto_pj::int,
	case when Desconto_pj_10M < Desconto_pj_11M then 1 else 0 end +
	case when Desconto_pj_9M < Desconto_pj_10M then 1 else 0 end +
	case when Desconto_pj_8M < Desconto_pj_9M then 1 else 0 end +
	case when Desconto_pj_7M < Desconto_pj_8M then 1 else 0 end +
	case when Desconto_pj_6M < Desconto_pj_7M then 1 else 0 end +
	case when Desconto_pj_5M < Desconto_pj_6M then 1 else 0 end +
	case when Desconto_pj_4M < Desconto_pj_5M then 1 else 0 end +
	case when Desconto_pj_3M < Desconto_pj_4M then 1 else 0 end +
	case when Desconto_pj_2M < Desconto_pj_3M then 1 else 0 end +
	case when Desconto_pj_1M < Desconto_pj_2M then 1 else 0 end +
	case when Desconto_pj_Curr < Desconto_pj_1M then 1 else 0 end::float as Meses_Reducao_Desconto_pj,
	case when Desconto_pj_10M > Desconto_pj_11M then 1 else 0 end +
	case when Desconto_pj_9M > Desconto_pj_10M then 1 else 0 end +
	case when Desconto_pj_8M > Desconto_pj_9M then 1 else 0 end +
	case when Desconto_pj_7M > Desconto_pj_8M then 1 else 0 end +
	case when Desconto_pj_6M > Desconto_pj_7M then 1 else 0 end +
	case when Desconto_pj_5M > Desconto_pj_6M then 1 else 0 end +
	case when Desconto_pj_4M > Desconto_pj_5M then 1 else 0 end +
	case when Desconto_pj_3M > Desconto_pj_4M then 1 else 0 end +
	case when Desconto_pj_2M > Desconto_pj_3M then 1 else 0 end +
	case when Desconto_pj_1M > Desconto_pj_2M then 1 else 0 end +
	case when Desconto_pj_Curr > Desconto_pj_1M then 1 else 0 end::float as Meses_Aumento_Desconto_pj,
	case when Desconto_pj_1M is null then null else
	case when Desconto_pj_Curr >= Desconto_pj_1M or Desconto_pj_1M is null then 0 else
	case when Desconto_pj_1M >= Desconto_pj_2M or Desconto_pj_2M is null then 1 else
	case when Desconto_pj_2M >= Desconto_pj_3M or Desconto_pj_3M is null then 2 else
	case when Desconto_pj_3M >= Desconto_pj_4M or Desconto_pj_4M is null then 3 else
	case when Desconto_pj_4M >= Desconto_pj_5M or Desconto_pj_5M is null then 4 else
	case when Desconto_pj_5M >= Desconto_pj_6M or Desconto_pj_6M is null then 5 else
	case when Desconto_pj_6M >= Desconto_pj_7M or Desconto_pj_7M is null then 6 else
	case when Desconto_pj_7M >= Desconto_pj_8M or Desconto_pj_8M is null then 7 else
	case when Desconto_pj_8M >= Desconto_pj_9M or Desconto_pj_9M is null then 8 else
	case when Desconto_pj_9M >= Desconto_pj_10M or Desconto_pj_10M is null then 9 else
	case when Desconto_pj_10M >= Desconto_pj_11M or Desconto_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_Desconto_pj,
	case when Desconto_pj_1M is null then null else
	case when Desconto_pj_Curr <= Desconto_pj_1M or Desconto_pj_1M is null then 0 else
	case when Desconto_pj_1M <= Desconto_pj_2M or Desconto_pj_2M is null then 1 else
	case when Desconto_pj_2M <= Desconto_pj_3M or Desconto_pj_3M is null then 2 else
	case when Desconto_pj_3M <= Desconto_pj_4M or Desconto_pj_4M is null then 3 else
	case when Desconto_pj_4M <= Desconto_pj_5M or Desconto_pj_5M is null then 4 else
	case when Desconto_pj_5M <= Desconto_pj_6M or Desconto_pj_6M is null then 5 else
	case when Desconto_pj_6M <= Desconto_pj_7M or Desconto_pj_7M is null then 6 else
	case when Desconto_pj_7M <= Desconto_pj_8M or Desconto_pj_8M is null then 7 else
	case when Desconto_pj_8M <= Desconto_pj_9M or Desconto_pj_9M is null then 8 else
	case when Desconto_pj_9M <= Desconto_pj_10M or Desconto_pj_10M is null then 9 else
	case when Desconto_pj_10M <= Desconto_pj_11M or Desconto_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_Desconto_pj,
	(case when Desconto_PJ_10M < Desconto_PJ_11M then Desconto_PJ_10M - Desconto_PJ_11M else 0 end +
	case when Desconto_PJ_9M < Desconto_PJ_10M then Desconto_PJ_9M - Desconto_PJ_10M else 0 end +
	case when Desconto_PJ_8M < Desconto_PJ_9M then Desconto_PJ_8M - Desconto_PJ_9M else 0 end +
	case when Desconto_PJ_7M < Desconto_PJ_8M then Desconto_PJ_7M - Desconto_PJ_8M else 0 end +
	case when Desconto_PJ_6M < Desconto_PJ_7M then Desconto_PJ_6M - Desconto_PJ_7M else 0 end +
	case when Desconto_PJ_5M < Desconto_PJ_6M then Desconto_PJ_5M - Desconto_PJ_6M else 0 end +
	case when Desconto_PJ_4M < Desconto_PJ_5M then Desconto_PJ_4M - Desconto_PJ_5M else 0 end +
	case when Desconto_PJ_3M < Desconto_PJ_4M then Desconto_PJ_3M - Desconto_PJ_4M else 0 end +
	case when Desconto_PJ_2M < Desconto_PJ_3M then Desconto_PJ_2M - Desconto_PJ_3M else 0 end +
	case when Desconto_PJ_1M < Desconto_PJ_2M then Desconto_PJ_1M - Desconto_PJ_2M else 0 end +
	case when Desconto_PJ_Curr < Desconto_PJ_1M then Desconto_PJ_Curr - Desconto_PJ_1M else 0 end)*(-1)::float as Saldo_Amort_Desconto_PJ,
	Financiamentos_PJ_Curr::float,
	Financiamentos_PJ_1M::float,
	Financiamentos_PJ_2M::float,
	Financiamentos_PJ_3M::float,
	Financiamentos_PJ_4M::float,
	Financiamentos_PJ_5M::float,
	Financiamentos_PJ_6M::float,
	Financiamentos_PJ_7M::float,
	Financiamentos_PJ_8M::float,
	Financiamentos_PJ_9M::float,
	Financiamentos_PJ_10M::float,
	Financiamentos_PJ_11M::float,
	greatest(Financiamentos_PJ_Curr,Financiamentos_PJ_1M,Financiamentos_PJ_2M,Financiamentos_PJ_3M,Financiamentos_PJ_4M,Financiamentos_PJ_5M,Financiamentos_PJ_6M,Financiamentos_PJ_7M,Financiamentos_PJ_8M,Financiamentos_PJ_9M,Financiamentos_PJ_10M,Financiamentos_PJ_11M)::float as max_Financiamentos_pj,
	Ever_Financiamentos_pj::float,
	count_Financiamentos_pj::int,
	case when Financiamentos_pj_10M < Financiamentos_pj_11M then 1 else 0 end +
	case when Financiamentos_pj_9M < Financiamentos_pj_10M then 1 else 0 end +
	case when Financiamentos_pj_8M < Financiamentos_pj_9M then 1 else 0 end +
	case when Financiamentos_pj_7M < Financiamentos_pj_8M then 1 else 0 end +
	case when Financiamentos_pj_6M < Financiamentos_pj_7M then 1 else 0 end +
	case when Financiamentos_pj_5M < Financiamentos_pj_6M then 1 else 0 end +
	case when Financiamentos_pj_4M < Financiamentos_pj_5M then 1 else 0 end +
	case when Financiamentos_pj_3M < Financiamentos_pj_4M then 1 else 0 end +
	case when Financiamentos_pj_2M < Financiamentos_pj_3M then 1 else 0 end +
	case when Financiamentos_pj_1M < Financiamentos_pj_2M then 1 else 0 end +
	case when Financiamentos_pj_Curr < Financiamentos_pj_1M then 1 else 0 end::float as Meses_Reducao_Financiamentos_pj,
	case when Financiamentos_pj_10M > Financiamentos_pj_11M then 1 else 0 end +
	case when Financiamentos_pj_9M > Financiamentos_pj_10M then 1 else 0 end +
	case when Financiamentos_pj_8M > Financiamentos_pj_9M then 1 else 0 end +
	case when Financiamentos_pj_7M > Financiamentos_pj_8M then 1 else 0 end +
	case when Financiamentos_pj_6M > Financiamentos_pj_7M then 1 else 0 end +
	case when Financiamentos_pj_5M > Financiamentos_pj_6M then 1 else 0 end +
	case when Financiamentos_pj_4M > Financiamentos_pj_5M then 1 else 0 end +
	case when Financiamentos_pj_3M > Financiamentos_pj_4M then 1 else 0 end +
	case when Financiamentos_pj_2M > Financiamentos_pj_3M then 1 else 0 end +
	case when Financiamentos_pj_1M > Financiamentos_pj_2M then 1 else 0 end +
	case when Financiamentos_pj_Curr > Financiamentos_pj_1M then 1 else 0 end::float as Meses_Aumento_Financiamentos_pj,
	case when Financiamentos_pj_1M is null then null else
	case when Financiamentos_pj_Curr >= Financiamentos_pj_1M or Financiamentos_pj_1M is null then 0 else
	case when Financiamentos_pj_1M >= Financiamentos_pj_2M or Financiamentos_pj_2M is null then 1 else
	case when Financiamentos_pj_2M >= Financiamentos_pj_3M or Financiamentos_pj_3M is null then 2 else
	case when Financiamentos_pj_3M >= Financiamentos_pj_4M or Financiamentos_pj_4M is null then 3 else
	case when Financiamentos_pj_4M >= Financiamentos_pj_5M or Financiamentos_pj_5M is null then 4 else
	case when Financiamentos_pj_5M >= Financiamentos_pj_6M or Financiamentos_pj_6M is null then 5 else
	case when Financiamentos_pj_6M >= Financiamentos_pj_7M or Financiamentos_pj_7M is null then 6 else
	case when Financiamentos_pj_7M >= Financiamentos_pj_8M or Financiamentos_pj_8M is null then 7 else
	case when Financiamentos_pj_8M >= Financiamentos_pj_9M or Financiamentos_pj_9M is null then 8 else
	case when Financiamentos_pj_9M >= Financiamentos_pj_10M or Financiamentos_pj_10M is null then 9 else
	case when Financiamentos_pj_10M >= Financiamentos_pj_11M or Financiamentos_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_Financiamentos_pj,
	case when Financiamentos_pj_1M is null then null else
	case when Financiamentos_pj_Curr <= Financiamentos_pj_1M or Financiamentos_pj_1M is null then 0 else
	case when Financiamentos_pj_1M <= Financiamentos_pj_2M or Financiamentos_pj_2M is null then 1 else
	case when Financiamentos_pj_2M <= Financiamentos_pj_3M or Financiamentos_pj_3M is null then 2 else
	case when Financiamentos_pj_3M <= Financiamentos_pj_4M or Financiamentos_pj_4M is null then 3 else
	case when Financiamentos_pj_4M <= Financiamentos_pj_5M or Financiamentos_pj_5M is null then 4 else
	case when Financiamentos_pj_5M <= Financiamentos_pj_6M or Financiamentos_pj_6M is null then 5 else
	case when Financiamentos_pj_6M <= Financiamentos_pj_7M or Financiamentos_pj_7M is null then 6 else
	case when Financiamentos_pj_7M <= Financiamentos_pj_8M or Financiamentos_pj_8M is null then 7 else
	case when Financiamentos_pj_8M <= Financiamentos_pj_9M or Financiamentos_pj_9M is null then 8 else
	case when Financiamentos_pj_9M <= Financiamentos_pj_10M or Financiamentos_pj_10M is null then 9 else
	case when Financiamentos_pj_10M <= Financiamentos_pj_11M or Financiamentos_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_Financiamentos_pj,
	(case when Financiamentos_PJ_10M < Financiamentos_PJ_11M then Financiamentos_PJ_10M - Financiamentos_PJ_11M else 0 end +
	case when Financiamentos_PJ_9M < Financiamentos_PJ_10M then Financiamentos_PJ_9M - Financiamentos_PJ_10M else 0 end +
	case when Financiamentos_PJ_8M < Financiamentos_PJ_9M then Financiamentos_PJ_8M - Financiamentos_PJ_9M else 0 end +
	case when Financiamentos_PJ_7M < Financiamentos_PJ_8M then Financiamentos_PJ_7M - Financiamentos_PJ_8M else 0 end +
	case when Financiamentos_PJ_6M < Financiamentos_PJ_7M then Financiamentos_PJ_6M - Financiamentos_PJ_7M else 0 end +
	case when Financiamentos_PJ_5M < Financiamentos_PJ_6M then Financiamentos_PJ_5M - Financiamentos_PJ_6M else 0 end +
	case when Financiamentos_PJ_4M < Financiamentos_PJ_5M then Financiamentos_PJ_4M - Financiamentos_PJ_5M else 0 end +
	case when Financiamentos_PJ_3M < Financiamentos_PJ_4M then Financiamentos_PJ_3M - Financiamentos_PJ_4M else 0 end +
	case when Financiamentos_PJ_2M < Financiamentos_PJ_3M then Financiamentos_PJ_2M - Financiamentos_PJ_3M else 0 end +
	case when Financiamentos_PJ_1M < Financiamentos_PJ_2M then Financiamentos_PJ_1M - Financiamentos_PJ_2M else 0 end +
	case when Financiamentos_PJ_Curr < Financiamentos_PJ_1M then Financiamentos_PJ_Curr - Financiamentos_PJ_1M else 0 end)*(-1)::float as Saldo_Amort_Financiamentos_PJ,
	Adiantamentos_PJ_Curr::float,
	Adiantamentos_PJ_1M::float,
	Adiantamentos_PJ_2M::float,
	Adiantamentos_PJ_3M::float,
	Adiantamentos_PJ_4M::float,
	Adiantamentos_PJ_5M::float,
	Adiantamentos_PJ_6M::float,
	Adiantamentos_PJ_7M::float,
	Adiantamentos_PJ_8M::float,
	Adiantamentos_PJ_9M::float,
	Adiantamentos_PJ_10M::float,
	Adiantamentos_PJ_11M::float,
	greatest(Adiantamentos_PJ_Curr,Adiantamentos_PJ_1M,Adiantamentos_PJ_2M,Adiantamentos_PJ_3M,Adiantamentos_PJ_4M,Adiantamentos_PJ_5M,Adiantamentos_PJ_6M,Adiantamentos_PJ_7M,Adiantamentos_PJ_8M,Adiantamentos_PJ_9M,Adiantamentos_PJ_10M,Adiantamentos_PJ_11M)::float as max_Adiantamentos_pj,
	Ever_Adiantamentos_pj::float,
	count_Adiantamentos_pj::int,
	case when Adiantamentos_pj_10M < Adiantamentos_pj_11M then 1 else 0 end +
	case when Adiantamentos_pj_9M < Adiantamentos_pj_10M then 1 else 0 end +
	case when Adiantamentos_pj_8M < Adiantamentos_pj_9M then 1 else 0 end +
	case when Adiantamentos_pj_7M < Adiantamentos_pj_8M then 1 else 0 end +
	case when Adiantamentos_pj_6M < Adiantamentos_pj_7M then 1 else 0 end +
	case when Adiantamentos_pj_5M < Adiantamentos_pj_6M then 1 else 0 end +
	case when Adiantamentos_pj_4M < Adiantamentos_pj_5M then 1 else 0 end +
	case when Adiantamentos_pj_3M < Adiantamentos_pj_4M then 1 else 0 end +
	case when Adiantamentos_pj_2M < Adiantamentos_pj_3M then 1 else 0 end +
	case when Adiantamentos_pj_1M < Adiantamentos_pj_2M then 1 else 0 end +
	case when Adiantamentos_pj_Curr < Adiantamentos_pj_1M then 1 else 0 end::float as Meses_Reducao_Adiantamentos_pj,
	case when Adiantamentos_pj_10M > Adiantamentos_pj_11M then 1 else 0 end +
	case when Adiantamentos_pj_9M > Adiantamentos_pj_10M then 1 else 0 end +
	case when Adiantamentos_pj_8M > Adiantamentos_pj_9M then 1 else 0 end +
	case when Adiantamentos_pj_7M > Adiantamentos_pj_8M then 1 else 0 end +
	case when Adiantamentos_pj_6M > Adiantamentos_pj_7M then 1 else 0 end +
	case when Adiantamentos_pj_5M > Adiantamentos_pj_6M then 1 else 0 end +
	case when Adiantamentos_pj_4M > Adiantamentos_pj_5M then 1 else 0 end +
	case when Adiantamentos_pj_3M > Adiantamentos_pj_4M then 1 else 0 end +
	case when Adiantamentos_pj_2M > Adiantamentos_pj_3M then 1 else 0 end +
	case when Adiantamentos_pj_1M > Adiantamentos_pj_2M then 1 else 0 end +
	case when Adiantamentos_pj_Curr > Adiantamentos_pj_1M then 1 else 0 end::float as Meses_Aumento_Adiantamentos_pj,
	case when Adiantamentos_pj_1M is null then null else
	case when Adiantamentos_pj_Curr >= Adiantamentos_pj_1M or Adiantamentos_pj_1M is null then 0 else
	case when Adiantamentos_pj_1M >= Adiantamentos_pj_2M or Adiantamentos_pj_2M is null then 1 else
	case when Adiantamentos_pj_2M >= Adiantamentos_pj_3M or Adiantamentos_pj_3M is null then 2 else
	case when Adiantamentos_pj_3M >= Adiantamentos_pj_4M or Adiantamentos_pj_4M is null then 3 else
	case when Adiantamentos_pj_4M >= Adiantamentos_pj_5M or Adiantamentos_pj_5M is null then 4 else
	case when Adiantamentos_pj_5M >= Adiantamentos_pj_6M or Adiantamentos_pj_6M is null then 5 else
	case when Adiantamentos_pj_6M >= Adiantamentos_pj_7M or Adiantamentos_pj_7M is null then 6 else
	case when Adiantamentos_pj_7M >= Adiantamentos_pj_8M or Adiantamentos_pj_8M is null then 7 else
	case when Adiantamentos_pj_8M >= Adiantamentos_pj_9M or Adiantamentos_pj_9M is null then 8 else
	case when Adiantamentos_pj_9M >= Adiantamentos_pj_10M or Adiantamentos_pj_10M is null then 9 else
	case when Adiantamentos_pj_10M >= Adiantamentos_pj_11M or Adiantamentos_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_Adiantamentos_pj,
	case when Adiantamentos_pj_1M is null then null else
	case when Adiantamentos_pj_Curr <= Adiantamentos_pj_1M or Adiantamentos_pj_1M is null then 0 else
	case when Adiantamentos_pj_1M <= Adiantamentos_pj_2M or Adiantamentos_pj_2M is null then 1 else
	case when Adiantamentos_pj_2M <= Adiantamentos_pj_3M or Adiantamentos_pj_3M is null then 2 else
	case when Adiantamentos_pj_3M <= Adiantamentos_pj_4M or Adiantamentos_pj_4M is null then 3 else
	case when Adiantamentos_pj_4M <= Adiantamentos_pj_5M or Adiantamentos_pj_5M is null then 4 else
	case when Adiantamentos_pj_5M <= Adiantamentos_pj_6M or Adiantamentos_pj_6M is null then 5 else
	case when Adiantamentos_pj_6M <= Adiantamentos_pj_7M or Adiantamentos_pj_7M is null then 6 else
	case when Adiantamentos_pj_7M <= Adiantamentos_pj_8M or Adiantamentos_pj_8M is null then 7 else
	case when Adiantamentos_pj_8M <= Adiantamentos_pj_9M or Adiantamentos_pj_9M is null then 8 else
	case when Adiantamentos_pj_9M <= Adiantamentos_pj_10M or Adiantamentos_pj_10M is null then 9 else
	case when Adiantamentos_pj_10M <= Adiantamentos_pj_11M or Adiantamentos_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_Adiantamentos_pj,
	(case when Adiantamentos_PJ_10M < Adiantamentos_PJ_11M then Adiantamentos_PJ_10M - Adiantamentos_PJ_11M else 0 end +
	case when Adiantamentos_PJ_9M < Adiantamentos_PJ_10M then Adiantamentos_PJ_9M - Adiantamentos_PJ_10M else 0 end +
	case when Adiantamentos_PJ_8M < Adiantamentos_PJ_9M then Adiantamentos_PJ_8M - Adiantamentos_PJ_9M else 0 end +
	case when Adiantamentos_PJ_7M < Adiantamentos_PJ_8M then Adiantamentos_PJ_7M - Adiantamentos_PJ_8M else 0 end +
	case when Adiantamentos_PJ_6M < Adiantamentos_PJ_7M then Adiantamentos_PJ_6M - Adiantamentos_PJ_7M else 0 end +
	case when Adiantamentos_PJ_5M < Adiantamentos_PJ_6M then Adiantamentos_PJ_5M - Adiantamentos_PJ_6M else 0 end +
	case when Adiantamentos_PJ_4M < Adiantamentos_PJ_5M then Adiantamentos_PJ_4M - Adiantamentos_PJ_5M else 0 end +
	case when Adiantamentos_PJ_3M < Adiantamentos_PJ_4M then Adiantamentos_PJ_3M - Adiantamentos_PJ_4M else 0 end +
	case when Adiantamentos_PJ_2M < Adiantamentos_PJ_3M then Adiantamentos_PJ_2M - Adiantamentos_PJ_3M else 0 end +
	case when Adiantamentos_PJ_1M < Adiantamentos_PJ_2M then Adiantamentos_PJ_1M - Adiantamentos_PJ_2M else 0 end +
	case when Adiantamentos_PJ_Curr < Adiantamentos_PJ_1M then Adiantamentos_PJ_Curr - Adiantamentos_PJ_1M else 0 end)*(-1)::float as Saldo_Amort_Adiantamentos_PJ,
	OutrosFinanciamentos_PJ_Curr::float,
	OutrosFinanciamentos_PJ_1M::float,
	OutrosFinanciamentos_PJ_2M::float,
	OutrosFinanciamentos_PJ_3M::float,
	OutrosFinanciamentos_PJ_4M::float,
	OutrosFinanciamentos_PJ_5M::float,
	OutrosFinanciamentos_PJ_6M::float,
	OutrosFinanciamentos_PJ_7M::float,
	OutrosFinanciamentos_PJ_8M::float,
	OutrosFinanciamentos_PJ_9M::float,
	OutrosFinanciamentos_PJ_10M::float,
	OutrosFinanciamentos_PJ_11M::float,
	greatest(OutrosFinanciamentos_PJ_Curr,OutrosFinanciamentos_PJ_1M,OutrosFinanciamentos_PJ_2M,OutrosFinanciamentos_PJ_3M,OutrosFinanciamentos_PJ_4M,OutrosFinanciamentos_PJ_5M,OutrosFinanciamentos_PJ_6M,OutrosFinanciamentos_PJ_7M,OutrosFinanciamentos_PJ_8M,OutrosFinanciamentos_PJ_9M,OutrosFinanciamentos_PJ_10M,OutrosFinanciamentos_PJ_11M)::float as max_OutrosFinanciamentos_pj,
	Ever_OutrosFinanciamentos_pj::float,
	count_OutrosFinanciamentos_pj::int,
	case when OutrosFinanciamentos_pj_10M < OutrosFinanciamentos_pj_11M then 1 else 0 end +
	case when OutrosFinanciamentos_pj_9M < OutrosFinanciamentos_pj_10M then 1 else 0 end +
	case when OutrosFinanciamentos_pj_8M < OutrosFinanciamentos_pj_9M then 1 else 0 end +
	case when OutrosFinanciamentos_pj_7M < OutrosFinanciamentos_pj_8M then 1 else 0 end +
	case when OutrosFinanciamentos_pj_6M < OutrosFinanciamentos_pj_7M then 1 else 0 end +
	case when OutrosFinanciamentos_pj_5M < OutrosFinanciamentos_pj_6M then 1 else 0 end +
	case when OutrosFinanciamentos_pj_4M < OutrosFinanciamentos_pj_5M then 1 else 0 end +
	case when OutrosFinanciamentos_pj_3M < OutrosFinanciamentos_pj_4M then 1 else 0 end +
	case when OutrosFinanciamentos_pj_2M < OutrosFinanciamentos_pj_3M then 1 else 0 end +
	case when OutrosFinanciamentos_pj_1M < OutrosFinanciamentos_pj_2M then 1 else 0 end +
	case when OutrosFinanciamentos_pj_Curr < OutrosFinanciamentos_pj_1M then 1 else 0 end::float as Meses_Reducao_OutrosFinanciamentos_pj,
	case when OutrosFinanciamentos_pj_10M > OutrosFinanciamentos_pj_11M then 1 else 0 end +
	case when OutrosFinanciamentos_pj_9M > OutrosFinanciamentos_pj_10M then 1 else 0 end +
	case when OutrosFinanciamentos_pj_8M > OutrosFinanciamentos_pj_9M then 1 else 0 end +
	case when OutrosFinanciamentos_pj_7M > OutrosFinanciamentos_pj_8M then 1 else 0 end +
	case when OutrosFinanciamentos_pj_6M > OutrosFinanciamentos_pj_7M then 1 else 0 end +
	case when OutrosFinanciamentos_pj_5M > OutrosFinanciamentos_pj_6M then 1 else 0 end +
	case when OutrosFinanciamentos_pj_4M > OutrosFinanciamentos_pj_5M then 1 else 0 end +
	case when OutrosFinanciamentos_pj_3M > OutrosFinanciamentos_pj_4M then 1 else 0 end +
	case when OutrosFinanciamentos_pj_2M > OutrosFinanciamentos_pj_3M then 1 else 0 end +
	case when OutrosFinanciamentos_pj_1M > OutrosFinanciamentos_pj_2M then 1 else 0 end +
	case when OutrosFinanciamentos_pj_Curr > OutrosFinanciamentos_pj_1M then 1 else 0 end::float as Meses_Aumento_OutrosFinanciamentos_pj,
	case when OutrosFinanciamentos_pj_1M is null then null else
	case when OutrosFinanciamentos_pj_Curr >= OutrosFinanciamentos_pj_1M or OutrosFinanciamentos_pj_1M is null then 0 else
	case when OutrosFinanciamentos_pj_1M >= OutrosFinanciamentos_pj_2M or OutrosFinanciamentos_pj_2M is null then 1 else
	case when OutrosFinanciamentos_pj_2M >= OutrosFinanciamentos_pj_3M or OutrosFinanciamentos_pj_3M is null then 2 else
	case when OutrosFinanciamentos_pj_3M >= OutrosFinanciamentos_pj_4M or OutrosFinanciamentos_pj_4M is null then 3 else
	case when OutrosFinanciamentos_pj_4M >= OutrosFinanciamentos_pj_5M or OutrosFinanciamentos_pj_5M is null then 4 else
	case when OutrosFinanciamentos_pj_5M >= OutrosFinanciamentos_pj_6M or OutrosFinanciamentos_pj_6M is null then 5 else
	case when OutrosFinanciamentos_pj_6M >= OutrosFinanciamentos_pj_7M or OutrosFinanciamentos_pj_7M is null then 6 else
	case when OutrosFinanciamentos_pj_7M >= OutrosFinanciamentos_pj_8M or OutrosFinanciamentos_pj_8M is null then 7 else
	case when OutrosFinanciamentos_pj_8M >= OutrosFinanciamentos_pj_9M or OutrosFinanciamentos_pj_9M is null then 8 else
	case when OutrosFinanciamentos_pj_9M >= OutrosFinanciamentos_pj_10M or OutrosFinanciamentos_pj_10M is null then 9 else
	case when OutrosFinanciamentos_pj_10M >= OutrosFinanciamentos_pj_11M or OutrosFinanciamentos_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_OutrosFinanciamentos_pj,
	case when OutrosFinanciamentos_pj_1M is null then null else
	case when OutrosFinanciamentos_pj_Curr <= OutrosFinanciamentos_pj_1M or OutrosFinanciamentos_pj_1M is null then 0 else
	case when OutrosFinanciamentos_pj_1M <= OutrosFinanciamentos_pj_2M or OutrosFinanciamentos_pj_2M is null then 1 else
	case when OutrosFinanciamentos_pj_2M <= OutrosFinanciamentos_pj_3M or OutrosFinanciamentos_pj_3M is null then 2 else
	case when OutrosFinanciamentos_pj_3M <= OutrosFinanciamentos_pj_4M or OutrosFinanciamentos_pj_4M is null then 3 else
	case when OutrosFinanciamentos_pj_4M <= OutrosFinanciamentos_pj_5M or OutrosFinanciamentos_pj_5M is null then 4 else
	case when OutrosFinanciamentos_pj_5M <= OutrosFinanciamentos_pj_6M or OutrosFinanciamentos_pj_6M is null then 5 else
	case when OutrosFinanciamentos_pj_6M <= OutrosFinanciamentos_pj_7M or OutrosFinanciamentos_pj_7M is null then 6 else
	case when OutrosFinanciamentos_pj_7M <= OutrosFinanciamentos_pj_8M or OutrosFinanciamentos_pj_8M is null then 7 else
	case when OutrosFinanciamentos_pj_8M <= OutrosFinanciamentos_pj_9M or OutrosFinanciamentos_pj_9M is null then 8 else
	case when OutrosFinanciamentos_pj_9M <= OutrosFinanciamentos_pj_10M or OutrosFinanciamentos_pj_10M is null then 9 else
	case when OutrosFinanciamentos_pj_10M <= OutrosFinanciamentos_pj_11M or OutrosFinanciamentos_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_OutrosFinanciamentos_pj,
	(case when OutrosFinanciamentos_PJ_10M < OutrosFinanciamentos_PJ_11M then OutrosFinanciamentos_PJ_10M - OutrosFinanciamentos_PJ_11M else 0 end +
	case when OutrosFinanciamentos_PJ_9M < OutrosFinanciamentos_PJ_10M then OutrosFinanciamentos_PJ_9M - OutrosFinanciamentos_PJ_10M else 0 end +
	case when OutrosFinanciamentos_PJ_8M < OutrosFinanciamentos_PJ_9M then OutrosFinanciamentos_PJ_8M - OutrosFinanciamentos_PJ_9M else 0 end +
	case when OutrosFinanciamentos_PJ_7M < OutrosFinanciamentos_PJ_8M then OutrosFinanciamentos_PJ_7M - OutrosFinanciamentos_PJ_8M else 0 end +
	case when OutrosFinanciamentos_PJ_6M < OutrosFinanciamentos_PJ_7M then OutrosFinanciamentos_PJ_6M - OutrosFinanciamentos_PJ_7M else 0 end +
	case when OutrosFinanciamentos_PJ_5M < OutrosFinanciamentos_PJ_6M then OutrosFinanciamentos_PJ_5M - OutrosFinanciamentos_PJ_6M else 0 end +
	case when OutrosFinanciamentos_PJ_4M < OutrosFinanciamentos_PJ_5M then OutrosFinanciamentos_PJ_4M - OutrosFinanciamentos_PJ_5M else 0 end +
	case when OutrosFinanciamentos_PJ_3M < OutrosFinanciamentos_PJ_4M then OutrosFinanciamentos_PJ_3M - OutrosFinanciamentos_PJ_4M else 0 end +
	case when OutrosFinanciamentos_PJ_2M < OutrosFinanciamentos_PJ_3M then OutrosFinanciamentos_PJ_2M - OutrosFinanciamentos_PJ_3M else 0 end +
	case when OutrosFinanciamentos_PJ_1M < OutrosFinanciamentos_PJ_2M then OutrosFinanciamentos_PJ_1M - OutrosFinanciamentos_PJ_2M else 0 end +
	case when OutrosFinanciamentos_PJ_Curr < OutrosFinanciamentos_PJ_1M then OutrosFinanciamentos_PJ_Curr - OutrosFinanciamentos_PJ_1M else 0 end)*(-1)::float as Saldo_Amort_OutrosFinanciamentos_PJ,
	(LimiteCredito_curto_PJ_Curr + LimiteCredito_longo_PJ_Curr)::float as LimiteCredito_PJ_Curr,
	(LimiteCredito_curto_PJ_1M + LimiteCredito_longo_PJ_1M)::float as LimiteCredito_PJ_1M,
	(LimiteCredito_curto_PJ_2M + LimiteCredito_longo_PJ_2M)::float as LimiteCredito_PJ_2M,
	(LimiteCredito_curto_PJ_3M + LimiteCredito_longo_PJ_3M)::float as LimiteCredito_PJ_3M,
	(LimiteCredito_curto_PJ_4M + LimiteCredito_longo_PJ_4M)::float as LimiteCredito_PJ_4M,
	(LimiteCredito_curto_PJ_5M + LimiteCredito_longo_PJ_5M)::float as LimiteCredito_PJ_5M,
	(LimiteCredito_curto_PJ_6M + LimiteCredito_longo_PJ_6M)::float as LimiteCredito_PJ_6M,
	(LimiteCredito_curto_PJ_7M + LimiteCredito_longo_PJ_7M)::float as LimiteCredito_PJ_7M,
	(LimiteCredito_curto_PJ_8M + LimiteCredito_longo_PJ_8M)::float as LimiteCredito_PJ_8M,
	(LimiteCredito_curto_PJ_9M + LimiteCredito_longo_PJ_9M)::float as LimiteCredito_PJ_9M,
	(LimiteCredito_curto_PJ_10M + LimiteCredito_longo_PJ_10M)::float as LimiteCredito_PJ_10M,
	(LimiteCredito_curto_PJ_11M + LimiteCredito_longo_PJ_11M)::float as LimiteCredito_PJ_11M,
	greatest((LimiteCredito_curto_PJ_Curr + LimiteCredito_longo_PJ_Curr),
		(LimiteCredito_curto_PJ_1M + LimiteCredito_longo_PJ_1M),
		(LimiteCredito_curto_PJ_2M + LimiteCredito_longo_PJ_2M),
		(LimiteCredito_curto_PJ_3M + LimiteCredito_longo_PJ_3M),
		(LimiteCredito_curto_PJ_4M + LimiteCredito_longo_PJ_4M),
		(LimiteCredito_curto_PJ_5M + LimiteCredito_longo_PJ_5M),
		(LimiteCredito_curto_PJ_6M + LimiteCredito_longo_PJ_6M),
		(LimiteCredito_curto_PJ_7M + LimiteCredito_longo_PJ_7M),
		(LimiteCredito_curto_PJ_8M + LimiteCredito_longo_PJ_8M),
		(LimiteCredito_curto_PJ_9M + LimiteCredito_longo_PJ_9M),
		(LimiteCredito_curto_PJ_10M + LimiteCredito_longo_PJ_10M),
		(LimiteCredito_curto_PJ_11M + LimiteCredito_longo_PJ_11M))::float as max_LimiteCredito_pj,
	(Ever_LimiteCredito_curto_pj + Ever_LimiteCredito_longo_pj)::float as Ever_LimiteCredito_pj,
	greatest(count_LimiteCredito_curto_pj,count_LimiteCredito_longo_pj)::int as count_LimiteCredito_pj,
	case when (LimiteCredito_curto_PJ_10M + LimiteCredito_longo_PJ_10M) < (LimiteCredito_curto_PJ_11M + LimiteCredito_longo_PJ_11M) then 1 else 0 end +
	case when (LimiteCredito_curto_PJ_9M + LimiteCredito_longo_PJ_9M) < (LimiteCredito_curto_PJ_10M + LimiteCredito_longo_PJ_10M) then 1 else 0 end +
	case when (LimiteCredito_curto_PJ_8M + LimiteCredito_longo_PJ_8M) < (LimiteCredito_curto_PJ_9M + LimiteCredito_longo_PJ_9M) then 1 else 0 end +
	case when (LimiteCredito_curto_PJ_7M + LimiteCredito_longo_PJ_7M) < (LimiteCredito_curto_PJ_8M + LimiteCredito_longo_PJ_8M) then 1 else 0 end +
	case when (LimiteCredito_curto_PJ_6M + LimiteCredito_longo_PJ_6M) < (LimiteCredito_curto_PJ_7M + LimiteCredito_longo_PJ_7M) then 1 else 0 end +
	case when (LimiteCredito_curto_PJ_5M + LimiteCredito_longo_PJ_5M) < (LimiteCredito_curto_PJ_6M + LimiteCredito_longo_PJ_6M) then 1 else 0 end +
	case when (LimiteCredito_curto_PJ_4M + LimiteCredito_longo_PJ_4M) < (LimiteCredito_curto_PJ_5M + LimiteCredito_longo_PJ_5M) then 1 else 0 end +
	case when (LimiteCredito_curto_PJ_3M + LimiteCredito_longo_PJ_3M) < (LimiteCredito_curto_PJ_4M + LimiteCredito_longo_PJ_4M) then 1 else 0 end +
	case when (LimiteCredito_curto_PJ_2M + LimiteCredito_longo_PJ_2M) < (LimiteCredito_curto_PJ_3M + LimiteCredito_longo_PJ_3M) then 1 else 0 end +
	case when (LimiteCredito_curto_PJ_1M + LimiteCredito_longo_PJ_1M) < (LimiteCredito_curto_PJ_2M + LimiteCredito_longo_PJ_2M) then 1 else 0 end +
	case when (LimiteCredito_curto_PJ_Curr + LimiteCredito_longo_PJ_Curr) < (LimiteCredito_curto_PJ_1M + LimiteCredito_longo_PJ_1M) then 1 else 0 end::float as Meses_Reducao_LimiteCredito_pj,
	case when (LimiteCredito_curto_PJ_10M + LimiteCredito_longo_PJ_10M) > (LimiteCredito_curto_PJ_11M + LimiteCredito_longo_PJ_11M) then 1 else 0 end +
	case when (LimiteCredito_curto_PJ_9M + LimiteCredito_longo_PJ_9M) > (LimiteCredito_curto_PJ_10M + LimiteCredito_longo_PJ_10M) then 1 else 0 end +
	case when (LimiteCredito_curto_PJ_8M + LimiteCredito_longo_PJ_8M) > (LimiteCredito_curto_PJ_9M + LimiteCredito_longo_PJ_9M) then 1 else 0 end +
	case when (LimiteCredito_curto_PJ_7M + LimiteCredito_longo_PJ_7M) > (LimiteCredito_curto_PJ_8M + LimiteCredito_longo_PJ_8M) then 1 else 0 end +
	case when (LimiteCredito_curto_PJ_6M + LimiteCredito_longo_PJ_6M) > (LimiteCredito_curto_PJ_7M + LimiteCredito_longo_PJ_7M) then 1 else 0 end +
	case when (LimiteCredito_curto_PJ_5M + LimiteCredito_longo_PJ_5M) > (LimiteCredito_curto_PJ_6M + LimiteCredito_longo_PJ_6M) then 1 else 0 end +
	case when (LimiteCredito_curto_PJ_4M + LimiteCredito_longo_PJ_4M) > (LimiteCredito_curto_PJ_5M + LimiteCredito_longo_PJ_5M) then 1 else 0 end +
	case when (LimiteCredito_curto_PJ_3M + LimiteCredito_longo_PJ_3M) > (LimiteCredito_curto_PJ_4M + LimiteCredito_longo_PJ_4M) then 1 else 0 end +
	case when (LimiteCredito_curto_PJ_2M + LimiteCredito_longo_PJ_2M) > (LimiteCredito_curto_PJ_3M + LimiteCredito_longo_PJ_3M) then 1 else 0 end +
	case when (LimiteCredito_curto_PJ_1M + LimiteCredito_longo_PJ_1M) > (LimiteCredito_curto_PJ_2M + LimiteCredito_longo_PJ_2M) then 1 else 0 end +
	case when (LimiteCredito_curto_PJ_Curr + LimiteCredito_longo_PJ_Curr) > (LimiteCredito_curto_PJ_1M + LimiteCredito_longo_PJ_1M) then 1 else 0 end::float as Meses_Aumento_LimiteCredito_pj,
	case when (LimiteCredito_curto_PJ_1M + LimiteCredito_longo_PJ_1M) is null then null else
		case when (LimiteCredito_curto_PJ_Curr + LimiteCredito_longo_PJ_Curr) >= (LimiteCredito_curto_PJ_1M + LimiteCredito_longo_PJ_1M) or (LimiteCredito_curto_PJ_1M + LimiteCredito_longo_PJ_1M) is null then 0 else
		case when (LimiteCredito_curto_PJ_1M + LimiteCredito_longo_PJ_1M) >= (LimiteCredito_curto_PJ_2M + LimiteCredito_longo_PJ_2M) or (LimiteCredito_curto_PJ_2M + LimiteCredito_longo_PJ_2M) is null then 1 else
		case when (LimiteCredito_curto_PJ_2M + LimiteCredito_longo_PJ_2M) >= (LimiteCredito_curto_PJ_3M + LimiteCredito_longo_PJ_3M) or (LimiteCredito_curto_PJ_3M + LimiteCredito_longo_PJ_3M) is null then 2 else
		case when (LimiteCredito_curto_PJ_3M + LimiteCredito_longo_PJ_3M) >= (LimiteCredito_curto_PJ_4M + LimiteCredito_longo_PJ_4M) or (LimiteCredito_curto_PJ_4M + LimiteCredito_longo_PJ_4M) is null then 3 else
		case when (LimiteCredito_curto_PJ_4M + LimiteCredito_longo_PJ_4M) >= (LimiteCredito_curto_PJ_5M + LimiteCredito_longo_PJ_5M) or (LimiteCredito_curto_PJ_5M + LimiteCredito_longo_PJ_5M) is null then 4 else
		case when (LimiteCredito_curto_PJ_5M + LimiteCredito_longo_PJ_5M) >= (LimiteCredito_curto_PJ_6M + LimiteCredito_longo_PJ_6M) or (LimiteCredito_curto_PJ_6M + LimiteCredito_longo_PJ_6M) is null then 5 else
		case when (LimiteCredito_curto_PJ_6M + LimiteCredito_longo_PJ_6M) >= (LimiteCredito_curto_PJ_7M + LimiteCredito_longo_PJ_7M) or (LimiteCredito_curto_PJ_7M + LimiteCredito_longo_PJ_7M) is null then 6 else
		case when (LimiteCredito_curto_PJ_7M + LimiteCredito_longo_PJ_7M) >= (LimiteCredito_curto_PJ_8M + LimiteCredito_longo_PJ_8M) or (LimiteCredito_curto_PJ_8M + LimiteCredito_longo_PJ_8M) is null then 7 else
		case when (LimiteCredito_curto_PJ_8M + LimiteCredito_longo_PJ_8M) >= (LimiteCredito_curto_PJ_9M + LimiteCredito_longo_PJ_9M) or (LimiteCredito_curto_PJ_9M + LimiteCredito_longo_PJ_9M) is null then 8 else
		case when (LimiteCredito_curto_PJ_9M + LimiteCredito_longo_PJ_9M) >= (LimiteCredito_curto_PJ_10M + LimiteCredito_longo_PJ_10M) or (LimiteCredito_curto_PJ_10M + LimiteCredito_longo_PJ_10M) is null then 9 else
		case when (LimiteCredito_curto_PJ_10M + LimiteCredito_longo_PJ_10M) >= (LimiteCredito_curto_PJ_11M + LimiteCredito_longo_PJ_11M) or (LimiteCredito_curto_PJ_11M + LimiteCredito_longo_PJ_11M) is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_LimiteCredito_pj,
	case when (LimiteCredito_curto_PJ_1M + LimiteCredito_longo_PJ_1M) is null then null else
		case when (LimiteCredito_curto_PJ_Curr + LimiteCredito_longo_PJ_Curr) <= (LimiteCredito_curto_PJ_1M + LimiteCredito_longo_PJ_1M) or (LimiteCredito_curto_PJ_1M + LimiteCredito_longo_PJ_1M) is null then 0 else
		case when (LimiteCredito_curto_PJ_1M + LimiteCredito_longo_PJ_1M) <= (LimiteCredito_curto_PJ_2M + LimiteCredito_longo_PJ_2M) or (LimiteCredito_curto_PJ_2M + LimiteCredito_longo_PJ_2M) is null then 1 else
		case when (LimiteCredito_curto_PJ_2M + LimiteCredito_longo_PJ_2M) <= (LimiteCredito_curto_PJ_3M + LimiteCredito_longo_PJ_3M) or (LimiteCredito_curto_PJ_3M + LimiteCredito_longo_PJ_3M) is null then 2 else
		case when (LimiteCredito_curto_PJ_3M + LimiteCredito_longo_PJ_3M) <= (LimiteCredito_curto_PJ_4M + LimiteCredito_longo_PJ_4M) or (LimiteCredito_curto_PJ_4M + LimiteCredito_longo_PJ_4M) is null then 3 else
		case when (LimiteCredito_curto_PJ_4M + LimiteCredito_longo_PJ_4M) <= (LimiteCredito_curto_PJ_5M + LimiteCredito_longo_PJ_5M) or (LimiteCredito_curto_PJ_5M + LimiteCredito_longo_PJ_5M) is null then 4 else
		case when (LimiteCredito_curto_PJ_5M + LimiteCredito_longo_PJ_5M) <= (LimiteCredito_curto_PJ_6M + LimiteCredito_longo_PJ_6M) or (LimiteCredito_curto_PJ_6M + LimiteCredito_longo_PJ_6M) is null then 5 else
		case when (LimiteCredito_curto_PJ_6M + LimiteCredito_longo_PJ_6M) <= (LimiteCredito_curto_PJ_7M + LimiteCredito_longo_PJ_7M) or (LimiteCredito_curto_PJ_7M + LimiteCredito_longo_PJ_7M) is null then 6 else
		case when (LimiteCredito_curto_PJ_7M + LimiteCredito_longo_PJ_7M) <= (LimiteCredito_curto_PJ_8M + LimiteCredito_longo_PJ_8M) or (LimiteCredito_curto_PJ_8M + LimiteCredito_longo_PJ_8M) is null then 7 else
		case when (LimiteCredito_curto_PJ_8M + LimiteCredito_longo_PJ_8M) <= (LimiteCredito_curto_PJ_9M + LimiteCredito_longo_PJ_9M) or (LimiteCredito_curto_PJ_9M + LimiteCredito_longo_PJ_9M) is null then 8 else
		case when (LimiteCredito_curto_PJ_9M + LimiteCredito_longo_PJ_9M) <= (LimiteCredito_curto_PJ_10M + LimiteCredito_longo_PJ_10M) or (LimiteCredito_curto_PJ_10M + LimiteCredito_longo_PJ_10M) is null then 9 else
		case when (LimiteCredito_curto_PJ_10M + LimiteCredito_longo_PJ_10M) <= (LimiteCredito_curto_PJ_11M + LimiteCredito_longo_PJ_11M) or (LimiteCredito_curto_PJ_11M + LimiteCredito_longo_PJ_11M) is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_LimiteCredito_pj,
	LimiteCredito_curto_PJ_Curr::float,
	LimiteCredito_curto_PJ_1M::float,
	LimiteCredito_curto_PJ_2M::float,
	LimiteCredito_curto_PJ_3M::float,
	LimiteCredito_curto_PJ_4M::float,
	LimiteCredito_curto_PJ_5M::float,
	LimiteCredito_curto_PJ_6M::float,
	LimiteCredito_curto_PJ_7M::float,
	LimiteCredito_curto_PJ_8M::float,
	LimiteCredito_curto_PJ_9M::float,
	LimiteCredito_curto_PJ_10M::float,
	LimiteCredito_curto_PJ_11M::float,
	greatest(LimiteCredito_curto_PJ_Curr,LimiteCredito_curto_PJ_1M,LimiteCredito_curto_PJ_2M,LimiteCredito_curto_PJ_3M,LimiteCredito_curto_PJ_4M,LimiteCredito_curto_PJ_5M,LimiteCredito_curto_PJ_6M,LimiteCredito_curto_PJ_7M,LimiteCredito_curto_PJ_8M,LimiteCredito_curto_PJ_9M,LimiteCredito_curto_PJ_10M,LimiteCredito_curto_PJ_11M)::float as max_LimiteCredito_curto_pj,
	Ever_LimiteCredito_curto_pj::float,
	count_LimiteCredito_curto_pj::int,
	case when LimiteCredito_curto_pj_10M < LimiteCredito_curto_pj_11M then 1 else 0 end +
	case when LimiteCredito_curto_pj_9M < LimiteCredito_curto_pj_10M then 1 else 0 end +
	case when LimiteCredito_curto_pj_8M < LimiteCredito_curto_pj_9M then 1 else 0 end +
	case when LimiteCredito_curto_pj_7M < LimiteCredito_curto_pj_8M then 1 else 0 end +
	case when LimiteCredito_curto_pj_6M < LimiteCredito_curto_pj_7M then 1 else 0 end +
	case when LimiteCredito_curto_pj_5M < LimiteCredito_curto_pj_6M then 1 else 0 end +
	case when LimiteCredito_curto_pj_4M < LimiteCredito_curto_pj_5M then 1 else 0 end +
	case when LimiteCredito_curto_pj_3M < LimiteCredito_curto_pj_4M then 1 else 0 end +
	case when LimiteCredito_curto_pj_2M < LimiteCredito_curto_pj_3M then 1 else 0 end +
	case when LimiteCredito_curto_pj_1M < LimiteCredito_curto_pj_2M then 1 else 0 end +
	case when LimiteCredito_curto_pj_Curr < LimiteCredito_curto_pj_1M then 1 else 0 end::float as Meses_Reducao_LimiteCredito_curto_pj,
	case when LimiteCredito_curto_pj_10M > LimiteCredito_curto_pj_11M then 1 else 0 end +
	case when LimiteCredito_curto_pj_9M > LimiteCredito_curto_pj_10M then 1 else 0 end +
	case when LimiteCredito_curto_pj_8M > LimiteCredito_curto_pj_9M then 1 else 0 end +
	case when LimiteCredito_curto_pj_7M > LimiteCredito_curto_pj_8M then 1 else 0 end +
	case when LimiteCredito_curto_pj_6M > LimiteCredito_curto_pj_7M then 1 else 0 end +
	case when LimiteCredito_curto_pj_5M > LimiteCredito_curto_pj_6M then 1 else 0 end +
	case when LimiteCredito_curto_pj_4M > LimiteCredito_curto_pj_5M then 1 else 0 end +
	case when LimiteCredito_curto_pj_3M > LimiteCredito_curto_pj_4M then 1 else 0 end +
	case when LimiteCredito_curto_pj_2M > LimiteCredito_curto_pj_3M then 1 else 0 end +
	case when LimiteCredito_curto_pj_1M > LimiteCredito_curto_pj_2M then 1 else 0 end +
	case when LimiteCredito_curto_pj_Curr > LimiteCredito_curto_pj_1M then 1 else 0 end::float as Meses_Aumento_LimiteCredito_curto_pj,
	case when LimiteCredito_curto_pj_1M is null then null else
	case when LimiteCredito_curto_pj_Curr >= LimiteCredito_curto_pj_1M or LimiteCredito_curto_pj_1M is null then 0 else
	case when LimiteCredito_curto_pj_1M >= LimiteCredito_curto_pj_2M or LimiteCredito_curto_pj_2M is null then 1 else
	case when LimiteCredito_curto_pj_2M >= LimiteCredito_curto_pj_3M or LimiteCredito_curto_pj_3M is null then 2 else
	case when LimiteCredito_curto_pj_3M >= LimiteCredito_curto_pj_4M or LimiteCredito_curto_pj_4M is null then 3 else
	case when LimiteCredito_curto_pj_4M >= LimiteCredito_curto_pj_5M or LimiteCredito_curto_pj_5M is null then 4 else
	case when LimiteCredito_curto_pj_5M >= LimiteCredito_curto_pj_6M or LimiteCredito_curto_pj_6M is null then 5 else
	case when LimiteCredito_curto_pj_6M >= LimiteCredito_curto_pj_7M or LimiteCredito_curto_pj_7M is null then 6 else
	case when LimiteCredito_curto_pj_7M >= LimiteCredito_curto_pj_8M or LimiteCredito_curto_pj_8M is null then 7 else
	case when LimiteCredito_curto_pj_8M >= LimiteCredito_curto_pj_9M or LimiteCredito_curto_pj_9M is null then 8 else
	case when LimiteCredito_curto_pj_9M >= LimiteCredito_curto_pj_10M or LimiteCredito_curto_pj_10M is null then 9 else
	case when LimiteCredito_curto_pj_10M >= LimiteCredito_curto_pj_11M or LimiteCredito_curto_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_LimiteCredito_curto_pj,
	case when LimiteCredito_curto_pj_1M is null then null else
	case when LimiteCredito_curto_pj_Curr <= LimiteCredito_curto_pj_1M or LimiteCredito_curto_pj_1M is null then 0 else
	case when LimiteCredito_curto_pj_1M <= LimiteCredito_curto_pj_2M or LimiteCredito_curto_pj_2M is null then 1 else
	case when LimiteCredito_curto_pj_2M <= LimiteCredito_curto_pj_3M or LimiteCredito_curto_pj_3M is null then 2 else
	case when LimiteCredito_curto_pj_3M <= LimiteCredito_curto_pj_4M or LimiteCredito_curto_pj_4M is null then 3 else
	case when LimiteCredito_curto_pj_4M <= LimiteCredito_curto_pj_5M or LimiteCredito_curto_pj_5M is null then 4 else
	case when LimiteCredito_curto_pj_5M <= LimiteCredito_curto_pj_6M or LimiteCredito_curto_pj_6M is null then 5 else
	case when LimiteCredito_curto_pj_6M <= LimiteCredito_curto_pj_7M or LimiteCredito_curto_pj_7M is null then 6 else
	case when LimiteCredito_curto_pj_7M <= LimiteCredito_curto_pj_8M or LimiteCredito_curto_pj_8M is null then 7 else
	case when LimiteCredito_curto_pj_8M <= LimiteCredito_curto_pj_9M or LimiteCredito_curto_pj_9M is null then 8 else
	case when LimiteCredito_curto_pj_9M <= LimiteCredito_curto_pj_10M or LimiteCredito_curto_pj_10M is null then 9 else
	case when LimiteCredito_curto_pj_10M <= LimiteCredito_curto_pj_11M or LimiteCredito_curto_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_LimiteCredito_curto_pj,
	LimiteCredito_longo_PJ_Curr::float,
	LimiteCredito_longo_PJ_1M::float,
	LimiteCredito_longo_PJ_2M::float,
	LimiteCredito_longo_PJ_3M::float,
	LimiteCredito_longo_PJ_4M::float,
	LimiteCredito_longo_PJ_5M::float,
	LimiteCredito_longo_PJ_6M::float,
	LimiteCredito_longo_PJ_7M::float,
	LimiteCredito_longo_PJ_8M::float,
	LimiteCredito_longo_PJ_9M::float,
	LimiteCredito_longo_PJ_10M::float,
	LimiteCredito_longo_PJ_11M::float,
	greatest(LimiteCredito_longo_PJ_Curr,LimiteCredito_longo_PJ_1M,LimiteCredito_longo_PJ_2M,LimiteCredito_longo_PJ_3M,LimiteCredito_longo_PJ_4M,LimiteCredito_longo_PJ_5M,LimiteCredito_longo_PJ_6M,LimiteCredito_longo_PJ_7M,LimiteCredito_longo_PJ_8M,LimiteCredito_longo_PJ_9M,LimiteCredito_longo_PJ_10M,LimiteCredito_longo_PJ_11M)::float as max_LimiteCredito_longo_pj,
	Ever_LimiteCredito_longo_pj::float,
	count_LimiteCredito_longo_pj::int,
	case when LimiteCredito_longo_pj_10M < LimiteCredito_longo_pj_11M then 1 else 0 end +
	case when LimiteCredito_longo_pj_9M < LimiteCredito_longo_pj_10M then 1 else 0 end +
	case when LimiteCredito_longo_pj_8M < LimiteCredito_longo_pj_9M then 1 else 0 end +
	case when LimiteCredito_longo_pj_7M < LimiteCredito_longo_pj_8M then 1 else 0 end +
	case when LimiteCredito_longo_pj_6M < LimiteCredito_longo_pj_7M then 1 else 0 end +
	case when LimiteCredito_longo_pj_5M < LimiteCredito_longo_pj_6M then 1 else 0 end +
	case when LimiteCredito_longo_pj_4M < LimiteCredito_longo_pj_5M then 1 else 0 end +
	case when LimiteCredito_longo_pj_3M < LimiteCredito_longo_pj_4M then 1 else 0 end +
	case when LimiteCredito_longo_pj_2M < LimiteCredito_longo_pj_3M then 1 else 0 end +
	case when LimiteCredito_longo_pj_1M < LimiteCredito_longo_pj_2M then 1 else 0 end +
	case when LimiteCredito_longo_pj_Curr < LimiteCredito_longo_pj_1M then 1 else 0 end::float as Meses_Reducao_LimiteCredito_longo_pj,
	case when LimiteCredito_longo_pj_10M > LimiteCredito_longo_pj_11M then 1 else 0 end +
	case when LimiteCredito_longo_pj_9M > LimiteCredito_longo_pj_10M then 1 else 0 end +
	case when LimiteCredito_longo_pj_8M > LimiteCredito_longo_pj_9M then 1 else 0 end +
	case when LimiteCredito_longo_pj_7M > LimiteCredito_longo_pj_8M then 1 else 0 end +
	case when LimiteCredito_longo_pj_6M > LimiteCredito_longo_pj_7M then 1 else 0 end +
	case when LimiteCredito_longo_pj_5M > LimiteCredito_longo_pj_6M then 1 else 0 end +
	case when LimiteCredito_longo_pj_4M > LimiteCredito_longo_pj_5M then 1 else 0 end +
	case when LimiteCredito_longo_pj_3M > LimiteCredito_longo_pj_4M then 1 else 0 end +
	case when LimiteCredito_longo_pj_2M > LimiteCredito_longo_pj_3M then 1 else 0 end +
	case when LimiteCredito_longo_pj_1M > LimiteCredito_longo_pj_2M then 1 else 0 end +
	case when LimiteCredito_longo_pj_Curr > LimiteCredito_longo_pj_1M then 1 else 0 end::float as Meses_Aumento_LimiteCredito_longo_pj,
	case when LimiteCredito_longo_pj_1M is null then null else
	case when LimiteCredito_longo_pj_Curr >= LimiteCredito_longo_pj_1M or LimiteCredito_longo_pj_1M is null then 0 else
	case when LimiteCredito_longo_pj_1M >= LimiteCredito_longo_pj_2M or LimiteCredito_longo_pj_2M is null then 1 else
	case when LimiteCredito_longo_pj_2M >= LimiteCredito_longo_pj_3M or LimiteCredito_longo_pj_3M is null then 2 else
	case when LimiteCredito_longo_pj_3M >= LimiteCredito_longo_pj_4M or LimiteCredito_longo_pj_4M is null then 3 else
	case when LimiteCredito_longo_pj_4M >= LimiteCredito_longo_pj_5M or LimiteCredito_longo_pj_5M is null then 4 else
	case when LimiteCredito_longo_pj_5M >= LimiteCredito_longo_pj_6M or LimiteCredito_longo_pj_6M is null then 5 else
	case when LimiteCredito_longo_pj_6M >= LimiteCredito_longo_pj_7M or LimiteCredito_longo_pj_7M is null then 6 else
	case when LimiteCredito_longo_pj_7M >= LimiteCredito_longo_pj_8M or LimiteCredito_longo_pj_8M is null then 7 else
	case when LimiteCredito_longo_pj_8M >= LimiteCredito_longo_pj_9M or LimiteCredito_longo_pj_9M is null then 8 else
	case when LimiteCredito_longo_pj_9M >= LimiteCredito_longo_pj_10M or LimiteCredito_longo_pj_10M is null then 9 else
	case when LimiteCredito_longo_pj_10M >= LimiteCredito_longo_pj_11M or LimiteCredito_longo_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_LimiteCredito_longo_pj,
	case when LimiteCredito_longo_pj_1M is null then null else
	case when LimiteCredito_longo_pj_Curr <= LimiteCredito_longo_pj_1M or LimiteCredito_longo_pj_1M is null then 0 else
	case when LimiteCredito_longo_pj_1M <= LimiteCredito_longo_pj_2M or LimiteCredito_longo_pj_2M is null then 1 else
	case when LimiteCredito_longo_pj_2M <= LimiteCredito_longo_pj_3M or LimiteCredito_longo_pj_3M is null then 2 else
	case when LimiteCredito_longo_pj_3M <= LimiteCredito_longo_pj_4M or LimiteCredito_longo_pj_4M is null then 3 else
	case when LimiteCredito_longo_pj_4M <= LimiteCredito_longo_pj_5M or LimiteCredito_longo_pj_5M is null then 4 else
	case when LimiteCredito_longo_pj_5M <= LimiteCredito_longo_pj_6M or LimiteCredito_longo_pj_6M is null then 5 else
	case when LimiteCredito_longo_pj_6M <= LimiteCredito_longo_pj_7M or LimiteCredito_longo_pj_7M is null then 6 else
	case when LimiteCredito_longo_pj_7M <= LimiteCredito_longo_pj_8M or LimiteCredito_longo_pj_8M is null then 7 else
	case when LimiteCredito_longo_pj_8M <= LimiteCredito_longo_pj_9M or LimiteCredito_longo_pj_9M is null then 8 else
	case when LimiteCredito_longo_pj_9M <= LimiteCredito_longo_pj_10M or LimiteCredito_longo_pj_10M is null then 9 else
	case when LimiteCredito_longo_pj_10M <= LimiteCredito_longo_pj_11M or LimiteCredito_longo_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_LimiteCredito_longo_pj,
	Prejuizo_PJ_Curr::float,
	Prejuizo_PJ_1M::float,
	Prejuizo_PJ_2M::float,
	Prejuizo_PJ_3M::float,
	Prejuizo_PJ_4M::float,
	Prejuizo_PJ_5M::float,
	Prejuizo_PJ_6M::float,
	Prejuizo_PJ_7M::float,
	Prejuizo_PJ_8M::float,
	Prejuizo_PJ_9M::float,
	Prejuizo_PJ_10M::float,
	Prejuizo_PJ_11M::float,
	greatest(Prejuizo_PJ_Curr,Prejuizo_PJ_1M,Prejuizo_PJ_2M,Prejuizo_PJ_3M,Prejuizo_PJ_4M,Prejuizo_PJ_5M,Prejuizo_PJ_6M,Prejuizo_PJ_7M,Prejuizo_PJ_8M,Prejuizo_PJ_9M,Prejuizo_PJ_10M,Prejuizo_PJ_11M)::float as max_Prejuizo_pj,
	case when Prejuizo_PJ_Curr > 0 then 1 else 0 end + case when Prejuizo_PJ_1M > 0 then 1 else 0 end + case when Prejuizo_PJ_2M > 0 then 1 else 0 end + case when Prejuizo_PJ_3M > 0 then 1 else 0 end + case when Prejuizo_PJ_4M > 0 then 1 else 0 end + case when Prejuizo_PJ_5M > 0 then 1 else 0 end + case when Prejuizo_PJ_6M > 0 then 1 else 0 end + case when Prejuizo_PJ_7M > 0 then 1 else 0 end + case when Prejuizo_PJ_8M > 0 then 1 else 0 end + case when Prejuizo_PJ_9M > 0 then 1 else 0 end + case when Prejuizo_PJ_10M > 0 then 1 else 0 end + case when Prejuizo_PJ_11M > 0 then 1 else 0 end as Meses_Prejuizo_PJ,
	case when Prejuizo_PJ_Curr > 0 then 0 when Prejuizo_PJ_1M > 0 then 1 when Prejuizo_PJ_2M > 0 then 2 when Prejuizo_PJ_3M > 0 then 3 when Prejuizo_PJ_4M > 0 then 4 when Prejuizo_PJ_5M > 0 then 5 when Prejuizo_PJ_6M > 0 then 6 when Prejuizo_PJ_7M > 0 then 7 when Prejuizo_PJ_8M > 0 then 8 when Prejuizo_PJ_9M > 0 then 9 when Prejuizo_PJ_10M > 0 then 10 when Prejuizo_PJ_11M > 0 then 11 else -1 end as Meses_Desde_Ultimo_Prejuizo_PJ,	
	Ever_Prejuizo_pj::float,
	count_Prejuizo_pj::int,
	CarteiraCredito_PJ_Curr::float,
	CarteiraCredito_PJ_1M::float,
	CarteiraCredito_PJ_2M::float,
	CarteiraCredito_PJ_3M::float,
	CarteiraCredito_PJ_4M::float,
	CarteiraCredito_PJ_5M::float,
	CarteiraCredito_PJ_6M::float,
	CarteiraCredito_PJ_7M::float,
	CarteiraCredito_PJ_8M::float,
	CarteiraCredito_PJ_9M::float,
	CarteiraCredito_PJ_10M::float,
	CarteiraCredito_PJ_11M::float,
	greatest(CarteiraCredito_PJ_Curr,CarteiraCredito_PJ_1M,CarteiraCredito_PJ_2M,CarteiraCredito_PJ_3M,CarteiraCredito_PJ_4M,CarteiraCredito_PJ_5M,CarteiraCredito_PJ_6M,CarteiraCredito_PJ_7M,CarteiraCredito_PJ_8M,CarteiraCredito_PJ_9M,CarteiraCredito_PJ_10M,CarteiraCredito_PJ_11M)::float as max_CarteiraCredito_pj,
	Ever_CarteiraCredito_pj::float,
	count_CarteiraCredito_pj::int,
	case when CarteiraCredito_pj_10M < CarteiraCredito_pj_11M then 1 else 0 end +
	case when CarteiraCredito_pj_9M < CarteiraCredito_pj_10M then 1 else 0 end +
	case when CarteiraCredito_pj_8M < CarteiraCredito_pj_9M then 1 else 0 end +
	case when CarteiraCredito_pj_7M < CarteiraCredito_pj_8M then 1 else 0 end +
	case when CarteiraCredito_pj_6M < CarteiraCredito_pj_7M then 1 else 0 end +
	case when CarteiraCredito_pj_5M < CarteiraCredito_pj_6M then 1 else 0 end +
	case when CarteiraCredito_pj_4M < CarteiraCredito_pj_5M then 1 else 0 end +
	case when CarteiraCredito_pj_3M < CarteiraCredito_pj_4M then 1 else 0 end +
	case when CarteiraCredito_pj_2M < CarteiraCredito_pj_3M then 1 else 0 end +
	case when CarteiraCredito_pj_1M < CarteiraCredito_pj_2M then 1 else 0 end +
	case when CarteiraCredito_pj_Curr < CarteiraCredito_pj_1M then 1 else 0 end::float as Meses_Reducao_CarteiraCredito_pj,
	case when CarteiraCredito_pj_10M > CarteiraCredito_pj_11M then 1 else 0 end +
	case when CarteiraCredito_pj_9M > CarteiraCredito_pj_10M then 1 else 0 end +
	case when CarteiraCredito_pj_8M > CarteiraCredito_pj_9M then 1 else 0 end +
	case when CarteiraCredito_pj_7M > CarteiraCredito_pj_8M then 1 else 0 end +
	case when CarteiraCredito_pj_6M > CarteiraCredito_pj_7M then 1 else 0 end +
	case when CarteiraCredito_pj_5M > CarteiraCredito_pj_6M then 1 else 0 end +
	case when CarteiraCredito_pj_4M > CarteiraCredito_pj_5M then 1 else 0 end +
	case when CarteiraCredito_pj_3M > CarteiraCredito_pj_4M then 1 else 0 end +
	case when CarteiraCredito_pj_2M > CarteiraCredito_pj_3M then 1 else 0 end +
	case when CarteiraCredito_pj_1M > CarteiraCredito_pj_2M then 1 else 0 end +
	case when CarteiraCredito_pj_Curr > CarteiraCredito_pj_1M then 1 else 0 end::float as Meses_Aumento_CarteiraCredito_pj,
	case when CarteiraCredito_pj_1M is null then null else
	case when CarteiraCredito_pj_Curr >= CarteiraCredito_pj_1M or CarteiraCredito_pj_1M is null then 0 else
	case when CarteiraCredito_pj_1M >= CarteiraCredito_pj_2M or CarteiraCredito_pj_2M is null then 1 else
	case when CarteiraCredito_pj_2M >= CarteiraCredito_pj_3M or CarteiraCredito_pj_3M is null then 2 else
	case when CarteiraCredito_pj_3M >= CarteiraCredito_pj_4M or CarteiraCredito_pj_4M is null then 3 else
	case when CarteiraCredito_pj_4M >= CarteiraCredito_pj_5M or CarteiraCredito_pj_5M is null then 4 else
	case when CarteiraCredito_pj_5M >= CarteiraCredito_pj_6M or CarteiraCredito_pj_6M is null then 5 else
	case when CarteiraCredito_pj_6M >= CarteiraCredito_pj_7M or CarteiraCredito_pj_7M is null then 6 else
	case when CarteiraCredito_pj_7M >= CarteiraCredito_pj_8M or CarteiraCredito_pj_8M is null then 7 else
	case when CarteiraCredito_pj_8M >= CarteiraCredito_pj_9M or CarteiraCredito_pj_9M is null then 8 else
	case when CarteiraCredito_pj_9M >= CarteiraCredito_pj_10M or CarteiraCredito_pj_10M is null then 9 else
	case when CarteiraCredito_pj_10M >= CarteiraCredito_pj_11M or CarteiraCredito_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_CarteiraCredito_pj,
	case when CarteiraCredito_pj_1M is null then null else
	case when CarteiraCredito_pj_Curr <= CarteiraCredito_pj_1M or CarteiraCredito_pj_1M is null then 0 else
	case when CarteiraCredito_pj_1M <= CarteiraCredito_pj_2M or CarteiraCredito_pj_2M is null then 1 else
	case when CarteiraCredito_pj_2M <= CarteiraCredito_pj_3M or CarteiraCredito_pj_3M is null then 2 else
	case when CarteiraCredito_pj_3M <= CarteiraCredito_pj_4M or CarteiraCredito_pj_4M is null then 3 else
	case when CarteiraCredito_pj_4M <= CarteiraCredito_pj_5M or CarteiraCredito_pj_5M is null then 4 else
	case when CarteiraCredito_pj_5M <= CarteiraCredito_pj_6M or CarteiraCredito_pj_6M is null then 5 else
	case when CarteiraCredito_pj_6M <= CarteiraCredito_pj_7M or CarteiraCredito_pj_7M is null then 6 else
	case when CarteiraCredito_pj_7M <= CarteiraCredito_pj_8M or CarteiraCredito_pj_8M is null then 7 else
	case when CarteiraCredito_pj_8M <= CarteiraCredito_pj_9M or CarteiraCredito_pj_9M is null then 8 else
	case when CarteiraCredito_pj_9M <= CarteiraCredito_pj_10M or CarteiraCredito_pj_10M is null then 9 else
	case when CarteiraCredito_pj_10M <= CarteiraCredito_pj_11M or CarteiraCredito_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_CarteiraCredito_pj,
	(case when CarteiraCredito_PJ_10M < CarteiraCredito_PJ_11M then CarteiraCredito_PJ_10M - CarteiraCredito_PJ_11M else 0 end +
	case when CarteiraCredito_PJ_9M < CarteiraCredito_PJ_10M then CarteiraCredito_PJ_9M - CarteiraCredito_PJ_10M else 0 end +
	case when CarteiraCredito_PJ_8M < CarteiraCredito_PJ_9M then CarteiraCredito_PJ_8M - CarteiraCredito_PJ_9M else 0 end +
	case when CarteiraCredito_PJ_7M < CarteiraCredito_PJ_8M then CarteiraCredito_PJ_7M - CarteiraCredito_PJ_8M else 0 end +
	case when CarteiraCredito_PJ_6M < CarteiraCredito_PJ_7M then CarteiraCredito_PJ_6M - CarteiraCredito_PJ_7M else 0 end +
	case when CarteiraCredito_PJ_5M < CarteiraCredito_PJ_6M then CarteiraCredito_PJ_5M - CarteiraCredito_PJ_6M else 0 end +
	case when CarteiraCredito_PJ_4M < CarteiraCredito_PJ_5M then CarteiraCredito_PJ_4M - CarteiraCredito_PJ_5M else 0 end +
	case when CarteiraCredito_PJ_3M < CarteiraCredito_PJ_4M then CarteiraCredito_PJ_3M - CarteiraCredito_PJ_4M else 0 end +
	case when CarteiraCredito_PJ_2M < CarteiraCredito_PJ_3M then CarteiraCredito_PJ_2M - CarteiraCredito_PJ_3M else 0 end +
	case when CarteiraCredito_PJ_1M < CarteiraCredito_PJ_2M then CarteiraCredito_PJ_1M - CarteiraCredito_PJ_2M else 0 end +
	case when CarteiraCredito_PJ_Curr < CarteiraCredito_PJ_1M then CarteiraCredito_PJ_Curr - CarteiraCredito_PJ_1M else 0 end)*(-1)::float as Saldo_Amort_CarteiraCredito_PJ
from(select direct_prospect_id,
	divida_atual_pj,
	count(distinct to_date) filter (where to_date <= data_referencia) as qtd_meses_modalidade_pj,
	--------------------------------EMPRESTIMOS
	sum(valor) filter (where to_date = data_referencia and lv1 = 'EmprÃ©stimos') as emprestimos_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PJ_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos') as Ever_emprestimos_pj,
	count(distinct to_date) filter (where valor > 0 and to_date <= data_referencia and lv1 = 'EmprÃ©stimos') as count_emprestimos_pj,
	--------------------------------EMPRESTIMOS -> CONTA GARANTIDA + CHEQUE ESPECIAL
	sum(valor) filter (where to_date = data_referencia and lv1 = 'EmprÃ©stimos' and lv2 in ('Conta Garantida','Cheque Especial')) as emprestimos_conta_garantida_cheque_especial_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'EmprÃ©stimos' and lv2 in ('Conta Garantida','Cheque Especial')) as emprestimos_conta_garantida_cheque_especial_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'EmprÃ©stimos' and lv2 in ('Conta Garantida','Cheque Especial')) as emprestimos_conta_garantida_cheque_especial_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'EmprÃ©stimos' and lv2 in ('Conta Garantida','Cheque Especial')) as emprestimos_conta_garantida_cheque_especial_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'EmprÃ©stimos' and lv2 in ('Conta Garantida','Cheque Especial')) as emprestimos_conta_garantida_cheque_especial_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'EmprÃ©stimos' and lv2 in ('Conta Garantida','Cheque Especial')) as emprestimos_conta_garantida_cheque_especial_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'EmprÃ©stimos' and lv2 in ('Conta Garantida','Cheque Especial')) as emprestimos_conta_garantida_cheque_especial_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'EmprÃ©stimos' and lv2 in ('Conta Garantida','Cheque Especial')) as emprestimos_conta_garantida_cheque_especial_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'EmprÃ©stimos' and lv2 in ('Conta Garantida','Cheque Especial')) as emprestimos_conta_garantida_cheque_especial_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'EmprÃ©stimos' and lv2 in ('Conta Garantida','Cheque Especial')) as emprestimos_conta_garantida_cheque_especial_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'EmprÃ©stimos' and lv2 in ('Conta Garantida','Cheque Especial')) as emprestimos_conta_garantida_cheque_especial_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'EmprÃ©stimos' and lv2 in ('Conta Garantida','Cheque Especial')) as emprestimos_conta_garantida_cheque_especial_PJ_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and lv2 in ('Conta Garantida','Cheque Especial')) as Ever_emprestimos_conta_garantida_cheque_especial_pj,
	count(distinct to_date) filter (where valor > 0 and to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and lv2 in ('Conta Garantida','Cheque Especial')) as count_emprestimos_conta_garantida_cheque_especial_pj,
	--------------------------------EMPRESTIMOS -> CHEQUE ESPECIAL
	sum(valor) filter (where to_date = data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PJ_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as Ever_emprestimos_cheque_especial_pj,
	count(distinct to_date) filter (where valor > 0 and to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as count_emprestimos_cheque_especial_pj,
	--------------------------------EMPRESTIMOS -> Capital de Giro com prazo vencimento superior 365 d
	sum(valor) filter (where to_date = data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo vencimento superior 365 d') as emprestimos_giro_longo_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo vencimento superior 365 d') as emprestimos_giro_longo_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo vencimento superior 365 d') as emprestimos_giro_longo_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo vencimento superior 365 d') as emprestimos_giro_longo_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo vencimento superior 365 d') as emprestimos_giro_longo_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo vencimento superior 365 d') as emprestimos_giro_longo_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo vencimento superior 365 d') as emprestimos_giro_longo_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo vencimento superior 365 d') as emprestimos_giro_longo_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo vencimento superior 365 d') as emprestimos_giro_longo_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo vencimento superior 365 d') as emprestimos_giro_longo_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo vencimento superior 365 d') as emprestimos_giro_longo_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo vencimento superior 365 d') as emprestimos_giro_longo_PJ_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo vencimento superior 365 d') as Ever_emprestimos_giro_longo_pj,
	count(distinct to_date) filter (where valor > 0 and to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo vencimento superior 365 d') as count_emprestimos_giro_longo_pj,
	--------------------------------EMPRESTIMOS -> Capital de Giro com prazo de vencimento atÃ© 365 d
	sum(valor) filter (where to_date = data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo de vencimento atÃ© 365 d') as emprestimos_giro_curto_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo de vencimento atÃ© 365 d') as emprestimos_giro_curto_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo de vencimento atÃ© 365 d') as emprestimos_giro_curto_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo de vencimento atÃ© 365 d') as emprestimos_giro_curto_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo de vencimento atÃ© 365 d') as emprestimos_giro_curto_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo de vencimento atÃ© 365 d') as emprestimos_giro_curto_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo de vencimento atÃ© 365 d') as emprestimos_giro_curto_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo de vencimento atÃ© 365 d') as emprestimos_giro_curto_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo de vencimento atÃ© 365 d') as emprestimos_giro_curto_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo de vencimento atÃ© 365 d') as emprestimos_giro_curto_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo de vencimento atÃ© 365 d') as emprestimos_giro_curto_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo de vencimento atÃ© 365 d') as emprestimos_giro_curto_PJ_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo de vencimento atÃ© 365 d') as Ever_emprestimos_giro_curto_pj,
	count(distinct to_date) filter (where valor > 0 and to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de Giro com prazo de vencimento atÃ© 365 d') as count_emprestimos_giro_curto_pj,
	--------------------------------EMPRESTIMOS -> CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito ou CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o ou CartÃ£o de crÃ©dito - nÃ£o migrado
	sum(valor) filter (where to_date = data_referencia and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PJ_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as Ever_emprestimos_cartao_credito_pj,
	count(distinct to_date) filter (where valor > 0 and to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as count_emprestimos_cartao_credito_pj,
	--------------------------------EMPRESTIMOS -> CONTA GARANTIDA
	sum(valor) filter (where to_date = data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Conta Garantida') as emprestimos_conta_garantida_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Conta Garantida') as emprestimos_conta_garantida_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Conta Garantida') as emprestimos_conta_garantida_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Conta Garantida') as emprestimos_conta_garantida_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Conta Garantida') as emprestimos_conta_garantida_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Conta Garantida') as emprestimos_conta_garantida_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Conta Garantida') as emprestimos_conta_garantida_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Conta Garantida') as emprestimos_conta_garantida_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Conta Garantida') as emprestimos_conta_garantida_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Conta Garantida') as emprestimos_conta_garantida_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Conta Garantida') as emprestimos_conta_garantida_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Conta Garantida') as emprestimos_conta_garantida_PJ_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Conta Garantida') as Ever_emprestimos_conta_garantida_pj,
	count(distinct to_date) filter (where valor > 0 and to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Conta Garantida') as count_emprestimos_conta_garantida_pj,
	--------------------------------EMPRESTIMOS -> OUTROS EMPRESTIMOS
	sum(valor) filter (where to_date = data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PJ_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as Ever_emprestimos_outros_emprestimos_pj,
	count(distinct to_date) filter (where valor > 0 and to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as count_emprestimos_outros_emprestimos_pj,
	--------------------------------EMPRESTIMOS -> Capital de giro com teto rotativo
	sum(valor) filter (where to_date = data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de giro com teto rotativo') as emprestimos_giro_rotativo_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de giro com teto rotativo') as emprestimos_giro_rotativo_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de giro com teto rotativo') as emprestimos_giro_rotativo_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de giro com teto rotativo') as emprestimos_giro_rotativo_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de giro com teto rotativo') as emprestimos_giro_rotativo_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de giro com teto rotativo') as emprestimos_giro_rotativo_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de giro com teto rotativo') as emprestimos_giro_rotativo_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de giro com teto rotativo') as emprestimos_giro_rotativo_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de giro com teto rotativo') as emprestimos_giro_rotativo_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de giro com teto rotativo') as emprestimos_giro_rotativo_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de giro com teto rotativo') as emprestimos_giro_rotativo_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de giro com teto rotativo') as emprestimos_giro_rotativo_PJ_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de giro com teto rotativo') as Ever_emprestimos_giro_rotativo_pj,
	count(distinct to_date) filter (where valor > 0 and to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Capital de giro com teto rotativo') as count_emprestimos_giro_rotativo_pj,
	--------------------------------OUTROS CREDITOS
	sum(valor) filter (where to_date = data_referencia and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PJ_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'Outros CrÃ©ditos') as Ever_OutrosCreditos_pj,
	count(distinct to_date) filter (where valor > 0 and to_date <= data_referencia and lv1 = 'Outros CrÃ©ditos') as count_OutrosCreditos_pj,
	--------------------------------DESCONTOS
	sum(valor) filter (where to_date = data_referencia and lv1 = 'Direitos CreditÃ³rios Descontados') as Desconto_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'Direitos CreditÃ³rios Descontados') as Desconto_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'Direitos CreditÃ³rios Descontados') as Desconto_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'Direitos CreditÃ³rios Descontados') as Desconto_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'Direitos CreditÃ³rios Descontados') as Desconto_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'Direitos CreditÃ³rios Descontados') as Desconto_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'Direitos CreditÃ³rios Descontados') as Desconto_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'Direitos CreditÃ³rios Descontados') as Desconto_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'Direitos CreditÃ³rios Descontados') as Desconto_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'Direitos CreditÃ³rios Descontados') as Desconto_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'Direitos CreditÃ³rios Descontados') as Desconto_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'Direitos CreditÃ³rios Descontados') as Desconto_PJ_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'Direitos CreditÃ³rios Descontados') as Ever_Desconto_pj,
	count(distinct to_date) filter (where valor > 0 and to_date <= data_referencia and lv1 = 'Direitos CreditÃ³rios Descontados') as count_Desconto_pj,
	--------------------------------FINANCIAMENTOS	
	sum(valor) filter (where to_date = data_referencia and lv1 = 'Financiamentos') as Financiamentos_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'Financiamentos') as Financiamentos_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'Financiamentos') as Financiamentos_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'Financiamentos') as Financiamentos_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'Financiamentos') as Financiamentos_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'Financiamentos') as Financiamentos_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'Financiamentos') as Financiamentos_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'Financiamentos') as Financiamentos_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'Financiamentos') as Financiamentos_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'Financiamentos') as Financiamentos_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'Financiamentos') as Financiamentos_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'Financiamentos') as Financiamentos_PJ_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'Financiamentos') as Ever_Financiamentos_pj,
	count(distinct to_date) filter (where valor > 0 and to_date <= data_referencia and lv1 = 'Financiamentos') as count_Financiamentos_pj,
	--------------------------------ADIANTAMENTOS	
	sum(valor) filter (where to_date = data_referencia and lv1 = 'Adiantamentos a Depositantes') as Adiantamentos_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'Adiantamentos a Depositantes') as Adiantamentos_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'Adiantamentos a Depositantes') as Adiantamentos_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'Adiantamentos a Depositantes') as Adiantamentos_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'Adiantamentos a Depositantes') as Adiantamentos_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'Adiantamentos a Depositantes') as Adiantamentos_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'Adiantamentos a Depositantes') as Adiantamentos_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'Adiantamentos a Depositantes') as Adiantamentos_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'Adiantamentos a Depositantes') as Adiantamentos_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'Adiantamentos a Depositantes') as Adiantamentos_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'Adiantamentos a Depositantes') as Adiantamentos_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'Adiantamentos a Depositantes') as Adiantamentos_PJ_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'Adiantamentos a Depositantes') as Ever_Adiantamentos_pj,
	count(distinct to_date) filter (where valor > 0 and to_date <= data_referencia and lv1 = 'Adiantamentos a Depositantes') as count_Adiantamentos_pj,
	--------------------------------OUTROS FINANCIAMENTOS
	sum(valor) filter (where to_date = data_referencia and lv1 in ('Financiamentos Rurais','Financiamentos Ã  ImportaÃ§Ã£o','Financiamentos Ã  ExportaÃ§Ã£o','Financiamentos ImobiliÃ¡rios')) as OutrosFinanciamentos_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 in ('Financiamentos Rurais','Financiamentos Ã  ImportaÃ§Ã£o','Financiamentos Ã  ExportaÃ§Ã£o','Financiamentos ImobiliÃ¡rios')) as OutrosFinanciamentos_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 in ('Financiamentos Rurais','Financiamentos Ã  ImportaÃ§Ã£o','Financiamentos Ã  ExportaÃ§Ã£o','Financiamentos ImobiliÃ¡rios')) as OutrosFinanciamentos_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 in ('Financiamentos Rurais','Financiamentos Ã  ImportaÃ§Ã£o','Financiamentos Ã  ExportaÃ§Ã£o','Financiamentos ImobiliÃ¡rios')) as OutrosFinanciamentos_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 in ('Financiamentos Rurais','Financiamentos Ã  ImportaÃ§Ã£o','Financiamentos Ã  ExportaÃ§Ã£o','Financiamentos ImobiliÃ¡rios')) as OutrosFinanciamentos_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 in ('Financiamentos Rurais','Financiamentos Ã  ImportaÃ§Ã£o','Financiamentos Ã  ExportaÃ§Ã£o','Financiamentos ImobiliÃ¡rios')) as OutrosFinanciamentos_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 in ('Financiamentos Rurais','Financiamentos Ã  ImportaÃ§Ã£o','Financiamentos Ã  ExportaÃ§Ã£o','Financiamentos ImobiliÃ¡rios')) as OutrosFinanciamentos_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 in ('Financiamentos Rurais','Financiamentos Ã  ImportaÃ§Ã£o','Financiamentos Ã  ExportaÃ§Ã£o','Financiamentos ImobiliÃ¡rios')) as OutrosFinanciamentos_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 in ('Financiamentos Rurais','Financiamentos Ã  ImportaÃ§Ã£o','Financiamentos Ã  ExportaÃ§Ã£o','Financiamentos ImobiliÃ¡rios')) as OutrosFinanciamentos_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 in ('Financiamentos Rurais','Financiamentos Ã  ImportaÃ§Ã£o','Financiamentos Ã  ExportaÃ§Ã£o','Financiamentos ImobiliÃ¡rios')) as OutrosFinanciamentos_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 in ('Financiamentos Rurais','Financiamentos Ã  ImportaÃ§Ã£o','Financiamentos Ã  ExportaÃ§Ã£o','Financiamentos ImobiliÃ¡rios')) as OutrosFinanciamentos_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 in ('Financiamentos Rurais','Financiamentos Ã  ImportaÃ§Ã£o','Financiamentos Ã  ExportaÃ§Ã£o','Financiamentos ImobiliÃ¡rios')) as OutrosFinanciamentos_PJ_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 in ('Financiamentos Rurais','Financiamentos Ã  ImportaÃ§Ã£o','Financiamentos Ã  ExportaÃ§Ã£o','Financiamentos ImobiliÃ¡rios')) as Ever_OutrosFinanciamentos_pj,
	count(distinct to_date) filter (where valor > 0 and to_date <= data_referencia and lv1 in ('Financiamentos Rurais','Financiamentos Ã  ImportaÃ§Ã£o','Financiamentos Ã  ExportaÃ§Ã£o','Financiamentos ImobiliÃ¡rios')) as count_OutrosFinanciamentos_pj,
	--------------------------------LIMITE DE CRÃ‰DITO CURTO PRAZO
	sum(valor) filter (where to_date = data_referencia and lv1 = 'Limite de CrÃ©dito (H)') as LimiteCredito_curto_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento atÃ© 360 dias') as LimiteCredito_curto_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento atÃ© 360 dias') as LimiteCredito_curto_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento atÃ© 360 dias') as LimiteCredito_curto_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento atÃ© 360 dias') as LimiteCredito_curto_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento atÃ© 360 dias') as LimiteCredito_curto_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento atÃ© 360 dias') as LimiteCredito_curto_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento atÃ© 360 dias') as LimiteCredito_curto_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento atÃ© 360 dias') as LimiteCredito_curto_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento atÃ© 360 dias') as LimiteCredito_curto_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento atÃ© 360 dias') as LimiteCredito_curto_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'Limite de CrÃ©dito (H)') as LimiteCredito_curto_PJ_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento atÃ© 360 dias') as Ever_LimiteCredito_curto_pj,
	count(distinct to_date) filter (where valor > 0 and to_date <= data_referencia and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento atÃ© 360 dias') as count_LimiteCredito_curto_pj,
	--------------------------------LIMITE DE CRÃ‰DITO LONGO PRAZO
	sum(valor) filter (where to_date = data_referencia and lv1 = 'Limite de CrÃ©dito (H)') as LimiteCredito_longo_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento acima de 360 dias') as LimiteCredito_longo_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento acima de 360 dias') as LimiteCredito_longo_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento acima de 360 dias') as LimiteCredito_longo_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento acima de 360 dias') as LimiteCredito_longo_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento acima de 360 dias') as LimiteCredito_longo_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento acima de 360 dias') as LimiteCredito_longo_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento acima de 360 dias') as LimiteCredito_longo_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento acima de 360 dias') as LimiteCredito_longo_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento acima de 360 dias') as LimiteCredito_longo_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento acima de 360 dias') as LimiteCredito_longo_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento acima de 360 dias') as LimiteCredito_longo_PJ_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento acima de 360 dias') as Ever_LimiteCredito_longo_pj,
	count(distinct to_date) filter (where valor > 0 and to_date <= data_referencia and lv1 = 'Limite de CrÃ©dito (H)' and lv2 = 'Limite de crÃ©dito com vencimento acima de 360 dias') as count_LimiteCredito_longo_pj,
	--------------------------------PREJUIZO
	sum(valor) filter (where to_date = data_referencia and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PJ_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'PrejuÃ­zo (B)') as Ever_Prejuizo_pj,
	count(distinct to_date) filter (where valor > 0 and to_date <= data_referencia and lv1 = 'PrejuÃ­zo (B)') as count_Prejuizo_pj,
	--------------------------------CARTEIRA CREDITO
	sum(valor) filter (where to_date = data_referencia and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PJ_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as Ever_CarteiraCredito_pj,
	count(distinct to_date) filter (where valor > 0 and to_date <= data_referencia and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as count_CarteiraCredito_pj
	from(select direct_prospect_id,divida_atual_pj,regexp_replace(lv1, '\d{2}\s-\s', '') as lv1,replace(replace(replace(replace(regexp_replace(lv2, '\d{4}\s-\s', ''),' Â– ',' - '),' -- ',' - '),' Â–- ',' - '),'interfinanceiros','Interfinanceiros') as lv2, to_date(data,'mm/yyyy'), case when valor is null then 0 else valor end::float,
			case when max_id is null then case when extract(day from ref_date) >= 16 then to_date(to_char(ref_date - interval '1 month' ,'Mon/yy'),'Mon/yy') else to_date(to_char(ref_date - interval '2 months' ,'Mon/yy'),'Mon/yy') end else first_value(to_date(data,'mm/yyyy')) over (partition by direct_prospect_id order by to_date(data,'mm/yyyy') desc) end as data_referencia
		from(select direct_prospect_id, ref_date,max_id,divida_atual_pj,Lv1, nome as Lv2, (jsonb_populate_recordset(null::meu2, serie)).*
			from (select direct_prospect_id, ref_date,max_id,case when max_id is not null then divida_atual_pj end as divida_atual_pj,nome as Lv1, (jsonb_populate_recordset(null::meu, detalhes)).*
				from (select direct_prospect_id, (data->'indicadores'->>'dividaAtual')::float as divida_atual_pj, to_date(data->'data_base'->>'Data-Base','mm/yyyy') as data_base,ref_date,max_id,(jsonb_populate_recordset(NULL ::lucas, data #> '{por_modalidade}')).*
				from (select direct_prospect_id,data,ref_date,max_id
					from credito_coleta cc
					join(select dp.direct_prospect_id, 
							max(coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date) + interval '1 day') as ref_date,
							max(dp.cnpj) as cnpj,
							max(cc.id) filter (where coalesce(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '1 day') as max_id,
							min(cc.id) as min_id
						from credito_coleta cc
						join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
						join temp_ids as aux on aux.direct_prospect_id = dp.direct_prospect_id
						left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
							from direct_prospects dp
							left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
							left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
							left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
							group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
						left join offers o on o.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
						left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on coalesce(max_id ,min_id) = lr.loan_request_id) lr on lr.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
						where documento_tipo = 'CNPJ' and tipo = 'SCR'
						group by 1) as t1 on cc.id = coalesce(max_id,min_id)) as t2) as t3) as t4) as t5) as t6 group by 1,2) as t7) as SCRModal_PJ on SCRModal_PJ.direct_prospect_id = dp.direct_prospect_id 
--------------------------TABLE SCR MODALIDADES PF
left join(select direct_prospect_id, divida_atual_pf,qtd_meses_modalidade_pf,
	emprestimos_pf_Curr::float,
	emprestimos_pf_1M::float,
	emprestimos_pf_2M::float,
	emprestimos_pf_3M::float,
	emprestimos_pf_4M::float,
	emprestimos_pf_5M::float,
	emprestimos_pf_6M::float,
	emprestimos_pf_7M::float,
	emprestimos_pf_8M::float,
	emprestimos_pf_9M::float,
	emprestimos_pf_10M::float,
	emprestimos_pf_11M::float,
	greatest(emprestimos_pf_Curr,emprestimos_pf_1M,emprestimos_pf_2M,emprestimos_pf_3M,emprestimos_pf_4M,emprestimos_pf_5M,emprestimos_pf_6M,emprestimos_pf_7M,emprestimos_pf_8M,emprestimos_pf_9M,emprestimos_pf_10M,emprestimos_pf_11M) as max_emprestimos_PF,
	Ever_emprestimos_pf::float,
	case when emprestimos_pf_10M < emprestimos_pf_11M then 1 else 0 end +
	case when emprestimos_pf_9M < emprestimos_pf_10M then 1 else 0 end +
	case when emprestimos_pf_8M < emprestimos_pf_9M then 1 else 0 end +
	case when emprestimos_pf_7M < emprestimos_pf_8M then 1 else 0 end +
	case when emprestimos_pf_6M < emprestimos_pf_7M then 1 else 0 end +
	case when emprestimos_pf_5M < emprestimos_pf_6M then 1 else 0 end +
	case when emprestimos_pf_4M < emprestimos_pf_5M then 1 else 0 end +
	case when emprestimos_pf_3M < emprestimos_pf_4M then 1 else 0 end +
	case when emprestimos_pf_2M < emprestimos_pf_3M then 1 else 0 end +
	case when emprestimos_pf_1M < emprestimos_pf_2M then 1 else 0 end +
	case when emprestimos_pf_Curr < emprestimos_pf_1M then 1 else 0 end::float as Meses_Reducao_emprestimos_pf,
	case when emprestimos_pf_10M > emprestimos_pf_11M then 1 else 0 end +
	case when emprestimos_pf_9M > emprestimos_pf_10M then 1 else 0 end +
	case when emprestimos_pf_8M > emprestimos_pf_9M then 1 else 0 end +
	case when emprestimos_pf_7M > emprestimos_pf_8M then 1 else 0 end +
	case when emprestimos_pf_6M > emprestimos_pf_7M then 1 else 0 end +
	case when emprestimos_pf_5M > emprestimos_pf_6M then 1 else 0 end +
	case when emprestimos_pf_4M > emprestimos_pf_5M then 1 else 0 end +
	case when emprestimos_pf_3M > emprestimos_pf_4M then 1 else 0 end +
	case when emprestimos_pf_2M > emprestimos_pf_3M then 1 else 0 end +
	case when emprestimos_pf_1M > emprestimos_pf_2M then 1 else 0 end +
	case when emprestimos_pf_Curr > emprestimos_pf_1M then 1 else 0 end::float as Meses_Aumento_emprestimos_pf,
	case when emprestimos_pf_1M is null then null else
	case when emprestimos_pf_Curr >= emprestimos_pf_1M or emprestimos_pf_1M is null then 0 else
	case when emprestimos_pf_1M >= emprestimos_pf_2M or emprestimos_pf_2M is null then 1 else
	case when emprestimos_pf_2M >= emprestimos_pf_3M or emprestimos_pf_3M is null then 2 else
	case when emprestimos_pf_3M >= emprestimos_pf_4M or emprestimos_pf_4M is null then 3 else
	case when emprestimos_pf_4M >= emprestimos_pf_5M or emprestimos_pf_5M is null then 4 else
	case when emprestimos_pf_5M >= emprestimos_pf_6M or emprestimos_pf_6M is null then 5 else
	case when emprestimos_pf_6M >= emprestimos_pf_7M or emprestimos_pf_7M is null then 6 else
	case when emprestimos_pf_7M >= emprestimos_pf_8M or emprestimos_pf_8M is null then 7 else
	case when emprestimos_pf_8M >= emprestimos_pf_9M or emprestimos_pf_9M is null then 8 else
	case when emprestimos_pf_9M >= emprestimos_pf_10M or emprestimos_pf_10M is null then 9 else
	case when emprestimos_pf_10M >= emprestimos_pf_11M or emprestimos_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_emprestimos_pf,
	case when emprestimos_pf_1M is null then null else
	case when emprestimos_pf_Curr <= emprestimos_pf_1M or emprestimos_pf_1M is null then 0 else
	case when emprestimos_pf_1M <= emprestimos_pf_2M or emprestimos_pf_2M is null then 1 else
	case when emprestimos_pf_2M <= emprestimos_pf_3M or emprestimos_pf_3M is null then 2 else
	case when emprestimos_pf_3M <= emprestimos_pf_4M or emprestimos_pf_4M is null then 3 else
	case when emprestimos_pf_4M <= emprestimos_pf_5M or emprestimos_pf_5M is null then 4 else
	case when emprestimos_pf_5M <= emprestimos_pf_6M or emprestimos_pf_6M is null then 5 else
	case when emprestimos_pf_6M <= emprestimos_pf_7M or emprestimos_pf_7M is null then 6 else
	case when emprestimos_pf_7M <= emprestimos_pf_8M or emprestimos_pf_8M is null then 7 else
	case when emprestimos_pf_8M <= emprestimos_pf_9M or emprestimos_pf_9M is null then 8 else
	case when emprestimos_pf_9M <= emprestimos_pf_10M or emprestimos_pf_10M is null then 9 else
	case when emprestimos_pf_10M <= emprestimos_pf_11M or emprestimos_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_emprestimos_pf,
	(case when emprestimos_PF_10M < emprestimos_PF_11M then emprestimos_PF_10M - emprestimos_PF_11M else 0 end +
	case when emprestimos_PF_9M < emprestimos_PF_10M then emprestimos_PF_9M - emprestimos_PF_10M else 0 end +
	case when emprestimos_PF_8M < emprestimos_PF_9M then emprestimos_PF_8M - emprestimos_PF_9M else 0 end +
	case when emprestimos_PF_7M < emprestimos_PF_8M then emprestimos_PF_7M - emprestimos_PF_8M else 0 end +
	case when emprestimos_PF_6M < emprestimos_PF_7M then emprestimos_PF_6M - emprestimos_PF_7M else 0 end +
	case when emprestimos_PF_5M < emprestimos_PF_6M then emprestimos_PF_5M - emprestimos_PF_6M else 0 end +
	case when emprestimos_PF_4M < emprestimos_PF_5M then emprestimos_PF_4M - emprestimos_PF_5M else 0 end +
	case when emprestimos_PF_3M < emprestimos_PF_4M then emprestimos_PF_3M - emprestimos_PF_4M else 0 end +
	case when emprestimos_PF_2M < emprestimos_PF_3M then emprestimos_PF_2M - emprestimos_PF_3M else 0 end +
	case when emprestimos_PF_1M < emprestimos_PF_2M then emprestimos_PF_1M - emprestimos_PF_2M else 0 end +
	case when emprestimos_PF_Curr < emprestimos_PF_1M then emprestimos_PF_Curr - emprestimos_PF_1M else 0 end)*(-1)::float as Saldo_Amort_emprestimos_PF,
	emprestimos_cheque_especial_pf_Curr::float,
	emprestimos_cheque_especial_pf_1M::float,
	emprestimos_cheque_especial_pf_2M::float,
	emprestimos_cheque_especial_pf_3M::float,
	emprestimos_cheque_especial_pf_4M::float,
	emprestimos_cheque_especial_pf_5M::float,
	emprestimos_cheque_especial_pf_6M::float,
	emprestimos_cheque_especial_pf_7M::float,
	emprestimos_cheque_especial_pf_8M::float,
	emprestimos_cheque_especial_pf_9M::float,
	emprestimos_cheque_especial_pf_10M::float,
	emprestimos_cheque_especial_pf_11M::float,
	greatest(emprestimos_cheque_especial_pf_Curr,emprestimos_cheque_especial_pf_1M,emprestimos_cheque_especial_pf_2M,emprestimos_cheque_especial_pf_3M,emprestimos_cheque_especial_pf_4M,emprestimos_cheque_especial_pf_5M,emprestimos_cheque_especial_pf_6M,emprestimos_cheque_especial_pf_7M,emprestimos_cheque_especial_pf_8M,emprestimos_cheque_especial_pf_9M,emprestimos_cheque_especial_pf_10M,emprestimos_cheque_especial_pf_11M)::float as max_emprestimos_cheque_especial_PF,
	Ever_emprestimos_cheque_especial_pf::float,
	case when emprestimos_cheque_especial_pf_10M < emprestimos_cheque_especial_pf_11M then 1 else 0 end +
	case when emprestimos_cheque_especial_pf_9M < emprestimos_cheque_especial_pf_10M then 1 else 0 end +
	case when emprestimos_cheque_especial_pf_8M < emprestimos_cheque_especial_pf_9M then 1 else 0 end +
	case when emprestimos_cheque_especial_pf_7M < emprestimos_cheque_especial_pf_8M then 1 else 0 end +
	case when emprestimos_cheque_especial_pf_6M < emprestimos_cheque_especial_pf_7M then 1 else 0 end +
	case when emprestimos_cheque_especial_pf_5M < emprestimos_cheque_especial_pf_6M then 1 else 0 end +
	case when emprestimos_cheque_especial_pf_4M < emprestimos_cheque_especial_pf_5M then 1 else 0 end +
	case when emprestimos_cheque_especial_pf_3M < emprestimos_cheque_especial_pf_4M then 1 else 0 end +
	case when emprestimos_cheque_especial_pf_2M < emprestimos_cheque_especial_pf_3M then 1 else 0 end +
	case when emprestimos_cheque_especial_pf_1M < emprestimos_cheque_especial_pf_2M then 1 else 0 end +
	case when emprestimos_cheque_especial_pf_Curr < emprestimos_cheque_especial_pf_1M then 1 else 0 end::float as Meses_Reducao_emprestimos_cheque_especial_pf,
	case when emprestimos_cheque_especial_pf_10M > emprestimos_cheque_especial_pf_11M then 1 else 0 end +
	case when emprestimos_cheque_especial_pf_9M > emprestimos_cheque_especial_pf_10M then 1 else 0 end +
	case when emprestimos_cheque_especial_pf_8M > emprestimos_cheque_especial_pf_9M then 1 else 0 end +
	case when emprestimos_cheque_especial_pf_7M > emprestimos_cheque_especial_pf_8M then 1 else 0 end +
	case when emprestimos_cheque_especial_pf_6M > emprestimos_cheque_especial_pf_7M then 1 else 0 end +
	case when emprestimos_cheque_especial_pf_5M > emprestimos_cheque_especial_pf_6M then 1 else 0 end +
	case when emprestimos_cheque_especial_pf_4M > emprestimos_cheque_especial_pf_5M then 1 else 0 end +
	case when emprestimos_cheque_especial_pf_3M > emprestimos_cheque_especial_pf_4M then 1 else 0 end +
	case when emprestimos_cheque_especial_pf_2M > emprestimos_cheque_especial_pf_3M then 1 else 0 end +
	case when emprestimos_cheque_especial_pf_1M > emprestimos_cheque_especial_pf_2M then 1 else 0 end +
	case when emprestimos_cheque_especial_pf_Curr > emprestimos_cheque_especial_pf_1M then 1 else 0 end::float as Meses_Aumento_emprestimos_cheque_especial_pf,
	case when emprestimos_cheque_especial_pf_1M is null then null else
	case when emprestimos_cheque_especial_pf_Curr >= emprestimos_cheque_especial_pf_1M or emprestimos_cheque_especial_pf_1M is null then 0 else
	case when emprestimos_cheque_especial_pf_1M >= emprestimos_cheque_especial_pf_2M or emprestimos_cheque_especial_pf_2M is null then 1 else
	case when emprestimos_cheque_especial_pf_2M >= emprestimos_cheque_especial_pf_3M or emprestimos_cheque_especial_pf_3M is null then 2 else
	case when emprestimos_cheque_especial_pf_3M >= emprestimos_cheque_especial_pf_4M or emprestimos_cheque_especial_pf_4M is null then 3 else
	case when emprestimos_cheque_especial_pf_4M >= emprestimos_cheque_especial_pf_5M or emprestimos_cheque_especial_pf_5M is null then 4 else
	case when emprestimos_cheque_especial_pf_5M >= emprestimos_cheque_especial_pf_6M or emprestimos_cheque_especial_pf_6M is null then 5 else
	case when emprestimos_cheque_especial_pf_6M >= emprestimos_cheque_especial_pf_7M or emprestimos_cheque_especial_pf_7M is null then 6 else
	case when emprestimos_cheque_especial_pf_7M >= emprestimos_cheque_especial_pf_8M or emprestimos_cheque_especial_pf_8M is null then 7 else
	case when emprestimos_cheque_especial_pf_8M >= emprestimos_cheque_especial_pf_9M or emprestimos_cheque_especial_pf_9M is null then 8 else
	case when emprestimos_cheque_especial_pf_9M >= emprestimos_cheque_especial_pf_10M or emprestimos_cheque_especial_pf_10M is null then 9 else
	case when emprestimos_cheque_especial_pf_10M >= emprestimos_cheque_especial_pf_11M or emprestimos_cheque_especial_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_emprestimos_cheque_especial_pf,
	case when emprestimos_cheque_especial_pf_1M is null then null else
	case when emprestimos_cheque_especial_pf_Curr <= emprestimos_cheque_especial_pf_1M or emprestimos_cheque_especial_pf_1M is null then 0 else
	case when emprestimos_cheque_especial_pf_1M <= emprestimos_cheque_especial_pf_2M or emprestimos_cheque_especial_pf_2M is null then 1 else
	case when emprestimos_cheque_especial_pf_2M <= emprestimos_cheque_especial_pf_3M or emprestimos_cheque_especial_pf_3M is null then 2 else
	case when emprestimos_cheque_especial_pf_3M <= emprestimos_cheque_especial_pf_4M or emprestimos_cheque_especial_pf_4M is null then 3 else
	case when emprestimos_cheque_especial_pf_4M <= emprestimos_cheque_especial_pf_5M or emprestimos_cheque_especial_pf_5M is null then 4 else
	case when emprestimos_cheque_especial_pf_5M <= emprestimos_cheque_especial_pf_6M or emprestimos_cheque_especial_pf_6M is null then 5 else
	case when emprestimos_cheque_especial_pf_6M <= emprestimos_cheque_especial_pf_7M or emprestimos_cheque_especial_pf_7M is null then 6 else
	case when emprestimos_cheque_especial_pf_7M <= emprestimos_cheque_especial_pf_8M or emprestimos_cheque_especial_pf_8M is null then 7 else
	case when emprestimos_cheque_especial_pf_8M <= emprestimos_cheque_especial_pf_9M or emprestimos_cheque_especial_pf_9M is null then 8 else
	case when emprestimos_cheque_especial_pf_9M <= emprestimos_cheque_especial_pf_10M or emprestimos_cheque_especial_pf_10M is null then 9 else
	case when emprestimos_cheque_especial_pf_10M <= emprestimos_cheque_especial_pf_11M or emprestimos_cheque_especial_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_emprestimos_cheque_especial_pf,
	(case when emprestimos_cheque_especial_PF_10M < emprestimos_cheque_especial_PF_11M then emprestimos_cheque_especial_PF_10M - emprestimos_cheque_especial_PF_11M else 0 end +
	case when emprestimos_cheque_especial_PF_9M < emprestimos_cheque_especial_PF_10M then emprestimos_cheque_especial_PF_9M - emprestimos_cheque_especial_PF_10M else 0 end +
	case when emprestimos_cheque_especial_PF_8M < emprestimos_cheque_especial_PF_9M then emprestimos_cheque_especial_PF_8M - emprestimos_cheque_especial_PF_9M else 0 end +
	case when emprestimos_cheque_especial_PF_7M < emprestimos_cheque_especial_PF_8M then emprestimos_cheque_especial_PF_7M - emprestimos_cheque_especial_PF_8M else 0 end +
	case when emprestimos_cheque_especial_PF_6M < emprestimos_cheque_especial_PF_7M then emprestimos_cheque_especial_PF_6M - emprestimos_cheque_especial_PF_7M else 0 end +
	case when emprestimos_cheque_especial_PF_5M < emprestimos_cheque_especial_PF_6M then emprestimos_cheque_especial_PF_5M - emprestimos_cheque_especial_PF_6M else 0 end +
	case when emprestimos_cheque_especial_PF_4M < emprestimos_cheque_especial_PF_5M then emprestimos_cheque_especial_PF_4M - emprestimos_cheque_especial_PF_5M else 0 end +
	case when emprestimos_cheque_especial_PF_3M < emprestimos_cheque_especial_PF_4M then emprestimos_cheque_especial_PF_3M - emprestimos_cheque_especial_PF_4M else 0 end +
	case when emprestimos_cheque_especial_PF_2M < emprestimos_cheque_especial_PF_3M then emprestimos_cheque_especial_PF_2M - emprestimos_cheque_especial_PF_3M else 0 end +
	case when emprestimos_cheque_especial_PF_1M < emprestimos_cheque_especial_PF_2M then emprestimos_cheque_especial_PF_1M - emprestimos_cheque_especial_PF_2M else 0 end +
	case when emprestimos_cheque_especial_PF_Curr < emprestimos_cheque_especial_PF_1M then emprestimos_cheque_especial_PF_Curr - emprestimos_cheque_especial_PF_1M else 0 end)*(-1)::float as Saldo_Amort_emprestimos_cheque_especial_PF,
	emprestimos_cartao_credito_pf_Curr::float,
	emprestimos_cartao_credito_pf_1M::float,
	emprestimos_cartao_credito_pf_2M::float,
	emprestimos_cartao_credito_pf_3M::float,
	emprestimos_cartao_credito_pf_4M::float,
	emprestimos_cartao_credito_pf_5M::float,
	emprestimos_cartao_credito_pf_6M::float,
	emprestimos_cartao_credito_pf_7M::float,
	emprestimos_cartao_credito_pf_8M::float,
	emprestimos_cartao_credito_pf_9M::float,
	emprestimos_cartao_credito_pf_10M::float,
	emprestimos_cartao_credito_pf_11M::float,
	greatest(emprestimos_cartao_credito_pf_Curr,emprestimos_cartao_credito_pf_1M,emprestimos_cartao_credito_pf_2M,emprestimos_cartao_credito_pf_3M,emprestimos_cartao_credito_pf_4M,emprestimos_cartao_credito_pf_5M,emprestimos_cartao_credito_pf_6M,emprestimos_cartao_credito_pf_7M,emprestimos_cartao_credito_pf_8M,emprestimos_cartao_credito_pf_9M,emprestimos_cartao_credito_pf_10M,emprestimos_cartao_credito_pf_11M)::float as max_emprestimos_cartao_credito_PF,
	Ever_emprestimos_cartao_credito_pf::float,
	case when emprestimos_cartao_credito_pf_10M < emprestimos_cartao_credito_pf_11M then 1 else 0 end +
	case when emprestimos_cartao_credito_pf_9M < emprestimos_cartao_credito_pf_10M then 1 else 0 end +
	case when emprestimos_cartao_credito_pf_8M < emprestimos_cartao_credito_pf_9M then 1 else 0 end +
	case when emprestimos_cartao_credito_pf_7M < emprestimos_cartao_credito_pf_8M then 1 else 0 end +
	case when emprestimos_cartao_credito_pf_6M < emprestimos_cartao_credito_pf_7M then 1 else 0 end +
	case when emprestimos_cartao_credito_pf_5M < emprestimos_cartao_credito_pf_6M then 1 else 0 end +
	case when emprestimos_cartao_credito_pf_4M < emprestimos_cartao_credito_pf_5M then 1 else 0 end +
	case when emprestimos_cartao_credito_pf_3M < emprestimos_cartao_credito_pf_4M then 1 else 0 end +
	case when emprestimos_cartao_credito_pf_2M < emprestimos_cartao_credito_pf_3M then 1 else 0 end +
	case when emprestimos_cartao_credito_pf_1M < emprestimos_cartao_credito_pf_2M then 1 else 0 end +
	case when emprestimos_cartao_credito_pf_Curr < emprestimos_cartao_credito_pf_1M then 1 else 0 end::float as Meses_Reducao_emprestimos_cartao_credito_pf,
	case when emprestimos_cartao_credito_pf_10M > emprestimos_cartao_credito_pf_11M then 1 else 0 end +
	case when emprestimos_cartao_credito_pf_9M > emprestimos_cartao_credito_pf_10M then 1 else 0 end +
	case when emprestimos_cartao_credito_pf_8M > emprestimos_cartao_credito_pf_9M then 1 else 0 end +
	case when emprestimos_cartao_credito_pf_7M > emprestimos_cartao_credito_pf_8M then 1 else 0 end +
	case when emprestimos_cartao_credito_pf_6M > emprestimos_cartao_credito_pf_7M then 1 else 0 end +
	case when emprestimos_cartao_credito_pf_5M > emprestimos_cartao_credito_pf_6M then 1 else 0 end +
	case when emprestimos_cartao_credito_pf_4M > emprestimos_cartao_credito_pf_5M then 1 else 0 end +
	case when emprestimos_cartao_credito_pf_3M > emprestimos_cartao_credito_pf_4M then 1 else 0 end +
	case when emprestimos_cartao_credito_pf_2M > emprestimos_cartao_credito_pf_3M then 1 else 0 end +
	case when emprestimos_cartao_credito_pf_1M > emprestimos_cartao_credito_pf_2M then 1 else 0 end +
	case when emprestimos_cartao_credito_pf_Curr > emprestimos_cartao_credito_pf_1M then 1 else 0 end::float as Meses_Aumento_emprestimos_cartao_credito_pf,
	case when emprestimos_cartao_credito_pf_1M is null then null else
	case when emprestimos_cartao_credito_pf_Curr >= emprestimos_cartao_credito_pf_1M or emprestimos_cartao_credito_pf_1M is null then 0 else
	case when emprestimos_cartao_credito_pf_1M >= emprestimos_cartao_credito_pf_2M or emprestimos_cartao_credito_pf_2M is null then 1 else
	case when emprestimos_cartao_credito_pf_2M >= emprestimos_cartao_credito_pf_3M or emprestimos_cartao_credito_pf_3M is null then 2 else
	case when emprestimos_cartao_credito_pf_3M >= emprestimos_cartao_credito_pf_4M or emprestimos_cartao_credito_pf_4M is null then 3 else
	case when emprestimos_cartao_credito_pf_4M >= emprestimos_cartao_credito_pf_5M or emprestimos_cartao_credito_pf_5M is null then 4 else
	case when emprestimos_cartao_credito_pf_5M >= emprestimos_cartao_credito_pf_6M or emprestimos_cartao_credito_pf_6M is null then 5 else
	case when emprestimos_cartao_credito_pf_6M >= emprestimos_cartao_credito_pf_7M or emprestimos_cartao_credito_pf_7M is null then 6 else
	case when emprestimos_cartao_credito_pf_7M >= emprestimos_cartao_credito_pf_8M or emprestimos_cartao_credito_pf_8M is null then 7 else
	case when emprestimos_cartao_credito_pf_8M >= emprestimos_cartao_credito_pf_9M or emprestimos_cartao_credito_pf_9M is null then 8 else
	case when emprestimos_cartao_credito_pf_9M >= emprestimos_cartao_credito_pf_10M or emprestimos_cartao_credito_pf_10M is null then 9 else
	case when emprestimos_cartao_credito_pf_10M >= emprestimos_cartao_credito_pf_11M or emprestimos_cartao_credito_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_emprestimos_cartao_credito_pf,
	case when emprestimos_cartao_credito_pf_1M is null then null else
	case when emprestimos_cartao_credito_pf_Curr <= emprestimos_cartao_credito_pf_1M or emprestimos_cartao_credito_pf_1M is null then 0 else
	case when emprestimos_cartao_credito_pf_1M <= emprestimos_cartao_credito_pf_2M or emprestimos_cartao_credito_pf_2M is null then 1 else
	case when emprestimos_cartao_credito_pf_2M <= emprestimos_cartao_credito_pf_3M or emprestimos_cartao_credito_pf_3M is null then 2 else
	case when emprestimos_cartao_credito_pf_3M <= emprestimos_cartao_credito_pf_4M or emprestimos_cartao_credito_pf_4M is null then 3 else
	case when emprestimos_cartao_credito_pf_4M <= emprestimos_cartao_credito_pf_5M or emprestimos_cartao_credito_pf_5M is null then 4 else
	case when emprestimos_cartao_credito_pf_5M <= emprestimos_cartao_credito_pf_6M or emprestimos_cartao_credito_pf_6M is null then 5 else
	case when emprestimos_cartao_credito_pf_6M <= emprestimos_cartao_credito_pf_7M or emprestimos_cartao_credito_pf_7M is null then 6 else
	case when emprestimos_cartao_credito_pf_7M <= emprestimos_cartao_credito_pf_8M or emprestimos_cartao_credito_pf_8M is null then 7 else
	case when emprestimos_cartao_credito_pf_8M <= emprestimos_cartao_credito_pf_9M or emprestimos_cartao_credito_pf_9M is null then 8 else
	case when emprestimos_cartao_credito_pf_9M <= emprestimos_cartao_credito_pf_10M or emprestimos_cartao_credito_pf_10M is null then 9 else
	case when emprestimos_cartao_credito_pf_10M <= emprestimos_cartao_credito_pf_11M or emprestimos_cartao_credito_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_emprestimos_cartao_credito_pf,
	(case when emprestimos_cartao_credito_PF_10M < emprestimos_cartao_credito_PF_11M then emprestimos_cartao_credito_PF_10M - emprestimos_cartao_credito_PF_11M else 0 end +
	case when emprestimos_cartao_credito_PF_9M < emprestimos_cartao_credito_PF_10M then emprestimos_cartao_credito_PF_9M - emprestimos_cartao_credito_PF_10M else 0 end +
	case when emprestimos_cartao_credito_PF_8M < emprestimos_cartao_credito_PF_9M then emprestimos_cartao_credito_PF_8M - emprestimos_cartao_credito_PF_9M else 0 end +
	case when emprestimos_cartao_credito_PF_7M < emprestimos_cartao_credito_PF_8M then emprestimos_cartao_credito_PF_7M - emprestimos_cartao_credito_PF_8M else 0 end +
	case when emprestimos_cartao_credito_PF_6M < emprestimos_cartao_credito_PF_7M then emprestimos_cartao_credito_PF_6M - emprestimos_cartao_credito_PF_7M else 0 end +
	case when emprestimos_cartao_credito_PF_5M < emprestimos_cartao_credito_PF_6M then emprestimos_cartao_credito_PF_5M - emprestimos_cartao_credito_PF_6M else 0 end +
	case when emprestimos_cartao_credito_PF_4M < emprestimos_cartao_credito_PF_5M then emprestimos_cartao_credito_PF_4M - emprestimos_cartao_credito_PF_5M else 0 end +
	case when emprestimos_cartao_credito_PF_3M < emprestimos_cartao_credito_PF_4M then emprestimos_cartao_credito_PF_3M - emprestimos_cartao_credito_PF_4M else 0 end +
	case when emprestimos_cartao_credito_PF_2M < emprestimos_cartao_credito_PF_3M then emprestimos_cartao_credito_PF_2M - emprestimos_cartao_credito_PF_3M else 0 end +
	case when emprestimos_cartao_credito_PF_1M < emprestimos_cartao_credito_PF_2M then emprestimos_cartao_credito_PF_1M - emprestimos_cartao_credito_PF_2M else 0 end +
	case when emprestimos_cartao_credito_PF_Curr < emprestimos_cartao_credito_PF_1M then emprestimos_cartao_credito_PF_Curr - emprestimos_cartao_credito_PF_1M else 0 end)*(-1)::float as Saldo_Amort_emprestimos_cartao_credito_PF,
	emprestimos_credito_pPJ_pf_Curr::float,
	emprestimos_credito_pPJ_pf_1M::float,
	emprestimos_credito_pPJ_pf_2M::float,
	emprestimos_credito_pPJ_pf_3M::float,
	emprestimos_credito_pPJ_pf_4M::float,
	emprestimos_credito_pPJ_pf_5M::float,
	emprestimos_credito_pPJ_pf_6M::float,
	emprestimos_credito_pPJ_pf_7M::float,
	emprestimos_credito_pPJ_pf_8M::float,
	emprestimos_credito_pPJ_pf_9M::float,
	emprestimos_credito_pPJ_pf_10M::float,
	emprestimos_credito_pPJ_pf_11M::float,
	greatest(emprestimos_credito_pPJ_pf_Curr,emprestimos_credito_pPJ_pf_1M,emprestimos_credito_pPJ_pf_2M,emprestimos_credito_pPJ_pf_3M,emprestimos_credito_pPJ_pf_4M,emprestimos_credito_pPJ_pf_5M,emprestimos_credito_pPJ_pf_6M,emprestimos_credito_pPJ_pf_7M,emprestimos_credito_pPJ_pf_8M,emprestimos_credito_pPJ_pf_9M,emprestimos_credito_pPJ_pf_10M,emprestimos_credito_pPJ_pf_11M)::float as max_emprestimos_credito_pPJ_PF,
	Ever_emprestimos_credito_pPJ_pf::float,
	case when emprestimos_credito_pPJ_pf_10M < emprestimos_credito_pPJ_pf_11M then 1 else 0 end +
	case when emprestimos_credito_pPJ_pf_9M < emprestimos_credito_pPJ_pf_10M then 1 else 0 end +
	case when emprestimos_credito_pPJ_pf_8M < emprestimos_credito_pPJ_pf_9M then 1 else 0 end +
	case when emprestimos_credito_pPJ_pf_7M < emprestimos_credito_pPJ_pf_8M then 1 else 0 end +
	case when emprestimos_credito_pPJ_pf_6M < emprestimos_credito_pPJ_pf_7M then 1 else 0 end +
	case when emprestimos_credito_pPJ_pf_5M < emprestimos_credito_pPJ_pf_6M then 1 else 0 end +
	case when emprestimos_credito_pPJ_pf_4M < emprestimos_credito_pPJ_pf_5M then 1 else 0 end +
	case when emprestimos_credito_pPJ_pf_3M < emprestimos_credito_pPJ_pf_4M then 1 else 0 end +
	case when emprestimos_credito_pPJ_pf_2M < emprestimos_credito_pPJ_pf_3M then 1 else 0 end +
	case when emprestimos_credito_pPJ_pf_1M < emprestimos_credito_pPJ_pf_2M then 1 else 0 end +
	case when emprestimos_credito_pPJ_pf_Curr < emprestimos_credito_pPJ_pf_1M then 1 else 0 end::float as Meses_Reducao_emprestimos_credito_pPJ_pf,
	case when emprestimos_credito_pPJ_pf_10M > emprestimos_credito_pPJ_pf_11M then 1 else 0 end +
	case when emprestimos_credito_pPJ_pf_9M > emprestimos_credito_pPJ_pf_10M then 1 else 0 end +
	case when emprestimos_credito_pPJ_pf_8M > emprestimos_credito_pPJ_pf_9M then 1 else 0 end +
	case when emprestimos_credito_pPJ_pf_7M > emprestimos_credito_pPJ_pf_8M then 1 else 0 end +
	case when emprestimos_credito_pPJ_pf_6M > emprestimos_credito_pPJ_pf_7M then 1 else 0 end +
	case when emprestimos_credito_pPJ_pf_5M > emprestimos_credito_pPJ_pf_6M then 1 else 0 end +
	case when emprestimos_credito_pPJ_pf_4M > emprestimos_credito_pPJ_pf_5M then 1 else 0 end +
	case when emprestimos_credito_pPJ_pf_3M > emprestimos_credito_pPJ_pf_4M then 1 else 0 end +
	case when emprestimos_credito_pPJ_pf_2M > emprestimos_credito_pPJ_pf_3M then 1 else 0 end +
	case when emprestimos_credito_pPJ_pf_1M > emprestimos_credito_pPJ_pf_2M then 1 else 0 end +
	case when emprestimos_credito_pPJ_pf_Curr > emprestimos_credito_pPJ_pf_1M then 1 else 0 end::float as Meses_Aumento_emprestimos_credito_pPJ_pf,
	case when emprestimos_credito_pPJ_pf_1M is null then null else
	case when emprestimos_credito_pPJ_pf_Curr >= emprestimos_credito_pPJ_pf_1M or emprestimos_credito_pPJ_pf_1M is null then 0 else
	case when emprestimos_credito_pPJ_pf_1M >= emprestimos_credito_pPJ_pf_2M or emprestimos_credito_pPJ_pf_2M is null then 1 else
	case when emprestimos_credito_pPJ_pf_2M >= emprestimos_credito_pPJ_pf_3M or emprestimos_credito_pPJ_pf_3M is null then 2 else
	case when emprestimos_credito_pPJ_pf_3M >= emprestimos_credito_pPJ_pf_4M or emprestimos_credito_pPJ_pf_4M is null then 3 else
	case when emprestimos_credito_pPJ_pf_4M >= emprestimos_credito_pPJ_pf_5M or emprestimos_credito_pPJ_pf_5M is null then 4 else
	case when emprestimos_credito_pPJ_pf_5M >= emprestimos_credito_pPJ_pf_6M or emprestimos_credito_pPJ_pf_6M is null then 5 else
	case when emprestimos_credito_pPJ_pf_6M >= emprestimos_credito_pPJ_pf_7M or emprestimos_credito_pPJ_pf_7M is null then 6 else
	case when emprestimos_credito_pPJ_pf_7M >= emprestimos_credito_pPJ_pf_8M or emprestimos_credito_pPJ_pf_8M is null then 7 else
	case when emprestimos_credito_pPJ_pf_8M >= emprestimos_credito_pPJ_pf_9M or emprestimos_credito_pPJ_pf_9M is null then 8 else
	case when emprestimos_credito_pPJ_pf_9M >= emprestimos_credito_pPJ_pf_10M or emprestimos_credito_pPJ_pf_10M is null then 9 else
	case when emprestimos_credito_pPJ_pf_10M >= emprestimos_credito_pPJ_pf_11M or emprestimos_credito_pPJ_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_emprestimos_credito_pPJ_pf,
	case when emprestimos_credito_pPJ_pf_1M is null then null else
	case when emprestimos_credito_pPJ_pf_Curr <= emprestimos_credito_pPJ_pf_1M or emprestimos_credito_pPJ_pf_1M is null then 0 else
	case when emprestimos_credito_pPJ_pf_1M <= emprestimos_credito_pPJ_pf_2M or emprestimos_credito_pPJ_pf_2M is null then 1 else
	case when emprestimos_credito_pPJ_pf_2M <= emprestimos_credito_pPJ_pf_3M or emprestimos_credito_pPJ_pf_3M is null then 2 else
	case when emprestimos_credito_pPJ_pf_3M <= emprestimos_credito_pPJ_pf_4M or emprestimos_credito_pPJ_pf_4M is null then 3 else
	case when emprestimos_credito_pPJ_pf_4M <= emprestimos_credito_pPJ_pf_5M or emprestimos_credito_pPJ_pf_5M is null then 4 else
	case when emprestimos_credito_pPJ_pf_5M <= emprestimos_credito_pPJ_pf_6M or emprestimos_credito_pPJ_pf_6M is null then 5 else
	case when emprestimos_credito_pPJ_pf_6M <= emprestimos_credito_pPJ_pf_7M or emprestimos_credito_pPJ_pf_7M is null then 6 else
	case when emprestimos_credito_pPJ_pf_7M <= emprestimos_credito_pPJ_pf_8M or emprestimos_credito_pPJ_pf_8M is null then 7 else
	case when emprestimos_credito_pPJ_pf_8M <= emprestimos_credito_pPJ_pf_9M or emprestimos_credito_pPJ_pf_9M is null then 8 else
	case when emprestimos_credito_pPJ_pf_9M <= emprestimos_credito_pPJ_pf_10M or emprestimos_credito_pPJ_pf_10M is null then 9 else
	case when emprestimos_credito_pPJ_pf_10M <= emprestimos_credito_pPJ_pf_11M or emprestimos_credito_pPJ_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_emprestimos_credito_pPJ_pf,
	(case when emprestimos_credito_pPJ_PF_10M < emprestimos_credito_pPJ_PF_11M then emprestimos_credito_pPJ_PF_10M - emprestimos_credito_pPJ_PF_11M else 0 end +
	case when emprestimos_credito_pPJ_PF_9M < emprestimos_credito_pPJ_PF_10M then emprestimos_credito_pPJ_PF_9M - emprestimos_credito_pPJ_PF_10M else 0 end +
	case when emprestimos_credito_pPJ_PF_8M < emprestimos_credito_pPJ_PF_9M then emprestimos_credito_pPJ_PF_8M - emprestimos_credito_pPJ_PF_9M else 0 end +
	case when emprestimos_credito_pPJ_PF_7M < emprestimos_credito_pPJ_PF_8M then emprestimos_credito_pPJ_PF_7M - emprestimos_credito_pPJ_PF_8M else 0 end +
	case when emprestimos_credito_pPJ_PF_6M < emprestimos_credito_pPJ_PF_7M then emprestimos_credito_pPJ_PF_6M - emprestimos_credito_pPJ_PF_7M else 0 end +
	case when emprestimos_credito_pPJ_PF_5M < emprestimos_credito_pPJ_PF_6M then emprestimos_credito_pPJ_PF_5M - emprestimos_credito_pPJ_PF_6M else 0 end +
	case when emprestimos_credito_pPJ_PF_4M < emprestimos_credito_pPJ_PF_5M then emprestimos_credito_pPJ_PF_4M - emprestimos_credito_pPJ_PF_5M else 0 end +
	case when emprestimos_credito_pPJ_PF_3M < emprestimos_credito_pPJ_PF_4M then emprestimos_credito_pPJ_PF_3M - emprestimos_credito_pPJ_PF_4M else 0 end +
	case when emprestimos_credito_pPJ_PF_2M < emprestimos_credito_pPJ_PF_3M then emprestimos_credito_pPJ_PF_2M - emprestimos_credito_pPJ_PF_3M else 0 end +
	case when emprestimos_credito_pPJ_PF_1M < emprestimos_credito_pPJ_PF_2M then emprestimos_credito_pPJ_PF_1M - emprestimos_credito_pPJ_PF_2M else 0 end +
	case when emprestimos_credito_pPJ_PF_Curr < emprestimos_credito_pPJ_PF_1M then emprestimos_credito_pPJ_PF_Curr - emprestimos_credito_pPJ_PF_1M else 0 end)*(-1)::float as Saldo_Amort_emprestimos_credito_pPJ_PF,
	emprestimos_credito_consignado_pf_Curr::float,
	emprestimos_credito_consignado_pf_1M::float,
	emprestimos_credito_consignado_pf_2M::float,
	emprestimos_credito_consignado_pf_3M::float,
	emprestimos_credito_consignado_pf_4M::float,
	emprestimos_credito_consignado_pf_5M::float,
	emprestimos_credito_consignado_pf_6M::float,
	emprestimos_credito_consignado_pf_7M::float,
	emprestimos_credito_consignado_pf_8M::float,
	emprestimos_credito_consignado_pf_9M::float,
	emprestimos_credito_consignado_pf_10M::float,
	emprestimos_credito_consignado_pf_11M::float,
	greatest(emprestimos_credito_consignado_pf_Curr,emprestimos_credito_consignado_pf_1M,emprestimos_credito_consignado_pf_2M,emprestimos_credito_consignado_pf_3M,emprestimos_credito_consignado_pf_4M,emprestimos_credito_consignado_pf_5M,emprestimos_credito_consignado_pf_6M,emprestimos_credito_consignado_pf_7M,emprestimos_credito_consignado_pf_8M,emprestimos_credito_consignado_pf_9M,emprestimos_credito_consignado_pf_10M,emprestimos_credito_consignado_pf_11M)::float as max_emprestimos_credito_consignado_PF,
	Ever_emprestimos_credito_consignado_pf::float,
	case when emprestimos_credito_consignado_pf_10M < emprestimos_credito_consignado_pf_11M then 1 else 0 end +
	case when emprestimos_credito_consignado_pf_9M < emprestimos_credito_consignado_pf_10M then 1 else 0 end +
	case when emprestimos_credito_consignado_pf_8M < emprestimos_credito_consignado_pf_9M then 1 else 0 end +
	case when emprestimos_credito_consignado_pf_7M < emprestimos_credito_consignado_pf_8M then 1 else 0 end +
	case when emprestimos_credito_consignado_pf_6M < emprestimos_credito_consignado_pf_7M then 1 else 0 end +
	case when emprestimos_credito_consignado_pf_5M < emprestimos_credito_consignado_pf_6M then 1 else 0 end +
	case when emprestimos_credito_consignado_pf_4M < emprestimos_credito_consignado_pf_5M then 1 else 0 end +
	case when emprestimos_credito_consignado_pf_3M < emprestimos_credito_consignado_pf_4M then 1 else 0 end +
	case when emprestimos_credito_consignado_pf_2M < emprestimos_credito_consignado_pf_3M then 1 else 0 end +
	case when emprestimos_credito_consignado_pf_1M < emprestimos_credito_consignado_pf_2M then 1 else 0 end +
	case when emprestimos_credito_consignado_pf_Curr < emprestimos_credito_consignado_pf_1M then 1 else 0 end::float as Meses_Reducao_emprestimos_credito_consignado_pf,
	case when emprestimos_credito_consignado_pf_10M > emprestimos_credito_consignado_pf_11M then 1 else 0 end +
	case when emprestimos_credito_consignado_pf_9M > emprestimos_credito_consignado_pf_10M then 1 else 0 end +
	case when emprestimos_credito_consignado_pf_8M > emprestimos_credito_consignado_pf_9M then 1 else 0 end +
	case when emprestimos_credito_consignado_pf_7M > emprestimos_credito_consignado_pf_8M then 1 else 0 end +
	case when emprestimos_credito_consignado_pf_6M > emprestimos_credito_consignado_pf_7M then 1 else 0 end +
	case when emprestimos_credito_consignado_pf_5M > emprestimos_credito_consignado_pf_6M then 1 else 0 end +
	case when emprestimos_credito_consignado_pf_4M > emprestimos_credito_consignado_pf_5M then 1 else 0 end +
	case when emprestimos_credito_consignado_pf_3M > emprestimos_credito_consignado_pf_4M then 1 else 0 end +
	case when emprestimos_credito_consignado_pf_2M > emprestimos_credito_consignado_pf_3M then 1 else 0 end +
	case when emprestimos_credito_consignado_pf_1M > emprestimos_credito_consignado_pf_2M then 1 else 0 end +
	case when emprestimos_credito_consignado_pf_Curr > emprestimos_credito_consignado_pf_1M then 1 else 0 end::float as Meses_Aumento_emprestimos_credito_consignado_pf,
	case when emprestimos_credito_consignado_pf_1M is null then null else
	case when emprestimos_credito_consignado_pf_Curr >= emprestimos_credito_consignado_pf_1M or emprestimos_credito_consignado_pf_1M is null then 0 else
	case when emprestimos_credito_consignado_pf_1M >= emprestimos_credito_consignado_pf_2M or emprestimos_credito_consignado_pf_2M is null then 1 else
	case when emprestimos_credito_consignado_pf_2M >= emprestimos_credito_consignado_pf_3M or emprestimos_credito_consignado_pf_3M is null then 2 else
	case when emprestimos_credito_consignado_pf_3M >= emprestimos_credito_consignado_pf_4M or emprestimos_credito_consignado_pf_4M is null then 3 else
	case when emprestimos_credito_consignado_pf_4M >= emprestimos_credito_consignado_pf_5M or emprestimos_credito_consignado_pf_5M is null then 4 else
	case when emprestimos_credito_consignado_pf_5M >= emprestimos_credito_consignado_pf_6M or emprestimos_credito_consignado_pf_6M is null then 5 else
	case when emprestimos_credito_consignado_pf_6M >= emprestimos_credito_consignado_pf_7M or emprestimos_credito_consignado_pf_7M is null then 6 else
	case when emprestimos_credito_consignado_pf_7M >= emprestimos_credito_consignado_pf_8M or emprestimos_credito_consignado_pf_8M is null then 7 else
	case when emprestimos_credito_consignado_pf_8M >= emprestimos_credito_consignado_pf_9M or emprestimos_credito_consignado_pf_9M is null then 8 else
	case when emprestimos_credito_consignado_pf_9M >= emprestimos_credito_consignado_pf_10M or emprestimos_credito_consignado_pf_10M is null then 9 else
	case when emprestimos_credito_consignado_pf_10M >= emprestimos_credito_consignado_pf_11M or emprestimos_credito_consignado_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_emprestimos_credito_consignado_pf,
	case when emprestimos_credito_consignado_pf_1M is null then null else
	case when emprestimos_credito_consignado_pf_Curr <= emprestimos_credito_consignado_pf_1M or emprestimos_credito_consignado_pf_1M is null then 0 else
	case when emprestimos_credito_consignado_pf_1M <= emprestimos_credito_consignado_pf_2M or emprestimos_credito_consignado_pf_2M is null then 1 else
	case when emprestimos_credito_consignado_pf_2M <= emprestimos_credito_consignado_pf_3M or emprestimos_credito_consignado_pf_3M is null then 2 else
	case when emprestimos_credito_consignado_pf_3M <= emprestimos_credito_consignado_pf_4M or emprestimos_credito_consignado_pf_4M is null then 3 else
	case when emprestimos_credito_consignado_pf_4M <= emprestimos_credito_consignado_pf_5M or emprestimos_credito_consignado_pf_5M is null then 4 else
	case when emprestimos_credito_consignado_pf_5M <= emprestimos_credito_consignado_pf_6M or emprestimos_credito_consignado_pf_6M is null then 5 else
	case when emprestimos_credito_consignado_pf_6M <= emprestimos_credito_consignado_pf_7M or emprestimos_credito_consignado_pf_7M is null then 6 else
	case when emprestimos_credito_consignado_pf_7M <= emprestimos_credito_consignado_pf_8M or emprestimos_credito_consignado_pf_8M is null then 7 else
	case when emprestimos_credito_consignado_pf_8M <= emprestimos_credito_consignado_pf_9M or emprestimos_credito_consignado_pf_9M is null then 8 else
	case when emprestimos_credito_consignado_pf_9M <= emprestimos_credito_consignado_pf_10M or emprestimos_credito_consignado_pf_10M is null then 9 else
	case when emprestimos_credito_consignado_pf_10M <= emprestimos_credito_consignado_pf_11M or emprestimos_credito_consignado_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_emprestimos_credito_consignado_pf,
	(case when emprestimos_credito_consignado_PF_10M < emprestimos_credito_consignado_PF_11M then emprestimos_credito_consignado_PF_10M - emprestimos_credito_consignado_PF_11M else 0 end +
	case when emprestimos_credito_consignado_PF_9M < emprestimos_credito_consignado_PF_10M then emprestimos_credito_consignado_PF_9M - emprestimos_credito_consignado_PF_10M else 0 end +
	case when emprestimos_credito_consignado_PF_8M < emprestimos_credito_consignado_PF_9M then emprestimos_credito_consignado_PF_8M - emprestimos_credito_consignado_PF_9M else 0 end +
	case when emprestimos_credito_consignado_PF_7M < emprestimos_credito_consignado_PF_8M then emprestimos_credito_consignado_PF_7M - emprestimos_credito_consignado_PF_8M else 0 end +
	case when emprestimos_credito_consignado_PF_6M < emprestimos_credito_consignado_PF_7M then emprestimos_credito_consignado_PF_6M - emprestimos_credito_consignado_PF_7M else 0 end +
	case when emprestimos_credito_consignado_PF_5M < emprestimos_credito_consignado_PF_6M then emprestimos_credito_consignado_PF_5M - emprestimos_credito_consignado_PF_6M else 0 end +
	case when emprestimos_credito_consignado_PF_4M < emprestimos_credito_consignado_PF_5M then emprestimos_credito_consignado_PF_4M - emprestimos_credito_consignado_PF_5M else 0 end +
	case when emprestimos_credito_consignado_PF_3M < emprestimos_credito_consignado_PF_4M then emprestimos_credito_consignado_PF_3M - emprestimos_credito_consignado_PF_4M else 0 end +
	case when emprestimos_credito_consignado_PF_2M < emprestimos_credito_consignado_PF_3M then emprestimos_credito_consignado_PF_2M - emprestimos_credito_consignado_PF_3M else 0 end +
	case when emprestimos_credito_consignado_PF_1M < emprestimos_credito_consignado_PF_2M then emprestimos_credito_consignado_PF_1M - emprestimos_credito_consignado_PF_2M else 0 end +
	case when emprestimos_credito_consignado_PF_Curr < emprestimos_credito_consignado_PF_1M then emprestimos_credito_consignado_PF_Curr - emprestimos_credito_consignado_PF_1M else 0 end)*(-1)::float as Saldo_Amort_emprestimos_credito_consignado_PF,
	emprestimos_outros_emprestimos_pf_Curr::float,
	emprestimos_outros_emprestimos_pf_1M::float,
	emprestimos_outros_emprestimos_pf_2M::float,
	emprestimos_outros_emprestimos_pf_3M::float,
	emprestimos_outros_emprestimos_pf_4M::float,
	emprestimos_outros_emprestimos_pf_5M::float,
	emprestimos_outros_emprestimos_pf_6M::float,
	emprestimos_outros_emprestimos_pf_7M::float,
	emprestimos_outros_emprestimos_pf_8M::float,
	emprestimos_outros_emprestimos_pf_9M::float,
	emprestimos_outros_emprestimos_pf_10M::float,
	emprestimos_outros_emprestimos_pf_11M::float,
	greatest(emprestimos_outros_emprestimos_pf_Curr,emprestimos_outros_emprestimos_pf_1M,emprestimos_outros_emprestimos_pf_2M,emprestimos_outros_emprestimos_pf_3M,emprestimos_outros_emprestimos_pf_4M,emprestimos_outros_emprestimos_pf_5M,emprestimos_outros_emprestimos_pf_6M,emprestimos_outros_emprestimos_pf_7M,emprestimos_outros_emprestimos_pf_8M,emprestimos_outros_emprestimos_pf_9M,emprestimos_outros_emprestimos_pf_10M,emprestimos_outros_emprestimos_pf_11M)::float as max_emprestimos_outros_emprestimos_PF,
	Ever_emprestimos_outros_emprestimos_pf::float,
	case when emprestimos_outros_emprestimos_pf_10M < emprestimos_outros_emprestimos_pf_11M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pf_9M < emprestimos_outros_emprestimos_pf_10M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pf_8M < emprestimos_outros_emprestimos_pf_9M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pf_7M < emprestimos_outros_emprestimos_pf_8M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pf_6M < emprestimos_outros_emprestimos_pf_7M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pf_5M < emprestimos_outros_emprestimos_pf_6M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pf_4M < emprestimos_outros_emprestimos_pf_5M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pf_3M < emprestimos_outros_emprestimos_pf_4M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pf_2M < emprestimos_outros_emprestimos_pf_3M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pf_1M < emprestimos_outros_emprestimos_pf_2M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pf_Curr < emprestimos_outros_emprestimos_pf_1M then 1 else 0 end::float as Meses_Reducao_emprestimos_outros_emprestimos_pf,
	case when emprestimos_outros_emprestimos_pf_10M > emprestimos_outros_emprestimos_pf_11M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pf_9M > emprestimos_outros_emprestimos_pf_10M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pf_8M > emprestimos_outros_emprestimos_pf_9M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pf_7M > emprestimos_outros_emprestimos_pf_8M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pf_6M > emprestimos_outros_emprestimos_pf_7M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pf_5M > emprestimos_outros_emprestimos_pf_6M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pf_4M > emprestimos_outros_emprestimos_pf_5M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pf_3M > emprestimos_outros_emprestimos_pf_4M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pf_2M > emprestimos_outros_emprestimos_pf_3M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pf_1M > emprestimos_outros_emprestimos_pf_2M then 1 else 0 end +
	case when emprestimos_outros_emprestimos_pf_Curr > emprestimos_outros_emprestimos_pf_1M then 1 else 0 end::float as Meses_Aumento_emprestimos_outros_emprestimos_pf,
	case when emprestimos_outros_emprestimos_pf_1M is null then null else
	case when emprestimos_outros_emprestimos_pf_Curr >= emprestimos_outros_emprestimos_pf_1M or emprestimos_outros_emprestimos_pf_1M is null then 0 else
	case when emprestimos_outros_emprestimos_pf_1M >= emprestimos_outros_emprestimos_pf_2M or emprestimos_outros_emprestimos_pf_2M is null then 1 else
	case when emprestimos_outros_emprestimos_pf_2M >= emprestimos_outros_emprestimos_pf_3M or emprestimos_outros_emprestimos_pf_3M is null then 2 else
	case when emprestimos_outros_emprestimos_pf_3M >= emprestimos_outros_emprestimos_pf_4M or emprestimos_outros_emprestimos_pf_4M is null then 3 else
	case when emprestimos_outros_emprestimos_pf_4M >= emprestimos_outros_emprestimos_pf_5M or emprestimos_outros_emprestimos_pf_5M is null then 4 else
	case when emprestimos_outros_emprestimos_pf_5M >= emprestimos_outros_emprestimos_pf_6M or emprestimos_outros_emprestimos_pf_6M is null then 5 else
	case when emprestimos_outros_emprestimos_pf_6M >= emprestimos_outros_emprestimos_pf_7M or emprestimos_outros_emprestimos_pf_7M is null then 6 else
	case when emprestimos_outros_emprestimos_pf_7M >= emprestimos_outros_emprestimos_pf_8M or emprestimos_outros_emprestimos_pf_8M is null then 7 else
	case when emprestimos_outros_emprestimos_pf_8M >= emprestimos_outros_emprestimos_pf_9M or emprestimos_outros_emprestimos_pf_9M is null then 8 else
	case when emprestimos_outros_emprestimos_pf_9M >= emprestimos_outros_emprestimos_pf_10M or emprestimos_outros_emprestimos_pf_10M is null then 9 else
	case when emprestimos_outros_emprestimos_pf_10M >= emprestimos_outros_emprestimos_pf_11M or emprestimos_outros_emprestimos_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_emprestimos_outros_emprestimos_pf,
	case when emprestimos_outros_emprestimos_pf_1M is null then null else
	case when emprestimos_outros_emprestimos_pf_Curr <= emprestimos_outros_emprestimos_pf_1M or emprestimos_outros_emprestimos_pf_1M is null then 0 else
	case when emprestimos_outros_emprestimos_pf_1M <= emprestimos_outros_emprestimos_pf_2M or emprestimos_outros_emprestimos_pf_2M is null then 1 else
	case when emprestimos_outros_emprestimos_pf_2M <= emprestimos_outros_emprestimos_pf_3M or emprestimos_outros_emprestimos_pf_3M is null then 2 else
	case when emprestimos_outros_emprestimos_pf_3M <= emprestimos_outros_emprestimos_pf_4M or emprestimos_outros_emprestimos_pf_4M is null then 3 else
	case when emprestimos_outros_emprestimos_pf_4M <= emprestimos_outros_emprestimos_pf_5M or emprestimos_outros_emprestimos_pf_5M is null then 4 else
	case when emprestimos_outros_emprestimos_pf_5M <= emprestimos_outros_emprestimos_pf_6M or emprestimos_outros_emprestimos_pf_6M is null then 5 else
	case when emprestimos_outros_emprestimos_pf_6M <= emprestimos_outros_emprestimos_pf_7M or emprestimos_outros_emprestimos_pf_7M is null then 6 else
	case when emprestimos_outros_emprestimos_pf_7M <= emprestimos_outros_emprestimos_pf_8M or emprestimos_outros_emprestimos_pf_8M is null then 7 else
	case when emprestimos_outros_emprestimos_pf_8M <= emprestimos_outros_emprestimos_pf_9M or emprestimos_outros_emprestimos_pf_9M is null then 8 else
	case when emprestimos_outros_emprestimos_pf_9M <= emprestimos_outros_emprestimos_pf_10M or emprestimos_outros_emprestimos_pf_10M is null then 9 else
	case when emprestimos_outros_emprestimos_pf_10M <= emprestimos_outros_emprestimos_pf_11M or emprestimos_outros_emprestimos_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_emprestimos_outros_emprestimos_pf,
	(case when emprestimos_outros_emprestimos_PF_10M < emprestimos_outros_emprestimos_PF_11M then emprestimos_outros_emprestimos_PF_10M - emprestimos_outros_emprestimos_PF_11M else 0 end +
	case when emprestimos_outros_emprestimos_PF_9M < emprestimos_outros_emprestimos_PF_10M then emprestimos_outros_emprestimos_PF_9M - emprestimos_outros_emprestimos_PF_10M else 0 end +
	case when emprestimos_outros_emprestimos_PF_8M < emprestimos_outros_emprestimos_PF_9M then emprestimos_outros_emprestimos_PF_8M - emprestimos_outros_emprestimos_PF_9M else 0 end +
	case when emprestimos_outros_emprestimos_PF_7M < emprestimos_outros_emprestimos_PF_8M then emprestimos_outros_emprestimos_PF_7M - emprestimos_outros_emprestimos_PF_8M else 0 end +
	case when emprestimos_outros_emprestimos_PF_6M < emprestimos_outros_emprestimos_PF_7M then emprestimos_outros_emprestimos_PF_6M - emprestimos_outros_emprestimos_PF_7M else 0 end +
	case when emprestimos_outros_emprestimos_PF_5M < emprestimos_outros_emprestimos_PF_6M then emprestimos_outros_emprestimos_PF_5M - emprestimos_outros_emprestimos_PF_6M else 0 end +
	case when emprestimos_outros_emprestimos_PF_4M < emprestimos_outros_emprestimos_PF_5M then emprestimos_outros_emprestimos_PF_4M - emprestimos_outros_emprestimos_PF_5M else 0 end +
	case when emprestimos_outros_emprestimos_PF_3M < emprestimos_outros_emprestimos_PF_4M then emprestimos_outros_emprestimos_PF_3M - emprestimos_outros_emprestimos_PF_4M else 0 end +
	case when emprestimos_outros_emprestimos_PF_2M < emprestimos_outros_emprestimos_PF_3M then emprestimos_outros_emprestimos_PF_2M - emprestimos_outros_emprestimos_PF_3M else 0 end +
	case when emprestimos_outros_emprestimos_PF_1M < emprestimos_outros_emprestimos_PF_2M then emprestimos_outros_emprestimos_PF_1M - emprestimos_outros_emprestimos_PF_2M else 0 end +
	case when emprestimos_outros_emprestimos_PF_Curr < emprestimos_outros_emprestimos_PF_1M then emprestimos_outros_emprestimos_PF_Curr - emprestimos_outros_emprestimos_PF_1M else 0 end)*(-1)::float as Saldo_Amort_emprestimos_outros_emprestimos_PF,
	OutrosCreditos_pf_Curr::float,
	OutrosCreditos_pf_1M::float,
	OutrosCreditos_pf_2M::float,
	OutrosCreditos_pf_3M::float,
	OutrosCreditos_pf_4M::float,
	OutrosCreditos_pf_5M::float,
	OutrosCreditos_pf_6M::float,
	OutrosCreditos_pf_7M::float,
	OutrosCreditos_pf_8M::float,
	OutrosCreditos_pf_9M::float,
	OutrosCreditos_pf_10M::float,
	OutrosCreditos_pf_11M::float,
	greatest(outroscreditos_pf_Curr,outroscreditos_pf_1M,outroscreditos_pf_2M,outroscreditos_pf_3M,outroscreditos_pf_4M,outroscreditos_pf_5M,outroscreditos_pf_6M,outroscreditos_pf_7M,outroscreditos_pf_8M,outroscreditos_pf_9M,outroscreditos_pf_10M,outroscreditos_pf_11M),
	Ever_OutrosCreditos_pf::float,
	case when OutrosCreditos_pf_10M < OutrosCreditos_pf_11M then 1 else 0 end +
	case when OutrosCreditos_pf_9M < OutrosCreditos_pf_10M then 1 else 0 end +
	case when OutrosCreditos_pf_8M < OutrosCreditos_pf_9M then 1 else 0 end +
	case when OutrosCreditos_pf_7M < OutrosCreditos_pf_8M then 1 else 0 end +
	case when OutrosCreditos_pf_6M < OutrosCreditos_pf_7M then 1 else 0 end +
	case when OutrosCreditos_pf_5M < OutrosCreditos_pf_6M then 1 else 0 end +
	case when OutrosCreditos_pf_4M < OutrosCreditos_pf_5M then 1 else 0 end +
	case when OutrosCreditos_pf_3M < OutrosCreditos_pf_4M then 1 else 0 end +
	case when OutrosCreditos_pf_2M < OutrosCreditos_pf_3M then 1 else 0 end +
	case when OutrosCreditos_pf_1M < OutrosCreditos_pf_2M then 1 else 0 end +
	case when OutrosCreditos_pf_Curr < OutrosCreditos_pf_1M then 1 else 0 end::float as Meses_Reducao_OutrosCreditos_pf,
	case when OutrosCreditos_pf_10M > OutrosCreditos_pf_11M then 1 else 0 end +
	case when OutrosCreditos_pf_9M > OutrosCreditos_pf_10M then 1 else 0 end +
	case when OutrosCreditos_pf_8M > OutrosCreditos_pf_9M then 1 else 0 end +
	case when OutrosCreditos_pf_7M > OutrosCreditos_pf_8M then 1 else 0 end +
	case when OutrosCreditos_pf_6M > OutrosCreditos_pf_7M then 1 else 0 end +
	case when OutrosCreditos_pf_5M > OutrosCreditos_pf_6M then 1 else 0 end +
	case when OutrosCreditos_pf_4M > OutrosCreditos_pf_5M then 1 else 0 end +
	case when OutrosCreditos_pf_3M > OutrosCreditos_pf_4M then 1 else 0 end +
	case when OutrosCreditos_pf_2M > OutrosCreditos_pf_3M then 1 else 0 end +
	case when OutrosCreditos_pf_1M > OutrosCreditos_pf_2M then 1 else 0 end +
	case when OutrosCreditos_pf_Curr > OutrosCreditos_pf_1M then 1 else 0 end::float as Meses_Aumento_OutrosCreditos_pf,
	case when OutrosCreditos_pf_1M is null then null else
	case when OutrosCreditos_pf_Curr >= OutrosCreditos_pf_1M or OutrosCreditos_pf_1M is null then 0 else
	case when OutrosCreditos_pf_1M >= OutrosCreditos_pf_2M or OutrosCreditos_pf_2M is null then 1 else
	case when OutrosCreditos_pf_2M >= OutrosCreditos_pf_3M or OutrosCreditos_pf_3M is null then 2 else
	case when OutrosCreditos_pf_3M >= OutrosCreditos_pf_4M or OutrosCreditos_pf_4M is null then 3 else
	case when OutrosCreditos_pf_4M >= OutrosCreditos_pf_5M or OutrosCreditos_pf_5M is null then 4 else
	case when OutrosCreditos_pf_5M >= OutrosCreditos_pf_6M or OutrosCreditos_pf_6M is null then 5 else
	case when OutrosCreditos_pf_6M >= OutrosCreditos_pf_7M or OutrosCreditos_pf_7M is null then 6 else
	case when OutrosCreditos_pf_7M >= OutrosCreditos_pf_8M or OutrosCreditos_pf_8M is null then 7 else
	case when OutrosCreditos_pf_8M >= OutrosCreditos_pf_9M or OutrosCreditos_pf_9M is null then 8 else
	case when OutrosCreditos_pf_9M >= OutrosCreditos_pf_10M or OutrosCreditos_pf_10M is null then 9 else
	case when OutrosCreditos_pf_10M >= OutrosCreditos_pf_11M or OutrosCreditos_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_OutrosCreditos_pf,
	case when OutrosCreditos_pf_1M is null then null else
	case when OutrosCreditos_pf_Curr <= OutrosCreditos_pf_1M or OutrosCreditos_pf_1M is null then 0 else
	case when OutrosCreditos_pf_1M <= OutrosCreditos_pf_2M or OutrosCreditos_pf_2M is null then 1 else
	case when OutrosCreditos_pf_2M <= OutrosCreditos_pf_3M or OutrosCreditos_pf_3M is null then 2 else
	case when OutrosCreditos_pf_3M <= OutrosCreditos_pf_4M or OutrosCreditos_pf_4M is null then 3 else
	case when OutrosCreditos_pf_4M <= OutrosCreditos_pf_5M or OutrosCreditos_pf_5M is null then 4 else
	case when OutrosCreditos_pf_5M <= OutrosCreditos_pf_6M or OutrosCreditos_pf_6M is null then 5 else
	case when OutrosCreditos_pf_6M <= OutrosCreditos_pf_7M or OutrosCreditos_pf_7M is null then 6 else
	case when OutrosCreditos_pf_7M <= OutrosCreditos_pf_8M or OutrosCreditos_pf_8M is null then 7 else
	case when OutrosCreditos_pf_8M <= OutrosCreditos_pf_9M or OutrosCreditos_pf_9M is null then 8 else
	case when OutrosCreditos_pf_9M <= OutrosCreditos_pf_10M or OutrosCreditos_pf_10M is null then 9 else
	case when OutrosCreditos_pf_10M <= OutrosCreditos_pf_11M or OutrosCreditos_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_OutrosCreditos_pf,
	(case when OutrosCreditos_PF_10M < OutrosCreditos_PF_11M then OutrosCreditos_PF_10M - OutrosCreditos_PF_11M else 0 end +
	case when OutrosCreditos_PF_9M < OutrosCreditos_PF_10M then OutrosCreditos_PF_9M - OutrosCreditos_PF_10M else 0 end +
	case when OutrosCreditos_PF_8M < OutrosCreditos_PF_9M then OutrosCreditos_PF_8M - OutrosCreditos_PF_9M else 0 end +
	case when OutrosCreditos_PF_7M < OutrosCreditos_PF_8M then OutrosCreditos_PF_7M - OutrosCreditos_PF_8M else 0 end +
	case when OutrosCreditos_PF_6M < OutrosCreditos_PF_7M then OutrosCreditos_PF_6M - OutrosCreditos_PF_7M else 0 end +
	case when OutrosCreditos_PF_5M < OutrosCreditos_PF_6M then OutrosCreditos_PF_5M - OutrosCreditos_PF_6M else 0 end +
	case when OutrosCreditos_PF_4M < OutrosCreditos_PF_5M then OutrosCreditos_PF_4M - OutrosCreditos_PF_5M else 0 end +
	case when OutrosCreditos_PF_3M < OutrosCreditos_PF_4M then OutrosCreditos_PF_3M - OutrosCreditos_PF_4M else 0 end +
	case when OutrosCreditos_PF_2M < OutrosCreditos_PF_3M then OutrosCreditos_PF_2M - OutrosCreditos_PF_3M else 0 end +
	case when OutrosCreditos_PF_1M < OutrosCreditos_PF_2M then OutrosCreditos_PF_1M - OutrosCreditos_PF_2M else 0 end +
	case when OutrosCreditos_PF_Curr < OutrosCreditos_PF_1M then OutrosCreditos_PF_Curr - OutrosCreditos_PF_1M else 0 end)*(-1)::float as Saldo_Amort_OutrosCreditos_PF,
	Financiamento_Veiculo_pf_Curr::float,
	Financiamento_Veiculo_pf_1M::float,
	Financiamento_Veiculo_pf_2M::float,
	Financiamento_Veiculo_pf_3M::float,
	Financiamento_Veiculo_pf_4M::float,
	Financiamento_Veiculo_pf_5M::float,
	Financiamento_Veiculo_pf_6M::float,
	Financiamento_Veiculo_pf_7M::float,
	Financiamento_Veiculo_pf_8M::float,
	Financiamento_Veiculo_pf_9M::float,
	Financiamento_Veiculo_pf_10M::float,
	Financiamento_Veiculo_pf_11M::float,
	greatest(Financiamento_Veiculo_pf_Curr,Financiamento_Veiculo_pf_1M,Financiamento_Veiculo_pf_2M,Financiamento_Veiculo_pf_3M,Financiamento_Veiculo_pf_4M,Financiamento_Veiculo_pf_5M,Financiamento_Veiculo_pf_6M,Financiamento_Veiculo_pf_7M,Financiamento_Veiculo_pf_8M,Financiamento_Veiculo_pf_9M,Financiamento_Veiculo_pf_10M,Financiamento_Veiculo_pf_11M)::float as max_Financiamento_Veiculo_PF,
	Ever_Financiamento_Veiculo_pf::float,
	case when Financiamento_Veiculo_pf_10M < Financiamento_Veiculo_pf_11M then 1 else 0 end +
	case when Financiamento_Veiculo_pf_9M < Financiamento_Veiculo_pf_10M then 1 else 0 end +
	case when Financiamento_Veiculo_pf_8M < Financiamento_Veiculo_pf_9M then 1 else 0 end +
	case when Financiamento_Veiculo_pf_7M < Financiamento_Veiculo_pf_8M then 1 else 0 end +
	case when Financiamento_Veiculo_pf_6M < Financiamento_Veiculo_pf_7M then 1 else 0 end +
	case when Financiamento_Veiculo_pf_5M < Financiamento_Veiculo_pf_6M then 1 else 0 end +
	case when Financiamento_Veiculo_pf_4M < Financiamento_Veiculo_pf_5M then 1 else 0 end +
	case when Financiamento_Veiculo_pf_3M < Financiamento_Veiculo_pf_4M then 1 else 0 end +
	case when Financiamento_Veiculo_pf_2M < Financiamento_Veiculo_pf_3M then 1 else 0 end +
	case when Financiamento_Veiculo_pf_1M < Financiamento_Veiculo_pf_2M then 1 else 0 end +
	case when Financiamento_Veiculo_pf_Curr < Financiamento_Veiculo_pf_1M then 1 else 0 end::float as Meses_Reducao_Financiamento_Veiculo_pf,
	case when Financiamento_Veiculo_pf_10M > Financiamento_Veiculo_pf_11M then 1 else 0 end +
	case when Financiamento_Veiculo_pf_9M > Financiamento_Veiculo_pf_10M then 1 else 0 end +
	case when Financiamento_Veiculo_pf_8M > Financiamento_Veiculo_pf_9M then 1 else 0 end +
	case when Financiamento_Veiculo_pf_7M > Financiamento_Veiculo_pf_8M then 1 else 0 end +
	case when Financiamento_Veiculo_pf_6M > Financiamento_Veiculo_pf_7M then 1 else 0 end +
	case when Financiamento_Veiculo_pf_5M > Financiamento_Veiculo_pf_6M then 1 else 0 end +
	case when Financiamento_Veiculo_pf_4M > Financiamento_Veiculo_pf_5M then 1 else 0 end +
	case when Financiamento_Veiculo_pf_3M > Financiamento_Veiculo_pf_4M then 1 else 0 end +
	case when Financiamento_Veiculo_pf_2M > Financiamento_Veiculo_pf_3M then 1 else 0 end +
	case when Financiamento_Veiculo_pf_1M > Financiamento_Veiculo_pf_2M then 1 else 0 end +
	case when Financiamento_Veiculo_pf_Curr > Financiamento_Veiculo_pf_1M then 1 else 0 end::float as Meses_Aumento_Financiamento_Veiculo_pf,
	case when Financiamento_Veiculo_pf_1M is null then null else
	case when Financiamento_Veiculo_pf_Curr >= Financiamento_Veiculo_pf_1M or Financiamento_Veiculo_pf_1M is null then 0 else
	case when Financiamento_Veiculo_pf_1M >= Financiamento_Veiculo_pf_2M or Financiamento_Veiculo_pf_2M is null then 1 else
	case when Financiamento_Veiculo_pf_2M >= Financiamento_Veiculo_pf_3M or Financiamento_Veiculo_pf_3M is null then 2 else
	case when Financiamento_Veiculo_pf_3M >= Financiamento_Veiculo_pf_4M or Financiamento_Veiculo_pf_4M is null then 3 else
	case when Financiamento_Veiculo_pf_4M >= Financiamento_Veiculo_pf_5M or Financiamento_Veiculo_pf_5M is null then 4 else
	case when Financiamento_Veiculo_pf_5M >= Financiamento_Veiculo_pf_6M or Financiamento_Veiculo_pf_6M is null then 5 else
	case when Financiamento_Veiculo_pf_6M >= Financiamento_Veiculo_pf_7M or Financiamento_Veiculo_pf_7M is null then 6 else
	case when Financiamento_Veiculo_pf_7M >= Financiamento_Veiculo_pf_8M or Financiamento_Veiculo_pf_8M is null then 7 else
	case when Financiamento_Veiculo_pf_8M >= Financiamento_Veiculo_pf_9M or Financiamento_Veiculo_pf_9M is null then 8 else
	case when Financiamento_Veiculo_pf_9M >= Financiamento_Veiculo_pf_10M or Financiamento_Veiculo_pf_10M is null then 9 else
	case when Financiamento_Veiculo_pf_10M >= Financiamento_Veiculo_pf_11M or Financiamento_Veiculo_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_Financiamento_Veiculo_pf,
	case when Financiamento_Veiculo_pf_1M is null then null else
	case when Financiamento_Veiculo_pf_Curr <= Financiamento_Veiculo_pf_1M or Financiamento_Veiculo_pf_1M is null then 0 else
	case when Financiamento_Veiculo_pf_1M <= Financiamento_Veiculo_pf_2M or Financiamento_Veiculo_pf_2M is null then 1 else
	case when Financiamento_Veiculo_pf_2M <= Financiamento_Veiculo_pf_3M or Financiamento_Veiculo_pf_3M is null then 2 else
	case when Financiamento_Veiculo_pf_3M <= Financiamento_Veiculo_pf_4M or Financiamento_Veiculo_pf_4M is null then 3 else
	case when Financiamento_Veiculo_pf_4M <= Financiamento_Veiculo_pf_5M or Financiamento_Veiculo_pf_5M is null then 4 else
	case when Financiamento_Veiculo_pf_5M <= Financiamento_Veiculo_pf_6M or Financiamento_Veiculo_pf_6M is null then 5 else
	case when Financiamento_Veiculo_pf_6M <= Financiamento_Veiculo_pf_7M or Financiamento_Veiculo_pf_7M is null then 6 else
	case when Financiamento_Veiculo_pf_7M <= Financiamento_Veiculo_pf_8M or Financiamento_Veiculo_pf_8M is null then 7 else
	case when Financiamento_Veiculo_pf_8M <= Financiamento_Veiculo_pf_9M or Financiamento_Veiculo_pf_9M is null then 8 else
	case when Financiamento_Veiculo_pf_9M <= Financiamento_Veiculo_pf_10M or Financiamento_Veiculo_pf_10M is null then 9 else
	case when Financiamento_Veiculo_pf_10M <= Financiamento_Veiculo_pf_11M or Financiamento_Veiculo_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_Financiamento_Veiculo_pf,
	(case when Financiamento_Veiculo_PF_10M < Financiamento_Veiculo_PF_11M then Financiamento_Veiculo_PF_10M - Financiamento_Veiculo_PF_11M else 0 end +
	case when Financiamento_Veiculo_PF_9M < Financiamento_Veiculo_PF_10M then Financiamento_Veiculo_PF_9M - Financiamento_Veiculo_PF_10M else 0 end +
	case when Financiamento_Veiculo_PF_8M < Financiamento_Veiculo_PF_9M then Financiamento_Veiculo_PF_8M - Financiamento_Veiculo_PF_9M else 0 end +
	case when Financiamento_Veiculo_PF_7M < Financiamento_Veiculo_PF_8M then Financiamento_Veiculo_PF_7M - Financiamento_Veiculo_PF_8M else 0 end +
	case when Financiamento_Veiculo_PF_6M < Financiamento_Veiculo_PF_7M then Financiamento_Veiculo_PF_6M - Financiamento_Veiculo_PF_7M else 0 end +
	case when Financiamento_Veiculo_PF_5M < Financiamento_Veiculo_PF_6M then Financiamento_Veiculo_PF_5M - Financiamento_Veiculo_PF_6M else 0 end +
	case when Financiamento_Veiculo_PF_4M < Financiamento_Veiculo_PF_5M then Financiamento_Veiculo_PF_4M - Financiamento_Veiculo_PF_5M else 0 end +
	case when Financiamento_Veiculo_PF_3M < Financiamento_Veiculo_PF_4M then Financiamento_Veiculo_PF_3M - Financiamento_Veiculo_PF_4M else 0 end +
	case when Financiamento_Veiculo_PF_2M < Financiamento_Veiculo_PF_3M then Financiamento_Veiculo_PF_2M - Financiamento_Veiculo_PF_3M else 0 end +
	case when Financiamento_Veiculo_PF_1M < Financiamento_Veiculo_PF_2M then Financiamento_Veiculo_PF_1M - Financiamento_Veiculo_PF_2M else 0 end +
	case when Financiamento_Veiculo_PF_Curr < Financiamento_Veiculo_PF_1M then Financiamento_Veiculo_PF_Curr - Financiamento_Veiculo_PF_1M else 0 end)*(-1)::float as Saldo_Amort_Financiamento_Veiculo_PF,
	Financiamento_Imobiliario_pf_Curr::float,
	Financiamento_Imobiliario_pf_1M::float,
	Financiamento_Imobiliario_pf_2M::float,
	Financiamento_Imobiliario_pf_3M::float,
	Financiamento_Imobiliario_pf_4M::float,
	Financiamento_Imobiliario_pf_5M::float,
	Financiamento_Imobiliario_pf_6M::float,
	Financiamento_Imobiliario_pf_7M::float,
	Financiamento_Imobiliario_pf_8M::float,
	Financiamento_Imobiliario_pf_9M::float,
	Financiamento_Imobiliario_pf_10M::float,
	Financiamento_Imobiliario_pf_11M::float,
	greatest(Financiamento_Imobiliario_pf_Curr,Financiamento_Imobiliario_pf_1M,Financiamento_Imobiliario_pf_2M,Financiamento_Imobiliario_pf_3M,Financiamento_Imobiliario_pf_4M,Financiamento_Imobiliario_pf_5M,Financiamento_Imobiliario_pf_6M,Financiamento_Imobiliario_pf_7M,Financiamento_Imobiliario_pf_8M,Financiamento_Imobiliario_pf_9M,Financiamento_Imobiliario_pf_10M,Financiamento_Imobiliario_pf_11M)::float as max_Financiamento_Imobiliario_PF,
	Ever_Financiamento_Imobiliario_pf::float,
	case when Financiamento_Imobiliario_pf_10M < Financiamento_Imobiliario_pf_11M then 1 else 0 end +
	case when Financiamento_Imobiliario_pf_9M < Financiamento_Imobiliario_pf_10M then 1 else 0 end +
	case when Financiamento_Imobiliario_pf_8M < Financiamento_Imobiliario_pf_9M then 1 else 0 end +
	case when Financiamento_Imobiliario_pf_7M < Financiamento_Imobiliario_pf_8M then 1 else 0 end +
	case when Financiamento_Imobiliario_pf_6M < Financiamento_Imobiliario_pf_7M then 1 else 0 end +
	case when Financiamento_Imobiliario_pf_5M < Financiamento_Imobiliario_pf_6M then 1 else 0 end +
	case when Financiamento_Imobiliario_pf_4M < Financiamento_Imobiliario_pf_5M then 1 else 0 end +
	case when Financiamento_Imobiliario_pf_3M < Financiamento_Imobiliario_pf_4M then 1 else 0 end +
	case when Financiamento_Imobiliario_pf_2M < Financiamento_Imobiliario_pf_3M then 1 else 0 end +
	case when Financiamento_Imobiliario_pf_1M < Financiamento_Imobiliario_pf_2M then 1 else 0 end +
	case when Financiamento_Imobiliario_pf_Curr < Financiamento_Imobiliario_pf_1M then 1 else 0 end::float as Meses_Reducao_Financiamento_Imobiliario_pf,
	case when Financiamento_Imobiliario_pf_10M > Financiamento_Imobiliario_pf_11M then 1 else 0 end +
	case when Financiamento_Imobiliario_pf_9M > Financiamento_Imobiliario_pf_10M then 1 else 0 end +
	case when Financiamento_Imobiliario_pf_8M > Financiamento_Imobiliario_pf_9M then 1 else 0 end +
	case when Financiamento_Imobiliario_pf_7M > Financiamento_Imobiliario_pf_8M then 1 else 0 end +
	case when Financiamento_Imobiliario_pf_6M > Financiamento_Imobiliario_pf_7M then 1 else 0 end +
	case when Financiamento_Imobiliario_pf_5M > Financiamento_Imobiliario_pf_6M then 1 else 0 end +
	case when Financiamento_Imobiliario_pf_4M > Financiamento_Imobiliario_pf_5M then 1 else 0 end +
	case when Financiamento_Imobiliario_pf_3M > Financiamento_Imobiliario_pf_4M then 1 else 0 end +
	case when Financiamento_Imobiliario_pf_2M > Financiamento_Imobiliario_pf_3M then 1 else 0 end +
	case when Financiamento_Imobiliario_pf_1M > Financiamento_Imobiliario_pf_2M then 1 else 0 end +
	case when Financiamento_Imobiliario_pf_Curr > Financiamento_Imobiliario_pf_1M then 1 else 0 end::float as Meses_Aumento_Financiamento_Imobiliario_pf,
	case when Financiamento_Imobiliario_pf_1M is null then null else
	case when Financiamento_Imobiliario_pf_Curr >= Financiamento_Imobiliario_pf_1M or Financiamento_Imobiliario_pf_1M is null then 0 else
	case when Financiamento_Imobiliario_pf_1M >= Financiamento_Imobiliario_pf_2M or Financiamento_Imobiliario_pf_2M is null then 1 else
	case when Financiamento_Imobiliario_pf_2M >= Financiamento_Imobiliario_pf_3M or Financiamento_Imobiliario_pf_3M is null then 2 else
	case when Financiamento_Imobiliario_pf_3M >= Financiamento_Imobiliario_pf_4M or Financiamento_Imobiliario_pf_4M is null then 3 else
	case when Financiamento_Imobiliario_pf_4M >= Financiamento_Imobiliario_pf_5M or Financiamento_Imobiliario_pf_5M is null then 4 else
	case when Financiamento_Imobiliario_pf_5M >= Financiamento_Imobiliario_pf_6M or Financiamento_Imobiliario_pf_6M is null then 5 else
	case when Financiamento_Imobiliario_pf_6M >= Financiamento_Imobiliario_pf_7M or Financiamento_Imobiliario_pf_7M is null then 6 else
	case when Financiamento_Imobiliario_pf_7M >= Financiamento_Imobiliario_pf_8M or Financiamento_Imobiliario_pf_8M is null then 7 else
	case when Financiamento_Imobiliario_pf_8M >= Financiamento_Imobiliario_pf_9M or Financiamento_Imobiliario_pf_9M is null then 8 else
	case when Financiamento_Imobiliario_pf_9M >= Financiamento_Imobiliario_pf_10M or Financiamento_Imobiliario_pf_10M is null then 9 else
	case when Financiamento_Imobiliario_pf_10M >= Financiamento_Imobiliario_pf_11M or Financiamento_Imobiliario_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_Financiamento_Imobiliario_pf,
	case when Financiamento_Imobiliario_pf_1M is null then null else
	case when Financiamento_Imobiliario_pf_Curr <= Financiamento_Imobiliario_pf_1M or Financiamento_Imobiliario_pf_1M is null then 0 else
	case when Financiamento_Imobiliario_pf_1M <= Financiamento_Imobiliario_pf_2M or Financiamento_Imobiliario_pf_2M is null then 1 else
	case when Financiamento_Imobiliario_pf_2M <= Financiamento_Imobiliario_pf_3M or Financiamento_Imobiliario_pf_3M is null then 2 else
	case when Financiamento_Imobiliario_pf_3M <= Financiamento_Imobiliario_pf_4M or Financiamento_Imobiliario_pf_4M is null then 3 else
	case when Financiamento_Imobiliario_pf_4M <= Financiamento_Imobiliario_pf_5M or Financiamento_Imobiliario_pf_5M is null then 4 else
	case when Financiamento_Imobiliario_pf_5M <= Financiamento_Imobiliario_pf_6M or Financiamento_Imobiliario_pf_6M is null then 5 else
	case when Financiamento_Imobiliario_pf_6M <= Financiamento_Imobiliario_pf_7M or Financiamento_Imobiliario_pf_7M is null then 6 else
	case when Financiamento_Imobiliario_pf_7M <= Financiamento_Imobiliario_pf_8M or Financiamento_Imobiliario_pf_8M is null then 7 else
	case when Financiamento_Imobiliario_pf_8M <= Financiamento_Imobiliario_pf_9M or Financiamento_Imobiliario_pf_9M is null then 8 else
	case when Financiamento_Imobiliario_pf_9M <= Financiamento_Imobiliario_pf_10M or Financiamento_Imobiliario_pf_10M is null then 9 else
	case when Financiamento_Imobiliario_pf_10M <= Financiamento_Imobiliario_pf_11M or Financiamento_Imobiliario_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_Financiamento_Imobiliario_pf,
	(case when Financiamento_Imobiliario_PF_10M < Financiamento_Imobiliario_PF_11M then Financiamento_Imobiliario_PF_10M - Financiamento_Imobiliario_PF_11M else 0 end +
	case when Financiamento_Imobiliario_PF_9M < Financiamento_Imobiliario_PF_10M then Financiamento_Imobiliario_PF_9M - Financiamento_Imobiliario_PF_10M else 0 end +
	case when Financiamento_Imobiliario_PF_8M < Financiamento_Imobiliario_PF_9M then Financiamento_Imobiliario_PF_8M - Financiamento_Imobiliario_PF_9M else 0 end +
	case when Financiamento_Imobiliario_PF_7M < Financiamento_Imobiliario_PF_8M then Financiamento_Imobiliario_PF_7M - Financiamento_Imobiliario_PF_8M else 0 end +
	case when Financiamento_Imobiliario_PF_6M < Financiamento_Imobiliario_PF_7M then Financiamento_Imobiliario_PF_6M - Financiamento_Imobiliario_PF_7M else 0 end +
	case when Financiamento_Imobiliario_PF_5M < Financiamento_Imobiliario_PF_6M then Financiamento_Imobiliario_PF_5M - Financiamento_Imobiliario_PF_6M else 0 end +
	case when Financiamento_Imobiliario_PF_4M < Financiamento_Imobiliario_PF_5M then Financiamento_Imobiliario_PF_4M - Financiamento_Imobiliario_PF_5M else 0 end +
	case when Financiamento_Imobiliario_PF_3M < Financiamento_Imobiliario_PF_4M then Financiamento_Imobiliario_PF_3M - Financiamento_Imobiliario_PF_4M else 0 end +
	case when Financiamento_Imobiliario_PF_2M < Financiamento_Imobiliario_PF_3M then Financiamento_Imobiliario_PF_2M - Financiamento_Imobiliario_PF_3M else 0 end +
	case when Financiamento_Imobiliario_PF_1M < Financiamento_Imobiliario_PF_2M then Financiamento_Imobiliario_PF_1M - Financiamento_Imobiliario_PF_2M else 0 end +
	case when Financiamento_Imobiliario_PF_Curr < Financiamento_Imobiliario_PF_1M then Financiamento_Imobiliario_PF_Curr - Financiamento_Imobiliario_PF_1M else 0 end)*(-1)::float as Saldo_Amort_Financiamento_Imobiliario_PF,
	AdiantamentosDescontos_pf_Curr::float,
	AdiantamentosDescontos_pf_1M::float,
	AdiantamentosDescontos_pf_2M::float,
	AdiantamentosDescontos_pf_3M::float,
	AdiantamentosDescontos_pf_4M::float,
	AdiantamentosDescontos_pf_5M::float,
	AdiantamentosDescontos_pf_6M::float,
	AdiantamentosDescontos_pf_7M::float,
	AdiantamentosDescontos_pf_8M::float,
	AdiantamentosDescontos_pf_9M::float,
	AdiantamentosDescontos_pf_10M::float,
	AdiantamentosDescontos_pf_11M::float,
	greatest(AdiantamentosDescontos_pf_Curr,AdiantamentosDescontos_pf_1M,AdiantamentosDescontos_pf_2M,AdiantamentosDescontos_pf_3M,AdiantamentosDescontos_pf_4M,AdiantamentosDescontos_pf_5M,AdiantamentosDescontos_pf_6M,AdiantamentosDescontos_pf_7M,AdiantamentosDescontos_pf_8M,AdiantamentosDescontos_pf_9M,AdiantamentosDescontos_pf_10M,AdiantamentosDescontos_pf_11M)::float as max_AdiantamentosDescontos_PF,
	Ever_AdiantamentosDescontos_pf::float,
	case when AdiantamentosDescontos_pf_10M < AdiantamentosDescontos_pf_11M then 1 else 0 end +
	case when AdiantamentosDescontos_pf_9M < AdiantamentosDescontos_pf_10M then 1 else 0 end +
	case when AdiantamentosDescontos_pf_8M < AdiantamentosDescontos_pf_9M then 1 else 0 end +
	case when AdiantamentosDescontos_pf_7M < AdiantamentosDescontos_pf_8M then 1 else 0 end +
	case when AdiantamentosDescontos_pf_6M < AdiantamentosDescontos_pf_7M then 1 else 0 end +
	case when AdiantamentosDescontos_pf_5M < AdiantamentosDescontos_pf_6M then 1 else 0 end +
	case when AdiantamentosDescontos_pf_4M < AdiantamentosDescontos_pf_5M then 1 else 0 end +
	case when AdiantamentosDescontos_pf_3M < AdiantamentosDescontos_pf_4M then 1 else 0 end +
	case when AdiantamentosDescontos_pf_2M < AdiantamentosDescontos_pf_3M then 1 else 0 end +
	case when AdiantamentosDescontos_pf_1M < AdiantamentosDescontos_pf_2M then 1 else 0 end +
	case when AdiantamentosDescontos_pf_Curr < AdiantamentosDescontos_pf_1M then 1 else 0 end::float as Meses_Reducao_AdiantamentosDescontos_pf,
	case when AdiantamentosDescontos_pf_10M > AdiantamentosDescontos_pf_11M then 1 else 0 end +
	case when AdiantamentosDescontos_pf_9M > AdiantamentosDescontos_pf_10M then 1 else 0 end +
	case when AdiantamentosDescontos_pf_8M > AdiantamentosDescontos_pf_9M then 1 else 0 end +
	case when AdiantamentosDescontos_pf_7M > AdiantamentosDescontos_pf_8M then 1 else 0 end +
	case when AdiantamentosDescontos_pf_6M > AdiantamentosDescontos_pf_7M then 1 else 0 end +
	case when AdiantamentosDescontos_pf_5M > AdiantamentosDescontos_pf_6M then 1 else 0 end +
	case when AdiantamentosDescontos_pf_4M > AdiantamentosDescontos_pf_5M then 1 else 0 end +
	case when AdiantamentosDescontos_pf_3M > AdiantamentosDescontos_pf_4M then 1 else 0 end +
	case when AdiantamentosDescontos_pf_2M > AdiantamentosDescontos_pf_3M then 1 else 0 end +
	case when AdiantamentosDescontos_pf_1M > AdiantamentosDescontos_pf_2M then 1 else 0 end +
	case when AdiantamentosDescontos_pf_Curr > AdiantamentosDescontos_pf_1M then 1 else 0 end::float as Meses_Aumento_AdiantamentosDescontos_pf,
	case when AdiantamentosDescontos_pf_1M is null then null else
	case when AdiantamentosDescontos_pf_Curr >= AdiantamentosDescontos_pf_1M or AdiantamentosDescontos_pf_1M is null then 0 else
	case when AdiantamentosDescontos_pf_1M >= AdiantamentosDescontos_pf_2M or AdiantamentosDescontos_pf_2M is null then 1 else
	case when AdiantamentosDescontos_pf_2M >= AdiantamentosDescontos_pf_3M or AdiantamentosDescontos_pf_3M is null then 2 else
	case when AdiantamentosDescontos_pf_3M >= AdiantamentosDescontos_pf_4M or AdiantamentosDescontos_pf_4M is null then 3 else
	case when AdiantamentosDescontos_pf_4M >= AdiantamentosDescontos_pf_5M or AdiantamentosDescontos_pf_5M is null then 4 else
	case when AdiantamentosDescontos_pf_5M >= AdiantamentosDescontos_pf_6M or AdiantamentosDescontos_pf_6M is null then 5 else
	case when AdiantamentosDescontos_pf_6M >= AdiantamentosDescontos_pf_7M or AdiantamentosDescontos_pf_7M is null then 6 else
	case when AdiantamentosDescontos_pf_7M >= AdiantamentosDescontos_pf_8M or AdiantamentosDescontos_pf_8M is null then 7 else
	case when AdiantamentosDescontos_pf_8M >= AdiantamentosDescontos_pf_9M or AdiantamentosDescontos_pf_9M is null then 8 else
	case when AdiantamentosDescontos_pf_9M >= AdiantamentosDescontos_pf_10M or AdiantamentosDescontos_pf_10M is null then 9 else
	case when AdiantamentosDescontos_pf_10M >= AdiantamentosDescontos_pf_11M or AdiantamentosDescontos_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_AdiantamentosDescontos_pf,
	case when AdiantamentosDescontos_pf_1M is null then null else
	case when AdiantamentosDescontos_pf_Curr <= AdiantamentosDescontos_pf_1M or AdiantamentosDescontos_pf_1M is null then 0 else
	case when AdiantamentosDescontos_pf_1M <= AdiantamentosDescontos_pf_2M or AdiantamentosDescontos_pf_2M is null then 1 else
	case when AdiantamentosDescontos_pf_2M <= AdiantamentosDescontos_pf_3M or AdiantamentosDescontos_pf_3M is null then 2 else
	case when AdiantamentosDescontos_pf_3M <= AdiantamentosDescontos_pf_4M or AdiantamentosDescontos_pf_4M is null then 3 else
	case when AdiantamentosDescontos_pf_4M <= AdiantamentosDescontos_pf_5M or AdiantamentosDescontos_pf_5M is null then 4 else
	case when AdiantamentosDescontos_pf_5M <= AdiantamentosDescontos_pf_6M or AdiantamentosDescontos_pf_6M is null then 5 else
	case when AdiantamentosDescontos_pf_6M <= AdiantamentosDescontos_pf_7M or AdiantamentosDescontos_pf_7M is null then 6 else
	case when AdiantamentosDescontos_pf_7M <= AdiantamentosDescontos_pf_8M or AdiantamentosDescontos_pf_8M is null then 7 else
	case when AdiantamentosDescontos_pf_8M <= AdiantamentosDescontos_pf_9M or AdiantamentosDescontos_pf_9M is null then 8 else
	case when AdiantamentosDescontos_pf_9M <= AdiantamentosDescontos_pf_10M or AdiantamentosDescontos_pf_10M is null then 9 else
	case when AdiantamentosDescontos_pf_10M <= AdiantamentosDescontos_pf_11M or AdiantamentosDescontos_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_AdiantamentosDescontos_pf,
	(case when AdiantamentosDescontos_PF_10M < AdiantamentosDescontos_PF_11M then AdiantamentosDescontos_PF_10M - AdiantamentosDescontos_PF_11M else 0 end +
	case when AdiantamentosDescontos_PF_9M < AdiantamentosDescontos_PF_10M then AdiantamentosDescontos_PF_9M - AdiantamentosDescontos_PF_10M else 0 end +
	case when AdiantamentosDescontos_PF_8M < AdiantamentosDescontos_PF_9M then AdiantamentosDescontos_PF_8M - AdiantamentosDescontos_PF_9M else 0 end +
	case when AdiantamentosDescontos_PF_7M < AdiantamentosDescontos_PF_8M then AdiantamentosDescontos_PF_7M - AdiantamentosDescontos_PF_8M else 0 end +
	case when AdiantamentosDescontos_PF_6M < AdiantamentosDescontos_PF_7M then AdiantamentosDescontos_PF_6M - AdiantamentosDescontos_PF_7M else 0 end +
	case when AdiantamentosDescontos_PF_5M < AdiantamentosDescontos_PF_6M then AdiantamentosDescontos_PF_5M - AdiantamentosDescontos_PF_6M else 0 end +
	case when AdiantamentosDescontos_PF_4M < AdiantamentosDescontos_PF_5M then AdiantamentosDescontos_PF_4M - AdiantamentosDescontos_PF_5M else 0 end +
	case when AdiantamentosDescontos_PF_3M < AdiantamentosDescontos_PF_4M then AdiantamentosDescontos_PF_3M - AdiantamentosDescontos_PF_4M else 0 end +
	case when AdiantamentosDescontos_PF_2M < AdiantamentosDescontos_PF_3M then AdiantamentosDescontos_PF_2M - AdiantamentosDescontos_PF_3M else 0 end +
	case when AdiantamentosDescontos_PF_1M < AdiantamentosDescontos_PF_2M then AdiantamentosDescontos_PF_1M - AdiantamentosDescontos_PF_2M else 0 end +
	case when AdiantamentosDescontos_PF_Curr < AdiantamentosDescontos_PF_1M then AdiantamentosDescontos_PF_Curr - AdiantamentosDescontos_PF_1M else 0 end)*(-1)::float as Saldo_Amort_AdiantamentosDescontos_PF,
	OutrosFinanciamentos_pf_Curr::float,
	OutrosFinanciamentos_pf_1M::float,
	OutrosFinanciamentos_pf_2M::float,
	OutrosFinanciamentos_pf_3M::float,
	OutrosFinanciamentos_pf_4M::float,
	OutrosFinanciamentos_pf_5M::float,
	OutrosFinanciamentos_pf_6M::float,
	OutrosFinanciamentos_pf_7M::float,
	OutrosFinanciamentos_pf_8M::float,
	OutrosFinanciamentos_pf_9M::float,
	OutrosFinanciamentos_pf_10M::float,
	OutrosFinanciamentos_pf_11M::float,
	greatest(OutrosFinanciamentos_pf_Curr,OutrosFinanciamentos_pf_1M,OutrosFinanciamentos_pf_2M,OutrosFinanciamentos_pf_3M,OutrosFinanciamentos_pf_4M,OutrosFinanciamentos_pf_5M,OutrosFinanciamentos_pf_6M,OutrosFinanciamentos_pf_7M,OutrosFinanciamentos_pf_8M,OutrosFinanciamentos_pf_9M,OutrosFinanciamentos_pf_10M,OutrosFinanciamentos_pf_11M)::float as max_OutrosFinanciamentos_PF,
	Ever_OutrosFinanciamentos_pf::float,
	case when OutrosFinanciamentos_pf_10M < OutrosFinanciamentos_pf_11M then 1 else 0 end +
	case when OutrosFinanciamentos_pf_9M < OutrosFinanciamentos_pf_10M then 1 else 0 end +
	case when OutrosFinanciamentos_pf_8M < OutrosFinanciamentos_pf_9M then 1 else 0 end +
	case when OutrosFinanciamentos_pf_7M < OutrosFinanciamentos_pf_8M then 1 else 0 end +
	case when OutrosFinanciamentos_pf_6M < OutrosFinanciamentos_pf_7M then 1 else 0 end +
	case when OutrosFinanciamentos_pf_5M < OutrosFinanciamentos_pf_6M then 1 else 0 end +
	case when OutrosFinanciamentos_pf_4M < OutrosFinanciamentos_pf_5M then 1 else 0 end +
	case when OutrosFinanciamentos_pf_3M < OutrosFinanciamentos_pf_4M then 1 else 0 end +
	case when OutrosFinanciamentos_pf_2M < OutrosFinanciamentos_pf_3M then 1 else 0 end +
	case when OutrosFinanciamentos_pf_1M < OutrosFinanciamentos_pf_2M then 1 else 0 end +
	case when OutrosFinanciamentos_pf_Curr < OutrosFinanciamentos_pf_1M then 1 else 0 end::float as Meses_Reducao_OutrosFinanciamentos_pf,
	case when OutrosFinanciamentos_pf_10M > OutrosFinanciamentos_pf_11M then 1 else 0 end +
	case when OutrosFinanciamentos_pf_9M > OutrosFinanciamentos_pf_10M then 1 else 0 end +
	case when OutrosFinanciamentos_pf_8M > OutrosFinanciamentos_pf_9M then 1 else 0 end +
	case when OutrosFinanciamentos_pf_7M > OutrosFinanciamentos_pf_8M then 1 else 0 end +
	case when OutrosFinanciamentos_pf_6M > OutrosFinanciamentos_pf_7M then 1 else 0 end +
	case when OutrosFinanciamentos_pf_5M > OutrosFinanciamentos_pf_6M then 1 else 0 end +
	case when OutrosFinanciamentos_pf_4M > OutrosFinanciamentos_pf_5M then 1 else 0 end +
	case when OutrosFinanciamentos_pf_3M > OutrosFinanciamentos_pf_4M then 1 else 0 end +
	case when OutrosFinanciamentos_pf_2M > OutrosFinanciamentos_pf_3M then 1 else 0 end +
	case when OutrosFinanciamentos_pf_1M > OutrosFinanciamentos_pf_2M then 1 else 0 end +
	case when OutrosFinanciamentos_pf_Curr > OutrosFinanciamentos_pf_1M then 1 else 0 end::float as Meses_Aumento_OutrosFinanciamentos_pf,
	case when OutrosFinanciamentos_pf_1M is null then null else
	case when OutrosFinanciamentos_pf_Curr >= OutrosFinanciamentos_pf_1M or OutrosFinanciamentos_pf_1M is null then 0 else
	case when OutrosFinanciamentos_pf_1M >= OutrosFinanciamentos_pf_2M or OutrosFinanciamentos_pf_2M is null then 1 else
	case when OutrosFinanciamentos_pf_2M >= OutrosFinanciamentos_pf_3M or OutrosFinanciamentos_pf_3M is null then 2 else
	case when OutrosFinanciamentos_pf_3M >= OutrosFinanciamentos_pf_4M or OutrosFinanciamentos_pf_4M is null then 3 else
	case when OutrosFinanciamentos_pf_4M >= OutrosFinanciamentos_pf_5M or OutrosFinanciamentos_pf_5M is null then 4 else
	case when OutrosFinanciamentos_pf_5M >= OutrosFinanciamentos_pf_6M or OutrosFinanciamentos_pf_6M is null then 5 else
	case when OutrosFinanciamentos_pf_6M >= OutrosFinanciamentos_pf_7M or OutrosFinanciamentos_pf_7M is null then 6 else
	case when OutrosFinanciamentos_pf_7M >= OutrosFinanciamentos_pf_8M or OutrosFinanciamentos_pf_8M is null then 7 else
	case when OutrosFinanciamentos_pf_8M >= OutrosFinanciamentos_pf_9M or OutrosFinanciamentos_pf_9M is null then 8 else
	case when OutrosFinanciamentos_pf_9M >= OutrosFinanciamentos_pf_10M or OutrosFinanciamentos_pf_10M is null then 9 else
	case when OutrosFinanciamentos_pf_10M >= OutrosFinanciamentos_pf_11M or OutrosFinanciamentos_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_OutrosFinanciamentos_pf,
	case when OutrosFinanciamentos_pf_1M is null then null else
	case when OutrosFinanciamentos_pf_Curr <= OutrosFinanciamentos_pf_1M or OutrosFinanciamentos_pf_1M is null then 0 else
	case when OutrosFinanciamentos_pf_1M <= OutrosFinanciamentos_pf_2M or OutrosFinanciamentos_pf_2M is null then 1 else
	case when OutrosFinanciamentos_pf_2M <= OutrosFinanciamentos_pf_3M or OutrosFinanciamentos_pf_3M is null then 2 else
	case when OutrosFinanciamentos_pf_3M <= OutrosFinanciamentos_pf_4M or OutrosFinanciamentos_pf_4M is null then 3 else
	case when OutrosFinanciamentos_pf_4M <= OutrosFinanciamentos_pf_5M or OutrosFinanciamentos_pf_5M is null then 4 else
	case when OutrosFinanciamentos_pf_5M <= OutrosFinanciamentos_pf_6M or OutrosFinanciamentos_pf_6M is null then 5 else
	case when OutrosFinanciamentos_pf_6M <= OutrosFinanciamentos_pf_7M or OutrosFinanciamentos_pf_7M is null then 6 else
	case when OutrosFinanciamentos_pf_7M <= OutrosFinanciamentos_pf_8M or OutrosFinanciamentos_pf_8M is null then 7 else
	case when OutrosFinanciamentos_pf_8M <= OutrosFinanciamentos_pf_9M or OutrosFinanciamentos_pf_9M is null then 8 else
	case when OutrosFinanciamentos_pf_9M <= OutrosFinanciamentos_pf_10M or OutrosFinanciamentos_pf_10M is null then 9 else
	case when OutrosFinanciamentos_pf_10M <= OutrosFinanciamentos_pf_11M or OutrosFinanciamentos_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_OutrosFinanciamentos_pf,
	(case when OutrosFinanciamentos_PF_10M < OutrosFinanciamentos_PF_11M then OutrosFinanciamentos_PF_10M - OutrosFinanciamentos_PF_11M else 0 end +
	case when OutrosFinanciamentos_PF_9M < OutrosFinanciamentos_PF_10M then OutrosFinanciamentos_PF_9M - OutrosFinanciamentos_PF_10M else 0 end +
	case when OutrosFinanciamentos_PF_8M < OutrosFinanciamentos_PF_9M then OutrosFinanciamentos_PF_8M - OutrosFinanciamentos_PF_9M else 0 end +
	case when OutrosFinanciamentos_PF_7M < OutrosFinanciamentos_PF_8M then OutrosFinanciamentos_PF_7M - OutrosFinanciamentos_PF_8M else 0 end +
	case when OutrosFinanciamentos_PF_6M < OutrosFinanciamentos_PF_7M then OutrosFinanciamentos_PF_6M - OutrosFinanciamentos_PF_7M else 0 end +
	case when OutrosFinanciamentos_PF_5M < OutrosFinanciamentos_PF_6M then OutrosFinanciamentos_PF_5M - OutrosFinanciamentos_PF_6M else 0 end +
	case when OutrosFinanciamentos_PF_4M < OutrosFinanciamentos_PF_5M then OutrosFinanciamentos_PF_4M - OutrosFinanciamentos_PF_5M else 0 end +
	case when OutrosFinanciamentos_PF_3M < OutrosFinanciamentos_PF_4M then OutrosFinanciamentos_PF_3M - OutrosFinanciamentos_PF_4M else 0 end +
	case when OutrosFinanciamentos_PF_2M < OutrosFinanciamentos_PF_3M then OutrosFinanciamentos_PF_2M - OutrosFinanciamentos_PF_3M else 0 end +
	case when OutrosFinanciamentos_PF_1M < OutrosFinanciamentos_PF_2M then OutrosFinanciamentos_PF_1M - OutrosFinanciamentos_PF_2M else 0 end +
	case when OutrosFinanciamentos_PF_Curr < OutrosFinanciamentos_PF_1M then OutrosFinanciamentos_PF_Curr - OutrosFinanciamentos_PF_1M else 0 end)*(-1)::float as Saldo_Amort_OutrosFinanciamentos_PF,
	HomeEquity_pf_Curr::float,
	HomeEquity_pf_1M::float,
	HomeEquity_pf_2M::float,
	HomeEquity_pf_3M::float,
	HomeEquity_pf_4M::float,
	HomeEquity_pf_5M::float,
	HomeEquity_pf_6M::float,
	HomeEquity_pf_7M::float,
	HomeEquity_pf_8M::float,
	HomeEquity_pf_9M::float,
	HomeEquity_pf_10M::float,
	HomeEquity_pf_11M::float,
	greatest(HomeEquity_pf_Curr,HomeEquity_pf_1M,HomeEquity_pf_2M,HomeEquity_pf_3M,HomeEquity_pf_4M,HomeEquity_pf_5M,HomeEquity_pf_6M,HomeEquity_pf_7M,HomeEquity_pf_8M,HomeEquity_pf_9M,HomeEquity_pf_10M,HomeEquity_pf_11M)::float as max_HomeEquity_PF,
	Ever_HomeEquity_pf::float,
	case when HomeEquity_pf_10M < HomeEquity_pf_11M then 1 else 0 end +
	case when HomeEquity_pf_9M < HomeEquity_pf_10M then 1 else 0 end +
	case when HomeEquity_pf_8M < HomeEquity_pf_9M then 1 else 0 end +
	case when HomeEquity_pf_7M < HomeEquity_pf_8M then 1 else 0 end +
	case when HomeEquity_pf_6M < HomeEquity_pf_7M then 1 else 0 end +
	case when HomeEquity_pf_5M < HomeEquity_pf_6M then 1 else 0 end +
	case when HomeEquity_pf_4M < HomeEquity_pf_5M then 1 else 0 end +
	case when HomeEquity_pf_3M < HomeEquity_pf_4M then 1 else 0 end +
	case when HomeEquity_pf_2M < HomeEquity_pf_3M then 1 else 0 end +
	case when HomeEquity_pf_1M < HomeEquity_pf_2M then 1 else 0 end +
	case when HomeEquity_pf_Curr < HomeEquity_pf_1M then 1 else 0 end::float as Meses_Reducao_HomeEquity_pf,
	case when HomeEquity_pf_10M > HomeEquity_pf_11M then 1 else 0 end +
	case when HomeEquity_pf_9M > HomeEquity_pf_10M then 1 else 0 end +
	case when HomeEquity_pf_8M > HomeEquity_pf_9M then 1 else 0 end +
	case when HomeEquity_pf_7M > HomeEquity_pf_8M then 1 else 0 end +
	case when HomeEquity_pf_6M > HomeEquity_pf_7M then 1 else 0 end +
	case when HomeEquity_pf_5M > HomeEquity_pf_6M then 1 else 0 end +
	case when HomeEquity_pf_4M > HomeEquity_pf_5M then 1 else 0 end +
	case when HomeEquity_pf_3M > HomeEquity_pf_4M then 1 else 0 end +
	case when HomeEquity_pf_2M > HomeEquity_pf_3M then 1 else 0 end +
	case when HomeEquity_pf_1M > HomeEquity_pf_2M then 1 else 0 end +
	case when HomeEquity_pf_Curr > HomeEquity_pf_1M then 1 else 0 end::float as Meses_Aumento_HomeEquity_pf,
	case when HomeEquity_pf_1M is null then null else
	case when HomeEquity_pf_Curr >= HomeEquity_pf_1M or HomeEquity_pf_1M is null then 0 else
	case when HomeEquity_pf_1M >= HomeEquity_pf_2M or HomeEquity_pf_2M is null then 1 else
	case when HomeEquity_pf_2M >= HomeEquity_pf_3M or HomeEquity_pf_3M is null then 2 else
	case when HomeEquity_pf_3M >= HomeEquity_pf_4M or HomeEquity_pf_4M is null then 3 else
	case when HomeEquity_pf_4M >= HomeEquity_pf_5M or HomeEquity_pf_5M is null then 4 else
	case when HomeEquity_pf_5M >= HomeEquity_pf_6M or HomeEquity_pf_6M is null then 5 else
	case when HomeEquity_pf_6M >= HomeEquity_pf_7M or HomeEquity_pf_7M is null then 6 else
	case when HomeEquity_pf_7M >= HomeEquity_pf_8M or HomeEquity_pf_8M is null then 7 else
	case when HomeEquity_pf_8M >= HomeEquity_pf_9M or HomeEquity_pf_9M is null then 8 else
	case when HomeEquity_pf_9M >= HomeEquity_pf_10M or HomeEquity_pf_10M is null then 9 else
	case when HomeEquity_pf_10M >= HomeEquity_pf_11M or HomeEquity_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_HomeEquity_pf,
	case when HomeEquity_pf_1M is null then null else
	case when HomeEquity_pf_Curr <= HomeEquity_pf_1M or HomeEquity_pf_1M is null then 0 else
	case when HomeEquity_pf_1M <= HomeEquity_pf_2M or HomeEquity_pf_2M is null then 1 else
	case when HomeEquity_pf_2M <= HomeEquity_pf_3M or HomeEquity_pf_3M is null then 2 else
	case when HomeEquity_pf_3M <= HomeEquity_pf_4M or HomeEquity_pf_4M is null then 3 else
	case when HomeEquity_pf_4M <= HomeEquity_pf_5M or HomeEquity_pf_5M is null then 4 else
	case when HomeEquity_pf_5M <= HomeEquity_pf_6M or HomeEquity_pf_6M is null then 5 else
	case when HomeEquity_pf_6M <= HomeEquity_pf_7M or HomeEquity_pf_7M is null then 6 else
	case when HomeEquity_pf_7M <= HomeEquity_pf_8M or HomeEquity_pf_8M is null then 7 else
	case when HomeEquity_pf_8M <= HomeEquity_pf_9M or HomeEquity_pf_9M is null then 8 else
	case when HomeEquity_pf_9M <= HomeEquity_pf_10M or HomeEquity_pf_10M is null then 9 else
	case when HomeEquity_pf_10M <= HomeEquity_pf_11M or HomeEquity_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_HomeEquity_pf,
	(case when HomeEquity_PF_10M < HomeEquity_PF_11M then HomeEquity_PF_10M - HomeEquity_PF_11M else 0 end +
	case when HomeEquity_PF_9M < HomeEquity_PF_10M then HomeEquity_PF_9M - HomeEquity_PF_10M else 0 end +
	case when HomeEquity_PF_8M < HomeEquity_PF_9M then HomeEquity_PF_8M - HomeEquity_PF_9M else 0 end +
	case when HomeEquity_PF_7M < HomeEquity_PF_8M then HomeEquity_PF_7M - HomeEquity_PF_8M else 0 end +
	case when HomeEquity_PF_6M < HomeEquity_PF_7M then HomeEquity_PF_6M - HomeEquity_PF_7M else 0 end +
	case when HomeEquity_PF_5M < HomeEquity_PF_6M then HomeEquity_PF_5M - HomeEquity_PF_6M else 0 end +
	case when HomeEquity_PF_4M < HomeEquity_PF_5M then HomeEquity_PF_4M - HomeEquity_PF_5M else 0 end +
	case when HomeEquity_PF_3M < HomeEquity_PF_4M then HomeEquity_PF_3M - HomeEquity_PF_4M else 0 end +
	case when HomeEquity_PF_2M < HomeEquity_PF_3M then HomeEquity_PF_2M - HomeEquity_PF_3M else 0 end +
	case when HomeEquity_PF_1M < HomeEquity_PF_2M then HomeEquity_PF_1M - HomeEquity_PF_2M else 0 end +
	case when HomeEquity_PF_Curr < HomeEquity_PF_1M then HomeEquity_PF_Curr - HomeEquity_PF_1M else 0 end)*(-1)::float as Saldo_Amort_HomeEquity_PF,
	LimiteCredito_PF_Curr::float,
	LimiteCredito_PF_1M::float,
	LimiteCredito_PF_2M::float,
	LimiteCredito_PF_3M::float,
	LimiteCredito_PF_4M::float,
	LimiteCredito_PF_5M::float,
	LimiteCredito_PF_6M::float,
	LimiteCredito_PF_7M::float,
	LimiteCredito_PF_8M::float,
	LimiteCredito_PF_9M::float,
	LimiteCredito_PF_10M::float,
	LimiteCredito_PF_11M::float,
	greatest(LimiteCredito_PF_Curr,LimiteCredito_PF_1M,LimiteCredito_PF_2M,LimiteCredito_PF_3M,LimiteCredito_PF_4M,LimiteCredito_PF_5M,LimiteCredito_PF_6M,LimiteCredito_PF_7M,LimiteCredito_PF_8M,LimiteCredito_PF_9M,LimiteCredito_PF_10M,LimiteCredito_PF_11M)::float as max_LimiteCredito_PF,
	Ever_LimiteCredito_PF::float,
	case when LimiteCredito_PF_10M < LimiteCredito_PF_11M then 1 else 0 end +
	case when LimiteCredito_PF_9M < LimiteCredito_PF_10M then 1 else 0 end +
	case when LimiteCredito_PF_8M < LimiteCredito_PF_9M then 1 else 0 end +
	case when LimiteCredito_PF_7M < LimiteCredito_PF_8M then 1 else 0 end +
	case when LimiteCredito_PF_6M < LimiteCredito_PF_7M then 1 else 0 end +
	case when LimiteCredito_PF_5M < LimiteCredito_PF_6M then 1 else 0 end +
	case when LimiteCredito_PF_4M < LimiteCredito_PF_5M then 1 else 0 end +
	case when LimiteCredito_PF_3M < LimiteCredito_PF_4M then 1 else 0 end +
	case when LimiteCredito_PF_2M < LimiteCredito_PF_3M then 1 else 0 end +
	case when LimiteCredito_PF_1M < LimiteCredito_PF_2M then 1 else 0 end +
	case when LimiteCredito_PF_Curr < LimiteCredito_PF_1M then 1 else 0 end::float as Meses_Reducao_LimiteCredito_PF,
	case when LimiteCredito_PF_10M > LimiteCredito_PF_11M then 1 else 0 end +
	case when LimiteCredito_PF_9M > LimiteCredito_PF_10M then 1 else 0 end +
	case when LimiteCredito_PF_8M > LimiteCredito_PF_9M then 1 else 0 end +
	case when LimiteCredito_PF_7M > LimiteCredito_PF_8M then 1 else 0 end +
	case when LimiteCredito_PF_6M > LimiteCredito_PF_7M then 1 else 0 end +
	case when LimiteCredito_PF_5M > LimiteCredito_PF_6M then 1 else 0 end +
	case when LimiteCredito_PF_4M > LimiteCredito_PF_5M then 1 else 0 end +
	case when LimiteCredito_PF_3M > LimiteCredito_PF_4M then 1 else 0 end +
	case when LimiteCredito_PF_2M > LimiteCredito_PF_3M then 1 else 0 end +
	case when LimiteCredito_PF_1M > LimiteCredito_PF_2M then 1 else 0 end +
	case when LimiteCredito_PF_Curr > LimiteCredito_PF_1M then 1 else 0 end::float as Meses_Aumento_LimiteCredito_PF,
	case when LimiteCredito_PF_1M is null then null else
	case when LimiteCredito_PF_Curr >= LimiteCredito_PF_1M or LimiteCredito_PF_1M is null then 0 else
	case when LimiteCredito_PF_1M >= LimiteCredito_PF_2M or LimiteCredito_PF_2M is null then 1 else
	case when LimiteCredito_PF_2M >= LimiteCredito_PF_3M or LimiteCredito_PF_3M is null then 2 else
	case when LimiteCredito_PF_3M >= LimiteCredito_PF_4M or LimiteCredito_PF_4M is null then 3 else
	case when LimiteCredito_PF_4M >= LimiteCredito_PF_5M or LimiteCredito_PF_5M is null then 4 else
	case when LimiteCredito_PF_5M >= LimiteCredito_PF_6M or LimiteCredito_PF_6M is null then 5 else
	case when LimiteCredito_PF_6M >= LimiteCredito_PF_7M or LimiteCredito_PF_7M is null then 6 else
	case when LimiteCredito_PF_7M >= LimiteCredito_PF_8M or LimiteCredito_PF_8M is null then 7 else
	case when LimiteCredito_PF_8M >= LimiteCredito_PF_9M or LimiteCredito_PF_9M is null then 8 else
	case when LimiteCredito_PF_9M >= LimiteCredito_PF_10M or LimiteCredito_PF_10M is null then 9 else
	case when LimiteCredito_PF_10M >= LimiteCredito_PF_11M or LimiteCredito_PF_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_LimiteCredito_PF,
	case when LimiteCredito_PF_1M is null then null else
	case when LimiteCredito_PF_Curr <= LimiteCredito_PF_1M or LimiteCredito_PF_1M is null then 0 else
	case when LimiteCredito_PF_1M <= LimiteCredito_PF_2M or LimiteCredito_PF_2M is null then 1 else
	case when LimiteCredito_PF_2M <= LimiteCredito_PF_3M or LimiteCredito_PF_3M is null then 2 else
	case when LimiteCredito_PF_3M <= LimiteCredito_PF_4M or LimiteCredito_PF_4M is null then 3 else
	case when LimiteCredito_PF_4M <= LimiteCredito_PF_5M or LimiteCredito_PF_5M is null then 4 else
	case when LimiteCredito_PF_5M <= LimiteCredito_PF_6M or LimiteCredito_PF_6M is null then 5 else
	case when LimiteCredito_PF_6M <= LimiteCredito_PF_7M or LimiteCredito_PF_7M is null then 6 else
	case when LimiteCredito_PF_7M <= LimiteCredito_PF_8M or LimiteCredito_PF_8M is null then 7 else
	case when LimiteCredito_PF_8M <= LimiteCredito_PF_9M or LimiteCredito_PF_9M is null then 8 else
	case when LimiteCredito_PF_9M <= LimiteCredito_PF_10M or LimiteCredito_PF_10M is null then 9 else
	case when LimiteCredito_PF_10M <= LimiteCredito_PF_11M or LimiteCredito_PF_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_LimiteCredito_PF,
	Prejuizo_PF_Curr::float,
	Prejuizo_PF_1M::float,
	Prejuizo_PF_2M::float,
	Prejuizo_PF_3M::float,
	Prejuizo_PF_4M::float,
	Prejuizo_PF_5M::float,
	Prejuizo_PF_6M::float,
	Prejuizo_PF_7M::float,
	Prejuizo_PF_8M::float,
	Prejuizo_PF_9M::float,
	Prejuizo_PF_10M::float,
	Prejuizo_PF_11M::float,
	greatest(Prejuizo_PF_Curr,Prejuizo_PF_1M,Prejuizo_PF_2M,Prejuizo_PF_3M,Prejuizo_PF_4M,Prejuizo_PF_5M,Prejuizo_PF_6M,Prejuizo_PF_7M,Prejuizo_PF_8M,Prejuizo_PF_9M,Prejuizo_PF_10M,Prejuizo_PF_11M)::float as max_Prejuizo_PF,
	case when Prejuizo_PF_Curr > 0 then 1 else 0 end + case when Prejuizo_PF_1M > 0 then 1 else 0 end + case when Prejuizo_PF_2M > 0 then 1 else 0 end + case when Prejuizo_PF_3M > 0 then 1 else 0 end + case when Prejuizo_PF_4M > 0 then 1 else 0 end + case when Prejuizo_PF_5M > 0 then 1 else 0 end + case when Prejuizo_PF_6M > 0 then 1 else 0 end + case when Prejuizo_PF_7M > 0 then 1 else 0 end + case when Prejuizo_PF_8M > 0 then 1 else 0 end + case when Prejuizo_PF_9M > 0 then 1 else 0 end + case when Prejuizo_PF_10M > 0 then 1 else 0 end + case when Prejuizo_PF_11M > 0 then 1 else 0 end as Meses_Prejuizo_PF,
	case when Prejuizo_PF_Curr > 0 then 0 when Prejuizo_PF_1M > 0 then 1 when Prejuizo_PF_2M > 0 then 2 when Prejuizo_PF_3M > 0 then 3 when Prejuizo_PF_4M > 0 then 4 when Prejuizo_PF_5M > 0 then 5 when Prejuizo_PF_6M > 0 then 6 when Prejuizo_PF_7M > 0 then 7 when Prejuizo_PF_8M > 0 then 8 when Prejuizo_PF_9M > 0 then 9 when Prejuizo_PF_10M > 0 then 10 when Prejuizo_PF_11M > 0 then 11 else -1 end as Meses_Desde_Ultimo_Prejuizo_PF,	
	Ever_Prejuizo_PF::float,
	CarteiraCredito_PF_Curr::float,
	CarteiraCredito_PF_1M::float,
	CarteiraCredito_PF_2M::float,
	CarteiraCredito_PF_3M::float,
	CarteiraCredito_PF_4M::float,
	CarteiraCredito_PF_5M::float,
	CarteiraCredito_PF_6M::float,
	CarteiraCredito_PF_7M::float,
	CarteiraCredito_PF_8M::float,
	CarteiraCredito_PF_9M::float,
	CarteiraCredito_PF_10M::float,
	CarteiraCredito_PF_11M::float,
	greatest(CarteiraCredito_PF_Curr,CarteiraCredito_PF_1M,CarteiraCredito_PF_2M,CarteiraCredito_PF_3M,CarteiraCredito_PF_4M,CarteiraCredito_PF_5M,CarteiraCredito_PF_6M,CarteiraCredito_PF_7M,CarteiraCredito_PF_8M,CarteiraCredito_PF_9M,CarteiraCredito_PF_10M,CarteiraCredito_PF_11M)::float as max_CarteiraCredito_PF,
	Ever_CarteiraCredito_PF::float,
	case when CarteiraCredito_PF_10M < CarteiraCredito_PF_11M then 1 else 0 end +
	case when CarteiraCredito_PF_9M < CarteiraCredito_PF_10M then 1 else 0 end +
	case when CarteiraCredito_PF_8M < CarteiraCredito_PF_9M then 1 else 0 end +
	case when CarteiraCredito_PF_7M < CarteiraCredito_PF_8M then 1 else 0 end +
	case when CarteiraCredito_PF_6M < CarteiraCredito_PF_7M then 1 else 0 end +
	case when CarteiraCredito_PF_5M < CarteiraCredito_PF_6M then 1 else 0 end +
	case when CarteiraCredito_PF_4M < CarteiraCredito_PF_5M then 1 else 0 end +
	case when CarteiraCredito_PF_3M < CarteiraCredito_PF_4M then 1 else 0 end +
	case when CarteiraCredito_PF_2M < CarteiraCredito_PF_3M then 1 else 0 end +
	case when CarteiraCredito_PF_1M < CarteiraCredito_PF_2M then 1 else 0 end +
	case when CarteiraCredito_PF_Curr < CarteiraCredito_PF_1M then 1 else 0 end::float as Meses_Reducao_CarteiraCredito_PF,
	case when CarteiraCredito_PF_10M > CarteiraCredito_PF_11M then 1 else 0 end +
	case when CarteiraCredito_PF_9M > CarteiraCredito_PF_10M then 1 else 0 end +
	case when CarteiraCredito_PF_8M > CarteiraCredito_PF_9M then 1 else 0 end +
	case when CarteiraCredito_PF_7M > CarteiraCredito_PF_8M then 1 else 0 end +
	case when CarteiraCredito_PF_6M > CarteiraCredito_PF_7M then 1 else 0 end +
	case when CarteiraCredito_PF_5M > CarteiraCredito_PF_6M then 1 else 0 end +
	case when CarteiraCredito_PF_4M > CarteiraCredito_PF_5M then 1 else 0 end +
	case when CarteiraCredito_PF_3M > CarteiraCredito_PF_4M then 1 else 0 end +
	case when CarteiraCredito_PF_2M > CarteiraCredito_PF_3M then 1 else 0 end +
	case when CarteiraCredito_PF_1M > CarteiraCredito_PF_2M then 1 else 0 end +
	case when CarteiraCredito_PF_Curr > CarteiraCredito_PF_1M then 1 else 0 end::float as Meses_Aumento_CarteiraCredito_PF,
	case when CarteiraCredito_PF_1M is null then null else
	case when CarteiraCredito_PF_Curr >= CarteiraCredito_PF_1M or CarteiraCredito_PF_1M is null then 0 else
	case when CarteiraCredito_PF_1M >= CarteiraCredito_PF_2M or CarteiraCredito_PF_2M is null then 1 else
	case when CarteiraCredito_PF_2M >= CarteiraCredito_PF_3M or CarteiraCredito_PF_3M is null then 2 else
	case when CarteiraCredito_PF_3M >= CarteiraCredito_PF_4M or CarteiraCredito_PF_4M is null then 3 else
	case when CarteiraCredito_PF_4M >= CarteiraCredito_PF_5M or CarteiraCredito_PF_5M is null then 4 else
	case when CarteiraCredito_PF_5M >= CarteiraCredito_PF_6M or CarteiraCredito_PF_6M is null then 5 else
	case when CarteiraCredito_PF_6M >= CarteiraCredito_PF_7M or CarteiraCredito_PF_7M is null then 6 else
	case when CarteiraCredito_PF_7M >= CarteiraCredito_PF_8M or CarteiraCredito_PF_8M is null then 7 else
	case when CarteiraCredito_PF_8M >= CarteiraCredito_PF_9M or CarteiraCredito_PF_9M is null then 8 else
	case when CarteiraCredito_PF_9M >= CarteiraCredito_PF_10M or CarteiraCredito_PF_10M is null then 9 else
	case when CarteiraCredito_PF_10M >= CarteiraCredito_PF_11M or CarteiraCredito_PF_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_CarteiraCredito_PF,
	case when CarteiraCredito_PF_1M is null then null else
	case when CarteiraCredito_PF_Curr <= CarteiraCredito_PF_1M or CarteiraCredito_PF_1M is null then 0 else
	case when CarteiraCredito_PF_1M <= CarteiraCredito_PF_2M or CarteiraCredito_PF_2M is null then 1 else
	case when CarteiraCredito_PF_2M <= CarteiraCredito_PF_3M or CarteiraCredito_PF_3M is null then 2 else
	case when CarteiraCredito_PF_3M <= CarteiraCredito_PF_4M or CarteiraCredito_PF_4M is null then 3 else
	case when CarteiraCredito_PF_4M <= CarteiraCredito_PF_5M or CarteiraCredito_PF_5M is null then 4 else
	case when CarteiraCredito_PF_5M <= CarteiraCredito_PF_6M or CarteiraCredito_PF_6M is null then 5 else
	case when CarteiraCredito_PF_6M <= CarteiraCredito_PF_7M or CarteiraCredito_PF_7M is null then 6 else
	case when CarteiraCredito_PF_7M <= CarteiraCredito_PF_8M or CarteiraCredito_PF_8M is null then 7 else
	case when CarteiraCredito_PF_8M <= CarteiraCredito_PF_9M or CarteiraCredito_PF_9M is null then 8 else
	case when CarteiraCredito_PF_9M <= CarteiraCredito_PF_10M or CarteiraCredito_PF_10M is null then 9 else
	case when CarteiraCredito_PF_10M <= CarteiraCredito_PF_11M or CarteiraCredito_PF_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_CarteiraCredito_PF,
	(case when CarteiraCredito_PF_10M < CarteiraCredito_PF_11M then CarteiraCredito_PF_10M - CarteiraCredito_PF_11M else 0 end +
	case when CarteiraCredito_PF_9M < CarteiraCredito_PF_10M then CarteiraCredito_PF_9M - CarteiraCredito_PF_10M else 0 end +
	case when CarteiraCredito_PF_8M < CarteiraCredito_PF_9M then CarteiraCredito_PF_8M - CarteiraCredito_PF_9M else 0 end +
	case when CarteiraCredito_PF_7M < CarteiraCredito_PF_8M then CarteiraCredito_PF_7M - CarteiraCredito_PF_8M else 0 end +
	case when CarteiraCredito_PF_6M < CarteiraCredito_PF_7M then CarteiraCredito_PF_6M - CarteiraCredito_PF_7M else 0 end +
	case when CarteiraCredito_PF_5M < CarteiraCredito_PF_6M then CarteiraCredito_PF_5M - CarteiraCredito_PF_6M else 0 end +
	case when CarteiraCredito_PF_4M < CarteiraCredito_PF_5M then CarteiraCredito_PF_4M - CarteiraCredito_PF_5M else 0 end +
	case when CarteiraCredito_PF_3M < CarteiraCredito_PF_4M then CarteiraCredito_PF_3M - CarteiraCredito_PF_4M else 0 end +
	case when CarteiraCredito_PF_2M < CarteiraCredito_PF_3M then CarteiraCredito_PF_2M - CarteiraCredito_PF_3M else 0 end +
	case when CarteiraCredito_PF_1M < CarteiraCredito_PF_2M then CarteiraCredito_PF_1M - CarteiraCredito_PF_2M else 0 end +
	case when CarteiraCredito_PF_Curr < CarteiraCredito_PF_1M then CarteiraCredito_PF_Curr - CarteiraCredito_PF_1M else 0 end)*(-1)::float as Saldo_Amort_CarteiraCredito_PF
from(select direct_prospect_id, divida_atual_pf,
	count(distinct to_date) filter (where to_date <= data_referencia) as qtd_meses_modalidade_PF,
	--------------------------------EMPRESTIMOS
	sum(valor) filter (where to_date = data_referencia and lv1 = 'EmprÃ©stimos') as emprestimos_PF_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PF_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PF_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PF_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PF_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PF_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PF_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PF_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PF_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PF_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PF_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'EmprÃ©stimos') as emprestimos_PF_11M,
	max(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos') as max_emprestimos_PF,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos') as Ever_emprestimos_PF,
	--------------------------------EMPRESTIMOS -> CHEQUE ESPECIAL
	sum(valor) filter (where to_date = data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as emprestimos_cheque_especial_PF_11M,
	max(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as max_emprestimos_cheque_especial_PF,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Cheque Especial') as Ever_emprestimos_cheque_especial_PF,
	--------------------------------EMPRESTIMOS -> CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito ou CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o ou CartÃ£o de crÃ©dito - nÃ£o migrado
	sum(valor) filter (where to_date = data_referencia and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PF_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PF_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PF_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PF_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PF_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PF_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PF_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PF_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PF_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PF_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PF_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as emprestimos_cartao_credito_PF_11M,
	max(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as max_emprestimos_cartao_credito_PF,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and (lv2 = 'CrÃ©dito Rotativo Vinculado a CartÃ£o de CrÃ©dito' or lv2 = 'CartÃ£o de CrÃ©dito Â– Compra ou Fatura Parcelada Pela InstituiÃ§Ã£o Financeira Emitente do CartÃ£o' or lv2 = 'CartÃ£o de crÃ©dito - nÃ£o migrado')) as Ever_emprestimos_cartao_credito_PF,
	--------------------------------EMPRESTIMOS -> CRÃ‰DITO QUE A PF CARREGA PARA A PJ
	sum(valor) filter (where to_date = data_referencia and lv1 = 'EmprÃ©stimos' and (lv2 = 'MicrocrÃ©dito' or lv2 = 'CrÃ©dito Pessoal - sem ConsignaÃ§Ã£o em Folha de Pagamento')) as emprestimos_credito_pPJ_PF_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'MicrocrÃ©dito' or lv2 = 'CrÃ©dito Pessoal - sem ConsignaÃ§Ã£o em Folha de Pagamento')) as emprestimos_credito_pPJ_PF_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'MicrocrÃ©dito' or lv2 = 'CrÃ©dito Pessoal - sem ConsignaÃ§Ã£o em Folha de Pagamento')) as emprestimos_credito_pPJ_PF_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'MicrocrÃ©dito' or lv2 = 'CrÃ©dito Pessoal - sem ConsignaÃ§Ã£o em Folha de Pagamento')) as emprestimos_credito_pPJ_PF_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'MicrocrÃ©dito' or lv2 = 'CrÃ©dito Pessoal - sem ConsignaÃ§Ã£o em Folha de Pagamento')) as emprestimos_credito_pPJ_PF_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'MicrocrÃ©dito' or lv2 = 'CrÃ©dito Pessoal - sem ConsignaÃ§Ã£o em Folha de Pagamento')) as emprestimos_credito_pPJ_PF_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'MicrocrÃ©dito' or lv2 = 'CrÃ©dito Pessoal - sem ConsignaÃ§Ã£o em Folha de Pagamento')) as emprestimos_credito_pPJ_PF_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'MicrocrÃ©dito' or lv2 = 'CrÃ©dito Pessoal - sem ConsignaÃ§Ã£o em Folha de Pagamento')) as emprestimos_credito_pPJ_PF_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'MicrocrÃ©dito' or lv2 = 'CrÃ©dito Pessoal - sem ConsignaÃ§Ã£o em Folha de Pagamento')) as emprestimos_credito_pPJ_PF_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'MicrocrÃ©dito' or lv2 = 'CrÃ©dito Pessoal - sem ConsignaÃ§Ã£o em Folha de Pagamento')) as emprestimos_credito_pPJ_PF_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'MicrocrÃ©dito' or lv2 = 'CrÃ©dito Pessoal - sem ConsignaÃ§Ã£o em Folha de Pagamento')) as emprestimos_credito_pPJ_PF_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'EmprÃ©stimos' and (lv2 = 'MicrocrÃ©dito' or lv2 = 'CrÃ©dito Pessoal - sem ConsignaÃ§Ã£o em Folha de Pagamento')) as emprestimos_credito_pPJ_PF_11M,
	max(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and (lv2 = 'MicrocrÃ©dito' or lv2 = 'CrÃ©dito Pessoal - sem ConsignaÃ§Ã£o em Folha de Pagamento')) as max_emprestimos_credito_pPJ_PF,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and (lv2 = 'MicrocrÃ©dito' or lv2 = 'CrÃ©dito Pessoal - sem ConsignaÃ§Ã£o em Folha de Pagamento')) as Ever_emprestimos_credito_pPJ_PF,
	--------------------------------EMPRESTIMOS -> CrÃ©dito Pessoal - com ConsignaÃ§Ã£o em Folha de Pagamento
	sum(valor) filter (where to_date = data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'CrÃ©dito Pessoal - com ConsignaÃ§Ã£o em Folha de Pagamento') as emprestimos_credito_consignado_PF_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'CrÃ©dito Pessoal - com ConsignaÃ§Ã£o em Folha de Pagamento') as emprestimos_credito_consignado_PF_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'CrÃ©dito Pessoal - com ConsignaÃ§Ã£o em Folha de Pagamento') as emprestimos_credito_consignado_PF_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'CrÃ©dito Pessoal - com ConsignaÃ§Ã£o em Folha de Pagamento') as emprestimos_credito_consignado_PF_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'CrÃ©dito Pessoal - com ConsignaÃ§Ã£o em Folha de Pagamento') as emprestimos_credito_consignado_PF_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'CrÃ©dito Pessoal - com ConsignaÃ§Ã£o em Folha de Pagamento') as emprestimos_credito_consignado_PF_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'CrÃ©dito Pessoal - com ConsignaÃ§Ã£o em Folha de Pagamento') as emprestimos_credito_consignado_PF_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'CrÃ©dito Pessoal - com ConsignaÃ§Ã£o em Folha de Pagamento') as emprestimos_credito_consignado_PF_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'CrÃ©dito Pessoal - com ConsignaÃ§Ã£o em Folha de Pagamento') as emprestimos_credito_consignado_PF_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'CrÃ©dito Pessoal - com ConsignaÃ§Ã£o em Folha de Pagamento') as emprestimos_credito_consignado_PF_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'CrÃ©dito Pessoal - com ConsignaÃ§Ã£o em Folha de Pagamento') as emprestimos_credito_consignado_PF_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'CrÃ©dito Pessoal - com ConsignaÃ§Ã£o em Folha de Pagamento') as emprestimos_credito_consignado_PF_11M,
	max(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'CrÃ©dito Pessoal - com ConsignaÃ§Ã£o em Folha de Pagamento') as max_emprestimos_credito_consignado_PF,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'CrÃ©dito Pessoal - com ConsignaÃ§Ã£o em Folha de Pagamento') as Ever_emprestimos_credito_consignado_PF,
	--------------------------------EMPRESTIMOS -> OUTROS EMPRESTIMOS
	sum(valor) filter (where to_date = data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PF_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PF_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PF_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PF_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PF_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PF_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PF_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PF_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PF_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PF_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PF_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as emprestimos_outros_emprestimos_PF_11M,
	max(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as max_emprestimos_outros_emprestimos_PF,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'EmprÃ©stimos' and lv2 = 'Outros EmprÃ©stimos') as Ever_emprestimos_outros_emprestimos_PF,
	--------------------------------OUTROS CREDITOS
	sum(valor) filter (where to_date = data_referencia and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PF_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PF_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PF_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PF_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PF_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PF_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PF_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PF_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PF_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PF_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PF_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'Outros CrÃ©ditos') as OutrosCreditos_PF_11M,
	max(valor) filter (where to_date <= data_referencia and lv1 = 'Outros CrÃ©ditos') as max_OutrosCreditos_PF,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'Outros CrÃ©ditos') as Ever_OutrosCreditos_PF,
	--------------------------------FINANCIAMENTOS DE AUTOMÃ“VEIS
	sum(valor) filter (where to_date = data_referencia and lv1 = 'Financiamentos' and lv2 = 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as Financiamento_Veiculo_PF_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'Financiamentos' and lv2 = 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as Financiamento_Veiculo_PF_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'Financiamentos' and lv2 = 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as Financiamento_Veiculo_PF_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'Financiamentos' and lv2 = 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as Financiamento_Veiculo_PF_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'Financiamentos' and lv2 = 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as Financiamento_Veiculo_PF_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'Financiamentos' and lv2 = 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as Financiamento_Veiculo_PF_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'Financiamentos' and lv2 = 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as Financiamento_Veiculo_PF_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'Financiamentos' and lv2 = 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as Financiamento_Veiculo_PF_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'Financiamentos' and lv2 = 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as Financiamento_Veiculo_PF_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'Financiamentos' and lv2 = 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as Financiamento_Veiculo_PF_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'Financiamentos' and lv2 = 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as Financiamento_Veiculo_PF_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'Financiamentos' and lv2 = 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as Financiamento_Veiculo_PF_11M,
	max(valor) filter (where to_date <= data_referencia and lv1 = 'Financiamentos' and lv2 = 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as max_Financiamento_Veiculo_PF,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'Financiamentos' and lv2 = 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as Ever_Financiamento_Veiculo_PF,
	--------------------------------FINANCIAMENTO IMOBILIÃ�RIO
	sum(valor) filter (where to_date = data_referencia and lv1 = 'Financiamentos ImobiliÃ¡rios') as Financiamento_Imobiliario_PF_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'Financiamentos ImobiliÃ¡rios') as Financiamento_Imobiliario_PF_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'Financiamentos ImobiliÃ¡rios') as Financiamento_Imobiliario_PF_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'Financiamentos ImobiliÃ¡rios') as Financiamento_Imobiliario_PF_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'Financiamentos ImobiliÃ¡rios') as Financiamento_Imobiliario_PF_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'Financiamentos ImobiliÃ¡rios') as Financiamento_Imobiliario_PF_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'Financiamentos ImobiliÃ¡rios') as Financiamento_Imobiliario_PF_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'Financiamentos ImobiliÃ¡rios') as Financiamento_Imobiliario_PF_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'Financiamentos ImobiliÃ¡rios') as Financiamento_Imobiliario_PF_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'Financiamentos ImobiliÃ¡rios') as Financiamento_Imobiliario_PF_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'Financiamentos ImobiliÃ¡rios') as Financiamento_Imobiliario_PF_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'Financiamentos ImobiliÃ¡rios') as Financiamento_Imobiliario_PF_11M,
	max(valor) filter (where to_date <= data_referencia and lv1 = 'Financiamentos ImobiliÃ¡rios') as max_Financiamento_Imobiliario_PF,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'Financiamentos ImobiliÃ¡rios') as Ever_Financiamento_Imobiliario_PF,
	--------------------------------ADIANTAMENTOS E DESCONTOS
	sum(valor) filter (where to_date = data_referencia and (lv1 = 'Adiantamentos a Depositantes' or lv1 = 'Direitos CreditÃ³rios Descontados')) as AdiantamentosDescontos_PF_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and (lv1 = 'Adiantamentos a Depositantes' or lv1 = 'Direitos CreditÃ³rios Descontados')) as AdiantamentosDescontos_PF_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and (lv1 = 'Adiantamentos a Depositantes' or lv1 = 'Direitos CreditÃ³rios Descontados')) as AdiantamentosDescontos_PF_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and (lv1 = 'Adiantamentos a Depositantes' or lv1 = 'Direitos CreditÃ³rios Descontados')) as AdiantamentosDescontos_PF_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and (lv1 = 'Adiantamentos a Depositantes' or lv1 = 'Direitos CreditÃ³rios Descontados')) as AdiantamentosDescontos_PF_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and (lv1 = 'Adiantamentos a Depositantes' or lv1 = 'Direitos CreditÃ³rios Descontados')) as AdiantamentosDescontos_PF_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and (lv1 = 'Adiantamentos a Depositantes' or lv1 = 'Direitos CreditÃ³rios Descontados')) as AdiantamentosDescontos_PF_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and (lv1 = 'Adiantamentos a Depositantes' or lv1 = 'Direitos CreditÃ³rios Descontados')) as AdiantamentosDescontos_PF_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and (lv1 = 'Adiantamentos a Depositantes' or lv1 = 'Direitos CreditÃ³rios Descontados')) as AdiantamentosDescontos_PF_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and (lv1 = 'Adiantamentos a Depositantes' or lv1 = 'Direitos CreditÃ³rios Descontados')) as AdiantamentosDescontos_PF_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and (lv1 = 'Adiantamentos a Depositantes' or lv1 = 'Direitos CreditÃ³rios Descontados')) as AdiantamentosDescontos_PF_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and (lv1 = 'Adiantamentos a Depositantes' or lv1 = 'Direitos CreditÃ³rios Descontados')) as AdiantamentosDescontos_PF_11M,
	max(valor) filter (where to_date <= data_referencia and (lv1 = 'Adiantamentos a Depositantes' or lv1 = 'Direitos CreditÃ³rios Descontados')) as max_AdiantamentosDescontos_PF,
	sum(valor) filter (where to_date <= data_referencia and (lv1 = 'Adiantamentos a Depositantes' or lv1 = 'Direitos CreditÃ³rios Descontados')) as Ever_AdiantamentosDescontos_PF,
	--------------------------------OUTROS FINANCIAMENTOS
	sum(valor) filter (where to_date = data_referencia and lv1 like 'Financiamentos%' and lv1 <> 'Financiamentos ImobiliÃ¡rios' and lv2 <> 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as OutrosFinanciamentos_PF_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 like 'Financiamentos%' and lv1 <> 'Financiamentos ImobiliÃ¡rios' and lv2 <> 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as OutrosFinanciamentos_PF_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 like 'Financiamentos%' and lv1 <> 'Financiamentos ImobiliÃ¡rios' and lv2 <> 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as OutrosFinanciamentos_PF_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 like 'Financiamentos%' and lv1 <> 'Financiamentos ImobiliÃ¡rios' and lv2 <> 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as OutrosFinanciamentos_PF_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 like 'Financiamentos%' and lv1 <> 'Financiamentos ImobiliÃ¡rios' and lv2 <> 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as OutrosFinanciamentos_PF_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 like 'Financiamentos%' and lv1 <> 'Financiamentos ImobiliÃ¡rios' and lv2 <> 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as OutrosFinanciamentos_PF_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 like 'Financiamentos%' and lv1 <> 'Financiamentos ImobiliÃ¡rios' and lv2 <> 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as OutrosFinanciamentos_PF_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 like 'Financiamentos%' and lv1 <> 'Financiamentos ImobiliÃ¡rios' and lv2 <> 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as OutrosFinanciamentos_PF_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 like 'Financiamentos%' and lv1 <> 'Financiamentos ImobiliÃ¡rios' and lv2 <> 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as OutrosFinanciamentos_PF_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 like 'Financiamentos%' and lv1 <> 'Financiamentos ImobiliÃ¡rios' and lv2 <> 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as OutrosFinanciamentos_PF_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 like 'Financiamentos%' and lv1 <> 'Financiamentos ImobiliÃ¡rios' and lv2 <> 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as OutrosFinanciamentos_PF_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 like 'Financiamentos%' and lv1 <> 'Financiamentos ImobiliÃ¡rios' and lv2 <> 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as OutrosFinanciamentos_PF_11M,
	max(valor) filter (where to_date <= data_referencia and lv1 like 'Financiamentos%' and lv1 <> 'Financiamentos ImobiliÃ¡rios' and lv2 <> 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as max_OutrosFinanciamentos_PF,
	sum(valor) filter (where to_date <= data_referencia and lv1 like 'Financiamentos%' and lv1 <> 'Financiamentos ImobiliÃ¡rios' and lv2 <> 'AquisiÃ§Ã£o de Bens Â– VeÃ­culos Automotores') as Ever_OutrosFinanciamentos_PF,
	--------------------------------HOME EQUITY
	sum(valor) filter (where to_date = data_referencia and lv2 = 'Home Equity') as HomeEquity_PF_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv2 = 'Home Equity') as HomeEquity_PF_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv2 = 'Home Equity') as HomeEquity_PF_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv2 = 'Home Equity') as HomeEquity_PF_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv2 = 'Home Equity') as HomeEquity_PF_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv2 = 'Home Equity') as HomeEquity_PF_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv2 = 'Home Equity') as HomeEquity_PF_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv2 = 'Home Equity') as HomeEquity_PF_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv2 = 'Home Equity') as HomeEquity_PF_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv2 = 'Home Equity') as HomeEquity_PF_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv2 = 'Home Equity') as HomeEquity_PF_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv2 = 'Home Equity') as HomeEquity_PF_11M,
	max(valor) filter (where to_date <= data_referencia and lv2 = 'Home Equity') as max_HomeEquity_PF,
	sum(valor) filter (where to_date <= data_referencia and lv2 = 'Home Equity') as Ever_HomeEquity_PF,
	--------------------------------LIMITE DE CRÃ‰DITO
	sum(valor) filter (where to_date = data_referencia and lv1 = 'Limite de CrÃ©dito (H)') as LimiteCredito_PF_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'Limite de CrÃ©dito (H)') as LimiteCredito_PF_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'Limite de CrÃ©dito (H)') as LimiteCredito_PF_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'Limite de CrÃ©dito (H)') as LimiteCredito_PF_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'Limite de CrÃ©dito (H)') as LimiteCredito_PF_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'Limite de CrÃ©dito (H)') as LimiteCredito_PF_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'Limite de CrÃ©dito (H)') as LimiteCredito_PF_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'Limite de CrÃ©dito (H)') as LimiteCredito_PF_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'Limite de CrÃ©dito (H)') as LimiteCredito_PF_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'Limite de CrÃ©dito (H)') as LimiteCredito_PF_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'Limite de CrÃ©dito (H)') as LimiteCredito_PF_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'Limite de CrÃ©dito (H)') as LimiteCredito_PF_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'Limite de CrÃ©dito (H)') as Ever_LimiteCredito_PF,
	--------------------------------PREJUIZO
	sum(valor) filter (where to_date = data_referencia and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PF_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PF_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PF_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PF_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PF_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PF_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PF_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PF_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PF_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PF_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PF_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 = 'PrejuÃ­zo (B)') as Prejuizo_PF_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 = 'PrejuÃ­zo (B)') as Ever_Prejuizo_PF,
	--------------------------------CARTEIRA CREDITO
	sum(valor) filter (where to_date = data_referencia and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months' and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as CarteiraCredito_PF_11M,
	sum(valor) filter (where to_date <= data_referencia and lv1 not in ('Repasses Interfinanceiros (D)','CoobrigaÃ§Ã£o (E)','CrÃ©ditos a Liberar (G)','Limite de CrÃ©dito (H)','Risco Indireto (I)')) as Ever_CarteiraCredito_PF
	from(select direct_prospect_id,divida_atual_pf,regexp_replace(lv1, '\d{2}\s-\s', '') as lv1,replace(replace(replace(replace(regexp_replace(lv2, '\d{4}\s-\s', ''),' Â– ',' - '),' -- ',' - '),' Â–- ',' - '),'interfinanceiros','Interfinanceiros') as lv2, to_date(data,'mm/yyyy'), case when valor is null then 0 else valor end::float,
			case when max_id is null then case when extract(day from ref_date) >= 16 then to_date(to_char(ref_date - interval '1 month' ,'Mon/yy'),'Mon/yy') else to_date(to_char(ref_date - interval '2 months' ,'Mon/yy'),'Mon/yy') end else first_value(to_date(data,'mm/yyyy')) over (partition by direct_prospect_id order by to_date(data,'mm/yyyy') desc) end as data_referencia
		from(select direct_prospect_id, ref_date,max_id,divida_atual_pf,Lv1, nome as Lv2, (jsonb_populate_recordset(null::meu2, serie)).*
			from (select direct_prospect_id, ref_date,max_id,case when max_id is not null then divida_atual_pf end as divida_atual_pf,nome as Lv1, (jsonb_populate_recordset(null::meu, detalhes)).*
				from (select direct_prospect_id, (data->'indicadores'->>'dividaAtual')::float as divida_atual_pf, to_date(data->'data_base'->>'Data-Base','mm/yyyy') as data_base,ref_date,max_id,(jsonb_populate_recordset(NULL ::lucas, data #> '{por_modalidade}')).*
				from (select direct_prospect_id,data,ref_date,max_id
					from credito_coleta cc
					join(select dp.direct_prospect_id, 
							max(coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date) + interval '1 day') as ref_date,
							max(dp.cpf) as cpf,
							max(cc.id) filter (where coalesce(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '1 day') as max_id,
							min(cc.id) as min_id
						from credito_coleta cc
						join direct_prospects dp on cc.documento = dp.cpf
						join temp_ids as aux on aux.direct_prospect_id = dp.direct_prospect_id
						left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
							from direct_prospects dp
							left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
							left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
							left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
							group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
						left join offers o on o.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
						left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on coalesce(max_id ,min_id) = lr.loan_request_id) lr on lr.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
						where documento_tipo = 'CPF' and tipo = 'SCR'
						group by 1) as t1 on cc.id = coalesce(max_id,min_id)) as t2) as t3) as t4) as t5) as t6 group by 1,2) as t7) as SCRModal_PF on SCRModal_PF.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id,(data->>'score')::float as serasa_coleta,("data"->>'pd')::float as pd_serasa
	from credito_coleta cc
	join(select dp.direct_prospect_id,
			max(dp.cnpj) as cnpj,
			max(cc.id) as max_id
		from credito_coleta cc
		join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
		join temp_ids as aux on aux.direct_prospect_id = dp.direct_prospect_id
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
		left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on coalesce(max_id ,min_id) = lr.loan_request_id) lr on lr.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
		where tipo in ('Serasa','RelatoSocios','RelatoBasico') and to_date(cc.data->'datetime'->>'data','yyyymmdd') <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date) + interval '1 day'
		group by 1) as t1 on cc.id = max_id) as SerasaScore on SerasaScore.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE CREDITA
left join (select cc.origem_id,
		cc."data"->'customer'->>'email' as credita_email,
		cc."data"->'customer'->'mobile'->>'areaCode' as credita_area_code,
		cc."data"->'customer'->'mobile'->>'regionCode' as credita_region_code,
		cc."data"->'customer'->'mobile'->>'phoneNumber' as credita_phone_number,
		cc."data"->'customer'->'document'->>'documentID' as credita_cpf,
		cc."data"->'customer'->'document'->>'documentCountry' as credita_country,
		(cc."data"->'customer'->>'companyScore')::float as credita_score_mei,
		cc."data"->>'leadType' as credita_lead_type,
		cc."data"->'loanOffer'->>'creditLineName' as credita_credit_line_name
	from credito_coleta cc
	join direct_prospects dp on dp.direct_prospect_id = cc.origem_id
	join temp_ids as aux on aux.direct_prospect_id = dp.direct_prospect_id
	where cc.tipo = 'Credita') as Credita on Credita.origem_id = dp.direct_prospect_id
--------------------------TABLE SERASA SOCIOS
left join (select direct_prospect_id, case when count(cpf_socios) = 0 then null else count(*) filter (where cpf = cpf_socios) end is_shareholder,case when count(cpf_socios) = 0 then null else count(*) end count_socios
	from(select t1.direct_prospect_id, t1.cpf, data->'acionistas'->jsonb_object_keys(data->'acionistas')->>'cpf' as cpf_socios
		from credito_coleta cc 
		join(select dp.direct_prospect_id,
				max(dp.cnpj) as cnpj,
				max(dp.cpf) as cpf,
				max(cc.id) as max_id
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join temp_ids as aux on aux.direct_prospect_id = dp.direct_prospect_id
			left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
				from direct_prospects dp
				left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
				left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
				left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
				group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
			left join offers o on o.offer_id = coalesce(acc_offer_id,max_offer_id,other_offer_id)
			left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on coalesce(max_id ,min_id) = lr.loan_request_id) lr on lr.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
			where tipo in ('Serasa','RelatoSocios','RelatoBasico') and coalesce(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS'),data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '1 day'
			group by 1) as t1 on cc.id = max_id) as t2
		group by 1) as SerasaCPF on SerasaCPF.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SERASA ANOTACOES
left join (select SPC.direct_prospect_id,
		--Empresa
		sum(coalesce(divida_valor,0)) filter (where spc.nome = 'empresa') as empresa_divida_valor,
		sum(coalesce(ccf_valor,0)) filter (where spc.nome = 'empresa') as empresa_ccf_valor,
		sum(coalesce(protesto_valor,0)) filter (where spc.nome = 'empresa') as empresa_protesto_valor,
		sum(coalesce(acao_valor,0)) filter (where spc.nome = 'empresa') as empresa_acao_valor,
		sum(coalesce(pefin_valor,0)) filter (where spc.nome = 'empresa') as empresa_pefin_valor,
		sum(coalesce(refin_valor,0)) filter (where spc.nome = 'empresa') as empresa_refin_valor,
		sum(coalesce(spc_valor,0)) filter (where spc.nome = 'empresa') as empresa_spc_valor,
		sum(coalesce(divida_valor,0) + coalesce(ccf_valor,0) + coalesce(protesto_valor,0) + coalesce(acao_valor,0) + coalesce(pefin_valor,0) + coalesce(refin_valor,0) + coalesce(spc_valor,0)) filter (where spc.nome = 'empresa') empresa_total_valor,
		sum(coalesce(divida_unit,0)) filter (where spc.nome = 'empresa') as empresa_divida_unit,
		sum(coalesce(ccf_unit,0)) filter (where spc.nome = 'empresa') as empresa_ccf_unit,
		sum(coalesce(protesto_unit,0)) filter (where spc.nome = 'empresa') as empresa_protesto_unit,
		sum(coalesce(acao_unit,0)) filter (where spc.nome = 'empresa') as empresa_acao_unit,
		sum(coalesce(pefin_unit,0)) filter (where spc.nome = 'empresa') as empresa_pefin_unit,
		sum(coalesce(refin_unit,0)) filter (where spc.nome = 'empresa') as empresa_refin_unit,
		sum(coalesce(spc_unit,0)) filter (where spc.nome = 'empresa') as empresa_spc_unit,
		sum(coalesce(divida_unit,0) + coalesce(ccf_unit,0) + coalesce(protesto_unit,0) + coalesce(acao_unit,0) + coalesce(pefin_unit,0) + coalesce(refin_unit,0) + coalesce(spc_unit,0)) filter (where spc.nome = 'empresa') as empresa_total_unit,
		--Socios
		sum(coalesce(divida_valor,0)) filter (where spc.spc_share is not null) as socio_divida_valor,
		sum(coalesce(ccf_valor,0)) filter (where spc.spc_share is not null) as socio_ccf_valor,
		sum(coalesce(acao_valor,0)) filter (where spc.spc_share is not null) as socio_acao_valor,
		sum(coalesce(pefin_valor,0)) filter (where spc.spc_share is not null) as socio_pefin_valor,
		sum(coalesce(protesto_valor,0)) filter (where spc.spc_share is not null) as socio_protesto_valor,
		sum(coalesce(refin_valor,0)) filter (where spc.spc_share is not null) as socio_refin_valor,
		sum(coalesce(spc_valor,0)) filter (where spc.spc_share is not null) as socio_spc_valor,
		sum(coalesce(divida_valor,0) + coalesce(ccf_valor,0) + coalesce(protesto_valor,0) + coalesce(acao_valor,0) + coalesce(pefin_valor,0) + coalesce(refin_valor,0) + coalesce(spc_valor,0)) filter (where spc.spc_share is not null) as socios_total_valor,
		sum(coalesce(divida_unit,0)) filter (where spc.spc_share is not null) as socio_divida_unit,
		sum(coalesce(ccf_unit,0)) filter (where spc.spc_share is not null) as socio_ccf_unit,
		sum(coalesce(protesto_unit,0)) filter (where spc.spc_share is not null) as socio_protesto_unit,
		sum(coalesce(acao_unit,0)) filter (where spc.spc_share is not null) as socio_acao_unit,
		sum(coalesce(pefin_unit,0)) filter (where spc.spc_share is not null) as socio_pefin_unit,
		sum(coalesce(refin_unit,0)) filter (where spc.spc_share is not null) as socio_refin_unit,
		sum(coalesce(spc_unit,0)) filter (where spc.spc_share is not null) as socio_spc_unit,
		sum(coalesce(divida_unit,0) + coalesce(ccf_unit,0) + coalesce(protesto_unit,0) + coalesce(acao_unit,0) + coalesce(pefin_unit,0) + coalesce(refin_unit,0) + coalesce(spc_unit,0)) filter (where spc.spc_share is not null) as socios_total_unit,
		--Socios major
		sum(coalesce(divida_valor,0)) filter (where spc.spc_share >=0.2) as socio_major_divida_valor,
		sum(coalesce(ccf_valor,0)) filter (where spc.spc_share >=0.2) as socio_major_ccf_valor,
		sum(coalesce(protesto_valor,0)) filter (where spc.spc_share >=0.2) as socio_major_protesto_valor,
		sum(coalesce(acao_valor,0)) filter (where spc.spc_share >=0.2) as socio_major_acao_valor,
		sum(coalesce(pefin_valor,0)) filter (where spc.spc_share >=0.2) as socio_major_pefin_valor,
		sum(coalesce(refin_valor,0)) filter (where spc.spc_share >=0.2) as socio_major_refin_valor,
		sum(coalesce(spc_valor,0)) filter (where spc.spc_share >=0.2) as socio_major_spc_valor,
		sum(coalesce(divida_valor,0) + coalesce(ccf_valor,0) + coalesce(protesto_valor,0) + coalesce(acao_valor,0) + coalesce(pefin_valor,0) + coalesce(refin_valor,0) + coalesce(spc_valor,0)) filter (where spc.spc_share >=0.2) as socios_major_total_valor,
		sum(coalesce(divida_unit,0)) filter (where spc.spc_share >=0.2) as socio_major_divida_unit,
		sum(coalesce(ccf_unit,0)) filter (where spc.spc_share >=0.2) as socio_major_ccf_unit,
		sum(coalesce(protesto_unit,0)) filter (where spc.spc_share >=0.2) as socio_major_protesto_unit,
		sum(coalesce(acao_unit,0)) filter (where spc.spc_share >=0.2) as socio_major_acao_unit,
		sum(coalesce(pefin_unit,0)) filter (where spc.spc_share >=0.2) as socio_major_pefin_unit,
		sum(coalesce(refin_unit,0)) filter (where spc.spc_share >=0.2) as socio_major_refin_unit,
		sum(coalesce(spc_unit,0)) filter (where spc.spc_share >=0.2) as socio_major_spc_unit,
		sum(coalesce(divida_unit,0) + coalesce(ccf_unit,0) + coalesce(protesto_unit,0) + coalesce(acao_unit,0) + coalesce(pefin_unit,0) + coalesce(refin_unit,0) + coalesce(spc_unit,0)) filter (where spc.spc_share >=0.2) as socios_major_total_unit,
		--Total (empresa + socios)
		sum(coalesce(divida_valor,0) + coalesce(ccf_valor,0) + coalesce(protesto_valor,0) + coalesce(acao_valor,0) + coalesce(pefin_valor,0) + coalesce(refin_valor,0) + coalesce(spc_valor,0)) as all_valor,
		sum(coalesce(divida_unit,0) + coalesce(ccf_unit,0) + coalesce(protesto_unit,0) + coalesce(acao_unit,0) + coalesce(pefin_unit,0) + coalesce(refin_unit,0) + coalesce(spc_unit,0)) as all_unit	
	from(
		--SOCIOS E SHARE COM SPC SOCIOS
		select socios_share.direct_prospect_id,socios_share.nome,socios_share.spc_share,case when coalesce(spc_socios_valor,'0')  = '' then '0' else spc_socios_valor end::float as spc_valor,case when coalesce(spc_socios_unit,'0') = '' then '0' else spc_socios_unit end::int as spc_unit
		from (select t1.direct_prospect_id,jsonb_object_keys("data"->'acionistas') as nome,("data"->'acionistas'->jsonb_object_keys("data"->'acionistas')->>'percentual')::float/100 as spc_share
			from credito_coleta cc
			join(select dp.direct_prospect_id,
					max(dp.cnpj) as cnpj, 
					max(cc.id) as max_id
				from credito_coleta cc
				join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
				join temp_ids as aux on aux.direct_prospect_id = dp.direct_prospect_id
				left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
					from direct_prospects dp
					left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
					left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
					left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
					group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
				left join offers o on o.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
				left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on coalesce(max_id ,min_id) = lr.loan_request_id) lr on lr.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
				where tipo in ('Serasa','RelatoSocios','RelatoBasico') and coalesce(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS'),data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '1 day'
				group by 1) as t1 on cc.id = max_id) as socios_share
		left join(select t1.direct_prospect_id,data->'spc'->jsonb_object_keys(data->'spc')->>'nome' as spc_nome,data->'spc'->jsonb_object_keys(data->'spc')->>'valor' as spc_socios_valor,data->'spc'->jsonb_object_keys(data->'spc')->>'quantidade' as spc_socios_unit
			from credito_coleta cc
			join(select dp.direct_prospect_id,
					max(dp.cnpj) as cnpj, 
					max(cc.id) max_id
				from credito_coleta cc
				join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
				join temp_ids as aux on aux.direct_prospect_id = dp.direct_prospect_id
				left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
					from direct_prospects dp
					left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
					left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
					left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
					group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
				left join offers o on o.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
				left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on coalesce(max_id ,min_id) = lr.loan_request_id) lr on lr.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
				where tipo in ('Serasa','RelatoSocios','RelatoBasico') and coalesce(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS'),data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '1 day'
				group by 1) as t1 on cc.id = max_id) as spc_socios on spc_socios.direct_prospect_id = socios_share.direct_prospect_id and spc_socios.spc_nome = socios_share.nome
	union
		--SPC EMPRESA
		select direct_prospect_id, 'empresa' as nome,null as spc_share,sum(coalesce(spc_valor,0)) as spc_valor, sum(coalesce(spc_unit,0)) as spc_unit	
		from(select t1.direct_prospect_id, ("data"->'anotacao_spc'->>'total_debito')::float as spc_valor, ("data"->'anotacao_spc'->>'total_ocr')::float as spc_unit
			from credito_coleta cc
			join(select dp.direct_prospect_id,
					max(dp.cnpj) as cnpj, 
					max(cc.id) as max_id
				from credito_coleta cc
				join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
				join temp_ids as aux on aux.direct_prospect_id = dp.direct_prospect_id
				left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
					from direct_prospects dp
					left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
					left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
					left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
					group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
				left join offers o on o.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
				left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on coalesce(max_id ,min_id) = lr.loan_request_id) lr on lr.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
				where tipo in ('Serasa','RelatoSocios','RelatoBasico') and coalesce(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS'),data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '1 day'
				group by 1) as t1 on cc.id = max_id) as t2
			group by 1) as SPC
	left join(
		--SERASA EMPRESA E SOCIO
		select t1.direct_prospect_id,
			jsonb_object_keys(cc."data"->'anotacoes') as serasa_nome,
			("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'DIVIDA_VENCIDA'->>'valor')::float as divida_valor,
			("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'CHEQUE'->>'valor')::float as ccf_valor,
			("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'PROTESTO'->>'valor')::float as protesto_valor,
			("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'ACAO_JUDICIAL'->>'valor')::float as acao_valor,
			("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Pefin'->>'valor_total')::float as pefin_valor,
			("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Refin'->>'valor_total')::float as refin_valor,
			("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'DIVIDA_VENCIDA'->>'quantidade')::float as divida_unit,
			("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'CHEQUE'->>'quantidade')::float as ccf_unit,
			("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'PROTESTO'->>'quantidade')::float as protesto_unit,
			("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'ACAO_JUDICIAL'->>'quantidade')::float as acao_unit,
			("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Pefin'->>'quantidade')::float as pefin_unit,
			("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Refin'->>'quantidade')::float as refin_unit
		from credito_coleta cc
		join(select dp.direct_prospect_id,
				max(dp.cnpj) as cnpj, 
				max(cc.id) as max_id
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join temp_ids as aux on aux.direct_prospect_id = dp.direct_prospect_id
			left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
				from direct_prospects dp
				left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
				left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
				left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
				group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
			left join offers o on o.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
			left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on coalesce(max_id ,min_id) = lr.loan_request_id) lr on lr.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
			where tipo in ('Serasa','RelatoSocios','RelatoBasico') and coalesce(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS'),data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '1 day'
			group by 1) as t1 on cc.id = max_id
	union
		select
			lead_id as direct_prospect_id,
			'empresa' as serasa_nome,
			experian_debt_overdue_value as divida_valor,
			experian_bad_check_value as ccf_valor,
			experian_registry_protest_value as protesto_valor,
			experian_legal_action_value as acao_valor,
			0 as pefin_valor,
			0 as pefin_valor,
			experian_debt_overdue_unit as divida_unit,
			experian_bad_check_unit as ccf_unit,
			experian_registry_protest_unit as protesto_unit,
			experian_legal_action_unit as acao_unit,
			0 as pefin_unit,
			0 as refin_unit
		from lucas_leal.serasas_antigos_parseados) Serasa on Serasa.direct_prospect_id = SPC.direct_prospect_id and SPC.nome = Serasa.serasa_nome
	group by 1) as SerasaRestr on SerasaRestr.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SERASA PROBLEMA COLIGADAS
left join(select direct_prospect_id, coalesce(count(*) filter (where empresas_problemas = 'S'),-1) as empresas_problema
	from(select t1.direct_prospect_id,data->'participacoes'->jsonb_object_keys("data"->'participacoes')->>'restricao' as empresas_problemas
		from credito_coleta cc
		join(select dp.direct_prospect_id,
				max(dp.cnpj) as cnpj,
				max(cc.id) as max_id
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join temp_ids as aux on aux.direct_prospect_id = dp.direct_prospect_id
			left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
				from direct_prospects dp
				left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
				left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
				left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
				group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
			left join offers o on o.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
			left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on coalesce(max_id ,min_id) = lr.loan_request_id) lr on lr.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
			where tipo in ('Serasa','RelatoSocios','RelatoBasico') and coalesce(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS'),data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '1 day'
			group by 1) as t1 on cc.id = max_id) as t1
	group by 1) as serasaColig on serasaColig.direct_prospect_id = dp.direct_prospect_id 
--------------------------TABLE SERASA CONSULTA FACTORING SERASA
left join (select direct_prospect_id,
	sum(case when position('INVEST DIR' in consultas) > 0 or 
		position('CREDITORIOS' in consultas) > 0 or 
		position('FUNDO DE INVE' in consultas) > 0 or 
		position('SECURITIZADORA' in consultas) > 0 or 
		position('FACTORING'in consultas) > 0 or 
		position('FOMENTO'in consultas) > 0 or 
		position('FIDC' in consultas) > 0 or 
		position('NOVA S R M' in consultas) > 0 or 
		position('RED AS' in consultas) > 0 or 
		position('DEL MONTE SERVICOS' in consultas) > 0 or 
		position('SERVICOS FINANCEIROS' in consultas) > 0 then 1 else 0 end) as consulta_factoring
	from(--Serasa
			select t1.direct_prospect_id,cc.data->'consulta_empresas'->jsonb_object_keys(data->'consulta_empresas')->>'nome' as consultas
				from credito_coleta cc
				join(select dp.direct_prospect_id,
						max(dp.cnpj) as cnpj, 
						max(cc.id) as max_id
					from credito_coleta cc
					join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
					join temp_ids as aux on aux.direct_prospect_id = dp.direct_prospect_id
					left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
						from direct_prospects dp
						left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
						left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
						left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
						group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
					left join offers o on o.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
					left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on coalesce(max_id ,min_id) = lr.loan_request_id) lr on lr.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
					where tipo in ('Serasa','RelatoSocios','RelatoBasico') and coalesce(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS'),data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '1 day'
					group by 1) as t1 on cc.id = max_id
		union--SPC
			select t1.direct_prospect_id,cc.data->'consulta_spc'->jsonb_object_keys(data->'consulta_spc')->>'nome' as consultas
				from credito_coleta cc
				join(select 
						dp.direct_prospect_id,
						max(dp.cnpj) as cnpj, 
						max(cc.id) as max_id
					from credito_coleta cc
					join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
					join temp_ids as aux on aux.direct_prospect_id = dp.direct_prospect_id
					left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
						from direct_prospects dp
						left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
						left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
						left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
						group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
					left join offers o on o.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
					left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on coalesce(max_id ,min_id) = lr.loan_request_id) lr on lr.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
					where tipo in ('Serasa','RelatoSocios','RelatoBasico') and coalesce(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS'),data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '1 day'
					group by 1) as t1 on cc.id = max_id) as t1
		group by 1
	union
		select
			lead_id as direct_prospect_id,
			experian_report_recently_taken_by_factoring as consulta_factoring
		from lucas_leal.serasas_antigos_parseados) as Consulta on Consulta.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SERASA CONSULTAS
left join (select direct_prospect_id,
	sum(case when serasa = '' then '0' else coalesce(serasa,'0') end::int) filter (where mes = data_base) as consultas_serasa_atual,
	sum(case when serasa = '' then '0' else coalesce(serasa,'0') end::int) filter (where mes = data_base - interval '1 month') as consultas_serasa_1m,
	sum(case when serasa = '' then '0' else coalesce(serasa,'0') end::int) filter (where mes = data_base - interval '2 months') as consultas_serasa_2m,
	sum(case when serasa = '' then '0' else coalesce(serasa,'0') end::int) filter (where mes = data_base - interval '3 months') as consultas_serasa_3m,
	sum(case when serasa = '' then '0' else coalesce(serasa,'0') end::int) filter (where mes = data_base - interval '4 months') as consultas_serasa_4m,
	sum(case when serasa = '' then '0' else coalesce(serasa,'0') end::int) filter (where mes = data_base - interval '5 months') as consultas_serasa_5m,
	sum(case when serasa = '' then '0' else coalesce(serasa,'0') end::int) filter (where mes = data_base - interval '6 months') as consultas_serasa_6m,
	sum(case when serasa = '' then '0' else coalesce(serasa,'0') end::int) filter (where mes = data_base - interval '7 months') as consultas_serasa_7m,
	sum(case when serasa = '' then '0' else coalesce(serasa,'0') end::int) filter (where mes = data_base - interval '8 months') as consultas_serasa_8m,
	sum(case when serasa = '' then '0' else coalesce(serasa,'0') end::int) filter (where mes = data_base - interval '9 months') as consultas_serasa_9m,
	sum(case when serasa = '' then '0' else coalesce(serasa,'0') end::int) filter (where mes = data_base - interval '10 months') as consultas_serasa_10m,
	sum(case when serasa = '' then '0' else coalesce(serasa,'0') end::int) filter (where mes = data_base - interval '11 months') as consultas_serasa_11m
	from (select direct_prospect_id,
			to_date(to_char(to_date(cc.data->'datetime'->>'data','yyyymmdd'),'Mon/yy'),'Mon/yy') as data_base, 
			to_date(jsonb_object_keys(data->'consultas'),'yy/mm') as mes,
			data->'consultas'->jsonb_object_keys(data->'consultas')->>'pesquisaBancos' as serasa
		from credito_coleta cc
		join(select dp.direct_prospect_id,
				max(dp.cnpj) as cnpj, 
				max(cc.id) as max_date
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join temp_ids as aux on aux.direct_prospect_id = dp.direct_prospect_id
			left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
				from direct_prospects dp
				left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
				left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
				left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
				group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
			left join offers o on o.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
			left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on coalesce(max_id ,min_id) = lr.loan_request_id) lr on lr.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
			where tipo in ('Serasa','RelatoSocios','RelatoBasico') and coalesce(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS'),data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '1 day'
			group by 1) as t1 on cc.id = max_date) as t2
	group by 1) as serasa_consultas on serasa_consultas.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SPC CONSULTAS
left join (select direct_prospect_id,
	sum(case when spc = '' then '0' else coalesce(spc,'0') end::int) filter (where mes = data_base) as consultas_spc_atual,
	sum(case when spc = '' then '0' else coalesce(spc,'0') end::int) filter (where mes = data_base - interval '1 month') as consultas_spc_1m,
	sum(case when spc = '' then '0' else coalesce(spc,'0') end::int) filter (where mes = data_base - interval '2 months') as consultas_spc_2m,
	sum(case when spc = '' then '0' else coalesce(spc,'0') end::int) filter (where mes = data_base - interval '3 months') as consultas_spc_3m,
	sum(case when spc = '' then '0' else coalesce(spc,'0') end::int) filter (where mes = data_base - interval '4 months') as consultas_spc_4m,
	sum(case when spc = '' then '0' else coalesce(spc,'0') end::int) filter (where mes = data_base - interval '5 months') as consultas_spc_5m,
	sum(case when spc = '' then '0' else coalesce(spc,'0') end::int) filter (where mes = data_base - interval '6 months') as consultas_spc_6m,
	sum(case when spc = '' then '0' else coalesce(spc,'0') end::int) filter (where mes = data_base - interval '7 months') as consultas_spc_7m,
	sum(case when spc = '' then '0' else coalesce(spc,'0') end::int) filter (where mes = data_base - interval '8 months') as consultas_spc_8m,
	sum(case when spc = '' then '0' else coalesce(spc,'0') end::int) filter (where mes = data_base - interval '9 months') as consultas_spc_9m,
	sum(case when spc = '' then '0' else coalesce(spc,'0') end::int) filter (where mes = data_base - interval '10 months') as consultas_spc_10m,
	sum(case when spc = '' then '0' else coalesce(spc,'0') end::int) filter (where mes = data_base - interval '11 months') as consultas_spc_11m
	from (select direct_prospect_id,
			to_date(to_char(to_date(cc.data->'datetime'->>'data','yyyymmdd'),'Mon/yy'),'Mon/yy') as data_base, 
				to_date(jsonb_object_keys(data->'consultas'),'yy/mm') as mes,
				replace(data->'registro_spc'->jsonb_object_keys(data->'registro_spc')->>'quantidade',' ','') spc
		from credito_coleta cc
		join(select dp.direct_prospect_id,
				max(dp.cnpj) as cnpj, 
				max(id) as max_date
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join temp_ids as aux on aux.direct_prospect_id = dp.direct_prospect_id
			left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
				from direct_prospects dp
				left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
				left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
				left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
				group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
			left join offers o on o.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
			left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on coalesce(max_id ,min_id) = lr.loan_request_id) lr on lr.offer_id = coalesce(acc_offer_id ,max_offer_id,other_offer_id)
			where tipo in ('Serasa','RelatoSocios','RelatoBasico') and coalesce(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS'),data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '1 day'
			group by 1) as t1 on cc.id = max_date) as t2
	group by 1
union		
	select
		lead_id as direct_prospect_id,
		experian_inquiries_curr as consultas_spc_atual,
		experian_inquiries_2m as consultas_spc_1m,
		experian_inquiries_3m as consultas_spc_2m,
		experian_inquiries_4m as consultas_spc_3m,
		experian_inquiries_5m as consultas_spc_4m,
		experian_inquiries_6m as consultas_spc_5m,
		experian_inquiries_7m as consultas_spc_6m,
		experian_inquiries_8m as consultas_spc_7m,
		experian_inquiries_9m as consultas_spc_8m,
		experian_inquiries_10m as consultas_spc_9m,
		experian_inquiries_11m as consultas_spc_10m,
		experian_inquiries_12m as consultas_spc_11m
	from lucas_leal.serasas_antigos_parseados) as spc_consultas on spc_consultas.direct_prospect_id = dp.direct_prospect_id
-------------------------TABLE CNPJs RELACIONADOS
left join(select direct_prospect_id,count(distinct cnpjs_relacionados) filter (where cnpjs_relacionados <> 'NA') as n_cnpjs_relacionados
	from(select dp.direct_prospect_id, workflow, unnest(string_to_array(replace(replace(replace(cnpjs_relacionados::text,'{}','NA'),'{',''),'}',''),',')) as cnpjs_relacionados
		from direct_prospects dp
		join temp_ids as aux on aux.direct_prospect_id = dp.direct_prospect_id) as t1
	where cnpjs_relacionados is not null
	group by 1) as cnpjs_relacionados on cnpjs_relacionados.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE CNPJs RELACIONADOS
left join(select direct_prospect_id,count(distinct cpfs_relacionados) filter (where cpfs_relacionados <> 'NA') as n_cpfs_relacionados
	from(select dp.direct_prospect_id, workflow,unnest(string_to_array(replace(replace(replace(cpfs_relacionados::text,'{}','NA'),'{',''),'}',''),',')) as cpfs_relacionados
		from direct_prospects dp
		join temp_ids as aux on aux.direct_prospect_id = dp.direct_prospect_id) as t1
	where cpfs_relacionados is not null
	group by 1) as cpfs_relacionados on cpfs_relacionados.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE CREDITO PARECER
left join (select emprestimo_id,
		count(*) filter (where criado_por = 67) as bizbot, 
		count(*) filter (where criado_por != 67) as nao_bizbot
	from credito_parecer 
	group by 1) as cp on cp.emprestimo_id = lr.loan_request_id
--------------------------TABLE AGENCIA BANCARIA
left join (select 
	uf, 
	count(*) as total_agencias_uf 
	from  agencias_bancarias_dez19 
	group by 1) agencias_uf on agencias_uf.uf = dp.state
--------------------------
left join (select 
	uf || '-' || municipio as id, 
	count(*) as total_agencias_municipio 
	from  agencias_bancarias_dez19 
	group by 1) agencias_municipio on agencias_municipio.id = dp.state || '-' || dp.city
--------------------------
left join (select 
	uf || '-' || municipio || '-' || bairro as id, 
	count(*) as total_agencias_bairro
	from  agencias_bancarias_dez19 
	group by 1) agencias_bairro on agencias_bairro.id = dp.state || '-' || dp.city || '-' || dp.neighbourhood
--------------------------
left join (select 
	get_postal_code_lucas(uf,cep) as postal, 
	count(*) as total_agencias_postal 
	from  agencias_bancarias_dez19 
	group by 1) agencias_postal on agencias_postal.postal = dp.state
--------------------------
	
	
	

