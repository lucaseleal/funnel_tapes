CREATE INDEX t_behaviour_score_historico_v2_emprestimo_id ON lucas_leal.t_behaviour_score_historico_v2 USING btree (emprestimo_id);
CREATE INDEX t_behaviour_score_historico_v2_dia ON lucas_leal.t_behaviour_score_historico_v2 USING btree (dia);

truncate lucas_leal.t_behaviour_score_historico_v2

select count(*),count(distinct emprestimo_id)
from t_behaviour_score_historico_v2

insert into lucas_leal.t_behaviour_score_historico_v2
select 
	t3.emprestimo_id,
	t3.dia,
    1::numeric / (1::numeric + exp('-1'::integer::numeric * ('-1.5877'::numeric + t3.woelateinstallments * 0.5193 + t3.woemaxlateinstallment * 0.3745 + t3.woeavgdayslate * 0.3307 + t3.woeearlyinstallments * 0.3473))) AS behaviourscore
from(select 
		t2.emprestimo_id,
		t2.dia,
	    CASE
	        WHEN t2.latedayscount > 4 THEN 2.1512
	        WHEN t2.latedayscount < 0 THEN 0::numeric
	        WHEN COALESCE(t2.collection_loan_max_late_by, 0) > 12 THEN 1.5663
	        ELSE '-0.7497'::numeric
	    END AS woelateinstallments,
	    CASE
	        WHEN t2.loan_original_final_term = 0 THEN '9999999999999999999999999999'::numeric
	        WHEN (COALESCE(t2.collection_pmt_of_max_late, 0)::numeric / t2.loan_original_final_term::numeric) > 0.75 THEN 1.192
	        WHEN (COALESCE(t2.collection_pmt_of_max_late, 0)::numeric / t2.loan_original_final_term::numeric) > 0.3 THEN 0.8243
	        WHEN (COALESCE(t2.collection_pmt_of_max_late, 0)::numeric / t2.loan_original_final_term::numeric) <= 0.3 THEN '-1.0517'::numeric
	        ELSE NULL::numeric
	    END AS woemaxlateinstallment,
	    CASE
	        WHEN t2.latedayscount = 0 OR t2.latedayssum = 0 THEN '-1.08378'::numeric
	        WHEN (t2.latedayssum::numeric / t2.latedayscount::numeric) > 7::numeric THEN 1.903417
	        WHEN (t2.latedayssum::numeric / t2.latedayscount::numeric) > 0::numeric THEN '-0.21778'::numeric
	        WHEN (t2.latedayssum::numeric / t2.latedayscount::numeric) <= 0::numeric THEN '-1.08378'::numeric
	        ELSE NULL::numeric
	    END AS woeavgdayslate,
	    CASE
	        WHEN t2.loan_original_final_term = 0 THEN '9999999999999999999999999999'::numeric
	        WHEN (t2.nearlyinstallments::numeric / t2.loan_original_final_term::numeric) > 0.272727 THEN '-1.0628'::numeric
	        WHEN (t2.nearlyinstallments::numeric / t2.loan_original_final_term::numeric) > 0.0555556 THEN '-0.1965'::numeric
	        WHEN (t2.nearlyinstallments::numeric / t2.loan_original_final_term::numeric) <= 0.0555556 THEN 0.7333
	        ELSE NULL::numeric
	    END AS woeearlyinstallments,
	    collection_loan_max_late_by,
	    latedayssum
	from(select
			t1.emprestimo_id,
			t1.dia,
	        CASE GREATEST(0,t1.max_late_pmt1, t1.max_late_pmt2, t1.max_late_pmt3, t1.max_late_pmt4, t1.max_late_pmt5, t1.max_late_pmt6, t1.max_late_pmt7, t1.max_late_pmt8, t1.max_late_pmt9, t1.max_late_pmt10, t1.max_late_pmt11, t1.max_late_pmt12, t1.max_late_pmt13, t1.max_late_pmt14, t1.max_late_pmt15, t1.max_late_pmt16, t1.max_late_pmt17, t1.max_late_pmt18, t1.max_late_pmt19, t1.max_late_pmt20, t1.max_late_pmt21, t1.max_late_pmt22, t1.max_late_pmt23, t1.max_late_pmt24)
	            WHEN 0 THEN 0
	            WHEN t1.max_late_pmt1 THEN 1
	            WHEN t1.max_late_pmt2 THEN 2
	            WHEN t1.max_late_pmt3 THEN 3
	            WHEN t1.max_late_pmt4 THEN 4
	            WHEN t1.max_late_pmt5 THEN 5
	            WHEN t1.max_late_pmt6 THEN 6
	            WHEN t1.max_late_pmt7 THEN 7
	            WHEN t1.max_late_pmt8 THEN 8
	            WHEN t1.max_late_pmt9 THEN 9
	            WHEN t1.max_late_pmt10 THEN 10
	            WHEN t1.max_late_pmt11 THEN 11
	            WHEN t1.max_late_pmt12 THEN 12
	            WHEN t1.max_late_pmt13 THEN 13
	            WHEN t1.max_late_pmt14 THEN 14
	            WHEN t1.max_late_pmt15 THEN 15
	            WHEN t1.max_late_pmt16 THEN 16
	            WHEN t1.max_late_pmt17 THEN 17
	            WHEN t1.max_late_pmt18 THEN 18
	            WHEN t1.max_late_pmt19 THEN 19
	            WHEN t1.max_late_pmt20 THEN 20
	            WHEN t1.max_late_pmt21 THEN 21
	            WHEN t1.max_late_pmt22 THEN 22
	            WHEN t1.max_late_pmt23 THEN 23
	            WHEN t1.max_late_pmt24 THEN 24
	            ELSE NULL::integer
	        END AS collection_pmt_of_max_late,
	        GREATEST(t1.max_late_pmt1, 
	        	t1.max_late_pmt2, 
	        	t1.max_late_pmt3, 
	        	t1.max_late_pmt4, 
	        	t1.max_late_pmt5, 
	        	t1.max_late_pmt6, 
	        	t1.max_late_pmt7, 
	        	t1.max_late_pmt8, 
	        	t1.max_late_pmt9, 
	        	t1.max_late_pmt10, 
	        	t1.max_late_pmt11, 
	        	t1.max_late_pmt12, 
	        	t1.max_late_pmt13, 
	        	t1.max_late_pmt14, 
	        	t1.max_late_pmt15, 
	        	t1.max_late_pmt16, 
	        	t1.max_late_pmt17, 
	        	t1.max_late_pmt18, 
	        	t1.max_late_pmt19, 
	        	t1.max_late_pmt20, 
	        	t1.max_late_pmt21, 
	        	t1.max_late_pmt22, 
	        	t1.max_late_pmt23, 
	        	t1.max_late_pmt24) AS collection_loan_max_late_by,
			(coalesce(t1.max_late_pmt1,0) < 0)::int + 
				(coalesce(t1.max_late_pmt2,0) < 0)::int +
				(coalesce(t1.max_late_pmt3,0) < 0)::int +
				(coalesce(t1.max_late_pmt4,0) < 0)::int +
				(coalesce(t1.max_late_pmt5,0) < 0)::int +
				(coalesce(t1.max_late_pmt6,0) < 0)::int +
				(coalesce(t1.max_late_pmt7,0) < 0)::int +
				(coalesce(t1.max_late_pmt8,0) < 0)::int +
				(coalesce(t1.max_late_pmt9,0) < 0)::int +
				(coalesce(t1.max_late_pmt10,0) < 0)::int +
				(coalesce(t1.max_late_pmt11,0) < 0)::int +
				(coalesce(t1.max_late_pmt12,0) < 0)::int +
				(coalesce(t1.max_late_pmt13,0) < 0)::int +
				(coalesce(t1.max_late_pmt14,0) < 0)::int +
				(coalesce(t1.max_late_pmt15,0) < 0)::int +
				(coalesce(t1.max_late_pmt16,0) < 0)::int +
				(coalesce(t1.max_late_pmt17,0) < 0)::int +
				(coalesce(t1.max_late_pmt18,0) < 0)::int +
				(coalesce(t1.max_late_pmt19,0) < 0)::int +
				(coalesce(t1.max_late_pmt20,0) < 0)::int +
				(coalesce(t1.max_late_pmt21,0) < 0)::int +
				(coalesce(t1.max_late_pmt22,0) < 0)::int +
				(coalesce(t1.max_late_pmt23,0) < 0)::int +
				(coalesce(t1.max_late_pmt24,0) < 0)::int as nearlyinstallments,
			(coalesce(t1.max_late_pmt1,0) > 0)::int + 
				(coalesce(t1.max_late_pmt2,0) > 0)::int +
				(coalesce(t1.max_late_pmt3,0) > 0)::int +
				(coalesce(t1.max_late_pmt4,0) > 0)::int +
				(coalesce(t1.max_late_pmt5,0) > 0)::int +
				(coalesce(t1.max_late_pmt6,0) > 0)::int +
				(coalesce(t1.max_late_pmt7,0) > 0)::int +
				(coalesce(t1.max_late_pmt8,0) > 0)::int +
				(coalesce(t1.max_late_pmt9,0) > 0)::int +
				(coalesce(t1.max_late_pmt10,0) > 0)::int +
				(coalesce(t1.max_late_pmt11,0) > 0)::int +
				(coalesce(t1.max_late_pmt12,0) > 0)::int +
				(coalesce(t1.max_late_pmt13,0) > 0)::int +
				(coalesce(t1.max_late_pmt14,0) > 0)::int +
				(coalesce(t1.max_late_pmt15,0) > 0)::int +
				(coalesce(t1.max_late_pmt16,0) > 0)::int +
				(coalesce(t1.max_late_pmt17,0) > 0)::int +
				(coalesce(t1.max_late_pmt18,0) > 0)::int +
				(coalesce(t1.max_late_pmt19,0) > 0)::int +
				(coalesce(t1.max_late_pmt20,0) > 0)::int +
				(coalesce(t1.max_late_pmt21,0) > 0)::int +
				(coalesce(t1.max_late_pmt22,0) > 0)::int +
				(coalesce(t1.max_late_pmt23,0) > 0)::int +
				(coalesce(t1.max_late_pmt24,0) > 0)::int as latedayscount,
			(coalesce(t1.max_late_pmt1,0) > 0)::int * coalesce(t1.max_late_pmt1,0) + 
				(coalesce(t1.max_late_pmt2,0) > 0)::int * coalesce(t1.max_late_pmt2,0) +
				(coalesce(t1.max_late_pmt3,0) > 0)::int * coalesce(t1.max_late_pmt3,0) +
				(coalesce(t1.max_late_pmt4,0) > 0)::int * coalesce(t1.max_late_pmt4,0) +
				(coalesce(t1.max_late_pmt5,0) > 0)::int * coalesce(t1.max_late_pmt5,0) +
				(coalesce(t1.max_late_pmt6,0) > 0)::int * coalesce(t1.max_late_pmt6,0) +
				(coalesce(t1.max_late_pmt7,0) > 0)::int * coalesce(t1.max_late_pmt7,0) +
				(coalesce(t1.max_late_pmt8,0) > 0)::int * coalesce(t1.max_late_pmt8,0) +
				(coalesce(t1.max_late_pmt9,0) > 0)::int * coalesce(t1.max_late_pmt9,0) +
				(coalesce(t1.max_late_pmt10,0) > 0)::int * coalesce(t1.max_late_pmt10,0) +
				(coalesce(t1.max_late_pmt11,0) > 0)::int * coalesce(t1.max_late_pmt11,0) +
				(coalesce(t1.max_late_pmt12,0) > 0)::int * coalesce(t1.max_late_pmt12,0) +
				(coalesce(t1.max_late_pmt13,0) > 0)::int * coalesce(t1.max_late_pmt13,0) +
				(coalesce(t1.max_late_pmt14,0) > 0)::int * coalesce(t1.max_late_pmt14,0) +
				(coalesce(t1.max_late_pmt15,0) > 0)::int * coalesce(t1.max_late_pmt15,0) +
				(coalesce(t1.max_late_pmt16,0) > 0)::int * coalesce(t1.max_late_pmt16,0) +
				(coalesce(t1.max_late_pmt17,0) > 0)::int * coalesce(t1.max_late_pmt17,0) +
				(coalesce(t1.max_late_pmt18,0) > 0)::int * coalesce(t1.max_late_pmt18,0) +
				(coalesce(t1.max_late_pmt19,0) > 0)::int * coalesce(t1.max_late_pmt19,0) +
				(coalesce(t1.max_late_pmt20,0) > 0)::int * coalesce(t1.max_late_pmt20,0) +
				(coalesce(t1.max_late_pmt21,0) > 0)::int * coalesce(t1.max_late_pmt21,0) +
				(coalesce(t1.max_late_pmt22,0) > 0)::int * coalesce(t1.max_late_pmt22,0) +
				(coalesce(t1.max_late_pmt23,0) > 0)::int * coalesce(t1.max_late_pmt23,0) +
				(coalesce(t1.max_late_pmt24,0) > 0)::int * coalesce(t1.max_late_pmt24,0) as latedayssum,
		        fpp.parcelas_qtd AS loan_original_final_term
		from(select
				emprestimo_id,
				dia,
				max(atraso_max) filter (where parcela = 1) as max_late_pmt1,
				max(atraso_max) filter (where parcela = 2) as max_late_pmt2,
				max(atraso_max) filter (where parcela = 3) as max_late_pmt3,
				max(atraso_max) filter (where parcela = 4) as max_late_pmt4,
				max(atraso_max) filter (where parcela = 5) as max_late_pmt5,
				max(atraso_max) filter (where parcela = 6) as max_late_pmt6,
				max(atraso_max) filter (where parcela = 7) as max_late_pmt7,
				max(atraso_max) filter (where parcela = 8) as max_late_pmt8,
				max(atraso_max) filter (where parcela = 9) as max_late_pmt9,
				max(atraso_max) filter (where parcela = 10) as max_late_pmt10,
				max(atraso_max) filter (where parcela = 11) as max_late_pmt11,
				max(atraso_max) filter (where parcela = 12) as max_late_pmt12,
				max(atraso_max) filter (where parcela = 13) as max_late_pmt13,
				max(atraso_max) filter (where parcela = 14) as max_late_pmt14,
				max(atraso_max) filter (where parcela = 15) as max_late_pmt15,
				max(atraso_max) filter (where parcela = 16) as max_late_pmt16,
				max(atraso_max) filter (where parcela = 17) as max_late_pmt17,
				max(atraso_max) filter (where parcela = 18) as max_late_pmt18,
				max(atraso_max) filter (where parcela = 19) as max_late_pmt19,
				max(atraso_max) filter (where parcela = 20) as max_late_pmt20,
				max(atraso_max) filter (where parcela = 21) as max_late_pmt21,
				max(atraso_max) filter (where parcela = 22) as max_late_pmt22,
				max(atraso_max) filter (where parcela = 23) as max_late_pmt23,
				max(atraso_max) filter (where parcela = 24) as max_late_pmt24
			from lucas_leal.t_collection_soft
			where dia > (select max(dia) from lucas_leal.t_behaviour_score_historico_v2)
			group by 1,2
			) as t1
		join (select 
				fp.emprestimo_id,
				fp.parcelas_qtd
			from public.financeiro_planopagamento fp 
			where "type" = 'ORIGINAL') fpp on fpp.emprestimo_id = t1.emprestimo_id
		) as t2
	) as t3


