

insert into t_signers_data
(select
	loan_request_id,
	max(signer_age) filter (where indice_administrador = 1 and signer_is_administrator) as administrator_age,
	max(signer_revenue) filter (where indice_administrador = 1 and signer_is_administrator) as administrator_revenue,
	max(signer_civil_status) filter (where indice_administrador = 1 and signer_is_administrator) as administrator_civil_status,
	max(signer_gender) filter (where indice_administrador = 1 and signer_is_administrator) as administrator_gender,
    avg(signer_age) FILTER (WHERE signer_share_percentage > 0) AS avg_age_partners,
    avg(signer_revenue) FILTER (WHERE signer_share_percentage > 0) AS avg_revenue_partners,
    count(*) FILTER (WHERE signer_gender = 'MALE' AND signer_share_percentage > 0) AS male_partners,
    count(*) FILTER (WHERE signer_gender = 'FEMALE' AND signer_share_percentage > 0) AS female_partners,
    count(*) FILTER (WHERE signer_share_percentage > 0) AS count_socios,
    count(*) FILTER (WHERE signer_is_avalista) AS count_avalistas,
    count(*) filter (where signer_is_avalista and coalesce(signer_share_percentage,0) = 0) as count_avalistas_nao_socios,
    count(*) AS count_total,
    count(*) filter (where signer_share_percentage > 0 and signer_is_solicitante) as is_shareholder,
	max(signer_age) filter (where indice_socio_majoritario = 1 and signer_share_percentage > 0) as majorsigner_age,
	max(signer_revenue) filter (where indice_socio_majoritario = 1 and signer_share_percentage > 0) as majorsigner_revenue,
	max(signer_civil_status) filter (where indice_socio_majoritario = 1 and signer_share_percentage > 0) as majorsigner_civil_status,
	max(signer_gender) filter (where indice_socio_majoritario = 1 and signer_share_percentage > 0) as majorsigner_gender,
	max(signer_age) filter (where indice_socio_mais_velho = 1 and signer_share_percentage > 0) as oldestsh_age,
	max(signer_revenue) filter (where indice_socio_mais_velho = 1 and signer_share_percentage > 0) as oldestsh_revenue,
	max(signer_civil_status) filter (where indice_socio_mais_velho = 1 and signer_share_percentage > 0) as oldestsh_civil_status,
	max(signer_gender) filter (where indice_socio_mais_velho = 1 and signer_share_percentage > 0) as oldestsh_gender,
	max(signer_age) filter (where signer_is_solicitante) as solicsignerscpf_age,
	max(signer_revenue) filter (where signer_is_solicitante) as solicsignerscpf_revenue,
	max(signer_civil_status) filter (where signer_is_solicitante) as solicsignerscpf_civil_status,
	max(signer_gender) filter (where signer_is_solicitante) as solicsignerscpf_gender
from(select 
		lr.loan_request_id,
		lr.loan_date,
		coalesce(s.share_percentage,es.participacao,ser.share) as signer_share_percentage,
		to_char(age(lr.loan_date::date, s.date_of_birth::date), 'yyyy'::text)::integer AS signer_age,
		s.revenue::numeric as signer_revenue,
		s.gender::text::character varying(50) as signer_gender,
		s.guarantor as signer_is_avalista,
		coalesce(s.cpf,es.cpf,ser.cpf_socios) as signer_cpf,
		coalesce(s.cpf,es.cpf,ser.cpf_socios) = dp.cpf as signer_is_solicitante,
		s.civil_status as signer_civil_status,
		coalesce(s.name,es.nome,ser.nome) as signer_name,
		s.email as signer_email,
		s.profession as signer_profession,
		s.rg as signer_rg,
		s.issuing_body as signer_issuing_body,
		s.country_of_birth as signer_country_of_birth,
		s.city_of_birth as signer_city_of_birth,
		s.date_of_birth as signer_date_of_bith,
		sa.street as signer_street_address,
		sa."number" as signer_number_address,
		sa.complement as signer_complement_address,
		sa.zip_code as signer_zip_code_address,
		c."name" as signer_city_address,
		ss."name" as signer_state_address,
		sa.country as signer_country_address,
		sa.neighbourhood as signer_neighbourhood_address,
		s.sign_spouse as signer_sign_spouse,
		s.shareholder as signer_is_shareholder,
		s.administrator as signer_is_administrator,
		s.spouse->>'cpf' as signer_spouse_cpf,
		s.spouse->>'name' as signer_spouse_name,
		s.spouse->>'email' as signer_spouse_email,
		s.signer_id as signer_id,
		s.client_id as signer_client_id,
		es_info.qualificacao as signer_neoway_qualificao,
		es_info.nivel_pep as signer_neoway_nivel_pep,
		es_info.ultima_atualizacao::date as signer_neoway_ultima_atualizacao,
		es_info.falecido as siger_neoway_falecido,
		row_number() over (partition by lr.loan_request_id order by case when s.administrator then 1 else 2 end, s.date_of_birth) as indice_administrador,
		row_number() over (partition by lr.loan_request_id order by coalesce(s.share_percentage,es.participacao,ser.share,0) desc, s.date_of_birth) as indice_socio_majoritario,
		row_number() over (partition by lr.loan_request_id order by case coalesce(s.share_percentage,es.participacao,ser.share,0) when 0 then null else s.date_of_birth end, coalesce(s.share_percentage,es.participacao,ser.share,0) desc) as indice_socio_mais_velho
	from public.direct_prospects dp
	join public.offers o on o.direct_prospect_id = dp.direct_prospect_id
	join public.loan_requests lr on lr.offer_id = o.offer_id
	left join public.loan_requests_signers lrs on lrs.loan_request_id = lr.loan_request_id
	left join public.signers s on s.signer_id = lrs.signer_id
	left join public.signer_addresses sa on sa.address_id = s.address_id
	left join public.cities c on c.city_id = sa.city_id
	left join public.states ss on ss.state_id = c.state_id
	left join (select
			es.cnpj,
			es.cpf,
			es.nome,
			es.participacao,
			row_number() over (partition by es.cnpj,es.cpf order by es.id) as indice
		from public.empresas_socios es
		left join (select distinct 
				dp.cnpj
			from public.signers s
			join public.direct_prospects dp on dp.client_id = s.client_id) as s on s.cnpj = es.cnpj
		where s.cnpj is null) es on es.cnpj = dp.cnpj and es.indice = 1
	left join (SELECT 
			t1.direct_prospect_id,
			t1.loan_request_id,
			t1.cnpj,
			t1.cpf,
		    ((cc.data -> 'acionistas'::text) -> jsonb_object_keys(cc.data -> 'acionistas'::text)) ->> 'cpf'::text AS cpf_socios,
			jsonb_object_keys(cc.data -> 'acionistas'::text) AS nome,
			(((cc.data -> 'acionistas'::text) -> jsonb_object_keys(cc.data -> 'acionistas'::text)) ->> 'percentual'::text)::numeric AS share
		FROM credito_coleta cc
		JOIN (SELECT dp.direct_prospect_id,
				max(lr.loan_request_id) as loan_request_id,
				max(dp.cnpj) as cnpj,
				max(dp.cpf) as cpf,
				max(cc.id) AS max_id
			FROM public.credito_coleta cc
			JOIN public.direct_prospects dp ON "left"(cc.documento::text, 8) = "left"(dp.cnpj::text, 8)
			join public.offers o on o.direct_prospect_id = dp.direct_prospect_id
			join public.loan_requests lr on lr.offer_id = o.offer_id
			left join (select 
					cnpj,
					max(participacao) as max_part 
				from public.empresas_socios
				group by 1) es on es.cnpj = dp.cnpj
			left join public.loan_requests_signers lrs on lrs.loan_request_id = lr.loan_request_id
			left join public.signers s on s.signer_id = lrs.signer_id
			WHERE to_date((cc.data -> 'datetime'::text) ->> 'data'::text, 'yyyymmdd'::text) <= lr.loan_date 
				AND (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying]::text[]))
				and es.cnpj is null
				and s.signer_id is null
				and lr.contrato_assinado_url is not null and lr.status = 'ACCEPTED'
			GROUP BY 1) t1 ON cc.id = t1.max_id) ser on ser.direct_prospect_id = dp.direct_prospect_id
	left join (select
			cnpj,
			cpf,
			nome,
			nivel_pep,
			qualificacao,
			participacao,
			ultima_atualizacao,
			falecido,
			row_number() over (partition by cnpj,cpf order by id) as indice
		from public.empresas_socios
		where participacao > 0) es_info on es_info.cnpj = dp.cnpj and es_info.cpf = coalesce(s.cpf,es.cpf,ser.cpf_socios) and es_info.indice = 1
	where lr.status::text = 'ACCEPTED'::text and lr.contrato_assinado_url is not null
		and lr.loan_request_id not in (select loan_request_id from t_signers_data)) as t1
group by 1)





 --t_signers_zip_code_data
insert into t_signers_zip_code_data 
(SELECT dp.direct_prospect_id,
    max(replace(sa.zip_code::text, '-'::text, ''::text)) AS signer_zipcode
   FROM direct_prospects dp
     JOIN offers o ON o.direct_prospect_id = dp.direct_prospect_id
     JOIN loan_requests lr ON lr.offer_id = o.offer_id
     JOIN loan_requests_signers lrs ON lrs.loan_request_id = lr.loan_request_id
     JOIN signers s ON s.signer_id = lrs.signer_id
     JOIN signer_addresses sa ON sa.address_id = s.address_id
  WHERE (dp.zip_code::text = ''::text OR dp.zip_code IS null) and dp.direct_prospect_id not in (select direct_prospect_id from t_signers_zip_code_data)
  GROUP BY dp.direct_prospect_id)

