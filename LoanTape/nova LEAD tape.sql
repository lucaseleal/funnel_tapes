grant select on data_science.lead_tape to priscila_mello;
grant select on data_science.lead_tape to data_science;
grant select on data_science.lead_tape to pedro_malan;
grant select on data_science.lead_tape to marcelo_prado;
grant select on data_science.lead_tape to cristiano_rocha;
grant select on data_science.lead_tape to rodrigo_pinho;
grant select on data_science.lead_tape to mauricio_werneck;
grant select on data_science.lead_tape to chicao;
grant select on data_science.lead_tape to caio_camara;
grant select on data_science.lead_tape to chicao;
grant select on lucas_leal.mv_lead_tape to isabel_tavares;

drop materialized view lucas_leal.mv_lead_tape;
alter table lucas_leal.mv_lead_tape2 rename to mv_lead_tape;
refresh materialized view lucas_leal.mv_lead_tape with data;

-- View indexes:
CREATE UNIQUE INDEX mv_lead_tape_lead_id ON lucas_leal.mv_lead_tape USING btree (lead_id);
CREATE INDEX mv_lead_tape_loan_date ON lucas_leal.mv_lead_tape USING btree (loan_contract_date);
CREATE INDEX mv_lead_tape_opt_in_date ON lucas_leal.mv_lead_tape USING btree (lead_opt_in_date);
CREATE INDEX mv_lead_tape_cohort_lead ON lucas_leal.mv_lead_tape USING btree (lead_cohort_lead);
CREATE INDEX mv_lead_tape_cohort_loan ON lucas_leal.mv_lead_tape USING btree (loan_cohort_loan);
CREATE UNIQUE INDEX mv_lead_tape_loan_request_id ON lucas_leal.mv_lead_tape USING btree (loan_request_id);

select this_view_last_refresh_time 
from mv_lead_tape mlt 
limit 1

create materialized view lucas_leal.mv_lead_tape2 as
--------------------------DIRETO
select 
	dp.direct_prospect_id as lead_id,
	dp.workflow as lead_status,
	dp.opt_in_date as lead_opt_in_date,
	to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy') as lead_cohort_lead,
	extract(hour from dp.opt_in_date)::int as lead_opt_in_hour,
	extract(day from dp.opt_in_date)::int as lead_opt_in_day,
	extract(dow from dp.opt_in_date)::int as lead_opt_in_dow,
	(extract('day' from date_trunc('week', case when extract(isodow from dp.opt_in_date::date) = 7 then dp.opt_in_date::date::date + 1 when extract(isodow from dp.opt_in_date::date) = 6 then dp.opt_in_date::date::date - 1 else dp.opt_in_date::date::date end) - date_trunc('week', case when extract(isodow from date_trunc('month', dp.opt_in_date::date)) = 7 then date_trunc('month', dp.opt_in_date::date)::date + 1 when extract(isodow from date_trunc('month', dp.opt_in_date::date)) = 6 then date_trunc('month', dp.opt_in_date::date)::date - 1 else date_trunc('month', dp.opt_in_date::date)::date end)) / 7 + 1)::int as lead_opt_in_wom,
	extract(month from dp.opt_in_date)::int as lead_opt_in_month,
	extract(year from dp.opt_in_date)::int as lead_opt_in_year,
	dp.amount_requested / greatest(dp.month_revenue,1) / 12 as lead_admin_incremental_leverage,
	(dp.amount_requested + divida_atual_pj) / greatest(dp.month_revenue,1) / 12 as lead_admin_total_leverage,
	o.max_value / greatest(dp.amount_requested,1) as lead_offer_over_application_value,
	lr.valor_solicitado / greatest(o.max_value,1)::float as lead_requested_over_offer_value,
	lr.value / greatest(lr.valor_solicitado,1)::float as lead_loan_over_requested_value,
	lr.prazo_solicitado / greatest(o.max_number_of_installments,1)::double precision as lead_requested_over_offer_term,
	lr.number_of_installments / greatest(lr.prazo_solicitado,1)::float as lead_loan_over_requested_term,
	case when lr.value /greatest(o.max_value,1)::float = lr.number_of_installments / greatest(o.max_number_of_installments,1)::float then 1 else (@li.pmt) / ((o.interest_rate / 100 + 0.009) / (1 - ( 1 + o.interest_rate / 100 + 0.009) ^ (- o.max_number_of_installments)) * o.max_value) end as lead_loan_over_offer_pmt,
	case when lr.value / greatest(lr.valor_solicitado,1)::float = lr.number_of_installments / greatest(lr.prazo_solicitado,1)::float then 1 else (@li.pmt) / ((o.interest_rate / 100 + 0.009) / (1 - ( 1 + o.interest_rate / 100 + 0.009) ^ (- lr.prazo_solicitado)) * lr.valor_solicitado) end as lead_loan_over_requested_pmt,
	case when lr.loan_date < dp.opt_in_date then 0 else extract(day from lr.loan_date - dp.opt_in_date) * 24 + extract(hour from lr.loan_date - dp.opt_in_date) + extract(minute from lr.loan_date - dp.opt_in_date) / 60 end as lead_full_track_time,
	replace(replace(replace(replace(replace(replace(replace(replace(replace(dp.reason_for_loan,'Capital de Giro','Working Capital'),'Expansão','Expansion'),'Compra de Estoque','Inventory'),'Outros','Others'),'Consolidação de Dívidas','Debt Consolidation'),'Marketing e Vendas','Sales and Marketing'),'Uso Pessoal','Personal Use'),'Reforma','Refurbishment'),'Compra de Equipamentos','Machinery') as lead_reason_for_loan,
	replace(replace(replace(replace(replace(replace(replace(replace(replace(dp.vinculo,'Administrador','Administrator'),'Outros','Other'),'Procurador','Attorney'),'Sócio','Shareholder'),'Administradora','Administrator'),'Sócia','Shareholder'),'Diretor','Director'),'Gerente Financeiro','CFO'),'Presidente','CEO') as lead_requester_relationship,
	dp.month_revenue as lead_informed_month_revenue,
	dp.amount_requested as lead_application_amount,
	lucas_leal.treat_source_name(lucas_leal.translate_char_encode_uft8(dp.utm_source)) as lead_marketing_channel,
	coalesce(case upper(lucas_leal.treat_source_name(lucas_leal.translate_char_encode_uft8(dp.utm_source))) 
		when 'ADWORDS' then 'adwords'
		when 'AGENT' then 'agent'
		when 'BIDU' then 'bidu'
		when 'BING' then 'Bing'
		when 'CREDITA' then 'credita'
		when 'FACEBOOK' then 'facebook'
		when 'FACEBOOK_ORG' then 'facebook'
		when 'FINANZERO' then 'finanzero'
		when 'FINPASS' then 'finpass'
		when 'GERU' then 'geru'
		when 'INSTAGRAM' then 'instagram'
		when 'JUROSBAIXOS' then 'jurosbaixos'
		when 'KONKERO' then 'konkero'
		when 'LINKEDIN' then 'linkedin'
		when 'MGM' then 'member-get-member'
		when 'ORGANICO' then 'organico'
		when 'BLOG' then 'organico'
		when 'SITE' then 'organico'
		when 'DIRECT_CHANNEL' then 'organico'
		when 'ANDROID' then 'organico'
		when 'RENEWAL' then 'organico'
		when 'EMAIL' then 'organico'
		when 'RD' then 'organico'
		when 'RD#/' then 'organico'
		when 'RDSTATION' then 'organico'
		when 'RD+STATION' then 'organico'
		when 'AMERICANAS' then 'other' 
		when 'IFOOD' then 'other' 
		when 'PEIXE-URBANO' then 'other' 
		when 'OUTBRAIN' then 'outbrain'
		when 'TABOOLA' then 'taboola'
		when 'CHARLES/' then 'agent'
		when 'MARCELOROMERA' then 'agent'
		when 'PBCONSULTORIA' then 'agent'
		when 'HMSEGUROS' then 'agent'
		when 'COMPARAONLINE' then 'agent'
		when 'CREDEXPRESS#/' then 'agent'
		else case when pp.identificacao is not null then 'agent' else dp.utm_source end
	end,'other') as lead_marketing_channel_group,
	lucas_leal.translate_char_encode_uft8(utm_medium)  as lead_marketing_medium,
	lucas_leal.treat_campaign_name(lucas_leal.translate_char_encode_uft8(dp.utm_source)) as lead_marketing_campaign,
	dp.client_id as lead_client_id,
	dp.opt_in_ip as lead_opt_in_ip,
	case when dp.n_company_applied_before is null then 0 else dp.n_company_applied_before end as lead_times_company_applied_before,
	case when dp.n_individual_applied_before is null then 0 when lucas_leal.treat_source_name(lucas_leal.translate_char_encode_uft8(dp.utm_source)) in ('fdex','capitalemp','finpass','IDEA','IMPERIO','ACESSE','andreozzi','ARAMAYO','AZUL','BONUS','CHARLES','CREDRAPIDO','CREDEXPRESS','DIGNESS','DUOCAPITAL','ISF','JEITONOVO','MONETAE','MRS','PLANOCAPITAL','R2A','VIETTO','vipac','VIRGINIA','ALMIRGUEDES','CREDPRIME','HMSEGUROS','CFC','REALCRED','ESPACOLIMA','FRIGO') then null else dp.n_individual_applied_before end as lead_times_individual_applied_before,
	dp.facebook::int lead_has_facebook_page,
	dp.likes lead_number_likes_facebook,
	dp.streetview::int lead_has_google_streetview,
	dp.site::int lead_has_website,
	case when upper(left(right(dp.email, length(dp.email)-position('@' in dp.email)),position('.' in right(dp.email, length(dp.email)-position('@' in dp.email)-1)))) in ('GMAIL','HOTMAIL','YAHOO','OUTLOOK','UOL','TERRA','BOL','LIVE','ICLOUD','IG','MSN','GLOBO') then replace(upper(left(right(dp.email, length(dp.email)-position('@' in dp.email)),position('.' in right(dp.email, length(dp.email)-position('@' in dp.email)-1)))),'GMAIIL','GMAIL') else 'OTHER' end as lead_email_server,
	case when (dp.month_revenue >= 100000 and age < 2) or (dp.month_revenue >= 200000 and age < 3) or (dp.month_revenue >= 300000 and age < 4) then 1 else 0 end as lead_Age_Revenue_Discrepancy,
	case when dp.phone = '' then null else substring(dp.phone,2,2) end as lead_phone_area_code,
--NEOWAY
	case when dp.revenue not in ('','NA') then dp.revenue end::numeric as neoway_estimated_annual_revenue,
	to_date(dp.date_created,'yyyy-mm-dd') as neoway_date_company_creation,
	dp.age as neoway_company_age,
	dp.employees as neoway_estimate_number_of_employees,
	dp.mei::int as neoway_is_mei,
	dp.is_simples::int as neoway_simple_tribute_method_applied,
	dp.cnae_code as neoway_economic_activity_national_registration_code,
	dp.cnae as neoway_economic_activity_national_registration_name,
	dp.social_capital::float as neoway_social_capital,
	dp.number_partners as neoway_estimated_number_of_shareholders,
	dp.number_coligadas as neoway_estimated_number_of_related_companies,
	dp.number_branches as neoway_estimated_number_of_branches,
	coalesce(case when dp.zip_code = '' then null else dp.zip_code end, signer_zipcode) as neoway_company_zip_code,
	lucas_leal.get_postal_code_lucas(dp.state::text,coalesce(case when dp.zip_code = '' then null else dp.zip_code end, signer_zipcode)::text) as neoway_company_zip_code_class,
	dp.neighbourhood as neoway_company_neighbourhood,
	dp.city as neoway_company_city,
	dp.state as neoway_company_state,
	lucas_leal.is_city_capital(dp.city,dp.state) as neoway_is_city_capital,
	lucas_leal.state_region(dp.state) as neoway_state_region,
	dp.tax_health as neoway_tax_health,
	dp.level_activity as neoway_level_activity,
	dp.activity_sector as neoway_activity_sector,
	dp.sector as neoway_sector,
	dp.pgfn_debt as neoway_government_debt,
--CREDNET
	dp.number_protests as crednet_protests_unit,
	dp.protests_amount crednet_protests_value,
	dp.protests_amount / greatest(dp.month_revenue,1) / 12 crednet_protests_value_over_revenue,
	dp.number_spc as crednet_company_pefin_unit,
	dp.spc_amount as crednet_company_pefin_value,
	dp.spc_amount / greatest(dp.month_revenue,1) / 12 as crednet_company_pefin_value_over_revenue,
	dp.number_acoes as crednet_company_legal_action_unit,
	dp.acoes_amount as crednet_company_legal_action_value,
	dp.acoes_amount / greatest(dp.month_revenue,1) / 12 crednet_legal_action_value_over_revenue,
	dp.number_refins as crednet_company_refin_unit,
	dp.refins_amount as crednet_company_refin_value,
	dp.refins_amount / greatest(dp.month_revenue,1) / 12 crednet_company_refin_value_over_revenue,
	dp.number_protests + dp.number_spc + dp.number_acoes + dp.number_refins as crednet_total_npr_units,
	dp.protests_amount + dp.spc_amount + dp.acoes_amount + dp.refins_amount as crednet_total_npr_value,
	(dp.protests_amount + dp.spc_amount + dp.acoes_amount + dp.refins_amount) / greatest(dp.month_revenue,1) / 12 as crednet_total_npr_value_over_revenue,
--SCORES
	lead_score::float as lead_biz_lead_score,
	dp.bizu_score::float as lead_biz_bizu1_score,
	case when dp.bizu_score >= 850 then 'A+' when dp.bizu_score >= 700 then 'A-' when dp.bizu_score >= 650 then 'B+' when dp.bizu_score >= 600 then 'B-' when dp.bizu_score >= 500 then 'C+'when dp.bizu_score >= 400 then 'C-' when dp.bizu_score >= 300 then 'D' when dp.bizu_score < 300 then 'E' end as lead_rating_class_bizu1,
	case when dp.opt_in_date < '2019-10-16' then coalesce(serasa_coleta,o.rating,dp.serasa_4) else dp.serasa_4 end as lead_experian_score4,
	lucas_leal.serasa_classe(case when dp.opt_in_date < '2019-10-16' then coalesce(serasa_coleta,o.rating,dp.serasa_4) else dp.serasa_4 end::int) as lead_class_experian_score4,
	case when dp.opt_in_date < '2019-10-16' then pd_serasa end as lead_experian_score4_associated_pd,
	case when dp.opt_in_date < '2019-10-16' then coalesce(bktest.s6,bktest2.score6,dp.serasa_6) else coalesce(serasa_coleta,dp.serasa_6) end as lead_experian_score6,
	lucas_leal.serasa_classe(case when dp.opt_in_date < '2019-10-16' then coalesce(bktest.s6,bktest2.score6,dp.serasa_6) else coalesce(serasa_coleta,dp.serasa_6) end::int) as lead_class_experian_score6,
	dp.proposal_score as lead_proposal_score,
	dp.proposal_amount as lead_proposal_limit,
	dp.proposal_interest as lead_proposal_interest_rate,
	dp.proposal_term as lead_proposal_term,
	dp.low_ticket_score as lead_low_ticket_score,
	dp.bizu2_score::float as lead_biz_bizu2_score,
	dp.pd_bizu2::float as lead_biz_bizu2_associated_pd,
	case when dp.bizu2_score >= 1100 then 'A+' when dp.bizu2_score >= 850 then 'A-' when dp.bizu2_score >= 720 then 'B+' when dp.bizu2_score >= 600 then 'B-' when dp.bizu2_score >= 500 then 'C+' when dp.bizu2_score >= 400 then 'C-' when dp.bizu2_score >= 300 then 'D' when dp.bizu2_score >= 200 then 'E' when dp.bizu2_score < 200 then 'F' end as lead_classe_Bizu2,
	dp.bizu21_score::float as lead_biz_bizu21_score,
	dp.pd_bizu21::float as lead_biz_bizu21_associated_pd,
	dp.bizu21_class as lead_classe_Bizu21,
	case when dp.opt_in_date::date < '2019-03-13' then dp.bizu_score when dp.opt_in_date::date < '2019-10-15' then dp.bizu2_score when dp.opt_in_date::date = '2019-10-15' then coalesce(dp.bizu21_score,dp.bizu2_score) else dp.bizu21_score end as lead_biz_bizux_score,
	case when dp.opt_in_date::date < '2019-03-13' then 
			(case when dp.bizu_score >= 850 then 'A+' when dp.bizu_score >= 700 then 'A-' when dp.bizu_score >= 650 then 'B+' when dp.bizu_score >= 600 then 'B-' when dp.bizu_score >= 500 then 'C+' when dp.bizu_score >= 400 then 'C-' when dp.bizu_score >= 300 then 'D' when dp.bizu_score < 300 then 'E' end)
		when dp.opt_in_date::date < '2019-10-15' then 
			(case when dp.bizu2_score >= 1100 then 'A+' when dp.bizu2_score >= 850 then 'A-' when dp.bizu2_score >= 720 then 'B+' when dp.bizu2_score >= 600 then 'B-' when dp.bizu2_score >= 500 then 'C+' when dp.bizu2_score >= 400 then 'C-' when dp.bizu2_score >= 300 then 'D' when dp.bizu2_score >= 200 then 'E' when dp.bizu2_score < 200 then 'F' end)
		when dp.opt_in_date::date = '2019-10-15' then 
			coalesce(case when dp.bizu21_score >= 1200 then 'A+' when dp.bizu21_score >= 950 then 'A-' when dp.bizu21_score >= 750 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end,
				case when dp.bizu2_score >= 1100 then 'A+' when dp.bizu2_score >= 850 then 'A-' when dp.bizu2_score >= 720 then 'B+' when dp.bizu2_score >= 600 then 'B-' when dp.bizu2_score >= 500 then 'C+' when dp.bizu2_score >= 400 then 'C-' when dp.bizu2_score >= 300 then 'D' when dp.bizu2_score >= 200 then 'E' when dp.bizu2_score < 200 then 'F' end)
		when dp.direct_prospect_id < 503019 then
			(case when dp.bizu21_score >= 1200 then 'A+' when dp.bizu21_score >= 950 then 'A-' when dp.bizu21_score >= 750 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end)
		else
			(case when dp.bizu21_score >= 1100 then 'A+' when dp.bizu21_score >= 850 then 'A-' when dp.bizu21_score >= 720 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end)
	end	as lead_classe_Bizux,
	vrt.behaviour_score_rn_menos1_new as lead_behaviour_rn_menos1_score,
	case when vrt.behaviour_score_rn_menos1_new <= .08 then 4 when vrt.behaviour_score_rn_menos1_new <= .3 then 3 when vrt.behaviour_score_rn_menos1_new <= .7 then 2 when vrt.behaviour_score_rn_menos1_new > .7 then 1 end as lead_behaviour_rn_menos1_class,
	vrt.behaviour_zone_full_new as lead_behaviour_rn_menos1_zone,
	vrt.renewal_zone_full_new as lead_rn_renewal_zone,
	lucas_leal.get_renewal_score_negativacao(((dp.protests_amount + dp.spc_amount + dp.acoes_amount + dp.refins_amount) / greatest(dp.month_revenue,1) / 12)::numeric, 
		dp.number_protests + dp.number_spc + dp.number_acoes + dp.number_refins,
		case when Months_Since_Last_Overdue_PJ is null or coalesce(Ever_long_term_debt_PJ,Ever_CarteiraCredito_PJ) = 0 then - 1 else Months_Since_Last_Overdue_PJ end::int,
		(case when coalesce(Months_Since_Last_Default_PJ,Meses_Desde_Ultimo_Prejuizo_PJ) is null or coalesce(Ever_long_term_debt_PJ,Ever_CarteiraCredito_PJ) = 0 then - 1 else Months_Since_Last_Default_PJ end)::int,
		(case when Months_Since_Last_Overdue_pf is null or coalesce(Ever_long_term_debt_pf,Ever_CarteiraCredito_pf) = 0 then - 1 else Months_Since_Last_Overdue_pf end)::int,
		(case when coalesce(Months_Since_Last_Default_pf,Meses_Desde_Ultimo_Prejuizo_pf) is null or coalesce(Ever_long_term_debt_pf,Ever_CarteiraCredito_pf) = 0 then - 1 else Months_Since_Last_Default_pf end)::int,
		coalesce(serasa_coleta,dp.serasa_4)::int) as lead_renewal_rn_score_negativacao,
	case when vrt.behaviour_score_rn_menos1_new <= .1 then 1 when vrt.behaviour_score_rn_menos1_new <= .4 then 2 when vrt.behaviour_score_rn_menos1_new > .4 then 3 end as lead_behaviour_rn_menos1_ranking,
	case when vrt.behaviour_score_rn_menos1_new <= .1 then 1 when vrt.behaviour_score_rn_menos1_new <= .4 then 2 when vrt.behaviour_score_rn_menos1_new > .4 then 3 end || '-' || 
		case 
			when lucas_leal.get_renewal_score_negativacao(((dp.protests_amount + dp.spc_amount + dp.acoes_amount + dp.refins_amount) / greatest(dp.month_revenue,1) / 12)::numeric, 
				dp.number_protests + dp.number_spc + dp.number_acoes + dp.number_refins,
				case when Months_Since_Last_Overdue_PJ is null or coalesce(Ever_long_term_debt_PJ,Ever_CarteiraCredito_PJ) = 0 then - 1 else Months_Since_Last_Overdue_PJ end::int,
				(case when coalesce(Months_Since_Last_Default_PJ,Meses_Desde_Ultimo_Prejuizo_PJ) is null or coalesce(Ever_long_term_debt_PJ,Ever_CarteiraCredito_PJ) = 0 then - 1 else Months_Since_Last_Default_PJ end)::int,
				(case when Months_Since_Last_Overdue_pf is null or coalesce(Ever_long_term_debt_pf,Ever_CarteiraCredito_pf) = 0 then - 1 else Months_Since_Last_Overdue_pf end)::int,
				(case when coalesce(Months_Since_Last_Default_pf,Meses_Desde_Ultimo_Prejuizo_pf) is null or coalesce(Ever_long_term_debt_pf,Ever_CarteiraCredito_pf) = 0 then - 1 else Months_Since_Last_Default_pf end)::int,
				coalesce(serasa_coleta,dp.serasa_4)::int) <= 2 then 'limpo'
			when lucas_leal.get_renewal_score_negativacao(((dp.protests_amount + dp.spc_amount + dp.acoes_amount + dp.refins_amount) / greatest(dp.month_revenue,1) / 12)::numeric, 
				dp.number_protests + dp.number_spc + dp.number_acoes + dp.number_refins,
				case when Months_Since_Last_Overdue_PJ is null or coalesce(Ever_long_term_debt_PJ,Ever_CarteiraCredito_PJ) = 0 then - 1 else Months_Since_Last_Overdue_PJ end::int,
				(case when coalesce(Months_Since_Last_Default_PJ,Meses_Desde_Ultimo_Prejuizo_PJ) is null or coalesce(Ever_long_term_debt_PJ,Ever_CarteiraCredito_PJ) = 0 then - 1 else Months_Since_Last_Default_PJ end)::int,
				(case when Months_Since_Last_Overdue_pf is null or coalesce(Ever_long_term_debt_pf,Ever_CarteiraCredito_pf) = 0 then - 1 else Months_Since_Last_Overdue_pf end)::int,
				(case when coalesce(Months_Since_Last_Default_pf,Meses_Desde_Ultimo_Prejuizo_pf) is null or coalesce(Ever_long_term_debt_pf,Ever_CarteiraCredito_pf) = 0 then - 1 else Months_Since_Last_Default_pf end)::int,
				coalesce(serasa_coleta,dp.serasa_4)::int) > 2 then 'sujo' || case when case when dp.opt_in_date::date < '2019-10-15' then dp.bizu2_score when dp.opt_in_date::date = '2019-10-15' then coalesce(dp.bizu21_score,dp.bizu2_score) else dp.bizu21_score end < 200 and vrt.behaviour_score_rn_menos1_new <= .4 then ' + F' end
		end as lead_behaviour_rn_ranking_politica,
	case when (coalesce(vrt.total_pago_ate_lead,0) / vrt.collection_original_p_plus_i < .8 and dp.opt_in_date::date < coalesce(vrt.data_quitacao,'2999-01-01'::date)) or
		coalesce(dp.workflow_detailed,'NULL') = 'Elegibilidade' or
		coalesce(position('Eligibilidade' in dp.observacoes),0) > 0 or
		coalesce(position('Negado Renovação Percentual' in dp.observacoes),0) > 0 or
		coalesce(position('Negado Renovacao Percentual' in dp.observacoes),0) > 0 then 0 else 1 end as lead_renewal_rn_menos1_elegivel,
--GEOFUSION
	microrregiao as geofusion_city_microregion,
	mesorregiao as geofusion_city_mesoregion,
	regiao_geografica geofusion_city_region,
	ric_imediata as geofusion_city_nearest_center,
	ric_intermediaria as geofusion_city_nearest_big_center,
	ric_ampliada as geofusion_city_nearest_capital,
	hierarquia as geofusion_city_hierarchy,
	"area" as geofusion_city_area_size,
	populacao_2017 as geofusion_city_population_2017,
	densidade_pop_2017 as geofusion_city_density_2017,
	domicilios_2017 as geofusion_city_number_of_houses_2017,
	pib_2015 as geofusion_city_gdp_2015_2015,
	pib_per_capta_2015 as geofusion_city_GDP_per_capta_2015,
	idh_2010 as geofusion_city_hdi_2010,
	idh_faixa_2010 as geofusion_city_hdi_2010_class,
	renda_media_2017 as geofusion_city_avg_revenue_2017,
	renda_nominal_2017 as geofusion_city_nominal_revenue_2017,
	pea_2016 as geofusion_city_economicaly_active_population_2016,
	faixa_pea_2016 as geofusion_city_economicaly_active_population_2016_class,
	populacao_ativa_2016 as geofusion_city_active_population_2016,
	idh_educacao_2010 as geofusion_city_hdi_education_2010,
	idh_educacao_faixa_2010 as geofusion_city_hdi_education_2010_class,
	idh_longevidade_2010 as geofusion_city_hdi_longevity_2010,
	idh_longevidade_faixa_2010 as geofusion_city_hdi_longevity_2010_class,
	idh_renda_2010 as geofusion_city_hdi_revenue_2010,
	idh_renda_faixa_2010 as geofusion_city_hdi_revenue_2010_class,
	segmento_municipal as geofusion_city_segmentation,
--BACEN
	total_agencias_uf as bacen_total_bank_branches_in_state,
	total_agencias_municipio as bacen_total_bank_branches_in_city,
	total_agencias_bairro as bacen_total_bank_branches_in_neighbourhood,
	total_agencias_postal as bacen_total_bank_branches_in_zip_code_class,
--------------------------INITIAL OFFER
	initialoff.offer_id as initial_offer_id,
	initialoff.status as initial_offer_status,
	initialoff.date_inserted as initial_offer_date,
	initialoff.interest_rate as initial_offer_interest_rate,
	initialoff.max_number_of_installments as initial_offer_max_term,
	initialoff.max_value as initial_offer_max_value,
	vfcsd.count_ofertas - 1 as count_negotiations,
	initialoff.data_ultimo_contato as initial_offer_last_contact_date,
	replace(initialoff.follow_up,'Carlos Saraiva','Cadu Saraiva') as initial_offer_analista_is,
--------------------------FINAL OFFER
	o.offer_id as final_offer_id,
	o.status as final_offer_status,
	o.date_inserted as final_offer_date,
	o.interest_rate as final_offer_interest_rate,
	o.max_number_of_installments as final_offer_max_term,
	o.max_value as final_offer_max_value,
	o.data_ultimo_contato as final_offer_last_contact_date,
	replace(o.follow_up,'Carlos Saraiva','Cadu Saraiva') as final_offer_analista_is,
--------------------------LOAN
	lr.loan_request_id as loan_request_id,
	lr.status as loan_request_status,
	lr.date_inserted as loan_request_date,
	lr.valor_solicitado as loan_requested_value,
	lr.prazo_solicitado as loan_requested_term,
	dp.previous_loans_count as loan_is_renewal,
	lr.value as loan_net_value,
	li.total_payment loan_gross_value,
	lr.taxa_final as loan_final_net_interest_rate,
	li.monthly_cet loan_final_gross_interest_rate,
	lr.number_of_installments as loan_original_final_term,
	(@li.pmt) as loan_original_final_pmt,
	(@li.pmt) / greatest(dp.month_revenue,1) as loan_original_final_pmt_leverage,
	li.commission as loan_commission_fee,
	lr.loan_date as loan_contract_date,
	to_date(to_char(lr.loan_date,'Mon/yy'),'Mon/yy') as loan_cohort_loan,
	extract (year from age(current_date,lr.loan_date)) * 12 + extract(month from age(current_date,lr.loan_date)) as loan_months_on_book,
	lr.portfolio_id::text loan_portfolio_id,
	ba.bank_id::text loan_bank_id,
	coalesce(lr.payment_day,original_1st_pmt_due_to) as loan_1st_pmt_due_date,
	extract(day from coalesce(lr.payment_day,original_1st_pmt_due_to)) as loan_1st_pmt_chosen_day_of_month,
	(extract('day' from date_trunc('week', case when extract(isodow from coalesce(lr.payment_day,original_1st_pmt_due_to)::date) = 7 then coalesce(lr.payment_day,original_1st_pmt_due_to)::date + 1 when extract(isodow from coalesce(lr.payment_day,original_1st_pmt_due_to)::date) = 6 then coalesce(lr.payment_day,original_1st_pmt_due_to)::date - 1 else coalesce(lr.payment_day,original_1st_pmt_due_to)::date end) - date_trunc('week', case when extract(isodow from date_trunc('month', coalesce(lr.payment_day,original_1st_pmt_due_to))::date) = 7 then date_trunc('month', coalesce(lr.payment_day,original_1st_pmt_due_to)::date)::date + 1 when extract(isodow from date_trunc('month', coalesce(lr.payment_day,original_1st_pmt_due_to))::date) = 6 then date_trunc('month', coalesce(lr.payment_day,original_1st_pmt_due_to)::date)::date - 1 else date_trunc('month', coalesce(lr.payment_day,original_1st_pmt_due_to)::date)::date end)) / 7 + 1)::int as loan_1st_pmt_chosen_wom,
	case when extract(day from coalesce(lr.payment_day,original_1st_pmt_due_to)) >= 20 then 'End' when extract(day from coalesce(lr.payment_day,original_1st_pmt_due_to)) >= 10 then 'Middle' else 'Beginning' end as loan_1st_pmt_chosen_wom_class,
	case when lr.payment_day is null then original_1st_pmt_due_to else lr.payment_day end - lr.loan_date as loan_grace_period,
	lr.data_ultimo_contato as loan_last_contact_date,
	lr.analista_responsavel as loan_analyst_responsible,
	(cp.bizbot > 0 and cp.nao_bizbot = 0)::int as loan_approved_by_bizbot,
	case when lr.all_docs_date is not null or cp.emprestimo_id is not null or lr.status in ('ACCEPTED','Cancelado','REJECTED') or ad.loan_request_id is not null then 1 else 0 end as loan_request_is_all_docs,
	case when lr.status = 'ACCEPTED' and lr.contrato_assinado_url is not null then 1 else 0 end as loan_is_loan,
--------------------------COLLECTION-PLANOS
	fully_paid as collection_loan_fully_paid,
	count_renegotiation as collection_count_restructures,
	count_unbroken_renegotiations as collection_count_unbroken_restructures,
	count_broken_renegotiation as collection_count_broken_restructures,
	primeira_parcela_renegociada as collection_pmt_number_of_1st_restructure,
	data_primeira_renegociacao as collection_date_1st_restructure,
	data_ultima_renegociacao as collection_date_last_restructure,
	count_postponing_plans as collection_count_postponing_restructures,
	parcelas.i_pmt_of_first_postponing as collection_i_pmt_of_first_postponing,
--Promessas pagamento
	coalesce(total_promessas,0) as collection_payment_promises_total,
	coalesce(total_promessas_cumpridas,0) as collection_payment_promises_fulfilled,
	coalesce(total_promessas_quebradas,0) as collection_payment_promises_broken,
	coalesce(total_promessas_canceladas,0) as collection_payment_promises_cancelled,
	coalesce(total_promessas_ativas,0) as collection_payment_promises_active,
--Historico negativacao
	coalesce(total_negativacoes,0) as collection_total_npr,
	coalesce(total_negativacoes_com_erro,0) as collection_npr_w_error,
	coalesce(total_negativacoes_sem_erro,0) as collection_npr_wo_error,
	coalesce(total_negativacoes_empresa,0) as collection_total_company_npr,
	coalesce(total_negativacoes_avalistas,0) as collection_total_guarantor_npr,
	coalesce(total_negativacoes_empresa_sem_erro,0) as collection_company_wo_error_npr,
	coalesce(total_negativacoes_avalistas_sem_erro,0) as collection_guarantor_wo_error_npr,
	coalesce(total_negativacao_parcelas,0) as collection_total_pmt_npr,
	coalesce(total_negativacao_parcelas_sem_erro,0) as collection_pmt_wo_error_npr,
	coalesce(sem_contato,0) as collection_unable_to_contact_right_person,
--------------------------COLLECTION-PARCELAS
	parcelas.n_parcelas as collection_loan_current_term,
	original_total_due_installments as collection_original_total_due_installments,
	original_total_overdue_installments - greatest(original_total_due_installments - paid_inst, 0) as collection_original_total_overdue_installments,
	due_inst as collection_total_due_installments,
	to_come_inst as collection_to_be_due_installments,
	parcelas.n_parcelas_pagas as collection_number_of_installments_paid,
	parcelas.n_parcelas_em_atraso as collection_number_of_installments_curr_late,
	coalesce(total_paid_amount,0) as collection_loan_amount_paid,
	coalesce(total_late_amount,0) as collection_loan_amount_late,
	original_p_and_i as collection_original_p_plus_i,
	curr_p_and_i as collection_current_p_plus_i,
	curr_p_and_i / greatest(original_p_and_i,1) as collection_current_over_original_p_plus_i,
	coalesce(EAD,0) as collection_current_ead,
	original_p_and_i - coalesce(total_paid_amount,0) as collection_original_ead,
	pmt_atual as collection_current_pmt_value,
	pmt_avg as collection_current_pmt_avg,
	parc_em_aberto as collection_current_pmt_number,
	current_inst_due_to as collection_current_pmt_due_to,
	coalesce(total_amount_to_be_due,0) as collection_loan_amount_to_be_due,
	coalesce(total_amount_late_under_60,0) as collection_loan_amount_late_under_60,
	coalesce(total_amount_over_60,0) as collection_loan_amount_over60,
	coalesce(total_amount_over_180,0) as collection_loan_amount_over180,
	coalesce(total_amount_over_360,0) as collection_loan_amount_over360,
	dpd_soft.max_late_pmt1 as collection_current_max_late_pmt1,
	dpd_soft.max_late_pmt2 as collection_current_max_late_pmt2,
	dpd_soft.max_late_pmt3 as collection_current_max_late_pmt3,
	dpd_soft.max_late_pmt4 as collection_current_max_late_pmt4,
	dpd_soft.max_late_pmt5 as collection_current_max_late_pmt5,
	dpd_soft.max_late_pmt6 as collection_current_max_late_pmt6,
	dpd_soft.max_late_pmt7 as collection_current_max_late_pmt7,
	dpd_soft.max_late_pmt8 as collection_current_max_late_pmt8,
	dpd_soft.max_late_pmt9 as collection_current_max_late_pmt9,
	dpd_soft.max_late_pmt10 as collection_current_max_late_pmt10,
	dpd_soft.max_late_pmt11 as collection_current_max_late_pmt11,
	dpd_soft.max_late_pmt12 as collection_current_max_late_pmt12,
	dpd_soft.max_late_pmt13 as collection_current_max_late_pmt13,
	dpd_soft.max_late_pmt14 as collection_current_max_late_pmt14,
	dpd_soft.max_late_pmt15 as collection_current_max_late_pmt15,
	dpd_soft.max_late_pmt16 as collection_current_max_late_pmt16,
	dpd_soft.max_late_pmt17 as collection_current_max_late_pmt17,
	dpd_soft.max_late_pmt18 as collection_current_max_late_pmt18,
	dpd_soft.max_late_pmt19 as collection_current_max_late_pmt19,
	dpd_soft.max_late_pmt20 as collection_current_max_late_pmt20,
	dpd_soft.max_late_pmt21 as collection_current_max_late_pmt21,
	dpd_soft.max_late_pmt22 as collection_current_max_late_pmt22,
	dpd_soft.max_late_pmt23 as collection_current_max_late_pmt23,
	dpd_soft.max_late_pmt24 as collection_current_max_late_pmt24,
	case when paid_inst = 0 and case parcelas.i_pmt_of_first_postponing when 1 then dpd_soft.max_late_pmt2 else dpd_soft.max_late_pmt1 end > 0 then 1 else 0 end as collection_is_current_fpd,
	case when paid_inst <= 1 and case parcelas.i_pmt_of_first_postponing when 2 then dpd_soft.max_late_pmt3 else dpd_soft.max_late_pmt2 end > 0 then 1 else 0 end as collection_is_current_spd,
	case when paid_inst <= 2 and case parcelas.i_pmt_of_first_postponing when 3 then dpd_soft.max_late_pmt4 else dpd_soft.max_late_pmt3 end > 0 then 1 else 0 end as collection_is_current_tpd,
	case when paid_inst <= 3 and case parcelas.i_pmt_of_first_postponing when 4 then dpd_soft.max_late_pmt5 else dpd_soft.max_late_pmt4 end > 0 then 1 else 0 end as collection_is_current_4thpd,
	(case parcelas.i_pmt_of_first_postponing when 1 then dpd_soft.max_late_pmt2 else dpd_soft.max_late_pmt1 end > 30)::int as collection_fpd_30,
	original_p_and_i - first_paid_inst_amount as collection_ead_for_spd,
	original_p_and_i - first_paid_inst_amount - second_paid_inst_amount as collection_ead_for_tpd,
	original_p_and_i - first_paid_inst_amount - second_paid_inst_amount - third_paid_inst_amount as collection_ead_for_4thpd,
--Current
	parcelas.curr_late as collection_loan_curr_late_by,
	(parcelas.curr_late > 30)::int as collection_loan_curr_late_over_30,
	(parcelas.curr_late > 60)::int as collection_loan_curr_late_over_60,
	(parcelas.curr_late > 90)::int as collection_loan_curr_late_over_90,
	parcelas.max_late as collection_loan_max_late_by,
	lucas_leal.get_collection_roll_band(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24,dpd_soft.max_late_pmt25,dpd_soft.max_late_pmt26,dpd_soft.max_late_pmt27,dpd_soft.max_late_pmt28,dpd_soft.max_late_pmt29,dpd_soft.max_late_pmt30,dpd_soft.max_late_pmt31,dpd_soft.max_late_pmt32,dpd_soft.max_late_pmt33,dpd_soft.max_late_pmt34,dpd_soft.max_late_pmt35,dpd_soft.max_late_pmt36,dpd_soft.max_late_pmt37,dpd_soft.max_late_pmt38,dpd_soft.max_late_pmt39,dpd_soft.max_late_pmt40,dpd_soft.max_late_pmt41,dpd_soft.max_late_pmt42,dpd_soft.max_late_pmt43,dpd_soft.max_late_pmt44,dpd_soft.max_late_pmt45,dpd_soft.max_late_pmt46,dpd_soft.max_late_pmt47,dpd_soft.max_late_pmt48) as collection_current_roll_band,
	coalesce(dpd_soft.max_late_pmt1,0) + coalesce(dpd_soft.max_late_pmt2,0) + coalesce(dpd_soft.max_late_pmt3,0) + coalesce(dpd_soft.max_late_pmt4,0) + coalesce(dpd_soft.max_late_pmt5,0) + coalesce(dpd_soft.max_late_pmt6,0) + coalesce(dpd_soft.max_late_pmt7,0) + coalesce(dpd_soft.max_late_pmt8,0) + coalesce(dpd_soft.max_late_pmt9,0) + coalesce(dpd_soft.max_late_pmt10,0) + coalesce(dpd_soft.max_late_pmt11,0) + coalesce(dpd_soft.max_late_pmt12,0) + coalesce(dpd_soft.max_late_pmt13,0) + coalesce(dpd_soft.max_late_pmt14,0) + coalesce(dpd_soft.max_late_pmt15,0) + coalesce(dpd_soft.max_late_pmt16,0) + coalesce(dpd_soft.max_late_pmt17,0) + coalesce(dpd_soft.max_late_pmt18,0) + coalesce(dpd_soft.max_late_pmt19,0) + coalesce(dpd_soft.max_late_pmt20,0) + coalesce(dpd_soft.max_late_pmt21,0) + coalesce(dpd_soft.max_late_pmt22,0) + coalesce(dpd_soft.max_late_pmt23,0) + coalesce(dpd_soft.max_late_pmt24,0) + coalesce(dpd_soft.max_late_pmt25,0) + coalesce(dpd_soft.max_late_pmt26,0) + coalesce(dpd_soft.max_late_pmt27,0) + coalesce(dpd_soft.max_late_pmt28,0) + coalesce(dpd_soft.max_late_pmt29,0) + coalesce(dpd_soft.max_late_pmt30,0) + coalesce(dpd_soft.max_late_pmt31,0) + coalesce(dpd_soft.max_late_pmt32,0) + coalesce(dpd_soft.max_late_pmt33,0) + coalesce(dpd_soft.max_late_pmt34,0) + coalesce(dpd_soft.max_late_pmt35,0) + coalesce(dpd_soft.max_late_pmt36,0) + coalesce(dpd_soft.max_late_pmt37,0) + coalesce(dpd_soft.max_late_pmt38,0) + coalesce(dpd_soft.max_late_pmt39,0) + coalesce(dpd_soft.max_late_pmt40,0) + coalesce(dpd_soft.max_late_pmt41,0) + coalesce(dpd_soft.max_late_pmt42,0) + coalesce(dpd_soft.max_late_pmt43,0) + coalesce(dpd_soft.max_late_pmt44,0) + coalesce(dpd_soft.max_late_pmt45,0) + coalesce(dpd_soft.max_late_pmt46,0) + coalesce(dpd_soft.max_late_pmt47,0) + coalesce(dpd_soft.max_late_pmt48,0) as collection_sum_payment_history_curve,	
	lucas_leal.get_collection_pmt_of_max_late(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24,dpd_soft.max_late_pmt25,dpd_soft.max_late_pmt26,dpd_soft.max_late_pmt27,dpd_soft.max_late_pmt28,dpd_soft.max_late_pmt29,dpd_soft.max_late_pmt30,dpd_soft.max_late_pmt31,dpd_soft.max_late_pmt32,dpd_soft.max_late_pmt33,dpd_soft.max_late_pmt34,dpd_soft.max_late_pmt35,dpd_soft.max_late_pmt36,dpd_soft.max_late_pmt37,dpd_soft.max_late_pmt38,dpd_soft.max_late_pmt39,dpd_soft.max_late_pmt40,dpd_soft.max_late_pmt41,dpd_soft.max_late_pmt42,dpd_soft.max_late_pmt43,dpd_soft.max_late_pmt44,dpd_soft.max_late_pmt45,dpd_soft.max_late_pmt46,dpd_soft.max_late_pmt47,dpd_soft.max_late_pmt48) as collection_pmt_of_max_late,
	(parcelas.max_late >= 30)::int as collection_loan_ever_30,
	(parcelas.max_late >=  60)::int as collection_loan_ever_60,
	(parcelas.max_late >=  90)::int as collection_loan_ever_90,
	case when dpd_soft.max_late_pmt1 >= 1 then 1 when dpd_soft.max_late_pmt2 >= 1 then 2 when dpd_soft.max_late_pmt3 >= 1 then 3 when dpd_soft.max_late_pmt4 >= 1 then 4 when dpd_soft.max_late_pmt5 >= 1 then 5 when dpd_soft.max_late_pmt6 >= 1 then 6 when dpd_soft.max_late_pmt7 >= 1 then 7 when dpd_soft.max_late_pmt8 >= 1 then 8 when dpd_soft.max_late_pmt9 >= 1 then 9 when dpd_soft.max_late_pmt10 >= 1 then 10 when dpd_soft.max_late_pmt11 >= 1 then 11 when dpd_soft.max_late_pmt12 >= 1 then 12 when dpd_soft.max_late_pmt13 >= 1 then 13 when dpd_soft.max_late_pmt14 >= 1 then 14 when dpd_soft.max_late_pmt15 >= 1 then 15 when dpd_soft.max_late_pmt16 >= 1 then 16 when dpd_soft.max_late_pmt17 >= 1 then 17 when dpd_soft.max_late_pmt18 >= 1 then 18 when dpd_soft.max_late_pmt19 >= 1 then 19 when dpd_soft.max_late_pmt20 >= 1 then 20 when dpd_soft.max_late_pmt21 >= 1 then 21 when dpd_soft.max_late_pmt22 >= 1 then 22 when dpd_soft.max_late_pmt23 >= 1 then 23 when dpd_soft.max_late_pmt24 >= 1 then 24 when dpd_soft.max_late_pmt25 >= 1 then 25  when dpd_soft.max_late_pmt26 >= 1 then 26  when dpd_soft.max_late_pmt27 >= 1 then 27  when dpd_soft.max_late_pmt28 >= 1 then 28  when dpd_soft.max_late_pmt29 >= 1 then 29  when dpd_soft.max_late_pmt30 >= 1 then 30  when dpd_soft.max_late_pmt31 >= 1 then 31  when dpd_soft.max_late_pmt32 >= 1 then 32  when dpd_soft.max_late_pmt33 >= 1 then 33  when dpd_soft.max_late_pmt34 >= 1 then 34  when dpd_soft.max_late_pmt35 >= 1 then 35  when dpd_soft.max_late_pmt36 >= 1 then 36  when dpd_soft.max_late_pmt37 >= 1 then 37  when dpd_soft.max_late_pmt38 >= 1 then 38  when dpd_soft.max_late_pmt39 >= 1 then 39  when dpd_soft.max_late_pmt40 >= 1 then 40  when dpd_soft.max_late_pmt41 >= 1 then 41  when dpd_soft.max_late_pmt42 >= 1 then 42  when dpd_soft.max_late_pmt43 >= 1 then 43  when dpd_soft.max_late_pmt44 >= 1 then 44  when dpd_soft.max_late_pmt45 >= 1 then 45  when dpd_soft.max_late_pmt46 >= 1 then 46  when dpd_soft.max_late_pmt47 >= 1 then 47  when dpd_soft.max_late_pmt48 >= 1 then 48 else 0 end as collection_pmt_of_over1,
	case when dpd_soft.max_late_pmt1 >= 1 then 1 when dpd_soft.max_late_pmt2 >= 1 then 2 when dpd_soft.max_late_pmt3 >= 1 then 3 when dpd_soft.max_late_pmt4 >= 1 then 4 when dpd_soft.max_late_pmt5 >= 1 then 5 when dpd_soft.max_late_pmt6 >= 1 then 6 when dpd_soft.max_late_pmt7 >= 1 then 7 when dpd_soft.max_late_pmt8 >= 1 then 8 when dpd_soft.max_late_pmt9 >= 1 then 9 when dpd_soft.max_late_pmt10 >= 1 then 10 when dpd_soft.max_late_pmt11 >= 1 then 11 when dpd_soft.max_late_pmt12 >= 1 then 12 when dpd_soft.max_late_pmt13 >= 1 then 13 when dpd_soft.max_late_pmt14 >= 1 then 14 when dpd_soft.max_late_pmt15 >= 1 then 15 when dpd_soft.max_late_pmt16 >= 1 then 16 when dpd_soft.max_late_pmt17 >= 1 then 17 when dpd_soft.max_late_pmt18 >= 1 then 18 when dpd_soft.max_late_pmt19 >= 1 then 19 when dpd_soft.max_late_pmt20 >= 1 then 20 when dpd_soft.max_late_pmt21 >= 1 then 21 when dpd_soft.max_late_pmt22 >= 1 then 22 when dpd_soft.max_late_pmt23 >= 1 then 23 when dpd_soft.max_late_pmt24 >= 1 then 24 when dpd_soft.max_late_pmt25 >= 1 then 25  when dpd_soft.max_late_pmt26 >= 1 then 26  when dpd_soft.max_late_pmt27 >= 1 then 27  when dpd_soft.max_late_pmt28 >= 1 then 28  when dpd_soft.max_late_pmt29 >= 1 then 29  when dpd_soft.max_late_pmt30 >= 1 then 30  when dpd_soft.max_late_pmt31 >= 1 then 31  when dpd_soft.max_late_pmt32 >= 1 then 32  when dpd_soft.max_late_pmt33 >= 1 then 33  when dpd_soft.max_late_pmt34 >= 1 then 34  when dpd_soft.max_late_pmt35 >= 1 then 35  when dpd_soft.max_late_pmt36 >= 1 then 36  when dpd_soft.max_late_pmt37 >= 1 then 37  when dpd_soft.max_late_pmt38 >= 1 then 38  when dpd_soft.max_late_pmt39 >= 1 then 39  when dpd_soft.max_late_pmt40 >= 1 then 40  when dpd_soft.max_late_pmt41 >= 1 then 41  when dpd_soft.max_late_pmt42 >= 1 then 42  when dpd_soft.max_late_pmt43 >= 1 then 43  when dpd_soft.max_late_pmt44 >= 1 then 44  when dpd_soft.max_late_pmt45 >= 1 then 45  when dpd_soft.max_late_pmt46 >= 1 then 46  when dpd_soft.max_late_pmt47 >= 1 then 47  when dpd_soft.max_late_pmt48 >= 1 then 48 else 0 end / lr.number_of_installments as collection_perc_loan_of_over1,
	case when dpd_soft.max_late_pmt1 >= 7 then 1 when dpd_soft.max_late_pmt2 >= 7 then 2 when dpd_soft.max_late_pmt3 >= 7 then 3 when dpd_soft.max_late_pmt4 >= 7 then 4 when dpd_soft.max_late_pmt5 >= 7 then 5 when dpd_soft.max_late_pmt6 >= 7 then 6 when dpd_soft.max_late_pmt7 >= 7 then 7 when dpd_soft.max_late_pmt8 >= 7 then 8 when dpd_soft.max_late_pmt9 >= 7 then 9 when dpd_soft.max_late_pmt10 >= 7 then 10 when dpd_soft.max_late_pmt11 >= 7 then 11 when dpd_soft.max_late_pmt12 >= 7 then 12 when dpd_soft.max_late_pmt13 >= 7 then 13 when dpd_soft.max_late_pmt14 >= 7 then 14 when dpd_soft.max_late_pmt15 >= 7 then 15 when dpd_soft.max_late_pmt16 >= 7 then 16 when dpd_soft.max_late_pmt17 >= 7 then 17 when dpd_soft.max_late_pmt18 >= 7 then 18 when dpd_soft.max_late_pmt19 >= 7 then 19 when dpd_soft.max_late_pmt20 >= 7 then 20 when dpd_soft.max_late_pmt21 >= 7 then 21 when dpd_soft.max_late_pmt22 >= 7 then 22 when dpd_soft.max_late_pmt23 >= 7 then 23 when dpd_soft.max_late_pmt24 >= 7 then 24 when dpd_soft.max_late_pmt25 >= 7 then 25  when dpd_soft.max_late_pmt26 >= 7 then 26  when dpd_soft.max_late_pmt27 >= 7 then 27  when dpd_soft.max_late_pmt28 >= 7 then 28  when dpd_soft.max_late_pmt29 >= 7 then 29  when dpd_soft.max_late_pmt30 >= 7 then 30  when dpd_soft.max_late_pmt31 >= 7 then 31  when dpd_soft.max_late_pmt32 >= 7 then 32  when dpd_soft.max_late_pmt33 >= 7 then 33  when dpd_soft.max_late_pmt34 >= 7 then 34  when dpd_soft.max_late_pmt35 >= 7 then 35  when dpd_soft.max_late_pmt36 >= 7 then 36  when dpd_soft.max_late_pmt37 >= 7 then 37  when dpd_soft.max_late_pmt38 >= 7 then 38  when dpd_soft.max_late_pmt39 >= 7 then 39  when dpd_soft.max_late_pmt40 >= 7 then 40  when dpd_soft.max_late_pmt41 >= 7 then 41  when dpd_soft.max_late_pmt42 >= 7 then 42  when dpd_soft.max_late_pmt43 >= 7 then 43  when dpd_soft.max_late_pmt44 >= 7 then 44  when dpd_soft.max_late_pmt45 >= 7 then 45  when dpd_soft.max_late_pmt46 >= 7 then 46  when dpd_soft.max_late_pmt47 >= 7 then 47  when dpd_soft.max_late_pmt48 >= 7 then 48 else 0 end as collection_pmt_of_over7,
	case when dpd_soft.max_late_pmt1 >= 7 then 1 when dpd_soft.max_late_pmt2 >= 7 then 2 when dpd_soft.max_late_pmt3 >= 7 then 3 when dpd_soft.max_late_pmt4 >= 7 then 4 when dpd_soft.max_late_pmt5 >= 7 then 5 when dpd_soft.max_late_pmt6 >= 7 then 6 when dpd_soft.max_late_pmt7 >= 7 then 7 when dpd_soft.max_late_pmt8 >= 7 then 8 when dpd_soft.max_late_pmt9 >= 7 then 9 when dpd_soft.max_late_pmt10 >= 7 then 10 when dpd_soft.max_late_pmt11 >= 7 then 11 when dpd_soft.max_late_pmt12 >= 7 then 12 when dpd_soft.max_late_pmt13 >= 7 then 13 when dpd_soft.max_late_pmt14 >= 7 then 14 when dpd_soft.max_late_pmt15 >= 7 then 15 when dpd_soft.max_late_pmt16 >= 7 then 16 when dpd_soft.max_late_pmt17 >= 7 then 17 when dpd_soft.max_late_pmt18 >= 7 then 18 when dpd_soft.max_late_pmt19 >= 7 then 19 when dpd_soft.max_late_pmt20 >= 7 then 20 when dpd_soft.max_late_pmt21 >= 7 then 21 when dpd_soft.max_late_pmt22 >= 7 then 22 when dpd_soft.max_late_pmt23 >= 7 then 23 when dpd_soft.max_late_pmt24 >= 7 then 24 when dpd_soft.max_late_pmt25 >= 7 then 25  when dpd_soft.max_late_pmt26 >= 7 then 26  when dpd_soft.max_late_pmt27 >= 7 then 27  when dpd_soft.max_late_pmt28 >= 7 then 28  when dpd_soft.max_late_pmt29 >= 7 then 29  when dpd_soft.max_late_pmt30 >= 7 then 30  when dpd_soft.max_late_pmt31 >= 7 then 31  when dpd_soft.max_late_pmt32 >= 7 then 32  when dpd_soft.max_late_pmt33 >= 7 then 33  when dpd_soft.max_late_pmt34 >= 7 then 34  when dpd_soft.max_late_pmt35 >= 7 then 35  when dpd_soft.max_late_pmt36 >= 7 then 36  when dpd_soft.max_late_pmt37 >= 7 then 37  when dpd_soft.max_late_pmt38 >= 7 then 38  when dpd_soft.max_late_pmt39 >= 7 then 39  when dpd_soft.max_late_pmt40 >= 7 then 40  when dpd_soft.max_late_pmt41 >= 7 then 41  when dpd_soft.max_late_pmt42 >= 7 then 42  when dpd_soft.max_late_pmt43 >= 7 then 43  when dpd_soft.max_late_pmt44 >= 7 then 44  when dpd_soft.max_late_pmt45 >= 7 then 45  when dpd_soft.max_late_pmt46 >= 7 then 46  when dpd_soft.max_late_pmt47 >= 7 then 47  when dpd_soft.max_late_pmt48 >= 7 then 48 else 0 end / lr.number_of_installments as collection_perc_loan_of_over7,
	case when dpd_soft.max_late_pmt1 >= 15 then 1 when dpd_soft.max_late_pmt2 >= 15 then 2 when dpd_soft.max_late_pmt3 >= 15 then 3 when dpd_soft.max_late_pmt4 >= 15 then 4 when dpd_soft.max_late_pmt5 >= 15 then 5 when dpd_soft.max_late_pmt6 >= 15 then 6 when dpd_soft.max_late_pmt7 >= 15 then 7 when dpd_soft.max_late_pmt8 >= 15 then 8 when dpd_soft.max_late_pmt9 >= 15 then 9 when dpd_soft.max_late_pmt10 >= 15 then 10 when dpd_soft.max_late_pmt11 >= 15 then 11 when dpd_soft.max_late_pmt12 >= 15 then 12 when dpd_soft.max_late_pmt13 >= 15 then 13 when dpd_soft.max_late_pmt14 >= 15 then 14 when dpd_soft.max_late_pmt15 >= 15 then 15 when dpd_soft.max_late_pmt16 >= 15 then 16 when dpd_soft.max_late_pmt17 >= 15 then 17 when dpd_soft.max_late_pmt18 >= 15 then 18 when dpd_soft.max_late_pmt19 >= 15 then 19 when dpd_soft.max_late_pmt20 >= 15 then 20 when dpd_soft.max_late_pmt21 >= 15 then 21 when dpd_soft.max_late_pmt22 >= 15 then 22 when dpd_soft.max_late_pmt23 >= 15 then 23 when dpd_soft.max_late_pmt24 >= 15 then 24 when dpd_soft.max_late_pmt25 >= 15 then 25  when dpd_soft.max_late_pmt26 >= 15 then 26  when dpd_soft.max_late_pmt27 >= 15 then 27  when dpd_soft.max_late_pmt28 >= 15 then 28  when dpd_soft.max_late_pmt29 >= 15 then 29  when dpd_soft.max_late_pmt30 >= 15 then 30  when dpd_soft.max_late_pmt31 >= 15 then 31  when dpd_soft.max_late_pmt32 >= 15 then 32  when dpd_soft.max_late_pmt33 >= 15 then 33  when dpd_soft.max_late_pmt34 >= 15 then 34  when dpd_soft.max_late_pmt35 >= 15 then 35  when dpd_soft.max_late_pmt36 >= 15 then 36  when dpd_soft.max_late_pmt37 >= 15 then 37  when dpd_soft.max_late_pmt38 >= 15 then 38  when dpd_soft.max_late_pmt39 >= 15 then 39  when dpd_soft.max_late_pmt40 >= 15 then 40  when dpd_soft.max_late_pmt41 >= 15 then 41  when dpd_soft.max_late_pmt42 >= 15 then 42  when dpd_soft.max_late_pmt43 >= 15 then 43  when dpd_soft.max_late_pmt44 >= 15 then 44  when dpd_soft.max_late_pmt45 >= 15 then 45  when dpd_soft.max_late_pmt46 >= 15 then 46  when dpd_soft.max_late_pmt47 >= 15 then 47  when dpd_soft.max_late_pmt48 >= 15 then 48 else 0 end as collection_pmt_of_over15,
	case when dpd_soft.max_late_pmt1 >= 15 then 1 when dpd_soft.max_late_pmt2 >= 15 then 2 when dpd_soft.max_late_pmt3 >= 15 then 3 when dpd_soft.max_late_pmt4 >= 15 then 4 when dpd_soft.max_late_pmt5 >= 15 then 5 when dpd_soft.max_late_pmt6 >= 15 then 6 when dpd_soft.max_late_pmt7 >= 15 then 7 when dpd_soft.max_late_pmt8 >= 15 then 8 when dpd_soft.max_late_pmt9 >= 15 then 9 when dpd_soft.max_late_pmt10 >= 15 then 10 when dpd_soft.max_late_pmt11 >= 15 then 11 when dpd_soft.max_late_pmt12 >= 15 then 12 when dpd_soft.max_late_pmt13 >= 15 then 13 when dpd_soft.max_late_pmt14 >= 15 then 14 when dpd_soft.max_late_pmt15 >= 15 then 15 when dpd_soft.max_late_pmt16 >= 15 then 16 when dpd_soft.max_late_pmt17 >= 15 then 17 when dpd_soft.max_late_pmt18 >= 15 then 18 when dpd_soft.max_late_pmt19 >= 15 then 19 when dpd_soft.max_late_pmt20 >= 15 then 20 when dpd_soft.max_late_pmt21 >= 15 then 21 when dpd_soft.max_late_pmt22 >= 15 then 22 when dpd_soft.max_late_pmt23 >= 15 then 23 when dpd_soft.max_late_pmt24 >= 15 then 24 when dpd_soft.max_late_pmt25 >= 15 then 25  when dpd_soft.max_late_pmt26 >= 15 then 26  when dpd_soft.max_late_pmt27 >= 15 then 27  when dpd_soft.max_late_pmt28 >= 15 then 28  when dpd_soft.max_late_pmt29 >= 15 then 29  when dpd_soft.max_late_pmt30 >= 15 then 30  when dpd_soft.max_late_pmt31 >= 15 then 31  when dpd_soft.max_late_pmt32 >= 15 then 32  when dpd_soft.max_late_pmt33 >= 15 then 33  when dpd_soft.max_late_pmt34 >= 15 then 34  when dpd_soft.max_late_pmt35 >= 15 then 35  when dpd_soft.max_late_pmt36 >= 15 then 36  when dpd_soft.max_late_pmt37 >= 15 then 37  when dpd_soft.max_late_pmt38 >= 15 then 38  when dpd_soft.max_late_pmt39 >= 15 then 39  when dpd_soft.max_late_pmt40 >= 15 then 40  when dpd_soft.max_late_pmt41 >= 15 then 41  when dpd_soft.max_late_pmt42 >= 15 then 42  when dpd_soft.max_late_pmt43 >= 15 then 43  when dpd_soft.max_late_pmt44 >= 15 then 44  when dpd_soft.max_late_pmt45 >= 15 then 45  when dpd_soft.max_late_pmt46 >= 15 then 46  when dpd_soft.max_late_pmt47 >= 15 then 47  when dpd_soft.max_late_pmt48 >= 15 then 48 else 0 end / lr.number_of_installments as collection_perc_loan_of_over15,
	case when dpd_soft.max_late_pmt1 >= 30 then 1 when dpd_soft.max_late_pmt2 >= 30 then 2 when dpd_soft.max_late_pmt3 >= 30 then 3 when dpd_soft.max_late_pmt4 >= 30 then 4 when dpd_soft.max_late_pmt5 >= 30 then 5 when dpd_soft.max_late_pmt6 >= 30 then 6 when dpd_soft.max_late_pmt7 >= 30 then 7 when dpd_soft.max_late_pmt8 >= 30 then 8 when dpd_soft.max_late_pmt9 >= 30 then 9 when dpd_soft.max_late_pmt10 >= 30 then 10 when dpd_soft.max_late_pmt11 >= 30 then 11 when dpd_soft.max_late_pmt12 >= 30 then 12 when dpd_soft.max_late_pmt13 >= 30 then 13 when dpd_soft.max_late_pmt14 >= 30 then 14 when dpd_soft.max_late_pmt15 >= 30 then 15 when dpd_soft.max_late_pmt16 >= 30 then 16 when dpd_soft.max_late_pmt17 >= 30 then 17 when dpd_soft.max_late_pmt18 >= 30 then 18 when dpd_soft.max_late_pmt19 >= 30 then 19 when dpd_soft.max_late_pmt20 >= 30 then 20 when dpd_soft.max_late_pmt21 >= 30 then 21 when dpd_soft.max_late_pmt22 >= 30 then 22 when dpd_soft.max_late_pmt23 >= 30 then 23 when dpd_soft.max_late_pmt24 >= 30 then 24 when dpd_soft.max_late_pmt25 >= 30 then 25  when dpd_soft.max_late_pmt26 >= 30 then 26  when dpd_soft.max_late_pmt27 >= 30 then 27  when dpd_soft.max_late_pmt28 >= 30 then 28  when dpd_soft.max_late_pmt29 >= 30 then 29  when dpd_soft.max_late_pmt30 >= 30 then 30  when dpd_soft.max_late_pmt31 >= 30 then 31  when dpd_soft.max_late_pmt32 >= 30 then 32  when dpd_soft.max_late_pmt33 >= 30 then 33  when dpd_soft.max_late_pmt34 >= 30 then 34  when dpd_soft.max_late_pmt35 >= 30 then 35  when dpd_soft.max_late_pmt36 >= 30 then 36  when dpd_soft.max_late_pmt37 >= 30 then 37  when dpd_soft.max_late_pmt38 >= 30 then 38  when dpd_soft.max_late_pmt39 >= 30 then 39  when dpd_soft.max_late_pmt40 >= 30 then 40  when dpd_soft.max_late_pmt41 >= 30 then 41  when dpd_soft.max_late_pmt42 >= 30 then 42  when dpd_soft.max_late_pmt43 >= 30 then 43  when dpd_soft.max_late_pmt44 >= 30 then 44  when dpd_soft.max_late_pmt45 >= 30 then 45  when dpd_soft.max_late_pmt46 >= 30 then 46  when dpd_soft.max_late_pmt47 >= 30 then 47  when dpd_soft.max_late_pmt48 >= 30 then 48 else 0 end as collection_pmt_of_over30,
	case when dpd_soft.max_late_pmt1 >= 30 then 1 when dpd_soft.max_late_pmt2 >= 30 then 2 when dpd_soft.max_late_pmt3 >= 30 then 3 when dpd_soft.max_late_pmt4 >= 30 then 4 when dpd_soft.max_late_pmt5 >= 30 then 5 when dpd_soft.max_late_pmt6 >= 30 then 6 when dpd_soft.max_late_pmt7 >= 30 then 7 when dpd_soft.max_late_pmt8 >= 30 then 8 when dpd_soft.max_late_pmt9 >= 30 then 9 when dpd_soft.max_late_pmt10 >= 30 then 10 when dpd_soft.max_late_pmt11 >= 30 then 11 when dpd_soft.max_late_pmt12 >= 30 then 12 when dpd_soft.max_late_pmt13 >= 30 then 13 when dpd_soft.max_late_pmt14 >= 30 then 14 when dpd_soft.max_late_pmt15 >= 30 then 15 when dpd_soft.max_late_pmt16 >= 30 then 16 when dpd_soft.max_late_pmt17 >= 30 then 17 when dpd_soft.max_late_pmt18 >= 30 then 18 when dpd_soft.max_late_pmt19 >= 30 then 19 when dpd_soft.max_late_pmt20 >= 30 then 20 when dpd_soft.max_late_pmt21 >= 30 then 21 when dpd_soft.max_late_pmt22 >= 30 then 22 when dpd_soft.max_late_pmt23 >= 30 then 23 when dpd_soft.max_late_pmt24 >= 30 then 24 when dpd_soft.max_late_pmt25 >= 30 then 25  when dpd_soft.max_late_pmt26 >= 30 then 26  when dpd_soft.max_late_pmt27 >= 30 then 27  when dpd_soft.max_late_pmt28 >= 30 then 28  when dpd_soft.max_late_pmt29 >= 30 then 29  when dpd_soft.max_late_pmt30 >= 30 then 30  when dpd_soft.max_late_pmt31 >= 30 then 31  when dpd_soft.max_late_pmt32 >= 30 then 32  when dpd_soft.max_late_pmt33 >= 30 then 33  when dpd_soft.max_late_pmt34 >= 30 then 34  when dpd_soft.max_late_pmt35 >= 30 then 35  when dpd_soft.max_late_pmt36 >= 30 then 36  when dpd_soft.max_late_pmt37 >= 30 then 37  when dpd_soft.max_late_pmt38 >= 30 then 38  when dpd_soft.max_late_pmt39 >= 30 then 39  when dpd_soft.max_late_pmt40 >= 30 then 40  when dpd_soft.max_late_pmt41 >= 30 then 41  when dpd_soft.max_late_pmt42 >= 30 then 42  when dpd_soft.max_late_pmt43 >= 30 then 43  when dpd_soft.max_late_pmt44 >= 30 then 44  when dpd_soft.max_late_pmt45 >= 30 then 45  when dpd_soft.max_late_pmt46 >= 30 then 46  when dpd_soft.max_late_pmt47 >= 30 then 47  when dpd_soft.max_late_pmt48 >= 30 then 48 else 0 end / lr.number_of_installments as collection_perc_loan_of_over30,
	case when dpd_soft.max_late_pmt1 >= 60 then 1 when dpd_soft.max_late_pmt2 >= 60 then 2 when dpd_soft.max_late_pmt3 >= 60 then 3 when dpd_soft.max_late_pmt4 >= 60 then 4 when dpd_soft.max_late_pmt5 >= 60 then 5 when dpd_soft.max_late_pmt6 >= 60 then 6 when dpd_soft.max_late_pmt7 >= 60 then 7 when dpd_soft.max_late_pmt8 >= 60 then 8 when dpd_soft.max_late_pmt9 >= 60 then 9 when dpd_soft.max_late_pmt10 >= 60 then 10 when dpd_soft.max_late_pmt11 >= 60 then 11 when dpd_soft.max_late_pmt12 >= 60 then 12 when dpd_soft.max_late_pmt13 >= 60 then 13 when dpd_soft.max_late_pmt14 >= 60 then 14 when dpd_soft.max_late_pmt15 >= 60 then 15 when dpd_soft.max_late_pmt16 >= 60 then 16 when dpd_soft.max_late_pmt17 >= 60 then 17 when dpd_soft.max_late_pmt18 >= 60 then 18 when dpd_soft.max_late_pmt19 >= 60 then 19 when dpd_soft.max_late_pmt20 >= 60 then 20 when dpd_soft.max_late_pmt21 >= 60 then 21 when dpd_soft.max_late_pmt22 >= 60 then 22 when dpd_soft.max_late_pmt23 >= 60 then 23 when dpd_soft.max_late_pmt24 >= 60 then 24 when dpd_soft.max_late_pmt25 >= 60 then 25  when dpd_soft.max_late_pmt26 >= 60 then 26  when dpd_soft.max_late_pmt27 >= 60 then 27  when dpd_soft.max_late_pmt28 >= 60 then 28  when dpd_soft.max_late_pmt29 >= 60 then 29  when dpd_soft.max_late_pmt30 >= 60 then 30  when dpd_soft.max_late_pmt31 >= 60 then 31  when dpd_soft.max_late_pmt32 >= 60 then 32  when dpd_soft.max_late_pmt33 >= 60 then 33  when dpd_soft.max_late_pmt34 >= 60 then 34  when dpd_soft.max_late_pmt35 >= 60 then 35  when dpd_soft.max_late_pmt36 >= 60 then 36  when dpd_soft.max_late_pmt37 >= 60 then 37  when dpd_soft.max_late_pmt38 >= 60 then 38  when dpd_soft.max_late_pmt39 >= 60 then 39  when dpd_soft.max_late_pmt40 >= 60 then 40  when dpd_soft.max_late_pmt41 >= 60 then 41  when dpd_soft.max_late_pmt42 >= 60 then 42  when dpd_soft.max_late_pmt43 >= 60 then 43  when dpd_soft.max_late_pmt44 >= 60 then 44  when dpd_soft.max_late_pmt45 >= 60 then 45  when dpd_soft.max_late_pmt46 >= 60 then 46  when dpd_soft.max_late_pmt47 >= 60 then 47  when dpd_soft.max_late_pmt48 >= 60 then 48 else 0 end as collection_pmt_of_over60,
	case when dpd_soft.max_late_pmt1 >= 60 then 1 when dpd_soft.max_late_pmt2 >= 60 then 2 when dpd_soft.max_late_pmt3 >= 60 then 3 when dpd_soft.max_late_pmt4 >= 60 then 4 when dpd_soft.max_late_pmt5 >= 60 then 5 when dpd_soft.max_late_pmt6 >= 60 then 6 when dpd_soft.max_late_pmt7 >= 60 then 7 when dpd_soft.max_late_pmt8 >= 60 then 8 when dpd_soft.max_late_pmt9 >= 60 then 9 when dpd_soft.max_late_pmt10 >= 60 then 10 when dpd_soft.max_late_pmt11 >= 60 then 11 when dpd_soft.max_late_pmt12 >= 60 then 12 when dpd_soft.max_late_pmt13 >= 60 then 13 when dpd_soft.max_late_pmt14 >= 60 then 14 when dpd_soft.max_late_pmt15 >= 60 then 15 when dpd_soft.max_late_pmt16 >= 60 then 16 when dpd_soft.max_late_pmt17 >= 60 then 17 when dpd_soft.max_late_pmt18 >= 60 then 18 when dpd_soft.max_late_pmt19 >= 60 then 19 when dpd_soft.max_late_pmt20 >= 60 then 20 when dpd_soft.max_late_pmt21 >= 60 then 21 when dpd_soft.max_late_pmt22 >= 60 then 22 when dpd_soft.max_late_pmt23 >= 60 then 23 when dpd_soft.max_late_pmt24 >= 60 then 24 when dpd_soft.max_late_pmt25 >= 60 then 25  when dpd_soft.max_late_pmt26 >= 60 then 26  when dpd_soft.max_late_pmt27 >= 60 then 27  when dpd_soft.max_late_pmt28 >= 60 then 28  when dpd_soft.max_late_pmt29 >= 60 then 29  when dpd_soft.max_late_pmt30 >= 60 then 30  when dpd_soft.max_late_pmt31 >= 60 then 31  when dpd_soft.max_late_pmt32 >= 60 then 32  when dpd_soft.max_late_pmt33 >= 60 then 33  when dpd_soft.max_late_pmt34 >= 60 then 34  when dpd_soft.max_late_pmt35 >= 60 then 35  when dpd_soft.max_late_pmt36 >= 60 then 36  when dpd_soft.max_late_pmt37 >= 60 then 37  when dpd_soft.max_late_pmt38 >= 60 then 38  when dpd_soft.max_late_pmt39 >= 60 then 39  when dpd_soft.max_late_pmt40 >= 60 then 40  when dpd_soft.max_late_pmt41 >= 60 then 41  when dpd_soft.max_late_pmt42 >= 60 then 42  when dpd_soft.max_late_pmt43 >= 60 then 43  when dpd_soft.max_late_pmt44 >= 60 then 44  when dpd_soft.max_late_pmt45 >= 60 then 45  when dpd_soft.max_late_pmt46 >= 60 then 46  when dpd_soft.max_late_pmt47 >= 60 then 47  when dpd_soft.max_late_pmt48 >= 60 then 48 else 0 end / lr.number_of_installments as collection_perc_loan_of_over60,
	(coalesce(dpd_soft.max_late_pmt1,-1) = 0 and paid_inst >= 1)::int + (coalesce(dpd_soft.max_late_pmt2,-1) = 0 and paid_inst >= 2)::int + (coalesce(dpd_soft.max_late_pmt3,-1) = 0 and paid_inst >= 3)::int + (coalesce(dpd_soft.max_late_pmt4,-1) = 0 and paid_inst >= 4)::int + (coalesce(dpd_soft.max_late_pmt5,-1) = 0 and paid_inst >= 5)::int + (coalesce(dpd_soft.max_late_pmt6,-1) = 0 and paid_inst >= 6)::int + (coalesce(dpd_soft.max_late_pmt7,-1) = 0 and paid_inst >= 7)::int + (coalesce(dpd_soft.max_late_pmt8,-1) = 0 and paid_inst >= 8)::int + (coalesce(dpd_soft.max_late_pmt9,-1) = 0 and paid_inst >= 9)::int + (coalesce(dpd_soft.max_late_pmt10,-1) = 0 and paid_inst >= 10)::int + (coalesce(dpd_soft.max_late_pmt11,-1) = 0 and paid_inst >= 11)::int + (coalesce(dpd_soft.max_late_pmt12,-1) = 0 and paid_inst >= 12)::int + (coalesce(dpd_soft.max_late_pmt13,-1) = 0 and paid_inst >= 13)::int + (coalesce(dpd_soft.max_late_pmt14,-1) = 0 and paid_inst >= 14)::int + (coalesce(dpd_soft.max_late_pmt15,-1) = 0 and paid_inst >= 15)::int + (coalesce(dpd_soft.max_late_pmt16,-1) = 0 and paid_inst >= 16)::int + (coalesce(dpd_soft.max_late_pmt17,-1) = 0 and paid_inst >= 17)::int + (coalesce(dpd_soft.max_late_pmt18,-1) = 0 and paid_inst >= 18)::int + (coalesce(dpd_soft.max_late_pmt19,-1) = 0 and paid_inst >= 19)::int + (coalesce(dpd_soft.max_late_pmt20,-1) = 0 and paid_inst >= 20)::int + (coalesce(dpd_soft.max_late_pmt21,-1) = 0 and paid_inst >= 21)::int + (coalesce(dpd_soft.max_late_pmt22,-1) = 0 and paid_inst >= 22)::int + (coalesce(dpd_soft.max_late_pmt23,-1) = 0 and paid_inst >= 23)::int + (coalesce(dpd_soft.max_late_pmt24,-1) = 0 and paid_inst >= 24)::int + (coalesce(dpd_soft.max_late_pmt25,-1) = 0 and paid_inst >= 25)::int + (coalesce(dpd_soft.max_late_pmt26,-1) = 0 and paid_inst >= 26)::int + (coalesce(dpd_soft.max_late_pmt27,-1) = 0 and paid_inst >= 27)::int + (coalesce(dpd_soft.max_late_pmt28,-1) = 0 and paid_inst >= 28)::int + (coalesce(dpd_soft.max_late_pmt29,-1) = 0 and paid_inst >= 29)::int + (coalesce(dpd_soft.max_late_pmt30,-1) = 0 and paid_inst >= 30)::int + (coalesce(dpd_soft.max_late_pmt31,-1) = 0 and paid_inst >= 31)::int + (coalesce(dpd_soft.max_late_pmt32,-1) = 0 and paid_inst >= 32)::int + (coalesce(dpd_soft.max_late_pmt33,-1) = 0 and paid_inst >= 33)::int + (coalesce(dpd_soft.max_late_pmt34,-1) = 0 and paid_inst >= 34)::int + (coalesce(dpd_soft.max_late_pmt35,-1) = 0 and paid_inst >= 35)::int + (coalesce(dpd_soft.max_late_pmt36,-1) = 0 and paid_inst >= 36)::int + (coalesce(dpd_soft.max_late_pmt37,-1) = 0 and paid_inst >= 37)::int + (coalesce(dpd_soft.max_late_pmt38,-1) = 0 and paid_inst >= 38)::int + (coalesce(dpd_soft.max_late_pmt39,-1) = 0 and paid_inst >= 39)::int + (coalesce(dpd_soft.max_late_pmt40,-1) = 0 and paid_inst >= 40)::int + (coalesce(dpd_soft.max_late_pmt41,-1) = 0 and paid_inst >= 41)::int + (coalesce(dpd_soft.max_late_pmt42,-1) = 0 and paid_inst >= 42)::int + (coalesce(dpd_soft.max_late_pmt43,-1) = 0 and paid_inst >= 43)::int + (coalesce(dpd_soft.max_late_pmt44,-1) = 0 and paid_inst >= 44)::int + (coalesce(dpd_soft.max_late_pmt45,-1) = 0 and paid_inst >= 45)::int + (coalesce(dpd_soft.max_late_pmt46,-1) = 0 and paid_inst >= 46)::int + (coalesce(dpd_soft.max_late_pmt47,-1) = 0 and paid_inst >= 47)::int + (coalesce(dpd_soft.max_late_pmt48,-1) = 0 and paid_inst >= 48)::int as collection_count_pmts_paid_on_time,
	(coalesce(dpd_soft.max_late_pmt1,0) < 0 and paid_inst >= 1)::int + (coalesce(dpd_soft.max_late_pmt2,0) < 0 and paid_inst >= 2)::int + (coalesce(dpd_soft.max_late_pmt3,0) < 0 and paid_inst >= 3)::int + (coalesce(dpd_soft.max_late_pmt4,0) < 0 and paid_inst >= 4)::int + (coalesce(dpd_soft.max_late_pmt5,0) < 0 and paid_inst >= 5)::int + (coalesce(dpd_soft.max_late_pmt6,0) < 0 and paid_inst >= 6)::int + (coalesce(dpd_soft.max_late_pmt7,0) < 0 and paid_inst >= 7)::int + (coalesce(dpd_soft.max_late_pmt8,0) < 0 and paid_inst >= 8)::int + (coalesce(dpd_soft.max_late_pmt9,0) < 0 and paid_inst >= 9)::int + (coalesce(dpd_soft.max_late_pmt10,0) < 0 and paid_inst >= 10)::int + (coalesce(dpd_soft.max_late_pmt11,0) < 0 and paid_inst >= 11)::int + (coalesce(dpd_soft.max_late_pmt12,0) < 0 and paid_inst >= 12)::int + (coalesce(dpd_soft.max_late_pmt13,0) < 0 and paid_inst >= 13)::int + (coalesce(dpd_soft.max_late_pmt14,0) < 0 and paid_inst >= 14)::int + (coalesce(dpd_soft.max_late_pmt15,0) < 0 and paid_inst >= 15)::int + (coalesce(dpd_soft.max_late_pmt16,0) < 0 and paid_inst >= 16)::int + (coalesce(dpd_soft.max_late_pmt17,0) < 0 and paid_inst >= 17)::int + (coalesce(dpd_soft.max_late_pmt18,0) < 0 and paid_inst >= 18)::int + (coalesce(dpd_soft.max_late_pmt19,0) < 0 and paid_inst >= 19)::int + (coalesce(dpd_soft.max_late_pmt20,0) < 0 and paid_inst >= 20)::int + (coalesce(dpd_soft.max_late_pmt21,0) < 0 and paid_inst >= 21)::int + (coalesce(dpd_soft.max_late_pmt22,0) < 0 and paid_inst >= 22)::int + (coalesce(dpd_soft.max_late_pmt23,0) < 0 and paid_inst >= 23)::int + (coalesce(dpd_soft.max_late_pmt24,0) < 0 and paid_inst >= 24)::int + (coalesce(dpd_soft.max_late_pmt25,0) < 0 and paid_inst >= 25)::int + (coalesce(dpd_soft.max_late_pmt26,0) < 0 and paid_inst >= 26)::int + (coalesce(dpd_soft.max_late_pmt27,0) < 0 and paid_inst >= 27)::int + (coalesce(dpd_soft.max_late_pmt28,0) < 0 and paid_inst >= 28)::int + (coalesce(dpd_soft.max_late_pmt29,0) < 0 and paid_inst >= 29)::int + (coalesce(dpd_soft.max_late_pmt30,0) < 0 and paid_inst >= 30)::int + (coalesce(dpd_soft.max_late_pmt31,0) < 0 and paid_inst >= 31)::int + (coalesce(dpd_soft.max_late_pmt32,0) < 0 and paid_inst >= 32)::int + (coalesce(dpd_soft.max_late_pmt33,0) < 0 and paid_inst >= 33)::int + (coalesce(dpd_soft.max_late_pmt34,0) < 0 and paid_inst >= 34)::int + (coalesce(dpd_soft.max_late_pmt35,0) < 0 and paid_inst >= 35)::int + (coalesce(dpd_soft.max_late_pmt36,0) < 0 and paid_inst >= 36)::int + (coalesce(dpd_soft.max_late_pmt37,0) < 0 and paid_inst >= 37)::int + (coalesce(dpd_soft.max_late_pmt38,0) < 0 and paid_inst >= 38)::int + (coalesce(dpd_soft.max_late_pmt39,0) < 0 and paid_inst >= 39)::int + (coalesce(dpd_soft.max_late_pmt40,0) < 0 and paid_inst >= 40)::int + (coalesce(dpd_soft.max_late_pmt41,0) < 0 and paid_inst >= 41)::int + (coalesce(dpd_soft.max_late_pmt42,0) < 0 and paid_inst >= 42)::int + (coalesce(dpd_soft.max_late_pmt43,0) < 0 and paid_inst >= 43)::int + (coalesce(dpd_soft.max_late_pmt44,0) < 0 and paid_inst >= 44)::int + (coalesce(dpd_soft.max_late_pmt45,0) < 0 and paid_inst >= 45)::int + (coalesce(dpd_soft.max_late_pmt46,0) < 0 and paid_inst >= 46)::int + (coalesce(dpd_soft.max_late_pmt47,0) < 0 and paid_inst >= 47)::int + (coalesce(dpd_soft.max_late_pmt48,0) < 0 and paid_inst >= 48)::int as collection_count_pmts_paid_in_advance,
	parcelas.n_parcelas_pagas_em_atraso as collection_count_pmts_paid_late,
	(parcelas.n_parcelas_pagas_em_atraso / parcelas2.loan_original_final_term >= 0.5)::int as collection_recurring_late_payments,
	count_paga_fora_de_ordem as collection_pmts_paid_out_of_order,
--PMT3 SOFT
	soft.ever_pmt_3 as collection_ever_by_pmt3_soft,
	(soft.ever_pmt_3 > 30)::int as collection_ever_30_pmt3_soft,
	(soft.ever_pmt_3 > 60)::int as collection_ever_60_pmt3_soft,
	soft.over_pmt_3 as collection_over_by_pmt3_soft,
	(soft.over_pmt_3 > 30)::int as collection_over_30_pmt3_soft,
	(soft.over_pmt_3 > 60)::int as collection_over_60_pmt3_soft,
	original_p_and_i - amount_paid.amount_paid_pmt_3 as collection_ead_pmt3,
	amount_paid.amount_paid_pmt_3 as collection_amount_paid_pmt3,
--PMT6 SOFT
	soft.ever_pmt_6 as collection_ever_by_pmt6_soft,
	(soft.ever_pmt_6 > 30)::int as collection_ever_30_pmt6_soft,
	(soft.ever_pmt_6 > 60)::int as collection_ever_60_pmt6_soft,
	soft.over_pmt_6 as collection_over_by_pmt6_soft,
	(soft.over_pmt_6 > 30)::int as collection_over_30_pmt6_soft,
	(soft.over_pmt_6 > 60)::int as collection_over_60_pmt6_soft,
--pmt12 AGGRESSIVE
	agg.ever_pmt_12 as collection_ever_by_pmt12_aggressive,
	(agg.ever_pmt_12 > 30)::int as collection_ever_30_pmt12_aggressive,
	(agg.ever_pmt_12 > 60)::int as collection_ever_60_pmt12_aggressive,
	agg.over_pmt_12 as collection_over_by_pmt12_aggressive,
	(agg.over_pmt_12 > 30)::int as collection_over_30_pmt12_aggressive,
	(agg.over_pmt_12 > 60)::int as collection_over_60_pmt12_aggressive,
	original_p_and_i - amount_paid.amount_paid_pmt_5 as collection_ead_pmt12,
	amount_paid.amount_paid_pmt_5 as collection_amount_paid_pmt12,
--pmt11 AGGRESSIVE
	agg.ever_pmt_11 as collection_ever_by_pmt11_aggressive,
	(agg.ever_pmt_11 > 30)::int as collection_ever_30_pmt11_aggressive,
	(agg.ever_pmt_11 > 60)::int as collection_ever_60_pmt11_aggressive,
	agg.over_pmt_11 as collection_over_by_pmt11_aggressive,
	(agg.over_pmt_11 > 30)::int as collection_over_30_pmt11_aggressive,
	(agg.over_pmt_11 > 60)::int as collection_over_60_pmt11_aggressive,
	original_p_and_i - amount_paid.amount_paid_pmt_5 as collection_ead_pmt11,
	amount_paid.amount_paid_pmt_5 as collection_amount_paid_pmt11,
--PMT10 AGGRESSIVE
	agg.ever_pmt_10 as collection_ever_by_pmt10_aggressive,
	(agg.ever_pmt_10 > 30)::int as collection_ever_30_pmt10_aggressive,
	(agg.ever_pmt_10 > 60)::int as collection_ever_60_pmt10_aggressive,
	agg.over_pmt_10 as collection_over_by_pmt10_aggressive,
	(agg.over_pmt_10 > 30)::int as collection_over_30_pmt10_aggressive,
	(agg.over_pmt_10 > 60)::int as collection_over_60_pmt10_aggressive,
	original_p_and_i - amount_paid.amount_paid_pmt_5 as collection_ead_pmt10,
	amount_paid.amount_paid_pmt_5 as collection_amount_paid_pmt10,
--PMT9 AGGRESSIVE
	agg.ever_pmt_9 as collection_ever_by_pmt9_aggressive,
	(agg.ever_pmt_9 > 30)::int as collection_ever_30_pmt9_aggressive,
	(agg.ever_pmt_9 > 60)::int as collection_ever_60_pmt9_aggressive,
	agg.over_pmt_9 as collection_over_by_pmt9_aggressive,
	(agg.over_pmt_9 > 30)::int as collection_over_30_pmt9_aggressive,
	(agg.over_pmt_9 > 60)::int as collection_over_60_pmt9_aggressive,
	original_p_and_i - amount_paid.amount_paid_pmt_5 as collection_ead_pmt9,
	amount_paid.amount_paid_pmt_5 as collection_amount_paid_pmt9,
--PMT8 AGGRESSIVE
	agg.ever_pmt_8 as collection_ever_by_pmt8_aggressive,
	(agg.ever_pmt_8 > 30)::int as collection_ever_30_pmt8_aggressive,
	(agg.ever_pmt_8 > 60)::int as collection_ever_60_pmt8_aggressive,
	agg.over_pmt_8 as collection_over_by_pmt8_aggressive,
	(agg.over_pmt_8 > 30)::int as collection_over_30_pmt8_aggressive,
	(agg.over_pmt_8 > 60)::int as collection_over_60_pmt8_aggressive,
	original_p_and_i - amount_paid.amount_paid_pmt_5 as collection_ead_pmt8,
	amount_paid.amount_paid_pmt_5 as collection_amount_paid_pmt8,
--PMT7 AGGRESSIVE
	agg.ever_pmt_7 as collection_ever_by_pmt7_aggressive,
	(agg.ever_pmt_7 > 30)::int as collection_ever_30_pmt7_aggressive,
	(agg.ever_pmt_7 > 60)::int as collection_ever_60_pmt7_aggressive,
	agg.over_pmt_7 as collection_over_by_pmt7_aggressive,
	(agg.over_pmt_7 > 30)::int as collection_over_30_pmt7_aggressive,
	(agg.over_pmt_7 > 60)::int as collection_over_60_pmt7_aggressive,
	original_p_and_i - amount_paid.amount_paid_pmt_5 as collection_ead_pmt7,
	amount_paid.amount_paid_pmt_5 as collection_amount_paid_pmt7,
--PMT6 AGGRESSIVE
	agg.ever_pmt_6 as collection_ever_by_pmt6_aggressive,
	(agg.ever_pmt_6 > 30)::int as collection_ever_30_pmt6_aggressive,
	(agg.ever_pmt_6 > 60)::int as collection_ever_60_pmt6_aggressive,
	agg.over_pmt_6 as collection_over_by_pmt6_aggressive,
	(agg.over_pmt_6 > 30)::int as collection_over_30_pmt6_aggressive,
	(agg.over_pmt_6 > 60)::int as collection_over_60_pmt6_aggressive,
	original_p_and_i - amount_paid.amount_paid_pmt_5 as collection_ead_pmt6,
	amount_paid.amount_paid_pmt_5 as collection_amount_paid_pmt6,
--PMT5 AGGRESSIVE
	agg.ever_pmt_5 as collection_ever_by_pmt5_aggressive,
	(agg.ever_pmt_5 > 30)::int as collection_ever_30_pmt5_aggressive,
	(agg.ever_pmt_5 > 60)::int as collection_ever_60_pmt5_aggressive,
	agg.over_pmt_5 as collection_over_by_pmt5_aggressive,
	(agg.over_pmt_5 > 30)::int as collection_over_30_pmt5_aggressive,
	(agg.over_pmt_5 > 60)::int as collection_over_60_pmt5_aggressive,
	original_p_and_i - amount_paid.amount_paid_pmt_5 as collection_ead_pmt5,
	amount_paid.amount_paid_pmt_4 as collection_amount_paid_pmt5,
--PMT4 AGGRESSIVE
	agg.ever_pmt_4 as collection_ever_by_pmt4_aggressive,
	(agg.ever_pmt_4 > 30)::int as collection_ever_30_pmt4_aggressive,
	(agg.ever_pmt_4 > 60)::int as collection_ever_60_pmt4_aggressive,
	agg.over_pmt_4 as collection_over_by_pmt4_aggressive,
	(agg.over_pmt_4 > 30)::int as collection_over_30_pmt4_aggressive,
	(agg.over_pmt_4 > 60)::int as collection_over_60_pmt4_aggressive,
	original_p_and_i - amount_paid.amount_paid_pmt_4 as collection_ead_pmt4,
	amount_paid.amount_paid_pmt_4 as collection_amount_paid_pmt4,
--PMT50% AGGRESSIVE	
	agg.ever_pmt_perc_50 as collection_ever_by_pmt50pct_aggressive,
	(agg.ever_pmt_perc_50 > 30)::int as collection_ever_30_pmt50pct_aggressive,
	(agg.ever_pmt_perc_50 > 60)::int as collection_ever_60_pmt50pct_aggressive,
	agg.over_pmt_perc_50 as collection_over_by_pmt50pct_aggressive,
	(agg.over_pmt_perc_50 > 30)::int as collection_over_30_pmt50pct_aggressive,
	(agg.over_pmt_perc_50 > 60)::int as collection_over_60_pmt50pct_aggressive,
	original_p_and_i - amount_paid.amount_paid_pmt_perc_50 as collection_ead_pmt50pct,
	amount_paid.amount_paid_pmt_perc_50 as collection_amount_paid_pmt50pct,
--------------------------SCR HISTORICO PJ
--Divida longo prazo
	case when coalesce(Ever_long_term_debt_PJ,Ever_CarteiraCredito_PJ) is not null then (coalesce(Ever_long_term_debt_PJ,Ever_CarteiraCredito_PJ) > 0)::int end as scr_Has_Debt_History_PJ, 
	coalesce(Ever_long_term_debt_PJ,Ever_CarteiraCredito_PJ) * 1000 as scr_ever_total_debt_PJ,
	coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) * 1000 as scr_curr_total_debt_PJ,
	coalesce(long_term_debt_PJ_5M,CarteiraCredito_PJ_5M) * 1000 as scr_6M_total_debt_PJ,
	coalesce(long_term_debt_PJ_11M,CarteiraCredito_PJ_11M) * 1000 as scr_12M_total_debt_PJ,
	coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) / greatest(dp.month_revenue, 1) / 12 * 1000 as scr_Curr_total_debt_PJ_over_revenue,
	coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) / greatest(amount_requested,1) * 1000 as scr_Curr_total_debt_PJ_over_application_value,
	coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) / case divida_atual_PJ when 0 then 1 else divida_atual_PJ end as scr_curr_total_debt_PJ_over_short_term_debt,
	coalesce(max_long_term_debt_PJ,max_CarteiraCredito_PJ) * 1000 as scr_max_total_debt_pj,
	coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) / case coalesce(max_long_term_debt_PJ,max_CarteiraCredito_PJ) when 0 then 1 else coalesce(max_long_term_debt_PJ,max_CarteiraCredito_PJ) end as scr_curr_over_max_total_debt_pj,
	case when coalesce(Ever_long_term_debt_PJ,Ever_CarteiraCredito_PJ) is not null then (coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) = coalesce(max_long_term_debt_PJ,max_CarteiraCredito_PJ) and coalesce(max_long_term_debt_PJ,max_CarteiraCredito_PJ) > 0)::int end as scr_All_TimeHigh_total_debt_pj,
	(coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) - coalesce(long_term_debt_PJ_5M,CarteiraCredito_PJ_5M)) / greatest(dp.month_revenue, 1) / 12 * 1000 as scr_var_abs_6M_total_debt_PJ_over_revenue,
	(coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) - coalesce(long_term_debt_PJ_11M,CarteiraCredito_PJ_11M)) / greatest(dp.month_revenue, 1) / 12 * 1000 as scr_var_abs_12M_total_debt_PJ_over_revenue,
	case when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) = 0 and coalesce(long_term_debt_PJ_5M,CarteiraCredito_PJ_5M) = 0 then 0 else coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) / case coalesce(long_term_debt_PJ_5M,CarteiraCredito_PJ_5M) when 0 then 1 else coalesce(long_term_debt_PJ_5M,CarteiraCredito_PJ_5M) end - 1 end as scr_var_rel_6M_total_debt_pj,
	case when coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) = 0 and coalesce(long_term_debt_PJ_11M,CarteiraCredito_PJ_11M) = 0 then 0 else coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) / case coalesce(long_term_debt_PJ_11M,CarteiraCredito_PJ_11M) when 0 then 1 else coalesce(long_term_debt_PJ_11M,CarteiraCredito_PJ_11M) end - 1 end as scr_var_rel_12M_total_debt_pj,
	coalesce(Meses_Aumento_DividaPJ,Meses_Aumento_CarteiraCredito_PJ) / greatest(coalesce(qtd_meses_escopo_PJ, qtd_meses_modalidade_PJ) - 1, 1) as scr_Months_Rise_total_debt_pj_over_total_months,
	coalesce(Meses_Reducao_DividaPJ,Meses_Reducao_CarteiraCredito_PJ) / greatest(coalesce(qtd_meses_escopo_PJ, qtd_meses_modalidade_PJ) - 1, 1) as scr_Months_Dive_total_debt_pj_over_total_months,
	coalesce(months_sth_PJ,months_sth_CarteiraCredito_PJ) / greatest(coalesce(qtd_meses_escopo_PJ, qtd_meses_modalidade_PJ) - 1, 1) as scr_consecutive_dive_total_debt_pj_over_total_months,
	coalesce(months_hth_PJ,months_hth_CarteiraCredito_PJ) / greatest(coalesce(qtd_meses_escopo_PJ, qtd_meses_modalidade_PJ) - 1, 1) as scr_consecutive_rise_total_debt_pj_over_total_months,
	coalesce(Saldo_Amort_DividaPJ,Saldo_Amort_CarteiraCredito_PJ) * 1000 as scr_total_debt_amortization_PJ,
	coalesce(Saldo_Amort_DividaPJ,Saldo_Amort_CarteiraCredito_PJ) / greatest(coalesce(Meses_Reducao_DividaPJ,Meses_Reducao_CarteiraCredito_PJ),1) / greatest(dp.month_revenue, 1) / 12 * 1000 as scr_Avg_Amortization_total_debt_PJ_over_revenue,
	coalesce(Saldo_Amort_DividaPJ,Saldo_Amort_CarteiraCredito_PJ) / greatest(coalesce(Meses_Reducao_DividaPJ,Meses_Reducao_CarteiraCredito_PJ),1) / ((@li.pmt)) * 1000 as scr_Avg_Amortization_total_debt_PJ_over_loan_PMT,
--Vencido
	Overdue_PJ_Curr * 1000 as scr_curr_overdue_debt_PJ,
	Max_Overdue_PJ * 1000 as scr_max_overdue_debt_PJ,
	Months_Overdue_PJ / greatest(Qtd_meses_escopo_PJ, 1) as scr_Months_Overdue_PJ_over_total_months,
	case when Months_Since_Last_Overdue_PJ is null or coalesce(Ever_long_term_debt_PJ,Ever_CarteiraCredito_PJ) = 0 then - 1 else Months_Since_Last_Overdue_PJ end as scr_Months_since_last_Overdue_PJ,
	Max_Overdue_PJ / greatest(dp.month_revenue, 1) / 12 * 1000 as scr_Max_Overdue_PJ_over_Revenue,
	Overdue_PJ_Curr / greatest(dp.month_revenue, 1) / 12 * 1000 as scr_Curr_Overdue_PJ_over_Revenue,
	Overdue_PJ_Curr / case Long_Term_Debt_PJ_Curr when 0 then 1 else Long_Term_Debt_PJ_Curr end as scr_Curr_Overdue_PJ_over_total_debt,
--Prejuizo
	coalesce(Default_PJ_Curr,Prejuizo_PJ_Curr) * 1000 as scr_curr_default_debt_PJ,
	coalesce(Max_Default_PJ,Max_Prejuizo_PJ) * 1000 as scr_max_default_debt_PJ,
	coalesce(Months_Default_PJ,Meses_Prejuizo_PJ) / greatest(coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ),1) as scr_Months_Default_PJ_over_total_months,
	case when coalesce(Months_Since_Last_Default_PJ,Meses_Desde_Ultimo_Prejuizo_PJ) is null or coalesce(Ever_long_term_debt_PJ,Ever_CarteiraCredito_PJ) = 0 then - 1 else Months_Since_Last_Default_PJ end as scr_Months_since_last_Default_PJ,
	coalesce(Max_Default_PJ,Max_Prejuizo_PJ) / greatest(dp.month_revenue, 1) / 12 * 1000 as scr_Max_Default_PJ_over_revenue,
	coalesce(Default_PJ_Curr,Prejuizo_PJ_Curr) / greatest(dp.month_revenue, 1) / 12 * 1000 as scr_Curr_Default_PJ_over_revenue,
	coalesce(Default_PJ_Curr,Prejuizo_PJ_Curr) / case coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) when 0 then 1 else coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) end as scr_Curr_Default_PJ_over_total_debt,
--Limite
	coalesce(Ever_Lim_Cred_PJ,Ever_LimiteCredito_PJ) * 1000 as scr_ever_Lim_Cred_PJ,
	coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) * 1000 as scr_curr_Lim_Cred_PJ,
	coalesce(Lim_Cred_PJ_5M,LimiteCredito_PJ_5M) * 1000 as scr_6M_Lim_Cred_PJ,
	coalesce(Lim_Cred_PJ_11M,LimiteCredito_PJ_11M) * 1000 as scr_12M_Lim_Cred_PJ,
	coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Curr_Lim_Cred_PJ_over_revenue,
	coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) / greatest(dp.amount_requested,1) * 1000 as scr_Curr_Lim_Cred_PJ_over_application_value,
	coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) / case coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) when 0 then 1 else coalesce(long_term_debt_PJ_Curr,CarteiraCredito_PJ_Curr) end as scr_curr_Lim_Cred_PJ_over_total_debt,
	coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) / case divida_atual_PJ when 0 then 1 else divida_atual_PJ end as scr_curr_Lim_Cred_PJ_over_short_term_debt,
	coalesce(max_Lim_Cred_PJ,max_LimiteCredito_PJ) * 1000 as scr_max_Lim_Cred_pj,
	coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) / case coalesce(max_Lim_Cred_PJ,max_LimiteCredito_PJ) when 0 then 1 else coalesce(max_Lim_Cred_PJ,max_LimiteCredito_PJ) end as scr_curr_over_max_Lim_Cred_pj,
	case when coalesce(Ever_Lim_Cred_PJ,Ever_LimiteCredito_PJ) is not null then (coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) = coalesce(max_Lim_Cred_PJ,max_LimiteCredito_PJ,0) and coalesce(max_Lim_Cred_PJ,max_LimiteCredito_PJ,0) > 0)::int end as scr_All_TimeHigh_Lim_Cred_pj,
	(coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) - coalesce(Lim_Cred_PJ_5M,LimiteCredito_PJ_5M)) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_6M_Lim_Cred_PJ_over_revenue,
	(coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) - coalesce(Lim_Cred_PJ_11M,LimiteCredito_PJ_11M)) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_12M_Lim_Cred_PJ_over_revenue,
	case when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) = 0 and coalesce(Lim_Cred_PJ_5M,LimiteCredito_PJ_5M) = 0 then 0 else coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) / case coalesce(Lim_Cred_PJ_5M,LimiteCredito_PJ_5M) when 0 then 1 else coalesce(Lim_Cred_PJ_5M,LimiteCredito_PJ_5M) end - 1 end as scr_var_rel_6M_Lim_Cred_pj,
	case when coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) = 0 and coalesce(Lim_Cred_PJ_11M,LimiteCredito_PJ_11M) = 0 then 0 else coalesce(Lim_Cred_PJ_Curr,LimiteCredito_PJ_Curr) / case coalesce(Lim_Cred_PJ_11M,LimiteCredito_PJ_11M) when 0 then 1 else coalesce(Lim_Cred_PJ_11M,LimiteCredito_PJ_11M) end - 1 end as scr_var_rel_12M_Lim_Cred_pj,
	coalesce(Meses_Aumento_Lim_Cred_PJ,Meses_Aumento_LimiteCredito_PJ) / greatest(coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) - 1,1) as scr_Months_Rise_Lim_Cred_pj_over_total_months,
	coalesce(Meses_Reducao_Lim_Cred_PJ,Meses_Reducao_LimiteCredito_PJ) / greatest(coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) - 1,1) as scr_Months_Dive_Lim_Cred_pj_over_total_months,
	coalesce(months_sth_lim_cred_PJ,months_sth_LimiteCredito_PJ) / greatest(coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) - 1,1) as scr_consecutive_dive_Lim_Cred_pj_over_total_months,
	coalesce(months_hth_lim_cred_PJ,months_hth_LimiteCredito_PJ) / greatest(coalesce(qtd_meses_escopo_PJ,qtd_meses_modalidade_PJ) - 1,1) as scr_consecutive_rise_Lim_Cred_pj_over_total_months,
--Operacoes
	Num_Ops_PJ as scr_Num_Fin_Ops_PJ,
	Num_FIs_PJ as scr_Num_Fin_Inst_PJ,
	First_Relation_FI_PJ as scr_First_Relation_Fin_Inst_PJ,
--------------------------SCR HISTORICO PF
--Divida longo prazo
	case when coalesce(Ever_long_term_debt_pf,Ever_CarteiraCredito_pf) is not null then (coalesce(Ever_long_term_debt_pf,Ever_CarteiraCredito_pf) > 0)::int end as scr_Has_Debt_History_pf, 
	coalesce(Ever_long_term_debt_pf,Ever_CarteiraCredito_pf) * 1000 as scr_ever_total_debt_pf,
	coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) * 1000 as scr_curr_total_debt_pf,
	coalesce(long_term_debt_pf_5M,CarteiraCredito_pf_5M) * 1000 as scr_6M_total_debt_pf,
	coalesce(long_term_debt_pf_11M,CarteiraCredito_pf_11M) * 1000 as scr_12M_total_debt_pf,
	coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) / greatest(dp.month_revenue, 1) / 12 * 1000 as scr_Curr_total_debt_pf_over_revenue,
	coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) / greatest(dp.amount_requested,1) * 1000 as scr_Curr_total_debt_pf_over_application_value,
	coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) / case divida_atual_pf when 0 then 1 else divida_atual_pf end as scr_curr_total_debt_pf_over_short_term_debt,
	coalesce(max_long_term_debt_pf,max_CarteiraCredito_pf) * 1000 as scr_max_total_debt_pf,
	coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) / case coalesce(max_long_term_debt_pf,max_CarteiraCredito_pf) when 0 then 1 else coalesce(max_long_term_debt_pf,max_CarteiraCredito_pf) end as scr_curr_over_max_total_debt_pf,
	case when coalesce(Ever_long_term_debt_pf,Ever_CarteiraCredito_pf) is not null then (coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) = coalesce(max_long_term_debt_pf,max_CarteiraCredito_pf) and coalesce(max_long_term_debt_pf,max_CarteiraCredito_pf) > 0)::int end as scr_All_TimeHigh_total_debt_pf,
	(coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) - coalesce(long_term_debt_pf_5M,CarteiraCredito_pf_5M)) / greatest(dp.month_revenue, 1) / 12 * 1000 as scr_var_abs_6M_total_debt_pf_over_revenue,
	(coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) - coalesce(long_term_debt_pf_11M,CarteiraCredito_pf_11M)) / greatest(dp.month_revenue, 1) / 12 * 1000 as scr_var_abs_12M_total_debt_pf_over_revenue,
	case when coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) = 0 and coalesce(long_term_debt_pf_5M,CarteiraCredito_pf_5M) = 0 then 0 else coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) / case coalesce(long_term_debt_pf_5M,CarteiraCredito_pf_5M) when 0 then 1 else coalesce(long_term_debt_pf_5M,CarteiraCredito_pf_5M) end - 1 end as scr_var_rel_6M_total_debt_pf,
	case when coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) = 0 and coalesce(long_term_debt_pf_11M,CarteiraCredito_pf_11M) = 0 then 0 else coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) / case coalesce(long_term_debt_pf_11M,CarteiraCredito_pf_11M) when 0 then 1 else coalesce(long_term_debt_pf_11M,CarteiraCredito_pf_11M) end - 1 end as scr_var_rel_12M_total_debt_pf,
	coalesce(Meses_Aumento_Dividapf,Meses_Aumento_CarteiraCredito_pf) / greatest(coalesce(qtd_meses_escopo_pf, qtd_meses_modalidade_pf) - 1, 1) as scr_Months_Rise_total_debt_pf_over_total_months,
	coalesce(Meses_Reducao_Dividapf,Meses_Reducao_CarteiraCredito_pf) / greatest(coalesce(qtd_meses_escopo_pf, qtd_meses_modalidade_pf) - 1, 1) as scr_Months_Dive_total_debt_pf_over_total_months,
	coalesce(months_sth_pf,months_sth_CarteiraCredito_pf) / greatest(coalesce(qtd_meses_escopo_pf, qtd_meses_modalidade_pf) - 1, 1) as scr_consecutive_dive_total_debt_pf_over_total_months,
	coalesce(months_hth_pf,months_hth_CarteiraCredito_pf) / greatest(coalesce(qtd_meses_escopo_pf, qtd_meses_modalidade_pf) - 1, 1) as scr_consecutive_rise_total_debt_pf_over_total_months,
	coalesce(Saldo_Amort_Dividapf,Saldo_Amort_CarteiraCredito_pf) * 1000 as scr_total_debt_amortization_pf,
	coalesce(Saldo_Amort_Dividapf,Saldo_Amort_CarteiraCredito_pf) / case coalesce(Meses_Reducao_Dividapf,Meses_Reducao_CarteiraCredito_pf) when 0 then 1 else coalesce(Meses_Reducao_Dividapf,Meses_Reducao_CarteiraCredito_pf) end / greatest(dp.month_revenue, 1) / 12 * 1000 as scr_Avg_Amortization_total_debt_pf_over_revenue,
	coalesce(Saldo_Amort_Dividapf,Saldo_Amort_CarteiraCredito_pf) / case coalesce(Meses_Reducao_Dividapf,Meses_Reducao_CarteiraCredito_pf) when 0 then 1 else coalesce(Meses_Reducao_Dividapf,Meses_Reducao_CarteiraCredito_pf) end / ((@li.pmt)) * 1000 as scr_Avg_Amortization_total_debt_pf_over_loan_PMT,
--Vencido
	Overdue_pf_Curr * 1000 as scr_curr_overdue_debt_pf,
	Max_Overdue_pf * 1000 as scr_max_overdue_debt_pf,
	Months_Overdue_pf / greatest(Qtd_meses_escopo_pf, 1) as scr_Months_Overdue_pf_over_total_months,
	case when Months_Since_Last_Overdue_pf is null or coalesce(Ever_long_term_debt_pf,Ever_CarteiraCredito_pf) = 0 then - 1 else Months_Since_Last_Overdue_pf end as scr_Months_since_last_Overdue_pf,
	Max_Overdue_pf / greatest(dp.month_revenue, 1) / 12 * 1000 as scr_Max_Overdue_pf_over_Revenue,
	Overdue_pf_Curr / greatest(dp.month_revenue, 1) / 12 * 1000 as scr_Curr_Overdue_pf_over_Revenue,
	Overdue_pf_Curr / case Long_Term_Debt_pf_Curr when 0 then 1 else Long_Term_Debt_pf_Curr end as scr_Curr_Overdue_pf_over_total_debt,
--Prejuizo
	coalesce(Default_pf_Curr,Prejuizo_pf_Curr) * 1000 as scr_curr_default_debt_pf,
	coalesce(Max_Default_pf,Max_Prejuizo_pf) * 1000 as scr_max_default_debt_pf,
	coalesce(Months_Default_pf,Meses_Prejuizo_pf) / greatest(coalesce(qtd_meses_escopo_pf,qtd_meses_modalidade_pf),1) as scr_Months_Default_pf_over_total_months,
	case when coalesce(Months_Since_Last_Default_pf,Meses_Desde_Ultimo_Prejuizo_pf) is null or coalesce(Ever_long_term_debt_pf,Ever_CarteiraCredito_pf) = 0 then - 1 else Months_Since_Last_Default_pf end as scr_Months_since_last_Default_pf,
	coalesce(Max_Default_pf,Max_Prejuizo_pf) / greatest(dp.month_revenue, 1) / 12 * 1000 as scr_Max_Default_pf_over_revenue,
	coalesce(Default_pf_Curr,Prejuizo_pf_Curr) / greatest(dp.month_revenue, 1) / 12 * 1000 as scr_Curr_Default_pf_over_revenue,
	coalesce(Default_pf_Curr,Prejuizo_pf_Curr) / case coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) when 0 then 1 else coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) end as scr_Curr_Default_pf_over_total_debt,
--Limite
	coalesce(Ever_Lim_Cred_pf,Ever_LimiteCredito_pf) * 1000 as scr_ever_Lim_Cred_pf,
	coalesce(Lim_Cred_pf_Curr,LimiteCredito_pf_Curr) * 1000 as scr_curr_Lim_Cred_pf,
	coalesce(Lim_Cred_pf_5M,LimiteCredito_pf_5M) * 1000 as scr_6M_Lim_Cred_pf,
	coalesce(Lim_Cred_pf_11M,LimiteCredito_pf_11M) * 1000 as scr_12M_Lim_Cred_pf,
	coalesce(Lim_Cred_pf_Curr,LimiteCredito_pf_Curr) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Curr_Lim_Cred_pf_over_revenue,
	coalesce(Lim_Cred_pf_Curr,LimiteCredito_pf_Curr) / greatest(dp.amount_requested,1) * 1000 as scr_Curr_Lim_Cred_pf_over_application_value,
	coalesce(Lim_Cred_pf_Curr,LimiteCredito_pf_Curr) / case coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) when 0 then 1 else coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) end as scr_curr_Lim_Cred_pf_over_total_debt,
	coalesce(Lim_Cred_pf_Curr,LimiteCredito_pf_Curr) / case divida_atual_pf when 0 then 1 else divida_atual_pf end as scr_curr_Lim_Cred_pf_over_short_term_debt,
	coalesce(max_Lim_Cred_pf,max_LimiteCredito_pf) * 1000 as scr_max_Lim_Cred_pf,
	coalesce(Lim_Cred_pf_Curr,LimiteCredito_pf_Curr) / case coalesce(max_Lim_Cred_pf,max_LimiteCredito_pf) when 0 then 1 else coalesce(max_Lim_Cred_pf,max_LimiteCredito_pf) end as scr_curr_over_max_Lim_Cred_pf,
	case when coalesce(Ever_Lim_Cred_pf,Ever_LimiteCredito_pf) is not null then (coalesce(Lim_Cred_pf_Curr,LimiteCredito_pf_Curr) = coalesce(max_Lim_Cred_pf,max_LimiteCredito_pf,0) and coalesce(max_Lim_Cred_pf,max_LimiteCredito_pf,0) > 0)::int end as scr_All_TimeHigh_Lim_Cred_pf,
	(coalesce(Lim_Cred_pf_Curr,LimiteCredito_pf_Curr) - coalesce(Lim_Cred_pf_5M,LimiteCredito_pf_5M)) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_6M_Lim_Cred_pf_over_revenue,
	(coalesce(Lim_Cred_pf_Curr,LimiteCredito_pf_Curr) - coalesce(Lim_Cred_pf_11M,LimiteCredito_pf_11M)) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_12M_Lim_Cred_pf_over_revenue,
	case when coalesce(Lim_Cred_pf_Curr,LimiteCredito_pf_Curr) = 0 and coalesce(Lim_Cred_pf_5M,LimiteCredito_pf_5M) = 0 then 0 else coalesce(Lim_Cred_pf_Curr,LimiteCredito_pf_Curr) / case coalesce(Lim_Cred_pf_5M,LimiteCredito_pf_5M) when 0 then 1 else coalesce(Lim_Cred_pf_5M,LimiteCredito_pf_5M) end - 1 end as scr_var_rel_6M_Lim_Cred_pf,
	case when coalesce(Lim_Cred_pf_Curr,LimiteCredito_pf_Curr) = 0 and coalesce(Lim_Cred_pf_11M,LimiteCredito_pf_11M) = 0 then 0 else coalesce(Lim_Cred_pf_Curr,LimiteCredito_pf_Curr) / case coalesce(Lim_Cred_pf_11M,LimiteCredito_pf_11M) when 0 then 1 else coalesce(Lim_Cred_pf_11M,LimiteCredito_pf_11M) end - 1 end as scr_var_rel_12M_Lim_Cred_pf,
	coalesce(Meses_Aumento_Lim_Cred_pf,Meses_Aumento_LimiteCredito_pf) / greatest(coalesce(qtd_meses_escopo_pf,qtd_meses_modalidade_pf) - 1,1) as scr_Months_Rise_Lim_Cred_pf_over_total_months,
	coalesce(Meses_Reducao_Lim_Cred_pf,Meses_Reducao_LimiteCredito_pf) / greatest(coalesce(qtd_meses_escopo_pf,qtd_meses_modalidade_pf) - 1,1) as scr_Months_Dive_Lim_Cred_pf_over_total_months,
	coalesce(months_sth_lim_cred_pf,months_sth_LimiteCredito_pf) / greatest(coalesce(qtd_meses_escopo_pf,qtd_meses_modalidade_pf) - 1,1) as scr_consecutive_dive_Lim_Cred_pf_over_total_months,
	coalesce(months_hth_lim_cred_pf,months_hth_LimiteCredito_pf) / greatest(coalesce(qtd_meses_escopo_pf,qtd_meses_modalidade_pf) - 1,1) as scr_consecutive_rise_Lim_Cred_pf_over_total_months,
--Operacoes
	Num_Ops_pf as scr_Num_Fin_Ops_pf,
	Num_FIs_pf as scr_Num_Fin_Inst_pf,
	First_Relation_FI_pf as scr_First_Relation_Fin_Inst_pf,
--------------------------SCR PJ MODALIDADE: INFO GERAL
	divida_atual_pj as scr_short_term_debt_PJ,
	case when emprestimos_PJ_Curr is null then 0 else qtd_meses_modalidade_pj end as scr_number_of_months_in_report_pj,
--------------------------SCR PJ MODALIDADE: EMPRESTIMOS TOTAL
	count_emprestimos_PJ as scr_count_all_loans_PJ,
	Ever_emprestimos_PJ * 1000 as scr_ever_all_loans_PJ,
	emprestimos_PJ_Curr * 1000 as scr_curr_all_loans_PJ,
	emprestimos_PJ_5M * 1000 as scr_6M_all_loans_PJ,
	emprestimos_PJ_11M * 1000 as scr_12M_all_loans_PJ,
	emprestimos_PJ_Curr / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Curr_all_loans_PJ_over_revenue,
	emprestimos_PJ_Curr / case coalesce(long_term_debt_pj_Curr,CarteiraCredito_pj_Curr) when 0 then 1 else coalesce(long_term_debt_pj_Curr,CarteiraCredito_pj_Curr) end as scr_curr_all_loans_PJ_over_total_debt,
	emprestimos_PJ_Curr / case divida_atual_PJ when 0 then 1 else divida_atual_PJ end as scr_curr_all_loans_PJ_over_short_term_debt,
	max_emprestimos_PJ * 1000 as scr_max_all_loans_pj,
	emprestimos_PJ_Curr / case max_emprestimos_PJ when 0 then 1 else max_emprestimos_PJ end as scr_curr_over_max_all_loans_pj,
	case when emprestimos_PJ_Curr is not null then (emprestimos_PJ_Curr = max_emprestimos_PJ and max_emprestimos_PJ > 0)::int end as scr_All_TimeHigh_all_loans_pj,
	(emprestimos_PJ_Curr - emprestimos_PJ_5M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_6M_all_loans_PJ_over_revenue,
	(emprestimos_PJ_Curr - emprestimos_PJ_11M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_12M_all_loans_PJ_over_revenue,
	case when emprestimos_pj_Curr = 0 and emprestimos_pj_5M = 0 then 0 else emprestimos_pj_Curr / case emprestimos_pj_5M when 0 then 1 else emprestimos_pj_5M end - 1 end as scr_var_rel_6M_all_loans_pj,
	case when emprestimos_pj_Curr = 0 and emprestimos_pj_11M = 0 then 0 else emprestimos_pj_Curr / case emprestimos_pj_11M when 0 then 1 else emprestimos_pj_11M end - 1 end as scr_var_rel_12M_all_loans_pj,
	Meses_Aumento_emprestimos_PJ / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_Months_Rise_all_loans_pj_over_total_months,
	Meses_Reducao_emprestimos_PJ / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_Months_Dive_all_loans_pj_over_total_months,
	months_sth_emprestimos_PJ / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_consecutive_dive_all_loans_pj_over_total_months,
	months_hth_emprestimos_PJ / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_consecutive_rise_all_loans_pj_over_total_months,
	Saldo_Amort_emprestimos_PJ * 1000 as scr_all_loans_amortization_PJ,
	Saldo_Amort_emprestimos_PJ / greatest(Meses_Reducao_emprestimos_PJ,1) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Avg_Amortization_all_loans_pj_over_revenue,
	Saldo_Amort_emprestimos_PJ / greatest(Meses_Reducao_emprestimos_PJ,1) / ((@li.pmt)) * 1000 as scr_Avg_Amortization_all_loans_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: EMPRESTIMOS CHEQUE ESPECIAL
	count_emprestimos_cheque_especial_pj as scr_count_overdraft_PJ,
	Ever_emprestimos_cheque_especial_pj * 1000 as scr_ever_overdraft_PJ,
	emprestimos_cheque_especial_pj_Curr * 1000 as scr_curr_overdraft_PJ,
	emprestimos_cheque_especial_pj_5M * 1000 as scr_6M_overdraft_PJ,
	emprestimos_cheque_especial_pj_11M * 1000 as scr_12M_overdraft_PJ,
	emprestimos_cheque_especial_pj_Curr / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Curr_overdraft_PJ_over_revenue,
	emprestimos_cheque_especial_pj_Curr / case coalesce(long_term_debt_pj_Curr,CarteiraCredito_pj_Curr) when 0 then 1 else coalesce(long_term_debt_pj_Curr,CarteiraCredito_pj_Curr) end as scr_curr_overdraft_PJ_over_total_debt,
	emprestimos_cheque_especial_pj_Curr / case divida_atual_PJ when 0 then 1 else divida_atual_PJ end as scr_curr_overdraft_PJ_over_short_term_debt,
	max_emprestimos_cheque_especial_pj * 1000 as scr_max_overdraft_pj,
	emprestimos_cheque_especial_pj_Curr / case max_emprestimos_cheque_especial_pj when 0 then 1 else max_emprestimos_cheque_especial_pj end as scr_curr_over_max_overdraft_pj,
	case when emprestimos_cheque_especial_pj_Curr is not null then (emprestimos_cheque_especial_pj_Curr = max_emprestimos_cheque_especial_pj and max_emprestimos_cheque_especial_pj > 0)::int end as scr_All_TimeHigh_overdraft_pj,
	(emprestimos_cheque_especial_pj_Curr - emprestimos_cheque_especial_pj_5M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_6M_overdraft_PJ_over_revenue,
	(emprestimos_cheque_especial_pj_Curr - emprestimos_cheque_especial_pj_11M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_12M_overdraft_PJ_over_revenue,
	case when emprestimos_cheque_especial_pj_Curr = 0 and emprestimos_cheque_especial_pj_5M = 0 then 0 else emprestimos_cheque_especial_pj_Curr / case emprestimos_cheque_especial_pj_5M when 0 then 1 else emprestimos_cheque_especial_pj_5M end - 1 end as scr_var_rel_6M_overdraft_pj,
	case when emprestimos_cheque_especial_pj_Curr = 0 and emprestimos_cheque_especial_pj_11M = 0 then 0 else emprestimos_cheque_especial_pj_Curr / case emprestimos_cheque_especial_pj_11M when 0 then 1 else emprestimos_cheque_especial_pj_11M end - 1 end as scr_var_rel_12M_overdraft_pj,
	Meses_Aumento_emprestimos_cheque_especial_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_Months_Rise_overdraft_pj_over_total_months,
	Meses_Reducao_emprestimos_cheque_especial_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_Months_Dive_overdraft_pj_over_total_months,
	months_sth_emprestimos_cheque_especial_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_consecutive_dive_overdraft_pj_over_total_months,
	months_hth_emprestimos_cheque_especial_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_consecutive_rise_overdraft_pj_over_total_months,
	Saldo_Amort_emprestimos_cheque_especial_pj * 1000 as scr_overdraft_amortization_PJ,
	Saldo_Amort_emprestimos_cheque_especial_pj / greatest(Meses_Reducao_emprestimos_cheque_especial_pj,1) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Avg_Amortization_overdraft_pj_over_revenue,
	Saldo_Amort_emprestimos_cheque_especial_pj / greatest(Meses_Reducao_emprestimos_cheque_especial_pj,1) / ((@li.pmt)) * 1000 as scr_Avg_Amortization_overdraft_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: EMPRESTIMOS CAPITAL DE GIRO LONGO PRAZO
	count_emprestimos_capital_giro_pj as scr_count_working_capital_PJ,
	Ever_emprestimos_capital_giro_pj * 1000 as scr_ever_working_capital_PJ,
	emprestimos_capital_giro_pj_Curr * 1000 as scr_curr_working_capital_PJ,
	emprestimos_capital_giro_pj_5M * 1000 as scr_6M_working_capital_PJ,
	emprestimos_capital_giro_pj_11M * 1000 as scr_12M_working_capital_PJ,
	emprestimos_capital_giro_pj_Curr / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Curr_working_capital_PJ_over_revenue,
	emprestimos_capital_giro_pj_Curr / case coalesce(long_term_debt_pj_Curr,CarteiraCredito_pj_Curr) when 0 then 1 else coalesce(long_term_debt_pj_Curr,CarteiraCredito_pj_Curr) end as scr_curr_working_capital_PJ_over_total_debt,
	emprestimos_capital_giro_pj_Curr / case divida_atual_PJ when 0 then 1 else divida_atual_PJ end as scr_curr_working_capital_PJ_over_short_term_debt,
	max_emprestimos_capital_giro_pj * 1000 as scr_max_working_capital_pj,
	emprestimos_capital_giro_pj_Curr / case max_emprestimos_capital_giro_pj when 0 then 1 else max_emprestimos_capital_giro_pj end as scr_curr_over_max_working_capital_pj,
	case when emprestimos_capital_giro_pj_Curr is not null then (emprestimos_capital_giro_pj_Curr = max_emprestimos_capital_giro_pj and max_emprestimos_capital_giro_pj > 0)::int end as scr_All_TimeHigh_working_capital_pj,
	(emprestimos_capital_giro_pj_Curr - emprestimos_capital_giro_pj_5M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_6M_working_capital_PJ_over_revenue,
	(emprestimos_capital_giro_pj_Curr - emprestimos_capital_giro_pj_11M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_12M_working_capital_PJ_over_revenue,	
	case when emprestimos_capital_giro_pj_Curr = 0 and emprestimos_capital_giro_pj_5M = 0 then 0 else emprestimos_capital_giro_pj_Curr / case emprestimos_capital_giro_pj_5M when 0 then 1 else emprestimos_capital_giro_pj_5M end - 1 end as scr_var_rel_6M_working_capital_pj,
	case when emprestimos_capital_giro_pj_Curr = 0 and emprestimos_capital_giro_pj_11M = 0 then 0 else emprestimos_capital_giro_pj_Curr / case emprestimos_capital_giro_pj_11M when 0 then 1 else emprestimos_capital_giro_pj_11M end - 1 end as scr_var_rel_12M_working_capital_pj,
	Meses_Aumento_emprestimos_capital_giro_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_Months_Rise_working_capital_pj_over_total_months,
	Meses_Reducao_emprestimos_capital_giro_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_Months_Dive_working_capital_pj_over_total_months,
	months_sth_emprestimos_capital_giro_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_consecutive_dive_working_capital_pj_over_total_months,
	months_hth_emprestimos_capital_giro_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_consecutive_rise_working_capital_pj_over_total_months,
	Saldo_Amort_emprestimos_capital_giro_pj * 1000 as scr_working_capital_amortization_PJ,
	Saldo_Amort_emprestimos_capital_giro_pj / greatest(Meses_Reducao_emprestimos_capital_giro_pj,1) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Avg_Amortization_working_capital_pj_over_revenue,
	Saldo_Amort_emprestimos_capital_giro_pj / greatest(Meses_Reducao_emprestimos_capital_giro_pj,1) / ((@li.pmt)) * 1000 as scr_Avg_Amortization_working_capital_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: EMPRESTIMOS CAPITAL DE GIRO CURTO PRAZO
	count_emprestimos_giro_curto_pj as scr_count_short_term_work_capital_PJ,
	Ever_emprestimos_giro_curto_pj * 1000 as scr_ever_short_term_work_capital_PJ,
	emprestimos_giro_curto_pj_Curr * 1000 as scr_curr_short_term_work_capital_PJ,
--------------------------SCR PJ MODALIDADE: EMPRESTIMOS CARTÃƒO DE CRÃ‰DITO
	count_emprestimos_cartao_credito_pj as scr_count_credit_card_PJ,
	Ever_emprestimos_cartao_credito_pj * 1000 as scr_ever_credit_card_PJ,
	emprestimos_cartao_credito_pj_Curr * 1000 as scr_curr_credit_card_PJ,
	emprestimos_cartao_credito_pj_5M * 1000 as scr_6M_credit_card_PJ,
	emprestimos_cartao_credito_pj_11M * 1000 as scr_12M_credit_card_PJ,
	emprestimos_cartao_credito_pj_Curr / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Curr_credit_card_PJ_over_revenue,
	emprestimos_cartao_credito_pj_Curr / case coalesce(long_term_debt_pj_Curr,CarteiraCredito_pj_Curr) when 0 then 1 else coalesce(long_term_debt_pj_Curr,CarteiraCredito_pj_Curr) end as scr_curr_credit_card_PJ_over_total_debt,
	emprestimos_cartao_credito_pj_Curr / case divida_atual_PJ when 0 then 1 else divida_atual_PJ end as scr_curr_credit_card_PJ_over_short_term_debt,
	max_emprestimos_cartao_credito_pj * 1000 as scr_max_credit_card_pj,
	emprestimos_cartao_credito_pj_Curr / case max_emprestimos_cartao_credito_pj when 0 then 1 else max_emprestimos_cartao_credito_pj end as scr_curr_over_max_credit_card_pj,
	case when emprestimos_cartao_credito_pj_Curr is not null then (emprestimos_cartao_credito_pj_Curr = max_emprestimos_cartao_credito_pj and max_emprestimos_cartao_credito_pj > 0)::int end as scr_All_TimeHigh_credit_card_pj,
	(emprestimos_cartao_credito_pj_Curr - emprestimos_cartao_credito_pj_5M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_6M_credit_card_PJ_over_revenue,
	(emprestimos_cartao_credito_pj_Curr - emprestimos_cartao_credito_pj_11M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_12M_credit_card_PJ_over_revenue,
	case when emprestimos_cartao_credito_pj_Curr = 0 and emprestimos_cartao_credito_pj_5M = 0 then 0 else emprestimos_cartao_credito_pj_Curr / case emprestimos_cartao_credito_pj_5M when 0 then 1 else emprestimos_cartao_credito_pj_5M end - 1 end as scr_var_rel_6M_credit_card_pj,
	case when emprestimos_cartao_credito_pj_Curr = 0 and emprestimos_cartao_credito_pj_11M = 0 then 0 else emprestimos_cartao_credito_pj_Curr / case emprestimos_cartao_credito_pj_11M when 0 then 1 else emprestimos_cartao_credito_pj_11M end - 1 end as scr_var_rel_12M_credit_card_pj,
	Meses_Aumento_emprestimos_cartao_credito_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_Months_Rise_credit_card_pj_over_total_months,
	Meses_Reducao_emprestimos_cartao_credito_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_Months_Dive_credit_card_pj_over_total_months,
	months_sth_emprestimos_cartao_credito_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_consecutive_dive_credit_card_pj_over_total_months,
	months_hth_emprestimos_cartao_credito_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_consecutive_rise_credit_card_pj_over_total_months,
	Saldo_Amort_emprestimos_cartao_credito_pj * 1000 as scr_credit_card_amortization_PJ,
	Saldo_Amort_emprestimos_cartao_credito_pj / greatest(Meses_Reducao_emprestimos_cartao_credito_pj,1) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Avg_Amortization_credit_card_pj_over_revenue,
	Saldo_Amort_emprestimos_cartao_credito_pj / greatest(Meses_Reducao_emprestimos_cartao_credito_pj,1) / ((@li.pmt)) * 1000 as scr_Avg_Amortization_credit_card_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: EMPRESTIMOS CONTA GARANTIDA
	count_emprestimos_conta_garantida_pj as scr_count_light_overdraft_PJ,
	Ever_emprestimos_conta_garantida_pj * 1000 as scr_ever_light_overdraft_PJ,
	emprestimos_conta_garantida_pj_Curr * 1000 as scr_curr_light_overdraft_PJ,
--------------------------SCR PJ MODALIDADE: EMPRESTIMOS OUTROS EMPRÃ‰STIMOS
	count_emprestimos_outros_emprestimos_pj as scr_count_other_loans_PJ,
	Ever_emprestimos_outros_emprestimos_pj * 1000 as scr_ever_other_loans_PJ,
	emprestimos_outros_emprestimos_pj_Curr * 1000 as scr_curr_other_loans_PJ,
--------------------------SCR PJ MODALIDADE: EMPRESTIMOS CAPITAL DE GIRO ROTATIVO
	count_emprestimos_giro_rotativo_pj as scr_count_overdraft_work_capital_PJ,
	Ever_emprestimos_giro_rotativo_pj * 1000 as scr_ever_overdraft_work_capital_PJ,
	emprestimos_giro_rotativo_pj_Curr * 1000 as scr_curr_overdraft_work_capital_PJ,
--------------------------SCR PJ MODALIDADE: EMPRESTIMOS CONTA GARANTIDA + CHEQUE ESPECIAL
	count_emprestimos_conta_garantida_cheque_especial_pj as scr_count_old_overdraft_PJ,
	Ever_emprestimos_conta_garantida_cheque_especial_pj * 1000 as scr_ever_old_overdraft_PJ,
	emprestimos_conta_garantida_cheque_especial_pj_Curr * 1000 as scr_curr_old_overdraft_PJ,
	emprestimos_conta_garantida_cheque_especial_pj_5M * 1000 as scr_6M_old_overdraft_PJ,
	emprestimos_conta_garantida_cheque_especial_pj_11M * 1000 as scr_12M_old_overdraft_PJ,
	emprestimos_conta_garantida_cheque_especial_pj_Curr / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Curr_old_overdraft_PJ_over_revenue,
	emprestimos_conta_garantida_cheque_especial_pj_Curr / case coalesce(long_term_debt_pj_Curr,CarteiraCredito_pj_Curr) when 0 then 1 else coalesce(long_term_debt_pj_Curr,CarteiraCredito_pj_Curr) end as scr_curr_old_overdraft_PJ_over_total_debt,
	emprestimos_conta_garantida_cheque_especial_pj_Curr / case divida_atual_PJ when 0 then 1 else divida_atual_PJ end as scr_curr_old_overdraft_PJ_over_short_term_debt,
	max_emprestimos_conta_garantida_cheque_especial_pj * 1000 as scr_max_old_overdraft_pj,
	emprestimos_conta_garantida_cheque_especial_pj_Curr / case max_emprestimos_conta_garantida_cheque_especial_pj when 0 then 1 else max_emprestimos_conta_garantida_cheque_especial_pj end as scr_curr_over_max_old_overdraft_pj,
	case when emprestimos_conta_garantida_cheque_especial_pj_Curr is not null then (emprestimos_conta_garantida_cheque_especial_pj_Curr = max_emprestimos_conta_garantida_cheque_especial_pj and max_emprestimos_conta_garantida_cheque_especial_pj > 0)::int end as scr_All_TimeHigh_old_overdraft_pj,
	(emprestimos_conta_garantida_cheque_especial_pj_Curr - emprestimos_conta_garantida_cheque_especial_pj_5M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_6M_old_overdraft_PJ_over_revenue,
	(emprestimos_conta_garantida_cheque_especial_pj_Curr - emprestimos_conta_garantida_cheque_especial_pj_11M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_12M_old_overdraft_PJ_over_revenue,
	case when emprestimos_conta_garantida_cheque_especial_pj_Curr = 0 and emprestimos_conta_garantida_cheque_especial_pj_5M = 0 then 0 else emprestimos_conta_garantida_cheque_especial_pj_Curr / case emprestimos_conta_garantida_cheque_especial_pj_5M when 0 then 1 else emprestimos_conta_garantida_cheque_especial_pj_5M end - 1 end as scr_var_rel_6M_old_overdraft_pj,
	case when emprestimos_conta_garantida_cheque_especial_pj_Curr = 0 and emprestimos_conta_garantida_cheque_especial_pj_11M = 0 then 0 else emprestimos_conta_garantida_cheque_especial_pj_Curr / case emprestimos_conta_garantida_cheque_especial_pj_11M when 0 then 1 else emprestimos_conta_garantida_cheque_especial_pj_11M end - 1 end as scr_var_rel_12M_old_overdraft_pj,
	Meses_Aumento_emprestimos_conta_garantida_cheque_especial_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_Months_Rise_old_overdraft_pj_over_total_months,
	Meses_Reducao_emprestimos_conta_garantida_cheque_especial_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_Months_Dive_old_overdraft_pj_over_total_months,
	months_sth_emprestimos_conta_garantida_cheque_especial_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_consecutive_dive_old_overdraft_pj_over_total_months,
	months_hth_emprestimos_conta_garantida_cheque_especial_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_consecutive_rise_old_overdraft_pj_over_total_months,
	Saldo_Amort_emprestimos_conta_garantida_cheque_especial_pj * 1000 as scr_old_overdraft_amortization_PJ,
	Saldo_Amort_emprestimos_conta_garantida_cheque_especial_pj / greatest(Meses_Reducao_emprestimos_conta_garantida_cheque_especial_pj,1) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Avg_Amortization_old_overdraft_pj_over_revenue,
	Saldo_Amort_emprestimos_conta_garantida_cheque_especial_pj / greatest(Meses_Reducao_emprestimos_conta_garantida_cheque_especial_pj,1) / ((@li.pmt)) * 1000 as scr_Avg_Amortization_old_overdraft_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: OUTROS CREDITOS
	count_outroscreditos_pj as scr_count_other_credits_PJ,
	Ever_outroscreditos_pj * 1000 as scr_ever_other_credits_PJ,
	outroscreditos_pj_Curr * 1000 as scr_curr_other_credits_PJ,
--------------------------SCR PJ MODALIDADE: DESCONTO DE DIREITOS CREDITORIOS
	count_desconto_pj as scr_count_cash_advance_PJ,
	Ever_desconto_pj * 1000 as scr_ever_cash_advance_PJ,
	desconto_pj_Curr * 1000 as scr_curr_cash_advance_PJ,
	desconto_pj_5M * 1000 as scr_6M_cash_advance_PJ,
	desconto_pj_11M * 1000 as scr_12M_cash_advance_PJ,
	desconto_pj_Curr / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Curr_cash_advance_PJ_over_revenue,
	desconto_pj_Curr / case coalesce(long_term_debt_pj_Curr,CarteiraCredito_pj_Curr) when 0 then 1 else coalesce(long_term_debt_pj_Curr,CarteiraCredito_pj_Curr) end as scr_curr_cash_advance_PJ_over_total_debt,
	desconto_pj_Curr / case divida_atual_PJ when 0 then 1 else divida_atual_PJ end as scr_curr_cash_advance_PJ_over_short_term_debt,
	max_desconto_pj * 1000 as scr_max_cash_advance_pj,
	desconto_pj_Curr / case max_desconto_pj when 0 then 1 else max_desconto_pj end as scr_curr_over_max_cash_advance_pj,
	case when desconto_pj_Curr is not null then (desconto_pj_Curr = max_desconto_pj and max_desconto_pj > 0)::int end as scr_All_TimeHigh_cash_advance_pj,
	(desconto_pj_Curr - desconto_pj_5M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_6M_cash_advance_PJ_over_revenue,
	(desconto_pj_Curr - desconto_pj_11M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_12M_cash_advance_PJ_over_revenue,
	case when desconto_pj_Curr = 0 and desconto_pj_5M = 0 then 0 else desconto_pj_Curr / case desconto_pj_5M when 0 then 1 else desconto_pj_5M end - 1 end as scr_var_rel_6M_cash_advance_pj,
	case when desconto_pj_Curr = 0 and desconto_pj_11M = 0 then 0 else desconto_pj_Curr / case desconto_pj_11M when 0 then 1 else desconto_pj_11M end - 1 end as scr_var_rel_12M_cash_advance_pj,
	Meses_Aumento_desconto_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_Months_Rise_cash_advance_pj_over_total_months,
	Meses_Reducao_desconto_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_Months_Dive_cash_advance_pj_over_total_months,
	months_sth_desconto_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_consecutive_dive_cash_advance_pj_over_total_months,
	months_hth_desconto_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_consecutive_rise_cash_advance_pj_over_total_months,
	Saldo_Amort_desconto_pj * 1000 as scr_cash_advance_amortization_PJ,
	Saldo_Amort_desconto_pj / greatest(Meses_Reducao_desconto_pj,1) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Avg_Amortization_cash_advance_pj_over_revenue,
	Saldo_Amort_desconto_pj / greatest(Meses_Reducao_desconto_pj,1) / ((@li.pmt)) * 1000 as scr_Avg_Amortization_cash_advance_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: FINANCIAMENTOS
	count_financiamentos_pj as scr_count_all_financings_PJ,
	Ever_financiamentos_pj * 1000 as scr_ever_all_financings_PJ,
	financiamentos_pj_Curr * 1000 as scr_curr_all_financings_PJ,
	financiamentos_pj_5M * 1000 as scr_6M_all_financings_PJ,
	financiamentos_pj_11M * 1000 as scr_12M_all_financings_PJ,
	financiamentos_pj_Curr / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Curr_all_financings_PJ_over_revenue,
	financiamentos_pj_Curr / case coalesce(long_term_debt_pj_Curr,CarteiraCredito_pj_Curr) when 0 then 1 else coalesce(long_term_debt_pj_Curr,CarteiraCredito_pj_Curr) end as scr_curr_all_financings_PJ_over_total_debt,
	financiamentos_pj_Curr / case divida_atual_PJ when 0 then 1 else divida_atual_PJ end as scr_curr_all_financings_PJ_over_short_term_debt,
	max_financiamentos_pj * 1000 as scr_max_all_financings_pj,
	financiamentos_pj_Curr / case max_financiamentos_pj when 0 then 1 else max_financiamentos_pj end as scr_curr_over_max_all_financings_pj,
	case when financiamentos_pj_Curr is not null then (financiamentos_pj_Curr = max_financiamentos_pj and max_financiamentos_pj > 0)::int end as scr_All_TimeHigh_all_financings_pj,
	(financiamentos_pj_Curr - financiamentos_pj_5M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_6M_all_financings_PJ_over_revenue,
	(financiamentos_pj_Curr - financiamentos_pj_11M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_12M_all_financings_PJ_over_revenue,
	case when financiamentos_pj_Curr = 0 and financiamentos_pj_5M = 0 then 0 else financiamentos_pj_Curr / case financiamentos_pj_5M when 0 then 1 else financiamentos_pj_5M end - 1 end as scr_var_rel_6M_all_financings_pj,
	case when financiamentos_pj_Curr = 0 and financiamentos_pj_11M = 0 then 0 else financiamentos_pj_Curr / case financiamentos_pj_11M when 0 then 1 else financiamentos_pj_11M end - 1 end as scr_var_rel_12M_all_financings_pj,
	Meses_Aumento_financiamentos_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_Months_Rise_all_financings_pj_over_total_months,
	Meses_Reducao_financiamentos_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_Months_Dive_all_financings_pj_over_total_months,
	months_sth_financiamentos_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_consecutive_dive_all_financings_pj_over_total_months,
	months_hth_financiamentos_pj / greatest(qtd_meses_modalidade_PJ - 1, 1) as scr_consecutive_rise_all_financings_pj_over_total_months,
	Saldo_Amort_financiamentos_pj * 1000 as scr_all_financings_amortization_PJ,
	Saldo_Amort_financiamentos_pj / greatest(Meses_Reducao_financiamentos_pj,1) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Avg_Amortization_all_financings_pj_over_revenue,
	Saldo_Amort_financiamentos_pj / greatest(Meses_Reducao_financiamentos_pj,1) / ((@li.pmt)) * 1000 as scr_Avg_Amortization_all_financings_pj_over_loan_PMT,
--------------------------SCR PJ MODALIDADE: FINANCIAMENTOS + OUTROS FINANCIAMENTOS
	count_todos_financiamentos_pj as scr_count_all_plus_other_financings_PJ,
	Ever_todos_financiamentos_pj * 1000 as scr_ever_all_plus_other_financings_PJ,
	todos_financiamentos_pj_Curr * 1000 as scr_curr_all_plus_other_financings_PJ,
--------------------------SCR PJ MODALIDADE: ADIANTAMENTO A DEPOSITANTES
	count_adiantamentos_pj as scr_count_deposit_advance_PJ,
	Ever_adiantamentos_pj * 1000 as scr_ever_deposit_advance_PJ,
	adiantamentos_pj_Curr * 1000 as scr_curr_deposit_advance_PJ,
--------------------------SCR PJ MODALIDADE: OUTROS FINANCIAMENTOS
	count_outrosfinanciamentos_pj as scr_count_other_financings_PJ,
	Ever_outrosfinanciamentos_pj * 1000 as scr_ever_other_financings_PJ,
	outrosfinanciamentos_pj_Curr * 1000 as scr_curr_other_financings_PJ,
--------------------------SCR PJ MODALIDADE: LIMITE DE CREDITO CURTO PRAZO
	count_limitecredito_curto_pj as scr_count_st_credit_limit_PJ,
	Ever_limitecredito_curto_pj * 1000 as scr_ever_st_credit_limit_PJ,
	limitecredito_curto_pj_Curr * 1000 as scr_curr_st_credit_limit_PJ,
--------------------------SCR PJ MODALIDADE: LIMITE DE CREDITO LONGO PRAZO
	count_limitecredito_longo_pj as scr_count_lt_credit_limit_PJ,
	Ever_limitecredito_longo_pj * 1000 as scr_ever_lt_credit_limit_PJ,
	limitecredito_longo_pj_Curr * 1000 as scr_curr_lt_credit_limit_PJ,
--------------------------SCR PF MODALIDADE: INFO GERAL
	divida_atual_pf as scr_short_term_debt_PF,
	case when emprestimos_pf_Curr is null then 0 else qtd_meses_modalidade_pf end as scr_number_of_months_in_report_pf,
--------------------------SCR PF MODALIDADE: EMPRESTIMOS TOTAL
	count_emprestimos_pf as scr_count_all_loans_pf,
	Ever_emprestimos_pf * 1000 as scr_ever_all_loans_pf,
	emprestimos_pf_Curr * 1000 as scr_curr_all_loans_pf,
	emprestimos_pf_5M * 1000 as scr_6M_all_loans_pf,
	emprestimos_pf_11M * 1000 as scr_12M_all_loans_pf,
	emprestimos_pf_Curr / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Curr_all_loans_pf_over_revenue,
	emprestimos_pf_Curr / case coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) when 0 then 1 else coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) end as scr_curr_all_loans_pf_over_total_debt,
	emprestimos_pf_Curr / case divida_atual_pf when 0 then 1 else divida_atual_pf end as scr_curr_all_loans_pf_over_short_term_debt,
	max_emprestimos_pf * 1000 as scr_max_all_loans_pf,
	emprestimos_pf_Curr / case max_emprestimos_pf when 0 then 1 else max_emprestimos_pf end as scr_curr_over_max_all_loans_pf,
	case when emprestimos_pf_Curr is not null then (emprestimos_pf_Curr = max_emprestimos_pf and max_emprestimos_pf > 0)::int end as scr_All_TimeHigh_all_loans_pf,
	(emprestimos_pf_Curr - emprestimos_pf_5M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_6M_all_loans_pf_over_revenue,
	(emprestimos_pf_Curr - emprestimos_pf_11M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_12M_all_loans_pf_over_revenue,
	case when emprestimos_pf_Curr = 0 and emprestimos_pf_5M = 0 then 0 else emprestimos_pf_Curr / case emprestimos_pf_5M when 0 then 1 else emprestimos_pf_5M end - 1 end as scr_var_rel_6M_all_loans_pf,
	case when emprestimos_pf_Curr = 0 and emprestimos_pf_11M = 0 then 0 else emprestimos_pf_Curr / case emprestimos_pf_11M when 0 then 1 else emprestimos_pf_11M end - 1 end as scr_var_rel_12M_all_loans_pf,
	Meses_Aumento_emprestimos_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_Months_Rise_all_loans_pf_over_total_months,
	Meses_Reducao_emprestimos_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_Months_Dive_all_loans_pf_over_total_months,
	months_sth_emprestimos_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_consecutive_dive_all_loans_pf_over_total_months,
	months_hth_emprestimos_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_consecutive_rise_all_loans_pf_over_total_months,
	Saldo_Amort_emprestimos_pf * 1000 as scr_all_loans_amortization_pf,
	Saldo_Amort_emprestimos_pf / greatest(Meses_Reducao_emprestimos_pf,1) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Avg_Amortization_all_loans_pf_over_revenue,
	Saldo_Amort_emprestimos_pf / greatest(Meses_Reducao_emprestimos_pf,1) / ((@li.pmt)) * 1000 as scr_Avg_Amortization_all_loans_pf_over_loan_PMT,
--------------------------SCR PF MODALIDADE: EMPRESTIMOS CHEQUE ESPECIAL
	count_emprestimos_cheque_especial_pf as scr_count_overdraft_pf,
	Ever_emprestimos_cheque_especial_pf * 1000 as scr_ever_overdraft_pf,
	emprestimos_cheque_especial_pf_Curr * 1000 as scr_curr_overdraft_pf,
	emprestimos_cheque_especial_pf_5M * 1000 as scr_6M_overdraft_pf,
	emprestimos_cheque_especial_pf_11M * 1000 as scr_12M_overdraft_pf,
	emprestimos_cheque_especial_pf_Curr / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Curr_overdraft_pf_over_revenue,
	emprestimos_cheque_especial_pf_Curr / case coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) when 0 then 1 else coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) end as scr_curr_overdraft_pf_over_total_debt,
	emprestimos_cheque_especial_pf_Curr / case divida_atual_pf when 0 then 1 else divida_atual_pf end as scr_curr_overdraft_pf_over_short_term_debt,
	max_emprestimos_cheque_especial_pf * 1000 as scr_max_overdraft_pf,
	emprestimos_cheque_especial_pf_Curr / case max_emprestimos_cheque_especial_pf when 0 then 1 else max_emprestimos_cheque_especial_pf end as scr_curr_over_max_overdraft_pf,
	case when emprestimos_cheque_especial_pf_Curr is not null then (emprestimos_cheque_especial_pf_Curr = max_emprestimos_cheque_especial_pf and max_emprestimos_cheque_especial_pf > 0)::int end as scr_All_TimeHigh_overdraft_pf,
	(emprestimos_cheque_especial_pf_Curr - emprestimos_cheque_especial_pf_5M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_6M_overdraft_pf_over_revenue,
	(emprestimos_cheque_especial_pf_Curr - emprestimos_cheque_especial_pf_11M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_12M_overdraft_pf_over_revenue,
	case when emprestimos_cheque_especial_pf_Curr = 0 and emprestimos_cheque_especial_pf_5M = 0 then 0 else emprestimos_cheque_especial_pf_Curr / case emprestimos_cheque_especial_pf_5M when 0 then 1 else emprestimos_cheque_especial_pf_5M end - 1 end as scr_var_rel_6M_overdraft_pf,
	case when emprestimos_cheque_especial_pf_Curr = 0 and emprestimos_cheque_especial_pf_11M = 0 then 0 else emprestimos_cheque_especial_pf_Curr / case emprestimos_cheque_especial_pf_11M when 0 then 1 else emprestimos_cheque_especial_pf_11M end - 1 end as scr_var_rel_12M_overdraft_pf,
	Meses_Aumento_emprestimos_cheque_especial_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_Months_Rise_overdraft_pf_over_total_months,
	Meses_Reducao_emprestimos_cheque_especial_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_Months_Dive_overdraft_pf_over_total_months,
	months_sth_emprestimos_cheque_especial_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_consecutive_dive_overdraft_pf_over_total_months,
	months_hth_emprestimos_cheque_especial_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_consecutive_rise_overdraft_pf_over_total_months,
	Saldo_Amort_emprestimos_cheque_especial_pf * 1000 as scr_overdraft_amortization_pf,
	Saldo_Amort_emprestimos_cheque_especial_pf / greatest(Meses_Reducao_emprestimos_cheque_especial_pf,1) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Avg_Amortization_overdraft_pf_over_revenue,
	Saldo_Amort_emprestimos_cheque_especial_pf / greatest(Meses_Reducao_emprestimos_cheque_especial_pf,1) / ((@li.pmt)) * 1000 as scr_Avg_Amortization_overdraft_pf_over_loan_PMT,
--------------------------SCR PF MODALIDADE: EMPRESTIMOS CARTÃƒO DE CRÃ‰DITO
	count_emprestimos_cartao_credito_pf as scr_count_credit_card_pf,
	Ever_emprestimos_cartao_credito_pf * 1000 as scr_ever_credit_card_pf,
	emprestimos_cartao_credito_pf_Curr * 1000 as scr_curr_credit_card_pf,
	emprestimos_cartao_credito_pf_5M * 1000 as scr_6M_credit_card_pf,
	emprestimos_cartao_credito_pf_11M * 1000 as scr_12M_credit_card_pf,
	emprestimos_cartao_credito_pf_Curr / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Curr_credit_card_pf_over_revenue,
	emprestimos_cartao_credito_pf_Curr / case coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) when 0 then 1 else coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) end as scr_curr_credit_card_pf_over_total_debt,
	emprestimos_cartao_credito_pf_Curr / case divida_atual_pf when 0 then 1 else divida_atual_pf end as scr_curr_credit_card_pf_over_short_term_debt,
	max_emprestimos_cartao_credito_pf * 1000 as scr_max_credit_card_pf,
	emprestimos_cartao_credito_pf_Curr / case max_emprestimos_cartao_credito_pf when 0 then 1 else max_emprestimos_cartao_credito_pf end as scr_curr_over_max_credit_card_pf,
	case when emprestimos_cartao_credito_pf_Curr is not null then (emprestimos_cartao_credito_pf_Curr = max_emprestimos_cartao_credito_pf and max_emprestimos_cartao_credito_pf > 0)::int end as scr_All_TimeHigh_credit_card_pf,
	(emprestimos_cartao_credito_pf_Curr - emprestimos_cartao_credito_pf_5M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_6M_credit_card_pf_over_revenue,
	(emprestimos_cartao_credito_pf_Curr - emprestimos_cartao_credito_pf_11M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_12M_credit_card_pf_over_revenue,
	case when emprestimos_cartao_credito_pf_Curr = 0 and emprestimos_cartao_credito_pf_5M = 0 then 0 else emprestimos_cartao_credito_pf_Curr / case emprestimos_cartao_credito_pf_5M when 0 then 1 else emprestimos_cartao_credito_pf_5M end - 1 end as scr_var_rel_6M_credit_card_pf,
	case when emprestimos_cartao_credito_pf_Curr = 0 and emprestimos_cartao_credito_pf_11M = 0 then 0 else emprestimos_cartao_credito_pf_Curr / case emprestimos_cartao_credito_pf_11M when 0 then 1 else emprestimos_cartao_credito_pf_11M end - 1 end as scr_var_rel_12M_credit_card_pf,
	Meses_Aumento_emprestimos_cartao_credito_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_Months_Rise_credit_card_pf_over_total_months,
	Meses_Reducao_emprestimos_cartao_credito_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_Months_Dive_credit_card_pf_over_total_months,
	months_sth_emprestimos_cartao_credito_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_consecutive_dive_credit_card_pf_over_total_months,
	months_hth_emprestimos_cartao_credito_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_consecutive_rise_credit_card_pf_over_total_months,
	Saldo_Amort_emprestimos_cartao_credito_pf * 1000 as scr_credit_card_amortization_pf,
	Saldo_Amort_emprestimos_cartao_credito_pf / greatest(Meses_Reducao_emprestimos_cartao_credito_pf,1) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Avg_Amortization_credit_card_pf_over_revenue,
	Saldo_Amort_emprestimos_cartao_credito_pf / greatest(Meses_Reducao_emprestimos_cartao_credito_pf,1) / ((@li.pmt)) * 1000 as scr_Avg_Amortization_credit_card_pf_over_loan_PMT,
--------------------------SCR PF MODALIDADE: EMPRESTIMOS CRÃ‰DITO PRA PJ
	count_emprestimos_credito_pPJ_pf as scr_count_credit_for_company_pf,
	Ever_emprestimos_credito_pPJ_pf * 1000 as scr_ever_credit_for_company_pf,
	emprestimos_credito_pPJ_pf_Curr * 1000 as scr_curr_credit_for_company_pf,
	emprestimos_credito_pPJ_pf_5M * 1000 as scr_6M_credit_for_company_pf,
	emprestimos_credito_pPJ_pf_11M * 1000 as scr_12M_credit_for_company_pf,
	emprestimos_credito_pPJ_pf_Curr / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Curr_credit_for_company_pf_over_revenue,
	emprestimos_credito_pPJ_pf_Curr / case coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) when 0 then 1 else coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) end as scr_curr_credit_for_company_pf_over_total_debt,
	emprestimos_credito_pPJ_pf_Curr / case divida_atual_pf when 0 then 1 else divida_atual_pf end as scr_curr_credit_for_company_pf_over_short_term_debt,
	max_emprestimos_credito_pPJ_pf * 1000 as scr_max_credit_for_company_pf,
	emprestimos_credito_pPJ_pf_Curr / case max_emprestimos_credito_pPJ_pf when 0 then 1 else max_emprestimos_credito_pPJ_pf end as scr_curr_over_max_credit_for_company_pf,
	case when emprestimos_credito_pPJ_pf_Curr is not null then (emprestimos_credito_pPJ_pf_Curr = max_emprestimos_credito_pPJ_pf and max_emprestimos_credito_pPJ_pf > 0)::int end as scr_All_TimeHigh_credit_for_company_pf,
	(emprestimos_credito_pPJ_pf_Curr - emprestimos_credito_pPJ_pf_5M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_6M_credit_for_company_pf_over_revenue,
	(emprestimos_credito_pPJ_pf_Curr - emprestimos_credito_pPJ_pf_11M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_12M_credit_for_company_pf_over_revenue,
	case when emprestimos_credito_pPJ_pf_Curr = 0 and emprestimos_credito_pPJ_pf_5M = 0 then 0 else emprestimos_credito_pPJ_pf_Curr / case emprestimos_credito_pPJ_pf_5M when 0 then 1 else emprestimos_credito_pPJ_pf_5M end - 1 end as scr_var_rel_6M_credit_for_company_pf,
	case when emprestimos_credito_pPJ_pf_Curr = 0 and emprestimos_credito_pPJ_pf_11M = 0 then 0 else emprestimos_credito_pPJ_pf_Curr / case emprestimos_credito_pPJ_pf_11M when 0 then 1 else emprestimos_credito_pPJ_pf_11M end - 1 end as scr_var_rel_12M_credit_for_company_pf,
	Meses_Aumento_emprestimos_credito_pPJ_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_Months_Rise_credit_for_company_pf_over_total_months,
	Meses_Reducao_emprestimos_credito_pPJ_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_Months_Dive_credit_for_company_pf_over_total_months,
	months_sth_emprestimos_credito_pPJ_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_consecutive_dive_credit_for_company_pf_over_total_months,
	months_hth_emprestimos_credito_pPJ_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_consecutive_rise_credit_for_company_pf_over_total_months,
	Saldo_Amort_emprestimos_credito_pPJ_pf * 1000 as scr_credit_for_company_amortization_pf,
	Saldo_Amort_emprestimos_credito_pPJ_pf / greatest(Meses_Reducao_emprestimos_credito_pPJ_pf,1) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Avg_Amortization_credit_for_company_pf_over_revenue,
	Saldo_Amort_emprestimos_credito_pPJ_pf / greatest(Meses_Reducao_emprestimos_credito_pPJ_pf,1) / ((@li.pmt)) * 1000 as scr_Avg_Amortization_credit_for_company_pf_over_loan_PMT,
--------------------------SCR PF MODALIDADE: EMPRESTIMOS CREDITO CONSIGNADO
	count_emprestimos_credito_consignado_pf as scr_count_consignado_pf,
	Ever_emprestimos_credito_consignado_pf * 1000 as scr_ever_consignado_pf,
	emprestimos_credito_consignado_pf_Curr * 1000 as scr_curr_consignado_pf,
	emprestimos_credito_consignado_pf_5M * 1000 as scr_6M_consignado_pf,
	emprestimos_credito_consignado_pf_11M * 1000 as scr_12M_consignado_pf,
	emprestimos_credito_consignado_pf_Curr / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Curr_consignado_pf_over_revenue,
	emprestimos_credito_consignado_pf_Curr / case coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) when 0 then 1 else coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) end as scr_curr_consignado_pf_over_total_debt,
	emprestimos_credito_consignado_pf_Curr / case divida_atual_pf when 0 then 1 else divida_atual_pf end as scr_curr_consignado_pf_over_short_term_debt,
	max_emprestimos_credito_consignado_pf * 1000 as scr_max_consignado_pf,
	emprestimos_credito_consignado_pf_Curr / case max_emprestimos_credito_consignado_pf when 0 then 1 else max_emprestimos_credito_consignado_pf end as scr_curr_over_max_consignado_pf,
	case when emprestimos_credito_consignado_pf_Curr is not null then (emprestimos_credito_consignado_pf_Curr = max_emprestimos_credito_consignado_pf and max_emprestimos_credito_consignado_pf > 0)::int end as scr_All_TimeHigh_consignado_pf,
	(emprestimos_credito_consignado_pf_Curr - emprestimos_credito_consignado_pf_5M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_6M_consignado_pf_over_revenue,
	(emprestimos_credito_consignado_pf_Curr - emprestimos_credito_consignado_pf_11M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_12M_consignado_pf_over_revenue,
	case when emprestimos_credito_consignado_pf_Curr = 0 and emprestimos_credito_consignado_pf_5M = 0 then 0 else emprestimos_credito_consignado_pf_Curr / case emprestimos_credito_consignado_pf_5M when 0 then 1 else emprestimos_credito_consignado_pf_5M end - 1 end as scr_var_rel_6M_consignado_pf,
	case when emprestimos_credito_consignado_pf_Curr = 0 and emprestimos_credito_consignado_pf_11M = 0 then 0 else emprestimos_credito_consignado_pf_Curr / case emprestimos_credito_consignado_pf_11M when 0 then 1 else emprestimos_credito_consignado_pf_11M end - 1 end as scr_var_rel_12M_consignado_pf,
	Meses_Aumento_emprestimos_credito_consignado_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_Months_Rise_consignado_pf_over_total_months,
	Meses_Reducao_emprestimos_credito_consignado_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_Months_Dive_consignado_pf_over_total_months,
	months_sth_emprestimos_credito_consignado_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_consecutive_dive_consignado_pf_over_total_months,
	months_hth_emprestimos_credito_consignado_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_consecutive_rise_consignado_pf_over_total_months,
	Saldo_Amort_emprestimos_credito_consignado_pf * 1000 as scr_consignado_amortization_pf,
	Saldo_Amort_emprestimos_credito_consignado_pf / greatest(Meses_Reducao_emprestimos_credito_consignado_pf,1) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Avg_Amortization_consignado_pf_over_revenue,
	Saldo_Amort_emprestimos_credito_consignado_pf / greatest(Meses_Reducao_emprestimos_credito_consignado_pf,1) / ((@li.pmt)) * 1000 as scr_Avg_Amortization_consignado_pf_over_loan_PMT,
--------------------------SCR PF MODALIDADE: EMPRESTIMOS OUTROS EMPRÃ‰STIMOS
	count_emprestimos_outros_emprestimos_pf as scr_count_other_loans_pf,
	Ever_emprestimos_outros_emprestimos_pf * 1000 as scr_ever_other_loans_pf,
	emprestimos_outros_emprestimos_pf_Curr * 1000 as scr_curr_other_loans_pf,
	emprestimos_outros_emprestimos_pf_5M * 1000 as scr_6M_other_loans_pf,
	emprestimos_outros_emprestimos_pf_11M * 1000 as scr_12M_other_loans_pf,
	emprestimos_outros_emprestimos_pf_Curr / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Curr_other_loans_pf_over_revenue,
	emprestimos_outros_emprestimos_pf_Curr / case coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) when 0 then 1 else coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) end as scr_curr_other_loans_pf_over_total_debt,
	emprestimos_outros_emprestimos_pf_Curr / case divida_atual_pf when 0 then 1 else divida_atual_pf end as scr_curr_other_loans_pf_over_short_term_debt,
	max_emprestimos_outros_emprestimos_pf * 1000 as scr_max_other_loans_pf,
	emprestimos_outros_emprestimos_pf_Curr / case max_emprestimos_outros_emprestimos_pf when 0 then 1 else max_emprestimos_outros_emprestimos_pf end as scr_curr_over_max_other_loans_pf,
	case when emprestimos_outros_emprestimos_pf_Curr is not null then (emprestimos_outros_emprestimos_pf_Curr = max_emprestimos_outros_emprestimos_pf and max_emprestimos_outros_emprestimos_pf > 0)::int end as scr_All_TimeHigh_other_loans_pf,
	(emprestimos_outros_emprestimos_pf_Curr - emprestimos_outros_emprestimos_pf_5M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_6M_other_loans_pf_over_revenue,
	(emprestimos_outros_emprestimos_pf_Curr - emprestimos_outros_emprestimos_pf_11M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_12M_other_loans_pf_over_revenue,
	case when emprestimos_outros_emprestimos_pf_Curr = 0 and emprestimos_outros_emprestimos_pf_5M = 0 then 0 else emprestimos_outros_emprestimos_pf_Curr / case emprestimos_outros_emprestimos_pf_5M when 0 then 1 else emprestimos_outros_emprestimos_pf_5M end - 1 end as scr_var_rel_6M_other_loans_pf,
	case when emprestimos_outros_emprestimos_pf_Curr = 0 and emprestimos_outros_emprestimos_pf_11M = 0 then 0 else emprestimos_outros_emprestimos_pf_Curr / case emprestimos_outros_emprestimos_pf_11M when 0 then 1 else emprestimos_outros_emprestimos_pf_11M end - 1 end as scr_var_rel_12M_other_loans_pf,
	Meses_Aumento_emprestimos_outros_emprestimos_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_Months_Rise_other_loans_pf_over_total_months,
	Meses_Reducao_emprestimos_outros_emprestimos_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_Months_Dive_other_loans_pf_over_total_months,
	months_sth_emprestimos_outros_emprestimos_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_consecutive_dive_other_loans_pf_over_total_months,
	months_hth_emprestimos_outros_emprestimos_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_consecutive_rise_other_loans_pf_over_total_months,
	Saldo_Amort_emprestimos_outros_emprestimos_pf * 1000 as scr_other_loans_amortization_pf,
	Saldo_Amort_emprestimos_outros_emprestimos_pf / greatest(Meses_Reducao_emprestimos_outros_emprestimos_pf,1) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Avg_Amortization_other_loans_pf_over_revenue,
	Saldo_Amort_emprestimos_outros_emprestimos_pf / greatest(Meses_Reducao_emprestimos_outros_emprestimos_pf,1) / ((@li.pmt)) * 1000 as scr_Avg_Amortization_other_loans_pf_over_loan_PMT,
--------------------------SCR PF MODALIDADE: FINANCIAMENTO DE VEÃ�CULOS
	count_financiamento_veiculo_pf as scr_count_vehicle_financing_pf,
	Ever_financiamento_veiculo_pf * 1000 as scr_ever_vehicle_financing_pf,
	financiamento_veiculo_pf_Curr * 1000 as scr_curr_vehicle_financing_pf,
	financiamento_veiculo_pf_5M * 1000 as scr_6M_vehicle_financing_pf,
	financiamento_veiculo_pf_11M * 1000 as scr_12M_vehicle_financing_pf,
	financiamento_veiculo_pf_Curr / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Curr_vehicle_financing_pf_over_revenue,
	financiamento_veiculo_pf_Curr / case coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) when 0 then 1 else coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) end as scr_curr_vehicle_financing_pf_over_total_debt,
	financiamento_veiculo_pf_Curr / case divida_atual_pf when 0 then 1 else divida_atual_pf end as scr_curr_vehicle_financing_pf_over_short_term_debt,
	max_financiamento_veiculo_pf * 1000 as scr_max_vehicle_financing_pf,
	financiamento_veiculo_pf_Curr / case max_financiamento_veiculo_pf when 0 then 1 else max_financiamento_veiculo_pf end as scr_curr_over_max_vehicle_financing_pf,
	case when financiamento_veiculo_pf_Curr is not null then (financiamento_veiculo_pf_Curr = max_financiamento_veiculo_pf and max_financiamento_veiculo_pf > 0)::int end as scr_All_TimeHigh_vehicle_financing_pf,
	(financiamento_veiculo_pf_Curr - financiamento_veiculo_pf_5M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_6M_vehicle_financing_pf_over_revenue,
	(financiamento_veiculo_pf_Curr - financiamento_veiculo_pf_11M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_12M_vehicle_financing_pf_over_revenue,
	case when financiamento_veiculo_pf_Curr = 0 and financiamento_veiculo_pf_5M = 0 then 0 else financiamento_veiculo_pf_Curr / case financiamento_veiculo_pf_5M when 0 then 1 else financiamento_veiculo_pf_5M end - 1 end as scr_var_rel_6M_vehicle_financing_pf,
	case when financiamento_veiculo_pf_Curr = 0 and financiamento_veiculo_pf_11M = 0 then 0 else financiamento_veiculo_pf_Curr / case financiamento_veiculo_pf_11M when 0 then 1 else financiamento_veiculo_pf_11M end - 1 end as scr_var_rel_12M_vehicle_financing_pf,
	Meses_Aumento_financiamento_veiculo_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_Months_Rise_vehicle_financing_pf_over_total_months,
	Meses_Reducao_financiamento_veiculo_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_Months_Dive_vehicle_financing_pf_over_total_months,
	months_sth_financiamento_veiculo_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_consecutive_dive_vehicle_financing_pf_over_total_months,
	months_hth_financiamento_veiculo_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_consecutive_rise_vehicle_financing_pf_over_total_months,
	Saldo_Amort_financiamento_veiculo_pf * 1000 as scr_vehicle_financing_amortization_pf,
	Saldo_Amort_financiamento_veiculo_pf / greatest(Meses_Reducao_financiamento_veiculo_pf,1) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Avg_Amortization_vehicle_financing_pf_over_revenue,
	Saldo_Amort_financiamento_veiculo_pf / greatest(Meses_Reducao_financiamento_veiculo_pf,1) / ((@li.pmt)) * 1000 as scr_Avg_Amortization_vehicle_financing_pf_over_loan_PMT,
--------------------------SCR PF MODALIDADE: FINANCIAMENTO IMOBILIÃ�RIO
	count_financiamento_imobiliario_pf as scr_count_realty_financing_pf,
	Ever_financiamento_imobiliario_pf * 1000 as scr_ever_realty_financing_pf,
	financiamento_imobiliario_pf_Curr * 1000 as scr_curr_realty_financing_pf,
	financiamento_imobiliario_pf_5M * 1000 as scr_6M_realty_financing_pf,
	financiamento_imobiliario_pf_11M * 1000 as scr_12M_realty_financing_pf,
	financiamento_imobiliario_pf_Curr / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Curr_realty_financing_pf_over_revenue,
	financiamento_imobiliario_pf_Curr / case coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) when 0 then 1 else coalesce(long_term_debt_pf_Curr,CarteiraCredito_pf_Curr) end as scr_curr_realty_financing_pf_over_total_debt,
	financiamento_imobiliario_pf_Curr / case divida_atual_pf when 0 then 1 else divida_atual_pf end as scr_curr_realty_financing_pf_over_short_term_debt,
	max_financiamento_imobiliario_pf * 1000 as scr_max_realty_financing_pf,
	financiamento_imobiliario_pf_Curr / case max_financiamento_imobiliario_pf when 0 then 1 else max_financiamento_imobiliario_pf end as scr_curr_over_max_realty_financing_pf,
	case when financiamento_imobiliario_pf_Curr is not null then (financiamento_imobiliario_pf_Curr = max_financiamento_imobiliario_pf and max_financiamento_imobiliario_pf > 0)::int end as scr_All_TimeHigh_realty_financing_pf,
	(financiamento_imobiliario_pf_Curr - financiamento_imobiliario_pf_5M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_6M_realty_financing_pf_over_revenue,
	(financiamento_imobiliario_pf_Curr - financiamento_imobiliario_pf_11M) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_var_abs_12M_realty_financing_pf_over_revenue,
	case when financiamento_imobiliario_pf_Curr = 0 and financiamento_imobiliario_pf_5M = 0 then 0 else financiamento_imobiliario_pf_Curr / case financiamento_imobiliario_pf_5M when 0 then 1 else financiamento_imobiliario_pf_5M end - 1 end as scr_var_rel_6M_realty_financing_pf,
	case when financiamento_imobiliario_pf_Curr = 0 and financiamento_imobiliario_pf_11M = 0 then 0 else financiamento_imobiliario_pf_Curr / case financiamento_imobiliario_pf_11M when 0 then 1 else financiamento_imobiliario_pf_11M end - 1 end as scr_var_rel_12M_realty_financing_pf,
	Meses_Aumento_financiamento_imobiliario_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_Months_Rise_realty_financing_pf_over_total_months,
	Meses_Reducao_financiamento_imobiliario_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_Months_Dive_realty_financing_pf_over_total_months,
	months_sth_financiamento_imobiliario_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_consecutive_dive_realty_financing_pf_over_total_months,
	months_hth_financiamento_imobiliario_pf / greatest(qtd_meses_modalidade_pf - 1, 1) as scr_consecutive_rise_realty_financing_pf_over_total_months,
	Saldo_Amort_financiamento_imobiliario_pf * 1000 as scr_realty_financing_amortization_pf,
	Saldo_Amort_financiamento_imobiliario_pf / greatest(Meses_Reducao_financiamento_imobiliario_pf,1) / greatest(dp.month_revenue,1) / 12 * 1000 as scr_Avg_Amortization_realty_financing_pf_over_revenue,
	Saldo_Amort_financiamento_imobiliario_pf / greatest(Meses_Reducao_financiamento_imobiliario_pf,1) / ((@li.pmt)) * 1000 as scr_Avg_Amortization_realty_financing_pf_over_loan_PMT,
--------------------------SCR PF MODALIDADE: ADIANTAMENTOS E DESCONTOS DE DIREITOS CREDITORIOS
	count_adiantamentosdescontos_pf as scr_count_cash_deposit_advance_pf,
	Ever_adiantamentosdescontos_pf * 1000 as scr_ever_cash_deposit_advance_pf,
	adiantamentosdescontos_pf_Curr * 1000 as scr_curr_cash_deposit_advance_pf,
--------------------------SCR PF MODALIDADE: OUTROS FINANCIAMENTOS
	count_outrosfinanciamentos_pf as scr_count_other_financings_pf,
	Ever_outrosfinanciamentos_pf * 1000 as scr_ever_other_financings_pf,
	outrosfinanciamentos_pf_Curr * 1000 as scr_curr_other_financings_pf,
--------------------------SCR PF MODALIDADE: HOME EQUITY
	count_homeequity_pf as scr_count_home_equity_pf,
	Ever_homeequity_pf * 1000 as scr_ever_home_equity_pf,
	homeequity_pf_Curr * 1000 as scr_curr_home_equity_pf,
--------------------------SERASA
	n_cnpjs_relacionados as experian_number_of_related_companies,
	n_cpfs_relacionados as experian_number_of_related_shareholders,
	check_socios.is_shareholder as experian_requester_is_shareholder,
	check_socios.count_socios as experian_count_shareholders,
--PROBLEMAS DA EMPRESA
	empresas_problema as experian_spced_related_companies,
	empresa_divida_valor as experian_company_overdue_debt_value,
	empresa_divida_valor::numeric / greatest(dp.month_revenue,1) / 12 as experian_company_overdue_debt_value_over_revenue,
	empresa_divida_unit as experian_company_overdue_debt_unit,
	empresa_ccf_valor as experian_company_bad_check_value,
	empresa_ccf_valor::numeric / greatest(dp.month_revenue,1) / 12 as experian_company_bad_check_value_over_revenue,
	empresa_ccf_unit as experian_company_bad_check_unit,
	empresa_protesto_valor as experian_company_protests_value,
	empresa_protesto_valor::numeric / greatest(dp.month_revenue,1) / 12 as experian_company_protests_value_over_revenue,
	empresa_protesto_unit as experian_company_protests_unit,
	empresa_acao_valor as experian_company_legal_action_value,
	empresa_acao_valor::numeric / greatest(dp.month_revenue,1) / 12 as experian_company_legal_action_value_over_revenue,
	empresa_acao_unit as experian_company_legal_action_unit,
	empresa_pefin_valor as experian_company_pefin_value,
	empresa_pefin_valor::numeric / greatest(dp.month_revenue,1) / 12 as experian_company_pefin_value_over_revenue,
	empresa_pefin_unit as experian_company_pefin_unit,
	empresa_refin_valor as experian_company_refin_value,
	empresa_refin_valor::numeric / greatest(dp.month_revenue,1) / 12 as experian_company_refin_value_over_revenue,
	empresa_refin_unit as experian_company_refin_unit,
	empresa_spc_valor as experian_company_spc_value,
	empresa_spc_valor::numeric / greatest(dp.month_revenue,1) / 12 as experian_company_spc_value_over_revenue,
	empresa_spc_unit as experian_company_spc_unit,
	empresa_total_valor as experian_company_spc_total_value,
	empresa_total_valor::numeric / greatest(dp.month_revenue,1) / 12 as experian_company_spc_total_value_over_revenue,
	empresa_total_unit as experian_company_spc_total_unit,
--PROBLEMAS DOS SOCIOS
	socio_divida_valor as experian_sh_overdue_debt_value,
	socio_divida_valor::numeric / greatest(dp.month_revenue,1) / 12 as experian_sh_overdue_debt_value_over_revenue,
	socio_divida_unit as experian_sh_overdue_debt_unit,
	socio_ccf_valor as experian_sh_bad_check_value,
	socio_ccf_valor::numeric / greatest(dp.month_revenue,1) / 12 as experian_sh_bad_check_value_over_revenue,
	socio_ccf_unit as experian_sh_bad_check_unit,
	socio_protesto_valor as experian_sh_protests_value,
	socio_protesto_valor::numeric / greatest(dp.month_revenue,1) / 12 as experian_sh_protests_value_over_revenue,
	socio_protesto_unit as experian_sh_protests_unit,
	socio_acao_valor as experian_sh_legal_action_value,
	socio_acao_valor::numeric / greatest(dp.month_revenue,1) / 12 as experian_sh_legal_action_value_over_revenue,
	socio_acao_unit as experian_sh_legal_action_unit,
	socio_pefin_valor as experian_sh_pefin_value,
	socio_pefin_valor::numeric / greatest(dp.month_revenue,1) / 12 as experian_sh_pefin_value_over_revenue,
	socio_pefin_unit as experian_sh_pefin_unit,
	socio_refin_valor as experian_sh_refin_value,
	socio_refin_valor::numeric / greatest(dp.month_revenue,1) / 12 as experian_sh_refin_value_over_revenue,
	socio_refin_unit as experian_sh_refin_unit,
	socio_spc_valor as experian_sh_spc_value,
	socio_spc_valor::numeric / greatest(dp.month_revenue,1) / 12 as experian_sh_spc_value_over_revenue,
	socio_spc_unit as experian_sh_spc_unit,
	socios_total_valor as experian_sh_spc_total_value,
	socios_total_valor::numeric / greatest(dp.month_revenue,1) / 12 as experian_sh_spc_total_value_over_revenue,
	socios_total_unit as experian_sh_spc_total_unit,
--PROBLEMAS SOMADOS
	all_valor as experian_company_sh_spc_total_value,
	all_valor::numeric / greatest(dp.month_revenue,1) / 12 as experian_company_sh_spc_total_value_over_revenue,
	all_unit as experian_company_sh_spc_total_unit,
--CONSULTAS SERASA + SPC
--	consulta_factoring as experian_report_recently_taken_by_factoring,
	consultas_serasa_atual + consultas_1m + consultas_2m + consultas_3m + consultas_4m + consultas_5m + consultas_6m + consultas_7m + consultas_8m + consultas_9m + consultas_10m + consultas_11m as experian_all_inquiries_ever,
	consultas_serasa_atual as experian_all_inquiries_curr,
	consultas_5m as experian_all_inquiries_6m,
	consultas_11m as experian_all_inquiries_12m,
	greatest(consultas_serasa_atual , consultas_1m , consultas_2m , consultas_3m , consultas_4m , consultas_5m , consultas_6m , consultas_7m , consultas_8m , consultas_9m , consultas_10m , consultas_11m ) as experian_all_inquiries_max,
	consultas_serasa_atual::numeric / greatest(consultas_serasa_atual , consultas_1m , consultas_2m , consultas_3m , consultas_4m , consultas_5m , consultas_6m , consultas_7m , consultas_8m , consultas_9m , consultas_10m , consultas_11m ,1) as experian_all_inquiries_atual_over_max,
	case when consultas_serasa_atual = 0 and consultas_5m = 0 then 0 else consultas_serasa_atual::numeric / greatest(consultas_5m,1) - 1 end as experian_all_inquiries_atual_over_6m,
	case when consultas_serasa_atual = 0 and consultas_11m = 0 then 0 else consultas_serasa_atual::numeric / greatest(consultas_11m,1) - 1 end as experian_all_inquiries_atual_over_12m,
	lucas_leal.months_dive(consultas_serasa_atual + consultas_1m + consultas_2m + consultas_3m + consultas_4m + consultas_5m + consultas_6m + consultas_7m + consultas_8m + consultas_9m + consultas_10m + consultas_11m ) as experian_all_inquiries_months_dive,
	lucas_leal.months_rise(consultas_serasa_atual + consultas_1m + consultas_2m + consultas_3m + consultas_4m + consultas_5m + consultas_6m + consultas_7m + consultas_8m + consultas_9m + consultas_10m + consultas_11m ) as experian_all_inquiries_months_rise,
	lucas_leal.months_sth(consultas_serasa_atual + consultas_1m + consultas_2m + consultas_3m + consultas_4m + consultas_5m + consultas_6m + consultas_7m + consultas_8m + consultas_9m + consultas_10m + consultas_11m ) as experian_all_inquiries_consectuive_months_dive,
	lucas_leal.months_hth(consultas_serasa_atual + consultas_1m + consultas_2m + consultas_3m + consultas_4m + consultas_5m + consultas_6m + consultas_7m + consultas_8m + consultas_9m + consultas_10m + consultas_11m ) as experian_all_inquiries_consectuive_months_rise,
--------------------------CREDIT UNDERWRITE
	cp.data_a1 as credit_underwrite_a1_date,
	trim(initcap(translate(cp.analista_a1,'._',' '))) as credit_underwrite_a1_name,
	cp.status_a1 as credit_underwrite_a1_status,
	cp.nota_a1 as credit_underwrite_a1_score,
	cp.valor_a1 as credit_underwrite_a1_value,
	cp.taxa_a1 as credit_underwrite_a1_interest,
	cp.prazo_a1 as credit_underwrite_a1_term,
	cp.data_a2 as credit_underwrite_a2_date,
	trim(initcap(translate(cp.analista_a2,'._',' '))) as credit_underwrite_a2_name,
	cp.status_a2 as credit_underwrite_a2_status,
	cp.nota_a2 as credit_underwrite_a2_score,
	cp.valor_a2 as credit_underwrite_a2_value,
	cp.taxa_a2 as credit_underwrite_a2_interest,
	cp.prazo_a2 as credit_underwrite_a2_term,
	cp.data_a3 as credit_underwrite_a3_date,
	trim(initcap(translate(cp.analista_a3,'._',' '))) as credit_underwrite_a3_name,
	cp.status_a3 as credit_underwrite_a3_status,
	cp.nota_a3 as credit_underwrite_a3_score,
	cp.valor_a3 as credit_underwrite_a3_value,
	cp.taxa_a3 as credit_underwrite_a3_interest,
	cp.prazo_a3 as credit_underwrite_a3_term,
--------------------------SIGNERS
	s.MajorSigner_Age as signers_MajorShareholder_Age, 
	s.MajorSigner_Revenue as signers_MajorShareholder_Revenue,
	s.MajorSigner_civil_status as signers_MajorShareholder_Maritial_Status,
	s.MajorSigner_gender as signers_MajorShareholdergender,
	s.SolicSignersCPF_Age as signers_RequesterSigner_Age,
	s.SolicSignersCPF_Revenue as signers_RequesterSigner_Revenue,
	s.SolicSignersCPF_civil_status as signers_RequesterSigner_Maritial_Status,
	s.SolicSignersCPF_gender as signers_RequesterSigner_gender,
	s.OldestSh_Age as signers_OldestSh_Age,
	s.OldestSh_Revenue as signers_OldestSh_Revenue,
	s.OldestSh_civil_status as signers_OldestSh_Maritial_status,
	s.OldestSh_gender as signers_OldestSh_gender,
	s.administrator_age as signers_AdminSigner_Age,
	s.administrator_revenue as signers_AdminSigner_Revenue,
	s.administrator_civil_status as signers_AdminSigner_Maritial_status,
	s.administrator_gender as signers_AdminSigner_gender,
	s.avg_age_partners as signers_avg_age_partners,
	s.avg_revenue_partners as signers_avg_revenue_partners,
	s.male_partners as signers_male_shareholders,
	s.female_partners as signers_female_shareholders,
	s.count_socios as signers_number_of_shareholders,
	s.count_avalistas as signers_number_of_guarantors,
	s.count_avalistas_nao_socios as signers_number_of_non_shareholder_guarantors,	
	s.count_total as signers_total_shs_and_guarantors,
--------------------------THIS VIEW
	now() - interval '3 hours' as this_view_last_refresh_time,
--------------------------BIZU 3
	coalesce(bizu3.bizu3_prob,dp.pd_bizu3,batf.bizu3_prob,scw.bizu3_score_prob)::numeric as lead_biz_bizu3_prob,
	lucas_leal.get_bizu3_class(dp.month_revenue::numeric,coalesce(bizu3.bizu3_prob,dp.pd_bizu3,batf.bizu3_prob,scw.bizu3_score_prob)::numeric) as lead_biz_bizu3_class,
--------------------------PODIO CLIENTE
	giulia.matriz_prime(case when dp.opt_in_date::date < '2019-10-15' then 
			(case when dp.bizu2_score >= 1100 then 'A+' when dp.bizu2_score >= 850 then 'A-' when dp.bizu2_score >= 720 then 'B+' when dp.bizu2_score >= 600 then 'B-' when dp.bizu2_score >= 500 then 'C+' when dp.bizu2_score >= 400 then 'C-' when dp.bizu2_score >= 300 then 'D' when dp.bizu2_score >= 200 then 'E' when dp.bizu2_score < 200 then 'F' end)
		when dp.opt_in_date::date = '2019-10-15' then 
			coalesce(case when dp.bizu21_score >= 1200 then 'A+' when dp.bizu21_score >= 950 then 'A-' when dp.bizu21_score >= 750 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end,
				case when dp.bizu2_score >= 1100 then 'A+' when dp.bizu2_score >= 850 then 'A-' when dp.bizu2_score >= 720 then 'B+' when dp.bizu2_score >= 600 then 'B-' when dp.bizu2_score >= 500 then 'C+' when dp.bizu2_score >= 400 then 'C-' when dp.bizu2_score >= 300 then 'D' when dp.bizu2_score >= 200 then 'E' when dp.bizu2_score < 200 then 'F' end)
		when dp.direct_prospect_id < 503019 then
			(case when dp.bizu21_score >= 1200 then 'A+' when dp.bizu21_score >= 950 then 'A-' when dp.bizu21_score >= 750 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end)
		else
			(case when dp.bizu21_score >= 1100 then 'A+' when dp.bizu21_score >= 850 then 'A-' when dp.bizu21_score >= 720 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end)
	end,lucas_leal.get_bizu3_class(dp.month_revenue::numeric,coalesce(bizu3.bizu3_prob,dp.pd_bizu3)::numeric)) as lead_biz_prime_matriz,
	giulia.regra_prime(giulia.matriz_prime(case when dp.opt_in_date::date < '2019-10-15' then 
			(case when dp.bizu2_score >= 1100 then 'A+' when dp.bizu2_score >= 850 then 'A-' when dp.bizu2_score >= 720 then 'B+' when dp.bizu2_score >= 600 then 'B-' when dp.bizu2_score >= 500 then 'C+' when dp.bizu2_score >= 400 then 'C-' when dp.bizu2_score >= 300 then 'D' when dp.bizu2_score >= 200 then 'E' when dp.bizu2_score < 200 then 'F' end)
		when dp.opt_in_date::date = '2019-10-15' then 
			coalesce(case when dp.bizu21_score >= 1200 then 'A+' when dp.bizu21_score >= 950 then 'A-' when dp.bizu21_score >= 750 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end,
				case when dp.bizu2_score >= 1100 then 'A+' when dp.bizu2_score >= 850 then 'A-' when dp.bizu2_score >= 720 then 'B+' when dp.bizu2_score >= 600 then 'B-' when dp.bizu2_score >= 500 then 'C+' when dp.bizu2_score >= 400 then 'C-' when dp.bizu2_score >= 300 then 'D' when dp.bizu2_score >= 200 then 'E' when dp.bizu2_score < 200 then 'F' end)
		when dp.direct_prospect_id < 503019 then
			(case when dp.bizu21_score >= 1200 then 'A+' when dp.bizu21_score >= 950 then 'A-' when dp.bizu21_score >= 750 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end)
		else
			(case when dp.bizu21_score >= 1100 then 'A+' when dp.bizu21_score >= 850 then 'A-' when dp.bizu21_score >= 720 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end)
	end,lucas_leal.get_bizu3_class(dp.month_revenue::numeric,coalesce(bizu3.bizu3_prob,dp.pd_bizu3)::numeric))) as lead_biz_prime_podio,
--------------------------TIPO CONSULTA SERASA
	tetcsif.consulta_proprio as experian_consulta_propria_empresa,
	tetcsif.consulta_factoring as experian_consulta_factoring,
	tetcsif.consulta_seguradora as experian_consulta_seguradoras,
	tetcsif.consulta_financeira as experian_consulta_financeiras,
	tetcsif.consulta_cobranca as experian_consulta_escritorios_cobranca,
	tetcsif.consulta_provedor_dado as experian_consulta_provedores_dados,
	tetcsif.consulta_fornecedores as experian_consulta_fornecedores,
	tetcsif.consulta_indefinido as experian_consulta_indefinido,
--------------------------NEOWAY
	greatest(tnpdf.count_imoveis,tnpdf.count_inpi_marcas,tnpdf.count_obras,tnpdf.count_pat,tnpdf.tem_aeronave,tnpdf.tem_exportacao,tnpdf.tem_licenca_ambiental,tnpdf.total_veiculos) > 0 as neoway_check_materialidade,
--------------------------OUTROS
	case parcelas.i_pmt_of_first_postponing when 1 then dpd_soft.max_late_pmt2 else dpd_soft.max_late_pmt1 end as collection_fpd_by,
	case parcelas.i_pmt_of_first_postponing when 2 then dpd_soft.max_late_pmt3 else dpd_soft.max_late_pmt2 end as collection_spd_by,
	case parcelas.i_pmt_of_first_postponing when 3 then dpd_soft.max_late_pmt4 else dpd_soft.max_late_pmt3 end as collection_tpd_by,
	case parcelas.i_pmt_of_first_postponing when 4 then dpd_soft.max_late_pmt5 else dpd_soft.max_late_pmt4 end as collection_4pd_by,
	dp.workflow_detailed as lead_workflow_detailed,
--------------------------RENEWAL RN
	vbt.behaviourscore as lead_behaviour_rn_score,
	case when vbt.behaviourscore <= .08 then 4 when vbt.behaviourscore <= .3 then 3 when vbt.behaviourscore <= .7 then 2 when vbt.behaviourscore > .7 then 1 end as lead_behaviour_rn_class,
	vbt.behaviour_zone as lead_behaviour_rn_zone,
	case when vbt.behaviourscore <= .1 then 1 when vbt.behaviourscore <= .4 then 2 when vbt.behaviourscore > .4 then 3 end as lead_behaviour_rn_ranking,
	vrt.lead_id_rn_menos1 as lead_behaviour_rn_menos1_id,
--------------------------FATURAMENTO COMPROVADO
	tbif.faturamento_medio as bizextratos3_faturamento_comprovado_medio,
	tbif.perc_faturamento_medio as bizextratos3_faturamento_comprovado_medio_sobre_informado,
	tbtif.faturamento_comprovado_medio as bizextratos4_faturamento_comprovado_medio,
	tbtif.perc_faturamento_comprovado_medio as bizextratos4_faturamento_comprovado_medio_sobre_informado,
--------------------------BIZU COMBO
	dp.bizu_combo_zone_number as lead_bizu_combo_zone_number,
	dp.bizu_combo_zone as lead_bizu_combo_zone,
	parcelas.n_parcelas::numeric / greatest(lr.number_of_installments,1) as collection_current_over_original_term,
----------------------------REJECT REASON
/*
	coalesce(motivo.reject_reason_bad_pmt_record_bnk_stmts,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_bad_pmt_record_bnk_stmts,
	coalesce(motivo.reject_reason_bad_pmt_record_experian_scr,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_bad_pmt_record_experian_scr,
	coalesce(motivo.reject_reason_cnae_quadro_societario,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_cnae_quadro_societario,
	coalesce(motivo.reject_reason_corte_risco_credito,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_corte_risco_credito,
	coalesce(motivo.reject_reason_divisao_grupo_economico,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_divisao_grupo_economico,
	coalesce(motivo.reject_reason_documentation_issues,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_documentation_issues,
	coalesce(motivo.reject_reason_e2020_faturamento_afetado_covid19,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_e2020_faturamento_afetado_covid19,
	coalesce(motivo.reject_reason_e2020_grupo_economico,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_e2020_grupo_economico,
	coalesce(motivo.reject_reason_e2020_queda_bizu_serasa,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_e2020_queda_bizu_serasa,
	coalesce(motivo.reject_reason_empresa_sem_faturamento,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_empresa_sem_faturamento,
	coalesce(motivo.reject_reason_enquadramento_covid19,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_enquadramento_covid19,
	coalesce(motivo.reject_reason_enquadramento_faturamento,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_enquadramento_faturamento,
	coalesce(motivo.reject_reason_faturamento_comprovado_maior_que_informado,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_faturamento_comprovado_maior_que_informado,
	coalesce(motivo.reject_reason_fraud_suspicion,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_fraud_suspicion,
	coalesce(motivo.reject_reason_high_debt_low_income,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_high_debt_low_income,
	coalesce(motivo.reject_reason_high_debt_recent_borrow,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_high_debt_recent_borrow,
	coalesce(motivo.reject_reason_income_decrease,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_income_decrease,
	coalesce(motivo.reject_reason_info_consent_denial,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_info_consent_denial,
	coalesce(motivo.reject_reason_justice_issues,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_justice_issues,
	coalesce(motivo.reject_reason_lack_bank_account,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_lack_bank_account,
	coalesce(motivo.reject_reason_low_income_bnk_stmts,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_low_income_bnk_stmts,
	coalesce(motivo.reject_reason_low_lt_score,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_low_lt_score,
	coalesce(motivo.reject_reason_low_margin,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_low_margin,
	coalesce(motivo.reject_reason_policy_company_activity,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_policy_company_activity,
	coalesce(motivo.reject_reason_policy_less_1yo,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_policy_less_1yo,
	coalesce(motivo.reject_reason_policy_low_experian_score,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_policy_low_experian_score,
	coalesce(motivo.reject_reason_policy_money_use,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_policy_money_use,
	coalesce(motivo.reject_reason_policy_recent_shareholder_change,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_policy_recent_shareholder_change,
	coalesce(motivo.reject_reason_policy_social_contract_change,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_policy_social_contract_change,
	coalesce(motivo.reject_reason_politica_baixo_serasa_analise_doc,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_politica_baixo_serasa_analise_doc,
	coalesce(motivo.reject_reason_politica_covid19,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_politica_covid19,
	coalesce(motivo.reject_reason_politica_empresa_sem_conta_pj,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_politica_empresa_sem_conta_pj,
	coalesce(motivo.reject_reason_queda_bizu_F,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_queda_bizu_F,
	coalesce(motivo.reject_reason_sebrae_empresa_sem_faturamento,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_sebrae_empresa_sem_faturamento,
	coalesce(motivo.reject_reason_sebrae_politica_faturamento,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_sebrae_politica_faturamento,
	coalesce(motivo.reject_reason_sebrae_politica_porte_empresa,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_sebrae_politica_porte_empresa,
	coalesce(motivo.reject_reason_sem_checklist,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_sem_checklist,
	coalesce(motivo.reject_reason_structure_complexity,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 0 end) as reject_reason_structure_complexity,
*/
----------------------------EXTRATO SCORE
	coalesce(batf.extrato_score_prob,scw.extrato_score_prob) as lead_extrato_score_prob,
	lucas_leal.get_extrato_score_class(dp.sector,coalesce(batf.extrato_score_prob,scw.extrato_score_prob)) as lead_extrato_score_class,
----------------------------MESA SCORE
	coalesce(batf.underwrite_final_score_1_prob,scw.mesa_score_prob) as lead_mesa_score_prob,
	coalesce(batf.underwrite_final_score_1_classe,scw.mesa_score) as lead_mesa_score_ranking,
	case when dp.opt_in_date::date < '2019-10-15' then dp.bizu2_score when dp.opt_in_date::date = '2019-10-15' then coalesce(dp.bizu21_score,dp.bizu2_score) else dp.bizu21_score end as lead_biz_bizu2_score_full,
	case when dp.opt_in_date::date < '2019-10-15' then 
			(case when dp.bizu2_score >= 1100 then 'A+' when dp.bizu2_score >= 850 then 'A-' when dp.bizu2_score >= 720 then 'B+' when dp.bizu2_score >= 600 then 'B-' when dp.bizu2_score >= 500 then 'C+' when dp.bizu2_score >= 400 then 'C-' when dp.bizu2_score >= 300 then 'D' when dp.bizu2_score >= 200 then 'E' when dp.bizu2_score < 200 then 'F' end)
		when dp.opt_in_date::date = '2019-10-15' then 
			coalesce(case when dp.bizu21_score >= 1200 then 'A+' when dp.bizu21_score >= 950 then 'A-' when dp.bizu21_score >= 750 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end,
				case when dp.bizu2_score >= 1100 then 'A+' when dp.bizu2_score >= 850 then 'A-' when dp.bizu2_score >= 720 then 'B+' when dp.bizu2_score >= 600 then 'B-' when dp.bizu2_score >= 500 then 'C+' when dp.bizu2_score >= 400 then 'C-' when dp.bizu2_score >= 300 then 'D' when dp.bizu2_score >= 200 then 'E' when dp.bizu2_score < 200 then 'F' end)
		when dp.direct_prospect_id < 503019 then
			(case when dp.bizu21_score >= 1200 then 'A+' when dp.bizu21_score >= 950 then 'A-' when dp.bizu21_score >= 750 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end)
		else
			(case when dp.bizu21_score >= 1100 then 'A+' when dp.bizu21_score >= 850 then 'A-' when dp.bizu21_score >= 720 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end)
	end	as lead_biz_bizu2_class_full,
	dp.outbound_loan_app_id as lead_outbound_loan_application_id,
	crisk.risk as lead_cnae_risk_0,
	crisk.risk_1 as lead_cnae_risk_1,
	crisk.risk_2 as lead_cnae_risk_2,
	crisk.risk_3 as lead_cnae_risk_3,
	crisk.risk_4 as lead_cnae_risk_4,
	crisk.risk_5 as lead_cnae_risk_5,
	coalesce(esteiras.caas,false)::int as lead_is_caas,
	coalesce(esteiras.mca,false)::int as lead_is_mca,
	coalesce(esteiras.outras_esteiras,false)::int as lead_is_outras_esteiras,
	dp.natureza_juridica as neoway_natureza_juridica,
	(dp.state_registration is not null) as neoway_tem_inscricao_estadual,
	bad_scr.ever_mob12 as bad_scr_ever_mob12,
	bad_scr.over_mob12 as bad_scr_over_mob12,
	bad_scr.got_credit_super_extended as bad_scr_got_credit,
	case when (dp.mei or dp.name ~ '.*\sMEI$' or dp.natureza_juridica = '2135') and dp.name !~ '.*\sLTDA$ | .*\sEIRELI$ | .*\sME$' then 1 else 0 end as is_mei_extended,
	(divida_atual_pj) / greatest(dp.month_revenue,1) / 12 as lead_short_term_leverage,
	coalesce(pgfn.tipo,bg2.pgfn) as infosimples_cnd_pgfn,
	mte.tem_cnd_mte as infosimples_cnd_mte,
	fgts.tem_cnd_fgts as infosimples_cnd_fgts,
	dp.psycho_score as lead_psycho_score,
	coalesce(cp.status_a3,cp.status_a2,cp.status_a1) as credit_underwrite_final_decision,
	(coalesce(cp.status_a3,cp.status_a2,cp.status_a1) ~~ 'Aprovar%' or lr.status = 'ACCEPTED')::int as loan_request_is_approved,
	case when coalesce(pgfn.tipo,bg2.pgfn) is not null and mte.tem_cnd_mte is not null and fgts.tem_cnd_fgts is not null then (coalesce(pgfn.tipo,bg2.pgfn) in ('Positiva com efeitos de negativa','Negativa') and mte.tem_cnd_mte = 'true' and fgts.tem_cnd_fgts = 'REGULAR') end::int as infosimples_bndes_elegivel,
	neoway_cnd.pgfn as neoway_cnd_pgfn,
	neoway_cnd.fgts as neoway_cnd_fgts,
	neoway_cnd.tst as neoway_cnd_tst,
	(neoway_cnd.pgfn in ('CERTIDAO NEGATIVA DE DEBITOS EMITIDA','CERTIDAO POSITIVA COM EFEITOS DE NEGATIVA EMITIDA') and neoway_cnd.tst in ('CERTIDAO NEGATIVA DE DEBITOS EMITIDA','CERTIDAO POSITIVA COM EFEITOS DE NEGATIVA EMITIDA') and neoway_cnd.fgts in ('CERTIDAO NEGATIVA DE DEBITOS EMITIDA','CERTIDAO POSITIVA COM EFEITOS DE NEGATIVA EMITIDA'))::int as neoway_bndes_elegivel,
	telesign.phone_type as telesign_phone_type,
	telesign.carrier_name as telesign_carrier_name,
	telesign.score_2 as telesign_score_2,
	telesign.risk_level_2 as telesign_risk_level_2,
	telesign.recommendation_2 as telesign_recommendation_2,
	telesign.customer_label as telesign_customer_label,
	telesign.reason_code_2 as telesign_reason_code_2,
	telesign.number_type as telesign_number_type,
	telesign.a2p_last_seen as telesign_a2p_last_seen,
	telesign.a2p_activity as telesign_a2p_activity,
	telesign.a2p_range_activity as telesign_a2p_range_activity,
	telesign.a2p_activity_risky_services_1 as telesign_a2p_activity_risky_services_1,
	telesign.a2p_activity_risky_services_2 as telesign_a2p_activity_risky_services_2,
	telesign.p2p_last_seen as telesign_p2p_last_seen,
	telesign.p2p_completed_calls as telesign_p2p_completed_calls,
	telesign.p2p_call_duration as telesign_p2p_call_duration,
	telesign.p2p_activity as telesign_p2p_activity,
	telesign.p2p_range_activity as telesign_p2p_range_activity,
	telesign.p2p_moreover as telesign_p2p_moreover,
	(case coalesce(parc_em_aberto + count_postponing_plans,0) when 0 then 0 when 1 then coalesce(dpd_soft.max_late_pmt1,0) when 2 then coalesce(dpd_soft.max_late_pmt2,0) when 3 then coalesce(dpd_soft.max_late_pmt3,0) when 4 then coalesce(dpd_soft.max_late_pmt4,0) when 5 then coalesce(dpd_soft.max_late_pmt5,0) when 6 then coalesce(dpd_soft.max_late_pmt6,0) when 7 then coalesce(dpd_soft.max_late_pmt7,0) when 8 then coalesce(dpd_soft.max_late_pmt8,0) when 9 then coalesce(dpd_soft.max_late_pmt9,0) when 10 then coalesce(dpd_soft.max_late_pmt10,0) when 11 then coalesce(dpd_soft.max_late_pmt11,0) when 12 then coalesce(dpd_soft.max_late_pmt12,0) when 13 then coalesce(dpd_soft.max_late_pmt13,0) when 14 then coalesce(dpd_soft.max_late_pmt14,0) when 15 then coalesce(dpd_soft.max_late_pmt15,0) when 16 then coalesce(dpd_soft.max_late_pmt16,0) when 17 then coalesce(dpd_soft.max_late_pmt17,0) when 18 then coalesce(dpd_soft.max_late_pmt18,0) when 19 then coalesce(dpd_soft.max_late_pmt19,0) when 20 then coalesce(dpd_soft.max_late_pmt20,0) when 21 then coalesce(dpd_soft.max_late_pmt21,0) when 22 then coalesce(dpd_soft.max_late_pmt22,0) when 23 then coalesce(dpd_soft.max_late_pmt23,0) when 24 then coalesce(dpd_soft.max_late_pmt24,0) when 25 then coalesce(dpd_soft.max_late_pmt25,0) when 26 then coalesce(dpd_soft.max_late_pmt26,0) when 27 then coalesce(dpd_soft.max_late_pmt27,0) when 28 then coalesce(dpd_soft.max_late_pmt28,0) when 29 then coalesce(dpd_soft.max_late_pmt29,0) when 30 then coalesce(dpd_soft.max_late_pmt30,0) when 31 then coalesce(dpd_soft.max_late_pmt31,0) when 32 then coalesce(dpd_soft.max_late_pmt32,0) when 33 then coalesce(dpd_soft.max_late_pmt33,0) when 34 then coalesce(dpd_soft.max_late_pmt34,0) when 35 then coalesce(dpd_soft.max_late_pmt35,0) when 36 then coalesce(dpd_soft.max_late_pmt36,0) when 37 then coalesce(dpd_soft.max_late_pmt37,0) when 38 then coalesce(dpd_soft.max_late_pmt38,0) when 39 then coalesce(dpd_soft.max_late_pmt39,0) when 40 then coalesce(dpd_soft.max_late_pmt40,0) when 41 then coalesce(dpd_soft.max_late_pmt41,0) when 42 then coalesce(dpd_soft.max_late_pmt42,0) when 43 then coalesce(dpd_soft.max_late_pmt43,0) when 44 then coalesce(dpd_soft.max_late_pmt44,0) when 45 then coalesce(dpd_soft.max_late_pmt45,0) when 46 then coalesce(dpd_soft.max_late_pmt46,0) when 47 then coalesce(dpd_soft.max_late_pmt47,0) when 48 then coalesce(dpd_soft.max_late_pmt48,0) end >= 15)::int as collection_loan_curr_late_over_15,
	lr.all_docs_date as loan_request_all_docs_date,
	antifraude.cor_final as loan_request_antifraud_color,
	coalesce(motivo.reject_reason_group_main,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 'none' end) as reject_reason_group_main,
	coalesce(motivo.reject_reason_list,case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then 'none' end) as reject_reason_list,
	giulia.regra_prime2(giulia.matriz_prime(case when dp.opt_in_date::date < '2019-10-15' then 
			(case when dp.bizu2_score >= 1100 then 'A+' when dp.bizu2_score >= 850 then 'A-' when dp.bizu2_score >= 720 then 'B+' when dp.bizu2_score >= 600 then 'B-' when dp.bizu2_score >= 500 then 'C+' when dp.bizu2_score >= 400 then 'C-' when dp.bizu2_score >= 300 then 'D' when dp.bizu2_score >= 200 then 'E' when dp.bizu2_score < 200 then 'F' end)
		when dp.opt_in_date::date = '2019-10-15' then 
			coalesce(case when dp.bizu21_score >= 1200 then 'A+' when dp.bizu21_score >= 950 then 'A-' when dp.bizu21_score >= 750 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end,
				case when dp.bizu2_score >= 1100 then 'A+' when dp.bizu2_score >= 850 then 'A-' when dp.bizu2_score >= 720 then 'B+' when dp.bizu2_score >= 600 then 'B-' when dp.bizu2_score >= 500 then 'C+' when dp.bizu2_score >= 400 then 'C-' when dp.bizu2_score >= 300 then 'D' when dp.bizu2_score >= 200 then 'E' when dp.bizu2_score < 200 then 'F' end)
		when dp.direct_prospect_id < 503019 then
			(case when dp.bizu21_score >= 1200 then 'A+' when dp.bizu21_score >= 950 then 'A-' when dp.bizu21_score >= 750 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end)
		else
			(case when dp.bizu21_score >= 1100 then 'A+' when dp.bizu21_score >= 850 then 'A-' when dp.bizu21_score >= 720 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end)
	end,lucas_leal.get_bizu3_class(dp.month_revenue::numeric,coalesce(bizu3.bizu3_prob,dp.pd_bizu3)::numeric))) as lead_biz_prime_profile,
	auto.motivo as loan_request_automatic_decision_motive,
	auto.decisao as loan_request_automatic_decision,
	dpt.elegivel_bndes as lead_tag_fidc_bndes_elegivel,
	dpt.possui_cnds_bndes as lead_tag_fidc_bndes_PossuiCNDs,
	(lr.portfolio_id = 14)::int as loan_request_is_fidc_bndes,
	(bizconta.cnpj is not null)::int as bizconta_cnpj_possui_conta_ativa,
	bem_consultado(tetcsif.consulta_cobranca::int,
		tetcsif.consulta_factoring::int,
		tetcsif.consulta_financeira::int,
		tetcsif.consulta_fornecedores::int,
		tetcsif.consulta_indefinido::int,
		tetcsif.consulta_proprio::int,
		tetcsif.consulta_provedor_dado::int,
		tetcsif.consulta_seguradora::int,
		greatest(tnpdf.count_imoveis,tnpdf.count_inpi_marcas,tnpdf.count_obras,tnpdf.count_pat,tnpdf.tem_aeronave,tnpdf.tem_exportacao,tnpdf.tem_licenca_ambiental,tnpdf.total_veiculos) > 0) as lead_bem_consultado,
	bg.class_bigguys::int as lead_big_guys_score,
	bg.prob_glmnet as lead_biguguys_glmnet_prob,
	bg.classe_glmnet::int as lead_biguguys_glmnet_classe,
	bg.prob_48 as lead_biguguys_48_prob,
	bg.classe_48::int as lead_biguguys_48_classe,
	bg2.rating_bndes_big_guys,
	bg2.rating_bndes_small_guys,
	bg2.rating_bndes_renewal,
	bg2.rating_bndes_consolidado,
	tnjcd.grupo as neoway_natureza_juridica_grupo,
	tnjcd.descricao as neoway_natureza_juridica_descricao
------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------FUNIL
from public.direct_prospects dp
join lucas_leal.v_funil_completo_sem_duplicacao vfcsd on vfcsd.direct_prospect_id = dp.direct_prospect_id 
left join public.clients c on c.client_id = dp.client_id
left join public.offers o on o.offer_id = vfcsd.offer_id 
left join public.offers initialoff on initialoff.offer_id = vfcsd.primeira_oferta_id 
left join public.loan_requests lr on lr.loan_request_id = vfcsd.loan_request_id 
left join public.loan_infos li on li.loan_info_id = lr.loan_request_id
--------------------------BANK ACCOUNTS
left join public.bank_accounts ba on ba.bank_account_id = lr.bank_account_id
--------------------------GEOFUSION
left join lucas_leal.geofusion_data gd on gd.pk = dp.state || dp.city
--------------------------SERASA 6 BACKTEST
left join backtests.s4_s6_bizu_20190923 as bktest on bktest.lead_id = dp.direct_prospect_id
left join lucas_leal.backtest_serasa_jan2020 as bktest2 on bktest2.lead_id = dp.direct_prospect_id
--------------------------AFILIADOS
left join (select distinct identificacao from parceiros_parceiro) pp on pp.identificacao = replace(dp.utm_source,'#/','')
--------------------------ALL DOCS
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
--------------------------RENEWAL TAPE RN
left join lucas_leal.v_renewal_tape vrt on vrt.lead_id_rn = dp.direct_prospect_id 
left join lucas_leal.v_behaviour_tape vbt on vbt.direct_prospect_id = dp.direct_prospect_id 
--------------------------BIZU 3
left join data_science.bizu3_calc as bizu3 on bizu3.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SCR HISTORICO PJ
left join lucas_leal.v_scr_pj_history_full as SCRHistPJ on SCRHistPJ.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SCR HISTORICO PF
left join lucas_leal.v_scr_pf_history_full as SCRHistPF on SCRHistPF.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SCR MODALIDADES PJ
left join lucas_leal.v_scr_pj_modal_full as SCRModal_PJ on SCRModal_PJ.direct_prospect_id = dp.direct_prospect_id 
--------------------------TABLE SCR MODALIDADES PF
left join lucas_leal.v_scr_pf_modal_full as SCRModal_PF on SCRModal_PF.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE PARCELAS
left JOIN analytics_engineering.v_current_soft_por_parcela_v2 as parcelas ON parcelas.emprestimo_id = lr.loan_request_id
left JOIN analytics_engineering.v_parcelas_indicators as parcelas2 ON parcelas2.emprestimo_id = lr.loan_request_id
--------------------------TABLE PLANO PAGAMENTO
left JOIN (SELECT lr.loan_request_id,
		max(case when fpp.status = 'Pago' then 1 else 0 end) as fully_paid,
		count(*) filter (where fpp.parcelas_qtd = 1 and fpp.status = 'Pago') as antecipou_emprestimo,
		count(*) - 1 as count_renegotiation,
		count(*) filter (where fpp.status not in ('Cancelado','EmEspera')) - 1 as count_unbroken_renegotiations,
		count(*) filter (where fpp.status = 'Cancelado') as count_broken_renegotiation,
		min(coalesce(fpp.creation_date,fpp.inicio_em)::date) filter (where fpp.inicio_em > lr.loan_date + interval '5 days') as data_primeira_renegociacao,
		max(coalesce(fpp.creation_date,fpp.inicio_em)::date) filter (where fpp.inicio_em > lr.loan_date + interval '5 days') as data_ultima_renegociacao,
		count(*) filter (where fpp.status not in ('Cancelado','EmEspera') and fpp."type" = 'POSTPONING') as count_postponing_plans
	FROM loan_requests lr
	JOIN offers o ON o.offer_id = lr.offer_id
	JOIN direct_prospects dp ON dp.direct_prospect_id = o.direct_prospect_id
	JOIN financeiro_planopagamento fpp ON fpp.emprestimo_id = lr.loan_request_id
	GROUP BY lr.loan_request_id) planopagamento ON planopagamento.loan_request_id = lr.loan_request_id
----------------------TABELA PROMESSA DE PAGAMENTO
left join(select cr.emprestimo_id,
		count(*) as total_promessas,
		count(*) filter (where cpp.status = 'Cumprida') as total_promessas_cumpridas,
		count(*) filter (where cpp.status = 'Quebrada') as total_promessas_quebradas,
		count(*) filter (where cpp.status = 'Cancelada') as total_promessas_canceladas,
		count(*) filter (where cpp.status = 'Ativa') as total_promessas_ativas,
		max((not(cr.contato_whatsapp) and not(cr.contato_telefone) and not(cr.contato_email))::int) as sem_contato
	from public.cobranca_regua cr
	join public.cobranca_promessapagamento cpp on cpp.regua_id = cr.id
	group by 1) as promessas on promessas.emprestimo_id = lr.loan_request_id
--------------------------TABELA HISTORICO DE NEGATIVACAO
left join(select cr.emprestimo_id,
		count(*) as total_negativacoes,
		count(*) filter (where sh.has_error = true) as total_negativacoes_com_erro,
		count(*) filter (where sh.has_error = false) as total_negativacoes_sem_erro,
		count(*) filter (where sh.company_spoiled = true) as total_negativacoes_empresa,
		count(*) filter (where sh.guarantor_spoiled = true) as total_negativacoes_avalistas,
		count(*) filter (where sh.company_spoiled = true and sh.has_error = false) as total_negativacoes_empresa_sem_erro,
		count(*) filter (where sh.guarantor_spoiled = true and sh.has_error = false) as total_negativacoes_avalistas_sem_erro,
		sum(sh.number_of_installments_spoiled) as total_negativacao_parcelas,
		sum(sh.number_of_installments_spoiled) filter (where sh.has_error = false) as total_negativacao_parcelas_sem_erro	
	from public.cobranca_regua cr
	join public.spoil_history sh on sh.collection_bar_id = cr.id
	group by 1) as historico_negativacao on historico_negativacao.emprestimo_id = lr.loan_request_id
--------------------------TABLE DPD SOFT
left join lucas_leal.v_current_soft_por_parcela as dpd_soft ON dpd_soft.emprestimo_id = lr.loan_request_id
--------------------------TABLE COLLECTION SOFT
left join (select
		soft.emprestimo_id,
		max(coalesce(soft.soft,0)) filter (where dt = photo.pmt_3) as over_pmt_3,
		max(coalesce(soft.soft,0)) filter (where dt = photo.pmt_4) as over_pmt_4,
		max(coalesce(soft.soft,0)) filter (where dt = photo.pmt_5) as over_pmt_5,
		max(coalesce(soft.soft,0)) filter (where dt = photo.pmt_6) as over_pmt_6,
		max(coalesce(soft.soft,0)) filter (where dt = photo.pmt_perc_50) as over_pmt_perc_50,
		max(soft.maxatraso) filter (where dt = photo.pmt_3) as ever_pmt_3,
		max(soft.maxatraso) filter (where dt = photo.pmt_4) as ever_pmt_4,
		max(soft.maxatraso) filter (where dt = photo.pmt_5) as ever_pmt_5,
		max(soft.maxatraso) filter (where dt = photo.pmt_6) as ever_pmt_6,
		max(soft.maxatraso) filter (where dt = photo.pmt_perc_50) as ever_pmt_perc_50
	from metabase.historico as soft
	join v_collection_photo_dates as photo on photo.emprestimo_id = soft.emprestimo_id
	group by 1) as soft on soft.emprestimo_id = lr.loan_request_id
--------------------------TABLE COLLECTION AGGRESSIVE
left join (select
		agg.loan_request_id,
		max(coalesce(case when photo.pmt_4 < coalesce(data_quitacao,'9999-12-31'::date) then agg.atraso end,0)) filter (where agg.dia = least(photo.pmt_4,data_quitacao)) as over_pmt_4,
		max(coalesce(case when photo.pmt_5 < coalesce(data_quitacao,'9999-12-31'::date) then agg.atraso end,0)) filter (where agg.dia = least(photo.pmt_5,data_quitacao)) as over_pmt_5,
		max(coalesce(case when photo.pmt_6 < coalesce(data_quitacao,'9999-12-31'::date) then agg.atraso end,0)) filter (where agg.dia = least(photo.pmt_6,data_quitacao)) as over_pmt_6,
		max(coalesce(case when photo.pmt_7 < coalesce(data_quitacao,'9999-12-31'::date) then agg.atraso end,0)) filter (where agg.dia = least(photo.pmt_7,data_quitacao)) as over_pmt_7,
		max(coalesce(case when photo.pmt_8 < coalesce(data_quitacao,'9999-12-31'::date) then agg.atraso end,0)) filter (where agg.dia = least(photo.pmt_8,data_quitacao)) as over_pmt_8,
		max(coalesce(case when photo.pmt_9 < coalesce(data_quitacao,'9999-12-31'::date) then agg.atraso end,0)) filter (where agg.dia = least(photo.pmt_9,data_quitacao)) as over_pmt_9,
		max(coalesce(case when photo.pmt_10 < coalesce(data_quitacao,'9999-12-31'::date) then agg.atraso end,0)) filter (where agg.dia = least(photo.pmt_10,data_quitacao)) as over_pmt_10,
		max(coalesce(case when photo.pmt_11 < coalesce(data_quitacao,'9999-12-31'::date) then agg.atraso end,0)) filter (where agg.dia = least(photo.pmt_11,data_quitacao)) as over_pmt_11,
		max(coalesce(case when photo.pmt_12 < coalesce(data_quitacao,'9999-12-31'::date) then agg.atraso end,0)) filter (where agg.dia = least(photo.pmt_12,data_quitacao)) as over_pmt_12,
		max(coalesce(case when photo.pmt_perc_50 < coalesce(data_quitacao,'9999-12-31'::date) then agg.atraso end,0)) filter (where agg.dia = least(photo.pmt_perc_50,data_quitacao)) as over_pmt_perc_50,
		max(coalesce(agg.atraso_maximo,0)) filter (where agg.dia = least(photo.pmt_4,data_quitacao)) as ever_pmt_4,
		max(coalesce(agg.atraso_maximo,0)) filter (where agg.dia = least(photo.pmt_5,data_quitacao)) as ever_pmt_5,
		max(coalesce(agg.atraso_maximo,0)) filter (where agg.dia = least(photo.pmt_6,data_quitacao)) as ever_pmt_6,
		max(coalesce(agg.atraso_maximo,0)) filter (where agg.dia = least(photo.pmt_7,data_quitacao)) as ever_pmt_7,
		max(coalesce(agg.atraso_maximo,0)) filter (where agg.dia = least(photo.pmt_8,data_quitacao)) as ever_pmt_8,
		max(coalesce(agg.atraso_maximo,0)) filter (where agg.dia = least(photo.pmt_9,data_quitacao)) as ever_pmt_9,
		max(coalesce(agg.atraso_maximo,0)) filter (where agg.dia = least(photo.pmt_10,data_quitacao)) as ever_pmt_10,
		max(coalesce(agg.atraso_maximo,0)) filter (where agg.dia = least(photo.pmt_11,data_quitacao)) as ever_pmt_11,
		max(coalesce(agg.atraso_maximo,0)) filter (where agg.dia = least(photo.pmt_12,data_quitacao)) as ever_pmt_12,
		max(coalesce(agg.atraso_maximo,0)) filter (where agg.dia = least(photo.pmt_perc_50,data_quitacao)) as ever_pmt_perc_50
	from t_collection_aggressive as agg
	join v_collection_photo_dates as photo on photo.emprestimo_id = agg.loan_request_id
	left join (select
			loan_request_id,
			max(dia) as data_quitacao
		from t_collection_aggressive t
		join public.financeiro_planopagamento fpp on fpp.emprestimo_id = t.loan_request_id
		where fpp.status = 'Pago'
		group by 1) as max_agg on max_agg.loan_request_id = agg.loan_request_id
	group by 1) as agg on agg.loan_request_id = lr.loan_request_id
--------------------------TABLE COLLECTION AMOUNT PAID
left join (select 
			fpp.emprestimo_id,
			sum(fp.pago) filter (where fp.pago_em <= photo.pmt_4) as amount_paid_pmt_3,
			sum(fp.pago) filter (where fp.pago_em <= photo.pmt_4) as amount_paid_pmt_4,
			sum(fp.pago) filter (where fp.pago_em <= photo.pmt_5) as amount_paid_pmt_5,
			sum(fp.pago) filter (where fp.pago_em <= photo.pmt_6) as amount_paid_pmt_6,
			sum(fp.pago) filter (where fp.pago_em <= photo.pmt_7) as amount_paid_pmt_7,
			sum(fp.pago) filter (where fp.pago_em <= photo.pmt_8) as amount_paid_pmt_8,
			sum(fp.pago) filter (where fp.pago_em <= photo.pmt_9) as amount_paid_pmt_9,
			sum(fp.pago) filter (where fp.pago_em <= photo.pmt_10) as amount_paid_pmt_10,
			sum(fp.pago) filter (where fp.pago_em <= photo.pmt_11) as amount_paid_pmt_11,
			sum(fp.pago) filter (where fp.pago_em <= photo.pmt_12) as amount_paid_pmt_12,
			sum(fp.pago) filter (where fp.pago_em <= photo.pmt_perc_50) as amount_paid_pmt_perc_50
		from public.financeiro_planopagamento fpp
		join public.financeiro_parcela fp on fp.plano_id = fpp.id
		join v_collection_photo_dates as photo on photo.emprestimo_id = fpp.emprestimo_id
		group by 1) as amount_paid on amount_paid.emprestimo_id = lr.loan_request_id
--------------------------TABLE SERASA SCORE
left join lucas_leal.t_experian_get_score_full as SerasaScore on SerasaScore.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SERASA SOCIOS
left join(select
		loan_request_id,
		count(*) filter (where participacao > 0) as count_socios,
		count(*) filter (where cpf_socio = cpf_solicitante) as is_shareholder
	from(select
			distinct
			lr.loan_request_id,
			dp.cpf as cpf_solicitante,
			coalesce(case when es.cpf = '' then null else es.cpf end,s.cpf) as cpf_socio,
			coalesce(es.participacao,s.share_percentage) as participacao
		from public.direct_prospects dp
		join public.offers o on o.direct_prospect_id = dp.direct_prospect_id
		join public.loan_requests lr on lr.offer_id = o.offer_id
		left join public.empresas_socios es on es.cnpj = dp.cnpj
		left join(select lr.loan_request_id,
				s.cpf,
				s.share_percentage
			from public.direct_prospects dp
			join public.offers o on o.direct_prospect_id = dp.direct_prospect_id
			join public.loan_requests lr on lr.offer_id = o.offer_id
			join public.loan_requests_signers lrs on lrs.loan_request_id = lr.loan_request_id
			join public.signers s on s.signer_id = lrs.signer_id
			left join lucas_leal.t_experian_get_shareholders_data t1 on t1.direct_prospect_id = dp.direct_prospect_id and t1.nome = upper(s.name::text)) as s on s.loan_request_id = lr.loan_request_id
		where lr.status = 'ACCEPTED') as t1
	group by 1) as check_socios on check_socios.loan_request_id = lr.loan_request_id
--------------------------TABLE SERASA ANOTACOES
left join lucas_leal.t_experian_get_company_negative_public_record_data_full as SerasaRestr on SerasaRestr.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SERASA PROBLEMA COLIGADAS
left join lucas_leal.t_experian_get_related_companies_data_full as serasaColig on serasaColig.direct_prospect_id = dp.direct_prospect_id 
--------------------------TABLE SERASA CONSULTAS
left join lucas_leal.t_experian_get_serasa_inquiries_data_full as serasa_consultas on serasa_consultas.direct_prospect_id = dp.direct_prospect_id
-------------------------TABLE CNPJs RELACIONADOS
left join lucas_leal.lead_get_cnpjs_related as cnpjs_relacionados on cnpjs_relacionados.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE CPFs RELACIONADOS
left join lucas_leal.lead_get_cpfs_related as cpfs_relacionados on cpfs_relacionados.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE CREDITO PARECER
left join lucas_leal.mv_credit_underwrite_indicators_full cp on cp.emprestimo_id = lr.loan_request_id
--------------------------TABLE MOTIVOS DE REJEICAO
left join () motivo on motivo.legacy_target_id = dp.direct_prospect_id
--------------------------TABLE AGENCIA BANCARIA
left join lucas_leal.agencias_bancarias_dez19_by_state as agencias_uf on agencias_uf.uf = dp.state
--------------------------
left join lucas_leal.agencias_bancarias_dez19_by_city agencias_municipio on agencias_municipio.id = dp.state || '-' || dp.city
--------------------------
left join lucas_leal.agencias_bancarias_dez19_by_neighbourhood agencias_bairro on agencias_bairro.id = dp.state || '-' || dp.city || '-' || dp.neighbourhood
--------------------------
left join lucas_leal.agencias_bancarias_dez19_by_postal_code agencias_postal on agencias_postal.postal = dp.state
--------------------------TABLE SOCIOS
left join lucas_leal.t_signers_indicators_full s on s.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SIGNERS ADDRESS
left join lucas_leal.t_signers_zip_code_data as signer_address on signer_address.direct_prospect_id = dp.direct_prospect_id
--------------------------CONSULTA SERASA TIPO
left join lucas_leal.t_experian_tipo_consultas_serasa_indicador_full as tetcsif on tetcsif.direct_prospect_id = dp.direct_prospect_id 
--------------------------NEOWAY
left join lucas_leal.t_neoway_pj_materialidade_full as tnpdf on tnpdf.direct_prospect_id = dp.direct_prospect_id 
--------------------------FATURAMENTO COMPROVADO
left join lucas_leal.t_bizextrato_indicators_full tbif on tbif.loan_request_id = lr.loan_request_id 
left join lucas_leal.t_bizextrato4_transactions_indicators_full tbtif on tbtif.loan_request_id = lr.loan_request_id 
--------------------------SCORE DE EXTRATO E MESA
left join lucas_leal.bizcredit_approval_tape_full batf on batf.loan_request_id = lr.loan_request_id
left join lucas_leal.mv_lead_tape_credit_worthiness_full as scw on scw.lead_id = dp.direct_prospect_id and scw.indice_analise = 1
--------------------------CNAE RISK
left join data_science.crisk as crisk on crisk.cnae_code = dp.cnae_code and crisk.risk_5 is not null
--------------------------ESTEIRAS
left join data_science.t_processo_automacao_canais_excluidos as esteiras on esteiras.utm_source = dp.utm_source
--------------------------BAD SCR
left join lucas_leal.t_backtest_bad_scr as bad_scr on bad_scr.lead_id = dp.direct_prospect_id
--------------------------INFOSIMPLES BNDES PGFN
left join (select 
		loan_app_id,
		row_number() over (partition by loan_app_id order by t1.data_output->>'code',
			(coalesce(to_date(certidoes ->> 'data_validade','dd/MM/yyyy'),'2999-01-01'::date) >= dp.opt_in_date::date and coalesce(to_date(certidoes ->> 'data_emissao','dd/MM/yyyy'),'1900-01-01') <= dp.opt_in_date::date)::int desc,
			to_date(certidoes ->> 'data_emissao','dd/MM/yyyy') desc) as indice_coleta,
		case 
			when t1.tipo not like 'backtest%' or (coalesce(to_date(certidoes ->> 'data_validade','dd/MM/yyyy'),'2999-01-01'::date) >= dp.opt_in_date::date and coalesce(to_date(certidoes ->> 'data_emissao','dd/MM/yyyy'),'1900-01-01') <= dp.opt_in_date::date)
				then case data_output ->> 'code'
					when '200' then coalesce(data_output -> 'data'->>'tipo',certidoes ->> 'tipo')
					when '611' then 'Indisponivel'
					when '612' then 'Indisponivel'
					end
			else 'Indisponivel'
		end as tipo
	from lucas_leal.t_bndes_infosimples_pgfn t1
	join public.direct_prospects dp on dp.loan_application_id = t1.loan_app_id 
	left join jsonb_array_elements(t1.data_output-> 'data' -> 'certidoes') as certidoes on true
) as pgfn on pgfn.loan_app_id = dp.loan_application_id and pgfn.indice_coleta = 1
--------------------------INFOSIMPLES BNDES MTE
left join (select 
		loan_app_id,
		coalesce(data_output -> 'data' ->> 'conseguiu_emitir_certidao_negativa','Indisponivel') as tem_cnd_mte
	from lucas_leal.t_bndes_infosimples_mte) as mte on mte.loan_app_id = dp.loan_application_id
--------------------------INFOSIMPLES BNDES FGTS
left join (select 
		loan_app_id,
		coalesce(data_output -> 'data' ->> 'situacao','Indisponivel') as tem_cnd_fgts
	from lucas_leal.t_bndes_infosimples_fgts) as fgts on fgts.loan_app_id = dp.loan_application_id
--------------------------NEOWAY CNDS
left join (select 
		direct_prospect_id,
		max(cnd_descricaosituacao) filter (where cnd_nome = 'PGFN') as pgfn,
		max(cnd_descricaosituacao) filter (where cnd_nome = 'FGTS') as fgts,
		max(cnd_descricaosituacao) filter (where cnd_nome = 'TST') as tst	
	from lucas_leal.t_neoway_pj_cnds_data_full
	group by 1) as neoway_cnd on neoway_cnd.direct_prospect_id = dp.direct_prospect_id
--------------------------
left join t_telesign_backtest_score as telesign on telesign.lead_id = dp.direct_prospect_id
left join lucas_leal.t_antifraude_modelo_neuroid_prop as antifraude on antifraude.lead_id = dp.direct_prospect_id
left join (select 
		loan_request_id ,
		motivo,
		decisao,
		row_number () over (partition by loan_request_id order by criado_em desc) as indice
	from backtests.automacao_mesa_politica_log) as auto on auto.loan_request_id = lr.loan_request_id and indice = 1
left join (select 
		directprospect_id, 
		count(*) filter (where tag_id = 316) as elegivel_bndes,
		count(*) filter (where tag_id = 317) as possui_cnds_bndes
	from public.direct_prospects_tag
	group by 1) dpt on dpt.directprospect_id = dp.direct_prospect_id
left join t_bizconta_contas_ativas as bizconta on bizconta.cnpj = dp.cnpj
left join lucas_leal.t_natureza_jurisca_codigos_descricao tnjcd on tnjcd.codigo_tratado = dp.natureza_juridica
left join analytics_engineering.big_guys2 bg2 on bg2.lead_id = dp.direct_prospect_id
left join analytics_engineering.mv_score_bigguys bg on bg.lead_id = dp.direct_prospect_id

