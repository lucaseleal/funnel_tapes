select 
	t1.lead_entrante,
	count(*) filter (where t1.mesmo_cnpj = 0 and dp.opt_in_date::date >= t0.data_explosao) as lead_relacionado_over15,
	count(*) filter (where t1.mesmo_cnpj = 1 and lr.status = 'REJECTED') as mesmo_cnpj_rejeitado,
	count(*) filter (where t1.mesmo_cnpj = 0 and lr.status = 'REJECTED') as lead_relacionado_rejeitado
from lucas_leal.mv_antivirus_full t1
join public.direct_prospects dp on dp.direct_prospect_id = t1.lead_entrante
join lucas_leal.v_funil_completo_sem_duplicacao vfcsd on vfcsd.direct_prospect_id = t1.lead_carteira
left join public.loan_requests lr on lr.loan_request_id = vfcsd.loan_request_id 
left join (select 
		t1.lead_id,
		t1.loan_1st_pmt_due_date + '15 days'::interval as data_explosao
	from data_science.lead_tape t1
	join public.installment_actual2 t2 on t2.loan_request_id = t1.loan_request_id and t2.installment_number = t1.collection_pmt_of_over15
	where t1.collection_loan_curr_late_by > 15
	) as t0 on t0.lead_id = t1.lead_carteira  
where (t1.mesmo_cnpj = 0 and dp.opt_in_date::date >= t0.data_explosao) or (t1.mesmo_cnpj = 1 and lr.status = 'REJECTED') or (t1.mesmo_cnpj = 0 and lr.status = 'REJECTED')
group by 1


create materialized view lucas_leal.mv_antivirus_full as 

--SERVI�O
select 
	s1.lead_entrante,
	s1.lead_carteira,
	max(s4.motivo_rejeicao) as motivo_rejeicao,
	max(s3.workflow) as status_lead_carteira,
	max(case when s2.cnpj = s3.cnpj then 1 else 0 end) as mesmo_cnpj,
	max(case s1.check_mark when 'cpf avalista' then 1 else 0 end) as cpf_avalista,
	max(case s1.check_mark when 'cpf conjuge' then 1 else 0 end) as cpf_conjuge,
	max(case s1.check_mark when 'email avalista' then 1 else 0 end) as email_avalista,
	max(case s1.check_mark when 'email conjuge' then 1 else 0 end) as email_conjuge,
	max(case s1.check_mark when 'cpf adm serasa' then 1 else 0 end) as cpf_adm_serasa,
	max(case s1.check_mark when 'cnpj coligada serasa' then 1 else 0 end) as cnpj_coligada_serasa,
	max(case s1.check_mark when 'cnpj coligada total neoway pf' then 1 else 0 end) as cnpj_coligada_total_neoway_pf,
	max(case s1.check_mark when 'cnpj coligada ativa neoway pf' then 1 else 0 end) as cnpj_coligada_ativa_neoway_pf,
	max(case s1.check_mark when 'cpf relacionado neoway pf' then 1 else 0 end) as cpf_relacionado_neoway_pf,
	max(case s1.check_mark when 'cpf relacionado admin' then 1 else 0 end) as cpf_relacionado_admin,
	max(case s1.check_mark when 'telefone neoway pf' then 1 else 0 end) as telefone_neoway_pf,
	max(case s1.check_mark when 'cnpj coligada neoway pj' then 1 else 0 end) as cnpj_coligada_neoway_pj,
	max(case s1.check_mark when 'cnpj relacionado admin' then 1 else 0 end) as cnpj_relacionado_admin,
	max(case s1.check_mark when 'cpf solicitante' then 1 else 0 end) as cpf_solicitante,
	max(case s1.check_mark when 'telefone solicitante' then 1 else 0 end) as telefone_solicitante,
	max(case s1.check_mark when 'email solicitante' then 1 else 0 end) as email_solicitante,
	max(case s1.check_mark when 'mesmo endereco solicitante' then 1 else 0 end) as endereco_solicitante,
	max(case s1.check_mark when 'ip solicitante' then 1 else 0 end) as ip_solicitante,
	max(coalesce(lt.collection_loan_curr_late_by,0)) as atraso_corrente,
	max(coalesce(lt.collection_loan_max_late_by,0)) as atraso_maximo,
	max(s3.opt_in_date::date) as data_lead_carteira,
    row_number() over (order by max(lt.collection_loan_curr_late_by), max(lt.collection_loan_max_late_by), case when max(s2.workflow) = 'PedidoAprovado' then 1 when max(s2.workflow) ~~ 'Pedido%' then 2 when max(s2.workflow) ~~ 'Oferta%' then 3 else 4 end) as ordem_lista,
    max(s3.name) as razao_social_carteira,
    (max(s4.suspeita_fraude) > 0)::int as tem_suspeita_fraude
from(
	select 
		q1.direct_prospect_id as lead_entrante,
		q2.direct_prospect_id as lead_carteira,
		'cpf avalista' as check_mark
	from public.direct_prospects q1 
	join lucas_leal.t_signers_data_full t1 on t1.signer_cpf = q1.cpf
	join public.direct_prospects q2 on q2.direct_prospect_id = t1.direct_prospect_id  and q1.direct_prospect_id > q2.direct_prospect_id
	where q1.cpf ~ '\d{11}'
and q1.direct_prospect_id = 1257816
union	
	select 
		q1.direct_prospect_id as lead_entrante,
		q2.direct_prospect_id as lead_carteira,
		'cpf conjuge' as check_mark
	from public.direct_prospects q1 
	join lucas_leal.t_signers_data_full t1 on t1.signer_spouse_cpf = q1.cpf 
	join public.direct_prospects q2 on q2.direct_prospect_id = t1.direct_prospect_id  and q1.direct_prospect_id > q2.direct_prospect_id
	where q1.cpf ~ '\d{11}'
and q1.direct_prospect_id = 1257816
union	
	select 
		q1.direct_prospect_id as lead_entrante,
		q2.direct_prospect_id as lead_carteira,
		'cpf adm serasa' as check_mark
	from public.direct_prospects q1 
	join lucas_leal.t_experian_controle_adm_full t1 on t1.controle_adm_cpf = q1.cpf
	join public.direct_prospects q2 on q2.direct_prospect_id = t1.direct_prospect_id  and q1.direct_prospect_id > q2.direct_prospect_id
	where q1.cpf ~ '\d{11}'
and q1.direct_prospect_id = 1257816
union	
	select 
		q1.direct_prospect_id as lead_entrante,
		q2.direct_prospect_id as lead_carteira,
		'cpf relacionado neoway pf' as check_mark
	from public.direct_prospects q1 
	join lucas_leal.t_neoway_pf_identity_data_full t1 on t1.cpf = q1.cpf
	join public.direct_prospects q2 on q2.direct_prospect_id = t1.direct_prospect_id  and q1.direct_prospect_id > q2.direct_prospect_id
	where q1.cpf ~ '\d{11}'
and q1.direct_prospect_id = 1257816
union	
	select 
		q1.direct_prospect_id as lead_entrante,
		q2.direct_prospect_id as lead_carteira,
		'cpf solicitante' as check_mark
	from public.direct_prospects q1
	join public.direct_prospects q2 on q2.cpf = q1.cpf  and q1.direct_prospect_id > q2.direct_prospect_id	
	where q1.cpf ~ '\d{11}'
and q1.direct_prospect_id = 1257816
union	
	select 
		q1.direct_prospect_id as lead_entrante,
		q2.direct_prospect_id as lead_carteira,
		'email avalista' as check_mark
	from public.direct_prospects q1 
	join lucas_leal.t_signers_data_full t1 on lower(t1.signer_email) = lower(q1.email) 
	join public.direct_prospects q2 on q2.direct_prospect_id = t1.direct_prospect_id  and q1.direct_prospect_id > q2.direct_prospect_id
	where q1.email ~ '.*@.*'
and q1.direct_prospect_id = 1257816
union	
	select 
		q1.direct_prospect_id as lead_entrante,
		q2.direct_prospect_id as lead_carteira,
		'email conjuge' as check_mark
	from public.direct_prospects q1 
	join lucas_leal.t_signers_data_full t1 on lower(t1.signer_spouse_email) = lower(q1.email) 
	join public.direct_prospects q2 on q2.direct_prospect_id = t1.direct_prospect_id  and q1.direct_prospect_id > q2.direct_prospect_id
	where q1.email ~ '.*@.*'
and q1.direct_prospect_id = 1257816
union	
	select 
		q1.direct_prospect_id as lead_entrante,
		q2.direct_prospect_id as lead_carteira,
		'email solicitante' as check_mark
	from public.direct_prospects q1
	left join (select distinct lower(email) as email from public.parceiros_parceiro) pp_email on pp_email.email = lower(q1.email)
	left join lucas_leal.v_antifraude_whitelist_email as wemail on wemail.email = q1.email
	left join (select distinct identificacao from public.parceiros_parceiro) pp_utm_source on pp_utm_source.identificacao = replace(q1.utm_source,'#/','')
	join public.direct_prospects q2 on lower(q2.email) = lower(q1.email) 
		 and q1.direct_prospect_id > q2.direct_prospect_id
	where coalesce(pp_email.email,pp_utm_source.identificacao,wemail.email) is null
		and q1.email ~ '.*@.*'
and q1.direct_prospect_id = 1257816
union	
	select 
		q1.direct_prospect_id as lead_entrante,
		q2.direct_prospect_id as lead_carteira,
		'cnpj coligada serasa' as check_mark
	from public.direct_prospects q1 
	join lucas_leal.t_experian_coligadas_full t1 on left(t1.coligadas_cnpj,8) = left(q1.cnpj,8)
	join public.direct_prospects q2 on q2.direct_prospect_id = t1.direct_prospect_id  and q1.direct_prospect_id > q2.direct_prospect_id
	where q1.cnpj ~ '\d{14}'
and q1.direct_prospect_id = 1257816
union	
	select 
		q1.direct_prospect_id as lead_entrante,
		q2.direct_prospect_id as lead_carteira,
		'cnpj coligada total neoway pf' as check_mark
	from public.direct_prospects q1 
	join lucas_leal.t_neoway_pf_pariticipacao_societaria_data_full t1 on left(t1.participacao_societaria_cnpj,8) = left(q1.cnpj,8)
	join public.direct_prospects q2 on q2.direct_prospect_id = t1.direct_prospect_id  and q1.direct_prospect_id > q2.direct_prospect_id
	where q1.cnpj ~ '\d{14}'
and q1.direct_prospect_id = 1257816
union	
	select 
		q1.direct_prospect_id as lead_entrante,
		q2.direct_prospect_id as lead_carteira,
		'cnpj coligada ativa neoway pf' as check_mark
	from public.direct_prospects q1 
	join lucas_leal.t_neoway_pf_pariticipacao_societaria_data_full t1 on left(t1.participacao_societaria_cnpj,8) = left(q1.cnpj,8)
	join public.direct_prospects q2 on q2.direct_prospect_id = t1.direct_prospect_id  and q1.direct_prospect_id > q2.direct_prospect_id
	where q1.cnpj ~ '\d{14}'
		and t1.participacao_societaria_situacao = 'ATIVA'
and q1.direct_prospect_id = 1257816
union	
	select 
		q1.direct_prospect_id as lead_entrante,
		q2.direct_prospect_id as lead_carteira,
		'cnpj coligada neoway pj' as check_mark
	from public.direct_prospects q1 
	join lucas_leal.t_neoway_pj_empresas_coligadas_full t1 on left(t1.cnpj,8) = left(q1.cnpj,8)
	join public.direct_prospects q2 on q2.direct_prospect_id = t1.direct_prospect_id  and q1.direct_prospect_id > q2.direct_prospect_id
	where q1.cnpj ~ '\d{14}'
and q1.direct_prospect_id = 1257816
union	
	select 
		q1.direct_prospect_id as lead_entrante,
		q2.direct_prospect_id as lead_carteira,
		'telefone neoway pf' as check_mark
	from public.direct_prospects q1 
	join lucas_leal.t_neoway_pf_telephone_data_full t1 on t1.telefone = replace(replace(replace(replace(q1.phone,'(',''),')',''),'-',''),' ','')
	join public.direct_prospects q2 on q2.direct_prospect_id = t1.direct_prospect_id  and q1.direct_prospect_id > q2.direct_prospect_id
	where q1.direct_prospect_id = 1257816
union	
	select 
		q1.direct_prospect_id as lead_entrante,
		q2.direct_prospect_id as lead_carteira,
		'telefone solicitante' as check_mark
	from public.direct_prospects q1
	left join (select distinct replace(replace(replace(replace(phone,'(',''),')',''),'-',''),' ','') as phone from public.parceiros_parceiro) pp_phone on pp_phone.phone = replace(replace(replace(replace(q1.phone,'(',''),')',''),'-',''),' ','')
	left join lucas_leal.v_antifraude_whitelist_phone as wphone on wphone.phone = replace(replace(replace(replace(q1.phone,'(',''),')',''),'-',''),' ','')
	left join (select distinct identificacao from public.parceiros_parceiro) pp_utm_source on pp_utm_source.identificacao = replace(q1.utm_source,'#/','')
	join public.direct_prospects q2 on replace(replace(replace(replace(q2.phone,'(',''),')',''),'-',''),' ','') = replace(replace(replace(replace(q1.phone,'(',''),')',''),'-',''),' ','') 
		 and q1.direct_prospect_id > q2.direct_prospect_id
	where coalesce(pp_phone.phone,pp_utm_source.identificacao,wphone.phone) is null	
and q1.direct_prospect_id = 1257816
union	
	select 
		q1.direct_prospect_id as lead_entrante,
		q2.direct_prospect_id as lead_carteira,
		'ip solicitante' as check_mark
	from public.direct_prospects q1
	left join (select distinct left(register_ip::text,position('/' in register_ip::text) - 1) as register_ip from public.parceiros_parceiro) pp_ip on pp_ip.register_ip = q1.opt_in_ip
	left join lucas_leal.v_antifraude_whitelist_ip as wip on wip.opt_in_ip = q1.opt_in_ip 	
	left join (select distinct identificacao from public.parceiros_parceiro) pp_utm_source on pp_utm_source.identificacao = replace(q1.utm_source,'#/','')
	join public.direct_prospects q2 on q2.opt_in_ip = q1.opt_in_ip 
		 and q1.direct_prospect_id > q2.direct_prospect_id
	where coalesce(pp_ip.register_ip,pp_utm_source.identificacao,wip.opt_in_ip) is null
and q1.direct_prospect_id = 1257816
union
	select 
		q1.direct_prospect_id as lead_entrante,
		q2.direct_prospect_id as lead_carteira,
		'mesmo endereco solicitante' as check_mark
	from public.direct_prospects q1
	left join lucas_leal.v_antifraude_whitelist_endereco wend on wend.endereco = upper(coalesce(q1.city,'') || coalesce(q1.street,'') || coalesce(q1."number",'') || coalesce(q1.complement,''))
	join public.direct_prospects q2 on upper(coalesce(q2.city,'') || coalesce(q2.street,'') || coalesce(q2."number",'') || coalesce(q2.complement,'')) = upper(coalesce(q1.city,'') || coalesce(q1.street,'') || coalesce(q1."number",'') || coalesce(q1.complement,''))
		 and q1.direct_prospect_id > q2.direct_prospect_id
	where coalesce(q2.street,'') || coalesce(q2."number",'') || coalesce(q2.complement,'') != ''
		and wend.endereco is null
and q1.direct_prospect_id = 1257816
union
	select 
		q1.direct_prospect_id as lead_entrante,
		q2.direct_prospect_id as lead_carteira,
		'mesmo cnpj relacionado admin' as check_mark
	from public.direct_prospects q1
	join lucas_leal.t_antifraude_cnpjs_relacionados_admin q2 on left(q1.cnpj,8) = left(q2.cnpjs_relacionados,8) and q1.direct_prospect_id > q2.direct_prospect_id
	where q1.cnpj ~ '\d{14}'
and q1.direct_prospect_id = 1257816
union
	select 
		q1.direct_prospect_id as lead_entrante,
		q2.direct_prospect_id as lead_carteira,
		'mesmo cpf relacionado admin' as check_mark
	from public.direct_prospects q1
	join lucas_leal.t_antifraude_cpfs_relacionados_admin q2 on q1.cpf = q2.cpfs_relacionados and q1.direct_prospect_id > q2.direct_prospect_id
	where q1.cnpj ~ '\d{11}'
and q1.direct_prospect_id = 1257816
) as s1
join public.direct_prospects s2 on s2.direct_prospect_id = s1.lead_entrante
join public.direct_prospects s3 on s3.direct_prospect_id = s1.lead_carteira
left join data_science.loan_tape lt on lt.lead_id = s1.lead_carteira
left join (select 
		legacy_target_id,
		string_agg(motivo_rejeicao,',') as motivo_rejeicao,
        count(*) filter (where motivo_rejeicao = 'Suspeita de Fraude') as suspeita_fraude
	from lucas_leal.t_lead_tape_credit_underwrite_selected_motives_full
	group by 1) as s4 on s4.legacy_target_id = s1.lead_carteira	
group by 1,2




