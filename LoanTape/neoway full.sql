CREATE INDEX t_neoway_pj_choose_report_full_lead_id ON lucas_leal.t_neoway_pj_choose_report_full USING btree (direct_prospect_id);
CREATE INDEX t_neoway_pj_choose_report_full_coleta_id ON lucas_leal.t_neoway_pj_choose_report_full USING btree (id);
insert into lucas_leal.t_neoway_pj_choose_report_full 
(select dp.direct_prospect_id,
    	cc.id,
		row_number() over (partition by dp.direct_prospect_id order by cc.data_coleta desc) as indice_ultimo_antes_data_referencia
	from public.credito_coleta cc
	join public.direct_prospects dp ON "left"(cc.documento::text, 8) = "left"(dp.cnpj::text, 8)
	join lucas_leal.v_funil_completo_sem_duplicacao vfcsd on vfcsd.direct_prospect_id = dp.direct_prospect_id 
    left join public.offers o on o.offer_id = vfcsd.offer_id 
    left join public.loan_requests lr on lr.loan_request_id = vfcsd.loan_request_id
	left join (select distinct direct_prospect_id from lucas_leal.t_neoway_pj_choose_report_full) t1 on t1.direct_prospect_id = dp.direct_prospect_id 
	where cc.documento_tipo::text = 'CNPJ'::text 
		and cc.tipo::text = 'Neoway'
		and cc.data_coleta::date <= coalesce(lr.loan_date,coalesce(lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '10 days')::date
		and t1.direct_prospect_id is null
	)

select count(*),count(distinct direct_prospect_id)
from lucas_leal.t_neoway_pj_choose_report_full
where indice_ultimo_antes_data_referencia = 1

select
    t1.direct_prospect_id,
    (t2.direct_prospect_id is not null)::int as substituir
from(select dp.direct_prospect_id,
	cc.id,
	row_number() over (partition by dp.direct_prospect_id order by cc.data_coleta desc) as indice_ultimo_antes_data_referencia
from credito_coleta cc
join direct_prospects dp ON "left"(cc.documento::text, 8) = "left"(dp.cnpj::text, 8)
join lucas_leal.v_funil_completo_sem_duplicacao vfcsd on vfcsd.direct_prospect_id = dp.direct_prospect_id 
left join offers o on o.offer_id = vfcsd.offer_id 
left join loan_requests lr on lr.loan_request_id = vfcsd.loan_request_id
where cc.documento_tipo::text = 'CNPJ'::text 
	and cc.tipo::text = 'Neoway'
	and cc.data_coleta::date <= coalesce(lr.loan_date,coalesce(lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '10 days')::date) as t1
left join lucas_leal.t_neoway_pj_choose_report_full t2 on t2.direct_prospect_id = t1.direct_prospect_id and t2.indice_ultimo_antes_data_referencia = 1
where t1.indice_ultimo_antes_data_referencia = 1 and t1.id != coalesce(t2.id,0000)

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE unique INDEX t_neoway_pj_materialidade_full_lead_id ON lucas_leal.t_neoway_pj_materialidade_full USING btree (direct_prospect_id)
CREATE INDEX t_neoway_pj_materialidade_full_coleta_id ON lucas_leal.t_neoway_pj_materialidade_full USING btree (coleta_id);
insert into lucas_leal.t_neoway_pj_materialidade_full
select
	t1.direct_prospect_id,
	t1.id as coleta_id,
	(cc.data->'_metadata'->'empresas'->'_metadata'->'licencasAmbientais'->'source' is not null)::int as tem_licenca_ambiental,
	(cc.data->'_metadata'->'empresas'->'_metadata'->'aeronaves'->'source' is not null)::int as tem_aeronave,
	coalesce(jsonb_array_length(cc.data->'inpiMarcas'),'0')::bigint as count_inpi_marcas,
	coalesce((cc.data->'detran'->>'totalVeiculos'),'0')::bigint as total_veiculos,
	coalesce(jsonb_array_length(cc.data->'imoveis'),'0')::bigint as count_imoveis,
	coalesce((cc.data->'totalObras'->>'quantidadeArts'),'0')::bigint as count_obras,
	(coalesce(position('EXPORTACAO' in cc.data->>'indicadoresFinanceiros'),0) > 0)::int as tem_exportacao,
	coalesce(jsonb_array_length(cc.data->'pat'),'0')::bigint as count_pat
from credito_coleta cc
join lucas_leal.t_neoway_pj_choose_report_full t1 on t1.id = cc.id
left join lucas_leal.t_neoway_pj_materialidade_full t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and t2.direct_prospect_id is null
	
select count(*),count(distinct direct_prospect_id)
from lucas_leal.t_neoway_pj_materialidade_full
	
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
create INDEX t_neoway_pj_empresas_coligadas_full_cnpj ON lucas_leal.t_neoway_pj_empresas_coligadas_full USING btree (cnpj);
create INDEX t_neoway_pj_empresas_coligadas_full_lead_id ON lucas_leal.t_neoway_pj_empresas_coligadas_full USING btree (direct_prospect_id);
create INDEX t_neoway_pj_empresas_coligadas_full_coleta_id ON lucas_leal.t_neoway_pj_empresas_coligadas_full USING btree (coleta_id);
insert into lucas_leal.t_neoway_pj_empresas_coligadas_full
(select 
	t1.direct_prospect_id,
	t1.id as coleta_id,
	coligadas->>'uf' as uf,
	coligadas->>'cnae' as cnae,
	coligadas->>'cnpj' as cnpj,
	coligadas->>'municipio' as municipio,
	coligadas->>'codigoCnae' as codigoCnae,
	coligadas->>'razaoSocial' as razaoSocial,
	(coligadas->>'dataAbertura')::date as dataAbertura
from credito_coleta cc,
	jsonb_array_elements(cc.data->'empresasColigadas') as coligadas,
	lucas_leal.t_neoway_pj_choose_report_full t1
left join (select distinct 
		direct_prospect_id 
	from lucas_leal.t_neoway_pj_empresas_coligadas_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and t1.id = cc.id
	and t2.direct_prospect_id is null
	)

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

create INDEX t_neoway_pj_socios_full_cpf ON lucas_leal.t_neoway_pj_socios_full USING btree (documento);
create INDEX t_neoway_pj_socios_full_lead_id ON lucas_leal.t_neoway_pj_socios_full USING btree (direct_prospect_id);
create INDEX t_neoway_pj_socios_full_coleta_id ON lucas_leal.t_neoway_pj_socios_full USING btree (coleta_id);
insert into lucas_leal.t_neoway_pj_socios_full
(select 
	t1.direct_prospect_id,
	t1.id as coleta_id,
	socios->>'nome' as nome,
	socios->>'falecido' as falecido,
	socios->>'nivelPep' as nivelPep,
	socios->>'documento' as documento,
	socios->>'qualificacao' as qualificacao,
	(socios->>'participacaoSocietaria')::numeric as participacaoSocietaria
from credito_coleta cc,
	jsonb_array_elements(cc.data->'socios') as socios,
	lucas_leal.t_neoway_pj_choose_report_full t1
left join (select distinct 
		direct_prospect_id 
	from lucas_leal.t_neoway_pj_socios_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and t1.id = cc.id
	and t2.direct_prospect_id is null
	)


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


create INDEX t_neoway_pf_choose_report_full_lead_id ON lucas_leal.t_neoway_pf_choose_report_full USING btree (direct_prospect_id);
create INDEX t_neoway_pf_choose_report_full_coleta_id ON lucas_leal.t_neoway_pf_choose_report_full USING btree (id);
create INDEX t_neoway_pf_choose_report_full_cpf ON lucas_leal.t_neoway_pf_choose_report_full USING btree (documento);
insert into lucas_leal.t_neoway_pf_choose_report_full
(select dp.direct_prospect_id,
    	cc.id,
    	cc.documento,
		row_number() over (partition by dp.direct_prospect_id,cc.documento order by cc.data_coleta desc) as indice_ultimo_antes_data_referencia
	from credito_coleta cc
	join public.loan_requests lr on lr.loan_request_id = cc.origem_id
	join public.offers o on o.offer_id = lr.offer_id 
	join public.direct_prospects dp on dp.client_id = o.client_id 
	join lucas_leal.v_funil_completo_sem_duplicacao vfcsd on vfcsd.direct_prospect_id = dp.direct_prospect_id 
	join public.offers o2 on o2.offer_id = vfcsd.offer_id 
	join public.loan_requests lr2 on lr2.loan_request_id = vfcsd.loan_request_id 
	left join (select distinct direct_prospect_id from lucas_leal.t_neoway_pf_choose_report_full) tspcrf on tspcrf.direct_prospect_id = dp.direct_prospect_id 
    where cc.documento_tipo::text = 'CPF'::text 
		and cc.tipo::text = 'Neoway'
		and cc.data_coleta::date <= coalesce(lr2.loan_date,coalesce(lr2.date_inserted,o2.date_inserted,dp.opt_in_date)::date + interval '10 days')::date
		and tspcrf.direct_prospect_id is null
	)
	

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



create INDEX t_neoway_pf_identity_data_full_lead_id ON lucas_leal.t_neoway_pf_identity_data_full USING btree (direct_prospect_id);
create INDEX t_neoway_pf_identity_data_full_cpf ON lucas_leal.t_neoway_pf_identity_data_full USING btree (documento);
create INDEX t_neoway_pf_identity_data_full_emprego_rais_telefone ON lucas_leal.t_neoway_pf_identity_data_full USING btree (emprego_rais_telefone);
insert into lucas_leal.t_neoway_pf_identity_data_full 
(select 
	t1.direct_prospect_id,
	t1.documento,
	(cc.data->>'cpf') as cpf,
	(cc.data->>'pis') as pis,
	(cc.data->>'nome') as nome,
	(cc.data->>'sexo') as genero,
	(cc.data->'cafir'->>'areaTotal')::numeric as cafir_area_total,
	(cc.data->'cafir'->>'numeroImoveisTitular')::int as cafir_numero_imoveis_titular,
	(cc.data->'cafir'->>'numeroImoveisCondomino')::int as cafir_numero_imoveis_condominio,
	(cc.data->>'idade')::int as idade,
	(cc.data->>'nomeMae') as nome_mae,
	(cc.data->'endereco'->>'uf') as endereco_uf,
	(cc.data->'endereco'->>'cep') as endereco_cep,
	(cc.data->'endereco'->>'municipio') as endereco_municipio,
	(cc.data->>'falecido')::bool as falecido,
	(cc.data->>'situacaoCpf') as situacao_cpf,
	(cc.data->>'dataNascimento')::date as data_nascimento,
	(cc.data->>'cpfDataInscricao') as cpf_data_inscricao,
	(cc.data->>'falecidoConfirmado')::bool as falecido_confirmado,
	(cc.data->>'qtdVeiculosPesados')::int as qtd_veiculos_pesados,
	cc.data->'enderecoEmpregoRaisNovo'->> 'cnpj' as emprego_rais_cnpj,
	cc.data->'enderecoEmpregoRaisNovo'->> 'razaoSocial' as emprego_rais_razao_social,
	cc.data->'enderecoEmpregoRaisNovo'->> 'uf' as emprego_rais_uf,
	cc.data->'enderecoEmpregoRaisNovo'->> 'municipio' as emprego_rais_municipio,
	cc.data->'enderecoEmpregoRaisNovo'->> 'logradouro' as emprego_rais_logradouro,
	cc.data->'enderecoEmpregoRaisNovo'->> 'numero' as emprego_rais_numero,
	cc.data->'enderecoEmpregoRaisNovo'->> 'complemento' as emprego_rais_complemento,
	cc.data->'enderecoEmpregoRaisNovo'->> 'bairro' as emprego_rais_bairro,
	cc.data->'enderecoEmpregoRaisNovo'->> 'cep' as emprego_rais_cep,
	cc.data->'enderecoEmpregoRaisNovo'->> 'descricaoCnae' as emprego_rais_cnae,
	(cc.data->'enderecoEmpregoRaisNovo'->> 'EnderecoResidencial')::bool as emprego_rais_endereco_residencial,
	cc.data->'enderecoEmpregoRaisNovo'->> 'faixaFaturamento' as emprego_rais_faixa_faturamento,
	cc.data->'enderecoEmpregoRaisNovo'->> 'precisaoGeo' as emprego_rais_precisao_geo,
	(cc.data->'enderecoEmpregoRaisNovo'->> 'quantidadeFuncionarios')::bigint as emprego_rais_qtd_funcionarios,
	cc.data->'enderecoEmpregoRaisNovo'->> 'ramoAtividade' as emprego_rais_ramo_atividade,
	cc.data->'enderecoEmpregoRaisNovo'->> 'telefone' as emprego_rais_telefone
from credito_coleta cc
join lucas_leal.t_neoway_pf_choose_report_full t1 on t1.id = cc.id
left join lucas_leal.t_neoway_pf_identity_data_full t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and t2.direct_prospect_id is null
)


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


create INDEX t_neoway_pf_telephone_data_full_lead_id ON lucas_leal.t_neoway_pf_telephone_data_full USING btree (direct_prospect_id);
create INDEX t_neoway_pf_telephone_data_full_cpf ON lucas_leal.t_neoway_pf_telephone_data_full USING btree (documento);
create INDEX t_neoway_pf_telephone_data_full_telefone ON lucas_leal.t_neoway_pf_telephone_data_full USING btree (telefone);
insert into lucas_leal.t_neoway_pf_telephone_data_full 
(select 
	t1.direct_prospect_id,
	t1.documento,
	(telefone::jsonb->>'numero') as telefone
from credito_coleta cc
join lucas_leal.t_neoway_pf_choose_report_full t1 on t1.id = cc.id
left join jsonb_array_elements(cc.data->'telefones') as telefone on true
left join (select distinct direct_prospect_id  from lucas_leal.t_neoway_pf_telephone_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and (telefone::jsonb->>'numero') is not null
	and t2.direct_prospect_id is null
)


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


create INDEX t_neoway_pf_outros_endereco_data_full_lead_id ON lucas_leal.t_neoway_pf_outros_endereco_data_full USING btree (direct_prospect_id);
create INDEX t_neoway_pf_outros_endereco_data_full_cpf ON lucas_leal.t_neoway_pf_outros_endereco_data_full USING btree (documento);
insert into lucas_leal.t_neoway_pf_outros_endereco_data_full 
(select 
	t1.direct_prospect_id,
	t1.documento,
	(enderecooutros->>'cep') as outros_enderecos_cep,
	(enderecooutros->>'uf') as outros_enderecos_uf,
	(enderecooutros->>'municipio') as outros_enderecos_municipio
from credito_coleta cc
join lucas_leal.t_neoway_pf_choose_report_full t1 on t1.id = cc.id
left join jsonb_array_elements(cc.data->'enderecoOutros') as enderecoOutros on true
left join (select distinct direct_prospect_id from lucas_leal.t_neoway_pf_outros_endereco_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and coalesce((enderecooutros->>'cep'),(enderecooutros->>'uf'),(enderecooutros->>'municipio')) is not null
	and t2.direct_prospect_id is null
)


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


create INDEX t_neoway_pf_pariticipacao_societaria_data_full_lead_id ON lucas_leal.t_neoway_pf_pariticipacao_societaria_data_full USING btree (direct_prospect_id);
create INDEX t_neoway_pf_pariticipacao_societaria_data_full_cpf ON lucas_leal.t_neoway_pf_pariticipacao_societaria_data_full USING btree (documento);
create INDEX t_neoway_pf_pariticipacao_societaria_data_full_cnpj ON lucas_leal.t_neoway_pf_pariticipacao_societaria_data_full USING btree (participacao_societaria_cnpj);
insert into lucas_leal.t_neoway_pf_pariticipacao_societaria_data_full 
(select 
	t1.direct_prospect_id,
	t1.documento,
	(participacaosocietaria->>'cnpj') as participacao_societaria_cnpj,
	(participacaosocietaria->>'situacao') as participacao_societaria_situacao
from credito_coleta cc
join lucas_leal.t_neoway_pf_choose_report_full t1 on t1.id = cc.id
left join jsonb_array_elements(cc.data->'participacaoSocietaria') as participacaoSocietaria on true
left join (select direct_prospect_id from lucas_leal.t_neoway_pf_pariticipacao_societaria_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and (participacaosocietaria->>'cnpj') is not null
	and t2.direct_prospect_id is null
)


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


create INDEX t_neoway_pf_historico_funcional_data_full_lead_id ON lucas_leal.t_neoway_pf_historico_funcional_data_full USING btree (direct_prospect_id);
create INDEX t_neoway_pf_historico_funcional_data_full_cpf ON lucas_leal.t_neoway_pf_historico_funcional_data_full USING btree (documento);
create INDEX t_neoway_pf_historico_funcional_data_full_cnpj ON lucas_leal.t_neoway_pf_historico_funcional_data_full USING btree (cnpj);
insert into lucas_leal.t_neoway_pf_historico_funcional_data_full 
(select 
	t1.direct_prospect_id,
	t1.documento,
	historicoFuncional->>'cnpj' as cnpj,
	historicoFuncional->>'razaoSocial' as razaoSocial,
	(historicoFuncional->>'dataAdmissao')::date as dataAdmissao,
	(historicoFuncional->>'dataDesligamento')::date as dataDesligamento,
	(historicoFuncional->>'numeroMesesEmpresa')::bigint as numeroMesesEmpresa	
from credito_coleta cc
join lucas_leal.t_neoway_pf_choose_report_full t1 on t1.id = cc.id
left join jsonb_array_elements(cc.data->'historicoFuncional') as historicoFuncional on true
left join (select distinct direct_prospect_id from lucas_leal.t_neoway_pf_historico_funcional_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and historicoFuncional->>'cnpj' is not null
	and t2.direct_prospect_id is null
)


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


create INDEX t_neoway_pf_historico_funcional_data_full_lead_id ON lucas_leal.t_neoway_pf_historico_funcional_data_full USING btree (direct_prospect_id);
create INDEX t_neoway_pf_historico_funcional_data_full_cpf ON lucas_leal.t_neoway_pf_historico_funcional_data_full USING btree (documento);
create INDEX t_neoway_pf_historico_funcional_data_full_cnpj ON lucas_leal.t_neoway_pf_historico_funcional_data_full USING btree (cnpj);
insert into lucas_leal.t_neoway_pf_historico_funcional_data_full 
(select 
	t1.direct_prospect_id,
	t1.documento,
	historicoFuncional->>'cnpj' as cnpj,
	historicoFuncional->>'razaoSocial' as razaoSocial,
	(historicoFuncional->>'dataAdmissao')::date as dataAdmissao,
	(historicoFuncional->>'dataDesligamento')::date as dataDesligamento,
	(historicoFuncional->>'numeroMesesEmpresa')::bigint as numeroMesesEmpresa	
from credito_coleta cc
join lucas_leal.t_neoway_pf_choose_report_full t1 on t1.id = cc.id
left join jsonb_array_elements(cc.data->'historicoFuncional') as historicoFuncional on true
left join (select distinct direct_prospect_id from lucas_leal.t_neoway_pf_historico_funcional_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and historicoFuncional->>'cnpj' is not null
	and t2.direct_prospect_id is null
)


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


create INDEX t_neoway_pj_cnaes_data_full_lead_id ON lucas_leal.t_neoway_pj_cnaes_data_full USING btree (direct_prospect_id);
create INDEX t_neoway_pj_cnaes_data_full_cnpj ON lucas_leal.t_neoway_pj_cnaes_data_full USING btree (cnpj);
create INDEX t_neoway_pj_cnaes_data_full_coleta_id ON lucas_leal.t_neoway_pj_cnaes_data_full USING btree (coleta_id);
insert into lucas_leal.t_neoway_pj_cnaes_data_full 
select
	t1.direct_prospect_id,
	cc.data ->> 'cnpj' as cnpj,
	t1.id as coleta_id,
	cnaes ->> 'codigo' as cnae_codigo,
	cnaes ->> 'descricao' as cnae_descricao
from credito_coleta cc
join jsonb_array_elements(cc.data->'cnaes') as cnaes on true
join lucas_leal.t_neoway_pj_choose_report_full t1 on t1.id = cc.id
left join (select distinct direct_prospect_id from lucas_leal.t_neoway_pj_cnaes_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and t2.direct_prospect_id is null


-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	
create INDEX t_neoway_pj_cnds_data_full_lead_id ON lucas_leal.t_neoway_pj_cnds_data_full USING btree (direct_prospect_id);
create INDEX t_neoway_pj_cnds_data_full_cnpj ON lucas_leal.t_neoway_pj_cnds_data_full USING btree (cnpj);
create INDEX t_neoway_pj_cnds_data_full_coleta_id ON lucas_leal.t_neoway_pj_cnds_data_full USING btree (coleta_id);
--insert into lucas_leal.t_neoway_pj_cnds_data_full 
create table lucas_leal.t_neoway_pj_cnds_data_full as
select
	t1.direct_prospect_id,
	cc.data ->> 'cnpj' as cnpj,
	t1.id as coleta_id,
	cnds ->> 'nome' as cnd_nome,
	cnds ->> 'descricaoSituacao' as cnd_descricaoSituacao,
	cnds ->> 'dataEmissao' as cnd_dataEmissao,
	cnds ->> 'dataValidade' as cnd_dataValidade,
	cnds ->> 'numeroCertificacao' as cnd_numeroCertificacao
from credito_coleta cc
join jsonb_array_elements(cc.data->'empresaSaudeTributaria'->'cnds') as cnds on true
join lucas_leal.t_neoway_pj_choose_report_full t1 on t1.id = cc.id
--left join (select distinct direct_prospect_id from lucas_leal.t_neoway_pj_cnds_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
--	and t2.direct_prospect_id is null



-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	
select
	t1.direct_prospect_id,
	t1.documento as cnpj,
	t1.id as coleta_id,
	cc.data -> 'cvm' ->> 'cadastro' as cvm_cadatro,
	cc.data -> 'cvm' ->> 'anoBalanco' as cvm_anoBalanco,
	cc.data -> 'cvm' ->> 'ativoTotal' as cvm_ativoTotal,
	cc.data -> 'cvm' ->> 'cadastro' as cvm_cadastro,
	cc.data -> 'cvm' ->> 'faturamentoBruto' as cvm_faturamentoBruto,
	cc.data -> 'cvm' ->> 'faturamentoLiquido' as cvm_faturamentoLiquido,
	cc.data -> 'cvm' ->> 'lucroBruto' as cvm_lucroBruto,
	cc.data -> 'cvm' ->> 'lucroLiquido' as cvm_lucroLiquido,
	cc.data -> 'cvm' ->> 'patrimonioLiquido' as cvm_patrimonioLiquido,
	cc.data -> 'cvm' ->> 'emailRF' as info_emailRF,
	cc.data -> 'info' ->> 'emailRF' as info_emailRF,
	cc.data -> 'info' ->> 'idadeEmpresa' as info_idadeEmpresa,
	cc.data -> 'info' ->> 'numeroTelefoneRF' as info_numeroTelefoneRF,
	cc.data -> 'info' ->> 'qsaDivergente' as info_qsaDivergente,
	cc.data -> 'info' ->> 'valorDividaPgfnDau' as info_valorDividaPgfnDau,
from credito_coleta cc
join lucas_leal.t_neoway_pj_choose_report_full t1 on t1.id = cc.id
where t1.indice_ultimo_antes_data_referencia = 1


select 
	jsonb_object_keys(jsonb_array_elements(cc.data->'empresaSaudeTributaria'->'cnds')),
	count(*)
from credito_coleta cc
join lucas_leal.t_neoway_pj_choose_report_full t1 on t1.id = cc.id
--join lucas_leal.mv_loan_tape_new mltn on mltn.lead_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1  
group by 1


'empresaSaudeTributaria'

_metadata
detran
matriz
potencialConsumo
endereco
cnaePrincipal
dataAbertura
razaoSocial
situacao
totalObras
natureza
socios
simplesNacional
beneficiarios
fantasia
totalFuncionarios
activityLevelV2
empresa
cnpj
faturamentoEstimado
telefones
faturamentoPresumido
saudeTributaria
empresaSintegra
nivelAtividade
empresaSaudeTributaria
sintegra
cnds
sociosJunta
beneficiariosJunta
crescimentoPorAno
empresasColigadas
funcionarios
exfuncionarios
tributaryHealth
empresaPgfnDau
veiculos
processoJudicialTotalizadores
filiais
arts
inpiMarcas
crescimentoPorAnoRais
empresaAntt
empresaRegistroBr
patCadastro
dominios
pat
imoveis
antt
calc
empresaDatasusEstabelecimento
susep
escolas
cnes
postosCombustiveis
veiculosPesados
indicadoresFinanceiros
postosCombustivel
empresaPontoAbastecimento
pontosAbastecimento
empresaCeis
message
ceis
faculdades
empresaEmecCurso
cepim
cvmParticipantes
error









-- A FAZER :
	
create table lucas_leal.t_neoway_pf_endereco_emprego_rais_novo_full as 

select distinct 
--	t1.direct_prospect_id,
--	t1.documento,
	jsonb_object_keys(participacaoSocietariaRF)
from credito_coleta cc
join lucas_leal.t_neoway_pf_choose_report_full t1 on t1.id = cc.id
left join jsonb_array_elements(cc.data->'participacaoSocietariaRF') as participacaoSocietariaRF on true
--left join lucas_leal.t_neoway_pf_data_full t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
--	and t2.direct_prospect_id is null


create table lucas_leal.t_neoway_pf_endereco_emprego_rais_novo_full as 
select 
	t1.direct_prospect_id,
	t1.documento,
	participacaoSocietariaUnico.*
from credito_coleta cc
join lucas_leal.t_neoway_pf_choose_report_full t1 on t1.id = cc.id
--left join jsonb_array_elements(cc.data->'participacaoSocietariaRF') as participacaoSocietariaRF on true
--left join jsonb_array_elements(cc.data->'participacaoSocietariaUnico') as participacaoSocietariaUnico on true
--left join lucas_leal.t_neoway_pf_data_full t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
--	and t2.direct_prospect_id is null


insert into lucas_leal.t_client_addresses_full
select 
	lr.loan_request_id,
	a.zip_code,
	a.street,
	a."number",
	a.complement,
	a.neighbourhood,
	a.client_id,
	c."name" as cidade,
	row_number() over (partition by lr.loan_request_id order by a.address_id) as indice_endereco
from public.client_addresses a
left join public.cities c on c.city_id = a.city_id 
join public.loan_requests lr on lr.address_id = a.address_id 
left join lucas_leal.t_client_addresses_full as t1 on t1.loan_request_id = lr.loan_request_id 
where street != 'ADICIONAR ENDERECO'
	and t1.loan_request_id is null

	
	
