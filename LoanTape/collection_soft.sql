CREATE INDEX t_collection_soft_emprestimo_id ON lucas_leal.t_collection_soft USING btree (emprestimo_id);
CREATE INDEX t_collection_soft_dia ON lucas_leal.t_collection_soft USING btree (dia);

truncate t_collection_soft

insert into t_collection_soft
SELECT 
	pgto.emprestimo_id,
	pgto.indice_pgto as parcela,
	w.dia,
	case when w.dia < coalesce(subst.dt_vcto_subst,vcto.dt_vcto) then 
			case when pgto.dt_pgto_subst < w.dia then
					pgto.dt_pgto_subst - coalesce(subst.dt_vcto_subst,vcto.dt_vcto) 
				else 0
			end
        else 
        	least(pgto.dt_pgto_subst, w.dia) - coalesce(subst.dt_vcto_subst,vcto.dt_vcto)
    end as atraso_max,
	case when w.dia < coalesce(subst.dt_vcto_subst,vcto.dt_vcto) then 
			case when pgto.dt_pgto_subst < w.dia then
					pgto.dt_pgto_subst - coalesce(subst.dt_vcto_subst,vcto.dt_vcto) 
				else 0
			end
        else 
        	case when w.dia > pgto.dt_pgto_subst then 0
        		else w.dia - coalesce(subst.dt_vcto_subst,vcto.dt_vcto)
        	end 
    end as atraso
from public.workdays w


join (select
		lr.loan_request_id,
		max(lr.loan_date) as dt_inicio,
		max(fp.pago_em) filter (where fpp.status = 'Pago') as dt_quitacao
	from public.loan_requests lr
	join public.financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join public.financeiro_parcela fp on fp.plano_id = fpp.id
	group by 1) as lr on w.dia between lr.dt_inicio and current_date
	
select *	
from ( 
	SELECT t1.emprestimo_id,
		t1.dt_pgto_subst,
		t1.pago,
		row_number() OVER (PARTITION BY t1.emprestimo_id ORDER BY t1.dt_pgto_subst) AS indice_pgto
	FROM ( 
		SELECT 
				fpp.emprestimo_id,
				fp.pago_em AS dt_pgto_subst,
				fp.pago
			FROM financeiro_planopagamento fpp
			JOIN financeiro_parcela fp ON fp.plano_id = fpp.id
			WHERE fp.status not in ('Cancelada','EmEspera')
		UNION ALL
			SELECT 
				emprestimo_id,
				creation_date::date AS dt_pgto_subst,
				NULL::numeric AS pago
			FROM financeiro_planopagamento
			WHERE status not in ('Cancelado','EmEspera') and type = 'POSTPONING'
			) t1
		where t1.emprestimo_id = 236
		) pgto --on pgto.emprestimo_id = lr.loan_request_id 
LEFT JOIN ( 
	SELECT vcto_1.emprestimo_id,
		vcto_1.plano_id,
		max(vcto_1.type::text) AS type,
		max(vcto_1.vencimento_em) AS dt_vcto_subst,
		sum(indice_subst.n_pgto_total) + 1::numeric + max(indice_subst.count_postponing_acum)::numeric AS indice_vcto_subst
	FROM ( 
		SELECT 
			fpp.emprestimo_id,
        	fp.plano_id,
            fpp.type,
            lucas_leal.offset_data_fim_de_semana(fp.vencimento_em) as vencimento_em,
            row_number() OVER (PARTITION BY fpp.emprestimo_id, fpp.id ORDER BY lucas_leal.offset_data_fim_de_semana(fp.vencimento_em), fp.numero) AS indice_vcto
        FROM financeiro_parcela fp
		JOIN financeiro_planopagamento fpp ON fpp.id = fp.plano_id
        WHERE fpp.status::text = 'Substituido'::text
        ) vcto_1
    JOIN ( SELECT fpp.emprestimo_id,
    		fp.plano_id,
            count(*) FILTER (WHERE fp.pago_em IS NOT NULL) AS n_pgto_plano
        FROM financeiro_parcela fp
        JOIN financeiro_planopagamento fpp ON fpp.id = fp.plano_id
        WHERE fpp.status::text <> ALL (ARRAY['Cancelado'::character varying::text, 'EmEspera'::character varying::text])
        GROUP BY fpp.emprestimo_id, fp.plano_id
        ) vcto_subst ON vcto_subst.emprestimo_id = vcto_1.emprestimo_id AND vcto_subst.plano_id = vcto_1.plano_id AND vcto_1.indice_vcto = (vcto_subst.n_pgto_plano + 1)
	JOIN ( SELECT fpp.emprestimo_id,
			fp.plano_id,
            count(*) FILTER (WHERE fp.pago_em IS NOT NULL) AS n_pgto_total,
            sum(
                CASE max(fpp.type::text)
                    WHEN 'POSTPONING'::text THEN 1
                    ELSE 0
                END) OVER (PARTITION BY fpp.emprestimo_id ORDER BY fp.plano_id) AS count_postponing_acum
		FROM financeiro_parcela fp
        JOIN financeiro_planopagamento fpp ON fpp.id = fp.plano_id
        WHERE fpp.status::text <> ALL (ARRAY['Cancelado'::character varying::text, 'EmEspera'::character varying::text])
        GROUP BY fpp.emprestimo_id, fp.plano_id
        ) indice_subst ON indice_subst.emprestimo_id = vcto_1.emprestimo_id AND indice_subst.plano_id <= vcto_1.plano_id
    GROUP BY vcto_1.emprestimo_id, vcto_1.plano_id
    ) subst ON subst.emprestimo_id = pgto.emprestimo_id AND subst.indice_vcto_subst = pgto.indice_pgto::numeric
LEFT JOIN ( SELECT fp_original.emprestimo_id,
		fp_current.indice_vcto,
        CASE fp_current.indice_vcto WHEN 1 THEN min(fp_original.vencimento_em) ELSE min(fp_current.vencimento_em) END AS dt_vcto,
		max(fp_current.type::text) AS type_vcto
    FROM ( SELECT fpp.emprestimo_id,
    		fp.plano_id,
            lucas_leal.offset_data_fim_de_semana(fp.vencimento_em) as vencimento_em
		FROM financeiro_planopagamento fpp
        JOIN financeiro_parcela fp ON fpp.id = fp.plano_id
        WHERE fpp.type::text = 'ORIGINAL'::text AND fp.numero = 1
        ) fp_original
    LEFT JOIN ( SELECT t1.emprestimo_id,
    		t1.plano_id,
            t1.vencimento_em,
            t1.indice_post,
            t1.type,
            row_number() OVER (PARTITION BY t1.emprestimo_id ORDER BY t1.plano_id, t1.vencimento_em) AS indice_vcto
        FROM ( SELECT fpp.emprestimo_id,
	        		fp.plano_id,
	                lucas_leal.offset_data_fim_de_semana(fp.vencimento_em) as vencimento_em,
	                post.indice_post,
	                fpp.type
	            FROM financeiro_planopagamento fpp
	            JOIN financeiro_parcela fp ON fpp.id = fp.plano_id
	            LEFT JOIN ( SELECT fpp_1.emprestimo_id,
	            		fpp_1.id,
	                    lucas_leal.offset_data_fim_de_semana(fp_1.vencimento_em) as vencimento_em,
	                    row_number() OVER (PARTITION BY fpp_1.emprestimo_id, fpp_1.id ORDER BY fpp_1.id, lucas_leal.offset_data_fim_de_semana(fp_1.vencimento_em)) AS indice_post
	                FROM financeiro_planopagamento fpp_1
	                JOIN financeiro_parcela fp_1 ON fp_1.plano_id = fpp_1.id
	                WHERE fpp_1.type::text = 'POSTPONING'::text AND (fpp_1.status::text <> ALL (ARRAY['Cancelado'::character varying::text, 'EmEspera'::character varying::text]))
	                ) post ON post.id = fpp.id AND post.indice_post = 1 AND post.vencimento_em = lucas_leal.offset_data_fim_de_semana(fp.vencimento_em)
	            WHERE fp.status::text <> ALL (ARRAY['Cancelada'::character varying::text, 'EmEspera'::character varying::text])
        	UNION ALL
				SELECT financeiro_planopagamento.emprestimo_id,
                	financeiro_planopagamento.id AS plano_id,
                    lucas_leal.offset_data_fim_de_semana(financeiro_planopagamento.creation_date::date) AS vencimento_em,
                    0 AS indice_post,
                    financeiro_planopagamento.type
                FROM financeiro_planopagamento
                WHERE financeiro_planopagamento.type::text = 'POSTPONING'::text AND (financeiro_planopagamento.status::text <> ALL (ARRAY['Cancelado'::character varying::text, 'EmEspera'::character varying::text]))
                ) t1
                ) fp_current ON fp_current.emprestimo_id = fp_original.emprestimo_id
          GROUP BY fp_original.emprestimo_id, fp_current.indice_vcto
          ) vcto ON vcto.emprestimo_id = pgto.emprestimo_id AND vcto.indice_vcto = pgto.indice_pgto
          
where w.dia > (select max(dia) from lucas_leal.t_collection_soft)
          
