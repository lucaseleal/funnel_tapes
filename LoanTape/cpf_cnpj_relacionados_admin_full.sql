create index t_antifraude_cnpjs_relacionados_admin_lead_id on lucas_leal.t_antifraude_cnpjs_relacionados_admin using btree(direct_prospect_id);
create index t_antifraude_cnpjs_relacionados_admin_cnpj on lucas_leal.t_antifraude_cnpjs_relacionados_admin using btree(cnpjs_relacionados);
insert into lucas_leal.t_antifraude_cnpjs_relacionados_admin 
SELECT dp.direct_prospect_id,
	unnest(string_to_array(replace(replace(dp.cnpjs_relacionados::text, '{'::text, ''::text), '}'::text, ''::text), ','::text)) AS cnpjs_relacionados
FROM direct_prospects dp
left join (select distinct direct_prospect_id from lucas_leal.t_antifraude_cnpjs_relacionados_admin) t2 on t2.direct_prospect_id = dp.direct_prospect_id 
where dp.cnpjs_relacionados::text != '{}'::text
	and t2.direct_prospect_id is null

create index t_antifraude_cpfs_relacionados_admin_lead_id on lucas_leal.t_antifraude_cpfs_relacionados_admin using btree(direct_prospect_id);
create index t_antifraude_cpfs_relacionados_admin_cpf on lucas_leal.t_antifraude_cpfs_relacionados_admin using btree(cpfs_relacionados);
insert into lucas_leal.t_antifraude_cpfs_relacionados_admin 
SELECT dp.direct_prospect_id,
	unnest(string_to_array(replace(replace(dp.cpfs_relacionados::text, '{'::text, ''::text), '}'::text, ''::text), ','::text)) AS cpfs_relacionados
FROM direct_prospects dp
left join (select distinct direct_prospect_id from lucas_leal.t_antifraude_cpfs_relacionados_admin) t2 on t2.direct_prospect_id = dp.direct_prospect_id 
where dp.cpfs_relacionados::text != '{}'::text
	and t2.direct_prospect_id is null

