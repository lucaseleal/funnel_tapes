create materialized view mv_lead_tape_bigguys_score as 
select 
	origem_id ,
	id as coleta_id,
	documento as cnpj,
	data ->> 'bizu_bg_combo_nome' as bizu_bg_combo_nome,
	(data ->> 'prob_bizu_bg_glmnet')::numeric as prob_bizu_bg_glmnet,
	(data ->> 'classe_bizu_bg_combo')::int as classe_bizu_bg_combo,
	(data ->> 'classe_bizu_bg_glmnet')::int as classe_bizu_bg_glmnet,
	(data ->> 'prob_bizu_bg_logistica')::numeric as prob_bizu_bg_logistica,
	(data ->> 'classe_bizu_bg_logistica')::int as classe_bizu_bg_logistica,
	row_number () over (partition by origem_id order by id desc) as indice
from public.credito_coleta cc 
where tipo = 'BigGuysScore'

create materialized view mv_lead_tape_infosimples_cnd_mte as 
select 
	origem_id,
	data ->> 'cpf' as cpf,
	data ->> 'cnpj' as cnpj,
	data ->> 'mensagem' as mensagem,
	data ->> 'empregador' as empregador,
	data ->> 'normalizado_cpf' as normalizado_cpf,
	data ->> 'emissao_datahora' as emissao_datahora,
	data ->> 'normalizado_cnpj' as normalizado_cnpj,
	data ->> 'codigo_autenticidade' as codigo_autenticidade,
	data ->> 'normalizado_emissao_datahora' as normalizado_emissao_datahora,
	data ->> 'conseguiu_emitir_certidao_negativa' as conseguiu_emitir_certidao_negativa
from public.credito_coleta cc 
where tipo = 'CertificateLaborDebts'


create materialized view mv_lead_tape_infosimples_cnd_fgts as
select 
	origem_id,
	documento,
	id as coleta_id,
	data ->> 'crf' as crf,
	data ->> 'datahora' as datahora,
	data ->> 'situacao' as situacao,
	data ->> 'inscricao' as inscricao,
	data ->> 'razao_social' as razao_social,
	data ->> 'validade_fim_data' as validade_fim_data,
	data ->> 'validade_inicio_data' as validade_inicio_data
from public.credito_coleta cc 
where tipo = 'CertificateOfRegularity'

create materialized view mv_lead_tape_infosimples_cnd_pgfn as
select 
	origem_id,
	documento,
	id as coleta_id,
	data ->> 'code' as crf,
	data -> 'header' ->> 'api_version' as datahora,
	data -> 'header' ->> 'service' as datahora,
	data -> 'header' -> 'parameters' ->> 'cnpj' as datahora,
	data -> 'header' -> 'parameters' ->> 'preferencia_emissao' as datahora,
	data ->> 'situacao' as situacao,
	data ->> 'inscricao' as inscricao,
	data ->> 'razao_social' as razao_social,
	data ->> 'validade_fim_data' as validade_fim_data,
	data ->> 'validade_inicio_data' as validade_inicio_data
from public.credito_coleta cc 
where tipo = 'CertificateTributaryCredit'

create unique index t_coleta_pgfn_pk on lucas_leal.t_coleta_pgfn using btree (pk);
create index t_coleta_pgfn_origem_id on lucas_leal.t_coleta_pgfn using btree (origem_id);
create unique index t_coleta_pgfn_coleta_id on lucas_leal.t_coleta_pgfn using btree (coleta_id);
insert into lucas_leal.t_coleta_pgfn
SELECT 
	origem_id || '-' || id as pk,	
	origem_id,
	id as coleta_id,
    COALESCE(data ->> 'tipo'::text, (data -> 'data'::text) ->> 'tipo'::text,
        CASE data ->> 'code'::text
            WHEN '611'::text THEN 'Indisponivel'::text
            WHEN '612'::text THEN 'Indisponivel'::text
            ELSE NULL::text
        END) AS certidao,
    data ->> 'code'::text AS codigo,
    row_number() OVER (PARTITION BY origem_id ORDER BY ((COALESCE(data ->> 'code'::text, data ->> 'tipo'::text) IS NOT NULL)::integer) DESC, data_coleta DESC) AS indice_coleta,
    data_coleta as coleta_data
   FROM public.credito_coleta 
  WHERE public.credito_coleta.tipo::text = 'CertificateTributaryCredit'::text
  	and data_coleta >= current_date - '15 days'::interval
 on conflict (pk)
 do update 
 	set
	origem_id = excluded.origem_id,
	coleta_id = excluded.coleta_id,
    codigo = excluded.codigo,
    indice_coleta = excluded.indice_coleta,
    coleta_data = excluded.coleta_data



''	:	200
''	:	A consulta foi realizada com sucesso e retornou um resultado.
	''		{20}
''	:	v1
''	:	receita-federal/pgfn
	''		{2}
''	:	03105987000105
''	:	2via
'client'	:	Bizcapital Correspondente Banc�rio LTDA
'client_name'	:	Bizcapital Correspondente Banc�rio LTDA
'token'	:	ZH8IjSfov3...
'token_name'	:	BizCapital
'billable'	:	true
'credits'	:	1
'price_extra'	:	0.04
'has_limit'	:	false
'limit'	:	0
'used'	:	0
'limit_price'	:	0.0
'used_price'	:	1021.16
'cache_hit'	:	false
'cached_at'	:	null
'requested_at'	:	2021-09-21T18:21:14.000-03:00
'elapsed_time_in_milliseconds'	:	2297
'signature'	:	U2FsdGVkX18VAN7VTmDp1D+krNeP+UiVKR17J9BQVABF/vgE3mhaluu10/nPmjQCVHUXA3B1Ue89OqHh4QiKkw==
'data_count'	:	1
	'data'		{20}
'certidao'	:	CERTID�O NEGATIVA DE D�BITOS RELATIVOS AOS TRIBUTOS FEDERAIS E � D�VIDA ATIVA DA UNI�O
'cnpj'	:	03.105.987/0001-05
'cnpj_situacao'	:	V�lida
'comprovante_tipo'	:	pdf
'consulta_comprovante'	:	899A.7FD2.DBFE.B0D2
'consulta_datahora'	:	03/09/2021 15:08:40
'cpf'	:	
'debitos_pgfn'	:	false
'debitos_rfb'	:	false
'descricao'	:	
'nome'	:	BONK COMERCIO DE FERRAMENTAS LTDA
'normalizado_cnpj'	:	03105987000105
'normalizado_consulta_datahora'	:	03/09/2021 15:08:40
'normalizado_cpf'	:	
'observacoes'	:	
'razao_social'	:	BONK COMERCIO DE FERRAMENTAS LTDA
'situacao'	:	V�lida
'tipo'	:	Negativa
'validade'	:	02/03/2022
'validade_prorrogada'	:	02/03/2022
'errors'	:	null
	'receipt'		{4}
'url'	:	https://api.infosimples.com/autenticidade
'id'	:	615701166
'key'	:	H2afousHyQbua51FSzsvL-5iluriah-439coCUwx

