
CREATE INDEX t_scr_pf_choose_report_full_direct_prospect_id ON lucas_leal.t_scr_pf_choose_report_full USING btree (direct_prospect_id);
CREATE INDEX t_scr_pf_choose_report_full_coleta_id ON lucas_leal.t_scr_pf_choose_report_full USING btree (id);

--t_scr_pf_choose_report_full
insert into lucas_leal.t_scr_pf_choose_report_full 
(select dp.direct_prospect_id,
    	coalesce(lr.loan_date,coalesce(lr.date_inserted,o.date_inserted,dp.opt_in_date) + interval '10 days')::date as data_referencia,
		cc.documento as cpf,
    	cc.id,
    	case when COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date <= coalesce(lr.loan_date,coalesce(lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '10 days') then 
    		row_number() over (partition by dp.direct_prospect_id order by (COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date <= coalesce(lr.loan_date,coalesce(lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '10 days'))::int desc,
    			COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta) desc) 
    	end as indice_ultimo_antes_data_referencia,
    	row_number() over (partition by dp.direct_prospect_id order by COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)) indice_asc,
    	row_number() over (partition by dp.direct_prospect_id order by COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta) desc) indice_desc
	from credito_coleta cc
	join direct_prospects dp on cc.documento::text = dp.cpf::text
	join lucas_leal.v_funil_completo_sem_duplicacao vfcsd on vfcsd.direct_prospect_id = dp.direct_prospect_id 
    left join offers o on o.offer_id = vfcsd.offer_id 
    left join loan_requests lr on lr.loan_request_id = vfcsd.loan_request_id
	left join (select distinct direct_prospect_id from lucas_leal.t_scr_pf_choose_report_full) tspcrf on tspcrf.direct_prospect_id = dp.direct_prospect_id 
	where cc.documento_tipo::text = 'CPF'::text 
		and cc.tipo::text = 'SCR'::text
		and tspcrf.direct_prospect_id is null
	)

	
--upsert
select
    t1.direct_prospect_id,
    (t2.direct_prospect_id is not null)::int as substituir
from(select dp.direct_prospect_id,
    	cc.id,
    	case when COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date <= coalesce(lr.loan_date,coalesce(lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '10 days') then 
    		row_number() over (partition by dp.direct_prospect_id order by (COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date <= coalesce(lr.loan_date,coalesce(lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '10 days'))::int desc,
    			COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta) desc) 
    	end as indice_ultimo_antes_data_referencia
	from credito_coleta cc
	join direct_prospects dp on cc.documento::text = dp.cpf::text
	join lucas_leal.v_funil_completo_sem_duplicacao vfcsd on vfcsd.direct_prospect_id = dp.direct_prospect_id 
    left join offers o on o.offer_id = vfcsd.offer_id 
    left join loan_requests lr on lr.loan_request_id = vfcsd.loan_request_id
	where cc.documento_tipo::text = 'CPF'::text 
		and cc.tipo::text = 'SCR'::text
	) as t1
left join lucas_leal.t_scr_pf_choose_report_full t2 on t2.direct_prospect_id = t1.direct_prospect_id and t2.indice_ultimo_antes_data_referencia = 1
where t1.indice_ultimo_antes_data_referencia = 1 and t1.id != coalesce(t2.id,0000)

        
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	
	

CREATE unique INDEX t_scr_pf_get_history_data_full_direct_prospect_id ON lucas_leal.t_scr_pf_get_history_data_full USING btree (direct_prospect_id);
insert into lucas_leal.t_scr_pf_get_history_data_full
(select
	t1.direct_prospect_id,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_curr else 0 end long_term_debt_pf_curr,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_1m else 0 end long_term_debt_pf_1m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_2m else 0 end long_term_debt_pf_2m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_3m else 0 end long_term_debt_pf_3m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_4m else 0 end long_term_debt_pf_4m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_5m else 0 end long_term_debt_pf_5m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_6m else 0 end long_term_debt_pf_6m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_7m else 0 end long_term_debt_pf_7m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_8m else 0 end long_term_debt_pf_8m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_9m else 0 end long_term_debt_pf_9m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_10m else 0 end long_term_debt_pf_10m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_11m else 0 end long_term_debt_pf_11m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_12m else 0 end long_term_debt_pf_12m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_13m else 0 end long_term_debt_pf_13m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_14m else 0 end long_term_debt_pf_14m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_15m else 0 end long_term_debt_pf_15m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_16m else 0 end long_term_debt_pf_16m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_17m else 0 end long_term_debt_pf_17m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_18m else 0 end long_term_debt_pf_18m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_19m else 0 end long_term_debt_pf_19m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_20m else 0 end long_term_debt_pf_20m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_21m else 0 end long_term_debt_pf_21m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_22m else 0 end long_term_debt_pf_22m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pf_23m else 0 end long_term_debt_pf_23m,
	case when hist_raw.thin_file is null then hist_json.ever_long_term_debt_pf else 0 end ever_long_term_debt_pf,
	case when hist_raw.thin_file is null then hist_json.max_long_term_debt_pf else 0 end max_long_term_debt_pf,
	case when hist_raw.thin_file is null then hist_json.overdue_pf_curr else 0 end overdue_pf_curr,
	case when hist_raw.thin_file is null then coalesce(hist_json.months_since_last_overdue_pf,-1) else -1 end months_since_last_overdue_pf,
	case when hist_raw.thin_file is null then hist_json.months_overdue_pf else 0 end months_overdue_pf,
	case when hist_raw.thin_file is null then hist_json.max_overdue_pf else 0 end max_overdue_pf,
	case when hist_raw.thin_file is null then hist_json.default_pf_curr else 0 end default_pf_curr,
	case when hist_raw.thin_file is null then coalesce(hist_json.months_since_last_default_pf,-1) else -1 end months_since_last_default_pf,
	case when hist_raw.thin_file is null then hist_json.months_default_pf else 0 end months_default_pf,
	case when hist_raw.thin_file is null then hist_json.max_default_pf else 0 end max_default_pf,
	case when hist_raw.thin_file is null then hist_json.qtd_meses_escopo_pf else 0 end qtd_meses_escopo_pf,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_curr else 0 end lim_cred_pf_curr,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_1m else 0 end lim_cred_pf_1m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_2m else 0 end lim_cred_pf_2m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_3m else 0 end lim_cred_pf_3m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_4m else 0 end lim_cred_pf_4m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_5m else 0 end lim_cred_pf_5m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_6m else 0 end lim_cred_pf_6m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_7m else 0 end lim_cred_pf_7m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_8m else 0 end lim_cred_pf_8m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_9m else 0 end lim_cred_pf_9m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_10m else 0 end lim_cred_pf_10m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_11m else 0 end lim_cred_pf_11m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_12m else 0 end lim_cred_pf_12m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_13m else 0 end lim_cred_pf_13m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_14m else 0 end lim_cred_pf_14m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_15m else 0 end lim_cred_pf_15m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_16m else 0 end lim_cred_pf_16m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_17m else 0 end lim_cred_pf_17m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_18m else 0 end lim_cred_pf_18m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_19m else 0 end lim_cred_pf_19m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_20m else 0 end lim_cred_pf_20m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_21m else 0 end lim_cred_pf_21m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_22m else 0 end lim_cred_pf_22m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pf_23m else 0 end lim_cred_pf_23m,
	case when hist_raw.thin_file is null then hist_json.ever_lim_cred_pf else 0 end ever_lim_cred_pf,
	case when hist_raw.thin_file is null then hist_json.max_lim_cred_pf else 0 end max_lim_cred_pf,
	case when hist_raw.thin_file is null then hist_raw.num_ops_pf else 0 end num_ops_pf,
	case when hist_raw.thin_file is null then hist_raw.num_fis_pf else 0 end num_fis_pf,
	case when hist_raw.thin_file is null then hist_raw.first_relation_fi_pf else 0 end first_relation_fi_pf
from lucas_leal.t_scr_pf_choose_report_full t1
left join (select distinct direct_prospect_id from lucas_leal.t_scr_pf_get_history_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
left join(SELECT t2.direct_prospect_id,
	    sum(t2.debtpf) FILTER (WHERE t2.data = t2.data_referencia) AS long_term_debt_pf_curr,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 mon'::interval)) AS long_term_debt_pf_1m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '2 mons'::interval)) AS long_term_debt_pf_2m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '3 mons'::interval)) AS long_term_debt_pf_3m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '4 mons'::interval)) AS long_term_debt_pf_4m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '5 mons'::interval)) AS long_term_debt_pf_5m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '6 mons'::interval)) AS long_term_debt_pf_6m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '7 mons'::interval)) AS long_term_debt_pf_7m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '8 mons'::interval)) AS long_term_debt_pf_8m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '9 mons'::interval)) AS long_term_debt_pf_9m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '10 mons'::interval)) AS long_term_debt_pf_10m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '11 mons'::interval)) AS long_term_debt_pf_11m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year'::interval)) AS long_term_debt_pf_12m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 1 mon'::interval)) AS long_term_debt_pf_13m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 2 mons'::interval)) AS long_term_debt_pf_14m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 3 mons'::interval)) AS long_term_debt_pf_15m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 4 mons'::interval)) AS long_term_debt_pf_16m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 5 mons'::interval)) AS long_term_debt_pf_17m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 6 mons'::interval)) AS long_term_debt_pf_18m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 7 mons'::interval)) AS long_term_debt_pf_19m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 8 mons'::interval)) AS long_term_debt_pf_20m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 9 mons'::interval)) AS long_term_debt_pf_21m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 10 mons'::interval)) AS long_term_debt_pf_22m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 11 mons'::interval)) AS long_term_debt_pf_23m,
	    sum(t2.debtpf) FILTER (WHERE t2.data <= t2.data_referencia) AS ever_long_term_debt_pf,
	    max(t2.debtpf) FILTER (WHERE t2.data <= t2.data_referencia) AS max_long_term_debt_pf,
	    sum(t2.vencidopf) FILTER (WHERE t2.data = t2.data_referencia) AS overdue_pf_curr,
	    date_part('year'::text, age(min(t2.data_referencia)::timestamp with time zone, max(t2.data) FILTER (WHERE t2.data <= t2.data_referencia AND t2.vencidopf > 0::double precision)::timestamp with time zone)) * 12::double precision + date_part('month'::text, age(min(t2.data_referencia)::timestamp with time zone, max(t2.data) FILTER (WHERE t2.data <= t2.data_referencia AND t2.vencidopf > 0::double precision)::timestamp with time zone)) AS months_since_last_overdue_pf,
	    count(t2.vencidopf) FILTER (WHERE t2.data <= t2.data_referencia AND t2.vencidopf > 0::double precision) AS months_overdue_pf,
	    max(t2.vencidopf) FILTER (WHERE t2.data <= t2.data_referencia) AS max_overdue_pf,
	    sum(t2.prejuizopf) FILTER (WHERE t2.data = t2.data_referencia) AS default_pf_curr,
	    date_part('year'::text, age(min(t2.data_referencia)::timestamp with time zone, max(t2.data) FILTER (WHERE t2.data <= t2.data_referencia AND t2.prejuizopf > 0::double precision)::timestamp with time zone)) * 12::double precision + date_part('month'::text, age(min(t2.data_referencia)::timestamp with time zone, max(t2.data) FILTER (WHERE t2.data <= t2.data_referencia AND t2.prejuizopf > 0::double precision)::timestamp with time zone)) AS months_since_last_default_pf,
	    count(t2.prejuizopf) FILTER (WHERE t2.data <= t2.data_referencia AND t2.prejuizopf > 0::double precision) AS months_default_pf,
	    max(t2.prejuizopf) FILTER (WHERE t2.data <= t2.data_referencia) AS max_default_pf,
	    count(*) FILTER (WHERE t2.data <= t2.data_referencia) AS qtd_meses_escopo_pf,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = t2.data_referencia) AS lim_cred_pf_curr,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 mon'::interval)) AS lim_cred_pf_1m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '2 mons'::interval)) AS lim_cred_pf_2m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '3 mons'::interval)) AS lim_cred_pf_3m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '4 mons'::interval)) AS lim_cred_pf_4m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '5 mons'::interval)) AS lim_cred_pf_5m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '6 mons'::interval)) AS lim_cred_pf_6m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '7 mons'::interval)) AS lim_cred_pf_7m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '8 mons'::interval)) AS lim_cred_pf_8m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '9 mons'::interval)) AS lim_cred_pf_9m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '10 mons'::interval)) AS lim_cred_pf_10m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '11 mons'::interval)) AS lim_cred_pf_11m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year'::interval)) AS lim_cred_pf_12m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 1 mon'::interval)) AS lim_cred_pf_13m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 2 mons'::interval)) AS lim_cred_pf_14m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 3 mons'::interval)) AS lim_cred_pf_15m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 4 mons'::interval)) AS lim_cred_pf_16m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 5 mons'::interval)) AS lim_cred_pf_17m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 6 mons'::interval)) AS lim_cred_pf_18m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 7 mons'::interval)) AS lim_cred_pf_19m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 8 mons'::interval)) AS lim_cred_pf_20m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 9 mons'::interval)) AS lim_cred_pf_21m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 10 mons'::interval)) AS lim_cred_pf_22m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 11 mons'::interval)) AS lim_cred_pf_23m,
	    sum(t2.limcredpf) FILTER (WHERE t2.data <= t2.data_referencia) AS ever_lim_cred_pf,
	    max(t2.limcredpf) FILTER (WHERE t2.data <= t2.data_referencia) AS max_lim_cred_pf
	FROM ( SELECT t1.direct_prospect_id,
            case
                WHEN t1.indice_ultimo_antes_data_referencia IS NOT NULL THEN to_date((((cc.data -> 'historico'::text) -> 'Carteira de Crédito'::text) -> 0) ->> 'data'::text, 'mm-yyyy'::text)
                ELSE
                CASE
                    WHEN date_part('day'::text, t1.data_referencia) >= 16::double precision THEN to_date(to_char(t1.data_referencia - '1 mon'::interval, 'Mon/yy'::text), 'Mon/yy'::text)
                    ELSE to_date(to_char(t1.data_referencia - '2 mons'::interval, 'Mon/yy'::text), 'Mon/yy'::text)
                END
            END AS data_referencia,
            to_date(jsonb_array_elements((cc.data -> 'historico'::text) -> 'Carteira de Crédito'::text) ->> 'data'::text, 'mm/yyyy'::text) AS data,
            replace(replace(replace(jsonb_array_elements((cc.data -> 'historico'::text) -> 'Carteira de Crédito'::text) ->> 'valor'::text, '-'::text, '0'::text), '.'::text, ''::text), ','::text, '.'::text)::double precision AS debtpf,
            replace(replace(replace(jsonb_array_elements((cc.data -> 'historico'::text) -> 'Vencido'::text) ->> 'valor'::text, '-'::text, '0'::text), '.'::text, ''::text), ','::text, '.'::text)::double precision AS vencidopf,
            replace(replace(replace(jsonb_array_elements((cc.data -> 'historico'::text) -> 'Prejuízo'::text) ->> 'valor'::text, '-'::text, '0'::text), '.'::text, ''::text), ','::text, '.'::text)::double precision AS prejuizopf,
            replace(replace(replace(jsonb_array_elements((cc.data -> 'historico'::text) -> 'Limite de Crédito'::text) ->> 'valor'::text, '-'::text, '0'::text), '.'::text, ''::text), ','::text, '.'::text)::double precision AS limcredpf
		FROM public.credito_coleta cc
        join lucas_leal.t_scr_pf_choose_report_full t1 ON cc.id = t1.id
		left join (select distinct direct_prospect_id from lucas_leal.t_scr_pf_get_history_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
        where coalesce(t1.indice_ultimo_antes_data_referencia,t1.indice_asc) = 1
        	and t2.direct_prospect_id is null
        	) t2
	GROUP BY t2.direct_prospect_id
	) as hist_json on hist_json.direct_prospect_id = t1.direct_prospect_id
left join(select
		t1.direct_prospect_id,
		CASE
		    WHEN COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date > t1.data_referencia + '6 mons'::interval THEN NULL::integer
		    ELSE replace(replace(
		    CASE
		        WHEN "position"(cc.raw, 'Quantidade de Operações'::text) > 0 THEN "substring"(cc.raw, "position"(cc.raw, 'Quantidade de Operações'::text) + length(
		        CASE
		            WHEN "position"(cc.raw, 'class="fu3" width="50%"'::text) > 0 THEN 'Quantidade de Operações</div></td><td class="fu3" width="50%"><b>'::text
		            ELSE 'Quantidade de Operações</div>        </td>        <td width="50%" class="fu3">                    <b>'::text
		        END), 3)
		        ELSE NULL::text
		    END, '<'::text, ''::text), '/'::text, ''::text)::integer
		END AS num_ops_pf,
		CASE
		    WHEN COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date > t1.data_referencia + '6 mons'::interval THEN NULL::integer
		    ELSE replace(replace(
		    CASE
		        WHEN "position"(cc.raw, 'Quantidade de IFs em que o Cliente possui Operações'::text) > 0 THEN "substring"(cc.raw, "position"(cc.raw, 'Quantidade de IFs em que o Cliente possui Operações'::text) + length(
		        CASE
		            WHEN "position"(cc.raw, 'class="fu3" width="50%"'::text) > 0 THEN 'Quantidade de IFs em que o Cliente possui Operações</div></td><td class="fu3" width="50%"><b>'::text
		            ELSE 'Quantidade de IFs em que o Cliente possui Operações</div>        </td>        <td width="50%" class="fu3">                    <b>'::text
		        END), 3)
		        ELSE NULL::text
		    END, '<'::text, ''::text), '/'::text, ''::text)::integer
		END AS num_fis_pf,
		CASE
		    WHEN COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date > t1.data_referencia + '6 mons'::interval THEN NULL::integer
		    ELSE date_part('year'::text, age(t1.data_referencia::timestamp with time zone, to_date(replace(replace(replace(
		    CASE
		        WHEN "position"(cc.raw, 'Data de Início de Relacionamento com a IF'::text) > 0 THEN "substring"(cc.raw, "position"(cc.raw, 'Data de Início de Relacionamento com a IF'::text) + length(
		        CASE
		            WHEN "position"(cc.raw, 'class="fu3" width="50%"'::text) > 0 THEN 'Data de Início de Relacionamento com a IF</div></td><td class="fu3" width="50%"><b>'::text
		            ELSE 'Data de Início de Relacionamento com a IF</div>        </td>        <td width="50%" class="fu3">                         <b>'::text
		        END), 10)
		        ELSE NULL::text
		    END, '<'::text, ''::text), '/'::text, ''::text), '-'::text, '0'::text), 'ddmmyyyy'::text)::timestamp with time zone))::integer
		END AS first_relation_fi_pf,
        coalesce(cc."data"-> 'erro',cc."data"-> 'error')::text as thin_file
   	FROM credito_coleta cc
    join lucas_leal.t_scr_pf_choose_report_full t1 ON cc.id = t1.id
	left join (select distinct direct_prospect_id from lucas_leal.t_scr_pf_get_history_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
    where coalesce(t1.indice_ultimo_antes_data_referencia,t1.indice_asc) = 1
    	and t2.direct_prospect_id is null
    	)  as hist_raw on hist_raw.direct_prospect_id = t1.direct_prospect_id
where coalesce(t1.indice_ultimo_antes_data_referencia,t1.indice_asc) = 1
	and t2.direct_prospect_id is null
)

select t1.direct_prospect_id 
FROM lucas_leal.t_scr_pf_choose_report_full t1 
left join (select distinct direct_prospect_id from lucas_leal.t_scr_pf_get_history_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where coalesce(t1.indice_ultimo_antes_data_referencia,t1.indice_asc) = 1
	and t2.direct_prospect_id is null


select count(*),count(distinct t1.direct_prospect_id)
from lucas_leal.t_scr_pf_choose_report_full t1 
left join (select distinct direct_prospect_id from lucas_leal.t_scr_pf_get_history_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where coalesce(t1.indice_ultimo_antes_data_referencia,t1.indice_asc) = 1
	and t2.direct_prospect_id is null

select count(*),count(distinct direct_prospect_id)
from lucas_leal.t_scr_pf_get_history_data_full
	
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE unique INDEX t_scr_pf_get_modal_data_full_direct_prospect_id ON lucas_leal.t_scr_pf_get_modal_data_full USING btree (direct_prospect_id);
insert into lucas_leal.t_scr_pf_get_modal_data_full 
(select
	t1.direct_prospect_id,
	case when modal_erro.thin_file is null then modal_json.divida_atual_pf else 0 end divida_atual_pf,
	case when modal_erro.thin_file is null then modal_json.qtd_meses_modalidade_pf else 0 end qtd_meses_modalidade_pf,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pf_curr else 0 end emprestimos_pf_curr,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pf_1m else 0 end emprestimos_pf_1m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pf_2m else 0 end emprestimos_pf_2m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pf_3m else 0 end emprestimos_pf_3m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pf_4m else 0 end emprestimos_pf_4m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pf_5m else 0 end emprestimos_pf_5m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pf_6m else 0 end emprestimos_pf_6m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pf_7m else 0 end emprestimos_pf_7m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pf_8m else 0 end emprestimos_pf_8m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pf_9m else 0 end emprestimos_pf_9m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pf_10m else 0 end emprestimos_pf_10m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pf_11m else 0 end emprestimos_pf_11m,
	case when modal_erro.thin_file is null then modal_json.max_emprestimos_pf else 0 end max_emprestimos_pf,
	case when modal_erro.thin_file is null then modal_json.ever_emprestimos_pf else 0 end ever_emprestimos_pf,
	case when modal_erro.thin_file is null then modal_json.count_emprestimos_pf else 0 end count_emprestimos_pf,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pf_curr else 0 end emprestimos_cheque_especial_pf_curr,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pf_1m else 0 end emprestimos_cheque_especial_pf_1m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pf_2m else 0 end emprestimos_cheque_especial_pf_2m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pf_3m else 0 end emprestimos_cheque_especial_pf_3m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pf_4m else 0 end emprestimos_cheque_especial_pf_4m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pf_5m else 0 end emprestimos_cheque_especial_pf_5m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pf_6m else 0 end emprestimos_cheque_especial_pf_6m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pf_7m else 0 end emprestimos_cheque_especial_pf_7m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pf_8m else 0 end emprestimos_cheque_especial_pf_8m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pf_9m else 0 end emprestimos_cheque_especial_pf_9m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pf_10m else 0 end emprestimos_cheque_especial_pf_10m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pf_11m else 0 end emprestimos_cheque_especial_pf_11m,
	case when modal_erro.thin_file is null then modal_json.max_emprestimos_cheque_especial_pf else 0 end max_emprestimos_cheque_especial_pf,
	case when modal_erro.thin_file is null then modal_json.ever_emprestimos_cheque_especial_pf else 0 end ever_emprestimos_cheque_especial_pf,
	case when modal_erro.thin_file is null then modal_json.count_emprestimos_cheque_especial_pf else 0 end count_emprestimos_cheque_especial_pf,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pf_curr else 0 end emprestimos_cartao_credito_pf_curr,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pf_1m else 0 end emprestimos_cartao_credito_pf_1m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pf_2m else 0 end emprestimos_cartao_credito_pf_2m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pf_3m else 0 end emprestimos_cartao_credito_pf_3m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pf_4m else 0 end emprestimos_cartao_credito_pf_4m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pf_5m else 0 end emprestimos_cartao_credito_pf_5m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pf_6m else 0 end emprestimos_cartao_credito_pf_6m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pf_7m else 0 end emprestimos_cartao_credito_pf_7m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pf_8m else 0 end emprestimos_cartao_credito_pf_8m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pf_9m else 0 end emprestimos_cartao_credito_pf_9m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pf_10m else 0 end emprestimos_cartao_credito_pf_10m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pf_11m else 0 end emprestimos_cartao_credito_pf_11m,
	case when modal_erro.thin_file is null then modal_json.max_emprestimos_cartao_credito_pf else 0 end max_emprestimos_cartao_credito_pf,
	case when modal_erro.thin_file is null then modal_json.ever_emprestimos_cartao_credito_pf else 0 end ever_emprestimos_cartao_credito_pf,
	case when modal_erro.thin_file is null then modal_json.count_emprestimos_cartao_credito_pf else 0 end count_emprestimos_cartao_credito_pf,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_ppj_pf_curr else 0 end emprestimos_credito_ppj_pf_curr,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_ppj_pf_1m else 0 end emprestimos_credito_ppj_pf_1m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_ppj_pf_2m else 0 end emprestimos_credito_ppj_pf_2m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_ppj_pf_3m else 0 end emprestimos_credito_ppj_pf_3m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_ppj_pf_4m else 0 end emprestimos_credito_ppj_pf_4m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_ppj_pf_5m else 0 end emprestimos_credito_ppj_pf_5m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_ppj_pf_6m else 0 end emprestimos_credito_ppj_pf_6m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_ppj_pf_7m else 0 end emprestimos_credito_ppj_pf_7m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_ppj_pf_8m else 0 end emprestimos_credito_ppj_pf_8m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_ppj_pf_9m else 0 end emprestimos_credito_ppj_pf_9m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_ppj_pf_10m else 0 end emprestimos_credito_ppj_pf_10m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_ppj_pf_11m else 0 end emprestimos_credito_ppj_pf_11m,
	case when modal_erro.thin_file is null then modal_json.max_emprestimos_credito_ppj_pf else 0 end max_emprestimos_credito_ppj_pf,
	case when modal_erro.thin_file is null then modal_json.ever_emprestimos_credito_ppj_pf else 0 end ever_emprestimos_credito_ppj_pf,
	case when modal_erro.thin_file is null then modal_json.count_emprestimos_credito_ppj_pf else 0 end count_emprestimos_credito_ppj_pf,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_consignado_pf_curr else 0 end emprestimos_credito_consignado_pf_curr,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_consignado_pf_1m else 0 end emprestimos_credito_consignado_pf_1m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_consignado_pf_2m else 0 end emprestimos_credito_consignado_pf_2m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_consignado_pf_3m else 0 end emprestimos_credito_consignado_pf_3m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_consignado_pf_4m else 0 end emprestimos_credito_consignado_pf_4m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_consignado_pf_5m else 0 end emprestimos_credito_consignado_pf_5m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_consignado_pf_6m else 0 end emprestimos_credito_consignado_pf_6m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_consignado_pf_7m else 0 end emprestimos_credito_consignado_pf_7m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_consignado_pf_8m else 0 end emprestimos_credito_consignado_pf_8m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_consignado_pf_9m else 0 end emprestimos_credito_consignado_pf_9m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_consignado_pf_10m else 0 end emprestimos_credito_consignado_pf_10m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_credito_consignado_pf_11m else 0 end emprestimos_credito_consignado_pf_11m,
	case when modal_erro.thin_file is null then modal_json.max_emprestimos_credito_consignado_pf else 0 end max_emprestimos_credito_consignado_pf,
	case when modal_erro.thin_file is null then modal_json.ever_emprestimos_credito_consignado_pf else 0 end ever_emprestimos_credito_consignado_pf,
	case when modal_erro.thin_file is null then modal_json.count_emprestimos_credito_consignado_pf else 0 end count_emprestimos_credito_consignado_pf,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pf_curr else 0 end emprestimos_outros_emprestimos_pf_curr,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pf_1m else 0 end emprestimos_outros_emprestimos_pf_1m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pf_2m else 0 end emprestimos_outros_emprestimos_pf_2m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pf_3m else 0 end emprestimos_outros_emprestimos_pf_3m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pf_4m else 0 end emprestimos_outros_emprestimos_pf_4m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pf_5m else 0 end emprestimos_outros_emprestimos_pf_5m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pf_6m else 0 end emprestimos_outros_emprestimos_pf_6m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pf_7m else 0 end emprestimos_outros_emprestimos_pf_7m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pf_8m else 0 end emprestimos_outros_emprestimos_pf_8m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pf_9m else 0 end emprestimos_outros_emprestimos_pf_9m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pf_10m else 0 end emprestimos_outros_emprestimos_pf_10m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pf_11m else 0 end emprestimos_outros_emprestimos_pf_11m,
	case when modal_erro.thin_file is null then modal_json.max_emprestimos_outros_emprestimos_pf else 0 end max_emprestimos_outros_emprestimos_pf,
	case when modal_erro.thin_file is null then modal_json.ever_emprestimos_outros_emprestimos_pf else 0 end ever_emprestimos_outros_emprestimos_pf,
	case when modal_erro.thin_file is null then modal_json.count_emprestimos_outros_emprestimos_pf else 0 end count_emprestimos_outros_emprestimos_pf,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pf_curr else 0 end outroscreditos_pf_curr,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pf_1m else 0 end outroscreditos_pf_1m,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pf_2m else 0 end outroscreditos_pf_2m,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pf_3m else 0 end outroscreditos_pf_3m,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pf_4m else 0 end outroscreditos_pf_4m,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pf_5m else 0 end outroscreditos_pf_5m,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pf_6m else 0 end outroscreditos_pf_6m,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pf_7m else 0 end outroscreditos_pf_7m,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pf_8m else 0 end outroscreditos_pf_8m,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pf_9m else 0 end outroscreditos_pf_9m,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pf_10m else 0 end outroscreditos_pf_10m,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pf_11m else 0 end outroscreditos_pf_11m,
	case when modal_erro.thin_file is null then modal_json.max_outroscreditos_pf else 0 end max_outroscreditos_pf,
	case when modal_erro.thin_file is null then modal_json.ever_outroscreditos_pf else 0 end ever_outroscreditos_pf,
	case when modal_erro.thin_file is null then modal_json.count_outroscreditos_pf else 0 end count_outroscreditos_pf,
	case when modal_erro.thin_file is null then modal_json.financiamento_veiculo_pf_curr else 0 end financiamento_veiculo_pf_curr,
	case when modal_erro.thin_file is null then modal_json.financiamento_veiculo_pf_1m else 0 end financiamento_veiculo_pf_1m,
	case when modal_erro.thin_file is null then modal_json.financiamento_veiculo_pf_2m else 0 end financiamento_veiculo_pf_2m,
	case when modal_erro.thin_file is null then modal_json.financiamento_veiculo_pf_3m else 0 end financiamento_veiculo_pf_3m,
	case when modal_erro.thin_file is null then modal_json.financiamento_veiculo_pf_4m else 0 end financiamento_veiculo_pf_4m,
	case when modal_erro.thin_file is null then modal_json.financiamento_veiculo_pf_5m else 0 end financiamento_veiculo_pf_5m,
	case when modal_erro.thin_file is null then modal_json.financiamento_veiculo_pf_6m else 0 end financiamento_veiculo_pf_6m,
	case when modal_erro.thin_file is null then modal_json.financiamento_veiculo_pf_7m else 0 end financiamento_veiculo_pf_7m,
	case when modal_erro.thin_file is null then modal_json.financiamento_veiculo_pf_8m else 0 end financiamento_veiculo_pf_8m,
	case when modal_erro.thin_file is null then modal_json.financiamento_veiculo_pf_9m else 0 end financiamento_veiculo_pf_9m,
	case when modal_erro.thin_file is null then modal_json.financiamento_veiculo_pf_10m else 0 end financiamento_veiculo_pf_10m,
	case when modal_erro.thin_file is null then modal_json.financiamento_veiculo_pf_11m else 0 end financiamento_veiculo_pf_11m,
	case when modal_erro.thin_file is null then modal_json.max_financiamento_veiculo_pf else 0 end max_financiamento_veiculo_pf,
	case when modal_erro.thin_file is null then modal_json.ever_financiamento_veiculo_pf else 0 end ever_financiamento_veiculo_pf,
	case when modal_erro.thin_file is null then modal_json.count_financiamento_veiculo_pf else 0 end count_financiamento_veiculo_pf,
	case when modal_erro.thin_file is null then modal_json.financiamento_imobiliario_pf_curr else 0 end financiamento_imobiliario_pf_curr,
	case when modal_erro.thin_file is null then modal_json.financiamento_imobiliario_pf_1m else 0 end financiamento_imobiliario_pf_1m,
	case when modal_erro.thin_file is null then modal_json.financiamento_imobiliario_pf_2m else 0 end financiamento_imobiliario_pf_2m,
	case when modal_erro.thin_file is null then modal_json.financiamento_imobiliario_pf_3m else 0 end financiamento_imobiliario_pf_3m,
	case when modal_erro.thin_file is null then modal_json.financiamento_imobiliario_pf_4m else 0 end financiamento_imobiliario_pf_4m,
	case when modal_erro.thin_file is null then modal_json.financiamento_imobiliario_pf_5m else 0 end financiamento_imobiliario_pf_5m,
	case when modal_erro.thin_file is null then modal_json.financiamento_imobiliario_pf_6m else 0 end financiamento_imobiliario_pf_6m,
	case when modal_erro.thin_file is null then modal_json.financiamento_imobiliario_pf_7m else 0 end financiamento_imobiliario_pf_7m,
	case when modal_erro.thin_file is null then modal_json.financiamento_imobiliario_pf_8m else 0 end financiamento_imobiliario_pf_8m,
	case when modal_erro.thin_file is null then modal_json.financiamento_imobiliario_pf_9m else 0 end financiamento_imobiliario_pf_9m,
	case when modal_erro.thin_file is null then modal_json.financiamento_imobiliario_pf_10m else 0 end financiamento_imobiliario_pf_10m,
	case when modal_erro.thin_file is null then modal_json.financiamento_imobiliario_pf_11m else 0 end financiamento_imobiliario_pf_11m,
	case when modal_erro.thin_file is null then modal_json.max_financiamento_imobiliario_pf else 0 end max_financiamento_imobiliario_pf,
	case when modal_erro.thin_file is null then modal_json.ever_financiamento_imobiliario_pf else 0 end ever_financiamento_imobiliario_pf,
	case when modal_erro.thin_file is null then modal_json.count_financiamento_imobiliario_pf else 0 end count_financiamento_imobiliario_pf,
	case when modal_erro.thin_file is null then modal_json.adiantamentosdescontos_pf_curr else 0 end adiantamentosdescontos_pf_curr,
	case when modal_erro.thin_file is null then modal_json.adiantamentosdescontos_pf_1m else 0 end adiantamentosdescontos_pf_1m,
	case when modal_erro.thin_file is null then modal_json.adiantamentosdescontos_pf_2m else 0 end adiantamentosdescontos_pf_2m,
	case when modal_erro.thin_file is null then modal_json.adiantamentosdescontos_pf_3m else 0 end adiantamentosdescontos_pf_3m,
	case when modal_erro.thin_file is null then modal_json.adiantamentosdescontos_pf_4m else 0 end adiantamentosdescontos_pf_4m,
	case when modal_erro.thin_file is null then modal_json.adiantamentosdescontos_pf_5m else 0 end adiantamentosdescontos_pf_5m,
	case when modal_erro.thin_file is null then modal_json.adiantamentosdescontos_pf_6m else 0 end adiantamentosdescontos_pf_6m,
	case when modal_erro.thin_file is null then modal_json.adiantamentosdescontos_pf_7m else 0 end adiantamentosdescontos_pf_7m,
	case when modal_erro.thin_file is null then modal_json.adiantamentosdescontos_pf_8m else 0 end adiantamentosdescontos_pf_8m,
	case when modal_erro.thin_file is null then modal_json.adiantamentosdescontos_pf_9m else 0 end adiantamentosdescontos_pf_9m,
	case when modal_erro.thin_file is null then modal_json.adiantamentosdescontos_pf_10m else 0 end adiantamentosdescontos_pf_10m,
	case when modal_erro.thin_file is null then modal_json.adiantamentosdescontos_pf_11m else 0 end adiantamentosdescontos_pf_11m,
	case when modal_erro.thin_file is null then modal_json.max_adiantamentosdescontos_pf else 0 end max_adiantamentosdescontos_pf,
	case when modal_erro.thin_file is null then modal_json.ever_adiantamentosdescontos_pf else 0 end ever_adiantamentosdescontos_pf,
	case when modal_erro.thin_file is null then modal_json.count_adiantamentosdescontos_pf else 0 end count_adiantamentosdescontos_pf,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pf_curr else 0 end outrosfinanciamentos_pf_curr,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pf_1m else 0 end outrosfinanciamentos_pf_1m,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pf_2m else 0 end outrosfinanciamentos_pf_2m,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pf_3m else 0 end outrosfinanciamentos_pf_3m,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pf_4m else 0 end outrosfinanciamentos_pf_4m,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pf_5m else 0 end outrosfinanciamentos_pf_5m,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pf_6m else 0 end outrosfinanciamentos_pf_6m,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pf_7m else 0 end outrosfinanciamentos_pf_7m,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pf_8m else 0 end outrosfinanciamentos_pf_8m,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pf_9m else 0 end outrosfinanciamentos_pf_9m,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pf_10m else 0 end outrosfinanciamentos_pf_10m,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pf_11m else 0 end outrosfinanciamentos_pf_11m,
	case when modal_erro.thin_file is null then modal_json.max_outrosfinanciamentos_pf else 0 end max_outrosfinanciamentos_pf,
	case when modal_erro.thin_file is null then modal_json.ever_outrosfinanciamentos_pf else 0 end ever_outrosfinanciamentos_pf,
	case when modal_erro.thin_file is null then modal_json.count_outrosfinanciamentos_pf else 0 end count_outrosfinanciamentos_pf,
	case when modal_erro.thin_file is null then modal_json.homeequity_pf_curr else 0 end homeequity_pf_curr,
	case when modal_erro.thin_file is null then modal_json.homeequity_pf_1m else 0 end homeequity_pf_1m,
	case when modal_erro.thin_file is null then modal_json.homeequity_pf_2m else 0 end homeequity_pf_2m,
	case when modal_erro.thin_file is null then modal_json.homeequity_pf_3m else 0 end homeequity_pf_3m,
	case when modal_erro.thin_file is null then modal_json.homeequity_pf_4m else 0 end homeequity_pf_4m,
	case when modal_erro.thin_file is null then modal_json.homeequity_pf_5m else 0 end homeequity_pf_5m,
	case when modal_erro.thin_file is null then modal_json.homeequity_pf_6m else 0 end homeequity_pf_6m,
	case when modal_erro.thin_file is null then modal_json.homeequity_pf_7m else 0 end homeequity_pf_7m,
	case when modal_erro.thin_file is null then modal_json.homeequity_pf_8m else 0 end homeequity_pf_8m,
	case when modal_erro.thin_file is null then modal_json.homeequity_pf_9m else 0 end homeequity_pf_9m,
	case when modal_erro.thin_file is null then modal_json.homeequity_pf_10m else 0 end homeequity_pf_10m,
	case when modal_erro.thin_file is null then modal_json.homeequity_pf_11m else 0 end homeequity_pf_11m,
	case when modal_erro.thin_file is null then modal_json.max_homeequity_pf else 0 end max_homeequity_pf,
	case when modal_erro.thin_file is null then modal_json.ever_homeequity_pf else 0 end ever_homeequity_pf,
	case when modal_erro.thin_file is null then modal_json.count_homeequity_pf else 0 end count_homeequity_pf,
	case when modal_erro.thin_file is null then modal_json.limitecredito_pf_curr else 0 end limitecredito_pf_curr,
	case when modal_erro.thin_file is null then modal_json.limitecredito_pf_1m else 0 end limitecredito_pf_1m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_pf_2m else 0 end limitecredito_pf_2m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_pf_3m else 0 end limitecredito_pf_3m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_pf_4m else 0 end limitecredito_pf_4m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_pf_5m else 0 end limitecredito_pf_5m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_pf_6m else 0 end limitecredito_pf_6m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_pf_7m else 0 end limitecredito_pf_7m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_pf_8m else 0 end limitecredito_pf_8m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_pf_9m else 0 end limitecredito_pf_9m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_pf_10m else 0 end limitecredito_pf_10m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_pf_11m else 0 end limitecredito_pf_11m,
	case when modal_erro.thin_file is null then modal_json.ever_limitecredito_pf else 0 end ever_limitecredito_pf,
	case when modal_erro.thin_file is null then modal_json.count_limitecredito_pf else 0 end count_limitecredito_pf,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pf_curr else 0 end prejuizo_pf_curr,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pf_1m else 0 end prejuizo_pf_1m,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pf_2m else 0 end prejuizo_pf_2m,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pf_3m else 0 end prejuizo_pf_3m,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pf_4m else 0 end prejuizo_pf_4m,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pf_5m else 0 end prejuizo_pf_5m,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pf_6m else 0 end prejuizo_pf_6m,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pf_7m else 0 end prejuizo_pf_7m,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pf_8m else 0 end prejuizo_pf_8m,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pf_9m else 0 end prejuizo_pf_9m,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pf_10m else 0 end prejuizo_pf_10m,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pf_11m else 0 end prejuizo_pf_11m,
	case when modal_erro.thin_file is null then modal_json.ever_prejuizo_pf else 0 end ever_prejuizo_pf,
	case when modal_erro.thin_file is null then modal_json.count_prejuizo_pf else 0 end count_prejuizo_pf,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pf_curr else 0 end carteiracredito_pf_curr,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pf_1m else 0 end carteiracredito_pf_1m,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pf_2m else 0 end carteiracredito_pf_2m,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pf_3m else 0 end carteiracredito_pf_3m,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pf_4m else 0 end carteiracredito_pf_4m,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pf_5m else 0 end carteiracredito_pf_5m,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pf_6m else 0 end carteiracredito_pf_6m,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pf_7m else 0 end carteiracredito_pf_7m,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pf_8m else 0 end carteiracredito_pf_8m,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pf_9m else 0 end carteiracredito_pf_9m,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pf_10m else 0 end carteiracredito_pf_10m,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pf_11m else 0 end carteiracredito_pf_11m,
	case when modal_erro.thin_file is null then modal_json.ever_carteiracredito_pf else 0 end ever_carteiracredito_pf
from lucas_leal.t_scr_pf_choose_report_full t1
left join (select distinct direct_prospect_id from lucas_leal.t_scr_pf_get_modal_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
left join(SELECT t6.direct_prospect_id,
		max(t6.divida_atual_pf) as divida_atual_pf,
		coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.to_date <= t6.data_referencia),0) as qtd_meses_modalidade_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text),0) as emprestimos_pf_curr,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as emprestimos_pf_1m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as emprestimos_pf_2m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as emprestimos_pf_3m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as emprestimos_pf_4m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as emprestimos_pf_5m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as emprestimos_pf_6m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as emprestimos_pf_7m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as emprestimos_pf_8m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as emprestimos_pf_9m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as emprestimos_pf_10m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as emprestimos_pf_11m,
		coalesce(max(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text),0) as max_emprestimos_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text),0) as ever_emprestimos_pf,
		coalesce(count(*) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text),0) as count_emprestimos_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as emprestimos_cheque_especial_pf_curr,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as emprestimos_cheque_especial_pf_1m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as emprestimos_cheque_especial_pf_2m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as emprestimos_cheque_especial_pf_3m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as emprestimos_cheque_especial_pf_4m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as emprestimos_cheque_especial_pf_5m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as emprestimos_cheque_especial_pf_6m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as emprestimos_cheque_especial_pf_7m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as emprestimos_cheque_especial_pf_8m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as emprestimos_cheque_especial_pf_9m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as emprestimos_cheque_especial_pf_10m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as emprestimos_cheque_especial_pf_11m,
		coalesce(max(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as max_emprestimos_cheque_especial_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as ever_emprestimos_cheque_especial_pf,
		coalesce(count(*) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as count_emprestimos_cheque_especial_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as emprestimos_cartao_credito_pf_curr,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as emprestimos_cartao_credito_pf_1m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as emprestimos_cartao_credito_pf_2m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as emprestimos_cartao_credito_pf_3m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as emprestimos_cartao_credito_pf_4m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as emprestimos_cartao_credito_pf_5m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as emprestimos_cartao_credito_pf_6m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as emprestimos_cartao_credito_pf_7m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as emprestimos_cartao_credito_pf_8m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as emprestimos_cartao_credito_pf_9m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as emprestimos_cartao_credito_pf_10m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as emprestimos_cartao_credito_pf_11m,
		coalesce(max(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as max_emprestimos_cartao_credito_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as ever_emprestimos_cartao_credito_pf,
		coalesce(count(*) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as count_emprestimos_cartao_credito_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Microcrédito'::text OR t6.lv2 = 'Crédito Pessoal - sem Consignação em Folha de Pagamento'::text)),0) as emprestimos_credito_ppj_pf_curr,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Microcrédito'::text OR t6.lv2 = 'Crédito Pessoal - sem Consignação em Folha de Pagamento'::text)),0) as emprestimos_credito_ppj_pf_1m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Microcrédito'::text OR t6.lv2 = 'Crédito Pessoal - sem Consignação em Folha de Pagamento'::text)),0) as emprestimos_credito_ppj_pf_2m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Microcrédito'::text OR t6.lv2 = 'Crédito Pessoal - sem Consignação em Folha de Pagamento'::text)),0) as emprestimos_credito_ppj_pf_3m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Microcrédito'::text OR t6.lv2 = 'Crédito Pessoal - sem Consignação em Folha de Pagamento'::text)),0) as emprestimos_credito_ppj_pf_4m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Microcrédito'::text OR t6.lv2 = 'Crédito Pessoal - sem Consignação em Folha de Pagamento'::text)),0) as emprestimos_credito_ppj_pf_5m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Microcrédito'::text OR t6.lv2 = 'Crédito Pessoal - sem Consignação em Folha de Pagamento'::text)),0) as emprestimos_credito_ppj_pf_6m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Microcrédito'::text OR t6.lv2 = 'Crédito Pessoal - sem Consignação em Folha de Pagamento'::text)),0) as emprestimos_credito_ppj_pf_7m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Microcrédito'::text OR t6.lv2 = 'Crédito Pessoal - sem Consignação em Folha de Pagamento'::text)),0) as emprestimos_credito_ppj_pf_8m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Microcrédito'::text OR t6.lv2 = 'Crédito Pessoal - sem Consignação em Folha de Pagamento'::text)),0) as emprestimos_credito_ppj_pf_9m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Microcrédito'::text OR t6.lv2 = 'Crédito Pessoal - sem Consignação em Folha de Pagamento'::text)),0) as emprestimos_credito_ppj_pf_10m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Microcrédito'::text OR t6.lv2 = 'Crédito Pessoal - sem Consignação em Folha de Pagamento'::text)),0) as emprestimos_credito_ppj_pf_11m,
		coalesce(max(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Microcrédito'::text OR t6.lv2 = 'Crédito Pessoal - sem Consignação em Folha de Pagamento'::text)),0) as max_emprestimos_credito_ppj_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Microcrédito'::text OR t6.lv2 = 'Crédito Pessoal - sem Consignação em Folha de Pagamento'::text)),0) as ever_emprestimos_credito_ppj_pf,
		coalesce(count(*) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Microcrédito'::text OR t6.lv2 = 'Crédito Pessoal - sem Consignação em Folha de Pagamento'::text)),0) as count_emprestimos_credito_ppj_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Crédito Pessoal - com Consignação em Folha de Pagamento'::text),0) as emprestimos_credito_consignado_pf_curr,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Crédito Pessoal - com Consignação em Folha de Pagamento'::text),0) as emprestimos_credito_consignado_pf_1m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Crédito Pessoal - com Consignação em Folha de Pagamento'::text),0) as emprestimos_credito_consignado_pf_2m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Crédito Pessoal - com Consignação em Folha de Pagamento'::text),0) as emprestimos_credito_consignado_pf_3m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Crédito Pessoal - com Consignação em Folha de Pagamento'::text),0) as emprestimos_credito_consignado_pf_4m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Crédito Pessoal - com Consignação em Folha de Pagamento'::text),0) as emprestimos_credito_consignado_pf_5m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Crédito Pessoal - com Consignação em Folha de Pagamento'::text),0) as emprestimos_credito_consignado_pf_6m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Crédito Pessoal - com Consignação em Folha de Pagamento'::text),0) as emprestimos_credito_consignado_pf_7m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Crédito Pessoal - com Consignação em Folha de Pagamento'::text),0) as emprestimos_credito_consignado_pf_8m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Crédito Pessoal - com Consignação em Folha de Pagamento'::text),0) as emprestimos_credito_consignado_pf_9m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Crédito Pessoal - com Consignação em Folha de Pagamento'::text),0) as emprestimos_credito_consignado_pf_10m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Crédito Pessoal - com Consignação em Folha de Pagamento'::text),0) as emprestimos_credito_consignado_pf_11m,
		coalesce(max(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Crédito Pessoal - com Consignação em Folha de Pagamento'::text),0) as max_emprestimos_credito_consignado_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Crédito Pessoal - com Consignação em Folha de Pagamento'::text),0) as ever_emprestimos_credito_consignado_pf,
		coalesce(count(*) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Crédito Pessoal - com Consignação em Folha de Pagamento'::text),0) as count_emprestimos_credito_consignado_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as emprestimos_outros_emprestimos_pf_curr,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as emprestimos_outros_emprestimos_pf_1m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as emprestimos_outros_emprestimos_pf_2m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as emprestimos_outros_emprestimos_pf_3m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as emprestimos_outros_emprestimos_pf_4m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as emprestimos_outros_emprestimos_pf_5m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as emprestimos_outros_emprestimos_pf_6m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as emprestimos_outros_emprestimos_pf_7m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as emprestimos_outros_emprestimos_pf_8m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as emprestimos_outros_emprestimos_pf_9m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as emprestimos_outros_emprestimos_pf_10m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as emprestimos_outros_emprestimos_pf_11m,
		coalesce(max(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as max_emprestimos_outros_emprestimos_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as ever_emprestimos_outros_emprestimos_pf,
		coalesce(count(*) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as count_emprestimos_outros_emprestimos_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Outros Créditos'::text),0) as outroscreditos_pf_curr,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as outroscreditos_pf_1m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as outroscreditos_pf_2m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as outroscreditos_pf_3m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as outroscreditos_pf_4m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as outroscreditos_pf_5m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as outroscreditos_pf_6m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as outroscreditos_pf_7m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as outroscreditos_pf_8m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as outroscreditos_pf_9m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as outroscreditos_pf_10m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as outroscreditos_pf_11m,
		coalesce(max(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Outros Créditos'::text),0) as max_outroscreditos_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Outros Créditos'::text),0) as ever_outroscreditos_pf,
		coalesce(count(*) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Outros Créditos'::text),0) as count_outroscreditos_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pf_curr,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pf_1m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pf_2m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pf_3m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pf_4m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pf_5m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pf_6m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pf_7m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pf_8m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pf_9m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pf_10m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pf_11m,
		coalesce(max(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  max_financiamento_veiculo_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  ever_financiamento_veiculo_pf,
		coalesce(count(*) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  count_financiamento_veiculo_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Financiamentos Imobiliários'::text),0) as financiamento_imobiliario_pf_curr,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Financiamentos Imobiliários'::text),0) as financiamento_imobiliario_pf_1m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Financiamentos Imobiliários'::text),0) as financiamento_imobiliario_pf_2m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Financiamentos Imobiliários'::text),0) as financiamento_imobiliario_pf_3m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Financiamentos Imobiliários'::text),0) as financiamento_imobiliario_pf_4m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Financiamentos Imobiliários'::text),0) as financiamento_imobiliario_pf_5m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Financiamentos Imobiliários'::text),0) as financiamento_imobiliario_pf_6m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Financiamentos Imobiliários'::text),0) as financiamento_imobiliario_pf_7m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Financiamentos Imobiliários'::text),0) as financiamento_imobiliario_pf_8m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Financiamentos Imobiliários'::text),0) as financiamento_imobiliario_pf_9m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Financiamentos Imobiliários'::text),0) as financiamento_imobiliario_pf_10m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Financiamentos Imobiliários'::text),0) as financiamento_imobiliario_pf_11m,
		coalesce(max(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Financiamentos Imobiliários'::text),0) as max_financiamento_imobiliario_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Financiamentos Imobiliários'::text),0) as ever_financiamento_imobiliario_pf,
		coalesce(count(*) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Financiamentos Imobiliários'::text),0) as count_financiamento_imobiliario_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND (t6.lv1 = 'Adiantamentos a Depositantes'::text OR t6.lv1 = 'Direitos Creditórios Descontados'::text)),0) as adiantamentosdescontos_pf_curr,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND (t6.lv1 = 'Adiantamentos a Depositantes'::text OR t6.lv1 = 'Direitos Creditórios Descontados'::text)),0) as adiantamentosdescontos_pf_1m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND (t6.lv1 = 'Adiantamentos a Depositantes'::text OR t6.lv1 = 'Direitos Creditórios Descontados'::text)),0) as adiantamentosdescontos_pf_2m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND (t6.lv1 = 'Adiantamentos a Depositantes'::text OR t6.lv1 = 'Direitos Creditórios Descontados'::text)),0) as adiantamentosdescontos_pf_3m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND (t6.lv1 = 'Adiantamentos a Depositantes'::text OR t6.lv1 = 'Direitos Creditórios Descontados'::text)),0) as adiantamentosdescontos_pf_4m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND (t6.lv1 = 'Adiantamentos a Depositantes'::text OR t6.lv1 = 'Direitos Creditórios Descontados'::text)),0) as adiantamentosdescontos_pf_5m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND (t6.lv1 = 'Adiantamentos a Depositantes'::text OR t6.lv1 = 'Direitos Creditórios Descontados'::text)),0) as adiantamentosdescontos_pf_6m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND (t6.lv1 = 'Adiantamentos a Depositantes'::text OR t6.lv1 = 'Direitos Creditórios Descontados'::text)),0) as adiantamentosdescontos_pf_7m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND (t6.lv1 = 'Adiantamentos a Depositantes'::text OR t6.lv1 = 'Direitos Creditórios Descontados'::text)),0) as adiantamentosdescontos_pf_8m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND (t6.lv1 = 'Adiantamentos a Depositantes'::text OR t6.lv1 = 'Direitos Creditórios Descontados'::text)),0) as adiantamentosdescontos_pf_9m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND (t6.lv1 = 'Adiantamentos a Depositantes'::text OR t6.lv1 = 'Direitos Creditórios Descontados'::text)),0) as adiantamentosdescontos_pf_10m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND (t6.lv1 = 'Adiantamentos a Depositantes'::text OR t6.lv1 = 'Direitos Creditórios Descontados'::text)),0) as adiantamentosdescontos_pf_11m,
		coalesce(max(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND (t6.lv1 = 'Adiantamentos a Depositantes'::text OR t6.lv1 = 'Direitos Creditórios Descontados'::text)),0) as max_adiantamentosdescontos_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND (t6.lv1 = 'Adiantamentos a Depositantes'::text OR t6.lv1 = 'Direitos Creditórios Descontados'::text)),0) as ever_adiantamentosdescontos_pf,
		coalesce(count(*) FILTER (WHERE t6.to_date <= t6.data_referencia AND (t6.lv1 = 'Adiantamentos a Depositantes'::text OR t6.lv1 = 'Direitos Creditórios Descontados'::text)),0) as count_adiantamentosdescontos_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 ~~ 'Financiamentos%'::text AND t6.lv1 <> 'Financiamentos Imobiliários'::text AND t6.lv2 <> 'Aquisição de Bens -Â– Veículos Automotores'::text),0) as outrosfinanciamentos_pf_curr,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 ~~ 'Financiamentos%'::text AND t6.lv1 <> 'Financiamentos Imobiliários'::text AND t6.lv2 <> 'Aquisição de Bens - Veículos Automotores'::text),0) as outrosfinanciamentos_pf_1m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 ~~ 'Financiamentos%'::text AND t6.lv1 <> 'Financiamentos Imobiliários'::text AND t6.lv2 <> 'Aquisição de Bens - Veículos Automotores'::text),0) as outrosfinanciamentos_pf_2m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 ~~ 'Financiamentos%'::text AND t6.lv1 <> 'Financiamentos Imobiliários'::text AND t6.lv2 <> 'Aquisição de Bens - Veículos Automotores'::text),0) as outrosfinanciamentos_pf_3m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 ~~ 'Financiamentos%'::text AND t6.lv1 <> 'Financiamentos Imobiliários'::text AND t6.lv2 <> 'Aquisição de Bens - Veículos Automotores'::text),0) as outrosfinanciamentos_pf_4m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 ~~ 'Financiamentos%'::text AND t6.lv1 <> 'Financiamentos Imobiliários'::text AND t6.lv2 <> 'Aquisição de Bens - Veículos Automotores'::text),0) as outrosfinanciamentos_pf_5m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 ~~ 'Financiamentos%'::text AND t6.lv1 <> 'Financiamentos Imobiliários'::text AND t6.lv2 <> 'Aquisição de Bens - Veículos Automotores'::text),0) as outrosfinanciamentos_pf_6m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 ~~ 'Financiamentos%'::text AND t6.lv1 <> 'Financiamentos Imobiliários'::text AND t6.lv2 <> 'Aquisição de Bens - Veículos Automotores'::text),0) as outrosfinanciamentos_pf_7m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 ~~ 'Financiamentos%'::text AND t6.lv1 <> 'Financiamentos Imobiliários'::text AND t6.lv2 <> 'Aquisição de Bens - Veículos Automotores'::text),0) as outrosfinanciamentos_pf_8m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 ~~ 'Financiamentos%'::text AND t6.lv1 <> 'Financiamentos Imobiliários'::text AND t6.lv2 <> 'Aquisição de Bens - Veículos Automotores'::text),0) as outrosfinanciamentos_pf_9m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 ~~ 'Financiamentos%'::text AND t6.lv1 <> 'Financiamentos Imobiliários'::text AND t6.lv2 <> 'Aquisição de Bens - Veículos Automotores'::text),0) as outrosfinanciamentos_pf_10m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 ~~ 'Financiamentos%'::text AND t6.lv1 <> 'Financiamentos Imobiliários'::text AND t6.lv2 <> 'Aquisição de Bens - Veículos Automotores'::text),0) as outrosfinanciamentos_pf_11m,
		coalesce(max(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 ~~ 'Financiamentos%'::text AND t6.lv1 <> 'Financiamentos Imobiliários'::text AND t6.lv2 <> 'Aquisição de Bens - Veículos Automotores'::text),0) as max_outrosfinanciamentos_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 ~~ 'Financiamentos%'::text AND t6.lv1 <> 'Financiamentos Imobiliários'::text AND t6.lv2 <> 'Aquisição de Bens - Veículos Automotores'::text),0) as ever_outrosfinanciamentos_pf,
		coalesce(count(*) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 ~~ 'Financiamentos%'::text AND t6.lv1 <> 'Financiamentos Imobiliários'::text AND t6.lv2 <> 'Aquisição de Bens - Veículos Automotores'::text),0) as count_outrosfinanciamentos_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv2 = 'Home Equity'::text),0) as homeequity_pf_curr,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv2 = 'Home Equity'::text),0) as homeequity_pf_1m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv2 = 'Home Equity'::text),0) as homeequity_pf_2m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv2 = 'Home Equity'::text),0) as homeequity_pf_3m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv2 = 'Home Equity'::text),0) as homeequity_pf_4m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv2 = 'Home Equity'::text),0) as homeequity_pf_5m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv2 = 'Home Equity'::text),0) as homeequity_pf_6m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv2 = 'Home Equity'::text),0) as homeequity_pf_7m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv2 = 'Home Equity'::text),0) as homeequity_pf_8m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv2 = 'Home Equity'::text),0) as homeequity_pf_9m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv2 = 'Home Equity'::text),0) as homeequity_pf_10m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv2 = 'Home Equity'::text),0) as homeequity_pf_11m,
		coalesce(max(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv2 = 'Home Equity'::text),0) as max_homeequity_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv2 = 'Home Equity'::text),0) as ever_homeequity_pf,
		coalesce(count(*) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv2 = 'Home Equity'::text),0) as count_homeequity_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Limite de Crédito (H)'::text),0) as limitecredito_pf_curr,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text),0) as limitecredito_pf_1m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text),0) as limitecredito_pf_2m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text),0) as limitecredito_pf_3m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text),0) as limitecredito_pf_4m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text),0) as limitecredito_pf_5m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text),0) as limitecredito_pf_6m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text),0) as limitecredito_pf_7m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text),0) as limitecredito_pf_8m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text),0) as limitecredito_pf_9m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text),0) as limitecredito_pf_10m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text),0) as limitecredito_pf_11m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Limite de Crédito (H)'::text),0) as ever_limitecredito_pf,
		coalesce(count(*) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Limite de Crédito (H)'::text),0) as count_limitecredito_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Prejuízo (B)'::text),0) as prejuizo_pf_curr,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as prejuizo_pf_1m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as prejuizo_pf_2m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as prejuizo_pf_3m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as prejuizo_pf_4m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as prejuizo_pf_5m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as prejuizo_pf_6m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as prejuizo_pf_7m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as prejuizo_pf_8m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as prejuizo_pf_9m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as prejuizo_pf_10m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as prejuizo_pf_11m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Prejuízo (B)'::text),0) as ever_prejuizo_pf,
		coalesce(count(*) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Prejuízo (B)'::text),0) as count_prejuizo_pf,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as carteiracredito_pf_curr,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as carteiracredito_pf_1m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as carteiracredito_pf_2m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as carteiracredito_pf_3m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as carteiracredito_pf_4m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as carteiracredito_pf_5m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as carteiracredito_pf_6m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as carteiracredito_pf_7m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as carteiracredito_pf_8m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as carteiracredito_pf_9m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as carteiracredito_pf_10m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as carteiracredito_pf_11m,
		coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as ever_carteiracredito_pf
	FROM ( SELECT t5.direct_prospect_id,
			t5.divida_atual_pf,
			regexp_replace(t5.lv1, '\d{2}\s-\s'::text, ''::text) AS lv1,
			replace(replace(replace(replace(regexp_replace(t5.lv2, '\d{4}\s-\s'::text, ''::text), ' Â– '::text, ' - '::text), ' -- '::text, ' - '::text), ' Â–- '::text, ' - '::text), 'interfinanceiros'::text, 'Interfinanceiros'::text) AS lv2,
			to_date(t5.data, 'mm/yyyy'::text) AS to_date,
			CASE
			    WHEN t5.valor IS NULL THEN 0::double precision
			    ELSE t5.valor
			END AS valor,
			CASE
			    WHEN t5.indice_ultimo_antes_data_referencia IS NULL THEN
			    CASE
			        WHEN date_part('day'::text, t5.data_referencia) >= 16::double precision THEN to_date(to_char(t5.data_referencia - '1 mon'::interval, 'Mon/yy'::text), 'Mon/yy'::text)
			        ELSE to_date(to_char(t5.data_referencia - '2 mons'::interval, 'Mon/yy'::text), 'Mon/yy'::text)
			    END
			    ELSE first_value(to_date(t5.data, 'mm/yyyy'::text)) OVER (PARTITION BY t5.direct_prospect_id ORDER BY (to_date(t5.data, 'mm/yyyy'::text)) DESC)
			END AS data_referencia
		FROM ( SELECT t4.direct_prospect_id,
	            t4.data_referencia,
	            t4.data_relatorio,
	            t4.divida_atual_pf,
	            t4.indice_ultimo_antes_data_referencia,
	            t4.lv1,
	            t4.nome AS lv2,
	            (jsonb_populate_recordset(NULL::meu2, t4.serie)).data AS data,
	            (jsonb_populate_recordset(NULL::meu2, t4.serie)).valor AS valor
			FROM ( SELECT t3.direct_prospect_id,
	                t3.data_referencia,
	                t3.data_relatorio,
                    CASE
                        WHEN data_relatorio <= t3.data_referencia + '6 mons'::interval THEN t3.divida_atual_pf
                        ELSE NULL::double precision
                    END AS divida_atual_pf,
                    t3.indice_ultimo_antes_data_referencia,
	                t3.nome AS lv1,
	                (jsonb_populate_recordset(NULL::meu, t3.detalhes)).nome AS nome,
	                (jsonb_populate_recordset(NULL::meu, t3.detalhes)).serie AS serie
				FROM ( SELECT t2.direct_prospect_id,
	                    ((t2.data -> 'indicadores'::text) ->> 'dividaAtual'::text)::double precision AS divida_atual_pf,
	                    to_date((t2.data -> 'data_base'::text) ->> 'Data-Base'::text, 'mm/yyyy'::text) AS data_base,
	                    t2.data_referencia,
						t2.data_relatorio,
						t2.indice_ultimo_antes_data_referencia,
	                    (jsonb_populate_recordset(NULL::lucas, t2.data #> '{por_modalidade}'::text[])).nome AS nome,
	                    (jsonb_populate_recordset(NULL::lucas, t2.data #> '{por_modalidade}'::text[])).detalhes AS detalhes
					FROM ( SELECT t1.direct_prospect_id,
						    cc.data,
						    t1.data_referencia,
						    COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date as data_relatorio,
						    t1.indice_ultimo_antes_data_referencia
						FROM credito_coleta cc
					    join lucas_leal.t_scr_pf_choose_report_full t1 ON cc.id = t1.id
					    left join (select distinct direct_prospect_id from lucas_leal.t_scr_pf_get_modal_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
					    where coalesce(t1.indice_ultimo_antes_data_referencia,t1.indice_asc) = 1
					    	and t2.direct_prospect_id is null
					    	) t2) t3) t4) t5) t6
	GROUP BY t6.direct_prospect_id) as modal_json on modal_json.direct_prospect_id = t1.direct_prospect_id
left join(select
		t1.direct_prospect_id,
		coalesce(cc."data"-> 'erro',cc."data"-> 'error')::text as thin_file
	FROM credito_coleta cc
    join lucas_leal.t_scr_pf_choose_report_full t1 ON cc.id = t1.id
    left join (select distinct direct_prospect_id from lucas_leal.t_scr_pf_get_modal_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
    where coalesce(t1.indice_ultimo_antes_data_referencia,t1.indice_asc) = 1
		and t2.direct_prospect_id is null
	) as modal_erro on modal_erro.direct_prospect_id = t1.direct_prospect_id
where coalesce(t1.indice_ultimo_antes_data_referencia,t1.indice_asc) = 1
	and t2.direct_prospect_id is null
)	

select count(*),count(distinct direct_prospect_id)
from lucas_leal.t_scr_pf_get_modal_data_full

select count(*),count(distinct t1.direct_prospect_id)
from lucas_leal.t_scr_pf_choose_report_full t1 
left join (select distinct direct_prospect_id from lucas_leal.t_scr_pf_get_modal_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where coalesce(t1.indice_ultimo_antes_data_referencia,t1.indice_asc) = 1
	and t2.direct_prospect_id is null

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


CREATE INDEX t_scr_pj_choose_report_full_direct_prospect_id ON lucas_leal.t_scr_pj_choose_report_full USING btree (direct_prospect_id);
CREATE INDEX t_scr_pj_choose_report_full_coleta_id ON lucas_leal.t_scr_pj_choose_report_full USING btree (id);

--t_scr_pj_choose_report_full
insert into lucas_leal.t_scr_pj_choose_report_full
(select dp.direct_prospect_id,
    	coalesce(lr.loan_date,coalesce(lr.date_inserted,o.date_inserted,dp.opt_in_date) + interval '10 days')::date as data_referencia,
    	cc.id,
    	case when COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date <= coalesce(lr.loan_date,coalesce(lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '10 days') then 
    		row_number() over (partition by dp.direct_prospect_id order by (COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date <= coalesce(lr.loan_date,coalesce(lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '10 days'))::int desc,
    			COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta) desc) 
    	end as indice_ultimo_antes_data_referencia,
    	row_number() over (partition by dp.direct_prospect_id order by COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)) indice_asc,
    	row_number() over (partition by dp.direct_prospect_id order by COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta) desc) indice_desc
	from credito_coleta cc
	join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
	join lucas_leal.v_funil_completo_sem_duplicacao vfcsd on vfcsd.direct_prospect_id = dp.direct_prospect_id 
    left join offers o on o.offer_id = vfcsd.offer_id 
    left join loan_requests lr on lr.loan_request_id = vfcsd.loan_request_id
	left join (select distinct direct_prospect_id from lucas_leal.t_scr_pj_choose_report_full) tspcrf on tspcrf.direct_prospect_id = dp.direct_prospect_id 
	where cc.documento_tipo::text = 'CNPJ'::text 
		and cc.tipo::text = 'SCR'::text
		and tspcrf.direct_prospect_id is null
	)

select count(*),count(distinct direct_prospect_id)
from lucas_leal.t_scr_pj_choose_report_full
where coalesce(indice_ultimo_antes_data_referencia,indice_asc) = 1	

--upsert
select
    t1.direct_prospect_id,
    (t2.direct_prospect_id is not null)::int as substituir
from(select dp.direct_prospect_id,
    	cc.id,
    	case when COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date <= coalesce(lr.loan_date,coalesce(lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '10 days') then 
    		row_number() over (partition by dp.direct_prospect_id order by (COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date <= coalesce(lr.loan_date,coalesce(lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '10 days'))::int desc,
    			COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta) desc) 
    	end as indice_ultimo_antes_data_referencia
	from credito_coleta cc
	join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
	join lucas_leal.v_funil_completo_sem_duplicacao vfcsd on vfcsd.direct_prospect_id = dp.direct_prospect_id 
    left join offers o on o.offer_id = vfcsd.offer_id 
    left join loan_requests lr on lr.loan_request_id = vfcsd.loan_request_id
	where cc.documento_tipo::text = 'CNPJ'::text 
		and cc.tipo::text = 'SCR'::text
	) as t1
left join lucas_leal.t_scr_pj_choose_report_full t2 on t2.direct_prospect_id = t1.direct_prospect_id and t2.indice_ultimo_antes_data_referencia = 1
where t1.indice_ultimo_antes_data_referencia = 1 and t1.id != coalesce(t2.id,0000)



----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE unique INDEX t_scr_pj_get_history_data_full_direct_prospect_id ON lucas_leal.t_scr_pj_get_history_data_full USING btree (direct_prospect_id);

insert into lucas_leal.t_scr_pj_get_history_data_full
(select
	t1.direct_prospect_id,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_curr else 0 end long_term_debt_pj_curr,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_1m else 0 end long_term_debt_pj_1m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_2m else 0 end long_term_debt_pj_2m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_3m else 0 end long_term_debt_pj_3m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_4m else 0 end long_term_debt_pj_4m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_5m else 0 end long_term_debt_pj_5m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_6m else 0 end long_term_debt_pj_6m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_7m else 0 end long_term_debt_pj_7m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_8m else 0 end long_term_debt_pj_8m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_9m else 0 end long_term_debt_pj_9m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_10m else 0 end long_term_debt_pj_10m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_11m else 0 end long_term_debt_pj_11m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_12m else 0 end long_term_debt_pj_12m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_13m else 0 end long_term_debt_pj_13m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_14m else 0 end long_term_debt_pj_14m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_15m else 0 end long_term_debt_pj_15m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_16m else 0 end long_term_debt_pj_16m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_17m else 0 end long_term_debt_pj_17m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_18m else 0 end long_term_debt_pj_18m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_19m else 0 end long_term_debt_pj_19m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_20m else 0 end long_term_debt_pj_20m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_21m else 0 end long_term_debt_pj_21m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_22m else 0 end long_term_debt_pj_22m,
	case when hist_raw.thin_file is null then hist_json.long_term_debt_pj_23m else 0 end long_term_debt_pj_23m,
	case when hist_raw.thin_file is null then hist_json.ever_long_term_debt_pj else 0 end ever_long_term_debt_pj,
	case when hist_raw.thin_file is null then hist_json.max_long_term_debt_pj else 0 end max_long_term_debt_pj,
	case when hist_raw.thin_file is null then hist_json.overdue_pj_curr else 0 end overdue_pj_curr,
	case when hist_raw.thin_file is null then coalesce(hist_json.months_since_last_overdue_pj,-1) else -1 end months_since_last_overdue_pj,
	case when hist_raw.thin_file is null then hist_json.months_overdue_pj else 0 end months_overdue_pj,
	case when hist_raw.thin_file is null then hist_json.max_overdue_pj else 0 end max_overdue_pj,
	case when hist_raw.thin_file is null then hist_json.default_pj_curr else 0 end default_pj_curr,
	case when hist_raw.thin_file is null then coalesce(hist_json.months_since_last_default_pj,-1) else -1 end months_since_last_default_pj,
	case when hist_raw.thin_file is null then hist_json.months_default_pj else 0 end months_default_pj,
	case when hist_raw.thin_file is null then hist_json.max_default_pj else 0 end max_default_pj,
	case when hist_raw.thin_file is null then hist_json.qtd_meses_escopo_pj else 0 end qtd_meses_escopo_pj,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_curr else 0 end lim_cred_pj_curr,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_1m else 0 end lim_cred_pj_1m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_2m else 0 end lim_cred_pj_2m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_3m else 0 end lim_cred_pj_3m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_4m else 0 end lim_cred_pj_4m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_5m else 0 end lim_cred_pj_5m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_6m else 0 end lim_cred_pj_6m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_7m else 0 end lim_cred_pj_7m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_8m else 0 end lim_cred_pj_8m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_9m else 0 end lim_cred_pj_9m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_10m else 0 end lim_cred_pj_10m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_11m else 0 end lim_cred_pj_11m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_12m else 0 end lim_cred_pj_12m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_13m else 0 end lim_cred_pj_13m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_14m else 0 end lim_cred_pj_14m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_15m else 0 end lim_cred_pj_15m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_16m else 0 end lim_cred_pj_16m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_17m else 0 end lim_cred_pj_17m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_18m else 0 end lim_cred_pj_18m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_19m else 0 end lim_cred_pj_19m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_20m else 0 end lim_cred_pj_20m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_21m else 0 end lim_cred_pj_21m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_22m else 0 end lim_cred_pj_22m,
	case when hist_raw.thin_file is null then hist_json.lim_cred_pj_23m else 0 end lim_cred_pj_23m,
	case when hist_raw.thin_file is null then hist_json.ever_lim_cred_pj else 0 end ever_lim_cred_pj,
	case when hist_raw.thin_file is null then hist_json.max_lim_cred_pj else 0 end max_lim_cred_pj,
	case when hist_raw.thin_file is null then hist_raw.num_ops_pj else 0 end num_ops_pj,
	case when hist_raw.thin_file is null then hist_raw.num_fis_pj else 0 end num_fis_pj,
	case when hist_raw.thin_file is null then hist_raw.first_relation_fi_pj else 0 end first_relation_fi_pj
from lucas_leal.t_scr_pj_choose_report_full t1 
left join (select distinct direct_prospect_id from lucas_leal.t_scr_pj_get_history_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
left join(SELECT t2.direct_prospect_id,
		sum(t2.debtpj) FILTER (WHERE t2.data = t2.data_referencia) AS long_term_debt_pj_curr,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 mon'::interval)) AS long_term_debt_pj_1m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '2 mons'::interval)) AS long_term_debt_pj_2m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '3 mons'::interval)) AS long_term_debt_pj_3m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '4 mons'::interval)) AS long_term_debt_pj_4m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '5 mons'::interval)) AS long_term_debt_pj_5m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '6 mons'::interval)) AS long_term_debt_pj_6m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '7 mons'::interval)) AS long_term_debt_pj_7m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '8 mons'::interval)) AS long_term_debt_pj_8m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '9 mons'::interval)) AS long_term_debt_pj_9m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '10 mons'::interval)) AS long_term_debt_pj_10m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '11 mons'::interval)) AS long_term_debt_pj_11m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year'::interval)) AS long_term_debt_pj_12m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 1 mon'::interval)) AS long_term_debt_pj_13m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 2 mons'::interval)) AS long_term_debt_pj_14m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 3 mons'::interval)) AS long_term_debt_pj_15m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 4 mons'::interval)) AS long_term_debt_pj_16m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 5 mons'::interval)) AS long_term_debt_pj_17m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 6 mons'::interval)) AS long_term_debt_pj_18m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 7 mons'::interval)) AS long_term_debt_pj_19m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 8 mons'::interval)) AS long_term_debt_pj_20m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 9 mons'::interval)) AS long_term_debt_pj_21m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 10 mons'::interval)) AS long_term_debt_pj_22m,
		sum(t2.debtpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 11 mons'::interval)) AS long_term_debt_pj_23m,
		sum(t2.debtpj) FILTER (WHERE t2.data <= t2.data_referencia) AS ever_long_term_debt_pj,
		max(t2.debtpj) FILTER (WHERE t2.data <= t2.data_referencia) AS max_long_term_debt_pj,
		sum(t2.vencidopj) FILTER (WHERE t2.data = t2.data_referencia) AS overdue_pj_curr,
		date_part('year'::text, age(min(t2.data_referencia)::timestamp with time zone, max(t2.data) FILTER (WHERE t2.data <= t2.data_referencia AND t2.vencidopj > 0::double precision)::timestamp with time zone)) * 12::double precision + date_part('month'::text, age(min(t2.data_referencia)::timestamp with time zone, max(t2.data) FILTER (WHERE t2.data <= t2.data_referencia AND t2.vencidopj > 0::double precision)::timestamp with time zone)) AS months_since_last_overdue_pj,
		count(t2.vencidopj) FILTER (WHERE t2.data <= t2.data_referencia AND t2.vencidopj > 0::double precision) AS months_overdue_pj,
		max(t2.vencidopj) FILTER (WHERE t2.data <= t2.data_referencia) AS max_overdue_pj,
		sum(t2.prejuizopj) FILTER (WHERE t2.data = t2.data_referencia) AS default_pj_curr,
		date_part('year'::text, age(min(t2.data_referencia)::timestamp with time zone, max(t2.data) FILTER (WHERE t2.data <= t2.data_referencia AND t2.prejuizopj > 0::double precision)::timestamp with time zone)) * 12::double precision + date_part('month'::text, age(min(t2.data_referencia)::timestamp with time zone, max(t2.data) FILTER (WHERE t2.data <= t2.data_referencia AND t2.prejuizopj > 0::double precision)::timestamp with time zone)) AS months_since_last_default_pj,
		count(t2.prejuizopj) FILTER (WHERE t2.data <= t2.data_referencia AND t2.prejuizopj > 0::double precision) AS months_default_pj,
		max(t2.prejuizopj) FILTER (WHERE t2.data <= t2.data_referencia) AS max_default_pj,
		count(*) FILTER (WHERE t2.data <= t2.data_referencia) AS qtd_meses_escopo_pj,
		sum(t2.limcredpj) FILTER (WHERE t2.data = t2.data_referencia) AS lim_cred_pj_curr,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 mon'::interval)) AS lim_cred_pj_1m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '2 mons'::interval)) AS lim_cred_pj_2m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '3 mons'::interval)) AS lim_cred_pj_3m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '4 mons'::interval)) AS lim_cred_pj_4m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '5 mons'::interval)) AS lim_cred_pj_5m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '6 mons'::interval)) AS lim_cred_pj_6m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '7 mons'::interval)) AS lim_cred_pj_7m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '8 mons'::interval)) AS lim_cred_pj_8m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '9 mons'::interval)) AS lim_cred_pj_9m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '10 mons'::interval)) AS lim_cred_pj_10m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '11 mons'::interval)) AS lim_cred_pj_11m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year'::interval)) AS lim_cred_pj_12m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 1 mon'::interval)) AS lim_cred_pj_13m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 2 mons'::interval)) AS lim_cred_pj_14m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 3 mons'::interval)) AS lim_cred_pj_15m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 4 mons'::interval)) AS lim_cred_pj_16m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 5 mons'::interval)) AS lim_cred_pj_17m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 6 mons'::interval)) AS lim_cred_pj_18m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 7 mons'::interval)) AS lim_cred_pj_19m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 8 mons'::interval)) AS lim_cred_pj_20m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 9 mons'::interval)) AS lim_cred_pj_21m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 10 mons'::interval)) AS lim_cred_pj_22m,
		sum(t2.limcredpj) FILTER (WHERE t2.data = (t2.data_referencia - '1 year 11 mons'::interval)) AS lim_cred_pj_23m,
		sum(t2.limcredpj) FILTER (WHERE t2.data <= t2.data_referencia) AS ever_lim_cred_pj,
		max(t2.limcredpj) FILTER (WHERE t2.data <= t2.data_referencia) AS max_lim_cred_pj
	FROM ( SELECT t1.direct_prospect_id,
            CASE
                WHEN t1.indice_ultimo_antes_data_referencia IS NOT NULL THEN to_date((((cc.data -> 'historico'::text) -> 'Carteira de Crédito'::text) -> 0) ->> 'data'::text, 'mm-yyyy'::text)
                ELSE
                CASE
                    WHEN date_part('day'::text, t1.data_referencia) >= 16::double precision THEN to_date(to_char(t1.data_referencia - '1 mon'::interval, 'Mon/yy'::text), 'Mon/yy'::text)
                    ELSE to_date(to_char(t1.data_referencia - '2 mons'::interval, 'Mon/yy'::text), 'Mon/yy'::text)
                END
            END AS data_referencia,
            to_date(jsonb_array_elements((cc.data -> 'historico'::text) -> 'Carteira de Crédito'::text) ->> 'data'::text, 'mm/yyyy'::text) AS data,
            replace(replace(replace(jsonb_array_elements((cc.data -> 'historico'::text) -> 'Carteira de Crédito'::text) ->> 'valor'::text, '-'::text, '0'::text), '.'::text, ''::text), ','::text, '.'::text)::double precision AS debtpj,
            replace(replace(replace(jsonb_array_elements((cc.data -> 'historico'::text) -> 'Vencido'::text) ->> 'valor'::text, '-'::text, '0'::text), '.'::text, ''::text), ','::text, '.'::text)::double precision AS vencidopj,
            replace(replace(replace(jsonb_array_elements((cc.data -> 'historico'::text) -> 'Prejuízo'::text) ->> 'valor'::text, '-'::text, '0'::text), '.'::text, ''::text), ','::text, '.'::text)::double precision AS prejuizopj,
			replace(replace(replace(jsonb_array_elements((cc.data -> 'historico'::text) -> 'Limite de Crédito'::text) ->> 'valor'::text, '-'::text, '0'::text), '.'::text, ''::text), ','::text, '.'::text)::double precision AS limcredpj
		from credito_coleta cc
	    join lucas_leal.t_scr_pj_choose_report_full t1 ON cc.id = t1.id
		left join (select distinct direct_prospect_id from lucas_leal.t_scr_pj_get_history_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
	    where coalesce(t1.indice_ultimo_antes_data_referencia,t1.indice_asc) = 1
			and t2.direct_prospect_id is null
		) t2
	GROUP BY t2.direct_prospect_id) as hist_json on hist_json.direct_prospect_id = t1.direct_prospect_id
left join(SELECT t1.direct_prospect_id,
            CASE
                WHEN COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date > t1.data_referencia + '6 mons'::interval THEN NULL::integer
                ELSE replace(replace(
                CASE
                    WHEN "position"(cc.raw, 'Quantidade de Operações'::text) > 0 THEN "substring"(cc.raw, "position"(cc.raw, 'Quantidade de Operações'::text) + length(
                    CASE
                        WHEN "position"(cc.raw, 'class="fu3" width="50%"'::text) > 0 THEN 'Quantidade de Operações</div></td><td class="fu3" width="50%"><b>'::text
                        ELSE 'Quantidade de Operações</div>        </td>        <td width="50%" class="fu3">                    <b>'::text
                    END), 3)
                    ELSE NULL::text
                END, '<'::text, ''::text), '/'::text, ''::text)::integer
            END AS num_ops_pj,
            CASE
                WHEN COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date > t1.data_referencia + '6 mons'::interval THEN NULL::integer
                ELSE replace(replace(
                CASE
                    WHEN "position"(cc.raw, 'Quantidade de IFs em que o Cliente possui Operações'::text) > 0 THEN "substring"(cc.raw, "position"(cc.raw, 'Quantidade de IFs em que o Cliente possui Operações'::text) + length(
                    CASE
                        WHEN "position"(cc.raw, 'class="fu3" width="50%"'::text) > 0 THEN 'Quantidade de IFs em que o Cliente possui Operações</div></td><td class="fu3" width="50%"><b>'::text
                        ELSE 'Quantidade de IFs em que o Cliente possui Operações</div>        </td>        <td width="50%" class="fu3">                    <b>'::text
                    END), 3)
                    ELSE NULL::text
                END, '<'::text, ''::text), '/'::text, ''::text)::integer
            END AS num_fis_pj,
            CASE
                WHEN COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date > t1.data_referencia + '6 mons'::interval THEN NULL::integer
                ELSE date_part('year'::text, age(t1.data_referencia::timestamp with time zone, to_date(replace(replace(replace(
                CASE
                    WHEN "position"(cc.raw, 'Data de Início de Relacionamento com a IF'::text) > 0 THEN "substring"(cc.raw, "position"(cc.raw, 'Data de Início de Relacionamento com a IF'::text) + length(
                    CASE
                        WHEN "position"(cc.raw, 'class="fu3" width="50%"'::text) > 0 THEN 'Data de Início de Relacionamento com a IF</div></td><td class="fu3" width="50%"><b>'::text
                        ELSE 'Data de Início de Relacionamento com a IF</div>        </td>        <td width="50%" class="fu3">                         <b>'::text
                    END), 10)
                    ELSE NULL::text
                END, '<'::text, ''::text), '/'::text, ''::text), '-'::text, '0'::text), 'ddmmyyyy'::text)::timestamp with time zone))::integer
            END AS first_relation_fi_pj,
			coalesce(cc."data"-> 'erro',cc."data"-> 'error')::text as thin_file
		FROM credito_coleta cc
	    join lucas_leal.t_scr_pj_choose_report_full t1 ON cc.id = t1.id
		left join (select distinct direct_prospect_id from lucas_leal.t_scr_pj_get_history_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
	    where coalesce(t1.indice_ultimo_antes_data_referencia,t1.indice_asc) = 1
			and t2.direct_prospect_id is null
         ) as hist_raw on hist_raw.direct_prospect_id = t1.direct_prospect_id
where coalesce(t1.indice_ultimo_antes_data_referencia,t1.indice_asc) = 1
	and t2.direct_prospect_id is null 
)


select count(*),count(distinct t1.direct_prospect_id)
from lucas_leal.t_scr_pj_choose_report_full t1 
left join (select distinct direct_prospect_id from lucas_leal.t_scr_pj_get_history_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where coalesce(t1.indice_ultimo_antes_data_referencia,t1.indice_asc) = 1
	and t2.direct_prospect_id is null


	
select count(*),count(distinct direct_prospect_id)
from lucas_leal.t_scr_pj_get_history_data_full
	
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE unique INDEX t_scr_pj_get_modal_data_full_direct_prospect_id ON lucas_leal.t_scr_pj_get_modal_data_full USING btree (direct_prospect_id);

insert into lucas_leal.t_scr_pj_get_modal_data_full
(select
	t1.direct_prospect_id,
	case when modal_erro.thin_file is null then modal_json.divida_atual_pj else 0 end divida_atual_pj,
	case when modal_erro.thin_file is null then modal_json.qtd_meses_modalidade_pj else 0 end qtd_meses_modalidade_pj,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pj_curr else 0 end emprestimos_pj_curr,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pj_1m else 0 end emprestimos_pj_1m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pj_2m else 0 end emprestimos_pj_2m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pj_3m else 0 end emprestimos_pj_3m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pj_4m else 0 end emprestimos_pj_4m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pj_5m else 0 end emprestimos_pj_5m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pj_6m else 0 end emprestimos_pj_6m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pj_7m else 0 end emprestimos_pj_7m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pj_8m else 0 end emprestimos_pj_8m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pj_9m else 0 end emprestimos_pj_9m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pj_10m else 0 end emprestimos_pj_10m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_pj_11m else 0 end emprestimos_pj_11m,
	case when modal_erro.thin_file is null then modal_json.ever_emprestimos_pj else 0 end ever_emprestimos_pj,
	case when modal_erro.thin_file is null then modal_json.count_emprestimos_pj else 0 end count_emprestimos_pj,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_cheque_especial_pj_curr else 0 end emprestimos_conta_garantida_cheque_especial_pj_curr,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_cheque_especial_pj_1m else 0 end emprestimos_conta_garantida_cheque_especial_pj_1m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_cheque_especial_pj_2m else 0 end emprestimos_conta_garantida_cheque_especial_pj_2m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_cheque_especial_pj_3m else 0 end emprestimos_conta_garantida_cheque_especial_pj_3m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_cheque_especial_pj_4m else 0 end emprestimos_conta_garantida_cheque_especial_pj_4m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_cheque_especial_pj_5m else 0 end emprestimos_conta_garantida_cheque_especial_pj_5m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_cheque_especial_pj_6m else 0 end emprestimos_conta_garantida_cheque_especial_pj_6m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_cheque_especial_pj_7m else 0 end emprestimos_conta_garantida_cheque_especial_pj_7m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_cheque_especial_pj_8m else 0 end emprestimos_conta_garantida_cheque_especial_pj_8m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_cheque_especial_pj_9m else 0 end emprestimos_conta_garantida_cheque_especial_pj_9m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_cheque_especial_pj_10m else 0 end emprestimos_conta_garantida_cheque_especial_pj_10m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_cheque_especial_pj_11m else 0 end emprestimos_conta_garantida_cheque_especial_pj_11m,
	case when modal_erro.thin_file is null then modal_json.ever_emprestimos_conta_garantida_cheque_especial_pj else 0 end ever_emprestimos_conta_garantida_cheque_especial_pj,
	case when modal_erro.thin_file is null then modal_json.count_emprestimos_conta_garantida_cheque_especial_pj else 0 end count_emprestimos_conta_garantida_cheque_especial_pj,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pj_curr else 0 end emprestimos_cheque_especial_pj_curr,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pj_1m else 0 end emprestimos_cheque_especial_pj_1m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pj_2m else 0 end emprestimos_cheque_especial_pj_2m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pj_3m else 0 end emprestimos_cheque_especial_pj_3m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pj_4m else 0 end emprestimos_cheque_especial_pj_4m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pj_5m else 0 end emprestimos_cheque_especial_pj_5m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pj_6m else 0 end emprestimos_cheque_especial_pj_6m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pj_7m else 0 end emprestimos_cheque_especial_pj_7m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pj_8m else 0 end emprestimos_cheque_especial_pj_8m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pj_9m else 0 end emprestimos_cheque_especial_pj_9m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pj_10m else 0 end emprestimos_cheque_especial_pj_10m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cheque_especial_pj_11m else 0 end emprestimos_cheque_especial_pj_11m,
	case when modal_erro.thin_file is null then modal_json.ever_emprestimos_cheque_especial_pj else 0 end ever_emprestimos_cheque_especial_pj,
	case when modal_erro.thin_file is null then modal_json.count_emprestimos_cheque_especial_pj else 0 end count_emprestimos_cheque_especial_pj,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_longo_pj_curr else 0 end emprestimos_giro_longo_pj_curr,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_longo_pj_1m else 0 end emprestimos_giro_longo_pj_1m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_longo_pj_2m else 0 end emprestimos_giro_longo_pj_2m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_longo_pj_3m else 0 end emprestimos_giro_longo_pj_3m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_longo_pj_4m else 0 end emprestimos_giro_longo_pj_4m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_longo_pj_5m else 0 end emprestimos_giro_longo_pj_5m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_longo_pj_6m else 0 end emprestimos_giro_longo_pj_6m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_longo_pj_7m else 0 end emprestimos_giro_longo_pj_7m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_longo_pj_8m else 0 end emprestimos_giro_longo_pj_8m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_longo_pj_9m else 0 end emprestimos_giro_longo_pj_9m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_longo_pj_10m else 0 end emprestimos_giro_longo_pj_10m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_longo_pj_11m else 0 end emprestimos_giro_longo_pj_11m,
	case when modal_erro.thin_file is null then modal_json.ever_emprestimos_giro_longo_pj else 0 end ever_emprestimos_giro_longo_pj,
	case when modal_erro.thin_file is null then modal_json.count_emprestimos_giro_longo_pj else 0 end count_emprestimos_giro_longo_pj,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_curto_pj_curr else 0 end emprestimos_giro_curto_pj_curr,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_curto_pj_1m else 0 end emprestimos_giro_curto_pj_1m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_curto_pj_2m else 0 end emprestimos_giro_curto_pj_2m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_curto_pj_3m else 0 end emprestimos_giro_curto_pj_3m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_curto_pj_4m else 0 end emprestimos_giro_curto_pj_4m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_curto_pj_5m else 0 end emprestimos_giro_curto_pj_5m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_curto_pj_6m else 0 end emprestimos_giro_curto_pj_6m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_curto_pj_7m else 0 end emprestimos_giro_curto_pj_7m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_curto_pj_8m else 0 end emprestimos_giro_curto_pj_8m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_curto_pj_9m else 0 end emprestimos_giro_curto_pj_9m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_curto_pj_10m else 0 end emprestimos_giro_curto_pj_10m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_curto_pj_11m else 0 end emprestimos_giro_curto_pj_11m,
	case when modal_erro.thin_file is null then modal_json.ever_emprestimos_giro_curto_pj else 0 end ever_emprestimos_giro_curto_pj,
	case when modal_erro.thin_file is null then modal_json.count_emprestimos_giro_curto_pj else 0 end count_emprestimos_giro_curto_pj,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pj_curr else 0 end emprestimos_cartao_credito_pj_curr,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pj_1m else 0 end emprestimos_cartao_credito_pj_1m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pj_2m else 0 end emprestimos_cartao_credito_pj_2m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pj_3m else 0 end emprestimos_cartao_credito_pj_3m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pj_4m else 0 end emprestimos_cartao_credito_pj_4m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pj_5m else 0 end emprestimos_cartao_credito_pj_5m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pj_6m else 0 end emprestimos_cartao_credito_pj_6m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pj_7m else 0 end emprestimos_cartao_credito_pj_7m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pj_8m else 0 end emprestimos_cartao_credito_pj_8m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pj_9m else 0 end emprestimos_cartao_credito_pj_9m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pj_10m else 0 end emprestimos_cartao_credito_pj_10m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_cartao_credito_pj_11m else 0 end emprestimos_cartao_credito_pj_11m,
	case when modal_erro.thin_file is null then modal_json.ever_emprestimos_cartao_credito_pj else 0 end ever_emprestimos_cartao_credito_pj,
	case when modal_erro.thin_file is null then modal_json.count_emprestimos_cartao_credito_pj else 0 end count_emprestimos_cartao_credito_pj,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_pj_curr else 0 end emprestimos_conta_garantida_pj_curr,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_pj_1m else 0 end emprestimos_conta_garantida_pj_1m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_pj_2m else 0 end emprestimos_conta_garantida_pj_2m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_pj_3m else 0 end emprestimos_conta_garantida_pj_3m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_pj_4m else 0 end emprestimos_conta_garantida_pj_4m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_pj_5m else 0 end emprestimos_conta_garantida_pj_5m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_pj_6m else 0 end emprestimos_conta_garantida_pj_6m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_pj_7m else 0 end emprestimos_conta_garantida_pj_7m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_pj_8m else 0 end emprestimos_conta_garantida_pj_8m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_pj_9m else 0 end emprestimos_conta_garantida_pj_9m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_pj_10m else 0 end emprestimos_conta_garantida_pj_10m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_conta_garantida_pj_11m else 0 end emprestimos_conta_garantida_pj_11m,
	case when modal_erro.thin_file is null then modal_json.ever_emprestimos_conta_garantida_pj else 0 end ever_emprestimos_conta_garantida_pj,
	case when modal_erro.thin_file is null then modal_json.count_emprestimos_conta_garantida_pj else 0 end count_emprestimos_conta_garantida_pj,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pj_curr else 0 end emprestimos_outros_emprestimos_pj_curr,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pj_1m else 0 end emprestimos_outros_emprestimos_pj_1m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pj_2m else 0 end emprestimos_outros_emprestimos_pj_2m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pj_3m else 0 end emprestimos_outros_emprestimos_pj_3m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pj_4m else 0 end emprestimos_outros_emprestimos_pj_4m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pj_5m else 0 end emprestimos_outros_emprestimos_pj_5m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pj_6m else 0 end emprestimos_outros_emprestimos_pj_6m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pj_7m else 0 end emprestimos_outros_emprestimos_pj_7m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pj_8m else 0 end emprestimos_outros_emprestimos_pj_8m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pj_9m else 0 end emprestimos_outros_emprestimos_pj_9m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pj_10m else 0 end emprestimos_outros_emprestimos_pj_10m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_outros_emprestimos_pj_11m else 0 end emprestimos_outros_emprestimos_pj_11m,
	case when modal_erro.thin_file is null then modal_json.ever_emprestimos_outros_emprestimos_pj else 0 end ever_emprestimos_outros_emprestimos_pj,
	case when modal_erro.thin_file is null then modal_json.count_emprestimos_outros_emprestimos_pj else 0 end count_emprestimos_outros_emprestimos_pj,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_rotativo_pj_curr else 0 end emprestimos_giro_rotativo_pj_curr,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_rotativo_pj_1m else 0 end emprestimos_giro_rotativo_pj_1m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_rotativo_pj_2m else 0 end emprestimos_giro_rotativo_pj_2m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_rotativo_pj_3m else 0 end emprestimos_giro_rotativo_pj_3m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_rotativo_pj_4m else 0 end emprestimos_giro_rotativo_pj_4m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_rotativo_pj_5m else 0 end emprestimos_giro_rotativo_pj_5m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_rotativo_pj_6m else 0 end emprestimos_giro_rotativo_pj_6m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_rotativo_pj_7m else 0 end emprestimos_giro_rotativo_pj_7m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_rotativo_pj_8m else 0 end emprestimos_giro_rotativo_pj_8m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_rotativo_pj_9m else 0 end emprestimos_giro_rotativo_pj_9m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_rotativo_pj_10m else 0 end emprestimos_giro_rotativo_pj_10m,
	case when modal_erro.thin_file is null then modal_json.emprestimos_giro_rotativo_pj_11m else 0 end emprestimos_giro_rotativo_pj_11m,
	case when modal_erro.thin_file is null then modal_json.ever_emprestimos_giro_rotativo_pj else 0 end ever_emprestimos_giro_rotativo_pj,
	case when modal_erro.thin_file is null then modal_json.count_emprestimos_giro_rotativo_pj else 0 end count_emprestimos_giro_rotativo_pj,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pj_curr else 0 end outroscreditos_pj_curr,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pj_1m else 0 end outroscreditos_pj_1m,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pj_2m else 0 end outroscreditos_pj_2m,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pj_3m else 0 end outroscreditos_pj_3m,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pj_4m else 0 end outroscreditos_pj_4m,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pj_5m else 0 end outroscreditos_pj_5m,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pj_6m else 0 end outroscreditos_pj_6m,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pj_7m else 0 end outroscreditos_pj_7m,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pj_8m else 0 end outroscreditos_pj_8m,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pj_9m else 0 end outroscreditos_pj_9m,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pj_10m else 0 end outroscreditos_pj_10m,
	case when modal_erro.thin_file is null then modal_json.outroscreditos_pj_11m else 0 end outroscreditos_pj_11m,
	case when modal_erro.thin_file is null then modal_json.ever_outroscreditos_pj else 0 end ever_outroscreditos_pj,
	case when modal_erro.thin_file is null then modal_json.count_outroscreditos_pj else 0 end count_outroscreditos_pj,
	case when modal_erro.thin_file is null then modal_json.desconto_pj_curr else 0 end desconto_pj_curr,
	case when modal_erro.thin_file is null then modal_json.desconto_pj_1m else 0 end desconto_pj_1m,
	case when modal_erro.thin_file is null then modal_json.desconto_pj_2m else 0 end desconto_pj_2m,
	case when modal_erro.thin_file is null then modal_json.desconto_pj_3m else 0 end desconto_pj_3m,
	case when modal_erro.thin_file is null then modal_json.desconto_pj_4m else 0 end desconto_pj_4m,
	case when modal_erro.thin_file is null then modal_json.desconto_pj_5m else 0 end desconto_pj_5m,
	case when modal_erro.thin_file is null then modal_json.desconto_pj_6m else 0 end desconto_pj_6m,
	case when modal_erro.thin_file is null then modal_json.desconto_pj_7m else 0 end desconto_pj_7m,
	case when modal_erro.thin_file is null then modal_json.desconto_pj_8m else 0 end desconto_pj_8m,
	case when modal_erro.thin_file is null then modal_json.desconto_pj_9m else 0 end desconto_pj_9m,
	case when modal_erro.thin_file is null then modal_json.desconto_pj_10m else 0 end desconto_pj_10m,
	case when modal_erro.thin_file is null then modal_json.desconto_pj_11m else 0 end desconto_pj_11m,
	case when modal_erro.thin_file is null then modal_json.ever_desconto_pj else 0 end ever_desconto_pj,
	case when modal_erro.thin_file is null then modal_json.count_desconto_pj else 0 end count_desconto_pj,
	case when modal_erro.thin_file is null then modal_json.financiamentos_pj_curr else 0 end financiamentos_pj_curr,
	case when modal_erro.thin_file is null then modal_json.financiamentos_pj_1m else 0 end financiamentos_pj_1m,
	case when modal_erro.thin_file is null then modal_json.financiamentos_pj_2m else 0 end financiamentos_pj_2m,
	case when modal_erro.thin_file is null then modal_json.financiamentos_pj_3m else 0 end financiamentos_pj_3m,
	case when modal_erro.thin_file is null then modal_json.financiamentos_pj_4m else 0 end financiamentos_pj_4m,
	case when modal_erro.thin_file is null then modal_json.financiamentos_pj_5m else 0 end financiamentos_pj_5m,
	case when modal_erro.thin_file is null then modal_json.financiamentos_pj_6m else 0 end financiamentos_pj_6m,
	case when modal_erro.thin_file is null then modal_json.financiamentos_pj_7m else 0 end financiamentos_pj_7m,
	case when modal_erro.thin_file is null then modal_json.financiamentos_pj_8m else 0 end financiamentos_pj_8m,
	case when modal_erro.thin_file is null then modal_json.financiamentos_pj_9m else 0 end financiamentos_pj_9m,
	case when modal_erro.thin_file is null then modal_json.financiamentos_pj_10m else 0 end financiamentos_pj_10m,
	case when modal_erro.thin_file is null then modal_json.financiamentos_pj_11m else 0 end financiamentos_pj_11m,
	case when modal_erro.thin_file is null then modal_json.ever_financiamentos_pj else 0 end ever_financiamentos_pj,
	case when modal_erro.thin_file is null then modal_json.count_financiamentos_pj else 0 end count_financiamentos_pj,
	case when modal_erro.thin_file is null then modal_json.adiantamentos_pj_curr else 0 end adiantamentos_pj_curr,
	case when modal_erro.thin_file is null then modal_json.adiantamentos_pj_1m else 0 end adiantamentos_pj_1m,
	case when modal_erro.thin_file is null then modal_json.adiantamentos_pj_2m else 0 end adiantamentos_pj_2m,
	case when modal_erro.thin_file is null then modal_json.adiantamentos_pj_3m else 0 end adiantamentos_pj_3m,
	case when modal_erro.thin_file is null then modal_json.adiantamentos_pj_4m else 0 end adiantamentos_pj_4m,
	case when modal_erro.thin_file is null then modal_json.adiantamentos_pj_5m else 0 end adiantamentos_pj_5m,
	case when modal_erro.thin_file is null then modal_json.adiantamentos_pj_6m else 0 end adiantamentos_pj_6m,
	case when modal_erro.thin_file is null then modal_json.adiantamentos_pj_7m else 0 end adiantamentos_pj_7m,
	case when modal_erro.thin_file is null then modal_json.adiantamentos_pj_8m else 0 end adiantamentos_pj_8m,
	case when modal_erro.thin_file is null then modal_json.adiantamentos_pj_9m else 0 end adiantamentos_pj_9m,
	case when modal_erro.thin_file is null then modal_json.adiantamentos_pj_10m else 0 end adiantamentos_pj_10m,
	case when modal_erro.thin_file is null then modal_json.adiantamentos_pj_11m else 0 end adiantamentos_pj_11m,
	case when modal_erro.thin_file is null then modal_json.ever_adiantamentos_pj else 0 end ever_adiantamentos_pj,
	case when modal_erro.thin_file is null then modal_json.count_adiantamentos_pj else 0 end count_adiantamentos_pj,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pj_curr else 0 end outrosfinanciamentos_pj_curr,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pj_1m else 0 end outrosfinanciamentos_pj_1m,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pj_2m else 0 end outrosfinanciamentos_pj_2m,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pj_3m else 0 end outrosfinanciamentos_pj_3m,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pj_4m else 0 end outrosfinanciamentos_pj_4m,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pj_5m else 0 end outrosfinanciamentos_pj_5m,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pj_6m else 0 end outrosfinanciamentos_pj_6m,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pj_7m else 0 end outrosfinanciamentos_pj_7m,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pj_8m else 0 end outrosfinanciamentos_pj_8m,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pj_9m else 0 end outrosfinanciamentos_pj_9m,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pj_10m else 0 end outrosfinanciamentos_pj_10m,
	case when modal_erro.thin_file is null then modal_json.outrosfinanciamentos_pj_11m else 0 end outrosfinanciamentos_pj_11m,
	case when modal_erro.thin_file is null then modal_json.ever_outrosfinanciamentos_pj else 0 end ever_outrosfinanciamentos_pj,
	case when modal_erro.thin_file is null then modal_json.count_outrosfinanciamentos_pj else 0 end count_outrosfinanciamentos_pj,
	case when modal_erro.thin_file is null then modal_json.limitecredito_curto_pj_curr else 0 end limitecredito_curto_pj_curr,
	case when modal_erro.thin_file is null then modal_json.limitecredito_curto_pj_1m else 0 end limitecredito_curto_pj_1m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_curto_pj_2m else 0 end limitecredito_curto_pj_2m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_curto_pj_3m else 0 end limitecredito_curto_pj_3m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_curto_pj_4m else 0 end limitecredito_curto_pj_4m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_curto_pj_5m else 0 end limitecredito_curto_pj_5m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_curto_pj_6m else 0 end limitecredito_curto_pj_6m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_curto_pj_7m else 0 end limitecredito_curto_pj_7m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_curto_pj_8m else 0 end limitecredito_curto_pj_8m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_curto_pj_9m else 0 end limitecredito_curto_pj_9m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_curto_pj_10m else 0 end limitecredito_curto_pj_10m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_curto_pj_11m else 0 end limitecredito_curto_pj_11m,
	case when modal_erro.thin_file is null then modal_json.ever_limitecredito_curto_pj else 0 end ever_limitecredito_curto_pj,
	case when modal_erro.thin_file is null then modal_json.count_limitecredito_curto_pj else 0 end count_limitecredito_curto_pj,
	case when modal_erro.thin_file is null then modal_json.limitecredito_longo_pj_curr else 0 end limitecredito_longo_pj_curr,
	case when modal_erro.thin_file is null then modal_json.limitecredito_longo_pj_1m else 0 end limitecredito_longo_pj_1m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_longo_pj_2m else 0 end limitecredito_longo_pj_2m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_longo_pj_3m else 0 end limitecredito_longo_pj_3m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_longo_pj_4m else 0 end limitecredito_longo_pj_4m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_longo_pj_5m else 0 end limitecredito_longo_pj_5m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_longo_pj_6m else 0 end limitecredito_longo_pj_6m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_longo_pj_7m else 0 end limitecredito_longo_pj_7m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_longo_pj_8m else 0 end limitecredito_longo_pj_8m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_longo_pj_9m else 0 end limitecredito_longo_pj_9m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_longo_pj_10m else 0 end limitecredito_longo_pj_10m,
	case when modal_erro.thin_file is null then modal_json.limitecredito_longo_pj_11m else 0 end limitecredito_longo_pj_11m,
	case when modal_erro.thin_file is null then modal_json.ever_limitecredito_longo_pj else 0 end ever_limitecredito_longo_pj,
	case when modal_erro.thin_file is null then modal_json.count_limitecredito_longo_pj else 0 end count_limitecredito_longo_pj,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pj_curr else 0 end prejuizo_pj_curr,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pj_1m else 0 end prejuizo_pj_1m,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pj_2m else 0 end prejuizo_pj_2m,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pj_3m else 0 end prejuizo_pj_3m,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pj_4m else 0 end prejuizo_pj_4m,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pj_5m else 0 end prejuizo_pj_5m,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pj_6m else 0 end prejuizo_pj_6m,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pj_7m else 0 end prejuizo_pj_7m,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pj_8m else 0 end prejuizo_pj_8m,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pj_9m else 0 end prejuizo_pj_9m,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pj_10m else 0 end prejuizo_pj_10m,
	case when modal_erro.thin_file is null then modal_json.prejuizo_pj_11m else 0 end prejuizo_pj_11m,
	case when modal_erro.thin_file is null then modal_json.ever_prejuizo_pj else 0 end ever_prejuizo_pj,
	case when modal_erro.thin_file is null then modal_json.count_prejuizo_pj else 0 end count_prejuizo_pj,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pj_curr else 0 end carteiracredito_pj_curr,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pj_1m else 0 end carteiracredito_pj_1m,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pj_2m else 0 end carteiracredito_pj_2m,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pj_3m else 0 end carteiracredito_pj_3m,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pj_4m else 0 end carteiracredito_pj_4m,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pj_5m else 0 end carteiracredito_pj_5m,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pj_6m else 0 end carteiracredito_pj_6m,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pj_7m else 0 end carteiracredito_pj_7m,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pj_8m else 0 end carteiracredito_pj_8m,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pj_9m else 0 end carteiracredito_pj_9m,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pj_10m else 0 end carteiracredito_pj_10m,
	case when modal_erro.thin_file is null then modal_json.carteiracredito_pj_11m else 0 end carteiracredito_pj_11m,
	case when modal_erro.thin_file is null then modal_json.ever_carteiracredito_pj else 0 end ever_carteiracredito_pj,
	case when modal_erro.thin_file is null then modal_json.count_carteiracredito_pj else 0 end count_carteiracredito_pj
from lucas_leal.t_scr_pj_choose_report_full t1
left join (select distinct direct_prospect_id from lucas_leal.t_scr_pj_get_modal_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
left join(SELECT t6.direct_prospect_id,
    t6.divida_atual_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.to_date <= t6.data_referencia),0) as  qtd_meses_modalidade_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text),0) as  emprestimos_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as  emprestimos_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as  emprestimos_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as  emprestimos_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as  emprestimos_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as  emprestimos_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as  emprestimos_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as  emprestimos_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as  emprestimos_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as  emprestimos_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as  emprestimos_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text),0) as  emprestimos_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text),0) as  ever_emprestimos_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text),0) as  count_emprestimos_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = ANY (ARRAY['Conta Garantida'::text, 'Cheque Especial'::text]))),0) as  emprestimos_conta_garantida_cheque_especial_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = ANY (ARRAY['Conta Garantida'::text, 'Cheque Especial'::text]))),0) as  emprestimos_conta_garantida_cheque_especial_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = ANY (ARRAY['Conta Garantida'::text, 'Cheque Especial'::text]))),0) as  emprestimos_conta_garantida_cheque_especial_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = ANY (ARRAY['Conta Garantida'::text, 'Cheque Especial'::text]))),0) as  emprestimos_conta_garantida_cheque_especial_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = ANY (ARRAY['Conta Garantida'::text, 'Cheque Especial'::text]))),0) as  emprestimos_conta_garantida_cheque_especial_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = ANY (ARRAY['Conta Garantida'::text, 'Cheque Especial'::text]))),0) as  emprestimos_conta_garantida_cheque_especial_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = ANY (ARRAY['Conta Garantida'::text, 'Cheque Especial'::text]))),0) as  emprestimos_conta_garantida_cheque_especial_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = ANY (ARRAY['Conta Garantida'::text, 'Cheque Especial'::text]))),0) as  emprestimos_conta_garantida_cheque_especial_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = ANY (ARRAY['Conta Garantida'::text, 'Cheque Especial'::text]))),0) as  emprestimos_conta_garantida_cheque_especial_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = ANY (ARRAY['Conta Garantida'::text, 'Cheque Especial'::text]))),0) as  emprestimos_conta_garantida_cheque_especial_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = ANY (ARRAY['Conta Garantida'::text, 'Cheque Especial'::text]))),0) as  emprestimos_conta_garantida_cheque_especial_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = ANY (ARRAY['Conta Garantida'::text, 'Cheque Especial'::text]))),0) as  emprestimos_conta_garantida_cheque_especial_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = ANY (ARRAY['Conta Garantida'::text, 'Cheque Especial'::text]))),0) as  ever_emprestimos_conta_garantida_cheque_especial_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = ANY (ARRAY['Conta Garantida'::text, 'Cheque Especial'::text]))),0) as  count_emprestimos_conta_garantida_cheque_especial_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as  emprestimos_cheque_especial_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as  emprestimos_cheque_especial_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as  emprestimos_cheque_especial_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as  emprestimos_cheque_especial_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as  emprestimos_cheque_especial_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as  emprestimos_cheque_especial_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as  emprestimos_cheque_especial_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as  emprestimos_cheque_especial_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as  emprestimos_cheque_especial_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as  emprestimos_cheque_especial_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as  emprestimos_cheque_especial_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as  emprestimos_cheque_especial_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as  ever_emprestimos_cheque_especial_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Cheque Especial'::text),0) as  count_emprestimos_cheque_especial_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  ever_emprestimos_giro_longo_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  count_emprestimos_giro_longo_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  ever_emprestimos_giro_curto_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  count_emprestimos_giro_curto_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as  emprestimos_cartao_credito_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as  emprestimos_cartao_credito_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as  emprestimos_cartao_credito_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as  emprestimos_cartao_credito_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as  emprestimos_cartao_credito_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as  emprestimos_cartao_credito_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as  emprestimos_cartao_credito_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as  emprestimos_cartao_credito_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as  emprestimos_cartao_credito_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as  emprestimos_cartao_credito_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as  emprestimos_cartao_credito_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as  emprestimos_cartao_credito_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as  ever_emprestimos_cartao_credito_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND (t6.lv2 = 'Crédito Rotativo Vinculado a Cartão de Crédito'::text OR t6.lv2 = 'Cartão de Crédito - Compra ou Fatura Parcelada Pela Instituição Financeira Emitente do Cartão'::text OR t6.lv2 = 'Cartão de crédito - não migrado'::text)),0) as  count_emprestimos_cartao_credito_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Conta Garantida'::text),0) as  emprestimos_conta_garantida_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Conta Garantida'::text),0) as  emprestimos_conta_garantida_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Conta Garantida'::text),0) as  emprestimos_conta_garantida_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Conta Garantida'::text),0) as  emprestimos_conta_garantida_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Conta Garantida'::text),0) as  emprestimos_conta_garantida_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Conta Garantida'::text),0) as  emprestimos_conta_garantida_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Conta Garantida'::text),0) as  emprestimos_conta_garantida_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Conta Garantida'::text),0) as  emprestimos_conta_garantida_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Conta Garantida'::text),0) as  emprestimos_conta_garantida_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Conta Garantida'::text),0) as  emprestimos_conta_garantida_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Conta Garantida'::text),0) as  emprestimos_conta_garantida_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Conta Garantida'::text),0) as  emprestimos_conta_garantida_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Conta Garantida'::text),0) as  ever_emprestimos_conta_garantida_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Conta Garantida'::text),0) as  count_emprestimos_conta_garantida_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as  emprestimos_outros_emprestimos_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as  emprestimos_outros_emprestimos_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as  emprestimos_outros_emprestimos_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as  emprestimos_outros_emprestimos_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as  emprestimos_outros_emprestimos_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as  emprestimos_outros_emprestimos_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as  emprestimos_outros_emprestimos_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as  emprestimos_outros_emprestimos_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as  emprestimos_outros_emprestimos_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as  emprestimos_outros_emprestimos_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as  emprestimos_outros_emprestimos_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as  emprestimos_outros_emprestimos_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as  ever_emprestimos_outros_emprestimos_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Outros Empréstimos'::text),0) as  count_emprestimos_outros_emprestimos_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  ever_emprestimos_giro_rotativo_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  count_emprestimos_giro_rotativo_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Outros Créditos'::text),0) as  outroscreditos_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as  outroscreditos_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as  outroscreditos_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as  outroscreditos_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as  outroscreditos_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as  outroscreditos_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as  outroscreditos_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as  outroscreditos_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as  outroscreditos_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as  outroscreditos_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as  outroscreditos_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Outros Créditos'::text),0) as  outroscreditos_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Outros Créditos'::text),0) as  ever_outroscreditos_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Outros Créditos'::text),0) as  count_outroscreditos_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Direitos Creditórios Descontados'::text),0) as  desconto_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Direitos Creditórios Descontados'::text),0) as  desconto_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Direitos Creditórios Descontados'::text),0) as  desconto_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Direitos Creditórios Descontados'::text),0) as  desconto_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Direitos Creditórios Descontados'::text),0) as  desconto_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Direitos Creditórios Descontados'::text),0) as  desconto_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Direitos Creditórios Descontados'::text),0) as  desconto_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Direitos Creditórios Descontados'::text),0) as  desconto_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Direitos Creditórios Descontados'::text),0) as  desconto_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Direitos Creditórios Descontados'::text),0) as  desconto_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Direitos Creditórios Descontados'::text),0) as  desconto_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Direitos Creditórios Descontados'::text),0) as  desconto_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Direitos Creditórios Descontados'::text),0) as  ever_desconto_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Direitos Creditórios Descontados'::text),0) as  count_desconto_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Financiamentos'::text),0) as  ever_financiamentos_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Financiamentos'::text),0) as  count_financiamentos_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Adiantamentos a Depositantes'::text),0) as  adiantamentos_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Adiantamentos a Depositantes'::text),0) as  adiantamentos_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Adiantamentos a Depositantes'::text),0) as  adiantamentos_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Adiantamentos a Depositantes'::text),0) as  adiantamentos_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Adiantamentos a Depositantes'::text),0) as  adiantamentos_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Adiantamentos a Depositantes'::text),0) as  adiantamentos_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Adiantamentos a Depositantes'::text),0) as  adiantamentos_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Adiantamentos a Depositantes'::text),0) as  adiantamentos_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Adiantamentos a Depositantes'::text),0) as  adiantamentos_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Adiantamentos a Depositantes'::text),0) as  adiantamentos_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Adiantamentos a Depositantes'::text),0) as  adiantamentos_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Adiantamentos a Depositantes'::text),0) as  adiantamentos_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Adiantamentos a Depositantes'::text),0) as  ever_adiantamentos_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Adiantamentos a Depositantes'::text),0) as  count_adiantamentos_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  ever_outrosfinanciamentos_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  count_outrosfinanciamentos_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento até 360 dias'::text),0) as  limitecredito_curto_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento até 360 dias'::text),0) as  limitecredito_curto_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento até 360 dias'::text),0) as  limitecredito_curto_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento até 360 dias'::text),0) as  limitecredito_curto_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento até 360 dias'::text),0) as  limitecredito_curto_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento até 360 dias'::text),0) as  limitecredito_curto_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento até 360 dias'::text),0) as  limitecredito_curto_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento até 360 dias'::text),0) as  limitecredito_curto_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento até 360 dias'::text),0) as  limitecredito_curto_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento até 360 dias'::text),0) as  limitecredito_curto_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento até 360 dias'::text),0) as  limitecredito_curto_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text),0) as  limitecredito_curto_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento até 360 dias'::text),0) as  ever_limitecredito_curto_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento até 360 dias'::text),0) as  count_limitecredito_curto_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento acima de 360 dias'::text),0) as  limitecredito_longo_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento acima de 360 dias'::text),0) as  limitecredito_longo_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento acima de 360 dias'::text),0) as  limitecredito_longo_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento acima de 360 dias'::text),0) as  limitecredito_longo_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento acima de 360 dias'::text),0) as  limitecredito_longo_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento acima de 360 dias'::text),0) as  limitecredito_longo_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento acima de 360 dias'::text),0) as  limitecredito_longo_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento acima de 360 dias'::text),0) as  limitecredito_longo_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento acima de 360 dias'::text),0) as  limitecredito_longo_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento acima de 360 dias'::text),0) as  limitecredito_longo_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento acima de 360 dias'::text),0) as  limitecredito_longo_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento acima de 360 dias'::text),0) as  limitecredito_longo_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento acima de 360 dias'::text),0) as  ever_limitecredito_longo_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Limite de Crédito (H)'::text AND t6.lv2 = 'Limite de crédito com vencimento acima de 360 dias'::text),0) as  count_limitecredito_longo_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Prejuízo (B)'::text),0) as  prejuizo_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as  prejuizo_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as  prejuizo_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as  prejuizo_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as  prejuizo_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as  prejuizo_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as  prejuizo_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as  prejuizo_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as  prejuizo_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as  prejuizo_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as  prejuizo_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Prejuízo (B)'::text),0) as  prejuizo_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Prejuízo (B)'::text),0) as  ever_prejuizo_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Prejuízo (B)'::text),0) as  count_prejuizo_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as  carteiracredito_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as  carteiracredito_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as  carteiracredito_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as  carteiracredito_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as  carteiracredito_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as  carteiracredito_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as  carteiracredito_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as  carteiracredito_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as  carteiracredito_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as  carteiracredito_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as  carteiracredito_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as  carteiracredito_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as  ever_carteiracredito_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as  count_carteiracredito_pj
   FROM ( SELECT t5.direct_prospect_id,
            t5.divida_atual_pj,
            regexp_replace(t5.lv1, '\d{2}\s-\s'::text, ''::text) AS lv1,
            replace(replace(replace(replace(regexp_replace(t5.lv2, '\d{4}\s-\s'::text, ''::text), ' Â– '::text, ' - '::text), ' -- '::text, ' - '::text), ' Â–- '::text, ' - '::text), 'interfinanceiros'::text, 'Interfinanceiros'::text) AS lv2,
            to_date(t5.data, 'mm/yyyy'::text) AS to_date,
                CASE
                    WHEN t5.valor IS NULL THEN 0::double precision
                    ELSE t5.valor
                END AS valor,
                CASE
                    WHEN t5.indice_ultimo_antes_data_referencia IS NULL THEN
                    CASE
                        WHEN date_part('day'::text, t5.data_referencia) >= 16::double precision THEN to_date(to_char(t5.data_referencia - '1 mon'::interval, 'Mon/yy'::text), 'Mon/yy'::text)
                        ELSE to_date(to_char(t5.data_referencia - '2 mons'::interval, 'Mon/yy'::text), 'Mon/yy'::text)
                    END
                    ELSE first_value(to_date(t5.data, 'mm/yyyy'::text)) OVER (PARTITION BY t5.direct_prospect_id ORDER BY (to_date(t5.data, 'mm/yyyy'::text)) DESC)
                END AS data_referencia
           FROM ( SELECT t4.direct_prospect_id,
                    t4.data_referencia,
                    t4.indice_ultimo_antes_data_referencia,
                    t4.divida_atual_pj,
                    t4.lv1,
                    t4.nome AS lv2,
                    (jsonb_populate_recordset(NULL::meu2, t4.serie)).data AS data,
                    (jsonb_populate_recordset(NULL::meu2, t4.serie)).valor AS valor
                   FROM ( SELECT t3.direct_prospect_id,
                            t3.data_referencia,
                            t3.indice_ultimo_antes_data_referencia,
                            CASE
                                WHEN data_relatorio <= t3.data_referencia + '6 mons'::interval THEN t3.divida_atual_pj
                                ELSE NULL::double precision
                            END AS divida_atual_pj,
                            t3.nome AS lv1,
                            (jsonb_populate_recordset(NULL::meu, t3.detalhes)).nome AS nome,
                            (jsonb_populate_recordset(NULL::meu, t3.detalhes)).serie AS serie
                           FROM ( SELECT t2.direct_prospect_id,
                                    ((t2.data -> 'indicadores'::text) ->> 'dividaAtual'::text)::double precision AS divida_atual_pj,
                                    to_date((t2.data -> 'data_base'::text) ->> 'Data-Base'::text, 'mm/yyyy'::text) AS data_base,
                                    t2.data_referencia,
                                    t2.data_relatorio,
                                    t2.indice_ultimo_antes_data_referencia,
                                    (jsonb_populate_recordset(NULL::lucas, t2.data #> '{por_modalidade}'::text[])).nome AS nome,
                                    (jsonb_populate_recordset(NULL::lucas, t2.data #> '{por_modalidade}'::text[])).detalhes AS detalhes
                                   FROM ( SELECT t1.direct_prospect_id,
									    cc.data,
									    t1.data_referencia,
									    COALESCE(to_timestamp(left(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text),19), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date as data_relatorio,
									    t1.indice_ultimo_antes_data_referencia
									FROM credito_coleta cc
									    join lucas_leal.t_scr_pj_choose_report_full t1 ON cc.id = t1.id
										left join (select distinct direct_prospect_id from lucas_leal.t_scr_pj_get_modal_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
									    where coalesce(t1.indice_ultimo_antes_data_referencia,t1.indice_asc) = 1
											and t2.direct_prospect_id is null
									     ) t2) t3) t4) t5) t6
  GROUP BY t6.direct_prospect_id, t6.divida_atual_pj) as modal_json on modal_json.direct_prospect_id = t1.direct_prospect_id
left join( SELECT t1.direct_prospect_id,
		coalesce(cc."data"-> 'erro',cc."data"-> 'error')::text as thin_file
	FROM credito_coleta cc
	JOIN lucas_leal.t_scr_pj_choose_report_full t1 ON cc.id = t1.id 
	left join (select distinct direct_prospect_id from lucas_leal.t_scr_pj_get_modal_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
    where coalesce(t1.indice_ultimo_antes_data_referencia,t1.indice_asc) = 1
		and t2.direct_prospect_id is null
    ) as modal_erro on modal_erro.direct_prospect_id = t1.direct_prospect_id
where coalesce(t1.indice_ultimo_antes_data_referencia,t1.indice_asc) = 1
	and t2.direct_prospect_id is null
)


select count(*),count(distinct t1.direct_prospect_id)
from lucas_leal.t_scr_pj_choose_report_full t1 
left join (select distinct direct_prospect_id from lucas_leal.t_scr_pj_get_modal_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where coalesce(t1.indice_ultimo_antes_data_referencia,t1.indice_asc) = 1
	and t2.direct_prospect_id is null
	
select count(*),count(distinct direct_prospect_id)
from lucas_leal.t_scr_pj_get_modal_data_full



select max(this_view_last_refresh_time)
from mv_lead_tape mlt  

