-- t_experian_choose_report
insert into lucas_leal.t_experian_choose_report
(SELECT dp.direct_prospect_id,
    dp.cnpj,
    dp.cpf,
    max(to_timestamp((((cc.data -> 'datetime'::text) ->> 'data'::text) || ' '::text) || ((cc.data -> 'datetime'::text) ->> 'hora'::text), 'yyyymmdd HH24:MI:SS'::text) + '00:00:00.001'::interval * cc.id::double precision) AS max_date
   FROM credito_coleta cc
     JOIN direct_prospects dp ON "left"(cc.documento::text, 8) = "left"(dp.cnpj::text, 8)
     JOIN offers o ON o.direct_prospect_id = dp.direct_prospect_id
     JOIN loan_requests lr ON lr.offer_id = o.offer_id
  WHERE lr.status::text = 'ACCEPTED'::text and lr.contrato_assinado_url is not null AND to_date((cc.data -> 'datetime'::text) ->> 'data'::text, 'yyyymmdd'::text) <= lr.loan_date AND (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying, 'RelatoBasico'::character varying]::text[]))
  	and dp.direct_prospect_id not in (select direct_prospect_id from lucas_leal.t_experian_choose_report)
  GROUP BY dp.direct_prospect_id, dp.cnpj, dp.cpf)

  
--t_experian_get_company_negative_public_record_data  
insert into lucas_leal.t_experian_get_company_negative_public_record_data
(select
	coalesce(t.direct_prospect_id,t1.direct_prospect_id) as direct_prospect_id,
	coalesce(empresa_divida_valor,0) as empresa_divida_valor,
	coalesce(empresa_ccf_valor,0) as empresa_ccf_valor,
	coalesce(empresa_protesto_valor,0) as empresa_protesto_valor,
	coalesce(empresa_acao_valor,0) as empresa_acao_valor,
	coalesce(empresa_pefin_valor,0) as empresa_pefin_valor,
	coalesce(empresa_refin_valor,0) as empresa_refin_valor,
	coalesce(empresa_spc_valor,0) as empresa_spc_valor,
	coalesce(empresa_divida_unit,0) as empresa_divida_unit,
	coalesce(empresa_ccf_unit,0) as empresa_ccf_unit,
	coalesce(empresa_protesto_unit,0) as empresa_protesto_unit,
	coalesce(empresa_acao_unit,0) as empresa_acao_unit,
	coalesce(empresa_pefin_unit,0) as empresa_pefin_unit,
	coalesce(empresa_refin_unit,0) as empresa_refin_unit,
	coalesce(empresa_spc_unit,0) as empresa_spc_unit,
	coalesce(socio_divida_valor,0) as socio_divida_valor,
	coalesce(socio_ccf_valor,0) as socio_ccf_valor,
	coalesce(socio_acao_valor,0) as socio_acao_valor,
	coalesce(socio_pefin_valor,0) as socio_pefin_valor,
	coalesce(socio_protesto_valor,0) as socio_protesto_valor,
	coalesce(socio_refin_valor,0) as socio_refin_valor,
	coalesce(socio_spc_valor,0) as socio_spc_valor,
	coalesce(socio_divida_unit,0) as socio_divida_unit,
	coalesce(socio_ccf_unit,0) as socio_ccf_unit,
	coalesce(socio_protesto_unit,0) as socio_protesto_unit,
	coalesce(socio_acao_unit,0) as socio_acao_unit,
	coalesce(socio_pefin_unit,0) as socio_pefin_unit,
	coalesce(socio_refin_unit,0) as socio_refin_unit,
	coalesce(socio_spc_unit,0) as socio_spc_unit,
	coalesce(empresa_divida_valor + empresa_ccf_valor + empresa_protesto_valor + empresa_acao_valor + empresa_pefin_valor + empresa_refin_valor + empresa_spc_valor,0) AS empresa_total_valor,
    coalesce(empresa_divida_unit + empresa_ccf_unit + empresa_protesto_unit + empresa_acao_unit + empresa_pefin_unit + empresa_refin_unit + empresa_spc_unit,0) AS empresa_total_unit,
	coalesce(socio_divida_valor + socio_ccf_valor + socio_protesto_valor + socio_acao_valor + socio_pefin_valor + socio_refin_valor + socio_spc_valor,0) AS socios_total_valor,
    coalesce(socio_divida_unit + socio_ccf_unit + socio_protesto_unit + socio_acao_unit + socio_pefin_unit + socio_refin_unit + socio_spc_unit,0) AS socios_total_unit,
	coalesce((empresa_divida_valor + empresa_ccf_valor + empresa_protesto_valor + empresa_acao_valor + empresa_pefin_valor + empresa_refin_valor + empresa_spc_valor) + (socio_divida_valor + socio_ccf_valor + socio_protesto_valor + socio_acao_valor + socio_pefin_valor + socio_refin_valor + socio_spc_valor),0) AS all_valor,
    coalesce((empresa_divida_unit + empresa_ccf_unit + empresa_protesto_unit + empresa_acao_unit + empresa_pefin_unit + empresa_refin_unit + empresa_spc_unit) + (socio_divida_unit + socio_ccf_unit + socio_protesto_unit + socio_acao_unit + socio_pefin_unit + socio_refin_unit + socio_spc_unit),0) AS all_unit
from lucas_leal.t_experian_choose_report t
full outer join(SELECT coalesce(spc.direct_prospect_id,serasa.direct_prospect_id) as direct_prospect_id,
	    COALESCE(sum(serasa.divida_valor) FILTER (WHERE spc.nome = 'empresa'::text), 0::double precision) AS empresa_divida_valor,
	    COALESCE(sum(serasa.ccf_valor) FILTER (WHERE spc.nome = 'empresa'::text), 0::double precision) AS empresa_ccf_valor,
	    COALESCE(sum(serasa.protesto_valor) FILTER (WHERE spc.nome = 'empresa'::text), 0::double precision) AS empresa_protesto_valor,
	    COALESCE(sum(serasa.acao_valor) FILTER (WHERE spc.nome = 'empresa'::text), 0::double precision) AS empresa_acao_valor,
	    COALESCE(sum(serasa.pefin_valor) FILTER (WHERE spc.nome = 'empresa'::text), 0::double precision) AS empresa_pefin_valor,
	    COALESCE(sum(serasa.refin_valor) FILTER (WHERE spc.nome = 'empresa'::text), 0::double precision) AS empresa_refin_valor,
	    COALESCE(sum(spc.spc_valor) FILTER (WHERE spc.nome = 'empresa'::text), 0::double precision) AS empresa_spc_valor,
	    coalesce(sum(serasa.divida_unit) FILTER (WHERE spc.nome = 'empresa'::text), 0::double precision) as empresa_divida_unit,
	    coalesce(sum(serasa.ccf_unit) FILTER (WHERE spc.nome = 'empresa'::text), 0::double precision) as empresa_ccf_unit,
	    coalesce(sum(serasa.protesto_unit) FILTER (WHERE spc.nome = 'empresa'::text), 0::double precision) as empresa_protesto_unit,
	    coalesce(sum(serasa.acao_unit) FILTER (WHERE spc.nome = 'empresa'::text), 0::double precision) as empresa_acao_unit,
	    coalesce(sum(serasa.pefin_unit) FILTER (WHERE spc.nome = 'empresa'::text), 0::double precision) as empresa_pefin_unit,
	    coalesce(sum(serasa.refin_unit) FILTER (WHERE spc.nome = 'empresa'::text), 0::double precision) as empresa_refin_unit,
	    coalesce(sum(spc.spc_unit) FILTER (WHERE spc.nome = 'empresa'::text), 0::double precision) as empresa_spc_unit,
	    coalesce(sum(serasa.divida_valor) FILTER (WHERE spc.spc_share IS NOT NULL), 0::double precision) as socio_divida_valor,
	    coalesce(sum(serasa.ccf_valor) FILTER (WHERE spc.spc_share IS NOT NULL), 0::double precision) as socio_ccf_valor,
	    coalesce(sum(serasa.acao_valor) FILTER (WHERE spc.spc_share IS NOT NULL), 0::double precision) as socio_acao_valor,
	    coalesce(sum(serasa.pefin_valor) FILTER (WHERE spc.spc_share IS NOT NULL), 0::double precision) as socio_pefin_valor,
	    coalesce(sum(serasa.protesto_valor) FILTER (WHERE spc.spc_share IS NOT NULL), 0::double precision) as socio_protesto_valor,
	    coalesce(sum(serasa.refin_valor) FILTER (WHERE spc.spc_share IS NOT NULL), 0::double precision) as socio_refin_valor,
	    coalesce(sum(spc.spc_valor) FILTER (WHERE spc.spc_share IS NOT NULL), 0::double precision) as socio_spc_valor,
	    coalesce(sum(serasa.divida_unit) FILTER (WHERE spc.spc_share IS NOT NULL), 0::double precision) as socio_divida_unit,
	    coalesce(sum(serasa.ccf_unit) FILTER (WHERE spc.spc_share IS NOT NULL), 0::double precision) as socio_ccf_unit,
	    coalesce(sum(serasa.protesto_unit) FILTER (WHERE spc.spc_share IS NOT NULL), 0::double precision) as socio_protesto_unit,
	    coalesce(sum(serasa.acao_unit) FILTER (WHERE spc.spc_share IS NOT NULL), 0::double precision) as socio_acao_unit,
	    coalesce(sum(serasa.pefin_unit) FILTER (WHERE spc.spc_share IS NOT NULL), 0::double precision) as socio_pefin_unit,
	    coalesce(sum(serasa.refin_unit) FILTER (WHERE spc.spc_share IS NOT NULL), 0::double precision) as socio_refin_unit,
	    coalesce(sum(spc.spc_unit) FILTER (WHERE spc.spc_share IS NOT NULL), 0::double precision) as socio_spc_unit
   FROM ( SELECT socios_share.direct_prospect_id,
            socios_share.nome,
            socios_share.spc_share,
                CASE
                    WHEN COALESCE(spc_socios.spc_socios_valor, '0'::text) = ''::text THEN '0'::text
                    ELSE spc_socios.spc_socios_valor
                END::double precision AS spc_valor,
                CASE
                    WHEN COALESCE(spc_socios.spc_socios_unit, '0'::text) = ''::text THEN '0'::text
                    ELSE spc_socios.spc_socios_unit
                END::integer AS spc_unit
           FROM ( SELECT t1.direct_prospect_id,
                    jsonb_object_keys(cc.data -> 'acionistas'::text) AS nome,
                    ((((cc.data -> 'acionistas'::text) -> jsonb_object_keys(cc.data -> 'acionistas'::text)) ->> 'percentual'::text)::double precision) / 100::double precision AS spc_share
                   FROM credito_coleta cc
                     JOIN lucas_leal.t_experian_choose_report t1 ON (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying, 'RelatoBasico'::character varying]::text[])) AND "left"(t1.cnpj::text, 8) = "left"(cc.documento::text, 8) AND (to_timestamp((((cc.data -> 'datetime'::text) ->> 'data'::text) || ' '::text) || ((cc.data -> 'datetime'::text) ->> 'hora'::text), 'yyyymmdd HH24:MI:SS'::text) + '00:00:01'::interval * cc.id::double precision) = t1.max_date
                     where t1.direct_prospect_id = 940268 --not in (select direct_prospect_id from lucas_leal.t_experian_get_company_negative_public_record_data)
                     ) socios_share
             LEFT JOIN ( SELECT t1.direct_prospect_id,
                    ((cc.data -> 'spc'::text) -> jsonb_object_keys(cc.data -> 'spc'::text)) ->> 'nome'::text AS spc_nome,
                    ((cc.data -> 'spc'::text) -> jsonb_object_keys(cc.data -> 'spc'::text)) ->> 'valor'::text AS spc_socios_valor,
                    ((cc.data -> 'spc'::text) -> jsonb_object_keys(cc.data -> 'spc'::text)) ->> 'quantidade'::text AS spc_socios_unit
                   FROM credito_coleta cc
                     JOIN lucas_leal.t_experian_choose_report t1 ON (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying, 'RelatoBasico'::character varying]::text[])) AND "left"(t1.cnpj::text, 8) = "left"(cc.documento::text, 8) AND (to_timestamp((((cc.data -> 'datetime'::text) ->> 'data'::text) || ' '::text) || ((cc.data -> 'datetime'::text) ->> 'hora'::text), 'yyyymmdd HH24:MI:SS'::text) + '00:00:01'::interval * cc.id::double precision) = t1.max_date
                     where t1.direct_prospect_id = 940268--not in (select direct_prospect_id from lucas_leal.t_experian_get_company_negative_public_record_data)
                     ) spc_socios ON spc_socios.direct_prospect_id = socios_share.direct_prospect_id AND spc_socios.spc_nome = socios_share.nome
        UNION
         SELECT t2.direct_prospect_id,
            'empresa'::text AS nome,
            NULL::double precision AS spc_share,
            sum(COALESCE(t2.spc_valor, 0::double precision)) AS spc_valor,
            sum(COALESCE(t2.spc_unit, 0::double precision)) AS spc_unit
           FROM ( SELECT t1.direct_prospect_id,
                    ((cc.data -> 'anotacao_spc'::text) ->> 'total_debito'::text)::double precision AS spc_valor,
                    ((cc.data -> 'anotacao_spc'::text) ->> 'total_ocr'::text)::double precision AS spc_unit
                   FROM credito_coleta cc
                     JOIN lucas_leal.t_experian_choose_report t1 ON (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying, 'RelatoBasico'::character varying]::text[])) AND "left"(t1.cnpj::text, 8) = "left"(cc.documento::text, 8) AND (to_timestamp((((cc.data -> 'datetime'::text) ->> 'data'::text) || ' '::text) || ((cc.data -> 'datetime'::text) ->> 'hora'::text), 'yyyymmdd HH24:MI:SS'::text) + '00:00:01'::interval * cc.id::double precision) = t1.max_date
                     where t1.direct_prospect_id = 940268--not in (select direct_prospect_id from lucas_leal.t_experian_get_company_negative_public_record_data)
                     ) t2 
          GROUP BY t2.direct_prospect_id) spc
     full outer JOIN ( SELECT t1.direct_prospect_id,
            jsonb_object_keys(cc.data -> 'anotacoes'::text) AS serasa_nome,
            (((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Resumo'::text) -> 'DIVIDA_VENCIDA'::text) ->> 'valor'::text)::double precision AS divida_valor,
            (((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Resumo'::text) -> 'CHEQUE'::text) ->> 'valor'::text)::double precision AS ccf_valor,
            (((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Resumo'::text) -> 'PROTESTO'::text) ->> 'valor'::text)::double precision AS protesto_valor,
            (((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Resumo'::text) -> 'ACAO_JUDICIAL'::text) ->> 'valor'::text)::double precision AS acao_valor,
            ((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Pefin'::text) ->> 'valor_total'::text)::double precision AS pefin_valor,
            ((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Refin'::text) ->> 'valor_total'::text)::double precision AS refin_valor,
            (((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Resumo'::text) -> 'DIVIDA_VENCIDA'::text) ->> 'quantidade'::text)::double precision AS divida_unit,
            (((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Resumo'::text) -> 'CHEQUE'::text) ->> 'quantidade'::text)::double precision AS ccf_unit,
            (((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Resumo'::text) -> 'PROTESTO'::text) ->> 'quantidade'::text)::double precision AS protesto_unit,
            (((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Resumo'::text) -> 'ACAO_JUDICIAL'::text) ->> 'quantidade'::text)::double precision AS acao_unit,
            ((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Pefin'::text) ->> 'quantidade'::text)::double precision AS pefin_unit,
            ((((cc.data -> 'anotacoes'::text) -> jsonb_object_keys(cc.data -> 'anotacoes'::text)) -> 'Refin'::text) ->> 'quantidade'::text)::double precision AS refin_unit
           FROM credito_coleta cc
           JOIN lucas_leal.t_experian_choose_report t1 ON (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying, 'RelatoBasico'::character varying]::text[])) AND "left"(t1.cnpj::text, 8) = "left"(cc.documento::text, 8) AND (to_timestamp((((cc.data -> 'datetime'::text) ->> 'data'::text) || ' '::text) || ((cc.data -> 'datetime'::text) ->> 'hora'::text), 'yyyymmdd HH24:MI:SS'::text) + '00:00:01'::interval * cc.id::double precision) = t1.max_date
           where t1.direct_prospect_id = 940268--not in (select direct_prospect_id from lucas_leal.t_experian_get_company_negative_public_record_data)
        UNION
         SELECT serasas_antigos_parseados.lead_id AS direct_prospect_id,
            'empresa'::text AS serasa_nome,
            serasas_antigos_parseados.experian_debt_overdue_value AS divida_valor,
            serasas_antigos_parseados.experian_bad_check_value AS ccf_valor,
            serasas_antigos_parseados.experian_registry_protest_value AS protesto_valor,
            serasas_antigos_parseados.experian_legal_action_value AS acao_valor,
            0 AS pefin_valor,
            0 AS pefin_valor,
            serasas_antigos_parseados.experian_debt_overdue_unit AS divida_unit,
            serasas_antigos_parseados.experian_bad_check_unit AS ccf_unit,
            serasas_antigos_parseados.experian_registry_protest_unit AS protesto_unit,
            serasas_antigos_parseados.experian_legal_action_unit AS acao_unit,
            0 AS pefin_unit,
            0 AS refin_unit
           FROM lucas_leal.serasas_antigos_parseados
           where serasas_antigos_parseados.lead_id = 940268--not in (select direct_prospect_id from lucas_leal.t_experian_get_company_negative_public_record_data)
           ) serasa ON serasa.direct_prospect_id = spc.direct_prospect_id AND spc.nome = serasa.serasa_nome
  GROUP BY 1) as t1 on t1.direct_prospect_id = t.direct_prospect_id
where t.direct_prospect_id = 940268--not in (select direct_prospect_id from lucas_leal.t_experian_get_company_negative_public_record_data)
)
 
  
--t_experian_get_factoring_inquiry_check_data
insert into t_experian_get_factoring_inquiry_check_data
(SELECT t2.direct_prospect_id,
    sum(
        CASE
            WHEN "position"(t2.consultas, 'INVEST DIR'::text) > 0 OR "position"(t2.consultas, 'CREDITORIOS'::text) > 0 OR "position"(t2.consultas, 'FUNDO DE INVE'::text) > 0 OR "position"(t2.consultas, 'SECURITIZADORA'::text) > 0 OR "position"(t2.consultas, 'FACTORING'::text) > 0 OR "position"(t2.consultas, 'FOMENTO'::text) > 0 OR "position"(t2.consultas, 'FIDC'::text) > 0 OR "position"(t2.consultas, 'NOVA S R M'::text) > 0 OR "position"(t2.consultas, 'RED AS'::text) > 0 OR "position"(t2.consultas, 'DEL MONTE SERVICOS'::text) > 0 OR "position"(t2.consultas, 'SERVICOS FINANCEIROS'::text) > 0 THEN 1
            ELSE 0
        END) AS consulta_factoring
   FROM ( SELECT t1.direct_prospect_id,
            ((cc.data -> 'consulta_empresas'::text) -> jsonb_object_keys(cc.data -> 'consulta_empresas'::text)) ->> 'nome'::text AS consultas
           FROM credito_coleta cc
             JOIN t_experian_choose_report t1 ON (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying, 'RelatoBasico'::character varying]::text[])) AND "left"(t1.cnpj::text, 8) = "left"(cc.documento::text, 8) AND (to_timestamp((((cc.data -> 'datetime'::text) ->> 'data'::text) || ' '::text) || ((cc.data -> 'datetime'::text) ->> 'hora'::text), 'yyyymmdd HH24:MI:SS'::text) + '00:00:00.001'::interval * cc.id::double precision) = t1.max_date
            where t1.direct_prospect_id not in (select direct_prospect_id from t_experian_get_factoring_inquiry_check_data) 
        UNION
         SELECT t1.direct_prospect_id,
            ((cc.data -> 'consulta_spc'::text) -> jsonb_object_keys(cc.data -> 'consulta_spc'::text)) ->> 'nome'::text AS consultas
           FROM credito_coleta cc
             JOIN t_experian_choose_report t1 ON (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying, 'RelatoBasico'::character varying]::text[])) AND "left"(t1.cnpj::text, 8) = "left"(cc.documento::text, 8) AND (to_timestamp((((cc.data -> 'datetime'::text) ->> 'data'::text) || ' '::text) || ((cc.data -> 'datetime'::text) ->> 'hora'::text), 'yyyymmdd HH24:MI:SS'::text) + '00:00:00.001'::interval * cc.id::double precision) = t1.max_date
            where t1.direct_prospect_id not in (select direct_prospect_id from t_experian_get_factoring_inquiry_check_data)) t2 
  GROUP BY t2.direct_prospect_id
/*
UNION
 SELECT serasas_antigos_parseados.lead_id AS direct_prospect_id,
    serasas_antigos_parseados.experian_report_recently_taken_by_factoring AS consulta_factoring
   FROM serasas_antigos_parseados
*/
   )

--t_experian_get_related_companies_data
insert into t_experian_get_related_companies_data
(SELECT t.direct_prospect_id,
	count(*) FILTER (WHERE t2.empresas_problemas = 'S'::text) AS empresas_problema
from t_experian_choose_report t
left join ( SELECT t1.direct_prospect_id,
		((cc.data -> 'participacoes'::text) -> jsonb_object_keys(cc.data -> 'participacoes'::text)) ->> 'restricao'::text AS empresas_problemas
	FROM credito_coleta cc
    JOIN t_experian_choose_report t1 ON (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying, 'RelatoBasico'::character varying]::text[])) AND "left"(t1.cnpj::text, 8) = "left"(cc.documento::text, 8) AND (to_timestamp((((cc.data -> 'datetime'::text) ->> 'data'::text) || ' '::text) || ((cc.data -> 'datetime'::text) ->> 'hora'::text), 'yyyymmdd HH24:MI:SS'::text) + '00:00:00.001'::interval * cc.id::double precision) = t1.max_date
    ) t2 on t2.direct_prospect_id = t.direct_prospect_id
where t.direct_prospect_id not in (select direct_prospect_id from t_experian_get_related_companies_data)
GROUP BY t.direct_prospect_id)


--t_experian_get_score
insert into t_experian_get_score
(SELECT t1.direct_prospect_id,
    (cc.data ->> 'score'::text)::double precision AS serasa_coleta,
    (cc.data ->> 'pd'::text)::double precision AS pd_serasa
   FROM credito_coleta cc
     JOIN t_experian_choose_report t1 ON "left"(t1.cnpj::text, 8) = "left"(cc.documento::text, 8) AND (to_timestamp((((cc.data -> 'datetime'::text) ->> 'data'::text) || ' '::text) || ((cc.data -> 'datetime'::text) ->> 'hora'::text), 'yyyymmdd HH24:MI:SS'::text) + '00:00:00.001'::interval * cc.id::double precision) = t1.max_date AND (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying, 'RelatoBasico'::character varying]::text[]))
	where t1.direct_prospect_id not in (select direct_prospect_id from t_experian_get_score))


--t_experian_get_shareholders_data
insert into t_experian_get_shareholders_data
(SELECT t1.direct_prospect_id,
    t1.cpf,
	((cc.data -> 'acionistas'::text) -> jsonb_object_keys(cc.data -> 'acionistas'::text)) ->> 'cpf'::text AS cpf_socios,
    jsonb_object_keys(cc.data -> 'acionistas'::text) AS nome,
    (((cc.data -> 'acionistas'::text) -> jsonb_object_keys(cc.data -> 'acionistas'::text)) ->> 'percentual'::text)::numeric AS share
   FROM credito_coleta cc
     JOIN t_experian_choose_report t1 ON (cc.tipo::text = ANY (ARRAY['Serasa'::character varying::text, 'RelatoSocios'::character varying::text, 'RelatoBasico'::character varying::text])) AND "left"(t1.cnpj::text, 8) = "left"(cc.documento::text, 8) AND (to_timestamp((((cc.data -> 'datetime'::text) ->> 'data'::text) || ' '::text) || ((cc.data -> 'datetime'::text) ->> 'hora'::text), 'yyyymmdd HH24:MI:SS'::text) + '00:00:00.001'::interval * cc.id::double precision) = t1.max_date
	where t1.direct_prospect_id not in (select direct_prospect_id from t_experian_get_shareholders_data))


--t_experian_get_serasa_inquiries_data
insert into t_experian_get_serasa_inquiries_data
(select
	coalesce(t.direct_prospect_id,serasa.direct_prospect_id,spc.direct_prospect_id) as direct_prospect_id,
	coalesce(consultas_serasa_atual,0) + coalesce(consultas_spc_atual,0) as consultas_serasa_atual,
	coalesce(consultas_serasa_1m,0) + coalesce(consultas_spc_1m,0) as consultas_1m,
	coalesce(consultas_serasa_2m,0) + coalesce(consultas_spc_2m,0) as consultas_2m,
	coalesce(consultas_serasa_3m,0) + coalesce(consultas_spc_3m,0) as consultas_3m,
	coalesce(consultas_serasa_4m,0) + coalesce(consultas_spc_4m,0) as consultas_4m,
	coalesce(consultas_serasa_5m,0) + coalesce(consultas_spc_5m,0) as consultas_5m,
	coalesce(consultas_serasa_6m,0) + coalesce(consultas_spc_6m,0) as consultas_6m,
	coalesce(consultas_serasa_7m,0) + coalesce(consultas_spc_7m,0) as consultas_7m,
	coalesce(consultas_serasa_8m,0) + coalesce(consultas_spc_8m,0) as consultas_8m,
	coalesce(consultas_serasa_9m,0) + coalesce(consultas_spc_9m,0) as consultas_9m,
	coalesce(consultas_serasa_10m,0) + coalesce(consultas_spc_10m,0) as consultas_10m,
	coalesce(consultas_serasa_11m,0) + coalesce(consultas_spc_11m,0) as consultas_11m
from lucas_leal.t_experian_choose_report t
full outer join(SELECT t2.direct_prospect_id,
    sum(
        CASE
            WHEN t2.serasa = ''::text THEN '0'::text
            ELSE COALESCE(t2.serasa, '0'::text)
        END::integer) FILTER (WHERE t2.mes = t2.data_base) AS consultas_serasa_atual,
    sum(
        CASE
            WHEN t2.serasa = ''::text THEN '0'::text
            ELSE COALESCE(t2.serasa, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '1 mon'::interval)) AS consultas_serasa_1m,
    sum(
        CASE
            WHEN t2.serasa = ''::text THEN '0'::text
            ELSE COALESCE(t2.serasa, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '2 mons'::interval)) AS consultas_serasa_2m,
    sum(
        CASE
            WHEN t2.serasa = ''::text THEN '0'::text
            ELSE COALESCE(t2.serasa, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '3 mons'::interval)) AS consultas_serasa_3m,
    sum(
        CASE
            WHEN t2.serasa = ''::text THEN '0'::text
            ELSE COALESCE(t2.serasa, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '4 mons'::interval)) AS consultas_serasa_4m,
    sum(
        CASE
            WHEN t2.serasa = ''::text THEN '0'::text
            ELSE COALESCE(t2.serasa, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '5 mons'::interval)) AS consultas_serasa_5m,
    sum(
        CASE
            WHEN t2.serasa = ''::text THEN '0'::text
            ELSE COALESCE(t2.serasa, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '6 mons'::interval)) AS consultas_serasa_6m,
    sum(
        CASE
            WHEN t2.serasa = ''::text THEN '0'::text
            ELSE COALESCE(t2.serasa, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '7 mons'::interval)) AS consultas_serasa_7m,
    sum(
        CASE
            WHEN t2.serasa = ''::text THEN '0'::text
            ELSE COALESCE(t2.serasa, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '8 mons'::interval)) AS consultas_serasa_8m,
    sum(
        CASE
            WHEN t2.serasa = ''::text THEN '0'::text
            ELSE COALESCE(t2.serasa, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '9 mons'::interval)) AS consultas_serasa_9m,
    sum(
        CASE
            WHEN t2.serasa = ''::text THEN '0'::text
            ELSE COALESCE(t2.serasa, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '10 mons'::interval)) AS consultas_serasa_10m,
    sum(
        CASE
            WHEN t2.serasa = ''::text THEN '0'::text
            ELSE COALESCE(t2.serasa, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '11 mons'::interval)) AS consultas_serasa_11m
   FROM ( SELECT t1.direct_prospect_id,
            to_date(to_char(to_date((cc.data -> 'datetime'::text) ->> 'data'::text, 'yyyymmdd'::text)::timestamp with time zone, 'Mon/yy'::text), 'Mon/yy'::text) AS data_base,
            to_date(jsonb_object_keys(cc.data -> 'consultas'::text), 'yy/mm'::text) AS mes,
            ((cc.data -> 'consultas'::text) -> jsonb_object_keys(cc.data -> 'consultas'::text)) ->> 'pesquisaBancos'::text AS serasa
           FROM credito_coleta cc
             JOIN t_experian_choose_report t1 ON (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying, 'RelatoBasico'::character varying]::text[])) AND "left"(t1.cnpj::text, 8) = "left"(cc.documento::text, 8) AND (to_timestamp((((cc.data -> 'datetime'::text) ->> 'data'::text) || ' '::text) || ((cc.data -> 'datetime'::text) ->> 'hora'::text), 'yyyymmdd HH24:MI:SS'::text) + '00:00:00.001'::interval * cc.id::double precision) = t1.max_date
             where t1.direct_prospect_id not in (select direct_prospect_id from t_experian_get_serasa_inquiries_data)) t2
  GROUP BY t2.direct_prospect_id) as serasa on serasa.direct_prospect_id = t.direct_prospect_id
full outer join (SELECT t2.direct_prospect_id,
    sum(
        CASE
            WHEN t2.spc = ''::text THEN '0'::text
            ELSE COALESCE(t2.spc, '0'::text)
        END::integer) FILTER (WHERE t2.mes = t2.data_base) AS consultas_spc_atual,
    sum(
        CASE
            WHEN t2.spc = ''::text THEN '0'::text
            ELSE COALESCE(t2.spc, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '1 mon'::interval)) AS consultas_spc_1m,
    sum(
        CASE
            WHEN t2.spc = ''::text THEN '0'::text
            ELSE COALESCE(t2.spc, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '2 mons'::interval)) AS consultas_spc_2m,
    sum(
        CASE
            WHEN t2.spc = ''::text THEN '0'::text
            ELSE COALESCE(t2.spc, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '3 mons'::interval)) AS consultas_spc_3m,
    sum(
        CASE
            WHEN t2.spc = ''::text THEN '0'::text
            ELSE COALESCE(t2.spc, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '4 mons'::interval)) AS consultas_spc_4m,
    sum(
        CASE
            WHEN t2.spc = ''::text THEN '0'::text
            ELSE COALESCE(t2.spc, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '5 mons'::interval)) AS consultas_spc_5m,
    sum(
        CASE
            WHEN t2.spc = ''::text THEN '0'::text
            ELSE COALESCE(t2.spc, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '6 mons'::interval)) AS consultas_spc_6m,
    sum(
        CASE
            WHEN t2.spc = ''::text THEN '0'::text
            ELSE COALESCE(t2.spc, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '7 mons'::interval)) AS consultas_spc_7m,
    sum(
        CASE
            WHEN t2.spc = ''::text THEN '0'::text
            ELSE COALESCE(t2.spc, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '8 mons'::interval)) AS consultas_spc_8m,
    sum(
        CASE
            WHEN t2.spc = ''::text THEN '0'::text
            ELSE COALESCE(t2.spc, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '9 mons'::interval)) AS consultas_spc_9m,
    sum(
        CASE
            WHEN t2.spc = ''::text THEN '0'::text
            ELSE COALESCE(t2.spc, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '10 mons'::interval)) AS consultas_spc_10m,
    sum(
        CASE
            WHEN t2.spc = ''::text THEN '0'::text
            ELSE COALESCE(t2.spc, '0'::text)
        END::integer) FILTER (WHERE t2.mes = (t2.data_base - '11 mons'::interval)) AS consultas_spc_11m
   FROM ( SELECT t1.direct_prospect_id,
            to_date(to_char(to_date((cc.data -> 'datetime'::text) ->> 'data'::text, 'yyyymmdd'::text)::timestamp with time zone, 'Mon/yy'::text), 'Mon/yy'::text) AS data_base,
            to_date(jsonb_object_keys(cc.data -> 'consultas'::text), 'yy/mm'::text) AS mes,
            replace(((cc.data -> 'registro_spc'::text) -> jsonb_object_keys(cc.data -> 'registro_spc'::text)) ->> 'quantidade'::text, ' '::text, ''::text) AS spc
           FROM credito_coleta cc
             JOIN t_experian_choose_report t1 ON (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying, 'RelatoBasico'::character varying]::text[])) AND "left"(t1.cnpj::text, 8) = "left"(cc.documento::text, 8) AND (to_timestamp((((cc.data -> 'datetime'::text) ->> 'data'::text) || ' '::text) || ((cc.data -> 'datetime'::text) ->> 'hora'::text), 'yyyymmdd HH24:MI:SS'::text) + '00:00:00.001'::interval * cc.id::double precision) = t1.max_date
             where t1.direct_prospect_id not in (select direct_prospect_id from t_experian_get_serasa_inquiries_data)) t2 
  GROUP BY t2.direct_prospect_id
UNION
 SELECT 
 	serasas_antigos_parseados.lead_id AS direct_prospect_id,
    serasas_antigos_parseados.experian_inquiries_curr AS consultas_spc_atual,
    serasas_antigos_parseados.experian_inquiries_2m AS consultas_spc_1m,
    serasas_antigos_parseados.experian_inquiries_3m AS consultas_spc_2m,
    serasas_antigos_parseados.experian_inquiries_4m AS consultas_spc_3m,
    serasas_antigos_parseados.experian_inquiries_5m AS consultas_spc_4m,
    serasas_antigos_parseados.experian_inquiries_6m AS consultas_spc_5m,
    serasas_antigos_parseados.experian_inquiries_7m AS consultas_spc_6m,
    serasas_antigos_parseados.experian_inquiries_8m AS consultas_spc_7m,
    serasas_antigos_parseados.experian_inquiries_9m AS consultas_spc_8m,
    serasas_antigos_parseados.experian_inquiries_10m AS consultas_spc_9m,
    serasas_antigos_parseados.experian_inquiries_11m AS consultas_spc_10m,
    serasas_antigos_parseados.experian_inquiries_12m AS consultas_spc_11m
   FROM serasas_antigos_parseados
   where serasas_antigos_parseados.lead_id not in (select direct_prospect_id from t_experian_get_serasa_inquiries_data)
   ) as spc on spc.direct_prospect_id = t.direct_prospect_id
where t.direct_prospect_id not in (select direct_prospect_id from t_experian_get_serasa_inquiries_data)
)

