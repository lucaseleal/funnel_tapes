--CASOS EM QUE CLIENTE ATRASA MUITO RENEGOCIA E DPS ADIA (EX: 1270, 4498 ...)
--VENCIMENTOS AINDA CAEM EM FERIADOS NACIONAIS

select
*
from t_collection_aggressive
where dia = '2020-11-30'::date 


--t_collection_aggressive
insert into lucas_leal.t_collection_aggressive
(select
	loan_request_id,
	dia,
	atraso,
	atraso_maximo
from(select
		dia,
		loan_request_id,
		vencimento_em,
		dt_pgto_subst,
		case when dt_pgto_subst < vencimento_em then dt_pgto_subst - vencimento_em else dia - vencimento_em end as atraso,
		max(case when dt_pgto_subst < vencimento_em then dt_pgto_subst - vencimento_em else dia - vencimento_em end) over (partition by loan_request_id order by dia) as atraso_maximo
	from(select
			w.dia,
			lr.loan_request_id,
			min(case extract('isodow' from vencimento_em) when 6 then 2 when 7 then 1 else 0 end + vencimento_em) as vencimento_em,
			min(dt_pgto_subst) as dt_pgto_subst
		from public.workdays w
		join (select
				lr.loan_request_id,
				max(lr.loan_date) as dt_inicio,
				max(fp.pago_em) filter (where fpp.status = 'Pago') as dt_quitacao
			from public.loan_requests lr
			join public.financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
			join public.financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as lr on w.dia between lr.dt_inicio and coalesce(dt_quitacao,current_date)
		left join(select
				vcto.emprestimo_id,
				vcto.vencimento_em,
				pgto.dt_pgto_subst
			from(select
					emprestimo_id,
					"type",
					row_number() over (partition by emprestimo_id order by vencimento_em) as indice_vcto,
					case replicado when 1 then vencimento_em + '1 month'::interval * (row_number() over (partition by emprestimo_id order by vencimento_em) - indice_vcto) else vencimento_em end::date
				from(select *, unnest(string_to_array(new_list,'/'))
					from(select
							fpp.emprestimo_id,
							fpp."type",
							fp.vencimento_em,
							row_number() over (partition by fpp.emprestimo_id order by fp.vencimento_em) as indice_vcto,
							t1.count_parcelas,
							regexp_replace(case when t1.count_parcelas - fp.indice_parcelas = t1.count_parcelas - t1.parcelas_qtd then repeat(vencimento_em::text || '/',(t1.count_parcelas - t1.parcelas_qtd + 1)::int) else vencimento_em::text end,'\/$','') as new_list,
							case when t1.count_parcelas - fp.indice_parcelas = t1.count_parcelas - t1.parcelas_qtd then 1 else 0 end as replicado
						from public.financeiro_planopagamento as fpp
						join (select *, row_number() over (partition by plano_id order by vencimento_em) as indice_parcelas from public.financeiro_parcela) as fp on fp.plano_id = fpp.id
						join (select
								fpp.emprestimo_id,
								fpp."type",
								fpp.id,
								lead(count(*) filter (where pgto.pago_em < fpp.creation_date::date) + 1) over (partition by fpp.emprestimo_id order by fpp.id) as count_parcelas,
								fpp.parcelas_qtd,
								coalesce(fpp.postponing_type,fpp.renegotiation_type) as sub_type
							from public.financeiro_planopagamento as fpp
							left join (select
									fpp.emprestimo_id,
									fp.pago_em
								from public.financeiro_planopagamento as fpp
								join public.financeiro_parcela as fp on fp.plano_id = fpp.id
								where fp.pago_em is not null) as pgto on pgto.emprestimo_id = fpp.emprestimo_id
							where fpp.status not in ('Cancelado','EmEspera') and fpp."type" in ('ORIGINAL','POSTPONING')
							group by 1,2,3) as t1 on t1.id = fpp.id and fp.indice_parcelas <= coalesce(t1.count_parcelas,1000)) as t2)as t3) as vcto 
			left join(select
					emprestimo_id,
					dt_pgto_subst,
					row_number() over (partition by emprestimo_id order by dt_pgto_subst) as indice_pgto				
				from(select
							fpp.emprestimo_id,
							fp.pago_em as dt_pgto_subst
						from public.financeiro_planopagamento fpp
						join public.financeiro_parcela fp on fp.plano_id = fpp.id
						where fp.pago_em is not null
					union all
						select
							emprestimo_id,
							creation_date::date as dt_pgto_subst
						from public.financeiro_planopagamento
						WHERE status not in ('Cancelado','EmEspera') and "type" = 'POSTPONING') as t1) as pgto on vcto.emprestimo_id = pgto.emprestimo_id and vcto.indice_vcto = pgto.indice_pgto) as calc_atraso on 
				calc_atraso.emprestimo_id = lr.loan_request_id and 
					case when coalesce(calc_atraso.dt_pgto_subst,current_date) > calc_atraso.vencimento_em then w.dia between calc_atraso.vencimento_em and coalesce(calc_atraso.dt_pgto_subst,current_date)
						else w.dia between calc_atraso.dt_pgto_subst and calc_atraso.vencimento_em end
		group by 1,2) as t1) as t2
where dia = '2020-11-30'::date) --> (select max(dia) from lucas_leal.t_collection_aggressive))

--t_collection_photo_dates
--insert into t_collection_photo_dates
create view v_collection_photo_dates as
(select 
	fpp.emprestimo_id,
	max(fp.vencimento_em) filter (where fp.numero = floor(fpp.parcelas_qtd::numeric / 2)) as pmt_perc_50,
	coalesce(max(fp.vencimento_em) filter (where fp.numero = 1), max(lr.loan_date) + '1 month'::interval)::date as pmt_1,
	coalesce(max(fp.vencimento_em) filter (where fp.numero = 2), max(lr.loan_date) + '2 month'::interval)::date as pmt_2,
	coalesce(max(fp.vencimento_em) filter (where fp.numero = 3), max(lr.loan_date) + '3 month'::interval)::date as pmt_3,
	coalesce(max(fp.vencimento_em) filter (where fp.numero = 4), max(lr.loan_date) + '4 month'::interval)::date as pmt_4,
	coalesce(max(fp.vencimento_em) filter (where fp.numero = 5), max(lr.loan_date) + '5 month'::interval)::date as pmt_5,
	coalesce(max(fp.vencimento_em) filter (where fp.numero = 6), max(lr.loan_date) + '6 month'::interval)::date as pmt_6,
	coalesce(max(fp.vencimento_em) filter (where fp.numero = 7), max(lr.loan_date) + '7 month'::interval)::date as pmt_7,
	coalesce(max(fp.vencimento_em) filter (where fp.numero = 8), max(lr.loan_date) + '8 month'::interval)::date as pmt_8,
	coalesce(max(fp.vencimento_em) filter (where fp.numero = 9), max(lr.loan_date) + '9 month'::interval)::date as pmt_9,
	coalesce(max(fp.vencimento_em) filter (where fp.numero = 10), max(lr.loan_date) + '10 month'::interval)::date as pmt_10,
	coalesce(max(fp.vencimento_em) filter (where fp.numero = 11), max(lr.loan_date) + '11 month'::interval)::date as pmt_11,
	coalesce(max(fp.vencimento_em) filter (where fp.numero = 12), max(lr.loan_date) + '12 month'::interval)::date as pmt_12
from public.financeiro_planopagamento fpp 
join public.financeiro_parcela fp on fp.plano_id = fpp.id
join public.loan_requests lr on lr.loan_request_id = fpp.emprestimo_id
where fpp."type" = 'ORIGINAL' --and fpp.emprestimo_id not in (select emprestimo_id from t_collection_photo_dates)
group by 1)


