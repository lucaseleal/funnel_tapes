

select distinct cnpj ,'(''' || cnpj || '''),'
from public.v_bankaccounts_data vbd 
where status = 'ACTIVE'

-- Base de queries para consulta:
with 
calendario as -- Query com todos os dias analisados
    (select * from 
    generate_series('October 5, 2020, 23:59',current_date,INTERVAL'1 day') dia
    ),
contas as -- Query com todas as contas que foram ativadas
    (select 
    mvsb.id as contaid,
    mvsb.started as conta_aberta,
    mvsb.state,
    mvsb.legal_nature,
    mvsb.founded_on
    from 
    metabase.mv_simple_bank_accounts as mvsb
    where status = 'ACTIVE'
    ),
transacoes as -- Query com todos os cash flows que ocorreram
    (select
    vst.id as id_flow,
    vst."ownerAccountId" as contaid,
    vst."executedAt" as dt_flow,
    1 as flow
    from
    public.v_sync_transactions as vst
    where vst.status = 'EXECUTED'
    ),    
calendario_contas as -- Query que retorna todos os dias desde 5 de outubro de 2020, sendo uma linha para cada conta ativa hoje que j� estava criada nesse dia
    (select * from 
    calendario as cal
    INNER JOIN LATERAL 
        (select * from contas as con2
        where con2.conta_aberta<cal.dia 
        ) as con on true
    )
select *,
case
    when
        (select count(*) from transacoes as t
        where t.contaid=cc.contaid
        and t.dt_flow<cc.dia
        and t.dt_flow>(cc.dia-INTERVAL'30 day')
        ) > 0 then 'ATIVA'
    else 
        'INATIVA'
end
from
calendario_contas as cc
limit 1000


select 
    mvsb.id as contaid,
    mvsb.started as conta_aberta,
    mvsb.state,
    mvsb.legal_nature,
    mvsb.founded_on
from metabase.mv_simple_bank_accounts as mvsb
where status = 'ACTIVE'
limit 100


select
    vst.id as id_flow,
    vst."ownerAccountId" as contaid,
    vst."executedAt" as dt_flow,
    1 as flow
from public.v_sync_transactions as vst
where vst.status = 'EXECUTED'
limit 100


