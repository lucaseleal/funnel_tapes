create index t_lead_tape_credit_underwrite_data_full_lead_id on lucas_leal.t_lead_tape_credit_underwrite_data_full using btree (legacy_target_id);
create index t_lead_tape_credit_underwrite_data_full_parecer_id on lucas_leal.t_lead_tape_credit_underwrite_data_full using btree (parecer_id);
create index t_lead_tape_credit_underwrite_data_full_analise_id on lucas_leal.t_lead_tape_credit_underwrite_data_full using btree (analise_id);
create unique index t_lead_tape_credit_underwrite_data_full_primary_key on lucas_leal.t_lead_tape_credit_underwrite_data_full using btree (primary_key);
--t_get_credit_underwrite_data_full
insert into lucas_leal.t_lead_tape_credit_underwrite_data_full 

create materialized view lucas_leal.mv_credit_underwrite_data_full as
select 
	sc.legacy_target_id,
	rank() over (partition by sc.legacy_target_id order by sc.id desc) as indice_analise,
	parecer ->> 'Id' as parecer_id,
	parecer ->> 'Status' as parecer_status,
	parecer ->> 'Comments' as parecer_comentario,
	coalesce(parecer ->> 'Username',case when (parecer ->> 'IsAutomatic')::bool then 'Bizbot' end) as parecer_analista,
	(parecer ->> 'NewAmount')::numeric as parecer_limite,
	(parecer ->> 'NewTenure')::int as parecer_prazo,
	(parecer ->> 'NewInterestRate')::numeric as parecer_taxa,
	(parecer -> 'LastChange' ->> 'Date')::timestamp as parecer_timestamp,
	(parecer ->> 'TrustLevel')::int as parecer_nota,
	(parecer ->> 'IsAutomatic')::bool as parecer_automatico,
	parecer ->> 'AuthorityLevel' as parecer_alcada,
	parecer ->> 'CreditOpinionSuggestionType' as parecer_decisao,
	sc.id as analise_id,
	coalesce(sc.legacy_target_id::text,'') || '-' || coalesce(sc.id::text,'') || '-' || coalesce(parecer ->> 'Id','') as primary_key
from sync_credit_analysis as sc
join jsonb_array_elements(sc.data_output->'CreditOpinions') as parecer on true
--left join lucas_leal.t_lead_tape_credit_underwrite_data_full t2 on t2.primary_key = coalesce(sc.legacy_target_id::text,'') || '-' || coalesce(sc.id::text,'') || '-' || coalesce(parecer ->> 'Id','')
where sc.legacy_target_id is not null --and coalesce(t2.parecer_timestamp,'2999-12-01 12:00:00') != (parecer -> 'LastChange' ->> 'Date')::timestamp


--t_get_credit_underwrite_indicators_full
refresh materialized view lucas_leal.mv_credit_underwrite_indicators_full with data;
create materialized view lucas_leal.mv_credit_underwrite_indicators_full as
((SELECT cp.emprestimo_id,
		count(*) FILTER (WHERE cp.criado_por_id = 67) AS bizbot,
	    count(*) FILTER (WHERE cp.criado_por_id <> 67) AS nao_bizbot,
	    min(cp.criado_em) FILTER (WHERE cp.indice = 1)::date AS data_a1,
	    max(cp.criado_por_nome) FILTER (WHERE cp.indice = 1) AS analista_a1,
	    max(cp.status::text) FILTER (WHERE cp.indice = 1) AS status_a1,
	    max(cp.confianca) FILTER (WHERE cp.indice = 1) AS nota_a1,
	    max(cp.limite) FILTER (WHERE cp.indice = 1) AS valor_a1,
	    max(cp.juros) FILTER (WHERE cp.indice = 1)::numeric(10,2) AS taxa_a1,
	    max(cp.prazo) FILTER (WHERE cp.indice = 1)::numeric(10,2) AS prazo_a1,
	    min(cp.criado_em) FILTER (WHERE cp.indice = 2)::date AS data_a2,
	    max(cp.criado_por_nome) FILTER (WHERE cp.indice = 2) AS analista_a2,
	    max(cp.status::text) FILTER (WHERE cp.indice = 2) AS status_a2,
	    max(cp.confianca) FILTER (WHERE cp.indice = 2) AS nota_a2,
	    max(cp.limite) FILTER (WHERE cp.indice = 2) AS valor_a2,
	    max(cp.juros) FILTER (WHERE cp.indice = 2)::numeric(10,2) AS taxa_a2,
	    max(cp.prazo) FILTER (WHERE cp.indice = 2)::numeric(10,2) AS prazo_a2,
	    min(cp.criado_em) FILTER (WHERE cp.indice = 3)::date AS data_a3,
	    max(cp.criado_por_nome) FILTER (WHERE cp.indice = 3) AS analista_a3,
	    max(cp.status::text) FILTER (WHERE cp.indice = 3) AS status_a3,
	    max(cp.confianca) FILTER (WHERE cp.indice = 3) AS nota_a3,
	    max(cp.limite) FILTER (WHERE cp.indice = 3) AS valor_a3,
	    max(cp.juros) FILTER (WHERE cp.indice = 3)::numeric(10,2) AS taxa_a3,
	    max(cp.prazo) FILTER (WHERE cp.indice = 3)::numeric(10,2) AS prazo_a3
	FROM ( SELECT cp.id,
			cp.confianca,
            cp.status,
            cp.tipo,
            cp.texto,
            cp.juros,
            cp.prazo,
            cp.criado_em,
            cp.criado_por as criado_por_id,
            regexp_replace(au.first_name::text || '_'::text || au.last_name::text,'\_$','') as criado_por_nome,
            cp.modificado_em,
            cp.modificado_por,
            cp.emprestimo_id,
            cp.limite,
            dense_rank() OVER (PARTITION BY cp.emprestimo_id ORDER BY (to_timestamp(to_char(cp.criado_em, 'yyyy-mm-dd HH24:MI:SS'::text), 'yyyy-mm-dd HH24:MI:SS'::text)), cp.tipo) AS indice
		FROM public.credito_parecer cp
		join lucas_leal.v_funil_completo_sem_duplicacao vfcsd on vfcsd.loan_request_id = cp.emprestimo_id 
		LEFT JOIN public.sync_credit_analysis sc ON sc.legacy_target_id = vfcsd.direct_prospect_id
		LEFT JOIN public_new.auth_user au ON au.id = cp.criado_por
--		left join lucas_leal.t_lead_tape_credit_underwrite_indicators_full t1 on t1.emprestimo_id = cp.emprestimo_id
		where sc.legacy_target_id IS null
--			and t1.emprestimo_id is null
		) cp
 	group by cp.emprestimo_id)
union all
	(select
		vfcsd.loan_request_id as emprestimo_id,
		count(*) filter (where parecer_automatico) as bizbot,
		count(*) filter (where not coalesce(parecer_automatico,false)) as nao_bizbot,
		max(t1.parecer_timestamp) filter (where t1.parecer_alcada = 'FIRST_AUTHORITY') as data_a1,
		max(t1.parecer_analista) filter (where t1.parecer_alcada = 'FIRST_AUTHORITY') as analista_a1,
		max(replace(replace(replace(t1.parecer_decisao, 'APPROVE_WITH_CHANGES','AprovarAlteracao'),'APPROVE','Aprovar'),'REPROVE','Rejeitar')) filter (where t1.parecer_alcada = 'FIRST_AUTHORITY') as status_a1,
		max(t1.parecer_nota) filter (where t1.parecer_alcada = 'FIRST_AUTHORITY') as nota_a1,
		max(t1.parecer_limite) filter (where t1.parecer_alcada = 'FIRST_AUTHORITY') as limite_a1,
		max(t1.parecer_taxa) filter (where t1.parecer_alcada = 'FIRST_AUTHORITY') as taxa_a1,
		max(t1.parecer_prazo) filter (where t1.parecer_alcada = 'FIRST_AUTHORITY') as prazo_a1,
		max(t1.parecer_timestamp) filter (where t1.parecer_alcada = 'SECOND_AUTHORITY') as data_a2,
		max(t1.parecer_analista) filter (where t1.parecer_alcada = 'SECOND_AUTHORITY') as analista_a2,
		max(replace(replace(replace(t1.parecer_decisao, 'APPROVE_WITH_CHANGES','AprovarAlteracao'),'APPROVE','Aprovar'),'REPROVE','Rejeitar')) filter (where t1.parecer_alcada = 'SECOND_AUTHORITY') as status_a2,
		max(t1.parecer_nota) filter (where t1.parecer_alcada = 'SECOND_AUTHORITY') as nota_a2,
		max(t1.parecer_limite) filter (where t1.parecer_alcada = 'SECOND_AUTHORITY') as limite_a2,
		max(t1.parecer_taxa) filter (where t1.parecer_alcada = 'SECOND_AUTHORITY') as taxa_a2,
		max(t1.parecer_prazo) filter (where t1.parecer_alcada = 'SECOND_AUTHORITY') as prazo_a2,
		max(t1.parecer_timestamp) filter (where t1.parecer_alcada = 'THIRD_AUTHORITY') as data_a3,
		max(t1.parecer_analista) filter (where t1.parecer_alcada = 'THIRD_AUTHORITY') as analista_a3,
		max(replace(replace(replace(t1.parecer_decisao, 'APPROVE_WITH_CHANGES','AprovarAlteracao'),'APPROVE','Aprovar'),'REPROVE','Rejeitar')) filter (where t1.parecer_alcada = 'THIRD_AUTHORITY') as status_a3,
		max(t1.parecer_nota) filter (where t1.parecer_alcada = 'THIRD_AUTHORITY') as nota_a3,
		max(t1.parecer_limite) filter (where t1.parecer_alcada = 'THIRD_AUTHORITY') as limite_a3,
		max(t1.parecer_taxa) filter (where t1.parecer_alcada = 'THIRD_AUTHORITY') as taxa_a3,
		max(t1.parecer_prazo) filter (where t1.parecer_alcada = 'THIRD_AUTHORITY') as prazo_a3
	from lucas_leal.mv_credit_underwrite_data_full as t1
	join lucas_leal.v_funil_completo_sem_duplicacao vfcsd on vfcsd.direct_prospect_id = t1.legacy_target_id
--	left join lucas_leal.t_lead_tape_credit_underwrite_indicators_full as t2 on t2.emprestimo_id = vfcsd.loan_request_id
	where t1.indice_analise = 1
--		and t2.emprestimo_id is null
	group by 1
	)
)

	
refresh materialized view lucas_leal.mv_credit_underwrite_selected_motives_full with data;

--insert into lucas_leal.t_lead_tape_credit_underwrite_selected_motives_full
create materialized view lucas_leal.mv_credit_underwrite_selected_motives_full_v2 as
	select distinct 
		sc.legacy_target_id,
		motivos_rejeicao ->> 'Title' as motivo_rejeicao,
		motivo_rejeicao_group(motivos_rejeicao ->> 'Title') as motivo_rejeicao_group
	from sync_credit_analysis as sc
	left join jsonb_array_elements(sc.data_output->'CreditOpinions') as parecer on true
	left join jsonb_array_elements(parecer -> 'SelectedMotives') as motivos_rejeicao on true
union
	select distinct 
		vfcsd.direct_prospect_id as legacy_target_id,
		replace(replace(mr.nome,'Pol�tica de Cr�dito (Score Serasa Baixo na an�lise de Documenta��o )','Pol�tica de Cr�dito (Score Serasa Baixo na an�lise de Documenta��o)'),
			'Pol�tica de Cr�dito (Quadro Societ�rio)','Pol�tica de Cr�dito (Altera��o no Contrato Social)') as motivo_rejeicao,
		motivo_rejeicao_group(replace(replace(mr.nome,'Pol�tica de Cr�dito (Score Serasa Baixo na an�lise de Documenta��o )','Pol�tica de Cr�dito (Score Serasa Baixo na an�lise de Documenta��o)'),
			'Pol�tica de Cr�dito (Quadro Societ�rio)','Pol�tica de Cr�dito (Altera��o no Contrato Social)')) as motivo_rejeicao_group
	from public.loan_requests lr
	join lucas_leal.v_funil_completo_sem_duplicacao vfcsd on vfcsd.loan_request_id = lr.loan_request_id 
	join public.loan_requests_motivos_rejeicao lrm ON lrm.loanrequest_id = lr.loan_request_id
	join public.motivos_rejeicao mr ON mr.id = lrm.motivosrejeicao_id

	
select motivos_rejeicao ->> 'Title'
from sync_credit_analysis as sc
left join jsonb_array_elements(sc.data_output->'CreditOpinions') as parecer on true
left join jsonb_array_elements(parecer -> 'SelectedMotives') as motivos_rejeicao on true
where legacy_target_id = 1523099
	
select *
from lucas_leal.mv_credit_underwrite_selected_motives_full_v2
where legacy_target_id = 1523099

create unique index mv_lead_tape_credit_worthiness_full_analise_id on lucas_leal.mv_lead_tape_credit_worthiness_full using btree(analise_id);
create unique index mv_lead_tape_credit_worthiness_full_lead_id on lucas_leal.mv_lead_tape_credit_worthiness_full using btree(lead_id);
refresh materialized view lucas_leal.mv_lead_tape_credit_worthiness_full;
create materialized view lucas_leal.mv_lead_tape_credit_worthiness_full as 
select *
from(
select 
	scw.id as analise_id,
	coalesce(scw.legacy_target_id,dp.direct_prospect_id) as lead_id,
	(scw.data_output->'Bizu'->>'BizuScore')::numeric as bizu_score,
	scw.data_output->'Bizu'->>'BizuVersion' as bizu_version,
	scw.data_output->'Bizu'->>'BizuIndicator' as bizu_class,
	(scw.data_output->>'IsMEI')::bool as is_mei,
	scw.data_output->>'Sector' as sector,
	(scw.data_output->'Serasa'->>'SerasaScore')::numeric::int as serasa_score,
	scw.data_output->'Serasa'->>'SerasaVersion' as serasa_version,
	(scw.data_output->'LowTicket'->>'LowTicketScore')::numeric as low_ticket_score,
	scw.data_output->>'CreditZone' as old_mesa_score_zone,
	scw.data_output->>'LegalNature' as natureza_juridica,
	scw.data_output->>'LegalNatureCategory' as natureza_juridica_categoria,
	scw.data_output->>'Probability' as old_mesa_score_prob,
	(scw.data_output->>'ShareCapital')::numeric as capital_social,
	replace(scw.data_output->>'ProcessingEnd','T',' ')::timestamp as analise_termino_timestamp,
	replace(scw.data_output->>'ProcessingStart','T',' ')::timestamp as analise_inicio_timestamp,
	scw.data_output->>'LoanApplicationId' as loan_application_id,
	scw.data_output->'StatementScore'->>'Model' as extrato_score_modelo,
	scw.data_output->'StatementScore'->>'Indicator' as extrato_score_classe,
	(scw.data_output->'StatementScore'->>'Probability')::numeric as extrato_score_prob,
	(scw.data_output->'ChecklistScore'->>'Score')::numeric as mesa_score,
	(scw.data_output->'ChecklistScore'->>'Probability')::numeric as mesa_score_prob,
	(scw.data_output->'Bizu3'->>'BizuScore')::numeric as bizu3_score,
	(scw.data_output->'Bizu3'->>'Probability')::numeric as bizu3_score_prob,
	scw.data_output->'Bizu3'->>'BizuIndicator' as bizu3_score_classe,
	scw.data_output->>'Cnae' as cnae,
	row_number() over (partition by scw.legacy_target_id order by scw.id desc) as indice_analise
from public.sync_credit_worthiness scw 
left join public.direct_prospects dp on dp.loan_application_id = scw.data_output->>'LoanApplicationId'
where coalesce(scw.legacy_target_id,dp.direct_prospect_id) is not null) as t1
where indice_analise = 1

insert into lucas_leal.t_lead_tape_credit_worthiness_full 
select 
	scw.id as analise_id,
	coalesce(scw.legacy_target_id,dp.direct_prospect_id) as lead_id,
	(scw.data_output->'Bizu'->>'BizuScore')::numeric as bizu_score,
	scw.data_output->'Bizu'->>'BizuVersion' as bizu_version,
	scw.data_output->'Bizu'->>'BizuIndicator' as bizu_class,
	(scw.data_output->>'IsMEI')::bool as is_mei,
	scw.data_output->>'Sector' as sector,
	(scw.data_output->'Serasa'->>'SerasaScore')::numeric::int as serasa_score,
	scw.data_output->'Serasa'->>'SerasaVersion' as serasa_version,
	(scw.data_output->'LowTicket'->>'LowTicketScore')::numeric as low_ticket_score,
	scw.data_output->>'CreditZone' as old_mesa_score_zone,
	scw.data_output->>'LegalNature' as natureza_juridica,
	scw.data_output->>'LegalNatureCategory' as natureza_juridica_categoria,
	scw.data_output->>'Probability' as old_mesa_score_prob,
	(scw.data_output->>'ShareCapital')::numeric as capital_social,
	replace(scw.data_output->>'ProcessingEnd','T',' ')::timestamp as analise_termino_timestamp,
	replace(scw.data_output->>'ProcessingStart','T',' ')::timestamp as analise_inicio_timestamp,
	scw.data_output->>'LoanApplicationId' as loan_application_id,
	scw.data_output->'StatementScore'->>'Model' as extrato_score_modelo,
	scw.data_output->'StatementScore'->>'Indicator' as extrato_score_classe,
	(scw.data_output->'StatementScore'->>'Probability')::numeric as extrato_score_prob,
	(scw.data_output->'ChecklistScore'->>'Score')::numeric as mesa_score,
	(scw.data_output->'ChecklistScore'->>'Probability')::numeric as mesa_score_prob,
	(scw.data_output->'Bizu3'->>'BizuScore')::numeric as bizu3_score,
	(scw.data_output->'Bizu3'->>'Probability')::numeric as bizu3_score_prob,
	scw.data_output->'Bizu3'->>'BizuIndicator' as bizu3_score_classe,
	scw.data_output->>'Cnae' as cnae,
	row_number() over (partition by scw.legacy_target_id order by scw.id desc) as indice_analise
from public.sync_credit_worthiness scw 
left join public.direct_prospects dp on dp.loan_application_id = scw.data_output->>'LoanApplicationId'
left join lucas_leal.t_lead_tape_credit_worthiness_full t2 on t2.lead_id = coalesce(scw.legacy_target_id,dp.direct_prospect_id)
where coalesce(scw.legacy_target_id,dp.direct_prospect_id) is not null
	and t2.lead_id is null
--	and scw.legacy_target_id = 1222656


refresh materialized view lucas_leal.mv_credit_checklist_full with data;
create materialized view lucas_leal.mv_credit_checklist_full as
SELECT t1.lead_id,
    t1.data_criacao,
    t1.pergunta,
    t1.parte,
    t1.ordem,
    t1.tiporesposta,
    string_agg(t1.resposta, ','::text) as resposta
   FROM ( SELECT ca.legacy_target_id AS lead_id,
            (ca.data_output ->> 'CreationDate'::text)::date AS data_criacao,
            checklist.value ->> 'QuestionnaireType'::text AS parte,
            checklist.value ->> 'Asking'::text AS pergunta,
            checklist.value ->> 'Order'::text AS ordem,
                CASE
                    WHEN (answer.value ->> 'TypeOfAnswer'::text) = 'DISCURSIVE'::text THEN answer.value ->> 'DiscursiveReply'::text
                    ELSE answer.value ->> 'AnswerText'::text
                END AS resposta,
            answer.value ->> 'TypeOfAnswer'::text AS tiporesposta
           FROM ( SELECT sync_credit_analysis.legacy_target_id,
                    sync_credit_analysis.data_output,
                    row_number() OVER (PARTITION BY sync_credit_analysis.legacy_target_id ORDER BY sync_credit_analysis.id DESC) AS indice_analise
                   FROM sync_credit_analysis) ca,
            direct_prospects dp,
            LATERAL jsonb_array_elements(ca.data_output -> 'CreditChecklist'::text) checklist(value),
            LATERAL jsonb_array_elements(checklist.value -> 'Answers'::text) answer(value)
          WHERE ca.legacy_target_id = dp.direct_prospect_id AND ca.indice_analise = 1 AND ((answer.value ->> 'BooleanReply'::text) = 'true'::text OR (answer.value ->> 'DiscursiveReply'::text) IS NOT NULL AND (answer.value ->> 'DiscursiveReply'::text) <> ''::text)
          ) t1
  GROUP BY 1,2,3,4,5,6


-- A FAZER
	data_output->'ClearSale'->>'id' as clearsale_id,
	data_output->'ClearSale'->>'Email' as clearsale_email,
	data_output->'ClearSale'->>'Phone' as clearsale_phone,
	data_output->'ClearSale'->'Address'->>'ZipCode' as clearsale_endereco_cep,
	replace(data_output->'ClearSale'->'Results'->'Score'->>'Date','T',' ')::timestamp as clearsale_score_date,
	(data_output->'ClearSale'->'Results'->'Score'->>'Value')::numeric as clearsale_score_value,
	data_output->'ClearSale'->'Results'->'Score'->>'Reason' as clearsale_score_reason,
	
SELECT t.direct_prospect_id,
    t.opt_in_date,
    t.bizu_combo_zone_number,
    ((t.teste -> 'Results'::text) ->> 'OverallScore'::text)::numeric AS psycho_score,
    t.teste ->> 'Status'::text AS status,
    t.teste,
    t.data_output -> 'WorthyCredits'::text AS worthy_from_data_output,
    c.wd,
    c.dia_util,
    c.ano_mes,
    c.c_week,
    c.biz_month_w,
    c.ano,
    c.mes,
    c.dia,
    c.quarter_ano,
    c.dt,
    t.data_output,
    (t.teste -> 'Results'::text) ->> 'CandidateComplete'::text AS completou
   FROM ( SELECT dp.direct_prospect_id,
                CASE
                    WHEN char_length(((saf.data_output -> 'WorthyCredits'::text)::character varying)::text) > 2 THEN "substring"(((saf.data_output -> 'WorthyCredits'::text)::character varying)::text, 2, char_length(((saf.data_output -> 'WorthyCredits'::text)::character varying)::text) - 2)::json
                    ELSE NULL::json
                END AS teste
           FROM direct_prospects dp
             JOIN sync_anti_fraud saf ON ((saf.data_output -> 'LoanApplicationRef'::text) ->> 'Id'::text) = dp.loan_application_id::text
             JOIN ( SELECT (saf_1.data_output -> 'LoanApplicationRef'::text) ->> 'Id'::text AS id,
                    max(saf_1.insert_date) AS dt_max
                   FROM sync_anti_fraud saf_1
                  GROUP BY ((saf_1.data_output -> 'LoanApplicationRef'::text) ->> 'Id'::text)) t_filtro ON ((saf.data_output -> 'LoanApplicationRef'::text) ->> 'Id'::text) = t_filtro.id AND t_filtro.dt_max = saf.insert_date
          WHERE dp.bizu_combo_zone_number = 9 AND (saf.data_output -> 'WorthyCredits'::text) IS NOT NULL AND (dp.direct_prospect_id <> ALL (ARRAY[1477234, 1471926, 1482578]))) t
     LEFT JOIN pedro_malan.calendar c ON c.dt = t.opt_in_date::date
  ORDER BY t.opt_in_date DESC	
  
select 
	*
from public.sync_anti_fraud
limit 10
