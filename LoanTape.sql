select *
from data_lake.output.loan_tape_lucas_20181008

drop table sandbox."loan_tape_v2.2.2_20181029"

	
select *
from lucas_leal."loan_tape_v2.2.2_20181127"

grant all privileges on table lucas_leal."loan_tape_v2.2.2_20190307" to "giulia.killer"

create table sandbox.populacao as select * from populacao

drop table lucas_leal."loan_tape_v2.2.2_20181127"

alter table lucas_leal."loan_tape_v2.2.2_2018118" rename to "loan_tape_v2.2.2_20181218";

CREATE TABLE lucas_leal."loan_tape_v2.2.2_20190307" AS
--BIZU 2.0
-- Versão sql
--------------------------LEAD
select dp.direct_prospect_id as lead_ID,
dp.opt_in_date as lead_opt_in_date,
extract(hour from dp.opt_in_date)::int as lead_opt_in_hour,
extract(day from dp.opt_in_date)::int as lead_opt_in_day,
extract(month from dp.opt_in_date)::int as lead_opt_in_month,
extract(year from dp.opt_in_date)::int as lead_opt_in_year,
case when amount_requested < 1000 then o.max_value else amount_requested end/month_revenue/12 as lead_incremental_debt_leverage,
o.max_value/case when amount_requested < 1000 then o.max_value else amount_requested end as lead_offer_over_application_value,
case when lr.valor_solicitado is not null then lr.valor_solicitado/o.max_value::float when requested_lessthan_offered = 1 then 1 else lr.value/o.max_value::float end as lead_loan_over_offer_value,
lr.number_of_installments/o.max_number_of_installments::double precision as lead_loan_over_offer_term,
case when case when lr.valor_solicitado is not null then lr.valor_solicitado/o.max_value::float when requested_lessthan_offered = 1 then 1 else lr.value/o.max_value::float end = lr.number_of_installments/o.max_number_of_installments::float then 1 else (@li.pmt) / ((o.interest_rate/100+0.009)/(1-(1+o.interest_rate/100+0.009)^(-o.max_number_of_installments)) * o.max_value) end as lead_loan_over_offer_pmt,
case when o.date_inserted<dp.opt_in_date then 0 else extract(day from o.date_inserted - dp.opt_in_date)*24 + extract(hour from o.date_inserted - dp.opt_in_date) + extract(minute from o.date_inserted - dp.opt_in_date)/60 end as lead_app_to_offer_time,
case when lr.date_inserted < o.date_inserted then 0 else extract(day from lr.date_inserted - o.date_inserted)*24 + extract(hour from lr.date_inserted - o.date_inserted) + extract(minute from lr.date_inserted - o.date_inserted)/60 end as lead_offer_to_request_time,
case when lr.loan_date < lr.date_inserted then 0 else extract(day from lr.loan_date - lr.date_inserted)*24 + extract(hour from lr.loan_date - lr.date_inserted) + extract(minute from lr.loan_date - lr.date_inserted)/60 end as lead_request_to_loan_time,
case when lr.loan_date < dp.opt_in_date then 0 else extract(day from lr.loan_date - dp.opt_in_date)*24 + extract(hour from lr.loan_date - dp.opt_in_date) + extract(minute from lr.loan_date - dp.opt_in_date)/60 end as lead_full_track_time,
replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(dp.reason_for_loan,'Capital de Giro','Working Capital'),'Expansão','Expansion'),'Compra de Estoque','Inventory'),'Outros','Others'),'Reformar o meu negócio','Refurbishment'),'Consolidação de Dívidas','Debt Consolidation'),'Marketing e Vendas','Sales and Marketing'),'Uso Pessoal','Personal Use'),'Compra de equipamentos','Machinery'),'Estoques','Inventory'),'Melhorar meu fluxo de caixa','Working Capital'),
	'Aumentar o volume
                        do meu estoque','Inventory'),'Comprar novos equipamentos','Machinery'),'Expandir minha empresa','Expansion'),'Quitar minhas dividas','Debt Consolidation'),'Reforma','Refurbishment'),'Compra de Equipamentos','Machinery') as lead_reason_for_loan,
replace(replace(replace(replace(dp.vinculo,'Administrador','Administrator'),'Outros','Other'),'Procurador','Attorney'),'Sócio','Shareholder') as lead_requester_relationship,
dp.month_revenue as lead_informed_month_revenue,
dp.revenue::double precision as lead_estimated_annual_revenue,
dp.amount_requested as lead_application_amount,
case when utm_source is null then 'NULL' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_source,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor',''),'-','') end as lead_marketing_channel,
case when dp.utm_medium is null then 'NULL' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(dp.utm_medium,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor','') end  as lead_marketing_medium,
case when utm_campaign is null then 'NULL' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_campaign,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'lookalike-maio','Adespresso Lookalike - Maio'),'mosaic-maio-top-revenue-big','Adespresso Mosaic Top Revenues (Big Ones) - Maio'),'%20',' '),'%3E','>'),'#howItWorksAnchor','') end as lead_marketing_campaign,
dp.opt_in_ip as lead_opt_in_ip,
case when dp.n_company_applied_before is null then 0 else dp.n_company_applied_before end as lead_times_company_applied_before,
case when dp.n_individual_applied_before is null then 0 when dp.utm_source in ('fdex','capitalemp') then null else dp.n_individual_applied_before end as lead_times_individual_applied_before,
to_date(dp.date_created,'yyyy-mm-dd') as lead_date_company_creation,
dp.age as lead_company_age,
case when (month_revenue >= 100000 and age < 2) or (month_revenue >= 200000 and age < 3) or (month_revenue >= 300000 and age < 4) then 1 else 0 end as lead_Age_Revenue_Discrepancy,
dp.employees as lead_estimate_number_of_employees,
dp.mei::int as lead_is_mei,
dp.is_simples::int as lead_simple_tribute_method_applied,
dp.cnae_code as lead_economic_activity_national_registration_code,
dp.cnae as lead_economic_activity_national_registration_name,
dp.social_capital::float as lead_social_capital,
dp.number_partners as lead_number_of_shareholders,
dp.number_coligadas as lead_number_of_related_companies,
dp.number_branches as lead_number_of_branches,
case when dp.phone = '' then null else substring(dp.phone,2,2)::int end as lead_phone_area_code,
case when (case when dp.zip_code = '' then null else dp.zip_code end) is null then signer_address.signer_zipcode else dp.zip_code end as lead_company_zip_code,
dp.neighbourhood as lead_company_neighbourhood,
dp.city as lead_company_city,
dp.state as lead_company_state,
p.populacao as lead_citys_population,
case when (dp.city = 'RIO BRANCO' and dp.state = 'AC') or (dp.city = 'MACEIO' and dp.state = 'AL') or (dp.city = 'MACAPA' and dp.state = 'AP') or (dp.city = 'MANAUS' and dp.state = 'AM') or (dp.city = 'SALVADOR' and dp.state = 'BA') or (dp.city = 'FORTALEZA' and dp.state = 'CE') or 
	(dp.city = 'BRASILIA' and dp.state = 'DF') or (dp.city = 'VITORIA' and dp.state = 'ES') or (dp.city = 'GOIANIA' and dp.state = 'GO') or (dp.city = 'SAO LUIS' and dp.state = 'MA') or (dp.city = 'CUIABA' and dp.state = 'MT') or (dp.city = 'CAMPO GRANDE' and dp.state = 'MS') or 
	(dp.city = 'BELO HORIZONTE' and dp.state = 'MG') or (dp.city = 'BELEM' and dp.state = 'PA') or (dp.city = 'JOAO PESSOA' and dp.state = 'PB') or (dp.city = 'CURITIBA' and dp.state = 'PR') or (dp.city = 'RECIFE' and dp.state = 'PE') or (dp.city = 'TERESINA' and dp.state = 'PI') or 
	(dp.city = 'RIO DE JANEIRO' and dp.state = 'RJ') or (dp.city = 'NATAL' and dp.state = 'RN') or (dp.city = 'PORTO VELHO' and dp.state = 'RO') or (dp.city = 'BOA VISTA' and dp.state = 'RR') or (dp.city = 'FLORIANOPOLIS' and dp.state = 'SC') or (dp.city = 'SAO PAULO' and dp.state = 'SP') or 
	(dp.city = 'ARACAJU' and dp.state = 'SE') or (dp.city = 'PALMAS' and dp.state = 'TO') or (dp.city = 'PORTO ALEGRE' and dp.state = 'RS') then 1 else 0 end as lead_is_city_capital,
case when dp.state in ('AM','RR','AP','PA','TO','RO','AC') then 'NORTE' when dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA') then 'NORDESTE' when dp.state in ('MT','MS','GO','DF') then 'CENTRO-OESTE' when dp.state in ('SP','RJ','MG','ES') then 'SUDESTE' when dp.state in ('PR','RS','SC') then 'SUL' else null end as lead_state_region,
dp.facebook::int lead_has_facebook_page,
dp.likes lead_number_likes_facebook,
dp.streetview::int lead_has_google_streetview,
dp.site::int lead_has_website,
replace(replace(replace(replace(replace(dp.tax_health,'VERDE','Green'),'AZUL','Blue'),'AMARELO','Yellow'),'LARANJA','Orange'),'CINZA','Grey') as lead_tax_health,
replace(replace(replace(replace(dp.level_activity,'MEDIA','Medium'),'ALTA','High'),'MUITO BAIXA','Very Low'),'BAIXA','Low') as lead_level_activity,
replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(dp.activity_sector,'VAREJO','Retail'),'SERVICOS DE ALOJAMENTO/ALIMENTACAO','Accomodation/Feeding'),
	'TELECOM','Telecommunication'),'TRANSPORTE','Transportation'),'SERVICOS ADMINISTRATIVOS','Administrative Services'),'SERVICOS DE SAUDE','Health services'),'SERVICOS PROFISSIONAIS E TECNICOS','Technical/Professional Services'),'INDUSTRIA DIGITAL','Digital Industry'),
	'QUIMICA-PETROQUIMICA','Chemistry/Petrochemistry'),'SERVICOS DE EDUCACAO','Education Services'),'BENS DE CONSUMO','Consumer goods'),'ATACADO','Wholesale'),'INDUSTRIA DA CONSTRUCAO','Construction Industry'),'SIDERURGICA-METALURGIA','Steel Industry'),'ELETROELETRONICOS','Eletronic goods'),
	'PRODUTOS DE AGROPECUARIA','Farming goods'),'TEXTEIS','Textiles'),'PAPEL E CELULOSE','Paper/Celulosis'),'SERVICOS DE SANEAMENTO BASICO','Sanitation Services'),'INDUSTRIA AUTOMOTIVA','Car Industry'),'ENERGIA','Energy'),'BENS DE CAPITAL','Capital goods'),'SERVICOS DIVERSOS','Miscellaneous Services'),
	'DIVERSOS','Miscellaneous'),'FARMACEUTICA','Farmaceuticals'),'MINERACAO','Mining') as lead_activity_sector,
replace(replace(replace(replace(replace(dp.sector,'COMERCIO','Retail'),'SERVICOS','Services'),'INDUSTRIA','Industry'),'AGROPECUARIA','Farming'),'CONSTRUCAO CIVIL','Construction') as lead_sector,
case when upper(left(right(dp.email, length(dp.email)-position('@' in dp.email)),position('.' in right(dp.email, length(dp.email)-position('@' in dp.email)-1))))  in ('GMAIL','HOTMAIL','YAHOO','OUTLOOK','UOL','TERRA','BOL','LIVE','ICLOUD','IG','MSN','GLOBO') then 
	replace(upper(left(right(dp.email, length(dp.email)-position('@' in dp.email)),position('.' in right(dp.email, length(dp.email)-position('@' in dp.email)-1)))),'GMAIIL','GMAIL') else 'OTHER' end as lead_email_server,
dp.pgfn_debt as lead_government_debt,
dp.number_protests as lead_notary_protests_unit,
dp.protests_amount lead_notary_protests_value,
dp.protests_amount/month_revenue/12 lead_notary_protests_value_byrevenue,
dp.number_spc lead_bureau_derogatory_unit,
dp.spc_amount lead_bureau_derogatory_value,
dp.spc_amount/month_revenue/12 lead_bureau_derogatory_value_byrevenue,
lead_score::float as lead_biz_lead_score,
bizu_score::float as lead_biz_bizu_score,
case when bizu_score >= 850 then 'A+' when bizu_score between 700 and 849.99 then 'A-' when bizu_score between 650 and 699.99 then 'B+' when bizu_score between 600 and 649.99 then 'B-' when bizu_score between 500 and 599.99 then 'C+'when bizu_score between 400 and 499.99 then 'C-'when bizu_score between 300 and 399.99 then 'D'else 'E' end as Rating_class_bizu,
case when serasa_coleta is null then o.rating::int else serasa_coleta end as lead_experian_score4,
case when case when serasa_coleta is null then o.rating::int else serasa_coleta end >= 750 then 'A+' when case when serasa_coleta is null then o.rating::int else serasa_coleta end between 600 and 749.99 then 'A-' when case when serasa_coleta is null then o.rating::int else serasa_coleta end between 450 and 599.99 then 'B+' when case when serasa_coleta is null then o.rating::int else serasa_coleta end between 350 and 449.99 then 'B-' when case when serasa_coleta is null then o.rating::int else serasa_coleta end between 250 and 349.99 then 'C+'when case when serasa_coleta is null then o.rating::int else serasa_coleta end between 150 and 249.99 then 'C-'when case when serasa_coleta is null then o.rating::int else serasa_coleta end between 100 and 149.99 then 'D' else 'E' end as Rating_class_serasa,
pd_serasa lead_experian_score4_associated_pd,
dp.bizu2_score::float as lead_biz_bizu2_score,
dp.pd_bizu2::float as lead_biz_bizu2_associated_pd,
case when dp.bizu2_score >= 1100 then 'A+' when dp.bizu2_score >= 850 then 'A-' when dp.bizu2_score >= 720 then 'B+' when dp.bizu2_score >= 600 then 'B-' when dp.bizu2_score >= 500 then 'C+' when dp.bizu2_score >= 400 then 'C-' when dp.bizu2_score >= 300 then 'D' when dp.bizu2_score >= 200 then 'E' when dp.bizu2_score < 200 then 'F' else 'Sem Bizu2' end as Classe_Bizu2,
--------------------------INITIAL OFFER
initialoff.date_inserted as offer_initial_offer_date,
initialoff.interest_rate offer_initial_offer_interest_rate,
initialoff.max_number_of_installments offer_initial_offer_max_term,
initialoff.max_value offer_initial_offer_max_value,
initialoff.count_neg - 1 as offer_count_negotiations,
--------------------------FINAL OFFER
o.date_inserted as offer_final_offer_date,
extract(hour from o.date_inserted)::int as offer_final_offer_hour,
extract(day from o.date_inserted)::int as offer_final_offer_day,
extract(month from o.date_inserted)::int as offer_final_offer_month,
extract(year from o.date_inserted)::int as offer_final_offer_year,
o.interest_rate offer_final_offer_interest_rate,
o.max_number_of_installments offer_final_offer_max_term,
o.max_value offer_final_offer_max_value,
--------------------------LOAN
lr.date_inserted as loan_request_date,
extract(hour from lr.date_inserted)::int as loan_request_hour,
extract(day from lr.date_inserted)::int as loan_request_day,
extract(month from lr.date_inserted)::int as loan_request_month,
extract(year from lr.date_inserted)::int as loan_request_year,
is_renew as loan_is_renewal,
lr.value loan_net_value,
li.total_payment loan_gross_value,
li.monthly_cet loan_final_interest_rate,
lr.number_of_installments loan_original_final_term,
(@li.pmt) as loan_original_final_pmt,
(@li.pmt)/month_revenue as loan_original_final_pmt_leverage,
lr.loan_date as loan_contract_date,
extract(day from lr.loan_date)::int as loan_contract_day,
extract(month from lr.loan_date)::int as loan_contract_month,
extract(year from lr.loan_date)::int as loan_contract_year,
lr.portfolio_id::text loan_portfolio_id,
ba.bank_id::text loan_bank_id,
extract(day from case when lr.payment_day is null then original_1st_pmt_due_to else lr.payment_day end) as loan_1st_pmt_chosen_day_of_month,
case when lr.payment_day is null then original_1st_pmt_due_to else lr.payment_day end - lr.loan_date as loan_grace_period,
--------------------------COLLECTION
fully_paid collection_loan_fully_paid,
due_inst + to_come_inst as collection_loan_current_term,
to_come_inst as collection_to_be_due_installments,
due_inst as collection_total_due_installments,
paid_inst collection_number_of_installments_paid,
late_inst collection_number_of_installments_curr_late, 
count_renegotiation as collection_count_renegotiation_plans,
ParcEmAberto as collection_current_installment_number_due,
dpd_soft.max_late_pmt1 as collection_max_late_pmt1,
dpd_soft.max_late_pmt2 as collection_max_late_pmt2,
dpd_soft.max_late_pmt3 as collection_max_late_pmt3,
dpd_soft.max_late_pmt4 as collection_max_late_pmt4,
dpd_soft.max_late_pmt5 as collection_max_late_pmt5,
dpd_soft.max_late_pmt6 as collection_max_late_pmt6,
dpd_soft.max_late_pmt7 as collection_max_late_pmt7,
dpd_soft.max_late_pmt8 as collection_max_late_pmt8,
dpd_soft.max_late_pmt9 as collection_max_late_pmt9,
dpd_soft.max_late_pmt10 as collection_max_late_pmt10,
dpd_soft.max_late_pmt11 as collection_max_late_pmt11,
dpd_soft.max_late_pmt12 as collection_max_late_pmt12,
case when paid_inst > 0 then 0 else dpd_soft.max_late_pmt1 end as collection_curr_fpd,
case when case when paid_inst > 0 then first_paid_inst_paid_on - original_1st_pmt_due_to else dpd_soft.max_late_pmt1 end >= 30 then 1 else 0 end as collection_fpd_30,
case when case when paid_inst > 0 then first_paid_inst_paid_on - original_1st_pmt_due_to else dpd_soft.max_late_pmt1 end >= 60 then 1 else 0 end as collection_fpd_60,
case when case when paid_inst > 0 then first_paid_inst_paid_on - original_1st_pmt_due_to else dpd_soft.max_late_pmt1 end >= 90 then 1 else 0 end as collection_fpd_90,
case when current_inst_due_to is null then 0 else current_date - current_inst_due_to end as collection_loan_curr_late_by,
case when current_inst_due_to is null then 0 else case when current_date - current_inst_due_to > 30 then 1 else 0 end end as collection_curr_over_30,
case when current_inst_due_to is null then 0 else case when current_date - current_inst_due_to > 60 then 1 else 0 end end as collection_curr_over_60,
case when current_inst_due_to is null then 0 else case when current_date - current_inst_due_to > 90 then 1 else 0 end end as collection_curr_over_90,
c.atraso_max_dias collection_client_max_late_by,
lr.atraso_max_dias collection_loan_max_late_by,
case when lr.atraso_max_dias > 30 then 1 else 0 end as collection_Ever_30,
case when lr.atraso_max_dias > 60 then 1 else 0 end as collection_Ever_60,
case when lr.atraso_max_dias > 90 then 1 else 0 end as collection_Ever_90,
case when c.atraso_max_dias > 180 then EAD * 1 when c.atraso_max_dias > 150 then EAD * 0.7 when c.atraso_max_dias > 120 then EAD * 0.5 when c.atraso_max_dias > 90 then EAD * 0.3 when c.atraso_max_dias > 60 then EAD * 0.1 when c.atraso_max_dias > 30 then EAD * 0.03 when c.atraso_max_dias > 14 then EAD * 0.01 when c.atraso_max_dias > 0 then EAD * 0.005 else 0 end as collection_Provision_for_loss_Central_Bank,
case when EAD is null then 0 else EAD end as collection_ead,
curr_total_payment as "colletion_loan_p+i_value_curr",
original_total_payment as "colletion_loan_p+i_value_original",
pmt_atual as collection_current_pmt,
case when total_paid is null then 0 else total_paid end collection_loan_amount_paid,
case when late_payment is null then 0 else late_payment end collection_loan_amount_late,
case when writeoff is null then 0 else writeoff end collection_loan_amount_writeoff,
case when dpd_soft.max_late_pmt1 >= 60 then 1 when dpd_soft.max_late_pmt2 >= 60 then 2 when dpd_soft.max_late_pmt3 >= 60 then 3 when dpd_soft.max_late_pmt4 >= 60 then 4 when dpd_soft.max_late_pmt5 >= 60 then 5 when dpd_soft.max_late_pmt6 >= 60 then 6 when dpd_soft.max_late_pmt7 >= 60 then 7 when dpd_soft.max_late_pmt8 >= 60 then 8 when dpd_soft.max_late_pmt9 >= 60 then 9 when dpd_soft.max_late_pmt10 >= 60 then 10 when dpd_soft.max_late_pmt11 >= 60 then 11 when dpd_soft.max_late_pmt12 >= 60 then 12 else 0 end as collection_pmt_of_over60,
case when pmt3_actual_soft.loan_request_id is null then null when pmt3_actual_soft.max_late_pmt1 >= 30 or pmt3_actual_soft.max_late_pmt2 >= 30 or pmt3_actual_soft.max_late_pmt3 >= 30 then 1 else 0 end as collection_ever_over30_pmt3_reneg,
case when pmt3_actual_soft.loan_request_id is null then null when pmt3_actual_soft.max_late_pmt1 >= 60 or pmt3_actual_soft.max_late_pmt2 >= 60 or pmt3_actual_soft.max_late_pmt3 >= 60 then 1 else 0 end as collection_ever_over60_pmt3_reneg,
case when pmt6_actual_soft.loan_request_id is null then null when pmt6_actual_soft.max_late_pmt1 >= 30 or pmt6_actual_soft.max_late_pmt2 >= 30 or pmt6_actual_soft.max_late_pmt3 >= 30 or pmt6_actual_soft.max_late_pmt4 >= 30 or pmt6_actual_soft.max_late_pmt5 >= 30 or pmt6_actual_soft.max_late_pmt6 >= 30 then 1 else 0 end as collection_ever_over30_pmt6_reneg,
case when pmt6_actual_soft.loan_request_id is null then null when pmt6_actual_soft.max_late_pmt1 >= 60 or pmt6_actual_soft.max_late_pmt2 >= 60 or pmt6_actual_soft.max_late_pmt3 >= 60 or pmt6_actual_soft.max_late_pmt4 >= 60 or pmt6_actual_soft.max_late_pmt5 >= 60 or pmt6_actual_soft.max_late_pmt6 >= 60 then 1 else 0 end as collection_ever_over60_pmt6_reneg,
case when pmt6_actual_soft.loan_request_id is null then null when pmt6_actual_soft.max_late_pmt1 >= 90 or pmt6_actual_soft.max_late_pmt2 >= 90 or pmt6_actual_soft.max_late_pmt3 >= 90 or pmt6_actual_soft.max_late_pmt4 >= 90 or pmt6_actual_soft.max_late_pmt5 >= 90 or pmt6_actual_soft.max_late_pmt6 >= 90 then 1 else 0 end as collection_ever_over90_pmt6_reneg,
case when pmt9_actual_soft.loan_request_id is null then null when pmt9_actual_soft.max_late_pmt1 >= 30 or pmt9_actual_soft.max_late_pmt2 >= 30 or pmt9_actual_soft.max_late_pmt3 >= 30 or pmt9_actual_soft.max_late_pmt4 >= 30 or pmt9_actual_soft.max_late_pmt5 >= 30 or pmt9_actual_soft.max_late_pmt6 >= 30 or pmt9_actual_soft.max_late_pmt7 >= 30 or pmt9_actual_soft.max_late_pmt8 >= 30 or pmt9_actual_soft.max_late_pmt9 >= 30 then 1 else 0 end as collection_ever_over30_pmt9_reneg,
case when pmt9_actual_soft.loan_request_id is null then null when pmt9_actual_soft.max_late_pmt1 >= 60 or pmt9_actual_soft.max_late_pmt2 >= 60 or pmt9_actual_soft.max_late_pmt3 >= 60 or pmt9_actual_soft.max_late_pmt4 >= 60 or pmt9_actual_soft.max_late_pmt5 >= 60 or pmt9_actual_soft.max_late_pmt6 >= 60 or pmt9_actual_soft.max_late_pmt7 >= 60 or pmt9_actual_soft.max_late_pmt8 >= 60 or pmt9_actual_soft.max_late_pmt9 >= 60 then 1 else 0 end as collection_ever_over60_pmt9_reneg,
case when pmt9_actual_soft.loan_request_id is null then null when pmt9_actual_soft.max_late_pmt1 >= 90 or pmt9_actual_soft.max_late_pmt2 >= 90 or pmt9_actual_soft.max_late_pmt3 >= 90 or pmt9_actual_soft.max_late_pmt4 >= 90 or pmt9_actual_soft.max_late_pmt5 >= 90 or pmt9_actual_soft.max_late_pmt6 >= 90 or pmt9_actual_soft.max_late_pmt7 >= 90 or pmt9_actual_soft.max_late_pmt8 >= 90 or pmt9_actual_soft.max_late_pmt9 >= 90 then 1 else 0 end as collection_ever_over90_pmt9_reneg,
case when pmt3_actual_soft.loan_request_id is null then null when (case when pmt3_actual_soft.ultparcpaga is null then pmt3_actual_soft.max_late_pmt1 when pmt3_actual_soft.ultparcpaga = 1 then pmt3_actual_soft.max_late_pmt2 when pmt3_actual_soft.ultparcpaga = 2 then pmt3_actual_soft.max_late_pmt3 when pmt3_actual_soft.ultparcpaga >= 3 then 0 end) >= 30 then 1 else 0 end as collection_curr_over30_pmt3_reneg,
case when pmt3_actual_soft.loan_request_id is null then null when (case when pmt3_actual_soft.ultparcpaga is null then pmt3_actual_soft.max_late_pmt1 when pmt3_actual_soft.ultparcpaga = 1 then pmt3_actual_soft.max_late_pmt2 when pmt3_actual_soft.ultparcpaga = 2 then pmt3_actual_soft.max_late_pmt3 when pmt3_actual_soft.ultparcpaga >= 3 then 0 end) >= 60 then 1 else 0 end as collection_curr_over60_pmt3_reneg,
case when pmt6_actual_soft.loan_request_id is null then null when (case when pmt6_actual_soft.ultparcpaga is null then pmt6_actual_soft.max_late_pmt1 when pmt6_actual_soft.ultparcpaga = 1 then pmt6_actual_soft.max_late_pmt2 when pmt6_actual_soft.ultparcpaga = 2 then pmt6_actual_soft.max_late_pmt3 when pmt6_actual_soft.ultparcpaga = 3 then pmt6_actual_soft.max_late_pmt4 when pmt6_actual_soft.ultparcpaga = 4 then pmt6_actual_soft.max_late_pmt5 when pmt6_actual_soft.ultparcpaga = 5 then pmt6_actual_soft.max_late_pmt6 when pmt6_actual_soft.ultparcpaga >= 6 then 0 end) >= 30 then 1 else 0 end as collection_curr_over30_pmt6_reneg,
case when pmt6_actual_soft.loan_request_id is null then null when (case when pmt6_actual_soft.ultparcpaga is null then pmt6_actual_soft.max_late_pmt1 when pmt6_actual_soft.ultparcpaga = 1 then pmt6_actual_soft.max_late_pmt2 when pmt6_actual_soft.ultparcpaga = 2 then pmt6_actual_soft.max_late_pmt3 when pmt6_actual_soft.ultparcpaga = 3 then pmt6_actual_soft.max_late_pmt4 when pmt6_actual_soft.ultparcpaga = 4 then pmt6_actual_soft.max_late_pmt5 when pmt6_actual_soft.ultparcpaga = 5 then pmt6_actual_soft.max_late_pmt6 when pmt6_actual_soft.ultparcpaga >= 6 then 0 end) >= 60 then 1 else 0 end as collection_curr_over60_pmt6_reneg,
case when pmt6_actual_soft.loan_request_id is null then null when (case when pmt6_actual_soft.ultparcpaga is null then pmt6_actual_soft.max_late_pmt1 when pmt6_actual_soft.ultparcpaga = 1 then pmt6_actual_soft.max_late_pmt2 when pmt6_actual_soft.ultparcpaga = 2 then pmt6_actual_soft.max_late_pmt3 when pmt6_actual_soft.ultparcpaga = 3 then pmt6_actual_soft.max_late_pmt4 when pmt6_actual_soft.ultparcpaga = 4 then pmt6_actual_soft.max_late_pmt5 when pmt6_actual_soft.ultparcpaga = 5 then pmt6_actual_soft.max_late_pmt6 when pmt6_actual_soft.ultparcpaga >= 6 then 0 end) >= 90 then 1 else 0 end as collection_curr_over90_pmt6_reneg,
case when pmt9_actual_soft.loan_request_id is null then null when (case when pmt9_actual_soft.ultparcpaga is null then pmt9_actual_soft.max_late_pmt1 when pmt9_actual_soft.ultparcpaga = 1 then pmt9_actual_soft.max_late_pmt2 when pmt9_actual_soft.ultparcpaga = 2 then pmt9_actual_soft.max_late_pmt3 when pmt9_actual_soft.ultparcpaga = 3 then pmt9_actual_soft.max_late_pmt4 when pmt9_actual_soft.ultparcpaga = 4 then pmt9_actual_soft.max_late_pmt5 when pmt9_actual_soft.ultparcpaga = 5 then pmt9_actual_soft.max_late_pmt6 when pmt9_actual_soft.ultparcpaga = 6 then pmt9_actual_soft.max_late_pmt7 when pmt9_actual_soft.ultparcpaga = 7 then pmt9_actual_soft.max_late_pmt8 when pmt9_actual_soft.ultparcpaga = 8 then pmt9_actual_soft.max_late_pmt9 when pmt9_actual_soft.ultparcpaga >= 9 then 0 end) >= 30 then 1 else 0 end as collection_curr_over30_pmt9_reneg,
case when pmt9_actual_soft.loan_request_id is null then null when (case when pmt9_actual_soft.ultparcpaga is null then pmt9_actual_soft.max_late_pmt1 when pmt9_actual_soft.ultparcpaga = 1 then pmt9_actual_soft.max_late_pmt2 when pmt9_actual_soft.ultparcpaga = 2 then pmt9_actual_soft.max_late_pmt3 when pmt9_actual_soft.ultparcpaga = 3 then pmt9_actual_soft.max_late_pmt4 when pmt9_actual_soft.ultparcpaga = 4 then pmt9_actual_soft.max_late_pmt5 when pmt9_actual_soft.ultparcpaga = 5 then pmt9_actual_soft.max_late_pmt6 when pmt9_actual_soft.ultparcpaga = 6 then pmt9_actual_soft.max_late_pmt7 when pmt9_actual_soft.ultparcpaga = 7 then pmt9_actual_soft.max_late_pmt8 when pmt9_actual_soft.ultparcpaga = 8 then pmt9_actual_soft.max_late_pmt9 when pmt9_actual_soft.ultparcpaga >= 9 then 0 end) >= 60 then 1 else 0 end as collection_curr_over60_pmt9_reneg,
case when pmt9_actual_soft.loan_request_id is null then null when (case when pmt9_actual_soft.ultparcpaga is null then pmt9_actual_soft.max_late_pmt1 when pmt9_actual_soft.ultparcpaga = 1 then pmt9_actual_soft.max_late_pmt2 when pmt9_actual_soft.ultparcpaga = 2 then pmt9_actual_soft.max_late_pmt3 when pmt9_actual_soft.ultparcpaga = 3 then pmt9_actual_soft.max_late_pmt4 when pmt9_actual_soft.ultparcpaga = 4 then pmt9_actual_soft.max_late_pmt5 when pmt9_actual_soft.ultparcpaga = 5 then pmt9_actual_soft.max_late_pmt6 when pmt9_actual_soft.ultparcpaga = 6 then pmt9_actual_soft.max_late_pmt7 when pmt9_actual_soft.ultparcpaga = 7 then pmt9_actual_soft.max_late_pmt8 when pmt9_actual_soft.ultparcpaga = 8 then pmt9_actual_soft.max_late_pmt9 when pmt9_actual_soft.ultparcpaga >= 9 then 0 end) >= 90 then 1 else 0 end as collection_curr_over90_pmt9_reneg,
case when pmt3_orig_soft.loan_request_id is null then null when pmt3_orig_soft.max_late_pmt1 >= 30 or pmt3_orig_soft.max_late_pmt2 >= 30 or pmt3_orig_soft.max_late_pmt3 >= 30 then 1 else 0 end as collection_ever_over30_pmt3_orig_soft,
case when pmt3_orig_soft.loan_request_id is null then null when pmt3_orig_soft.max_late_pmt1 >= 60 or pmt3_orig_soft.max_late_pmt2 >= 60 or pmt3_orig_soft.max_late_pmt3 >= 60 then 1 else 0 end as collection_ever_over60_pmt3_orig_soft,
case when pmt6_orig_soft.loan_request_id is null then null when pmt6_orig_soft.max_late_pmt1 >= 30 or pmt6_orig_soft.max_late_pmt2 >= 30 or pmt6_orig_soft.max_late_pmt3 >= 30 or pmt6_orig_soft.max_late_pmt4 >= 30 or pmt6_orig_soft.max_late_pmt5 >= 30 or pmt6_orig_soft.max_late_pmt6 >= 30 then 1 else 0 end as collection_ever_over30_pmt6_orig_soft,
case when pmt6_orig_soft.loan_request_id is null then null when pmt6_orig_soft.max_late_pmt1 >= 60 or pmt6_orig_soft.max_late_pmt2 >= 60 or pmt6_orig_soft.max_late_pmt3 >= 60 or pmt6_orig_soft.max_late_pmt4 >= 60 or pmt6_orig_soft.max_late_pmt5 >= 60 or pmt6_orig_soft.max_late_pmt6 >= 60 then 1 else 0 end as collection_ever_over60_pmt6_orig_soft,
case when pmt6_orig_soft.loan_request_id is null then null when pmt6_orig_soft.max_late_pmt1 >= 90 or pmt6_orig_soft.max_late_pmt2 >= 90 or pmt6_orig_soft.max_late_pmt3 >= 90 or pmt6_orig_soft.max_late_pmt4 >= 90 or pmt6_orig_soft.max_late_pmt5 >= 90 or pmt6_orig_soft.max_late_pmt6 >= 90 then 1 else 0 end as collection_ever_over90_pmt6_orig_soft,
case when pmt9_orig_soft.loan_request_id is null then null when pmt9_orig_soft.max_late_pmt1 >= 30 or pmt9_orig_soft.max_late_pmt2 >= 30 or pmt9_orig_soft.max_late_pmt3 >= 30 or pmt9_orig_soft.max_late_pmt4 >= 30 or pmt9_orig_soft.max_late_pmt5 >= 30 or pmt9_orig_soft.max_late_pmt6 >= 30 or pmt9_orig_soft.max_late_pmt7 >= 30 or pmt9_orig_soft.max_late_pmt8 >= 30 or pmt9_orig_soft.max_late_pmt9 >= 30 then 1 else 0 end as collection_ever_over30_pmt9_orig_soft,
case when pmt9_orig_soft.loan_request_id is null then null when pmt9_orig_soft.max_late_pmt1 >= 60 or pmt9_orig_soft.max_late_pmt2 >= 60 or pmt9_orig_soft.max_late_pmt3 >= 60 or pmt9_orig_soft.max_late_pmt4 >= 60 or pmt9_orig_soft.max_late_pmt5 >= 60 or pmt9_orig_soft.max_late_pmt6 >= 60 or pmt9_orig_soft.max_late_pmt7 >= 60 or pmt9_orig_soft.max_late_pmt8 >= 60 or pmt9_orig_soft.max_late_pmt9 >= 60 then 1 else 0 end as collection_ever_over60_pmt9_orig_soft,
case when pmt9_orig_soft.loan_request_id is null then null when pmt9_orig_soft.max_late_pmt1 >= 90 or pmt9_orig_soft.max_late_pmt2 >= 90 or pmt9_orig_soft.max_late_pmt3 >= 90 or pmt9_orig_soft.max_late_pmt4 >= 90 or pmt9_orig_soft.max_late_pmt5 >= 90 or pmt9_orig_soft.max_late_pmt6 >= 90 or pmt9_orig_soft.max_late_pmt7 >= 90 or pmt9_orig_soft.max_late_pmt8 >= 90 or pmt9_orig_soft.max_late_pmt9 >= 90 then 1 else 0 end as collection_ever_over90_pmt9_orig_soft,
case when pmt3_orig_soft.loan_request_id is null then null when (case when pmt3_orig_soft.ultparcpaga is null then pmt3_orig_soft.max_late_pmt1 when pmt3_orig_soft.ultparcpaga = 1 then pmt3_orig_soft.max_late_pmt2 when pmt3_orig_soft.ultparcpaga = 2 then pmt3_orig_soft.max_late_pmt3 when pmt3_orig_soft.ultparcpaga >= 3 then 0 end) >= 30 then 1 else 0 end as collection_curr_over30_pmt3_orig_soft,
case when pmt3_orig_soft.loan_request_id is null then null when (case when pmt3_orig_soft.ultparcpaga is null then pmt3_orig_soft.max_late_pmt1 when pmt3_orig_soft.ultparcpaga = 1 then pmt3_orig_soft.max_late_pmt2 when pmt3_orig_soft.ultparcpaga = 2 then pmt3_orig_soft.max_late_pmt3 when pmt3_orig_soft.ultparcpaga >= 3 then 0 end) >= 60 then 1 else 0 end as collection_curr_over60_pmt3_orig_soft,
case when pmt6_orig_soft.loan_request_id is null then null when (case when pmt6_orig_soft.ultparcpaga is null then pmt6_orig_soft.max_late_pmt1 when pmt6_orig_soft.ultparcpaga = 1 then pmt6_orig_soft.max_late_pmt2 when pmt6_orig_soft.ultparcpaga = 2 then pmt6_orig_soft.max_late_pmt3 when pmt6_orig_soft.ultparcpaga = 3 then pmt6_orig_soft.max_late_pmt4 when pmt6_orig_soft.ultparcpaga = 4 then pmt6_orig_soft.max_late_pmt5 when pmt6_orig_soft.ultparcpaga = 5 then pmt6_orig_soft.max_late_pmt6 when pmt6_orig_soft.ultparcpaga >= 6 then 0 end) >= 30 then 1 else 0 end as collection_curr_over30_pmt6_orig_soft,
case when pmt6_orig_soft.loan_request_id is null then null when (case when pmt6_orig_soft.ultparcpaga is null then pmt6_orig_soft.max_late_pmt1 when pmt6_orig_soft.ultparcpaga = 1 then pmt6_orig_soft.max_late_pmt2 when pmt6_orig_soft.ultparcpaga = 2 then pmt6_orig_soft.max_late_pmt3 when pmt6_orig_soft.ultparcpaga = 3 then pmt6_orig_soft.max_late_pmt4 when pmt6_orig_soft.ultparcpaga = 4 then pmt6_orig_soft.max_late_pmt5 when pmt6_orig_soft.ultparcpaga = 5 then pmt6_orig_soft.max_late_pmt6 when pmt6_orig_soft.ultparcpaga >= 6 then 0 end) >= 60 then 1 else 0 end as collection_curr_over60_pmt6_orig_soft,
case when pmt6_orig_soft.loan_request_id is null then null when (case when pmt6_orig_soft.ultparcpaga is null then pmt6_orig_soft.max_late_pmt1 when pmt6_orig_soft.ultparcpaga = 1 then pmt6_orig_soft.max_late_pmt2 when pmt6_orig_soft.ultparcpaga = 2 then pmt6_orig_soft.max_late_pmt3 when pmt6_orig_soft.ultparcpaga = 3 then pmt6_orig_soft.max_late_pmt4 when pmt6_orig_soft.ultparcpaga = 4 then pmt6_orig_soft.max_late_pmt5 when pmt6_orig_soft.ultparcpaga = 5 then pmt6_orig_soft.max_late_pmt6 when pmt6_orig_soft.ultparcpaga >= 6 then 0 end) >= 90 then 1 else 0 end as collection_curr_over90_pmt6_orig_soft,
case when pmt9_orig_soft.loan_request_id is null then null when (case when pmt9_orig_soft.ultparcpaga is null then pmt9_orig_soft.max_late_pmt1 when pmt9_orig_soft.ultparcpaga = 1 then pmt9_orig_soft.max_late_pmt2 when pmt9_orig_soft.ultparcpaga = 2 then pmt9_orig_soft.max_late_pmt3 when pmt9_orig_soft.ultparcpaga = 3 then pmt9_orig_soft.max_late_pmt4 when pmt9_orig_soft.ultparcpaga = 4 then pmt9_orig_soft.max_late_pmt5 when pmt9_orig_soft.ultparcpaga = 5 then pmt9_orig_soft.max_late_pmt6 when pmt9_orig_soft.ultparcpaga = 6 then pmt9_orig_soft.max_late_pmt7 when pmt9_orig_soft.ultparcpaga = 7 then pmt9_orig_soft.max_late_pmt8 when pmt9_orig_soft.ultparcpaga = 8 then pmt9_orig_soft.max_late_pmt9 when pmt9_orig_soft.ultparcpaga >= 9 then 0 end) >= 30 then 1 else 0 end as collection_curr_over30_pmt9_orig_soft,
case when pmt9_orig_soft.loan_request_id is null then null when (case when pmt9_orig_soft.ultparcpaga is null then pmt9_orig_soft.max_late_pmt1 when pmt9_orig_soft.ultparcpaga = 1 then pmt9_orig_soft.max_late_pmt2 when pmt9_orig_soft.ultparcpaga = 2 then pmt9_orig_soft.max_late_pmt3 when pmt9_orig_soft.ultparcpaga = 3 then pmt9_orig_soft.max_late_pmt4 when pmt9_orig_soft.ultparcpaga = 4 then pmt9_orig_soft.max_late_pmt5 when pmt9_orig_soft.ultparcpaga = 5 then pmt9_orig_soft.max_late_pmt6 when pmt9_orig_soft.ultparcpaga = 6 then pmt9_orig_soft.max_late_pmt7 when pmt9_orig_soft.ultparcpaga = 7 then pmt9_orig_soft.max_late_pmt8 when pmt9_orig_soft.ultparcpaga = 8 then pmt9_orig_soft.max_late_pmt9 when pmt9_orig_soft.ultparcpaga >= 9 then 0 end) >= 60 then 1 else 0 end as collection_curr_over60_pmt9_orig_soft,
case when pmt9_orig_soft.loan_request_id is null then null when (case when pmt9_orig_soft.ultparcpaga is null then pmt9_orig_soft.max_late_pmt1 when pmt9_orig_soft.ultparcpaga = 1 then pmt9_orig_soft.max_late_pmt2 when pmt9_orig_soft.ultparcpaga = 2 then pmt9_orig_soft.max_late_pmt3 when pmt9_orig_soft.ultparcpaga = 3 then pmt9_orig_soft.max_late_pmt4 when pmt9_orig_soft.ultparcpaga = 4 then pmt9_orig_soft.max_late_pmt5 when pmt9_orig_soft.ultparcpaga = 5 then pmt9_orig_soft.max_late_pmt6 when pmt9_orig_soft.ultparcpaga = 6 then pmt9_orig_soft.max_late_pmt7 when pmt9_orig_soft.ultparcpaga = 7 then pmt9_orig_soft.max_late_pmt8 when pmt9_orig_soft.ultparcpaga = 8 then pmt9_orig_soft.max_late_pmt9 when pmt9_orig_soft.ultparcpaga >= 9 then 0 end) >= 90 then 1 else 0 end as collection_curr_over90_pmt9_orig_soft,
case when pmt6_orig_aggressive.loan_request_id is null then null when pmt6_orig_aggressive.max_late_pmt1 >= 30 or pmt6_orig_aggressive.max_late_pmt2 >= 30 or pmt6_orig_aggressive.max_late_pmt3 >= 30 or pmt6_orig_aggressive.max_late_pmt4 >= 30 or pmt6_orig_aggressive.max_late_pmt5 >= 30 or pmt6_orig_aggressive.max_late_pmt6 >= 30 then 1 else 0 end as collection_ever_over30_pmt6_orig_aggressive,
case when pmt6_orig_aggressive.loan_request_id is null then null when pmt6_orig_aggressive.max_late_pmt1 >= 60 or pmt6_orig_aggressive.max_late_pmt2 >= 60 or pmt6_orig_aggressive.max_late_pmt3 >= 60 or pmt6_orig_aggressive.max_late_pmt4 >= 60 or pmt6_orig_aggressive.max_late_pmt5 >= 60 or pmt6_orig_aggressive.max_late_pmt6 >= 60 then 1 else 0 end as collection_ever_over60_pmt6_orig_aggressive,
case when pmt6_orig_aggressive.loan_request_id is null then null when pmt6_orig_aggressive.max_late_pmt1 >= 90 or pmt6_orig_aggressive.max_late_pmt2 >= 90 or pmt6_orig_aggressive.max_late_pmt3 >= 90 or pmt6_orig_aggressive.max_late_pmt4 >= 90 or pmt6_orig_aggressive.max_late_pmt5 >= 90 or pmt6_orig_aggressive.max_late_pmt6 >= 90 then 1 else 0 end as collection_ever_over90_pmt6_orig_aggressive,
case when pmt6_orig_aggressive.loan_request_id is null then null when (case when pmt6_orig_aggressive.ultparcpaga is null then pmt6_orig_aggressive.max_late_pmt1 when pmt6_orig_aggressive.ultparcpaga = 1 then pmt6_orig_aggressive.max_late_pmt2 when pmt6_orig_aggressive.ultparcpaga = 2 then pmt6_orig_aggressive.max_late_pmt3 when pmt6_orig_aggressive.ultparcpaga = 3 then pmt6_orig_aggressive.max_late_pmt4 when pmt6_orig_aggressive.ultparcpaga = 4 then pmt6_orig_aggressive.max_late_pmt5 when pmt6_orig_aggressive.ultparcpaga = 5 then pmt6_orig_aggressive.max_late_pmt6 when pmt6_orig_aggressive.ultparcpaga >= 6 then 0 end) >= 30 then 1 else 0 end as collection_curr_over30_pmt6_orig_aggressive,
case when pmt6_orig_aggressive.loan_request_id is null then null when (case when pmt6_orig_aggressive.ultparcpaga is null then pmt6_orig_aggressive.max_late_pmt1 when pmt6_orig_aggressive.ultparcpaga = 1 then pmt6_orig_aggressive.max_late_pmt2 when pmt6_orig_aggressive.ultparcpaga = 2 then pmt6_orig_aggressive.max_late_pmt3 when pmt6_orig_aggressive.ultparcpaga = 3 then pmt6_orig_aggressive.max_late_pmt4 when pmt6_orig_aggressive.ultparcpaga = 4 then pmt6_orig_aggressive.max_late_pmt5 when pmt6_orig_aggressive.ultparcpaga = 5 then pmt6_orig_aggressive.max_late_pmt6 when pmt6_orig_aggressive.ultparcpaga >= 6 then 0 end) >= 60 then 1 else 0 end as collection_curr_over60_pmt6_orig_aggressive,
case when pmt6_orig_aggressive.loan_request_id is null then null when (case when pmt6_orig_aggressive.ultparcpaga is null then pmt6_orig_aggressive.max_late_pmt1 when pmt6_orig_aggressive.ultparcpaga = 1 then pmt6_orig_aggressive.max_late_pmt2 when pmt6_orig_aggressive.ultparcpaga = 2 then pmt6_orig_aggressive.max_late_pmt3 when pmt6_orig_aggressive.ultparcpaga = 3 then pmt6_orig_aggressive.max_late_pmt4 when pmt6_orig_aggressive.ultparcpaga = 4 then pmt6_orig_aggressive.max_late_pmt5 when pmt6_orig_aggressive.ultparcpaga = 5 then pmt6_orig_aggressive.max_late_pmt6 when pmt6_orig_aggressive.ultparcpaga >= 6 then 0 end) >= 90 then 1 else 0 end as collection_curr_over90_pmt6_orig_aggressive,
case when mob7_soft.loan_request_id is null then null when mob7_soft.max_late_pmt1 >= 30 or mob7_soft.max_late_pmt2 >= 30 or mob7_soft.max_late_pmt3 >= 30 or mob7_soft.max_late_pmt4 >= 30 or mob7_soft.max_late_pmt5 >= 30 or mob7_soft.max_late_pmt6 >= 30 then 1 else 0 end as collection_ever_over30_mob7_soft,
case when mob7_soft.loan_request_id is null then null when mob7_soft.max_late_pmt1 >= 60 or mob7_soft.max_late_pmt2 >= 60 or mob7_soft.max_late_pmt3 >= 60 or mob7_soft.max_late_pmt4 >= 60 or mob7_soft.max_late_pmt5 >= 60 or mob7_soft.max_late_pmt6 >= 60 then 1 else 0 end as collection_ever_over60_mob7_soft,
case when mob7_soft.loan_request_id is null then null when mob7_soft.max_late_pmt1 >= 90 or mob7_soft.max_late_pmt2 >= 90 or mob7_soft.max_late_pmt3 >= 90 or mob7_soft.max_late_pmt4 >= 90 or mob7_soft.max_late_pmt5 >= 90 or mob7_soft.max_late_pmt6 >= 90 then 1 else 0 end as collection_ever_over90_mob7_soft,
case when mob7_soft.loan_request_id is null then null when (case when mob7_soft.ultparcpaga is null then mob7_soft.max_late_pmt1 when mob7_soft.ultparcpaga = 1 then mob7_soft.max_late_pmt2 when mob7_soft.ultparcpaga = 2 then mob7_soft.max_late_pmt3 when mob7_soft.ultparcpaga = 3 then mob7_soft.max_late_pmt4 when mob7_soft.ultparcpaga = 4 then mob7_soft.max_late_pmt5 when mob7_soft.ultparcpaga = 5 then mob7_soft.max_late_pmt6 when mob7_soft.ultparcpaga >= 6 then 0 end) >= 30 then 1 else 0 end as collection_curr_over30_mob7_soft,
case when mob7_soft.loan_request_id is null then null when (case when mob7_soft.ultparcpaga is null then mob7_soft.max_late_pmt1 when mob7_soft.ultparcpaga = 1 then mob7_soft.max_late_pmt2 when mob7_soft.ultparcpaga = 2 then mob7_soft.max_late_pmt3 when mob7_soft.ultparcpaga = 3 then mob7_soft.max_late_pmt4 when mob7_soft.ultparcpaga = 4 then mob7_soft.max_late_pmt5 when mob7_soft.ultparcpaga = 5 then mob7_soft.max_late_pmt6 when mob7_soft.ultparcpaga >= 6 then 0 end) >= 60 then 1 else 0 end as collection_curr_over60_mob7_soft,
case when mob7_soft.loan_request_id is null then null when (case when mob7_soft.ultparcpaga is null then mob7_soft.max_late_pmt1 when mob7_soft.ultparcpaga = 1 then mob7_soft.max_late_pmt2 when mob7_soft.ultparcpaga = 2 then mob7_soft.max_late_pmt3 when mob7_soft.ultparcpaga = 3 then mob7_soft.max_late_pmt4 when mob7_soft.ultparcpaga = 4 then mob7_soft.max_late_pmt5 when mob7_soft.ultparcpaga = 5 then mob7_soft.max_late_pmt6 when mob7_soft.ultparcpaga >= 6 then 0 end) >= 90 then 1 else 0 end as collection_curr_over90_mob7_soft,
case when mob7_aggressive.loan_request_id is null then null when mob7_aggressive.max_late_pmt1 >= 30 or mob7_aggressive.max_late_pmt2 >= 30 or mob7_aggressive.max_late_pmt3 >= 30 or mob7_aggressive.max_late_pmt4 >= 30 or mob7_aggressive.max_late_pmt5 >= 30 or mob7_aggressive.max_late_pmt6 >= 30 then 1 else 0 end as collection_ever_over30_mob7_aggressive,
case when mob7_aggressive.loan_request_id is null then null when mob7_aggressive.max_late_pmt1 >= 60 or mob7_aggressive.max_late_pmt2 >= 60 or mob7_aggressive.max_late_pmt3 >= 60 or mob7_aggressive.max_late_pmt4 >= 60 or mob7_aggressive.max_late_pmt5 >= 60 or mob7_aggressive.max_late_pmt6 >= 60 then 1 else 0 end as collection_ever_over60_mob7_aggressive,
case when mob7_aggressive.loan_request_id is null then null when mob7_aggressive.max_late_pmt1 >= 90 or mob7_aggressive.max_late_pmt2 >= 90 or mob7_aggressive.max_late_pmt3 >= 90 or mob7_aggressive.max_late_pmt4 >= 90 or mob7_aggressive.max_late_pmt5 >= 90 or mob7_aggressive.max_late_pmt6 >= 90 then 1 else 0 end as collection_ever_over90_mob7_aggressive,
case when mob7_aggressive.loan_request_id is null then null when (case when mob7_aggressive.ultparcpaga is null then mob7_aggressive.max_late_pmt1 when mob7_aggressive.ultparcpaga = 1 then mob7_aggressive.max_late_pmt2 when mob7_aggressive.ultparcpaga = 2 then mob7_aggressive.max_late_pmt3 when mob7_aggressive.ultparcpaga = 3 then mob7_aggressive.max_late_pmt4 when mob7_aggressive.ultparcpaga = 4 then mob7_aggressive.max_late_pmt5 when mob7_aggressive.ultparcpaga = 5 then mob7_aggressive.max_late_pmt6 when mob7_aggressive.ultparcpaga >= 6 then 0 end) >= 30 then 1 else 0 end as collection_curr_over30_mob7_aggressive,
case when mob7_aggressive.loan_request_id is null then null when (case when mob7_aggressive.ultparcpaga is null then mob7_aggressive.max_late_pmt1 when mob7_aggressive.ultparcpaga = 1 then mob7_aggressive.max_late_pmt2 when mob7_aggressive.ultparcpaga = 2 then mob7_aggressive.max_late_pmt3 when mob7_aggressive.ultparcpaga = 3 then mob7_aggressive.max_late_pmt4 when mob7_aggressive.ultparcpaga = 4 then mob7_aggressive.max_late_pmt5 when mob7_aggressive.ultparcpaga = 5 then mob7_aggressive.max_late_pmt6 when mob7_aggressive.ultparcpaga >= 6 then 0 end) >= 60 then 1 else 0 end as collection_curr_over60_mob7_aggressive,
case when mob7_aggressive.loan_request_id is null then null when (case when mob7_aggressive.ultparcpaga is null then mob7_aggressive.max_late_pmt1 when mob7_aggressive.ultparcpaga = 1 then mob7_aggressive.max_late_pmt2 when mob7_aggressive.ultparcpaga = 2 then mob7_aggressive.max_late_pmt3 when mob7_aggressive.ultparcpaga = 3 then mob7_aggressive.max_late_pmt4 when mob7_aggressive.ultparcpaga = 4 then mob7_aggressive.max_late_pmt5 when mob7_aggressive.ultparcpaga = 5 then mob7_aggressive.max_late_pmt6 when mob7_aggressive.ultparcpaga >= 6 then 0 end) >= 90 then 1 else 0 end as collection_curr_over90_mob7_aggressive,
case when mob6_aggressive.loan_request_id is null then null when mob6_aggressive.max_late_pmt1 >= 30 or mob6_aggressive.max_late_pmt2 >= 30 or mob6_aggressive.max_late_pmt3 >= 30 or mob6_aggressive.max_late_pmt4 >= 30 or mob6_aggressive.max_late_pmt5 >= 30 or mob6_aggressive.max_late_pmt6 >= 30 then 1 else 0 end as collection_ever_over30_mob6_aggressive,
case when mob6_aggressive.loan_request_id is null then null when mob6_aggressive.max_late_pmt1 >= 60 or mob6_aggressive.max_late_pmt2 >= 60 or mob6_aggressive.max_late_pmt3 >= 60 or mob6_aggressive.max_late_pmt4 >= 60 or mob6_aggressive.max_late_pmt5 >= 60 or mob6_aggressive.max_late_pmt6 >= 60 then 1 else 0 end as collection_ever_over60_mob6_aggressive,
case when mob6_aggressive.loan_request_id is null then null when mob6_aggressive.max_late_pmt1 >= 90 or mob6_aggressive.max_late_pmt2 >= 90 or mob6_aggressive.max_late_pmt3 >= 90 or mob6_aggressive.max_late_pmt4 >= 90 or mob6_aggressive.max_late_pmt5 >= 90 or mob6_aggressive.max_late_pmt6 >= 90 then 1 else 0 end as collection_ever_over90_mob6_aggressive,
case when mob6_aggressive.loan_request_id is null then null when (case when mob6_aggressive.ultparcpaga is null then mob6_aggressive.max_late_pmt1 when mob6_aggressive.ultparcpaga = 1 then mob6_aggressive.max_late_pmt2 when mob6_aggressive.ultparcpaga = 2 then mob6_aggressive.max_late_pmt3 when mob6_aggressive.ultparcpaga = 3 then mob6_aggressive.max_late_pmt4 when mob6_aggressive.ultparcpaga = 4 then mob6_aggressive.max_late_pmt5 when mob6_aggressive.ultparcpaga = 5 then mob6_aggressive.max_late_pmt6 when mob6_aggressive.ultparcpaga >= 6 then 0 end) >= 30 then 1 else 0 end as collection_curr_over30_mob6_aggressive,
case when mob6_aggressive.loan_request_id is null then null when (case when mob6_aggressive.ultparcpaga is null then mob6_aggressive.max_late_pmt1 when mob6_aggressive.ultparcpaga = 1 then mob6_aggressive.max_late_pmt2 when mob6_aggressive.ultparcpaga = 2 then mob6_aggressive.max_late_pmt3 when mob6_aggressive.ultparcpaga = 3 then mob6_aggressive.max_late_pmt4 when mob6_aggressive.ultparcpaga = 4 then mob6_aggressive.max_late_pmt5 when mob6_aggressive.ultparcpaga = 5 then mob6_aggressive.max_late_pmt6 when mob6_aggressive.ultparcpaga >= 6 then 0 end) >= 60 then 1 else 0 end as collection_curr_over60_mob6_aggressive,
case when mob6_aggressive.loan_request_id is null then null when (case when mob6_aggressive.ultparcpaga is null then mob6_aggressive.max_late_pmt1 when mob6_aggressive.ultparcpaga = 1 then mob6_aggressive.max_late_pmt2 when mob6_aggressive.ultparcpaga = 2 then mob6_aggressive.max_late_pmt3 when mob6_aggressive.ultparcpaga = 3 then mob6_aggressive.max_late_pmt4 when mob6_aggressive.ultparcpaga = 4 then mob6_aggressive.max_late_pmt5 when mob6_aggressive.ultparcpaga = 5 then mob6_aggressive.max_late_pmt6 when mob6_aggressive.ultparcpaga >= 6 then 0 end) >= 90 then 1 else 0 end as collection_curr_over90_mob6_aggressive,
case when pmt5_orig_aggressive.loan_request_id is null then null when pmt5_orig_aggressive.max_late_pmt1 >= 30 or pmt5_orig_aggressive.max_late_pmt2 >= 30 or pmt5_orig_aggressive.max_late_pmt3 >= 30 or pmt5_orig_aggressive.max_late_pmt4 >= 30 or pmt5_orig_aggressive.max_late_pmt5 >= 30 then 1 else 0 end as collection_ever_over30_pmt5_orig_aggressive,
case when pmt5_orig_aggressive.loan_request_id is null then null when pmt5_orig_aggressive.max_late_pmt1 >= 60 or pmt5_orig_aggressive.max_late_pmt2 >= 60 or pmt5_orig_aggressive.max_late_pmt3 >= 60 or pmt5_orig_aggressive.max_late_pmt4 >= 60 or pmt5_orig_aggressive.max_late_pmt5 >= 60 then 1 else 0 end as collection_ever_over60_pmt5_orig_aggressive,
case when pmt5_orig_aggressive.loan_request_id is null then null when pmt5_orig_aggressive.max_late_pmt1 >= 90 or pmt5_orig_aggressive.max_late_pmt2 >= 90 or pmt5_orig_aggressive.max_late_pmt3 >= 90 or pmt5_orig_aggressive.max_late_pmt4 >= 90 or pmt5_orig_aggressive.max_late_pmt5 >= 90 then 1 else 0 end as collection_ever_over90_pmt5_orig_aggressive,
case when pmt5_orig_aggressive.loan_request_id is null then null when (case when pmt5_orig_aggressive.ultparcpaga is null then pmt5_orig_aggressive.max_late_pmt1 when pmt5_orig_aggressive.ultparcpaga = 1 then pmt5_orig_aggressive.max_late_pmt2 when pmt5_orig_aggressive.ultparcpaga = 2 then pmt5_orig_aggressive.max_late_pmt3 when pmt5_orig_aggressive.ultparcpaga = 3 then pmt5_orig_aggressive.max_late_pmt4 when pmt5_orig_aggressive.ultparcpaga = 4 then pmt5_orig_aggressive.max_late_pmt5 when pmt5_orig_aggressive.ultparcpaga = 5 then 0 end) >= 30 then 1 else 0 end as collection_curr_over30_pmt5_orig_aggressive,
case when pmt5_orig_aggressive.loan_request_id is null then null when (case when pmt5_orig_aggressive.ultparcpaga is null then pmt5_orig_aggressive.max_late_pmt1 when pmt5_orig_aggressive.ultparcpaga = 1 then pmt5_orig_aggressive.max_late_pmt2 when pmt5_orig_aggressive.ultparcpaga = 2 then pmt5_orig_aggressive.max_late_pmt3 when pmt5_orig_aggressive.ultparcpaga = 3 then pmt5_orig_aggressive.max_late_pmt4 when pmt5_orig_aggressive.ultparcpaga = 4 then pmt5_orig_aggressive.max_late_pmt5 when pmt5_orig_aggressive.ultparcpaga = 5 then 0 end) >= 60 then 1 else 0 end as collection_curr_over60_pmt5_orig_aggressive,
case when pmt5_orig_aggressive.loan_request_id is null then null when (case when pmt5_orig_aggressive.ultparcpaga is null then pmt5_orig_aggressive.max_late_pmt1 when pmt5_orig_aggressive.ultparcpaga = 1 then pmt5_orig_aggressive.max_late_pmt2 when pmt5_orig_aggressive.ultparcpaga = 2 then pmt5_orig_aggressive.max_late_pmt3 when pmt5_orig_aggressive.ultparcpaga = 3 then pmt5_orig_aggressive.max_late_pmt4 when pmt5_orig_aggressive.ultparcpaga = 4 then pmt5_orig_aggressive.max_late_pmt5 when pmt5_orig_aggressive.ultparcpaga = 5 then 0 end) >= 90 then 1 else 0 end as collection_curr_over90_pmt5_orig_aggressive,
--------------------------SCR HISTORICO PJ
--Divida longo prazo
case when tem_scr = 0 then null else case when Ever_long_term_debt_PJ > 0 then 1 else 0 end end as scr_Has_Debt_History_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_Curr*1000 end as scr_curr_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_1M*1000 end as scr_2M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_2M*1000 end as scr_3M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_3M*1000 end as scr_4M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_4M*1000 end as scr_5M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_5M*1000 end as scr_6M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_6M*1000 end as scr_7M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_7M*1000 end as scr_8M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_8M*1000 end as scr_9M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_9M*1000 end as scr_10M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_10M*1000 end as scr_11M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_11M*1000 end as scr_12M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Ever_long_term_debt_PJ*1000 end as scr_ever_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Max_Long_Term_Debt_PJ*1000 end as scr_max_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_Curr/month_revenue/12*1000 end as scr_long_term_debt_PJ_over_revenue,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else (Long_Term_Debt_PJ_Curr - Long_Term_Debt_PJ_5M)/month_revenue/12*1000 end as scr_var_abs_6M_long_term_debt_PJ_over_revenue,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else (Long_Term_Debt_PJ_Curr - Long_Term_Debt_PJ_11M)/month_revenue/12*1000 end as scr_var_abs_12M_long_term_debt_PJ_over_revenue,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 when Long_Term_Debt_PJ_Curr = 0 then 0 when Long_Term_Debt_PJ_5M = 0 then 2019 else Long_Term_Debt_PJ_Curr / Long_Term_Debt_PJ_5M-1 end as scr_var_rel_6M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 when Long_Term_Debt_PJ_Curr = 0 then 0 when Long_Term_Debt_PJ_11M = 0 then 2019 else Long_Term_Debt_PJ_Curr / Long_Term_Debt_PJ_11M-1 end as scr_var_rel_12M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Qtd_meses_escopo_pj end as scr_number_of_months_in_report_pj,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Meses_Aumento_DividaPJ end as scr_Months_Rise_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Meses_Aumento_DividaPJ/(case when Qtd_meses_escopo_pj <= 1 then 1 else Qtd_meses_escopo_pj-1 end) end as scr_Months_Rise_long_term_debt_PJ_over_total_months,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Meses_Reducao_DividaPJ end as scr_Months_Dive_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Meses_Reducao_DividaPJ/(case when Qtd_meses_escopo_pj <= 1 then 1 else Qtd_meses_escopo_pj-1 end) end as scr_Months_Dive_long_term_debt_PJ_over_total_months,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else months_sth_pj end as scr_month_consecutive_dive_long_term_debt_pj,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else months_sth_pj/(case when Qtd_meses_escopo_pj <= 1 then 1 else Qtd_meses_escopo_pj-1 end) end as scr_month_consecutive_dive_long_term_debt_pj_over_total_months,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else months_hth_pj end as scr_month_consecutive_rise_long_term_debt_pj,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else months_hth_pj/(case when Qtd_meses_escopo_pj <= 1 then 1 else Qtd_meses_escopo_pj-1 end) end as scr_month_consecutive_rise_long_term_debt_pj_over_total_months,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 when Max_Long_Term_Debt_PJ = 0 then 0 else Long_Term_Debt_PJ_Curr / Max_Long_Term_Debt_PJ end as scr_curr_over_max_long_term_debt_pj,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else case when Long_Term_Debt_PJ_Curr = Max_Long_Term_Debt_PJ then 1 else 0 end end as scr_All_TimeHigh_long_term_debt_PJ,
--Vencido
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Overdue_PJ_Curr*1000 end as scr_curr_overdue_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Max_Overdue_PJ*1000 end as scr_max_overdue_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Months_Overdue_PJ end as scr_Months_Overdue_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Months_Overdue_PJ/Qtd_meses_escopo_pj end as scr_Months_Overdue_PJ_over_total_months,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then -1 when Months_Since_Last_Overdue_PJ is null then - 1 else Months_Since_Last_Overdue_PJ end as scr_Months_since_last_Overdue_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Max_Overdue_PJ/month_revenue/12*1000 end as scr_Max_Overdue_PJ_over_Revenue,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Overdue_PJ_Curr/month_revenue/12*1000 end as scr_Curr_Overdue_PJ_over_Revenue,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 when Overdue_PJ_Curr = 0 then 0 when Long_Term_Debt_PJ_Curr = 0 then 2019 else Overdue_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_Curr_Overdue_PJ_long_term_debt,
--Prejuizo
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Default_PJ_Curr*1000 end as scr_curr_default_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Max_Default_PJ*1000 end as scr_max_default_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Months_Default_PJ end as scr_Months_Default_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Months_Default_PJ/Qtd_meses_escopo_pj end as scr_Months_Default_PJ_over_total_months,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then -1 when Months_Since_Last_Default_PJ is null then - 1 else Months_Since_Last_Default_PJ end as scr_Months_since_last_Default_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Max_Default_PJ/month_revenue/12*1000 end as scr_Max_Default_PJ_over_revenue,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Default_PJ_Curr/month_revenue/12*1000 end as scr_Curr_Default_PJ_over_revenue,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 when Default_PJ_Curr = 0 then 0 when Long_Term_Debt_PJ_Curr = 0 then 2019 else Default_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_Curr_Default_PJ_over_long_term_debt,
--Saldo
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Saldo_Amort_DividaPJ * 1000 end as scr_long_term_debt_amortization_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 when Saldo_Amort_DividaPJ = 0 then 0 else Saldo_Amort_DividaPJ/Meses_Reducao_DividaPJ/month_revenue/12*1000 end as scr_Avg_Amortization_LTDebt_PJ_over_revenue,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 when Saldo_Amort_DividaPJ = 0 then 0 else Saldo_Amort_DividaPJ/Meses_Reducao_DividaPJ/((@li.pmt))*1000 end as scr_Avg_Amortization_LTDebt_PJ_over_loan_PMT,
--Limite
case when tem_scr = 0 then null when Ever_Lim_Cred_pj is null then 0 else Ever_Lim_Cred_pj * 1000 end as scr_ever_credit_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_Curr * 1000 end as scr_curr_credit_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_1M * 1000 end as scr_2M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_2M * 1000 end as scr_3M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_3M * 1000 end as scr_4M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_4M * 1000 end as scr_5M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_5M * 1000 end as scr_6M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_6M * 1000 end as scr_7M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_7M * 1000 end as scr_8M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_8M * 1000 end as scr_9M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_9M * 1000 end as scr_10M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_10M * 1000 end as scr_11M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_11M * 1000 end as scr_12M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_Curr / month_revenue / 12 * 1000 end as scr_Curr_Credit_Limit_pj_over_revenue,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_Curr / case when amount_requested < 1000 then o.max_value else amount_requested end*1000 end as scr_Curr_Credit_Limit_pj_over_application_value,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_Curr / li.total_payment * 1000 end as scr_Curr_Credit_Limit_pj_over_loan_value,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 when Lim_Cred_pj_Curr = 0 then 0 when Long_Term_Debt_pj_Curr = 0 then 2019 else Lim_Cred_pj_Curr/Long_Term_Debt_pj_Curr end as scr_Curr_Credit_Limit_pj_over_long_term_debt,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Max_Lim_Cred_pj * 1000 end as scr_max_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 when Max_Lim_Cred_pj = 0 then 0 else Lim_Cred_pj_Curr / Max_Lim_Cred_pj end as scr_curr_over_max_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else case when Lim_Cred_pj_Curr = Max_Lim_Cred_pj then 1 else 0 end end as scr_All_TimeHigh_credit_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else (Lim_Cred_pj_Curr - Long_Term_Debt_pj_5M) / month_revenue / 12 * 1000 end as scr_var_abs_6M_cred_limit_pj_over_revenue,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else (Lim_Cred_pj_Curr - Long_Term_Debt_pj_11M)/month_revenue / 12 * 1000 end as scr_var_abs_12M_cred_limit_pj_over_revenue,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 when Long_Term_Debt_pj_Curr = 0 then 0 when Long_Term_Debt_pj_5M = 0 then 2019 else Long_Term_Debt_pj_Curr / Long_Term_Debt_pj_5M - 1 end as scr_var_rel_6M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 when Long_Term_Debt_pj_Curr = 0 then 0 when Long_Term_Debt_pj_11M = 0 then 2019 else Long_Term_Debt_pj_Curr / Long_Term_Debt_pj_11M - 1 end as scr_var_rel_12M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Meses_Aumento_Lim_Cred_pj end as scr_Months_Rise_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Meses_Aumento_Lim_Cred_pj / (case when Qtd_meses_escopo_pj <= 1 then 1 else Qtd_meses_escopo_pj - 1 end) end as scr_Months_Rise_cred_limit_pj_over_total_months,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Meses_Reducao_Lim_Cred_pj end as scr_Months_Dive_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Meses_Reducao_Lim_Cred_pj / (case when Qtd_meses_escopo_pj <= 1 then 1 else Qtd_meses_escopo_pj - 1 end) end as scr_Months_Dive_cred_limit_pj_over_total_months,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else months_sth_lim_cred_pj end as scr_month_consecutive_dive_cred_limit_debt_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else months_sth_lim_cred_pj / (case when Qtd_meses_escopo_pj <= 1 then 1 else Qtd_meses_escopo_pj - 1 end) end as scr_month_consecutive_dive_cred_limit_pj_over_total_months,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else months_hth_lim_cred_pj end as scr_month_consecutive_rise_cred_limit_debt_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else months_hth_lim_cred_pj / (case when Qtd_meses_escopo_pj <= 1 then 1 else Qtd_meses_escopo_pj - 1 end) end as scr_month_consecutive_rise_cred_limit_pj_over_total_months,
--Operacoes
case when tem_scr = 0 then null else case when Num_Ops_PJ is null then 0 else Num_Ops_PJ end end as scr_Num_Fin_Ops_PJ,
case when tem_scr = 0 then null else case when Num_FIs_PJ is null then 0 else Num_FIs_PJ end end as scr_Num_Fin_Inst_PJ,
case when tem_scr = 0 then null else case when First_Relation_FI_PJ is null then 0 else First_Relation_FI_PJ end end as scr_First_Relation_Fin_Inst_PJ,
--------------------------SCR HISTORICO PF
--Divida longo prazo
case when tem_scr_pf = 0 then null else case when Ever_long_term_debt_PF > 0 then 1 else 0 end end as scr_Has_Debt_History_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_Curr * 1000 end as scr_curr_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_1M * 1000 end as scr_2M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_2M * 1000 end as scr_3M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_3M * 1000 end as scr_4M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_4M * 1000 end as scr_5M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_5M * 1000 end as scr_6M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_6M * 1000 end as scr_7M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_7M * 1000 end as scr_8M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_8M * 1000 end as scr_9M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_9M * 1000 end as scr_10M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_10M * 1000 end as scr_11M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_11M * 1000 end as scr_12M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Ever_long_term_debt_PF * 1000 end as scr_ever_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Max_Long_Term_Debt_PF * 1000 end as scr_max_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_Curr / month_revenue / 12 * 1000 end as scr_long_term_debt_PF_over_revenue,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else (Long_Term_Debt_PF_Curr - Long_Term_Debt_PF_5M) / month_revenue / 12 * 1000 end as scr_var_abs_6M_long_term_debt_PF_over_revenue,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else (Long_Term_Debt_PF_Curr - Long_Term_Debt_PF_11M) / month_revenue / 12 * 1000 end as scr_var_abs_12M_long_term_debt_PF_over_revenue,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 when Long_Term_Debt_PF_Curr = 0 then 0 when Long_Term_Debt_PF_5M = 0 then 2019 else Long_Term_Debt_PF_Curr / Long_Term_Debt_PF_5M - 1 end as scr_var_rel_6M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 when Long_Term_Debt_PF_Curr = 0 then 0 when Long_Term_Debt_PF_11M = 0 then 2019 else Long_Term_Debt_PF_Curr / Long_Term_Debt_PF_11M - 1 end as scr_var_rel_12M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Qtd_meses_escopo_PF end as scr_number_of_months_in_report_pf,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Meses_Aumento_DividaPF end as scr_Months_Rise_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Meses_Aumento_DividaPF / (case when Qtd_meses_escopo_PF <= 1 then 1 else Qtd_meses_escopo_PF - 1 end) end as scr_Months_Rise_long_term_debt_PF_over_total_months,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Meses_Reducao_DividaPF end as scr_Months_Dive_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Meses_Reducao_DividaPF / (case when Qtd_meses_escopo_PF <= 1 then 1 else Qtd_meses_escopo_PF - 1 end) end as scr_Months_Dive_long_term_debt_PF_over_total_months,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else months_sth_PF end as scr_month_consecutive_dive_long_term_debt_pf,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else months_sth_PF / (case when Qtd_meses_escopo_PF <= 1 then 1 else Qtd_meses_escopo_PF - 1 end) end as scr_month_consecutive_dive_long_term_debt_pf_over_total_months,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else months_hth_PF end as scr_month_consecutive_rise_long_term_debt_pf,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else months_hth_PF / (case when Qtd_meses_escopo_PF <= 1 then 1 else Qtd_meses_escopo_PF - 1 end) end as scr_month_consecutive_rise_long_term_debt_pf_over_total_months,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 when Max_Long_Term_Debt_PF = 0 then 0 else Long_Term_Debt_PF_Curr / Max_Long_Term_Debt_PF end as scr_curr_over_max_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else case when Long_Term_Debt_PF_Curr = Max_Long_Term_Debt_PF then 1 else 0 end end as scr_All_TimeHigh_long_term_debt_PF,
--Vencido
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Overdue_PF_Curr * 1000 end as scr_curr_overdue_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Max_Overdue_PF * 1000 end as scr_max_overdue_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Months_Overdue_PF end as scr_Months_Overdue_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else (Months_Overdue_PF / Qtd_meses_escopo_PF)::float end as scr_Months_Overdue_PF_over_total_months,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then -1 when Months_Since_Last_Overdue_PF is null then - 1 else Months_Since_Last_Overdue_PF end as scr_Months_since_last_Overdue_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Max_Overdue_PF / month_revenue / 12 * 1000 end as scr_Max_Overdue_PF_over_Revenue,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Overdue_PF_Curr / month_revenue / 12 * 1000 end as scr_Curr_Overdue_PF_over_Revenue,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 when Overdue_PF_Curr = 0 then 0 when Long_Term_Debt_PF_Curr = 0 then 2019 else Overdue_PF_Curr/Long_Term_Debt_PF_Curr end as scr_Curr_Overdue_PF_long_term_debt,
--Prejuizo
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Default_PF_Curr * 1000 end as scr_curr_default_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Max_Default_PF * 1000 end as scr_max_default_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Months_Default_PF end as scr_Months_Default_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else (Months_Default_PF / Qtd_meses_escopo_PF)::float end as scr_Months_Default_PF_over_total_months,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then -1 when Months_Since_Last_Default_PF is null then - 1 else Months_Since_Last_Default_PF end as scr_Months_since_last_Default_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Max_Default_PF / month_revenue / 12 * 1000 end as scr_Max_Default_PF_over_revenue,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Default_PF_Curr / month_revenue / 12 * 1000 end as scr_Curr_Default_PF_over_revenue,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 when Default_PF_Curr = 0 then 0 when Long_Term_Debt_PF_Curr = 0 then 2019 else Default_PF_Curr/Long_Term_Debt_PF_Curr end as scr_Curr_Default_PF_over_long_term_debt,
--Saldo
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Saldo_Amort_DividaPF * 1000 end as scr_long_term_debt_amortization_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 when Saldo_Amort_DividaPF = 0 then 0 else Saldo_Amort_DividaPF / Meses_Reducao_DividaPF / month_revenue / 12 * 1000 end as scr_Avg_Amortization_LTDebt_PF_over_revenue,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 when Saldo_Amort_DividaPF = 0 then 0 else Saldo_Amort_DividaPF / Meses_Reducao_DividaPF / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_LTDebt_PF_over_loan_PMT,
--Limite
case when tem_scr_pf = 0 then null when Ever_Lim_Cred_PF is null then 0 else Ever_Lim_Cred_PF * 1000 end as scr_ever_credit_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_Curr * 1000 end as scr_curr_credit_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_1M * 1000 end as scr_2M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_2M * 1000 end as scr_3M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_3M * 1000 end as scr_4M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_4M * 1000 end as scr_5M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_5M * 1000 end as scr_6M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_6M * 1000 end as scr_7M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_7M * 1000 end as scr_8M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_8M * 1000 end as scr_9M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_9M * 1000 end as scr_10M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_10M * 1000 end as scr_11M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_11M * 1000 end as scr_12M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_Curr / month_revenue / 12 * 1000 end as scr_Curr_Credit_Limit_PF_over_revenue,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_Curr / case when amount_requested < 1000 then o.max_value else amount_requested end * 1000 end as scr_Curr_Credit_Limit_PF_over_application_value,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_Curr / li.total_payment * 1000 end as scr_Curr_Credit_Limit_PF_over_loan_value,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 when Lim_Cred_PF_Curr = 0 then 0 when Long_Term_Debt_PF_Curr = 0 then 2019 else Lim_Cred_PF_Curr/Long_Term_Debt_PF_Curr end as scr_Curr_Credit_Limit_PF_over_long_term_debt,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Max_Lim_Cred_PF * 1000 end as scr_max_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 when Max_Lim_Cred_PF = 0 then 0 else Lim_Cred_PF_Curr / Max_Lim_Cred_PF end as scr_curr_over_max_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else case when Lim_Cred_PF_Curr = Max_Lim_Cred_PF then 1 else 0 end end as scr_All_TimeHigh_credit_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else (Lim_Cred_PF_Curr - Long_Term_Debt_PF_5M)/month_revenue/ 12 * 1000 end as scr_var_abs_6M_cred_limit_PF_over_revenue,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else (Lim_Cred_PF_Curr - Long_Term_Debt_PF_11M)/month_revenue / 12 * 1000 end as scr_var_abs_12M_cred_limit_PF_over_revenue,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 when Long_Term_Debt_PF_Curr = 0 then 0 when Long_Term_Debt_PF_5M = 0 then 2019 else Long_Term_Debt_PF_Curr / Long_Term_Debt_PF_5M - 1 end as scr_var_rel_6M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 when Long_Term_Debt_PF_Curr = 0 then 0 when Long_Term_Debt_PF_11M = 0 then 2019 else Long_Term_Debt_PF_Curr / Long_Term_Debt_PF_11M - 1 end as scr_var_rel_12M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Meses_Aumento_Lim_Cred_PF end as scr_Months_Rise_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Meses_Aumento_Lim_Cred_PF / (case when Qtd_meses_escopo_PF <= 1 then 1 else Qtd_meses_escopo_PF - 1 end) end as scr_Months_Rise_cred_limit_PF_over_total_months,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Meses_Reducao_Lim_Cred_PF end as scr_Months_Dive_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Meses_Reducao_Lim_Cred_PF / (case when Qtd_meses_escopo_PF <= 1 then 1 else Qtd_meses_escopo_PF - 1 end) end as scr_Months_Dive_cred_limit_PF_over_total_months,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else months_sth_lim_cred_PF end as scr_month_consecutive_dive_cred_limit_debt_pf,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else months_sth_lim_cred_PF / (case when Qtd_meses_escopo_PF <= 1 then 1 else Qtd_meses_escopo_PF - 1 end) end as scr_month_consecutive_dive_cred_limit_pf_over_total_months,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else months_hth_lim_cred_PF end as scr_month_consecutive_rise_cred_limit_debt_pf,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else months_hth_lim_cred_PF / (case when Qtd_meses_escopo_PF <= 1 then 1 else Qtd_meses_escopo_PF - 1 end) end as scr_month_consecutive_rise_cred_limit_pf_over_total_months,
--Operacoes
case when tem_scr_pf = 0 then null else case when Num_Ops_PF is null then 0 else Num_Ops_PF end end as scr_Num_Fin_Ops_PF,
case when tem_scr_pf = 0 then null else case when Num_FIs_PF is null then 0 else Num_FIs_PF end end as scr_Num_Fin_Inst_PF,
case when tem_scr_pf = 0 then null else case when First_Relation_FI_PF is null then 0 else First_Relation_FI_PF end end as scr_First_Relation_Fin_Inst_PF,
--------------------------SCR DIREITOS CREDITORIOS
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Ever_invoice_disc * 1000 end as scr_ever_invoice_disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_Curr * 1000 end as scr_curr_invoice_disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_1M * 1000 end as scr_2M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_2M * 1000 end as scr_3M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_3M * 1000 end as scr_4M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_4M * 1000 end as scr_5M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_5M * 1000 end as scr_6M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_6M * 1000 end as scr_7M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_7M * 1000 end as scr_8M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_8M * 1000 end as scr_9M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_9M * 1000 end as scr_10M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_10M * 1000 end as scr_11M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_11M * 1000 end as scr_12M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_Curr / month_revenue / 12 * 1000 end as scr_Curr_InvoiceDisc_PJ_over_revenue,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 when Invoice_Disc_PJ_Curr = 0 then 0 when Long_Term_Debt_PJ_Curr = 0 then 2019 when Invoice_Disc_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 else Invoice_Disc_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_Curr_invoice_disc_debt_over_long_term_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else max_invoice_disc * 1000 end as scr_max_invoice_disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 when max_invoice_disc = 0 then 0 else Invoice_Disc_PJ_Curr / max_invoice_disc end as scr_curr_over_max_invoice_disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else case when Invoice_Disc_PJ_Curr = max_invoice_disc then 1 else 0 end end as scr_All_TimeHigh_invoice_disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else (Invoice_Disc_PJ_Curr - Invoice_Disc_PJ_5M) / month_revenue / 12 * 1000 end as scr_var_abs_6M_invoice_disc_debt_over_revenue,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else (Invoice_Disc_PJ_Curr - Invoice_Disc_PJ_11M) / month_revenue / 12 * 1000 end as scr_var_abs_12M_invoice_disc_debt_over_revenue,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 when Invoice_Disc_PJ_Curr = 0 then 0 when Invoice_Disc_PJ_5M = 0 then 2019 else Invoice_Disc_PJ_Curr / Invoice_Disc_PJ_5M - 1 end as scr_var_rel_6M_invoice_disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 when Invoice_Disc_PJ_Curr = 0 then 0 when Invoice_Disc_PJ_11M = 0 then 2019 else Invoice_Disc_PJ_Curr / Invoice_Disc_PJ_11M - 1 end as scr_var_rel_12M_invoice_disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Meses_Aumento_invoice_disc end as scr_Months_Rise_Invoice_Disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Meses_Aumento_invoice_disc / (case when qtd_meses_invoice_disc <= 1 then 1 else qtd_meses_invoice_disc - 1 end) end as scr_Months_Rise_Invoice_Disc_debt_over_total_months,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Meses_Reducao_invoice_disc end as scr_Months_Dive_Invoice_Disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Meses_Reducao_invoice_disc / (case when qtd_meses_invoice_disc <= 1 then 1 else qtd_meses_invoice_disc - 1 end) end as scr_Months_Dive_Invoice_Disc_debt_over_total_months,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else months_sth_invoice_disc end as scr_month_consecutive_dive_invoice_disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else months_sth_invoice_disc / (case when qtd_meses_invoice_disc <= 1 then 1 else qtd_meses_invoice_disc - 1 end) end as scr_month_consecutive_dive_invoice_disc_debt_over_total_months,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else months_hth_invoice_disc end as scr_month_consecutive_rise_invoice_disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else months_hth_invoice_disc / (case when qtd_meses_invoice_disc <= 1 then 1 else qtd_meses_invoice_disc - 1 end) end as scr_month_consecutive_rise_invoice_disc_debt_over_total_months,
--------------------------SCR EMPRESTIMOS ROTATIVO
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else Ever_overdraft * 1000 end as scr_ever_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_Curr*1000 end as scr_curr_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_1M * 1000 end as scr_2M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_2M * 1000 end as scr_3M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_3M * 1000 end as scr_4M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_4M * 1000 end as scr_5M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_5M * 1000 end as scr_6M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_6M * 1000 end as scr_7M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_7M * 1000 end as scr_8M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_8M * 1000 end as scr_9M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_9M * 1000 end as scr_10M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_10M * 1000 end as scr_11M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_11M * 1000 end as scr_12M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_Curr / month_revenue / 12 * 1000 end as scr_Curr_overdraft_debt_PJ_over_revenue,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 when overdraft_PJ_Curr = 0 then 0 when Long_Term_Debt_PJ_Curr = 0 then 2019 when overdraft_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 else overdraft_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_Curr_overdraft_debt_over_long_term_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 when overdraft_PJ_Curr = 0 then 0 when divida_curto_prazo = 0 then 2019 else overdraft_PJ_Curr/divida_curto_prazo end as scr_Curr_overdraft_debt_over_short_term_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else max_overdraft * 1000 end as scr_max_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 when max_overdraft = 0 then 0 else overdraft_PJ_Curr / max_overdraft end as scr_curr_over_max_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else case when overdraft_PJ_Curr = max_overdraft then 1 else 0 end end as scr_All_TimeHigh_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else (overdraft_PJ_Curr - overdraft_PJ_5M)/month_revenue / 12 * 1000 end as scr_var_abs_6M_overdraft_debt_over_revenue,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else (overdraft_PJ_Curr - overdraft_PJ_11M)/month_revenue / 12 * 1000 end as scr_var_abs_12M_overdraft_debt_over_revenue,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 when overdraft_PJ_Curr  = 0 then 0 when overdraft_PJ_5M = 0 then 2019 else overdraft_PJ_Curr / overdraft_PJ_5M - 1 end as scr_var_rel_6M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 when overdraft_PJ_Curr  = 0 then 0 when overdraft_PJ_11M = 0 then 2019 else overdraft_PJ_Curr / overdraft_PJ_11M - 1 end as scr_var_rel_12M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else Meses_Aumento_overdraft end as scr_Months_Rise_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else Meses_Aumento_overdraft / (case when qtd_meses_overdraft <= 1 then 1 else qtd_meses_overdraft-1 end) end as scr_Months_Rise_overdraft_debt_over_total_months,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else Meses_Reducao_overdraft end as scr_Months_Dive_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else Meses_Reducao_overdraft / (case when qtd_meses_overdraft <= 1 then 1 else qtd_meses_overdraft-1 end) end as scr_Months_Dive_overdraft_debt_over_total_months,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else months_sth_overdraft end as scr_month_consecutive_dive_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else months_sth_overdraft / (case when qtd_meses_overdraft <= 1 then 1 else qtd_meses_overdraft-1 end) end as scr_month_consecutive_dive_overdraft_debt_over_total_months,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else months_hth_overdraft end as scr_month_consecutive_rise_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else months_hth_overdraft / (case when qtd_meses_overdraft <= 1 then 1 else qtd_meses_overdraft-1 end) end as scr_month_consecutive_rise_overdraft_debt_over_total_months,
--------------------------SCR PJ DIVIDA CONCORRENTE
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else divida_curto_prazo end as scr_short_term_debt_PJ,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else Ever_competitive_debt * 1000 end as scr_ever_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_Curr * 1000 end as scr_curr_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_1M * 1000 end as scr_2M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_2M * 1000 end as scr_3M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_3M * 1000 end as scr_4M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_4M * 1000 end as scr_5M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_5M * 1000 end as scr_6M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_6M * 1000 end as scr_7M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_7M * 1000 end as scr_8M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_8M * 1000 end as scr_9M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_9M * 1000 end as scr_10M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_10M * 1000 end as scr_11M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_11M * 1000 end as scr_12M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_Curr / month_revenue / 12 * 1000 end as scr_Curr_competitive_debt_PJ_over_revenue,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 when competitive_debt_PJ_Curr = 0 then 0 when Long_Term_Debt_PJ_Curr = 0 then 2019 when competitive_debt_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 else competitive_debt_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_Curr_competitive_debt_over_long_term_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 when competitive_debt_PJ_Curr = 0 then 0 when divida_curto_prazo = 0 then 2019 else competitive_debt_PJ_Curr/divida_curto_prazo end as scr_Curr_competitive_debt_over_short_term_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else max_competitive_debt * 1000 end as scr_max_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 when max_competitive_debt = 0 then 0 else competitive_debt_PJ_Curr / max_competitive_debt end as scr_curr_over_max_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else case when competitive_debt_PJ_Curr = max_competitive_debt then 1 else 0 end end as scr_All_TimeHigh_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else (competitive_debt_PJ_Curr - competitive_debt_PJ_5M) / month_revenue / 12 * 1000 end as scr_var_abs_6M_competitive_debt_over_revenue,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else (competitive_debt_PJ_Curr - competitive_debt_PJ_11M) / month_revenue / 12 * 1000 end as scr_var_abs_12M_competitive_debt_over_revenue,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 when competitive_debt_PJ_Curr = 0 then 0 when competitive_debt_PJ_5M = 0 then 2019 else competitive_debt_PJ_Curr / competitive_debt_PJ_5M - 1 end as scr_var_rel_6M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 when competitive_debt_PJ_Curr = 0 then 0 when competitive_debt_PJ_11M = 0 then 2019 else competitive_debt_PJ_Curr / competitive_debt_PJ_11M - 1 end as scr_var_rel_12M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else Meses_Aumento_competitive_debt end as scr_Months_Rise_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else Meses_Aumento_competitive_debt / (case when qtd_meses_competitive_debt <= 1 then 1 else qtd_meses_competitive_debt-1 end) end as scr_Months_Rise_competitive_debt_over_total_months,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else Meses_Reducao_competitive_debt end as scr_Months_Dive_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else Meses_Reducao_competitive_debt / (case when qtd_meses_competitive_debt <= 1 then 1 else qtd_meses_competitive_debt-1 end) end as scr_Months_Dive_competitive_debt_over_total_months,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else months_sth_competitive_debt end as scr_month_consecutive_dive_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else months_sth_competitive_debt / (case when qtd_meses_competitive_debt <= 1 then 1 else qtd_meses_competitive_debt-1 end) end as scr_month_consecutive_dive_competitive_debt_over_total_months,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else months_hth_competitive_debt end as scr_month_consecutive_rise_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else months_hth_competitive_debt / (case when qtd_meses_competitive_debt <= 1 then 1 else qtd_meses_competitive_debt-1 end) end as scr_month_consecutive_rise_competitive_debt_over_total_months,
--------------------------SCR PJ DIVIDA CONCORRENTE PF
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else divida_curto_prazo_pf end as scr_short_term_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else Ever_competitive_debt_pf * 1000 end as scr_ever_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_Curr * 1000 end as scr_curr_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_1M * 1000 end as scr_2M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_2M * 1000 end as scr_3M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_3M * 1000 end as scr_4M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_4M * 1000 end as scr_5M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_5M * 1000 end as scr_6M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_6M * 1000 end as scr_7M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_7M * 1000 end as scr_8M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_8M * 1000 end as scr_9M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_9M * 1000 end as scr_10M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_10M * 1000 end as scr_11M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_11M * 1000 end as scr_12M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_Curr/month_revenue/12*1000 end as scr_Curr_competitive_debt_pf_over_revenue,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 when competitive_debt_pf_Curr = 0 then 0 when Long_Term_Debt_pf_Curr = 0 then 2019 when competitive_debt_pf_Curr > Long_Term_Debt_pf_Curr then 1 else competitive_debt_pf_Curr/Long_Term_Debt_pf_Curr end as scr_Curr_competitive_debt_over_long_term_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 when competitive_debt_pf_Curr = 0 then 0 when divida_curto_prazo_pf = 0 then 2019 else competitive_debt_pf_Curr/divida_curto_prazo_pf end as scr_Curr_competitive_debt_over_short_term_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else max_competitive_debt_pf * 1000 end as scr_max_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 when max_competitive_debt_pf = 0 then 0 else competitive_debt_pf_Curr / max_competitive_debt_pf end as scr_curr_over_max_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else case when competitive_debt_pf_Curr = max_competitive_debt_pf then 1 else 0 end end as scr_All_TimeHigh_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else (competitive_debt_pf_Curr - competitive_debt_pf_5M)/month_revenue / 12 * 1000 end as scr_var_abs_6M_competitive_debt_over_revenue_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else (competitive_debt_pf_Curr - competitive_debt_pf_11M)/month_revenue / 12 * 1000 end as scr_var_abs_12M_competitive_debt_over_revenue_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 when competitive_debt_pf_Curr = 0 then 0 when competitive_debt_pf_5M = 0 then 2019 else competitive_debt_pf_Curr / competitive_debt_pf_5M - 1 end as scr_var_rel_6M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 when competitive_debt_pf_Curr = 0 then 0 when competitive_debt_pf_11M = 0 then 2019 else competitive_debt_pf_Curr / competitive_debt_pf_11M - 1 end as scr_var_rel_12M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else Meses_Aumento_competitive_debt_pf end as scr_Months_Rise_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else Meses_Aumento_competitive_debt_pf / (case when qtd_meses_competitive_debt_pf <= 1 then 1 else qtd_meses_competitive_debt_pf-1 end) end as scr_Months_Rise_competitive_debt_over_total_months_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else Meses_Reducao_competitive_debt_pf end as scr_Months_Dive_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else Meses_Reducao_competitive_debt_pf / (case when qtd_meses_competitive_debt_pf <= 1 then 1 else qtd_meses_competitive_debt_pf-1 end) end as scr_Months_Dive_competitive_debt_over_total_months_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else months_sth_competitive_debt_pf end as scr_month_consecutive_dive_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else months_sth_competitive_debt_pf / (case when qtd_meses_competitive_debt_pf <= 1 then 1 else qtd_meses_competitive_debt_pf-1 end) end as scr_month_consecutive_dive_competitive_debt_over_total_months_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else months_hth_competitive_debt_pf end as scr_month_consecutive_rise_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else months_hth_competitive_debt_pf / (case when qtd_meses_competitive_debt_pf <= 1 then 1 else qtd_meses_competitive_debt_pf-1 end) end as scr_month_consecutive_rise_competitive_debt_over_total_months_pf,
case when tem_scr_pf = 0 then null when vehicle_debt_pf_Curr is null then 0 else vehicle_debt_pf_Curr end as scr_Curr_vehicle_debt_pf,
case when tem_scr_pf = 0 then null when vehicle_debt_pf_Curr is null then 0 else vehicle_debt_pf_1M end as scr_2M_vehicle_debt_pf,
case when tem_scr_pf = 0 then null when vehicle_debt_pf_Curr is null then 0 else vehicle_debt_pf_2m end as scr_3M_vehicle_debt_pf,
case when tem_scr_pf = 0 then null when vehicle_debt_pf_Curr is null then 0 else vehicle_debt_pf_3m end as scr_4M_vehicle_debt_pf,
case when tem_scr_pf = 0 then null when vehicle_debt_pf_Curr is null then 0 else vehicle_debt_pf_4m end as scr_5M_vehicle_debt_pf,
case when tem_scr_pf = 0 then null when vehicle_debt_pf_Curr is null then 0 else vehicle_debt_pf_5m end as scr_6M_vehicle_debt_pf,
case when tem_scr_pf = 0 then null when vehicle_debt_pf_Curr is null then 0 else (vehicle_debt_pf_Curr - vehicle_debt_pf_2M)/month_revenue / 12 * 1000 end as scr_var_abs_3M_vehicle_debt_over_revenue_pf,
case when tem_scr_pf = 0 then null when vehicle_debt_pf_Curr is null then 0 else (vehicle_debt_pf_Curr - vehicle_debt_pf_5M)/month_revenue / 12 * 1000 end as scr_var_abs_6M_vehicle_debt_over_revenue_pf,
case when tem_scr_pf = 0 then null when vehicle_debt_pf_Curr is null then 0 when vehicle_debt_pf_Curr = 0 then 0 when vehicle_debt_pf_2M = 0 then 2019 else vehicle_debt_pf_Curr / vehicle_debt_pf_2M - 1 end as scr_var_rel_3M_vehicle_debt_pf,
case when tem_scr_pf = 0 then null when vehicle_debt_pf_Curr is null then 0 when vehicle_debt_pf_Curr = 0 then 0 when vehicle_debt_pf_5M = 0 then 2019 else vehicle_debt_pf_Curr / vehicle_debt_pf_5M - 1 end as scr_var_rel_6M_vehicle_debt_pf,
--------------------------SERASA
is_shareholder as experian_is_shareholder,
count_socios as experian_count_shareholders,
case when tem_serasa = 0 then null when empresas_problema is null then 0 else case when empresas_problema is null then -1 else empresas_problema end end as experian_derogatory_marked_related_companies,
case when tem_serasa = 0 then null when empresa_divida_valor is null then 0 else empresa_divida_valor end as experian_company_overdue_debt_value,
case when tem_serasa = 0 then null when empresa_divida_unit is null then 0 else empresa_divida_unit end as experian_company_overdue_debt_unit,
case when tem_serasa = 0 then null when empresa_ccf_valor is null then 0 else empresa_ccf_valor end as experian_company_bad_check_value,
case when tem_serasa = 0 then null when empresa_ccf_unit is null then 0 else empresa_ccf_unit end as experian_company_bad_check_unit,
case when tem_serasa = 0 then null when empresa_protesto_valor is null then 0 else empresa_protesto_valor end as experian_company_notary_registry_protests_value,
case when tem_serasa = 0 then null when empresa_protesto_unit is null then 0 else empresa_protesto_unit end as experian_company_notary_registry_protests_unit,
case when tem_serasa = 0 then null when empresa_acao_valor is null then 0 else empresa_acao_valor end as experian_company_legal_action_value,
case when tem_serasa = 0 then null when empresa_acao_unit is null then 0 else empresa_acao_unit end as experian_company_legal_action_unit,
case when tem_serasa = 0 then null when empresa_pefin_valor is null then 0 else empresa_pefin_valor end as experian_company_negative_mark_by_non_fin_companies_value,
case when tem_serasa = 0 then null when empresa_pefin_unit is null then 0 else empresa_pefin_unit end as experian_company_negative_mark_by_non_fin_companies_unit,
case when tem_serasa = 0 then null when empresa_refin_valor is null then 0 else empresa_refin_valor end as experian_company_negative_mark_by_fin_companies_value,
case when tem_serasa = 0 then null when empresa_refin_unit is null then 0 else empresa_refin_unit end as experian_company_negative_mark_by_fin_companies_unit,
case when tem_serasa = 0 then null when empresa_spc_valor is null then 0 else empresa_spc_valor end as experian_company_bureau_derogatory_value,
case when tem_serasa = 0 then null when empresa_spc_unit is null then 0 else empresa_spc_unit end as experian_company_bureau_derogatory_unit,
case when tem_serasa = 0 then null when empresa_total_valor is null then 0 else empresa_total_valor end as experian_company_derogatory_mark_total_value,
case when tem_serasa = 0 then null when empresa_total_unit is null then 0 else empresa_total_unit end as experian_company_derogatory_mark_total_unit,
case when tem_serasa = 0 then null when socio_divida_valor is null then 0 else socio_divida_valor end as experian_sh_overdue_debt_value,
case when tem_serasa = 0 then null when socio_divida_unit is null then 0 else socio_divida_unit end as experian_sh_overdue_debt_unit,
case when tem_serasa = 0 then null when socio_ccf_valor is null then 0 else socio_ccf_valor end as experian_sh_bad_check_value,
case when tem_serasa = 0 then null when socio_ccf_unit is null then 0 else socio_ccf_unit end as experian_sh_bad_check_unit,
case when tem_serasa = 0 then null when socio_protesto_valor is null then 0 else socio_protesto_valor end as experian_sh_notary_registry_protests_value,
case when tem_serasa = 0 then null when socio_protesto_unit is null then 0 else socio_protesto_unit end as experian_sh_notary_registry_protests_unit,
case when tem_serasa = 0 then null when socio_acao_valor is null then 0 else socio_acao_valor end as experian_sh_legal_action_value,
case when tem_serasa = 0 then null when socio_acao_unit is null then 0 else socio_acao_unit end as experian_sh_legal_action_unit,
case when tem_serasa = 0 then null when socio_pefin_valor is null then 0 else socio_pefin_valor end as experian_sh_negative_mark_by_non_fin_companies_value,
case when tem_serasa = 0 then null when socio_pefin_unit is null then 0 else socio_pefin_unit end as experian_sh_negative_mark_by_non_fin_companies_unit,
case when tem_serasa = 0 then null when socio_refin_valor is null then 0 else socio_refin_valor end as experian_sh_negative_mark_by_fin_companies_value,
case when tem_serasa = 0 then null when socio_refin_unit is null then 0 else socio_refin_unit end as experian_sh_negative_mark_by_fin_companies_unit,
case when tem_serasa = 0 then null when socio_spc_valor is null then 0 else socio_spc_valor end as experian_sh_bureau_derogatory_value,
case when tem_serasa = 0 then null when socio_spc_unit is null then 0 else socio_spc_unit end as experian_sh_bureau_derogatory_unit,
case when tem_serasa = 0 then null when socios_total_valor is null then 0 else socios_total_valor end as experian_sh_derogatory_mark_total_value,
case when tem_serasa = 0 then null when socios_total_unit is null then 0 else socios_total_unit end as experian_sh_derogatory_mark_total_unit,
case when tem_serasa = 0 then null when socio_major_divida_valor is null then 0 else socio_major_divida_valor end as experian_major_sh_overdue_debt_value,
case when tem_serasa = 0 then null when socio_major_divida_unit is null then 0 else socio_major_divida_unit end as experian_major_sh_overdue_debt_unit,
case when tem_serasa = 0 then null when socio_major_ccf_valor is null then 0 else socio_major_ccf_valor end as experian_major_sh_bad_check_value,
case when tem_serasa = 0 then null when socio_major_ccf_unit is null then 0 else socio_major_ccf_unit end as experian_major_sh_bad_check_unit,
case when tem_serasa = 0 then null when socio_major_protesto_valor is null then 0 else socio_major_protesto_valor end as experian_major_sh_notary_registry_protests_value,
case when tem_serasa = 0 then null when socio_major_protesto_unit is null then 0 else socio_major_protesto_unit end as experian_major_sh_notary_registry_protests_unit,
case when tem_serasa = 0 then null when socio_major_acao_valor is null then 0 else socio_major_acao_valor end as experian_major_sh_legal_action_value,
case when tem_serasa = 0 then null when socio_major_acao_unit is null then 0 else socio_major_acao_unit end as experian_major_sh_legal_action_unit,
case when tem_serasa = 0 then null when socio_major_pefin_valor is null then 0 else socio_major_pefin_valor end as experian_major_sh_negative_mark_by_non_fin_companies_value,
case when tem_serasa = 0 then null when socio_major_pefin_unit is null then 0 else socio_major_pefin_unit end as experian_major_sh_negative_mark_by_non_fin_companies_unit,
case when tem_serasa = 0 then null when socio_major_refin_valor is null then 0 else socio_major_refin_valor end as experian_major_sh_negative_mark_by_fin_companies_value,
case when tem_serasa = 0 then null when socio_major_refin_unit is null then 0 else socio_major_refin_unit end as experian_major_sh_negative_mark_by_fin_companies_unit,
case when tem_serasa = 0 then null when socio_major_spc_valor is null then 0 else socio_major_spc_valor end as experian_major_sh_bureau_derogatory_value,
case when tem_serasa = 0 then null when socio_major_spc_unit is null then 0 else socio_major_spc_unit end as experian_major_sh_bureau_derogatory_unit,
case when tem_serasa = 0 then null when socios_major_total_valor is null then 0 else socios_major_total_valor end as experian_major_sh_derogatory_mark_total_value,
case when tem_serasa = 0 then null when socios_major_total_unit is null then 0 else socios_major_total_unit end as experian_major_sh_derogatory_mark_total_unit,
case when tem_serasa = 0 then null when all_valor is null then 0 else all_valor end as "experian_company+shareholders_derogatory_mark_total_value",
case when tem_serasa = 0 then null when all_unit is null then 0 else all_unit end as "experian_company+shareholders_derogatory_mark_total_unit",
case when tem_serasa = 0 then null else case when consulta_factoring_serasa + consulta_factoring_spc > 0 then 1 else 0 end end experian_report_recently_taken_by_factoring,
case when tem_serasa = 0 or consultas_atual is null then null else consultas_atual end as experian_inquiries_curr, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_1m end as experian_inquiries_2m, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_2m end as experian_inquiries_3m, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_3m end as experian_inquiries_4m, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_4m end as experian_inquiries_5m, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_5m end as experian_inquiries_6m, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_6m end as experian_inquiries_7m, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_7m end as experian_inquiries_8m, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_8m end as experian_inquiries_9m, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_9m end as experian_inquiries_10m, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_10m end as experian_inquiries_11m, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_11m end as experian_inquiries_12m, 
--------------------------SIGNERS
MajorSigner_Age as signers_MajorShareholder_Age, 
MajorSigner_Revenue as signers_MajorShareholder_Revenue,
MajorSigner_civil_status as signers_MajorShareholder_Maritial_Status,
MajorSigner_gender as signers_MajorShareholdergender,
SolicSignersCPF_Age as signers_RequesterSigner_Age,
SolicSignersCPF_Revenue as signers_RequesterSigner_Revenue,
SolicSignerCPF_civil_status as signers_RequesterSigner_Maritial_Status,
SolicSignerCPF_gender as signers_RequesterSigner_gender,
OldestSh_Age as signers_OldestSh_Age,
OldestSh_Revenue as signers_OldestSh_Revenue,
OldestSh_civil_status as signers_OldestSh_Maritial_status,
OldestSh_gender as signers_OldestSh_gender,
AdminSigner_Age as signers_AdminSigner_Age,
AdminSigner_Revenue as signers_AdminSigner_Revenue,
AdminSigner_civil_status as signers_AdminSigner_Maritial_status,
AdminSigner_gender as signers_AdminSigner_gender,
avg_age_partners as signers_avg_age_partners,
avg_revenue_partners as signers_avg_revenue_partners,
male_partners as signers_male_shareholders,
female_partners as signers_female_shareholders,
--------------------------CHECKLIST
social_contract_confirmed as checklist_social_contract_confirmed,
company_address_confirmed as checklist_company_address_confirmed,
bank_statements_confirmed as checklist_bank_statements_confirmed,
company_account_confirmed as checklist_company_account_confirmed,
personal_rg as checklist_shareholder_rg,
personal_cpf as checklist_shareholder_cpf,
proof_personal_address as checklist_personal_address_confirmed,
proof_income as checklist_shareholder_income_confirmed,
spouse_info as checklist_shareholder_spouse_info,
proved_informed_revenue as checklist_proved_informed_revenue,
positive_balance_account as checklist_positive_balance_account,
low_negative_balance as checklist_low_negative_balance,
largest_cashflow_company_account as checklist_largest_cashflow_company_account,
continuous_income as checklist_continuous_income,
income_unrelated_government as checklist_income_unrelated_government,
diversified_clients as checklist_diversified_clients,
only_company_expenses as checklist_only_company_expenses,
different_personal_company_address as checklist_different_personal_company_address,
dilligent_credit_simulation as checklist_dilligent_credit_simulation,
requested_lessthan_offered as checklist_requested_lessthan_offered,
company_usedto_credit as checklist_company_usedto_credit,
partner_has_assets as checklist_partner_has_assets,
low_personal_debt as checklist_low_personal_debt,
percent_documentation as checklist_percent_documentation,
score_documentation as checklist_score,
--------------------------TAGS
app_auto as tags_approved_automatically,
offer_auto as tags_offered_automatically,
interest_rate_test1 as tags_interest_rate_test_scope
--------------------------
FROM direct_prospects dp
join clients c on c.client_id = dp.client_id
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join (select o.* ,count_neg from offers o join (select direct_prospect_id,min(offer_id) as min_id,count(*) as count_neg from offers group by 1) as initial_off on initial_off.min_id = o.offer_id) as initialoff on initialoff.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
join bank_accounts ba on ba.bank_account_id = lr.bank_account_id
left join populacao p on p.municipio = dp.city and p.uf = dp.state
--------------------------TABLE TEM SCR, SCR PF E SERASA
left join (select direct_prospect_id, 
		case when max_scr is null and min_scr is null then 0 else 1 end as tem_scr, 
		case when max_scr_pf is null and min_scr_pf is null then 0 else 1 end as tem_scr_pf, 
		case when max_serasa is null then 0 else 1 end as tem_serasa 
		from (select dp.direct_prospect_id,
				max(id) filter (where to_date(left(cc.data->>'criado_em',10),'yyyy-mm-dd') <= lr.loan_date and (cc.data->'historico' is not null or cc.data->'erro' is not null) and tipo = 'SCR' and documento_tipo = 'CNPJ') as max_scr, 
				min(id) filter (where (cc.data->'historico' is not null or cc.data->'erro' is not null) and tipo = 'SCR' and documento_tipo = 'CNPJ') as min_scr,
				max(id) filter (where to_date(left(cc.data->>'criado_em',10),'yyyy-mm-dd') <= lr.loan_date and (cc.data->'historico' is not null or cc.data->'erro' is not null) and tipo = 'SCR' and documento_tipo = 'CPF') as max_scr_pf, 
				min(id) filter (where (cc.data->'historico' is not null or cc.data->'erro' is not null) and tipo = 'SCR' and documento_tipo = 'CPF') as min_scr_pf,
				max(id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date and tipo = 'Serasa') as max_serasa
			from credito_coleta cc 
			join direct_prospects dp on case when cc.documento_tipo = 'CPF' then dp.cpf = cc.documento when cc.documento_tipo = 'CNPJ' then left(dp.cnpj,8) = left(cc.documento,8) end
			join offers o on o.direct_prospect_id = dp.direct_prospect_id 
			join loan_requests lr on lr.offer_id = o.offer_id 
			where lr.status = 'ACCEPTED' group by 1) as t1) as ValidSCRSerasa on ValidSCRSerasa.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SCR PJ
left join(select direct_prospect_id,
	Num_Ops_PJ::float,
	Num_FIs_PJ::float,
	First_Relation_FI_PJ::float,
	Long_Term_Debt_PJ_Curr::float,
	Long_Term_Debt_PJ_1M::float,
	Long_Term_Debt_PJ_2M::float,
	Long_Term_Debt_PJ_3M::float,
	Long_Term_Debt_PJ_4M::float,
	Long_Term_Debt_PJ_5M::float,
	Long_Term_Debt_PJ_6M::float,
	Long_Term_Debt_PJ_7M::float,
	Long_Term_Debt_PJ_8M::float,
	Long_Term_Debt_PJ_9M::float,
	Long_Term_Debt_PJ_10M::float,
	Long_Term_Debt_PJ_11M::float,
	Ever_long_term_debt_PJ::float,
	Max_Long_Term_Debt_PJ::float,
	Overdue_PJ_Curr::float,
	Months_Since_Last_Overdue_PJ::float,
	Months_Overdue_PJ::float,
	Max_Overdue_PJ::float,
	Default_PJ_Curr::float,
	Months_Since_Last_Default_PJ::float,
	Months_Default_PJ::float,
	Max_Default_PJ::float,
	Qtd_meses_escopo_pj::float,
	case when Long_Term_Debt_PJ_22M > Long_Term_Debt_PJ_23M then 1 else 0 end +
	case when Long_Term_Debt_PJ_21M > Long_Term_Debt_PJ_22M then 1 else 0 end +
	case when Long_Term_Debt_PJ_20M > Long_Term_Debt_PJ_21M then 1 else 0 end +
	case when Long_Term_Debt_PJ_19M > Long_Term_Debt_PJ_20M then 1 else 0 end +
	case when Long_Term_Debt_PJ_18M > Long_Term_Debt_PJ_19M then 1 else 0 end +
	case when Long_Term_Debt_PJ_17M > Long_Term_Debt_PJ_18M then 1 else 0 end +
	case when Long_Term_Debt_PJ_16M > Long_Term_Debt_PJ_17M then 1 else 0 end +
	case when Long_Term_Debt_PJ_15M > Long_Term_Debt_PJ_16M then 1 else 0 end +
	case when Long_Term_Debt_PJ_14M > Long_Term_Debt_PJ_15M then 1 else 0 end +
	case when Long_Term_Debt_PJ_13M > Long_Term_Debt_PJ_14M then 1 else 0 end +
	case when Long_Term_Debt_PJ_12M > Long_Term_Debt_PJ_13M then 1 else 0 end +
	case when Long_Term_Debt_PJ_11M > Long_Term_Debt_PJ_12M then 1 else 0 end +
	case when Long_Term_Debt_PJ_10M > Long_Term_Debt_PJ_11M then 1 else 0 end +
	case when Long_Term_Debt_PJ_9M > Long_Term_Debt_PJ_10M then 1 else 0 end +
	case when Long_Term_Debt_PJ_8M > Long_Term_Debt_PJ_9M then 1 else 0 end +
	case when Long_Term_Debt_PJ_7M > Long_Term_Debt_PJ_8M then 1 else 0 end +
	case when Long_Term_Debt_PJ_6M > Long_Term_Debt_PJ_7M then 1 else 0 end +
	case when Long_Term_Debt_PJ_5M > Long_Term_Debt_PJ_6M then 1 else 0 end +
	case when Long_Term_Debt_PJ_4M > Long_Term_Debt_PJ_5M then 1 else 0 end +
	case when Long_Term_Debt_PJ_3M > Long_Term_Debt_PJ_4M then 1 else 0 end +
	case when Long_Term_Debt_PJ_2M > Long_Term_Debt_PJ_3M then 1 else 0 end +
	case when Long_Term_Debt_PJ_1M > Long_Term_Debt_PJ_2M then 1 else 0 end +
	case when Long_Term_Debt_PJ_Curr > Long_Term_Debt_PJ_1M then 1 else 0 end::float as Meses_Aumento_DividaPJ,
	case when Long_Term_Debt_PJ_22M < Long_Term_Debt_PJ_23M then 1 else 0 end +
	case when Long_Term_Debt_PJ_21M < Long_Term_Debt_PJ_22M then 1 else 0 end +
	case when Long_Term_Debt_PJ_20M < Long_Term_Debt_PJ_21M then 1 else 0 end +
	case when Long_Term_Debt_PJ_19M < Long_Term_Debt_PJ_20M then 1 else 0 end +
	case when Long_Term_Debt_PJ_18M < Long_Term_Debt_PJ_19M then 1 else 0 end +
	case when Long_Term_Debt_PJ_17M < Long_Term_Debt_PJ_18M then 1 else 0 end +
	case when Long_Term_Debt_PJ_16M < Long_Term_Debt_PJ_17M then 1 else 0 end +
	case when Long_Term_Debt_PJ_15M < Long_Term_Debt_PJ_16M then 1 else 0 end +
	case when Long_Term_Debt_PJ_14M < Long_Term_Debt_PJ_15M then 1 else 0 end +
	case when Long_Term_Debt_PJ_13M < Long_Term_Debt_PJ_14M then 1 else 0 end +
	case when Long_Term_Debt_PJ_12M < Long_Term_Debt_PJ_13M then 1 else 0 end +
	case when Long_Term_Debt_PJ_11M < Long_Term_Debt_PJ_12M then 1 else 0 end +
	case when Long_Term_Debt_PJ_10M < Long_Term_Debt_PJ_11M then 1 else 0 end +
	case when Long_Term_Debt_PJ_9M < Long_Term_Debt_PJ_10M then 1 else 0 end +
	case when Long_Term_Debt_PJ_8M < Long_Term_Debt_PJ_9M then 1 else 0 end +
	case when Long_Term_Debt_PJ_7M < Long_Term_Debt_PJ_8M then 1 else 0 end +
	case when Long_Term_Debt_PJ_6M < Long_Term_Debt_PJ_7M then 1 else 0 end +
	case when Long_Term_Debt_PJ_5M < Long_Term_Debt_PJ_6M then 1 else 0 end +
	case when Long_Term_Debt_PJ_4M < Long_Term_Debt_PJ_5M then 1 else 0 end +
	case when Long_Term_Debt_PJ_3M < Long_Term_Debt_PJ_4M then 1 else 0 end +
	case when Long_Term_Debt_PJ_2M < Long_Term_Debt_PJ_3M then 1 else 0 end +
	case when Long_Term_Debt_PJ_1M < Long_Term_Debt_PJ_2M then 1 else 0 end +
	case when Long_Term_Debt_PJ_Curr < Long_Term_Debt_PJ_1M then 1 else 0 end::float as Meses_Reducao_DividaPJ,
	(case when Long_Term_Debt_PJ_22M < Long_Term_Debt_PJ_23M then Long_Term_Debt_PJ_22M - Long_Term_Debt_PJ_23M else 0 end +
	case when Long_Term_Debt_PJ_21M < Long_Term_Debt_PJ_22M then Long_Term_Debt_PJ_21M - Long_Term_Debt_PJ_22M else 0 end +
	case when Long_Term_Debt_PJ_20M < Long_Term_Debt_PJ_21M then Long_Term_Debt_PJ_20M - Long_Term_Debt_PJ_21M else 0 end +
	case when Long_Term_Debt_PJ_19M < Long_Term_Debt_PJ_20M then Long_Term_Debt_PJ_19M - Long_Term_Debt_PJ_20M else 0 end +
	case when Long_Term_Debt_PJ_18M < Long_Term_Debt_PJ_19M then Long_Term_Debt_PJ_18M - Long_Term_Debt_PJ_19M else 0 end +
	case when Long_Term_Debt_PJ_17M < Long_Term_Debt_PJ_18M then Long_Term_Debt_PJ_17M - Long_Term_Debt_PJ_18M else 0 end +
	case when Long_Term_Debt_PJ_16M < Long_Term_Debt_PJ_17M then Long_Term_Debt_PJ_16M - Long_Term_Debt_PJ_17M else 0 end +
	case when Long_Term_Debt_PJ_15M < Long_Term_Debt_PJ_16M then Long_Term_Debt_PJ_15M - Long_Term_Debt_PJ_16M else 0 end +
	case when Long_Term_Debt_PJ_14M < Long_Term_Debt_PJ_15M then Long_Term_Debt_PJ_14M - Long_Term_Debt_PJ_15M else 0 end +
	case when Long_Term_Debt_PJ_13M < Long_Term_Debt_PJ_14M then Long_Term_Debt_PJ_13M - Long_Term_Debt_PJ_14M else 0 end +
	case when Long_Term_Debt_PJ_12M < Long_Term_Debt_PJ_13M then Long_Term_Debt_PJ_12M - Long_Term_Debt_PJ_13M else 0 end +
	case when Long_Term_Debt_PJ_11M < Long_Term_Debt_PJ_12M then Long_Term_Debt_PJ_11M - Long_Term_Debt_PJ_12M else 0 end +
	case when Long_Term_Debt_PJ_10M < Long_Term_Debt_PJ_11M then Long_Term_Debt_PJ_10M - Long_Term_Debt_PJ_11M else 0 end +
	case when Long_Term_Debt_PJ_9M < Long_Term_Debt_PJ_10M then Long_Term_Debt_PJ_9M - Long_Term_Debt_PJ_10M else 0 end +
	case when Long_Term_Debt_PJ_8M < Long_Term_Debt_PJ_9M then Long_Term_Debt_PJ_8M - Long_Term_Debt_PJ_9M else 0 end +
	case when Long_Term_Debt_PJ_7M < Long_Term_Debt_PJ_8M then Long_Term_Debt_PJ_7M - Long_Term_Debt_PJ_8M else 0 end +
	case when Long_Term_Debt_PJ_6M < Long_Term_Debt_PJ_7M then Long_Term_Debt_PJ_6M - Long_Term_Debt_PJ_7M else 0 end +
	case when Long_Term_Debt_PJ_5M < Long_Term_Debt_PJ_6M then Long_Term_Debt_PJ_5M - Long_Term_Debt_PJ_6M else 0 end +
	case when Long_Term_Debt_PJ_4M < Long_Term_Debt_PJ_5M then Long_Term_Debt_PJ_4M - Long_Term_Debt_PJ_5M else 0 end +
	case when Long_Term_Debt_PJ_3M < Long_Term_Debt_PJ_4M then Long_Term_Debt_PJ_3M - Long_Term_Debt_PJ_4M else 0 end +
	case when Long_Term_Debt_PJ_2M < Long_Term_Debt_PJ_3M then Long_Term_Debt_PJ_2M - Long_Term_Debt_PJ_3M else 0 end +
	case when Long_Term_Debt_PJ_1M < Long_Term_Debt_PJ_2M then Long_Term_Debt_PJ_1M - Long_Term_Debt_PJ_2M else 0 end +
	case when Long_Term_Debt_PJ_Curr < Long_Term_Debt_PJ_1M then Long_Term_Debt_PJ_Curr - Long_Term_Debt_PJ_1M else 0 end)*(-1)::float as Saldo_Amort_DividaPJ,
	case when Long_Term_Debt_PJ_1M is null then null else
	case when Long_Term_Debt_PJ_Curr >= Long_Term_Debt_PJ_1M then 0 else
	case when Long_Term_Debt_PJ_1M >= Long_Term_Debt_PJ_2M or Long_Term_Debt_PJ_2M is null then 1 else
	case when Long_Term_Debt_PJ_2M >= Long_Term_Debt_PJ_3M or Long_Term_Debt_PJ_3M is null then 2 else
	case when Long_Term_Debt_PJ_3M >= Long_Term_Debt_PJ_4M or Long_Term_Debt_PJ_4M is null then 3 else
	case when Long_Term_Debt_PJ_4M >= Long_Term_Debt_PJ_5M or Long_Term_Debt_PJ_5M is null then 4 else
	case when Long_Term_Debt_PJ_5M >= Long_Term_Debt_PJ_6M or Long_Term_Debt_PJ_6M is null then 5 else
	case when Long_Term_Debt_PJ_6M >= Long_Term_Debt_PJ_7M or Long_Term_Debt_PJ_7M is null then 6 else
	case when Long_Term_Debt_PJ_7M >= Long_Term_Debt_PJ_8M or Long_Term_Debt_PJ_8M is null then 7 else
	case when Long_Term_Debt_PJ_8M >= Long_Term_Debt_PJ_9M or Long_Term_Debt_PJ_9M is null then 8 else
	case when Long_Term_Debt_PJ_9M >= Long_Term_Debt_PJ_10M or Long_Term_Debt_PJ_10M is null then 9 else
	case when Long_Term_Debt_PJ_10M >= Long_Term_Debt_PJ_11M or Long_Term_Debt_PJ_11M is null then 10 else
	case when Long_Term_Debt_PJ_11M >= Long_Term_Debt_PJ_12M or Long_Term_Debt_PJ_12M is null then 11 else
	case when Long_Term_Debt_PJ_12M >= Long_Term_Debt_PJ_13M or Long_Term_Debt_PJ_13M is null then 12 else
	case when Long_Term_Debt_PJ_13M >= Long_Term_Debt_PJ_14M or Long_Term_Debt_PJ_14M is null then 13 else
	case when Long_Term_Debt_PJ_14M >= Long_Term_Debt_PJ_15M or Long_Term_Debt_PJ_15M is null then 14 else
	case when Long_Term_Debt_PJ_15M >= Long_Term_Debt_PJ_16M or Long_Term_Debt_PJ_16M is null then 15 else
	case when Long_Term_Debt_PJ_16M >= Long_Term_Debt_PJ_17M or Long_Term_Debt_PJ_17M is null then 16 else
	case when Long_Term_Debt_PJ_17M >= Long_Term_Debt_PJ_18M or Long_Term_Debt_PJ_18M is null then 17 else
	case when Long_Term_Debt_PJ_18M >= Long_Term_Debt_PJ_19M or Long_Term_Debt_PJ_19M is null then 18 else
	case when Long_Term_Debt_PJ_19M >= Long_Term_Debt_PJ_20M or Long_Term_Debt_PJ_20M is null then 19 else
	case when Long_Term_Debt_PJ_20M >= Long_Term_Debt_PJ_21M or Long_Term_Debt_PJ_21M is null then 20 else
	case when Long_Term_Debt_PJ_21M >= Long_Term_Debt_PJ_22M or Long_Term_Debt_PJ_22M is null then 21 else
	case when Long_Term_Debt_PJ_22M >= Long_Term_Debt_PJ_23M or Long_Term_Debt_PJ_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_sth_pj,
	case when Long_Term_Debt_PJ_1M is null then null else
	case when Long_Term_Debt_PJ_Curr <= Long_Term_Debt_PJ_1M then 0 else
	case when Long_Term_Debt_PJ_1M <= Long_Term_Debt_PJ_2M or Long_Term_Debt_PJ_2M is null then 1 else
	case when Long_Term_Debt_PJ_2M <= Long_Term_Debt_PJ_3M or Long_Term_Debt_PJ_3M is null then 2 else
	case when Long_Term_Debt_PJ_3M <= Long_Term_Debt_PJ_4M or Long_Term_Debt_PJ_4M is null then 3 else
	case when Long_Term_Debt_PJ_4M <= Long_Term_Debt_PJ_5M or Long_Term_Debt_PJ_5M is null then 4 else
	case when Long_Term_Debt_PJ_5M <= Long_Term_Debt_PJ_6M or Long_Term_Debt_PJ_6M is null then 5 else
	case when Long_Term_Debt_PJ_6M <= Long_Term_Debt_PJ_7M or Long_Term_Debt_PJ_7M is null then 6 else
	case when Long_Term_Debt_PJ_7M <= Long_Term_Debt_PJ_8M or Long_Term_Debt_PJ_8M is null then 7 else
	case when Long_Term_Debt_PJ_8M <= Long_Term_Debt_PJ_9M or Long_Term_Debt_PJ_9M is null then 8 else
	case when Long_Term_Debt_PJ_9M <= Long_Term_Debt_PJ_10M or Long_Term_Debt_PJ_10M is null then 9 else
	case when Long_Term_Debt_PJ_10M <= Long_Term_Debt_PJ_11M or Long_Term_Debt_PJ_11M is null then 10 else
	case when Long_Term_Debt_PJ_11M <= Long_Term_Debt_PJ_12M or Long_Term_Debt_PJ_12M is null then 11 else
	case when Long_Term_Debt_PJ_12M <= Long_Term_Debt_PJ_13M or Long_Term_Debt_PJ_13M is null then 12 else
	case when Long_Term_Debt_PJ_13M <= Long_Term_Debt_PJ_14M or Long_Term_Debt_PJ_14M is null then 13 else
	case when Long_Term_Debt_PJ_14M <= Long_Term_Debt_PJ_15M or Long_Term_Debt_PJ_15M is null then 14 else
	case when Long_Term_Debt_PJ_15M <= Long_Term_Debt_PJ_16M or Long_Term_Debt_PJ_16M is null then 15 else
	case when Long_Term_Debt_PJ_16M <= Long_Term_Debt_PJ_17M or Long_Term_Debt_PJ_17M is null then 16 else
	case when Long_Term_Debt_PJ_17M <= Long_Term_Debt_PJ_18M or Long_Term_Debt_PJ_18M is null then 17 else
	case when Long_Term_Debt_PJ_18M <= Long_Term_Debt_PJ_19M or Long_Term_Debt_PJ_19M is null then 18 else
	case when Long_Term_Debt_PJ_19M <= Long_Term_Debt_PJ_20M or Long_Term_Debt_PJ_20M is null then 19 else
	case when Long_Term_Debt_PJ_20M <= Long_Term_Debt_PJ_21M or Long_Term_Debt_PJ_21M is null then 20 else
	case when Long_Term_Debt_PJ_21M <= Long_Term_Debt_PJ_22M or Long_Term_Debt_PJ_22M is null then 21 else
	case when Long_Term_Debt_PJ_22M <= Long_Term_Debt_PJ_23M or Long_Term_Debt_PJ_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_hth_pj,
	Lim_Cred_PJ_Curr::float,
	Lim_Cred_PJ_1M::float,
	Lim_Cred_PJ_2M::float,
	Lim_Cred_PJ_3M::float,
	Lim_Cred_PJ_4M::float,
	Lim_Cred_PJ_5M::float,
	Lim_Cred_PJ_6M::float,
	Lim_Cred_PJ_7M::float,
	Lim_Cred_PJ_8M::float,
	Lim_Cred_PJ_9M::float,
	Lim_Cred_PJ_10M::float,
	Lim_Cred_PJ_11M::float,
	Ever_Lim_Cred_PJ::float,
	Max_Lim_Cred_PJ::float,
	case when Lim_cred_PJ_22M > Lim_cred_PJ_23M then 1 else 0 end +
	case when Lim_cred_PJ_21M > Lim_cred_PJ_22M then 1 else 0 end +
	case when Lim_cred_PJ_20M > Lim_cred_PJ_21M then 1 else 0 end +
	case when Lim_cred_PJ_19M > Lim_cred_PJ_20M then 1 else 0 end +
	case when Lim_cred_PJ_18M > Lim_cred_PJ_19M then 1 else 0 end +
	case when Lim_cred_PJ_17M > Lim_cred_PJ_18M then 1 else 0 end +
	case when Lim_cred_PJ_16M > Lim_cred_PJ_17M then 1 else 0 end +
	case when Lim_cred_PJ_15M > Lim_cred_PJ_16M then 1 else 0 end +
	case when Lim_cred_PJ_14M > Lim_cred_PJ_15M then 1 else 0 end +
	case when Lim_cred_PJ_13M > Lim_cred_PJ_14M then 1 else 0 end +
	case when Lim_cred_PJ_12M > Lim_cred_PJ_13M then 1 else 0 end +
	case when Lim_cred_PJ_11M > Lim_cred_PJ_12M then 1 else 0 end +
	case when Lim_cred_PJ_10M > Lim_cred_PJ_11M then 1 else 0 end +
	case when Lim_cred_PJ_9M > Lim_cred_PJ_10M then 1 else 0 end +
	case when Lim_cred_PJ_8M > Lim_cred_PJ_9M then 1 else 0 end +
	case when Lim_cred_PJ_7M > Lim_cred_PJ_8M then 1 else 0 end +
	case when Lim_cred_PJ_6M > Lim_cred_PJ_7M then 1 else 0 end +
	case when Lim_cred_PJ_5M > Lim_cred_PJ_6M then 1 else 0 end +
	case when Lim_cred_PJ_4M > Lim_cred_PJ_5M then 1 else 0 end +
	case when Lim_cred_PJ_3M > Lim_cred_PJ_4M then 1 else 0 end +
	case when Lim_cred_PJ_2M > Lim_cred_PJ_3M then 1 else 0 end +
	case when Lim_cred_PJ_1M > Lim_cred_PJ_2M then 1 else 0 end +
	case when Lim_cred_PJ_Curr > Lim_cred_PJ_1M then 1 else 0 end::float as Meses_Aumento_Lim_Cred_PJ,
	case when Lim_cred_PJ_22M < Lim_cred_PJ_23M then 1 else 0 end +
	case when Lim_cred_PJ_21M < Lim_cred_PJ_22M then 1 else 0 end +
	case when Lim_cred_PJ_20M < Lim_cred_PJ_21M then 1 else 0 end +
	case when Lim_cred_PJ_19M < Lim_cred_PJ_20M then 1 else 0 end +
	case when Lim_cred_PJ_18M < Lim_cred_PJ_19M then 1 else 0 end +
	case when Lim_cred_PJ_17M < Lim_cred_PJ_18M then 1 else 0 end +
	case when Lim_cred_PJ_16M < Lim_cred_PJ_17M then 1 else 0 end +
	case when Lim_cred_PJ_15M < Lim_cred_PJ_16M then 1 else 0 end +
	case when Lim_cred_PJ_14M < Lim_cred_PJ_15M then 1 else 0 end +
	case when Lim_cred_PJ_13M < Lim_cred_PJ_14M then 1 else 0 end +
	case when Lim_cred_PJ_12M < Lim_cred_PJ_13M then 1 else 0 end +
	case when Lim_cred_PJ_11M < Lim_cred_PJ_12M then 1 else 0 end +
	case when Lim_cred_PJ_10M < Lim_cred_PJ_11M then 1 else 0 end +
	case when Lim_cred_PJ_9M < Lim_cred_PJ_10M then 1 else 0 end +
	case when Lim_cred_PJ_8M < Lim_cred_PJ_9M then 1 else 0 end +
	case when Lim_cred_PJ_7M < Lim_cred_PJ_8M then 1 else 0 end +
	case when Lim_cred_PJ_6M < Lim_cred_PJ_7M then 1 else 0 end +
	case when Lim_cred_PJ_5M < Lim_cred_PJ_6M then 1 else 0 end +
	case when Lim_cred_PJ_4M < Lim_cred_PJ_5M then 1 else 0 end +
	case when Lim_cred_PJ_3M < Lim_cred_PJ_4M then 1 else 0 end +
	case when Lim_cred_PJ_2M < Lim_cred_PJ_3M then 1 else 0 end +
	case when Lim_cred_PJ_1M < Lim_cred_PJ_2M then 1 else 0 end +
	case when Lim_cred_PJ_Curr < Lim_cred_PJ_1M then 1 else 0 end::float as Meses_Reducao_Lim_Cred_PJ,
	case when Lim_cred_PJ_1M is null then null else
	case when Lim_cred_PJ_Curr >= Lim_cred_PJ_1M or Lim_cred_PJ_1M is null then 0 else
	case when Lim_cred_PJ_1M >= Lim_cred_PJ_2M or Lim_cred_PJ_2M is null then 1 else
	case when Lim_cred_PJ_2M >= Lim_cred_PJ_3M or Lim_cred_PJ_3M is null then 2 else
	case when Lim_cred_PJ_3M >= Lim_cred_PJ_4M or Lim_cred_PJ_4M is null then 3 else
	case when Lim_cred_PJ_4M >= Lim_cred_PJ_5M or Lim_cred_PJ_5M is null then 4 else
	case when Lim_cred_PJ_5M >= Lim_cred_PJ_6M or Lim_cred_PJ_6M is null then 5 else
	case when Lim_cred_PJ_6M >= Lim_cred_PJ_7M or Lim_cred_PJ_7M is null then 6 else
	case when Lim_cred_PJ_7M >= Lim_cred_PJ_8M or Lim_cred_PJ_8M is null then 7 else
	case when Lim_cred_PJ_8M >= Lim_cred_PJ_9M or Lim_cred_PJ_9M is null then 8 else
	case when Lim_cred_PJ_9M >= Lim_cred_PJ_10M or Lim_cred_PJ_10M is null then 9 else
	case when Lim_cred_PJ_10M >= Lim_cred_PJ_11M or Lim_cred_PJ_11M is null then 10 else
	case when Lim_cred_PJ_11M >= Lim_cred_PJ_12M or Lim_cred_PJ_12M is null then 11 else
	case when Lim_cred_PJ_12M >= Lim_cred_PJ_13M or Lim_cred_PJ_13M is null then 12 else
	case when Lim_cred_PJ_13M >= Lim_cred_PJ_14M or Lim_cred_PJ_14M is null then 13 else
	case when Lim_cred_PJ_14M >= Lim_cred_PJ_15M or Lim_cred_PJ_15M is null then 14 else
	case when Lim_cred_PJ_15M >= Lim_cred_PJ_16M or Lim_cred_PJ_16M is null then 15 else
	case when Lim_cred_PJ_16M >= Lim_cred_PJ_17M or Lim_cred_PJ_17M is null then 16 else
	case when Lim_cred_PJ_17M >= Lim_cred_PJ_18M or Lim_cred_PJ_18M is null then 17 else
	case when Lim_cred_PJ_18M >= Lim_cred_PJ_19M or Lim_cred_PJ_19M is null then 18 else
	case when Lim_cred_PJ_19M >= Lim_cred_PJ_20M or Lim_cred_PJ_20M is null then 19 else
	case when Lim_cred_PJ_20M >= Lim_cred_PJ_21M or Lim_cred_PJ_21M is null then 20 else
	case when Lim_cred_PJ_21M >= Lim_cred_PJ_22M or Lim_cred_PJ_22M is null then 21 else
	case when Lim_cred_PJ_22M >= Lim_cred_PJ_23M or Lim_cred_PJ_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_sth_lim_cred_pj,
	case when Lim_cred_PJ_1M is null then null else
	case when Lim_cred_PJ_Curr <= Lim_cred_PJ_1M or Lim_cred_PJ_1M is null then 0 else
	case when Lim_cred_PJ_1M <= Lim_cred_PJ_2M or Lim_cred_PJ_2M is null then 1 else
	case when Lim_cred_PJ_2M <= Lim_cred_PJ_3M or Lim_cred_PJ_3M is null then 2 else
	case when Lim_cred_PJ_3M <= Lim_cred_PJ_4M or Lim_cred_PJ_4M is null then 3 else
	case when Lim_cred_PJ_4M <= Lim_cred_PJ_5M or Lim_cred_PJ_5M is null then 4 else
	case when Lim_cred_PJ_5M <= Lim_cred_PJ_6M or Lim_cred_PJ_6M is null then 5 else
	case when Lim_cred_PJ_6M <= Lim_cred_PJ_7M or Lim_cred_PJ_7M is null then 6 else
	case when Lim_cred_PJ_7M <= Lim_cred_PJ_8M or Lim_cred_PJ_8M is null then 7 else
	case when Lim_cred_PJ_8M <= Lim_cred_PJ_9M or Lim_cred_PJ_9M is null then 8 else
	case when Lim_cred_PJ_9M <= Lim_cred_PJ_10M or Lim_cred_PJ_10M is null then 9 else
	case when Lim_cred_PJ_10M <= Lim_cred_PJ_11M or Lim_cred_PJ_11M is null then 10 else
	case when Lim_cred_PJ_11M <= Lim_cred_PJ_12M or Lim_cred_PJ_12M is null then 11 else
	case when Lim_cred_PJ_12M <= Lim_cred_PJ_13M or Lim_cred_PJ_13M is null then 12 else
	case when Lim_cred_PJ_13M <= Lim_cred_PJ_14M or Lim_cred_PJ_14M is null then 13 else
	case when Lim_cred_PJ_14M <= Lim_cred_PJ_15M or Lim_cred_PJ_15M is null then 14 else
	case when Lim_cred_PJ_15M <= Lim_cred_PJ_16M or Lim_cred_PJ_16M is null then 15 else
	case when Lim_cred_PJ_16M <= Lim_cred_PJ_17M or Lim_cred_PJ_17M is null then 16 else
	case when Lim_cred_PJ_17M <= Lim_cred_PJ_18M or Lim_cred_PJ_18M is null then 17 else
	case when Lim_cred_PJ_18M <= Lim_cred_PJ_19M or Lim_cred_PJ_19M is null then 18 else
	case when Lim_cred_PJ_19M <= Lim_cred_PJ_20M or Lim_cred_PJ_20M is null then 19 else
	case when Lim_cred_PJ_20M <= Lim_cred_PJ_21M or Lim_cred_PJ_21M is null then 20 else
	case when Lim_cred_PJ_21M <= Lim_cred_PJ_22M or Lim_cred_PJ_22M is null then 21 else
	case when Lim_cred_PJ_22M <= Lim_cred_PJ_23M or Lim_cred_PJ_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_hth_lim_cred_pj
from(select direct_prospect_id,
	Num_Ops_PJ,
	Num_FIs_PJ,
	First_Relation_FI_PJ,
	sum(DebtPJ) filter (where data = data_referencia) as Long_Term_Debt_PJ_Curr,
	sum(DebtPJ) filter (where data = data_referencia - interval '1 month') as Long_Term_Debt_PJ_1M,
	sum(DebtPJ) filter (where data = data_referencia - interval '2 months') as Long_Term_Debt_PJ_2M,
	sum(DebtPJ) filter (where data = data_referencia - interval '3 months') as Long_Term_Debt_PJ_3M,
	sum(DebtPJ) filter (where data = data_referencia - interval '4 months') as Long_Term_Debt_PJ_4M,
	sum(DebtPJ) filter (where data = data_referencia - interval '5 months') as Long_Term_Debt_PJ_5M,
	sum(DebtPJ) filter (where data = data_referencia - interval '6 months') as Long_Term_Debt_PJ_6M,
	sum(DebtPJ) filter (where data = data_referencia - interval '7 months') as Long_Term_Debt_PJ_7M,
	sum(DebtPJ) filter (where data = data_referencia - interval '8 months') as Long_Term_Debt_PJ_8M,
	sum(DebtPJ) filter (where data = data_referencia - interval '9 months') as Long_Term_Debt_PJ_9M,
	sum(DebtPJ) filter (where data = data_referencia - interval '10 months') as Long_Term_Debt_PJ_10M,
	sum(DebtPJ) filter (where data = data_referencia - interval '11 months') as Long_Term_Debt_PJ_11M,
	sum(DebtPJ) filter (where data = data_referencia - interval '12 months') as Long_Term_Debt_PJ_12M,
	sum(DebtPJ) filter (where data = data_referencia - interval '13 months') as Long_Term_Debt_PJ_13M,
	sum(DebtPJ) filter (where data = data_referencia - interval '14 months') as Long_Term_Debt_PJ_14M,
	sum(DebtPJ) filter (where data = data_referencia - interval '15 months') as Long_Term_Debt_PJ_15M,
	sum(DebtPJ) filter (where data = data_referencia - interval '16 months') as Long_Term_Debt_PJ_16M,
	sum(DebtPJ) filter (where data = data_referencia - interval '17 months') as Long_Term_Debt_PJ_17M,
	sum(DebtPJ) filter (where data = data_referencia - interval '18 months') as Long_Term_Debt_PJ_18M,
	sum(DebtPJ) filter (where data = data_referencia - interval '19 months') as Long_Term_Debt_PJ_19M,
	sum(DebtPJ) filter (where data = data_referencia - interval '20 months') as Long_Term_Debt_PJ_20M,
	sum(DebtPJ) filter (where data = data_referencia - interval '21 months') as Long_Term_Debt_PJ_21M,
	sum(DebtPJ) filter (where data = data_referencia - interval '22 months') as Long_Term_Debt_PJ_22M,
	sum(DebtPJ) filter (where data = data_referencia - interval '23 months') as Long_Term_Debt_PJ_23M,
	sum(DebtPJ) filter (where data <= data_referencia) as Ever_long_term_debt_PJ,
	max(DebtPJ) filter (where data <= data_referencia) as Max_Long_Term_Debt_PJ,
	sum(VencidoPJ) filter (where data = data_referencia) as Overdue_PJ_Curr,
	extract(year from age(min(data_referencia), max(data) filter (where data <= data_referencia and VencidoPJ > 0)))*12 + extract(month from age(min(data_referencia), max(data) filter (where data <= data_referencia and VencidoPJ > 0))) as Months_Since_Last_Overdue_PJ,
	count(VencidoPJ) filter (where data <= data_referencia and VencidoPJ > 0) as Months_Overdue_PJ,
	max(VencidoPJ) filter (where data <= data_referencia) as Max_Overdue_PJ,
	sum(PrejuizoPJ) filter (where data = data_referencia) as Default_PJ_Curr,
	extract(year from age(min(data_referencia), max(data) filter (where data <= data_referencia and PrejuizoPJ > 0)))*12 + extract(month from age(min(data_referencia), max(data) filter (where data <= data_referencia and PrejuizoPJ > 0)))  as Months_Since_Last_Default_PJ,
	count(PrejuizoPJ) filter (where data <= data_referencia and PrejuizoPJ > 0) as Months_Default_PJ,
	max(PrejuizoPJ) filter (where data <= data_referencia) as Max_Default_PJ,
	count(*) filter (where data <= data_referencia) as Qtd_meses_escopo_pj,
	sum(LimCredPJ) filter (where data = data_referencia) as Lim_Cred_PJ_Curr,
	sum(LimCredPJ) filter (where data = data_referencia - interval '1 month') as Lim_Cred_PJ_1M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '2 months') as Lim_Cred_PJ_2M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '3 months') as Lim_Cred_PJ_3M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '4 months') as Lim_Cred_PJ_4M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '5 months') as Lim_Cred_PJ_5M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '6 months') as Lim_Cred_PJ_6M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '7 months') as Lim_Cred_PJ_7M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '8 months') as Lim_Cred_PJ_8M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '9 months') as Lim_Cred_PJ_9M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '10 months') as Lim_Cred_PJ_10M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '11 months') as Lim_Cred_PJ_11M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '12 months') as Lim_Cred_PJ_12M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '13 months') as Lim_Cred_PJ_13M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '14 months') as Lim_Cred_PJ_14M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '15 months') as Lim_Cred_PJ_15M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '16 months') as Lim_Cred_PJ_16M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '17 months') as Lim_Cred_PJ_17M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '18 months') as Lim_Cred_PJ_18M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '19 months') as Lim_Cred_PJ_19M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '20 months') as Lim_Cred_PJ_20M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '21 months') as Lim_Cred_PJ_21M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '22 months') as Lim_Cred_PJ_22M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '23 months') as Lim_Cred_PJ_23M,
	sum(LimCredPJ) filter (where data <= data_referencia) as Ever_Lim_Cred_PJ,
	max(LimCredPJ) filter (where data <= data_referencia) as Max_Lim_Cred_PJ
	from(select direct_prospect_id,
			case when extract(day from loan_date) >= 16 then case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end else case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end end as data_referencia,
			to_date(jsonb_array_elements(cc.data->'historico'->'Carteira de Crédito')->>'data','mm/yyyy') as data,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Carteira de Crédito')->>'valor','-','0'),'.',''),',','.')::float DebtPJ,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Vencido')->>'valor','-','0'),'.',''),',','.')::float VencidoPJ,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Prejuízo')->>'valor','-','0'),'.',''),',','.')::float PrejuizoPJ,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Limite de Crédito')->>'valor','-','0'),'.',''),',','.')::float LimCredPJ,
			replace(replace(case when position('Quantidade de Operações' in cc.raw) > 0 then substring(cc.raw,position('Quantidade de Operações' in cc.raw)+length('Quantidade de Operações</div></td><td class="fu3" width="50%"><b>'),3) else null end,'<',''),'/','')::int as Num_Ops_PJ,
			replace(replace(case when position('Quantidade de IFs em que o Cliente possui Operações' in cc.raw) > 0 then substring(cc.raw,position('Quantidade de IFs em que o Cliente possui Operações' in cc.raw)+
				length('Quantidade de IFs em que o Cliente possui Operações</div></td><td class="fu3" width="50%"><b>'),3) else null end,'<',''),'/','')::int as Num_FIs_PJ,
			extract(year from age(loan_date,to_date(replace(replace(replace(case when position('Data de Início de Relacionamento com a IF' in cc.raw) > 0 then substring(cc.raw,position('Data de Início de Relacionamento com a IF' in cc.raw)+
				length('Data de Início de Relacionamento com a IF</div></td><td class="fu3" width="50%"><b>'),10) else null end,'<',''),'/',''),'-','0'),'ddmmyyyy')))::int as First_Relation_FI_PJ
		from credito_coleta cc
		join(select dp.direct_prospect_id, lr.loan_date,cnpj,
				max(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id) filter (where to_date(left(data->>'criado_em',10),'yyyy-mm-dd') <= lr.loan_date and (cc.data->'historico' is not null or cc.data->'erro' is not null)) as max_date, 
				min(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id) filter (where data->'historico' is not null or cc.data->'erro' is not null) as min_date
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join offers o on o.direct_prospect_id = dp.direct_prospect_id
			join loan_requests lr on lr.offer_id = o.offer_id 
			where documento_tipo = 'CNPJ' and tipo = 'SCR' and lr.status = 'ACCEPTED'
			group by 1,2,3) as t1 on left(cc.documento,8) = left(cnpj,8) and case when max_date is null then to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id = min_date else to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id = max_date end) as t2
	group by 1,2,3,4) as t3) as SCRHistPJ on SCRHistPJ.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SCR PF
left join(select direct_prospect_id,
	Num_Ops_pf::float,
	Num_FIs_pf::float,
	First_Relation_FI_pf::float,
	Long_Term_Debt_pf_Curr::float,
	Long_Term_Debt_pf_1M::float,
	Long_Term_Debt_pf_2M::float,
	Long_Term_Debt_pf_3M::float,
	Long_Term_Debt_pf_4M::float,
	Long_Term_Debt_pf_5M::float,
	Long_Term_Debt_pf_6M::float,
	Long_Term_Debt_pf_7M::float,
	Long_Term_Debt_pf_8M::float,
	Long_Term_Debt_pf_9M::float,
	Long_Term_Debt_pf_10M::float,
	Long_Term_Debt_pf_11M::float,
	Ever_long_term_debt_pf::float,
	Max_Long_Term_Debt_pf::float,
	Overdue_pf_Curr::float,
	Months_Since_Last_Overdue_pf::float,
	Months_Overdue_pf::float,
	Max_Overdue_pf::float,
	Default_pf_Curr::float,
	Months_Since_Last_Default_pf::float,
	Months_Default_pf::float,
	Max_Default_pf::float,
	Qtd_meses_escopo_pf::float,
	case when Long_Term_Debt_pf_22M > Long_Term_Debt_pf_23M then 1 else 0 end +
	case when Long_Term_Debt_pf_21M > Long_Term_Debt_pf_22M then 1 else 0 end +
	case when Long_Term_Debt_pf_20M > Long_Term_Debt_pf_21M then 1 else 0 end +
	case when Long_Term_Debt_pf_19M > Long_Term_Debt_pf_20M then 1 else 0 end +
	case when Long_Term_Debt_pf_18M > Long_Term_Debt_pf_19M then 1 else 0 end +
	case when Long_Term_Debt_pf_17M > Long_Term_Debt_pf_18M then 1 else 0 end +
	case when Long_Term_Debt_pf_16M > Long_Term_Debt_pf_17M then 1 else 0 end +
	case when Long_Term_Debt_pf_15M > Long_Term_Debt_pf_16M then 1 else 0 end +
	case when Long_Term_Debt_pf_14M > Long_Term_Debt_pf_15M then 1 else 0 end +
	case when Long_Term_Debt_pf_13M > Long_Term_Debt_pf_14M then 1 else 0 end +
	case when Long_Term_Debt_pf_12M > Long_Term_Debt_pf_13M then 1 else 0 end +
	case when Long_Term_Debt_pf_11M > Long_Term_Debt_pf_12M then 1 else 0 end +
	case when Long_Term_Debt_pf_10M > Long_Term_Debt_pf_11M then 1 else 0 end +
	case when Long_Term_Debt_pf_9M > Long_Term_Debt_pf_10M then 1 else 0 end +
	case when Long_Term_Debt_pf_8M > Long_Term_Debt_pf_9M then 1 else 0 end +
	case when Long_Term_Debt_pf_7M > Long_Term_Debt_pf_8M then 1 else 0 end +
	case when Long_Term_Debt_pf_6M > Long_Term_Debt_pf_7M then 1 else 0 end +
	case when Long_Term_Debt_pf_5M > Long_Term_Debt_pf_6M then 1 else 0 end +
	case when Long_Term_Debt_pf_4M > Long_Term_Debt_pf_5M then 1 else 0 end +
	case when Long_Term_Debt_pf_3M > Long_Term_Debt_pf_4M then 1 else 0 end +
	case when Long_Term_Debt_pf_2M > Long_Term_Debt_pf_3M then 1 else 0 end +
	case when Long_Term_Debt_pf_1M > Long_Term_Debt_pf_2M then 1 else 0 end +
	case when Long_Term_Debt_pf_Curr > Long_Term_Debt_pf_1M then 1 else 0 end::float as Meses_Aumento_Dividapf,
	case when Long_Term_Debt_pf_22M < Long_Term_Debt_pf_23M then 1 else 0 end +
	case when Long_Term_Debt_pf_21M < Long_Term_Debt_pf_22M then 1 else 0 end +
	case when Long_Term_Debt_pf_20M < Long_Term_Debt_pf_21M then 1 else 0 end +
	case when Long_Term_Debt_pf_19M < Long_Term_Debt_pf_20M then 1 else 0 end +
	case when Long_Term_Debt_pf_18M < Long_Term_Debt_pf_19M then 1 else 0 end +
	case when Long_Term_Debt_pf_17M < Long_Term_Debt_pf_18M then 1 else 0 end +
	case when Long_Term_Debt_pf_16M < Long_Term_Debt_pf_17M then 1 else 0 end +
	case when Long_Term_Debt_pf_15M < Long_Term_Debt_pf_16M then 1 else 0 end +
	case when Long_Term_Debt_pf_14M < Long_Term_Debt_pf_15M then 1 else 0 end +
	case when Long_Term_Debt_pf_13M < Long_Term_Debt_pf_14M then 1 else 0 end +
	case when Long_Term_Debt_pf_12M < Long_Term_Debt_pf_13M then 1 else 0 end +
	case when Long_Term_Debt_pf_11M < Long_Term_Debt_pf_12M then 1 else 0 end +
	case when Long_Term_Debt_pf_10M < Long_Term_Debt_pf_11M then 1 else 0 end +
	case when Long_Term_Debt_pf_9M < Long_Term_Debt_pf_10M then 1 else 0 end +
	case when Long_Term_Debt_pf_8M < Long_Term_Debt_pf_9M then 1 else 0 end +
	case when Long_Term_Debt_pf_7M < Long_Term_Debt_pf_8M then 1 else 0 end +
	case when Long_Term_Debt_pf_6M < Long_Term_Debt_pf_7M then 1 else 0 end +
	case when Long_Term_Debt_pf_5M < Long_Term_Debt_pf_6M then 1 else 0 end +
	case when Long_Term_Debt_pf_4M < Long_Term_Debt_pf_5M then 1 else 0 end +
	case when Long_Term_Debt_pf_3M < Long_Term_Debt_pf_4M then 1 else 0 end +
	case when Long_Term_Debt_pf_2M < Long_Term_Debt_pf_3M then 1 else 0 end +
	case when Long_Term_Debt_pf_1M < Long_Term_Debt_pf_2M then 1 else 0 end +
	case when Long_Term_Debt_pf_Curr < Long_Term_Debt_pf_1M then 1 else 0 end::float as Meses_Reducao_Dividapf,
	(case when Long_Term_Debt_pf_22M < Long_Term_Debt_pf_23M then Long_Term_Debt_pf_22M - Long_Term_Debt_pf_23M else 0 end +
	case when Long_Term_Debt_pf_21M < Long_Term_Debt_pf_22M then Long_Term_Debt_pf_21M - Long_Term_Debt_pf_22M else 0 end +
	case when Long_Term_Debt_pf_20M < Long_Term_Debt_pf_21M then Long_Term_Debt_pf_20M - Long_Term_Debt_pf_21M else 0 end +
	case when Long_Term_Debt_pf_19M < Long_Term_Debt_pf_20M then Long_Term_Debt_pf_19M - Long_Term_Debt_pf_20M else 0 end +
	case when Long_Term_Debt_pf_18M < Long_Term_Debt_pf_19M then Long_Term_Debt_pf_18M - Long_Term_Debt_pf_19M else 0 end +
	case when Long_Term_Debt_pf_17M < Long_Term_Debt_pf_18M then Long_Term_Debt_pf_17M - Long_Term_Debt_pf_18M else 0 end +
	case when Long_Term_Debt_pf_16M < Long_Term_Debt_pf_17M then Long_Term_Debt_pf_16M - Long_Term_Debt_pf_17M else 0 end +
	case when Long_Term_Debt_pf_15M < Long_Term_Debt_pf_16M then Long_Term_Debt_pf_15M - Long_Term_Debt_pf_16M else 0 end +
	case when Long_Term_Debt_pf_14M < Long_Term_Debt_pf_15M then Long_Term_Debt_pf_14M - Long_Term_Debt_pf_15M else 0 end +
	case when Long_Term_Debt_pf_13M < Long_Term_Debt_pf_14M then Long_Term_Debt_pf_13M - Long_Term_Debt_pf_14M else 0 end +
	case when Long_Term_Debt_pf_12M < Long_Term_Debt_pf_13M then Long_Term_Debt_pf_12M - Long_Term_Debt_pf_13M else 0 end +
	case when Long_Term_Debt_pf_11M < Long_Term_Debt_pf_12M then Long_Term_Debt_pf_11M - Long_Term_Debt_pf_12M else 0 end +
	case when Long_Term_Debt_pf_10M < Long_Term_Debt_pf_11M then Long_Term_Debt_pf_10M - Long_Term_Debt_pf_11M else 0 end +
	case when Long_Term_Debt_pf_9M < Long_Term_Debt_pf_10M then Long_Term_Debt_pf_9M - Long_Term_Debt_pf_10M else 0 end +
	case when Long_Term_Debt_pf_8M < Long_Term_Debt_pf_9M then Long_Term_Debt_pf_8M - Long_Term_Debt_pf_9M else 0 end +
	case when Long_Term_Debt_pf_7M < Long_Term_Debt_pf_8M then Long_Term_Debt_pf_7M - Long_Term_Debt_pf_8M else 0 end +
	case when Long_Term_Debt_pf_6M < Long_Term_Debt_pf_7M then Long_Term_Debt_pf_6M - Long_Term_Debt_pf_7M else 0 end +
	case when Long_Term_Debt_pf_5M < Long_Term_Debt_pf_6M then Long_Term_Debt_pf_5M - Long_Term_Debt_pf_6M else 0 end +
	case when Long_Term_Debt_pf_4M < Long_Term_Debt_pf_5M then Long_Term_Debt_pf_4M - Long_Term_Debt_pf_5M else 0 end +
	case when Long_Term_Debt_pf_3M < Long_Term_Debt_pf_4M then Long_Term_Debt_pf_3M - Long_Term_Debt_pf_4M else 0 end +
	case when Long_Term_Debt_pf_2M < Long_Term_Debt_pf_3M then Long_Term_Debt_pf_2M - Long_Term_Debt_pf_3M else 0 end +
	case when Long_Term_Debt_pf_1M < Long_Term_Debt_pf_2M then Long_Term_Debt_pf_1M - Long_Term_Debt_pf_2M else 0 end +
	case when Long_Term_Debt_pf_Curr < Long_Term_Debt_pf_1M then Long_Term_Debt_pf_Curr - Long_Term_Debt_pf_1M else 0 end)*(-1)::float as Saldo_Amort_Dividapf,
	case when Long_Term_Debt_pf_1M is null then null else
	case when Long_Term_Debt_pf_Curr >= Long_Term_Debt_pf_1M then 0 else
	case when Long_Term_Debt_pf_1M >= Long_Term_Debt_pf_2M or Long_Term_Debt_pf_2M is null then 1 else
	case when Long_Term_Debt_pf_2M >= Long_Term_Debt_pf_3M or Long_Term_Debt_pf_3M is null then 2 else
	case when Long_Term_Debt_pf_3M >= Long_Term_Debt_pf_4M or Long_Term_Debt_pf_4M is null then 3 else
	case when Long_Term_Debt_pf_4M >= Long_Term_Debt_pf_5M or Long_Term_Debt_pf_5M is null then 4 else
	case when Long_Term_Debt_pf_5M >= Long_Term_Debt_pf_6M or Long_Term_Debt_pf_6M is null then 5 else
	case when Long_Term_Debt_pf_6M >= Long_Term_Debt_pf_7M or Long_Term_Debt_pf_7M is null then 6 else
	case when Long_Term_Debt_pf_7M >= Long_Term_Debt_pf_8M or Long_Term_Debt_pf_8M is null then 7 else
	case when Long_Term_Debt_pf_8M >= Long_Term_Debt_pf_9M or Long_Term_Debt_pf_9M is null then 8 else
	case when Long_Term_Debt_pf_9M >= Long_Term_Debt_pf_10M or Long_Term_Debt_pf_10M is null then 9 else
	case when Long_Term_Debt_pf_10M >= Long_Term_Debt_pf_11M or Long_Term_Debt_pf_11M is null then 10 else
	case when Long_Term_Debt_pf_11M >= Long_Term_Debt_pf_12M or Long_Term_Debt_pf_12M is null then 11 else
	case when Long_Term_Debt_pf_12M >= Long_Term_Debt_pf_13M or Long_Term_Debt_pf_13M is null then 12 else
	case when Long_Term_Debt_pf_13M >= Long_Term_Debt_pf_14M or Long_Term_Debt_pf_14M is null then 13 else
	case when Long_Term_Debt_pf_14M >= Long_Term_Debt_pf_15M or Long_Term_Debt_pf_15M is null then 14 else
	case when Long_Term_Debt_pf_15M >= Long_Term_Debt_pf_16M or Long_Term_Debt_pf_16M is null then 15 else
	case when Long_Term_Debt_pf_16M >= Long_Term_Debt_pf_17M or Long_Term_Debt_pf_17M is null then 16 else
	case when Long_Term_Debt_pf_17M >= Long_Term_Debt_pf_18M or Long_Term_Debt_pf_18M is null then 17 else
	case when Long_Term_Debt_pf_18M >= Long_Term_Debt_pf_19M or Long_Term_Debt_pf_19M is null then 18 else
	case when Long_Term_Debt_pf_19M >= Long_Term_Debt_pf_20M or Long_Term_Debt_pf_20M is null then 19 else
	case when Long_Term_Debt_pf_20M >= Long_Term_Debt_pf_21M or Long_Term_Debt_pf_21M is null then 20 else
	case when Long_Term_Debt_pf_21M >= Long_Term_Debt_pf_22M or Long_Term_Debt_pf_22M is null then 21 else
	case when Long_Term_Debt_pf_22M >= Long_Term_Debt_pf_23M or Long_Term_Debt_pf_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_sth_pf,
	case when Long_Term_Debt_pf_1M is null then null else
	case when Long_Term_Debt_pf_Curr <= Long_Term_Debt_pf_1M then 0 else
	case when Long_Term_Debt_pf_1M <= Long_Term_Debt_pf_2M or Long_Term_Debt_pf_2M is null then 1 else
	case when Long_Term_Debt_pf_2M <= Long_Term_Debt_pf_3M or Long_Term_Debt_pf_3M is null then 2 else
	case when Long_Term_Debt_pf_3M <= Long_Term_Debt_pf_4M or Long_Term_Debt_pf_4M is null then 3 else
	case when Long_Term_Debt_pf_4M <= Long_Term_Debt_pf_5M or Long_Term_Debt_pf_5M is null then 4 else
	case when Long_Term_Debt_pf_5M <= Long_Term_Debt_pf_6M or Long_Term_Debt_pf_6M is null then 5 else
	case when Long_Term_Debt_pf_6M <= Long_Term_Debt_pf_7M or Long_Term_Debt_pf_7M is null then 6 else
	case when Long_Term_Debt_pf_7M <= Long_Term_Debt_pf_8M or Long_Term_Debt_pf_8M is null then 7 else
	case when Long_Term_Debt_pf_8M <= Long_Term_Debt_pf_9M or Long_Term_Debt_pf_9M is null then 8 else
	case when Long_Term_Debt_pf_9M <= Long_Term_Debt_pf_10M or Long_Term_Debt_pf_10M is null then 9 else
	case when Long_Term_Debt_pf_10M <= Long_Term_Debt_pf_11M or Long_Term_Debt_pf_11M is null then 10 else
	case when Long_Term_Debt_pf_11M <= Long_Term_Debt_pf_12M or Long_Term_Debt_pf_12M is null then 11 else
	case when Long_Term_Debt_pf_12M <= Long_Term_Debt_pf_13M or Long_Term_Debt_pf_13M is null then 12 else
	case when Long_Term_Debt_pf_13M <= Long_Term_Debt_pf_14M or Long_Term_Debt_pf_14M is null then 13 else
	case when Long_Term_Debt_pf_14M <= Long_Term_Debt_pf_15M or Long_Term_Debt_pf_15M is null then 14 else
	case when Long_Term_Debt_pf_15M <= Long_Term_Debt_pf_16M or Long_Term_Debt_pf_16M is null then 15 else
	case when Long_Term_Debt_pf_16M <= Long_Term_Debt_pf_17M or Long_Term_Debt_pf_17M is null then 16 else
	case when Long_Term_Debt_pf_17M <= Long_Term_Debt_pf_18M or Long_Term_Debt_pf_18M is null then 17 else
	case when Long_Term_Debt_pf_18M <= Long_Term_Debt_pf_19M or Long_Term_Debt_pf_19M is null then 18 else
	case when Long_Term_Debt_pf_19M <= Long_Term_Debt_pf_20M or Long_Term_Debt_pf_20M is null then 19 else
	case when Long_Term_Debt_pf_20M <= Long_Term_Debt_pf_21M or Long_Term_Debt_pf_21M is null then 20 else
	case when Long_Term_Debt_pf_21M <= Long_Term_Debt_pf_22M or Long_Term_Debt_pf_22M is null then 21 else
	case when Long_Term_Debt_pf_22M <= Long_Term_Debt_pf_23M or Long_Term_Debt_pf_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_hth_pf,
	Lim_Cred_pf_Curr::float,
	Lim_Cred_pf_1M::float,
	Lim_Cred_pf_2M::float,
	Lim_Cred_pf_3M::float,
	Lim_Cred_pf_4M::float,
	Lim_Cred_pf_5M::float,
	Lim_Cred_pf_6M::float,
	Lim_Cred_pf_7M::float,
	Lim_Cred_pf_8M::float,
	Lim_Cred_pf_9M::float,
	Lim_Cred_pf_10M::float,
	Lim_Cred_pf_11M::float,
	Ever_Lim_Cred_pf::float,
	Max_Lim_Cred_pf::float,
	case when Lim_cred_pf_22M > Lim_cred_pf_23M then 1 else 0 end +
	case when Lim_cred_pf_21M > Lim_cred_pf_22M then 1 else 0 end +
	case when Lim_cred_pf_20M > Lim_cred_pf_21M then 1 else 0 end +
	case when Lim_cred_pf_19M > Lim_cred_pf_20M then 1 else 0 end +
	case when Lim_cred_pf_18M > Lim_cred_pf_19M then 1 else 0 end +
	case when Lim_cred_pf_17M > Lim_cred_pf_18M then 1 else 0 end +
	case when Lim_cred_pf_16M > Lim_cred_pf_17M then 1 else 0 end +
	case when Lim_cred_pf_15M > Lim_cred_pf_16M then 1 else 0 end +
	case when Lim_cred_pf_14M > Lim_cred_pf_15M then 1 else 0 end +
	case when Lim_cred_pf_13M > Lim_cred_pf_14M then 1 else 0 end +
	case when Lim_cred_pf_12M > Lim_cred_pf_13M then 1 else 0 end +
	case when Lim_cred_pf_11M > Lim_cred_pf_12M then 1 else 0 end +
	case when Lim_cred_pf_10M > Lim_cred_pf_11M then 1 else 0 end +
	case when Lim_cred_pf_9M > Lim_cred_pf_10M then 1 else 0 end +
	case when Lim_cred_pf_8M > Lim_cred_pf_9M then 1 else 0 end +
	case when Lim_cred_pf_7M > Lim_cred_pf_8M then 1 else 0 end +
	case when Lim_cred_pf_6M > Lim_cred_pf_7M then 1 else 0 end +
	case when Lim_cred_pf_5M > Lim_cred_pf_6M then 1 else 0 end +
	case when Lim_cred_pf_4M > Lim_cred_pf_5M then 1 else 0 end +
	case when Lim_cred_pf_3M > Lim_cred_pf_4M then 1 else 0 end +
	case when Lim_cred_pf_2M > Lim_cred_pf_3M then 1 else 0 end +
	case when Lim_cred_pf_1M > Lim_cred_pf_2M then 1 else 0 end +
	case when Lim_cred_pf_Curr > Lim_cred_pf_1M then 1 else 0 end::float as Meses_Aumento_Lim_Cred_pf,
	case when Lim_cred_pf_22M < Lim_cred_pf_23M then 1 else 0 end +
	case when Lim_cred_pf_21M < Lim_cred_pf_22M then 1 else 0 end +
	case when Lim_cred_pf_20M < Lim_cred_pf_21M then 1 else 0 end +
	case when Lim_cred_pf_19M < Lim_cred_pf_20M then 1 else 0 end +
	case when Lim_cred_pf_18M < Lim_cred_pf_19M then 1 else 0 end +
	case when Lim_cred_pf_17M < Lim_cred_pf_18M then 1 else 0 end +
	case when Lim_cred_pf_16M < Lim_cred_pf_17M then 1 else 0 end +
	case when Lim_cred_pf_15M < Lim_cred_pf_16M then 1 else 0 end +
	case when Lim_cred_pf_14M < Lim_cred_pf_15M then 1 else 0 end +
	case when Lim_cred_pf_13M < Lim_cred_pf_14M then 1 else 0 end +
	case when Lim_cred_pf_12M < Lim_cred_pf_13M then 1 else 0 end +
	case when Lim_cred_pf_11M < Lim_cred_pf_12M then 1 else 0 end +
	case when Lim_cred_pf_10M < Lim_cred_pf_11M then 1 else 0 end +
	case when Lim_cred_pf_9M < Lim_cred_pf_10M then 1 else 0 end +
	case when Lim_cred_pf_8M < Lim_cred_pf_9M then 1 else 0 end +
	case when Lim_cred_pf_7M < Lim_cred_pf_8M then 1 else 0 end +
	case when Lim_cred_pf_6M < Lim_cred_pf_7M then 1 else 0 end +
	case when Lim_cred_pf_5M < Lim_cred_pf_6M then 1 else 0 end +
	case when Lim_cred_pf_4M < Lim_cred_pf_5M then 1 else 0 end +
	case when Lim_cred_pf_3M < Lim_cred_pf_4M then 1 else 0 end +
	case when Lim_cred_pf_2M < Lim_cred_pf_3M then 1 else 0 end +
	case when Lim_cred_pf_1M < Lim_cred_pf_2M then 1 else 0 end +
	case when Lim_cred_pf_Curr < Lim_cred_pf_1M then 1 else 0 end::float as Meses_Reducao_Lim_Cred_pf,
	case when Lim_cred_pf_1M is null then null else
	case when Lim_cred_pf_Curr >= Lim_cred_pf_1M or Lim_cred_pf_1M is null then 0 else
	case when Lim_cred_pf_1M >= Lim_cred_pf_2M or Lim_cred_pf_2M is null then 1 else
	case when Lim_cred_pf_2M >= Lim_cred_pf_3M or Lim_cred_pf_3M is null then 2 else
	case when Lim_cred_pf_3M >= Lim_cred_pf_4M or Lim_cred_pf_4M is null then 3 else
	case when Lim_cred_pf_4M >= Lim_cred_pf_5M or Lim_cred_pf_5M is null then 4 else
	case when Lim_cred_pf_5M >= Lim_cred_pf_6M or Lim_cred_pf_6M is null then 5 else
	case when Lim_cred_pf_6M >= Lim_cred_pf_7M or Lim_cred_pf_7M is null then 6 else
	case when Lim_cred_pf_7M >= Lim_cred_pf_8M or Lim_cred_pf_8M is null then 7 else
	case when Lim_cred_pf_8M >= Lim_cred_pf_9M or Lim_cred_pf_9M is null then 8 else
	case when Lim_cred_pf_9M >= Lim_cred_pf_10M or Lim_cred_pf_10M is null then 9 else
	case when Lim_cred_pf_10M >= Lim_cred_pf_11M or Lim_cred_pf_11M is null then 10 else
	case when Lim_cred_pf_11M >= Lim_cred_pf_12M or Lim_cred_pf_12M is null then 11 else
	case when Lim_cred_pf_12M >= Lim_cred_pf_13M or Lim_cred_pf_13M is null then 12 else
	case when Lim_cred_pf_13M >= Lim_cred_pf_14M or Lim_cred_pf_14M is null then 13 else
	case when Lim_cred_pf_14M >= Lim_cred_pf_15M or Lim_cred_pf_15M is null then 14 else
	case when Lim_cred_pf_15M >= Lim_cred_pf_16M or Lim_cred_pf_16M is null then 15 else
	case when Lim_cred_pf_16M >= Lim_cred_pf_17M or Lim_cred_pf_17M is null then 16 else
	case when Lim_cred_pf_17M >= Lim_cred_pf_18M or Lim_cred_pf_18M is null then 17 else
	case when Lim_cred_pf_18M >= Lim_cred_pf_19M or Lim_cred_pf_19M is null then 18 else
	case when Lim_cred_pf_19M >= Lim_cred_pf_20M or Lim_cred_pf_20M is null then 19 else
	case when Lim_cred_pf_20M >= Lim_cred_pf_21M or Lim_cred_pf_21M is null then 20 else
	case when Lim_cred_pf_21M >= Lim_cred_pf_22M or Lim_cred_pf_22M is null then 21 else
	case when Lim_cred_pf_22M >= Lim_cred_pf_23M or Lim_cred_pf_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_sth_lim_cred_pf,
	case when Lim_cred_pf_1M is null then null else
	case when Lim_cred_pf_Curr <= Lim_cred_pf_1M or Lim_cred_pf_1M is null then 0 else
	case when Lim_cred_pf_1M <= Lim_cred_pf_2M or Lim_cred_pf_2M is null then 1 else
	case when Lim_cred_pf_2M <= Lim_cred_pf_3M or Lim_cred_pf_3M is null then 2 else
	case when Lim_cred_pf_3M <= Lim_cred_pf_4M or Lim_cred_pf_4M is null then 3 else
	case when Lim_cred_pf_4M <= Lim_cred_pf_5M or Lim_cred_pf_5M is null then 4 else
	case when Lim_cred_pf_5M <= Lim_cred_pf_6M or Lim_cred_pf_6M is null then 5 else
	case when Lim_cred_pf_6M <= Lim_cred_pf_7M or Lim_cred_pf_7M is null then 6 else
	case when Lim_cred_pf_7M <= Lim_cred_pf_8M or Lim_cred_pf_8M is null then 7 else
	case when Lim_cred_pf_8M <= Lim_cred_pf_9M or Lim_cred_pf_9M is null then 8 else
	case when Lim_cred_pf_9M <= Lim_cred_pf_10M or Lim_cred_pf_10M is null then 9 else
	case when Lim_cred_pf_10M <= Lim_cred_pf_11M or Lim_cred_pf_11M is null then 10 else
	case when Lim_cred_pf_11M <= Lim_cred_pf_12M or Lim_cred_pf_12M is null then 11 else
	case when Lim_cred_pf_12M <= Lim_cred_pf_13M or Lim_cred_pf_13M is null then 12 else
	case when Lim_cred_pf_13M <= Lim_cred_pf_14M or Lim_cred_pf_14M is null then 13 else
	case when Lim_cred_pf_14M <= Lim_cred_pf_15M or Lim_cred_pf_15M is null then 14 else
	case when Lim_cred_pf_15M <= Lim_cred_pf_16M or Lim_cred_pf_16M is null then 15 else
	case when Lim_cred_pf_16M <= Lim_cred_pf_17M or Lim_cred_pf_17M is null then 16 else
	case when Lim_cred_pf_17M <= Lim_cred_pf_18M or Lim_cred_pf_18M is null then 17 else
	case when Lim_cred_pf_18M <= Lim_cred_pf_19M or Lim_cred_pf_19M is null then 18 else
	case when Lim_cred_pf_19M <= Lim_cred_pf_20M or Lim_cred_pf_20M is null then 19 else
	case when Lim_cred_pf_20M <= Lim_cred_pf_21M or Lim_cred_pf_21M is null then 20 else
	case when Lim_cred_pf_21M <= Lim_cred_pf_22M or Lim_cred_pf_22M is null then 21 else
	case when Lim_cred_pf_22M <= Lim_cred_pf_23M or Lim_cred_pf_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_hth_lim_cred_pf
from(select direct_prospect_id,
	Num_Ops_pf,
	Num_FIs_pf,
	First_Relation_FI_pf,
	sum(Debtpf) filter (where data = data_referencia) as Long_Term_Debt_pf_Curr,
	sum(Debtpf) filter (where data = data_referencia - interval '1 month') as Long_Term_Debt_pf_1M,
	sum(Debtpf) filter (where data = data_referencia - interval '2 months') as Long_Term_Debt_pf_2M,
	sum(Debtpf) filter (where data = data_referencia - interval '3 months') as Long_Term_Debt_pf_3M,
	sum(Debtpf) filter (where data = data_referencia - interval '4 months') as Long_Term_Debt_pf_4M,
	sum(Debtpf) filter (where data = data_referencia - interval '5 months') as Long_Term_Debt_pf_5M,
	sum(Debtpf) filter (where data = data_referencia - interval '6 months') as Long_Term_Debt_pf_6M,
	sum(Debtpf) filter (where data = data_referencia - interval '7 months') as Long_Term_Debt_pf_7M,
	sum(Debtpf) filter (where data = data_referencia - interval '8 months') as Long_Term_Debt_pf_8M,
	sum(Debtpf) filter (where data = data_referencia - interval '9 months') as Long_Term_Debt_pf_9M,
	sum(Debtpf) filter (where data = data_referencia - interval '10 months') as Long_Term_Debt_pf_10M,
	sum(Debtpf) filter (where data = data_referencia - interval '11 months') as Long_Term_Debt_pf_11M,
	sum(Debtpf) filter (where data = data_referencia - interval '12 months') as Long_Term_Debt_pf_12M,
	sum(Debtpf) filter (where data = data_referencia - interval '13 months') as Long_Term_Debt_pf_13M,
	sum(Debtpf) filter (where data = data_referencia - interval '14 months') as Long_Term_Debt_pf_14M,
	sum(Debtpf) filter (where data = data_referencia - interval '15 months') as Long_Term_Debt_pf_15M,
	sum(Debtpf) filter (where data = data_referencia - interval '16 months') as Long_Term_Debt_pf_16M,
	sum(Debtpf) filter (where data = data_referencia - interval '17 months') as Long_Term_Debt_pf_17M,
	sum(Debtpf) filter (where data = data_referencia - interval '18 months') as Long_Term_Debt_pf_18M,
	sum(Debtpf) filter (where data = data_referencia - interval '19 months') as Long_Term_Debt_pf_19M,
	sum(Debtpf) filter (where data = data_referencia - interval '20 months') as Long_Term_Debt_pf_20M,
	sum(Debtpf) filter (where data = data_referencia - interval '21 months') as Long_Term_Debt_pf_21M,
	sum(Debtpf) filter (where data = data_referencia - interval '22 months') as Long_Term_Debt_pf_22M,
	sum(Debtpf) filter (where data = data_referencia - interval '23 months') as Long_Term_Debt_pf_23M,
	sum(Debtpf) filter (where data <= data_referencia) as Ever_long_term_debt_pf,
	max(Debtpf) filter (where data <= data_referencia) as Max_Long_Term_Debt_pf,
	sum(Vencidopf) filter (where data = data_referencia) as Overdue_pf_Curr,
	extract(year from age(min(data_referencia), max(data) filter (where data <= data_referencia and Vencidopf > 0)))*12 + extract(month from age(min(data_referencia), max(data) filter (where data <= data_referencia and Vencidopf > 0))) as Months_Since_Last_Overdue_pf,
	count(Vencidopf) filter (where data <= data_referencia and Vencidopf > 0) as Months_Overdue_pf,
	max(Vencidopf) filter (where data <= data_referencia) as Max_Overdue_pf,
	sum(Prejuizopf) filter (where data = data_referencia) as Default_pf_Curr,
	extract(year from age(min(data_referencia), max(data) filter (where data <= data_referencia and Prejuizopf > 0)))*12 + extract(month from age(min(data_referencia), max(data) filter (where data <= data_referencia and Prejuizopf > 0)))  as Months_Since_Last_Default_pf,
	count(Prejuizopf) filter (where data <= data_referencia and Prejuizopf > 0) as Months_Default_pf,
	max(Prejuizopf) filter (where data <= data_referencia) as Max_Default_pf,
	count(*) filter (where data <= data_referencia) as Qtd_meses_escopo_pf,
	sum(LimCredpf) filter (where data = data_referencia) as Lim_Cred_pf_Curr,
	sum(LimCredpf) filter (where data = data_referencia - interval '1 month') as Lim_Cred_pf_1M,
	sum(LimCredpf) filter (where data = data_referencia - interval '2 months') as Lim_Cred_pf_2M,
	sum(LimCredpf) filter (where data = data_referencia - interval '3 months') as Lim_Cred_pf_3M,
	sum(LimCredpf) filter (where data = data_referencia - interval '4 months') as Lim_Cred_pf_4M,
	sum(LimCredpf) filter (where data = data_referencia - interval '5 months') as Lim_Cred_pf_5M,
	sum(LimCredpf) filter (where data = data_referencia - interval '6 months') as Lim_Cred_pf_6M,
	sum(LimCredpf) filter (where data = data_referencia - interval '7 months') as Lim_Cred_pf_7M,
	sum(LimCredpf) filter (where data = data_referencia - interval '8 months') as Lim_Cred_pf_8M,
	sum(LimCredpf) filter (where data = data_referencia - interval '9 months') as Lim_Cred_pf_9M,
	sum(LimCredpf) filter (where data = data_referencia - interval '10 months') as Lim_Cred_pf_10M,
	sum(LimCredpf) filter (where data = data_referencia - interval '11 months') as Lim_Cred_pf_11M,
	sum(LimCredpf) filter (where data = data_referencia - interval '12 months') as Lim_Cred_pf_12M,
	sum(LimCredpf) filter (where data = data_referencia - interval '13 months') as Lim_Cred_pf_13M,
	sum(LimCredpf) filter (where data = data_referencia - interval '14 months') as Lim_Cred_pf_14M,
	sum(LimCredpf) filter (where data = data_referencia - interval '15 months') as Lim_Cred_pf_15M,
	sum(LimCredpf) filter (where data = data_referencia - interval '16 months') as Lim_Cred_pf_16M,
	sum(LimCredpf) filter (where data = data_referencia - interval '17 months') as Lim_Cred_pf_17M,
	sum(LimCredpf) filter (where data = data_referencia - interval '18 months') as Lim_Cred_pf_18M,
	sum(LimCredpf) filter (where data = data_referencia - interval '19 months') as Lim_Cred_pf_19M,
	sum(LimCredpf) filter (where data = data_referencia - interval '20 months') as Lim_Cred_pf_20M,
	sum(LimCredpf) filter (where data = data_referencia - interval '21 months') as Lim_Cred_pf_21M,
	sum(LimCredpf) filter (where data = data_referencia - interval '22 months') as Lim_Cred_pf_22M,
	sum(LimCredpf) filter (where data = data_referencia - interval '23 months') as Lim_Cred_pf_23M,
	sum(LimCredpf) filter (where data <= data_referencia) as Ever_Lim_Cred_pf,
	max(LimCredpf) filter (where data <= data_referencia) as Max_Lim_Cred_pf
	from(select direct_prospect_id,
			case when extract(day from loan_date) >= 16 then case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end else case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end end as data_referencia,
			to_date(jsonb_array_elements(cc.data->'historico'->'Carteira de Crédito')->>'data','mm/yyyy') as data,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Carteira de Crédito')->>'valor','-','0'),'.',''),',','.')::float Debtpf,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Vencido')->>'valor','-','0'),'.',''),',','.')::float Vencidopf,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Prejuízo')->>'valor','-','0'),'.',''),',','.')::float Prejuizopf,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Limite de Crédito')->>'valor','-','0'),'.',''),',','.')::float LimCredpf,
			replace(replace(case when position('Quantidade de Operações' in cc.raw) > 0 then substring(cc.raw,position('Quantidade de Operações' in cc.raw)+length('Quantidade de Operações</div></td><td class="fu3" width="50%"><b>'),3) else null end,'<',''),'/','')::int as Num_Ops_pf,
			replace(replace(case when position('Quantidade de IFs em que o Cliente possui Operações' in cc.raw) > 0 then substring(cc.raw,position('Quantidade de IFs em que o Cliente possui Operações' in cc.raw)+
				length('Quantidade de IFs em que o Cliente possui Operações</div></td><td class="fu3" width="50%"><b>'),3) else null end,'<',''),'/','')::int as Num_FIs_pf,
			extract(year from age(loan_date,to_date(replace(replace(replace(case when position('Data de Início de Relacionamento com a IF' in cc.raw) > 0 then substring(cc.raw,position('Data de Início de Relacionamento com a IF' in cc.raw)+
				length('Data de Início de Relacionamento com a IF</div></td><td class="fu3" width="50%"><b>'),10) else null end,'<',''),'/',''),'-','0'),'ddmmyyyy')))::int as First_Relation_FI_pf
		from credito_coleta cc
		join(select dp.direct_prospect_id, lr.loan_date,cpf,
				max(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id) filter (where to_date(left(data->>'criado_em',10),'yyyy-mm-dd') <= lr.loan_date and (cc.data->'historico' is not null or cc.data->'erro' is not null)) as max_date, 
				min(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id) filter (where data->'historico' is not null or cc.data->'erro' is not null) as min_date
			from credito_coleta cc
			join direct_prospects dp on cc.documento = dp.cpf
			join offers o on o.direct_prospect_id = dp.direct_prospect_id
			join loan_requests lr on lr.offer_id = o.offer_id 
			where documento_tipo = 'CPF' and tipo = 'SCR' and lr.status = 'ACCEPTED'
			group by 1,2,3) as t1 on t1.cpf = cc.documento and case when max_date is null then to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id = min_date else to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id = max_date end) as t2
	group by 1,2,3,4) as t3) as SCRHistPF on SCRHistPF.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SCR DIREITOS CREDITORIOS PJ
left join (select direct_prospect_id,
	Invoice_Disc_PJ_Curr::float,
	Invoice_Disc_PJ_1M::float,
	Invoice_Disc_PJ_2M::float,
	Invoice_Disc_PJ_3M::float,
	Invoice_Disc_PJ_4M::float,
	Invoice_Disc_PJ_5M::float,
	Invoice_Disc_PJ_6M::float,
	Invoice_Disc_PJ_7M::float,
	Invoice_Disc_PJ_8M::float,
	Invoice_Disc_PJ_9M::float,
	Invoice_Disc_PJ_10M::float,
	Invoice_Disc_PJ_11M::float,
	max_invoice_disc::float,
	Ever_invoice_disc::float,
	case when invoice_disc_pj_10M < invoice_disc_pj_11M then 1 else 0 end +
	case when invoice_disc_pj_9M < invoice_disc_pj_10M then 1 else 0 end +
	case when invoice_disc_pj_8M < invoice_disc_pj_9M then 1 else 0 end +
	case when invoice_disc_pj_7M < invoice_disc_pj_8M then 1 else 0 end +
	case when invoice_disc_pj_6M < invoice_disc_pj_7M then 1 else 0 end +
	case when invoice_disc_pj_5M < invoice_disc_pj_6M then 1 else 0 end +
	case when invoice_disc_pj_4M < invoice_disc_pj_5M then 1 else 0 end +
	case when invoice_disc_pj_3M < invoice_disc_pj_4M then 1 else 0 end +
	case when invoice_disc_pj_2M < invoice_disc_pj_3M then 1 else 0 end +
	case when invoice_disc_pj_1M < invoice_disc_pj_2M then 1 else 0 end +
	case when invoice_disc_pj_Curr < invoice_disc_pj_1M then 1 else 0 end::float as Meses_Reducao_invoice_disc,
	case when invoice_disc_pj_10M > invoice_disc_pj_11M then 1 else 0 end +
	case when invoice_disc_pj_9M > invoice_disc_pj_10M then 1 else 0 end +
	case when invoice_disc_pj_8M > invoice_disc_pj_9M then 1 else 0 end +
	case when invoice_disc_pj_7M > invoice_disc_pj_8M then 1 else 0 end +
	case when invoice_disc_pj_6M > invoice_disc_pj_7M then 1 else 0 end +
	case when invoice_disc_pj_5M > invoice_disc_pj_6M then 1 else 0 end +
	case when invoice_disc_pj_4M > invoice_disc_pj_5M then 1 else 0 end +
	case when invoice_disc_pj_3M > invoice_disc_pj_4M then 1 else 0 end +
	case when invoice_disc_pj_2M > invoice_disc_pj_3M then 1 else 0 end +
	case when invoice_disc_pj_1M > invoice_disc_pj_2M then 1 else 0 end +
	case when invoice_disc_pj_Curr > invoice_disc_pj_1M then 1 else 0 end::float as Meses_Aumento_invoice_disc,
	case when invoice_disc_pj_1M is null then null else
	case when invoice_disc_pj_Curr >= invoice_disc_pj_1M or invoice_disc_pj_1M is null then 0 else
	case when invoice_disc_pj_1M >= invoice_disc_pj_2M or invoice_disc_pj_2M is null then 1 else
	case when invoice_disc_pj_2M >= invoice_disc_pj_3M or invoice_disc_pj_3M is null then 2 else
	case when invoice_disc_pj_3M >= invoice_disc_pj_4M or invoice_disc_pj_4M is null then 3 else
	case when invoice_disc_pj_4M >= invoice_disc_pj_5M or invoice_disc_pj_5M is null then 4 else
	case when invoice_disc_pj_5M >= invoice_disc_pj_6M or invoice_disc_pj_6M is null then 5 else
	case when invoice_disc_pj_6M >= invoice_disc_pj_7M or invoice_disc_pj_7M is null then 6 else
	case when invoice_disc_pj_7M >= invoice_disc_pj_8M or invoice_disc_pj_8M is null then 7 else
	case when invoice_disc_pj_8M >= invoice_disc_pj_9M or invoice_disc_pj_9M is null then 8 else
	case when invoice_disc_pj_9M >= invoice_disc_pj_10M or invoice_disc_pj_10M is null then 9 else
	case when invoice_disc_pj_10M >= invoice_disc_pj_11M or invoice_disc_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_invoice_disc,
	case when invoice_disc_pj_1M is null then null else
	case when invoice_disc_pj_Curr <= invoice_disc_pj_1M or invoice_disc_pj_1M is null then 0 else
	case when invoice_disc_pj_1M <= invoice_disc_pj_2M or invoice_disc_pj_2M is null then 1 else
	case when invoice_disc_pj_2M <= invoice_disc_pj_3M or invoice_disc_pj_3M is null then 2 else
	case when invoice_disc_pj_3M <= invoice_disc_pj_4M or invoice_disc_pj_4M is null then 3 else
	case when invoice_disc_pj_4M <= invoice_disc_pj_5M or invoice_disc_pj_5M is null then 4 else
	case when invoice_disc_pj_5M <= invoice_disc_pj_6M or invoice_disc_pj_6M is null then 5 else
	case when invoice_disc_pj_6M <= invoice_disc_pj_7M or invoice_disc_pj_7M is null then 6 else
	case when invoice_disc_pj_7M <= invoice_disc_pj_8M or invoice_disc_pj_8M is null then 7 else
	case when invoice_disc_pj_8M <= invoice_disc_pj_9M or invoice_disc_pj_9M is null then 8 else
	case when invoice_disc_pj_9M <= invoice_disc_pj_10M or invoice_disc_pj_10M is null then 9 else
	case when invoice_disc_pj_10M <= invoice_disc_pj_11M or invoice_disc_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_invoice_disc,
	qtd_meses_invoice_disc::float
from(select direct_prospect_id,
	sum(valor) filter (where to_date = data_referencia) as Invoice_Disc_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months') as Invoice_Disc_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months') as Invoice_Disc_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months') as Invoice_Disc_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months') as Invoice_Disc_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months') as Invoice_Disc_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months') as Invoice_Disc_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months') as Invoice_Disc_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months') as Invoice_Disc_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months') as Invoice_Disc_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months') as Invoice_Disc_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months') as Invoice_Disc_PJ_11M,
	max(valor) filter (where to_date <= data_referencia) as max_invoice_disc,
	sum(valor) filter (where to_date <= data_referencia) as Ever_invoice_disc,
	count(*) filter (where to_date <= data_referencia) as qtd_meses_invoice_disc
	from(select direct_prospect_id, to_date(data,'mm/yyyy'),case when valor is null then 0 else valor end as valor,data_referencia
		from (select direct_prospect_id,(jsonb_populate_recordset(null::meu2, serie)).*,data_referencia
			from (select direct_prospect_id,(jsonb_populate_recordset( NULL ::meu, data #> '{por_modalidade}')).*,
					case when extract(day from loan_date) >= 16 then case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end else case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end end as data_referencia
				from credito_coleta cc
				join(select dp.direct_prospect_id, lr.loan_date,cnpj,
						max(to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS')) filter (where to_date(left(cc.data->>'criado_em',10),'yyyy-mm-dd') <= lr.loan_date and cc.data->'historico' is not null) as max_date, 
						min(to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS')) filter (where cc.data->'historico' is not null) as min_date
					from credito_coleta cc
					join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
					join offers o on o.direct_prospect_id = dp.direct_prospect_id
					join loan_requests lr on lr.offer_id = o.offer_id 
					where documento_tipo = 'CNPJ' and tipo = 'SCR' and lr.status = 'ACCEPTED'
					group by 1,2,3) as t1 on left(cc.documento,8) = left(cnpj,8) and case when max_date is null then to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS') = min_date else 
						to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS') = max_date end) as t2
		where nome = 'Direitos Creditórios Descontados') as t3) as t4
		group by 1) as t5) as SCRDirCred on SCRDirCred.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SCR EMPRESTIMOS ROTATIVO PJ
left join (select direct_prospect_id,
	overdraft_PJ_Curr::float,
	overdraft_PJ_1M::float,
	overdraft_PJ_2M::float,
	overdraft_PJ_3M::float,
	overdraft_PJ_4M::float,
	overdraft_PJ_5M::float,
	overdraft_PJ_6M::float,
	overdraft_PJ_7M::float,
	overdraft_PJ_8M::float,
	overdraft_PJ_9M::float,
	overdraft_PJ_10M::float,
	overdraft_PJ_11M::float,
	max_overdraft::float,
	Ever_overdraft::float,
	case when overdraft_pj_10M < overdraft_pj_11M then 1 else 0 end +
	case when overdraft_pj_9M < overdraft_pj_10M then 1 else 0 end +
	case when overdraft_pj_8M < overdraft_pj_9M then 1 else 0 end +
	case when overdraft_pj_7M < overdraft_pj_8M then 1 else 0 end +
	case when overdraft_pj_6M < overdraft_pj_7M then 1 else 0 end +
	case when overdraft_pj_5M < overdraft_pj_6M then 1 else 0 end +
	case when overdraft_pj_4M < overdraft_pj_5M then 1 else 0 end +
	case when overdraft_pj_3M < overdraft_pj_4M then 1 else 0 end +
	case when overdraft_pj_2M < overdraft_pj_3M then 1 else 0 end +
	case when overdraft_pj_1M < overdraft_pj_2M then 1 else 0 end +
	case when overdraft_pj_Curr < overdraft_pj_1M then 1 else 0 end::float as Meses_Reducao_overdraft,
	case when overdraft_pj_10M > overdraft_pj_11M then 1 else 0 end +
	case when overdraft_pj_9M > overdraft_pj_10M then 1 else 0 end +
	case when overdraft_pj_8M > overdraft_pj_9M then 1 else 0 end +
	case when overdraft_pj_7M > overdraft_pj_8M then 1 else 0 end +
	case when overdraft_pj_6M > overdraft_pj_7M then 1 else 0 end +
	case when overdraft_pj_5M > overdraft_pj_6M then 1 else 0 end +
	case when overdraft_pj_4M > overdraft_pj_5M then 1 else 0 end +
	case when overdraft_pj_3M > overdraft_pj_4M then 1 else 0 end +
	case when overdraft_pj_2M > overdraft_pj_3M then 1 else 0 end +
	case when overdraft_pj_1M > overdraft_pj_2M then 1 else 0 end +
	case when overdraft_pj_Curr > overdraft_pj_1M then 1 else 0 end::float as Meses_Aumento_overdraft,
	case when overdraft_pj_1M is null then null else
	case when overdraft_pj_Curr >= overdraft_pj_1M or overdraft_pj_1M is null then 0 else
	case when overdraft_pj_1M >= overdraft_pj_2M or overdraft_pj_2M is null then 1 else
	case when overdraft_pj_2M >= overdraft_pj_3M or overdraft_pj_3M is null then 2 else
	case when overdraft_pj_3M >= overdraft_pj_4M or overdraft_pj_4M is null then 3 else
	case when overdraft_pj_4M >= overdraft_pj_5M or overdraft_pj_5M is null then 4 else
	case when overdraft_pj_5M >= overdraft_pj_6M or overdraft_pj_6M is null then 5 else
	case when overdraft_pj_6M >= overdraft_pj_7M or overdraft_pj_7M is null then 6 else
	case when overdraft_pj_7M >= overdraft_pj_8M or overdraft_pj_8M is null then 7 else
	case when overdraft_pj_8M >= overdraft_pj_9M or overdraft_pj_9M is null then 8 else
	case when overdraft_pj_9M >= overdraft_pj_10M or overdraft_pj_10M is null then 9 else
	case when overdraft_pj_10M >= overdraft_pj_11M or overdraft_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_overdraft,
	case when overdraft_pj_1M is null then null else
	case when overdraft_pj_Curr <= overdraft_pj_1M or overdraft_pj_1M is null then 0 else
	case when overdraft_pj_1M <= overdraft_pj_2M or overdraft_pj_2M is null then 1 else
	case when overdraft_pj_2M <= overdraft_pj_3M or overdraft_pj_3M is null then 2 else
	case when overdraft_pj_3M <= overdraft_pj_4M or overdraft_pj_4M is null then 3 else
	case when overdraft_pj_4M <= overdraft_pj_5M or overdraft_pj_5M is null then 4 else
	case when overdraft_pj_5M <= overdraft_pj_6M or overdraft_pj_6M is null then 5 else
	case when overdraft_pj_6M <= overdraft_pj_7M or overdraft_pj_7M is null then 6 else
	case when overdraft_pj_7M <= overdraft_pj_8M or overdraft_pj_8M is null then 7 else
	case when overdraft_pj_8M <= overdraft_pj_9M or overdraft_pj_9M is null then 8 else
	case when overdraft_pj_9M <= overdraft_pj_10M or overdraft_pj_10M is null then 9 else
	case when overdraft_pj_10M <= overdraft_pj_11M or overdraft_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_overdraft,
	qtd_meses_overdraft::float
from(select direct_prospect_id,
	sum(valor) filter (where to_date = data_referencia) as overdraft_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months') as overdraft_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months') as overdraft_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months') as overdraft_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months') as overdraft_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months') as overdraft_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months') as overdraft_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months') as overdraft_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months') as overdraft_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months') as overdraft_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months') as overdraft_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months') as overdraft_PJ_11M,
	max(valor) filter (where to_date <= data_referencia) as max_overdraft,
	sum(valor) filter (where to_date <= data_referencia) as Ever_overdraft,
	count(*) filter (where to_date <= data_referencia) as qtd_meses_overdraft
	from(select direct_prospect_id, data_referencia, Lv1, to_date(data,'mm/yyyy'),sum(case when valor is null then 0 else valor end) as valor from (
		select direct_prospect_id,data_referencia, Lv1, nome as Lv2, (jsonb_populate_recordset(null::meu2, serie)).* from (	
		select direct_prospect_id,data_referencia, nome as Lv1, (jsonb_populate_recordset(null::meu, detalhes)).* from (
		select direct_prospect_id, (jsonb_populate_recordset( NULL ::lucas, data #> '{por_modalidade}')).*,
			case when extract(day from loan_date) >= 16 then case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end else case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end end as data_referencia
		from credito_coleta cc
		join(select dp.direct_prospect_id, lr.loan_date,cnpj,
				max(to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS')) filter (where to_date(left(cc.data->>'criado_em',10),'yyyy-mm-dd') <= lr.loan_date and cc.data->'historico' is not null) as max_date, 
				min(to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS')) filter (where cc.data->'historico' is not null) as min_date
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join offers o on o.direct_prospect_id = dp.direct_prospect_id
			join loan_requests lr on lr.offer_id = o.offer_id 
			where documento_tipo = 'CNPJ' and tipo = 'SCR' and lr.status = 'ACCEPTED'
			group by 1,2,3) as t1 on left(cc.documento,8) = left(cnpj,8) and case when max_date is null then to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS') = min_date else 
				to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS') = max_date end) as t2) as t3) as t4
		where lv1 = 'Empréstimos' and (lv2 = 'Cheque Especial' or lv2 = 'Conta Garantida')
		group by 1,2,3,4) as t5
	group by 1) as t6) as SCREmprest on SCREmprest.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SCR DIVIDA CONCORRENTE PF
left join(select direct_prospect_id, 
	qtd_meses_competitive_debt_pf::float,
	divida_atual::float as divida_curto_prazo_pf,
	max_competitive_debt_pf::float,
	competitive_debt_pf_Curr::float,
	competitive_debt_pf_1m::float,
	competitive_debt_pf_2m::float,
	competitive_debt_pf_3m::float,
	competitive_debt_pf_4m::float,
	competitive_debt_pf_5m::float,
	competitive_debt_pf_6m::float,
	competitive_debt_pf_7m::float,
	competitive_debt_pf_8m::float,
	competitive_debt_pf_9m::float,
	competitive_debt_pf_10m::float,
	competitive_debt_pf_11m::float,
	Ever_competitive_debt_pf::float,
	case when competitive_debt_pf_10M < competitive_debt_pf_11M then 1 else 0 end +
	case when competitive_debt_pf_9M < competitive_debt_pf_10M then 1 else 0 end +
	case when competitive_debt_pf_8M < competitive_debt_pf_9M then 1 else 0 end +
	case when competitive_debt_pf_7M < competitive_debt_pf_8M then 1 else 0 end +
	case when competitive_debt_pf_6M < competitive_debt_pf_7M then 1 else 0 end +
	case when competitive_debt_pf_5M < competitive_debt_pf_6M then 1 else 0 end +
	case when competitive_debt_pf_4M < competitive_debt_pf_5M then 1 else 0 end +
	case when competitive_debt_pf_3M < competitive_debt_pf_4M then 1 else 0 end +
	case when competitive_debt_pf_2M < competitive_debt_pf_3M then 1 else 0 end +
	case when competitive_debt_pf_1M < competitive_debt_pf_2M then 1 else 0 end +
	case when competitive_debt_pf_Curr < competitive_debt_pf_1M then 1 else 0 end::float as Meses_Reducao_competitive_debt_pf,
	case when competitive_debt_pf_10M > competitive_debt_pf_11M then 1 else 0 end +
	case when competitive_debt_pf_9M > competitive_debt_pf_10M then 1 else 0 end +
	case when competitive_debt_pf_8M > competitive_debt_pf_9M then 1 else 0 end +
	case when competitive_debt_pf_7M > competitive_debt_pf_8M then 1 else 0 end +
	case when competitive_debt_pf_6M > competitive_debt_pf_7M then 1 else 0 end +
	case when competitive_debt_pf_5M > competitive_debt_pf_6M then 1 else 0 end +
	case when competitive_debt_pf_4M > competitive_debt_pf_5M then 1 else 0 end +
	case when competitive_debt_pf_3M > competitive_debt_pf_4M then 1 else 0 end +
	case when competitive_debt_pf_2M > competitive_debt_pf_3M then 1 else 0 end +
	case when competitive_debt_pf_1M > competitive_debt_pf_2M then 1 else 0 end +
	case when competitive_debt_pf_Curr > competitive_debt_pf_1M then 1 else 0 end::float as Meses_Aumento_competitive_debt_pf,
	case when competitive_debt_pf_1M is null then null else
	case when competitive_debt_pf_Curr >= competitive_debt_pf_1M or competitive_debt_pf_1M is null then 0 else
	case when competitive_debt_pf_1M >= competitive_debt_pf_2M or competitive_debt_pf_2M is null then 1 else
	case when competitive_debt_pf_2M >= competitive_debt_pf_3M or competitive_debt_pf_3M is null then 2 else
	case when competitive_debt_pf_3M >= competitive_debt_pf_4M or competitive_debt_pf_4M is null then 3 else
	case when competitive_debt_pf_4M >= competitive_debt_pf_5M or competitive_debt_pf_5M is null then 4 else
	case when competitive_debt_pf_5M >= competitive_debt_pf_6M or competitive_debt_pf_6M is null then 5 else
	case when competitive_debt_pf_6M >= competitive_debt_pf_7M or competitive_debt_pf_7M is null then 6 else
	case when competitive_debt_pf_7M >= competitive_debt_pf_8M or competitive_debt_pf_8M is null then 7 else
	case when competitive_debt_pf_8M >= competitive_debt_pf_9M or competitive_debt_pf_9M is null then 8 else
	case when competitive_debt_pf_9M >= competitive_debt_pf_10M or competitive_debt_pf_10M is null then 9 else
	case when competitive_debt_pf_10M >= competitive_debt_pf_11M or competitive_debt_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_competitive_debt_pf,
	case when competitive_debt_pf_1M is null then null else
	case when competitive_debt_pf_Curr <= competitive_debt_pf_1M or competitive_debt_pf_1M is null then 0 else
	case when competitive_debt_pf_1M <= competitive_debt_pf_2M or competitive_debt_pf_2M is null then 1 else
	case when competitive_debt_pf_2M <= competitive_debt_pf_3M or competitive_debt_pf_3M is null then 2 else
	case when competitive_debt_pf_3M <= competitive_debt_pf_4M or competitive_debt_pf_4M is null then 3 else
	case when competitive_debt_pf_4M <= competitive_debt_pf_5M or competitive_debt_pf_5M is null then 4 else
	case when competitive_debt_pf_5M <= competitive_debt_pf_6M or competitive_debt_pf_6M is null then 5 else
	case when competitive_debt_pf_6M <= competitive_debt_pf_7M or competitive_debt_pf_7M is null then 6 else
	case when competitive_debt_pf_7M <= competitive_debt_pf_8M or competitive_debt_pf_8M is null then 7 else
	case when competitive_debt_pf_8M <= competitive_debt_pf_9M or competitive_debt_pf_9M is null then 8 else
	case when competitive_debt_pf_9M <= competitive_debt_pf_10M or competitive_debt_pf_10M is null then 9 else
	case when competitive_debt_pf_10M <= competitive_debt_pf_11M or competitive_debt_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_competitive_debt_pf,
	vehicle_debt_pf_Curr::float,
	vehicle_debt_pf_1M::float,
	vehicle_debt_pf_2m::float,
	vehicle_debt_pf_3m::float,
	vehicle_debt_pf_4m::float,
	vehicle_debt_pf_5m::float,
	Ever_vehicle_debt_pf
from(select direct_prospect_id, 
		divida_atual,
		count(*) filter (where data <= data_referencia) as qtd_meses_competitive_debt_pf,
		sum(financiamento_carro) filter (where data = data_referencia) as vehicle_debt_pf_Curr,
		sum(financiamento_carro) filter (where data = data_referencia - interval '1 month') as vehicle_debt_pf_1M,
		sum(financiamento_carro) filter (where data = data_referencia - interval '2 months') as vehicle_debt_pf_2m,
		sum(financiamento_carro) filter (where data = data_referencia - interval '3 months') as vehicle_debt_pf_3m,
		sum(financiamento_carro) filter (where data = data_referencia - interval '4 months') as vehicle_debt_pf_4m,
		sum(financiamento_carro) filter (where data = data_referencia - interval '5 months') as vehicle_debt_pf_5m,
		sum(financiamento_carro) filter (where data = data_referencia - interval '6 months') as vehicle_debt_pf_6m,
		sum(financiamento_carro) filter (where data = data_referencia - interval '7 months') as vehicle_debt_pf_7m,
		sum(financiamento_carro) filter (where data = data_referencia - interval '8 months') as vehicle_debt_pf_8m,
		sum(financiamento_carro) filter (where data = data_referencia - interval '9 months') as vehicle_debt_pf_9m,
		sum(financiamento_carro) filter (where data = data_referencia - interval '10 months') as vehicle_debt_pf_10m,
		sum(financiamento_carro) filter (where data = data_referencia - interval '11 months') as vehicle_debt_pf_11m,
		sum(financiamento_carro) filter (where data <= data_referencia) as Ever_vehicle_debt_pf,		
		max(divida_concorrente_pf) filter (where data <= data_referencia) as max_competitive_debt_pf,
		sum(divida_concorrente_pf) filter (where data = data_referencia) as competitive_debt_pf_Curr,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '1 month') as competitive_debt_pf_1M,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '2 months') as competitive_debt_pf_2m,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '3 months') as competitive_debt_pf_3m,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '4 months') as competitive_debt_pf_4m,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '5 months') as competitive_debt_pf_5m,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '6 months') as competitive_debt_pf_6m,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '7 months') as competitive_debt_pf_7m,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '8 months') as competitive_debt_pf_8m,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '9 months') as competitive_debt_pf_9m,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '10 months') as competitive_debt_pf_10m,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '11 months') as competitive_debt_pf_11m,
		sum(divida_concorrente_pf) filter (where data <= data_referencia) as Ever_competitive_debt_pf
		from(select direct_prospect_id,data_referencia,to_date as data,divida_atual,sum(valor) filter (where lv2 = 'Aquisição de Bens  Veículos Automotores') as financiamento_carro,
				sum(valor) filter (where lv1 not in ('Financiamentos Imobiliários','Direitos Creditórios Descontados','Adiantamentos a Depositantes','Outros Créditos') and lv1 not like '%(%' and lv2 not in('Aquisição de Bens  Veículos Automotores','Aquisição de Bens  Outros Bens')) as divida_concorrente_pf
			from(select direct_prospect_id, data_referencia,divida_atual,Lv1, Lv2, to_date(data,'mm/yyyy'), case when valor is null then 0 else valor end::float
				from(select direct_prospect_id, data_referencia,divida_atual,Lv1, nome as Lv2, (jsonb_populate_recordset(null::meu2, serie)).*
					from (select direct_prospect_id, data_referencia,case when data_base = data_referencia then divida_atual else null end as divida_atual,nome as Lv1, (jsonb_populate_recordset(null::meu, detalhes)).* 
						from (select direct_prospect_id, (data->'indicadores'->>'dividaAtual')::float as divida_atual, to_date(data->'data_base'->>'Data-Base','mm/yyyy') as data_base,data_referencia,(jsonb_populate_recordset( NULL ::lucas, data #> '{por_modalidade}')).*
						from (select direct_prospect_id, data,case when extract(day from loan_date) >= 16 then case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end else case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end end as data_referencia
							from credito_coleta cc
							join(select dp.direct_prospect_id, lr.loan_date,cpf,
									max(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id) filter (where to_date(left(data->>'criado_em',10),'yyyy-mm-dd') <= lr.loan_date and (cc.data->'historico' is not null or cc.data->'erro' is not null)) as max_date, 
									min(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id) filter (where data->'historico' is not null or cc.data->'erro' is not null) as min_date
								from credito_coleta cc
								join direct_prospects dp on cc.documento = dp.cpf
								join offers o on o.direct_prospect_id = dp.direct_prospect_id
								join loan_requests lr on lr.offer_id = o.offer_id 
								where documento_tipo = 'CPF' and tipo = 'SCR' and lr.status = 'ACCEPTED'
								group by 1,2,3) as t1 on t1.cpf = cc.documento and case when max_date is null then to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id = min_date else to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id = max_date end) as t2) as t3) as t4) as t5) as t6
		group by 1,2,3,4) as t7
	group by 1,2) as t8) as SCRDivnaoconc_pf on SCRDivnaoconc_pf.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SCR DIVIDA CONCORRENTE PJ
left join(select direct_prospect_id, 
	divida_curto_prazo::float,
	max_competitive_debt::float,
	qtd_meses_competitive_debt::float,
	competitive_debt_pj_Curr::float,
	competitive_debt_pj_1m::float,
	competitive_debt_pj_2m::float,
	competitive_debt_pj_3m::float,
	competitive_debt_pj_4m::float,
	competitive_debt_pj_5m::float,
	competitive_debt_pj_6m::float,
	competitive_debt_pj_7m::float,
	competitive_debt_pj_8m::float,
	competitive_debt_pj_9m::float,
	competitive_debt_pj_10m::float,
	competitive_debt_pj_11m::float,
	Ever_competitive_debt::float,
	case when competitive_debt_pj_10M < competitive_debt_pj_11M then 1 else 0 end +
	case when competitive_debt_pj_9M < competitive_debt_pj_10M then 1 else 0 end +
	case when competitive_debt_pj_8M < competitive_debt_pj_9M then 1 else 0 end +
	case when competitive_debt_pj_7M < competitive_debt_pj_8M then 1 else 0 end +
	case when competitive_debt_pj_6M < competitive_debt_pj_7M then 1 else 0 end +
	case when competitive_debt_pj_5M < competitive_debt_pj_6M then 1 else 0 end +
	case when competitive_debt_pj_4M < competitive_debt_pj_5M then 1 else 0 end +
	case when competitive_debt_pj_3M < competitive_debt_pj_4M then 1 else 0 end +
	case when competitive_debt_pj_2M < competitive_debt_pj_3M then 1 else 0 end +
	case when competitive_debt_pj_1M < competitive_debt_pj_2M then 1 else 0 end +
	case when competitive_debt_pj_Curr < competitive_debt_pj_1M then 1 else 0 end::float as Meses_Reducao_competitive_debt,
	case when competitive_debt_pj_10M > competitive_debt_pj_11M then 1 else 0 end +
	case when competitive_debt_pj_9M > competitive_debt_pj_10M then 1 else 0 end +
	case when competitive_debt_pj_8M > competitive_debt_pj_9M then 1 else 0 end +
	case when competitive_debt_pj_7M > competitive_debt_pj_8M then 1 else 0 end +
	case when competitive_debt_pj_6M > competitive_debt_pj_7M then 1 else 0 end +
	case when competitive_debt_pj_5M > competitive_debt_pj_6M then 1 else 0 end +
	case when competitive_debt_pj_4M > competitive_debt_pj_5M then 1 else 0 end +
	case when competitive_debt_pj_3M > competitive_debt_pj_4M then 1 else 0 end +
	case when competitive_debt_pj_2M > competitive_debt_pj_3M then 1 else 0 end +
	case when competitive_debt_pj_1M > competitive_debt_pj_2M then 1 else 0 end +
	case when competitive_debt_pj_Curr > competitive_debt_pj_1M then 1 else 0 end::float as Meses_Aumento_competitive_debt,
	case when competitive_debt_pj_1M is null then null else
	case when competitive_debt_pj_Curr >= competitive_debt_pj_1M or competitive_debt_pj_1M is null then 0 else
	case when competitive_debt_pj_1M >= competitive_debt_pj_2M or competitive_debt_pj_2M is null then 1 else
	case when competitive_debt_pj_2M >= competitive_debt_pj_3M or competitive_debt_pj_3M is null then 2 else
	case when competitive_debt_pj_3M >= competitive_debt_pj_4M or competitive_debt_pj_4M is null then 3 else
	case when competitive_debt_pj_4M >= competitive_debt_pj_5M or competitive_debt_pj_5M is null then 4 else
	case when competitive_debt_pj_5M >= competitive_debt_pj_6M or competitive_debt_pj_6M is null then 5 else
	case when competitive_debt_pj_6M >= competitive_debt_pj_7M or competitive_debt_pj_7M is null then 6 else
	case when competitive_debt_pj_7M >= competitive_debt_pj_8M or competitive_debt_pj_8M is null then 7 else
	case when competitive_debt_pj_8M >= competitive_debt_pj_9M or competitive_debt_pj_9M is null then 8 else
	case when competitive_debt_pj_9M >= competitive_debt_pj_10M or competitive_debt_pj_10M is null then 9 else
	case when competitive_debt_pj_10M >= competitive_debt_pj_11M or competitive_debt_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_competitive_debt,
	case when competitive_debt_pj_1M is null then null else
	case when competitive_debt_pj_Curr <= competitive_debt_pj_1M or competitive_debt_pj_1M is null then 0 else
	case when competitive_debt_pj_1M <= competitive_debt_pj_2M or competitive_debt_pj_2M is null then 1 else
	case when competitive_debt_pj_2M <= competitive_debt_pj_3M or competitive_debt_pj_3M is null then 2 else
	case when competitive_debt_pj_3M <= competitive_debt_pj_4M or competitive_debt_pj_4M is null then 3 else
	case when competitive_debt_pj_4M <= competitive_debt_pj_5M or competitive_debt_pj_5M is null then 4 else
	case when competitive_debt_pj_5M <= competitive_debt_pj_6M or competitive_debt_pj_6M is null then 5 else
	case when competitive_debt_pj_6M <= competitive_debt_pj_7M or competitive_debt_pj_7M is null then 6 else
	case when competitive_debt_pj_7M <= competitive_debt_pj_8M or competitive_debt_pj_8M is null then 7 else
	case when competitive_debt_pj_8M <= competitive_debt_pj_9M or competitive_debt_pj_9M is null then 8 else
	case when competitive_debt_pj_9M <= competitive_debt_pj_10M or competitive_debt_pj_10M is null then 9 else
	case when competitive_debt_pj_10M <= competitive_debt_pj_11M or competitive_debt_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_competitive_debt
from(select direct_prospect_id, 
	divida_curto_prazo,
	max(competitive_debt) filter (where data <= data_referencia) as max_competitive_debt,
	sum(competitive_debt) filter (where data = data_referencia) as competitive_debt_pj_Curr,
	sum(competitive_debt) filter (where data = data_referencia - interval '1 month') as competitive_debt_pj_1M,
	sum(competitive_debt) filter (where data = data_referencia - interval '2 months') as competitive_debt_pj_2m,
	sum(competitive_debt) filter (where data = data_referencia - interval '3 months') as competitive_debt_pj_3m,
	sum(competitive_debt) filter (where data = data_referencia - interval '4 months') as competitive_debt_pj_4m,
	sum(competitive_debt) filter (where data = data_referencia - interval '5 months') as competitive_debt_pj_5m,
	sum(competitive_debt) filter (where data = data_referencia - interval '6 months') as competitive_debt_pj_6m,
	sum(competitive_debt) filter (where data = data_referencia - interval '7 months') as competitive_debt_pj_7m,
	sum(competitive_debt) filter (where data = data_referencia - interval '8 months') as competitive_debt_pj_8m,
	sum(competitive_debt) filter (where data = data_referencia - interval '9 months') as competitive_debt_pj_9m,
	sum(competitive_debt) filter (where data = data_referencia - interval '10 months') as competitive_debt_pj_10m,
	sum(competitive_debt) filter (where data = data_referencia - interval '11 months') as competitive_debt_pj_11m,
	sum(competitive_debt) filter (where data <= data_referencia) as Ever_competitive_debt,
	count(*) filter (where data <= data_referencia) as qtd_meses_competitive_debt
from(select direct_prospect_id, case when extract(day from loan_date) >= 16 then case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end else case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end end as data_referencia,
		to_date(jsonb_array_elements(data->'por_modalidade'->0->'serie')->>'data','mm/yyyy') as data,
		(jsonb_array_elements(cc.data->'indicadores'->'dividasMes'))::text::float as competitive_debt,
		(cc.data->'indicadores'->>'dividaAtual')::float as divida_curto_prazo
	from credito_coleta cc
	join(select dp.direct_prospect_id, lr.loan_date,cnpj,
			max(to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS')) filter (where to_date(left(cc.data->>'criado_em',10),'yyyy-mm-dd') <= lr.loan_date and cc.data->'historico' is not null) as max_date, 
			min(to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS')) filter (where cc.data->'historico' is not null) as min_date
		from credito_coleta cc
		join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
		join offers o on o.direct_prospect_id = dp.direct_prospect_id
		join loan_requests lr on lr.offer_id = o.offer_id 
		where documento_tipo = 'CNPJ' and tipo = 'SCR' and lr.status = 'ACCEPTED'
		group by 1,2,3) as t1 on left(cc.documento,8) = left(cnpj,8) and case when max_date is null then to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS') = min_date else 
			to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS') = max_date end) as t2
	group by 1,2) as t3) as SCRDivnaoconc on SCRDivnaoconc.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SERASA SOCIOS
left join (select direct_prospect_id, count(*) filter (where cpf = cpf_socios) is_shareholder,count(*) count_socios
	from(select direct_prospect_id, cpf, data->'acionistas'->jsonb_object_keys(data->'acionistas')->>'cpf' as cpf_socios
		from credito_coleta cc
		join(select dp.direct_prospect_id, dp.cnpj, dp.cpf, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join offers o on o.direct_prospect_id = dp.direct_prospect_id
			join loan_requests lr on lr.offer_id = o.offer_id 
			where tipo = 'Serasa' and lr.status = 'ACCEPTED'
			group by 1,2) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t2
	group by 1) as SerasaCPF on SerasaCPF.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SERASA ANOTACOES
left join (select dp.direct_prospect_id,
	--Empresa
	case when empresa_divida_valor is null then 0 else empresa_divida_valor end as empresa_divida_valor,
	case when empresa_divida_unit is null then 0 else empresa_divida_unit end as empresa_divida_unit,
	case when empresa_ccf_valor is null then 0 else empresa_ccf_valor end as empresa_ccf_valor,
	case when empresa_ccf_unit is null then 0 else empresa_ccf_unit end as empresa_ccf_unit,
	case when empresa_protesto_valor is null then 0 else empresa_protesto_valor end as empresa_protesto_valor,
	case when empresa_protesto_unit is null then 0 else empresa_protesto_unit end as empresa_protesto_unit,
	case when empresa_acao_valor is null then 0 else empresa_acao_valor end as empresa_acao_valor,
	case when empresa_acao_unit is null then 0 else empresa_acao_unit end as empresa_acao_unit,
	case when empresa_pefin_valor is null then 0 else empresa_pefin_valor end as empresa_pefin_valor,
	case when empresa_pefin_unit is null then 0 else empresa_pefin_unit end as empresa_pefin_unit,
	case when empresa_refin_valor is null then 0 else empresa_refin_valor end as empresa_refin_valor,
	case when empresa_refin_unit is null then 0 else empresa_refin_unit end as empresa_refin_unit,
	case when empresa_spc_valor is null then 0 else empresa_spc_valor end as empresa_spc_valor,
	case when empresa_spc_unit is null then 0 else empresa_spc_unit end as empresa_spc_unit,
	case when empresa_divida_valor is null then 0 else empresa_divida_valor end +
		case when empresa_ccf_valor is null then 0 else empresa_ccf_valor end +
		case when empresa_protesto_valor is null then 0 else empresa_protesto_valor end +
		case when empresa_acao_valor is null then 0 else empresa_acao_valor end +
		case when empresa_pefin_valor is null then 0 else empresa_pefin_valor end +
		case when empresa_refin_valor is null then 0 else empresa_refin_valor end +
		case when empresa_spc_valor is null then 0 else empresa_spc_valor end empresa_total_valor,
	case when empresa_divida_unit is null then 0 else empresa_divida_unit end +
		case when empresa_ccf_unit is null then 0 else empresa_ccf_unit end +
		case when empresa_protesto_unit is null then 0 else empresa_protesto_unit end +
		case when empresa_acao_unit is null then 0 else empresa_acao_unit end +
		case when empresa_pefin_unit is null then 0 else empresa_pefin_unit end +
		case when empresa_refin_unit is null then 0 else empresa_refin_unit end +
		case when empresa_spc_unit is null then 0 else empresa_spc_unit end as empresa_total_unit,
	--Socios
	case when socio_divida_valor is null then 0 else socio_divida_valor end as socio_divida_valor,
	case when socio_divida_unit is null then 0 else socio_divida_unit end as socio_divida_unit,
	case when socio_ccf_valor is null then 0 else socio_ccf_valor end as socio_ccf_valor,
	case when socio_ccf_unit is null then 0 else socio_ccf_unit end as socio_ccf_unit,
	case when socio_protesto_valor is null then 0 else socio_protesto_valor end as socio_protesto_valor,
	case when socio_protesto_unit is null then 0 else socio_protesto_unit end as socio_protesto_unit,
	case when socio_acao_valor is null then 0 else socio_acao_valor end as socio_acao_valor,
	case when socio_acao_unit is null then 0 else socio_acao_unit end as socio_acao_unit,
	case when socio_pefin_valor is null then 0 else socio_pefin_valor end as socio_pefin_valor,
	case when socio_pefin_unit is null then 0 else socio_pefin_unit end as socio_pefin_unit,
	case when socio_refin_valor is null then 0 else socio_refin_valor end as socio_refin_valor,
	case when socio_refin_unit is null then 0 else socio_refin_unit end as socio_refin_unit,
	case when socio_spc_valor is null then 0 else socio_spc_valor end as socio_spc_valor,
	case when socio_spc_unit is null then 0 else socio_spc_unit end as socio_spc_unit,
	case when socio_divida_valor is null then 0 else socio_divida_valor end + 
		case when socio_ccf_valor is null then 0 else socio_ccf_valor end +
		case when socio_protesto_valor is null then 0 else socio_protesto_valor end +
		case when socio_acao_valor is null then 0 else socio_acao_valor end +
		case when socio_pefin_valor is null then 0 else socio_pefin_valor end +
		case when socio_refin_valor is null then 0 else socio_refin_valor end +
		case when socio_spc_valor is null then 0 else socio_spc_valor end  as socios_total_valor,
	case when socio_divida_unit is null then 0 else socio_divida_unit end + 
		case when socio_ccf_unit is null then 0 else socio_ccf_unit end +
		case when socio_protesto_unit is null then 0 else socio_protesto_unit end +
		case when socio_acao_unit is null then 0 else socio_acao_unit end +
		case when socio_pefin_unit is null then 0 else socio_pefin_unit end +
		case when socio_refin_unit is null then 0 else socio_refin_unit end +
		case when socio_spc_unit is null then 0 else socio_spc_unit end as socios_total_unit,
	--Socios major
	case when socio_major_divida_valor is null then 0 else socio_major_divida_valor end as socio_major_divida_valor,
	case when socio_major_divida_unit is null then 0 else socio_major_divida_unit end as socio_major_divida_unit,
	case when socio_major_ccf_valor is null then 0 else socio_major_ccf_valor end as socio_major_ccf_valor,
	case when socio_major_ccf_unit is null then 0 else socio_major_ccf_unit end as socio_major_ccf_unit,
	case when socio_major_protesto_valor is null then 0 else socio_major_protesto_valor end as socio_major_protesto_valor,
	case when socio_major_protesto_unit is null then 0 else socio_major_protesto_unit end as socio_major_protesto_unit,
	case when socio_major_acao_valor is null then 0 else socio_major_acao_valor end as socio_major_acao_valor,
	case when socio_major_acao_unit is null then 0 else socio_major_acao_unit end as socio_major_acao_unit,
	case when socio_major_pefin_valor is null then 0 else socio_major_pefin_valor end as socio_major_pefin_valor,
	case when socio_major_pefin_unit is null then 0 else socio_major_pefin_unit end as socio_major_pefin_unit,
	case when socio_major_refin_valor is null then 0 else socio_major_refin_valor end as socio_major_refin_valor,
	case when socio_major_refin_unit is null then 0 else socio_major_refin_unit end as socio_major_refin_unit,
	case when socio_major_spc_valor is null then 0 else socio_major_spc_valor end as socio_major_spc_valor,
	case when socio_major_spc_unit is null then 0 else socio_major_spc_unit end as socio_major_spc_unit,
	case when socio_major_divida_valor is null then 0 else socio_major_divida_valor end +case when socio_major_ccf_valor is null then 0 else socio_major_ccf_valor end +case when socio_major_protesto_valor is null then 0 else socio_major_protesto_valor end +case when socio_major_acao_valor is null then 0 else socio_major_acao_valor end +case when socio_major_pefin_valor is null then 0 else socio_major_pefin_valor end +case when socio_major_refin_valor is null then 0 else socio_major_refin_valor end +case when socio_major_spc_valor is null then 0 else socio_major_spc_valor end as socios_major_total_valor,
	case when socio_major_divida_unit is null then 0 else socio_major_divida_unit end +case when socio_major_ccf_unit is null then 0 else socio_major_ccf_unit end +case when socio_major_protesto_unit is null then 0 else socio_major_protesto_unit end +case when socio_major_acao_unit is null then 0 else socio_major_acao_unit end +case when socio_major_pefin_unit is null then 0 else socio_major_pefin_unit end +case when socio_major_refin_unit is null then 0 else socio_major_refin_unit end +case when socio_major_spc_unit is null then 0 else socio_major_spc_unit end as socios_major_total_unit,
	--Total (empresa + socios)
	case when empresa_divida_valor is null then 0 else empresa_divida_valor end +
		case when empresa_ccf_valor is null then 0 else empresa_ccf_valor end +
		case when empresa_protesto_valor is null then 0 else empresa_protesto_valor end +
		case when empresa_acao_valor is null then 0 else empresa_acao_valor end +
		case when empresa_pefin_valor is null then 0 else empresa_pefin_valor end +
		case when empresa_refin_valor is null then 0 else empresa_refin_valor end +
		case when empresa_spc_valor is null then 0 else empresa_spc_valor end +
		case when socio_divida_valor is null then 0 else socio_divida_valor end + 
		case when socio_ccf_valor is null then 0 else socio_ccf_valor end +
		case when socio_protesto_valor is null then 0 else socio_protesto_valor end +
		case when socio_acao_valor is null then 0 else socio_acao_valor end +
		case when socio_pefin_valor is null then 0 else socio_pefin_valor end +
		case when socio_refin_valor is null then 0 else socio_refin_valor end +
		case when socio_spc_valor is null then 0 else socio_spc_valor end as all_valor,
	case when empresa_divida_unit is null then 0 else empresa_divida_unit end +
		case when empresa_ccf_unit is null then 0 else empresa_ccf_unit end +
		case when empresa_protesto_unit is null then 0 else empresa_protesto_unit end +
		case when empresa_acao_unit is null then 0 else empresa_acao_unit end +
		case when empresa_pefin_unit is null then 0 else empresa_pefin_unit end +
		case when empresa_refin_unit is null then 0 else empresa_refin_unit end +
		case when empresa_spc_unit is null then 0 else empresa_spc_unit end +
		case when socio_divida_unit is null then 0 else socio_divida_unit end + 
		case when socio_ccf_unit is null then 0 else socio_ccf_unit end +
		case when socio_protesto_unit is null then 0 else socio_protesto_unit end +
		case when socio_acao_unit is null then 0 else socio_acao_unit end +
		case when socio_pefin_unit is null then 0 else socio_pefin_unit end +
		case when socio_refin_unit is null then 0 else socio_refin_unit end +
		case when socio_spc_unit is null then 0 else socio_spc_unit end as all_unit
from(select dp.direct_prospect_id from direct_prospects dp join offers o on o.direct_prospect_id = dp.direct_prospect_id join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as dp
left join(select direct_prospect_id,
		--Empresa
		sum(case when divida_valor is null then 0 else divida_valor end) filter (where serasa_nome = 'empresa') as empresa_divida_valor,
		sum(case when divida_unit is null then 0 else divida_unit end) filter (where serasa_nome = 'empresa') as empresa_divida_unit,
		sum(case when ccf_valor is null then 0 else ccf_valor end) filter (where serasa_nome = 'empresa') as empresa_ccf_valor,
		sum(case when ccf_unit is null then 0 else ccf_unit end) filter (where serasa_nome = 'empresa') as empresa_ccf_unit,
		sum(case when protesto_valor is null then 0 else protesto_valor end) filter (where serasa_nome = 'empresa') as empresa_protesto_valor,
		sum(case when protesto_unit is null then 0 else protesto_unit end) filter (where serasa_nome = 'empresa') as empresa_protesto_unit,
		sum(case when acao_valor is null then 0 else acao_valor end) filter (where serasa_nome = 'empresa') as empresa_acao_valor,
		sum(case when acao_unit is null then 0 else acao_unit end) filter (where serasa_nome = 'empresa') as empresa_acao_unit,
		sum(case when pefin_valor is null then 0 else pefin_valor end) filter (where serasa_nome = 'empresa') as empresa_pefin_valor,
		sum(case when pefin_unit is null then 0 else pefin_unit end) filter (where serasa_nome = 'empresa') as empresa_pefin_unit,
		sum(case when refin_valor is null then 0 else refin_valor end) filter (where serasa_nome = 'empresa') as empresa_refin_valor,
		sum(case when refin_unit is null then 0 else refin_unit end) filter (where serasa_nome = 'empresa') as empresa_refin_unit,
		--Socios
		sum(case when divida_valor is null then 0 else divida_valor end) filter (where serasa_nome <> 'empresa') as socio_divida_valor,
		sum(case when divida_unit is null then 0 else divida_unit end) filter (where serasa_nome <> 'empresa') as socio_divida_unit,
		sum(case when ccf_valor is null then 0 else ccf_valor end) filter (where serasa_nome <> 'empresa') as socio_ccf_valor,
		sum(case when ccf_unit is null then 0 else ccf_unit end) filter (where serasa_nome <> 'empresa') as socio_ccf_unit,
		sum(case when protesto_valor is null then 0 else protesto_valor end) filter (where serasa_nome <> 'empresa') as socio_protesto_valor,
		sum(case when protesto_unit is null then 0 else protesto_unit end) filter (where serasa_nome <> 'empresa') as socio_protesto_unit,
		sum(case when acao_valor is null then 0 else acao_valor end) filter (where serasa_nome <> 'empresa') as socio_acao_valor,
		sum(case when acao_unit is null then 0 else acao_unit end) filter (where serasa_nome <> 'empresa') as socio_acao_unit,
		sum(case when pefin_valor is null then 0 else pefin_valor end) filter (where serasa_nome <> 'empresa') as socio_pefin_valor,
		sum(case when pefin_unit is null then 0 else pefin_unit end) filter (where serasa_nome <> 'empresa') as socio_pefin_unit,
		sum(case when refin_valor is null then 0 else refin_valor end) filter (where serasa_nome <> 'empresa') as socio_refin_valor,
		sum(case when refin_unit is null then 0 else refin_unit end) filter (where serasa_nome <> 'empresa') as socio_refin_unit,
		--Socios majoritarios
		sum(case when divida_valor is null then 0 else divida_valor end) filter (where serasa_share >= 0.2) as socio_major_divida_valor,
		sum(case when divida_unit is null then 0 else divida_unit end) filter (where serasa_share >= 0.2) as socio_major_divida_unit,
		sum(case when ccf_valor is null then 0 else ccf_valor end) filter (where serasa_share >= 0.2) as socio_major_ccf_valor,
		sum(case when ccf_unit is null then 0 else ccf_unit end) filter (where serasa_share >= 0.2) as socio_major_ccf_unit,
		sum(case when protesto_valor is null then 0 else protesto_valor end) filter (where serasa_share >= 0.2) as socio_major_protesto_valor,
		sum(case when protesto_unit is null then 0 else protesto_unit end) filter (where serasa_share >= 0.2) as socio_major_protesto_unit,
		sum(case when acao_valor is null then 0 else acao_valor end) filter (where serasa_share >= 0.2) as socio_major_acao_valor,
		sum(case when acao_unit is null then 0 else acao_unit end) filter (where serasa_share >= 0.2) as socio_major_acao_unit,
		sum(case when pefin_valor is null then 0 else pefin_valor end) filter (where serasa_share >= 0.2) as socio_major_pefin_valor,
		sum(case when pefin_unit is null then 0 else pefin_unit end) filter (where serasa_share >= 0.2) as socio_major_pefin_unit,
		sum(case when refin_valor is null then 0 else refin_valor end) filter (where serasa_share >= 0.2) as socio_major_refin_valor,
		sum(case when refin_unit is null then 0 else refin_unit end) filter (where serasa_share >= 0.2) as socio_major_refin_unit
	from(select t2.*,t3.serasa_share
		from(select direct_prospect_id,
				jsonb_object_keys(cc."data"->'anotacoes') as serasa_nome,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'DIVIDA_VENCIDA'->>'valor')::float as divida_valor,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'DIVIDA_VENCIDA'->>'quantidade')::float as divida_unit,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'CHEQUE'->>'valor')::float as ccf_valor,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'CHEQUE'->>'quantidade')::float as ccf_unit,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'PROTESTO'->>'valor')::float as protesto_valor,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'PROTESTO'->>'quantidade')::float as protesto_unit,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'ACAO_JUDICIAL'->>'valor')::float as acao_valor,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'ACAO_JUDICIAL'->>'quantidade')::float as acao_unit,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Pefin'->>'valor_total')::float as pefin_valor,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Pefin'->>'quantidade')::float as pefin_unit,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Refin'->>'valor_total')::float as refin_valor,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Refin'->>'quantidade')::float as refin_unit
			from credito_coleta cc
			join(select dp.direct_prospect_id, dp.cnpj, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
				from credito_coleta cc
				join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
				join offers o on o.direct_prospect_id = dp.direct_prospect_id
				join loan_requests lr on lr.offer_id = o.offer_id 
				where tipo = 'Serasa' and lr.status = 'ACCEPTED'
				group by 1,2) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t2
		left join(select direct_prospect_id,
					jsonb_object_keys("data"->'acionistas') as nome,
					("data"->'acionistas'->jsonb_object_keys("data"->'acionistas')->>'percentual')::float/100 as serasa_share
				from credito_coleta cc
				join(select dp.direct_prospect_id, dp.cnpj, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
					from credito_coleta cc
					join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
					join offers o on o.direct_prospect_id = dp.direct_prospect_id
					join loan_requests lr on lr.offer_id = o.offer_id 
					where tipo = 'Serasa' and lr.status = 'ACCEPTED'
					group by 1,2) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t3 on t3.nome = t2.serasa_nome and t3.direct_prospect_id = t2.direct_prospect_id) as t4
	group by 1) as serasa on serasa.direct_prospect_id = dp.direct_prospect_id
left join(select direct_prospect_id,
		--Socios
		sum(case when spc_socios_valor is null then 0 else spc_socios_valor end) as socio_spc_valor,
		sum(case when spc_socios_unit is null then 0 else spc_socios_unit end) as socio_spc_unit,
		--Socios majoritarios
		sum(case when spc_socios_valor is null then 0 else spc_socios_valor end) filter (where spc_share >= 0.2) as socio_major_spc_valor,
		sum(case when spc_socios_unit is null then 0 else spc_socios_unit end) filter (where spc_share >= 0.2) as socio_major_spc_unit
	from(select t2.direct_prospect_id,spc_nome,spc_share,
				case when spc_socios_valor is null or spc_socios_valor = '' then 0 else spc_socios_valor::float end as spc_socios_valor,
				case when spc_socios_unit is null or spc_socios_unit = '' then 0 else spc_socios_unit::float end as spc_socios_unit
			from(select direct_prospect_id,
					data->'spc'->jsonb_object_keys(data->'spc')->>'nome' as spc_nome,
					data->'spc'->jsonb_object_keys(data->'spc')->>'valor' as spc_socios_valor,
					data->'spc'->jsonb_object_keys(data->'spc')->>'quantidade' as spc_socios_unit
				from credito_coleta cc
				join(select dp.direct_prospect_id, dp.cnpj, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
					from credito_coleta cc
					join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
					join offers o on o.direct_prospect_id = dp.direct_prospect_id
					join loan_requests lr on lr.offer_id = o.offer_id 
					where tipo = 'Serasa' and lr.status = 'ACCEPTED'
					group by 1,2) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t2
			left join(select direct_prospect_id,
						jsonb_object_keys("data"->'acionistas') as nome,
						("data"->'acionistas'->jsonb_object_keys("data"->'acionistas')->>'percentual')::float/100 as spc_share
					from credito_coleta cc
					join(select dp.direct_prospect_id, dp.cnpj, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
						from credito_coleta cc
						join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
						join offers o on o.direct_prospect_id = dp.direct_prospect_id
						join loan_requests lr on lr.offer_id = o.offer_id 
						where tipo = 'Serasa' and lr.status = 'ACCEPTED'
						group by 1,2) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t3 on t3.nome = t2.spc_nome and t3.direct_prospect_id = t2.direct_prospect_id) as t4
	group by 1) as spc_socios on spc_socios.direct_prospect_id = dp.direct_prospect_id
left join(select direct_prospect_id,
		sum(case when spc_valor is null then 0 else spc_valor end) as empresa_spc_valor,
		sum(case when spc_unit is null then 0 else spc_unit end) as empresa_spc_unit	
	from(select direct_prospect_id,
			("data"->'anotacao_spc'->>'total_debito')::float as spc_valor,
			("data"->'anotacao_spc'->>'total_ocr')::float as spc_unit
		from credito_coleta cc
		join(select dp.direct_prospect_id, dp.cnpj, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join offers o on o.direct_prospect_id = dp.direct_prospect_id
			join loan_requests lr on lr.offer_id = o.offer_id 
			where tipo = 'Serasa' and lr.status = 'ACCEPTED'
			group by 1,2) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t2
	group by 1) as spc_empresa on spc_empresa.direct_prospect_id = dp.direct_prospect_id) as SerasaRestr on SerasaRestr.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SERASA PROBLEMA COLIGADAS
left join(select direct_prospect_id, count(*) filter (where empresas_problemas = 'S') as empresas_problema
	from(select direct_prospect_id,
			"data"->'participacoes'->jsonb_object_keys("data"->'participacoes')->>'restricao' as empresas_problemas
		from credito_coleta cc
		join(select dp.direct_prospect_id, dp.cnpj, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join offers o on o.direct_prospect_id = dp.direct_prospect_id
			join loan_requests lr on lr.offer_id = o.offer_id 
			where tipo = 'Serasa' and lr.status = 'ACCEPTED'
			group by 1,2) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t1
	group by 1) as serasaColig on serasaColig.direct_prospect_id = dp.direct_prospect_id 
--------------------------TABLE SERASA CONSULTA FACTORING SERASA
left join (select direct_prospect_id,sum(case when position('INVEST DIR' in consultas_serasa) > 0 or 
	position('CREDITORIOS' in consultas_serasa) > 0 or
	position('FUNDO DE INVE' in consultas_serasa) > 0 or
	position('SECURITIZADORA' in consultas_serasa) > 0 or
	position('FACTORING'in consultas_serasa) > 0 or
	position('FOMENTO'in consultas_serasa) > 0 or
	position('FIDC' in consultas_serasa) > 0 or
	position('NOVA S R M' in consultas_serasa) > 0 or
	position('SERVICOS FINANCEIROS' in consultas_serasa) > 0 then 1 else 0 end) as consulta_factoring_serasa
	from (select direct_prospect_id,cc.data->'consulta_empresas'->jsonb_object_keys(data->'consulta_empresas')->>'nome' as consultas_serasa
		from credito_coleta cc
		join(select dp.direct_prospect_id, dp.cnpj, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join offers o on o.direct_prospect_id = dp.direct_prospect_id
			join loan_requests lr on lr.offer_id = o.offer_id 
			where tipo = 'Serasa' and lr.status = 'ACCEPTED'
			group by 1,2) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t2 
	group by 1) as ConsultaSerasa on ConsultaSerasa.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SERASA CONSULTA FACTORING SPC
left join (select direct_prospect_id,sum(case when position('INVEST DIR' in consultas_spc) > 0 or 
	position('CREDITORIOS' in consultas_spc) > 0 or
	position('FUNDO DE INVE' in consultas_spc) > 0 or
	position('SECURITIZADORA' in consultas_spc) > 0 or
	position('FACTORING'in consultas_spc) > 0 or
	position('FOMENTO'in consultas_spc) > 0 or
	position('FIDC' in consultas_spc) > 0 or
	position('NOVA S R M' in consultas_spc) > 0 or
	position('SERVICOS FINANCEIROS' in consultas_spc) > 0 then 1 else 0 end) as consulta_factoring_spc
	from (select direct_prospect_id,cc.data->'consulta_spc'->jsonb_object_keys(data->'consulta_spc')->>'nome' as consultas_spc
		from credito_coleta cc
		join(select dp.direct_prospect_id, dp.cnpj, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join offers o on o.direct_prospect_id = dp.direct_prospect_id
			join loan_requests lr on lr.offer_id = o.offer_id 
			where tipo = 'Serasa' and lr.status = 'ACCEPTED'
			group by 1,2) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t2
	group by 1) as ConsultaSPC on ConsultaSPC.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SERASA CONSULTAS
left join (select direct_prospect_id,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base)+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base) as consultas_atual,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '1 month')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '1 month') as consultas_1m,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '2 months')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '2 months') as consultas_2m,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '3 months')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '3 months') as consultas_3m,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '4 months')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '4 months') as consultas_4m,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '5 months')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '5 months') as consultas_5m,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '6 months')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '6 months') as consultas_6m,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '7 months')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '7 months') as consultas_7m,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '8 months')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '8 months') as consultas_8m,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '9 months')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '9 months') as consultas_9m,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '10 months')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '10 months') as consultas_10m,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '11 months')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '11 months') as consultas_11m
	from (select direct_prospect_id,
			to_date(to_char(to_date(cc.data->'datetime'->>'data','yyyymmdd'),'Mon/yy'),'Mon/yy') as data_base,
			to_date(jsonb_object_keys(data->'consultas'),'yy/mm') as mes,data->'consultas'->jsonb_object_keys(data->'consultas')->>'pesquisaEmpresas' empresas, 
		replace(data->'registro_spc'->jsonb_object_keys(data->'registro_spc')->>'quantidade',' ','') spc
		from credito_coleta cc
		join(select dp.direct_prospect_id, dp.cnpj, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join offers o on o.direct_prospect_id = dp.direct_prospect_id
			join loan_requests lr on lr.offer_id = o.offer_id 
			where tipo = 'Serasa' and lr.status = 'ACCEPTED'
			group by 1,2) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t2
	group by 1) as serasaconsultas on serasaconsultas.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta,("data"->>'pd')::float as pd_serasa
	from credito_coleta cc
	join(select dp.direct_prospect_id, dp.cnpj, dp.serasa_4,o.rating,max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
		from credito_coleta cc
		join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
		join offers o on o.direct_prospect_id = dp.direct_prospect_id
		join loan_requests lr on lr.offer_id = o.offer_id 
		where tipo = 'Serasa' and lr.status = 'ACCEPTED'
		group by 1,2,3,4) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SÓCIO MAJORITÁRIO MAIS VELHO
left join (select lrs.loan_request_id,extract(year from age(loan_date,s.date_of_birth))::int as MajorSigner_Age, s.revenue as MajorSigner_Revenue,s.civil_status as MajorSigner_civil_status,s.gender as MajorSigner_gender,avg_age_partners,avg_revenue_partners,male_partners,female_partners
	from signers s
	join loan_requests_signers lrs on lrs.signer_id = s.signer_id
	join(select lrs.loan_request_id, signer_percentage,avg_age_partners,avg_revenue_partners,male_partners,female_partners,loan_date,min(date_of_birth) as data_nasc
		from signers s
		join loan_requests_signers lrs on lrs.signer_id = s.signer_id
		join(select lr.loan_request_id, 
				lr.loan_date, 
				max(s.share_percentage) as signer_percentage,
				avg(extract(year from age(lr.loan_date,s.date_of_birth))) filter (where s.share_percentage > 0) as avg_age_partners,
				avg(s.revenue) filter (where s.share_percentage > 0) as avg_revenue_partners,
				count(*) filter (where s.gender = 'MALE' and s.share_percentage > 0) as male_partners, 
				count(*) filter (where s.gender = 'FEMALE' and s.share_percentage > 0) as female_partners
			from signers s
			join loan_requests_signers as lrs on lrs.signer_id = s.signer_id
			join loan_requests lr on lr.loan_request_id = lrs.loan_request_id
			where lr.status = 'ACCEPTED'
			group by 1,2) as t1 on t1.loan_request_id = lrs.loan_request_id and s.share_percentage = t1. signer_percentage
		group by 1,2,3,4,5,6,7) as t2 on t2.loan_request_id = lrs.loan_request_id and s.date_of_birth = t2.data_nasc and s.share_percentage = t2.signer_percentage) as MajSigners on MajSigners.loan_request_id = lr.loan_request_id
--------------------------TABLE SOLICITANTE POR CPF
left join(select lrs.loan_request_id, extract(year from age(lr.loan_date,s.date_of_birth))::int as SolicSignersCPF_Age, s.revenue as SolicSignersCPF_Revenue,s.civil_status as SolicSignerCPF_civil_status,s.gender as SolicSignerCPF_gender
	from direct_prospects dp
	join offers o on o.direct_prospect_id = dp.direct_prospect_id
	join loan_requests lr on lr.offer_id = o.offer_id
	join loan_requests_signers lrs on lrs.loan_request_id = lr.loan_request_id
	join signers s on s.signer_id = lrs.signer_id
	where lr.status = 'ACCEPTED' and case when dp.cpf is null then null else case when dp.cpf = s.cpf then 1 else 0 end end = 1) as SolicCPFSigners on SolicCPFSigners.loan_request_id = lr.loan_request_id 
--------------------------TABLE SÓCIO MAIS VELHO
left join(select lrs.loan_request_id,max(extract(year from age(loan_date,s.date_of_birth)))::int as OldestSh_Age, max(s.revenue) as OldestSh_Revenue,max(s.civil_status) as OldestSh_civil_status,max(s.gender) as OldestSh_gender
	from signers s
	join loan_requests_signers lrs on lrs.signer_id = s.signer_id
	join(select lr.loan_request_id, lr.loan_date, min(s.date_of_birth) as min_date
		from signers s
		join loan_requests_signers as lrs on lrs.signer_id = s.signer_id
		join loan_requests lr on lr.loan_request_id = lrs.loan_request_id
		where lr.status = 'ACCEPTED' and s.share_percentage > 0
		group by 1,2) as t1 on t1.loan_request_id = lrs.loan_request_id and s.date_of_birth = t1.min_date 
	group by 1) as Signers on Signers.loan_request_id = lr.loan_request_id
--------------------------TABLE ADMNISTRADOR MAIS VELHO
left join(select lrs.loan_request_id, max(extract(year from age(loan_date,s.date_of_birth)))::int as AdminSigner_Age, max(s.revenue) as AdminSigner_Revenue,max(s.civil_status) as AdminSigner_civil_status,max(s.gender) as AdminSigner_gender 
	from signers s
	join loan_requests_signers lrs on lrs.signer_id = s.signer_id
	join(select lrs.loan_request_id,lr.loan_date, min(s.date_of_birth) data_nasc
		from signers s
		join loan_requests_signers lrs on lrs.signer_id = s.signer_id
		join loan_requests lr on lr.loan_request_id = lrs.loan_request_id
		where s.administrator = true
		group by 1,2) as t2 on t2.loan_request_id = lrs.loan_request_id and t2.data_nasc = s.date_of_birth
	group by 1) as AdminSigners on AdminSigners.loan_request_id = lr.loan_request_id
--------------------------TABLE CHECKLIST
left join(select t2.emprestimo_id,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'valor_pj') as largest_cashflow_company_account,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'faturamento') as proved_informed_revenue,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'empreza_azul') as positive_balance_account,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'fluxo_entradas') as continuous_income,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'saldo_negativo') as low_negative_balance,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'entradas_desvinculadas') as income_unrelated_government,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'diversificacao_clientes') as diversified_clients,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'somente_despesas_empresa') as only_company_expenses,
	sum(pontuacao) filter (where tipo = 'Outros' and campo = 'endereco') as different_personal_company_address,
	sum(pontuacao) filter (where tipo = 'Outros' and campo = 'socio_bens') as partner_has_assets,
	sum(pontuacao) filter (where tipo = 'Outros' and campo = 'empresa_credito') as company_usedto_credit,
	sum(pontuacao) filter (where tipo = 'Outros' and campo = 'solicitou_menos') as requested_lessthan_offered,
	sum(pontuacao) filter (where tipo = 'Outros' and campo = 'endividamento_baixo') as low_personal_debt,
	sum(pontuacao) filter (where tipo = 'Outros' and campo = 'simulacao_diligente') as dilligent_credit_simulation,
	sum(pontuacao) filter (where tipo = 'Empresa' and campo = 'conta_pj') as company_account_confirmed,
	sum(pontuacao) filter (where tipo = 'Empresa' and campo = 'contrato_social') as social_contract_confirmed,
	sum(pontuacao) filter (where tipo = 'Empresa' and campo = 'extratos_bancarios') as bank_statements_confirmed,
	sum(pontuacao) filter (where tipo = 'Empresa' and campo = 'comprovante_endereço') as company_address_confirmed,
	sum(pontuacao) filter (where tipo = 'Física' and campo = 'cpf')/min(num_socios) as personal_cpf,
	sum(pontuacao) filter (where tipo = 'Física' and campo = 'identidade')/min(num_socios) as personal_rg,
	sum(pontuacao) filter (where tipo = 'Física' and campo = 'conjuge_info')/min(num_socios) as spouse_info,
	sum(pontuacao) filter (where tipo = 'Física' and campo = 'comprovante_renda')/min(num_socios) as proof_income,
	sum(pontuacao) filter (where tipo = 'Física' and campo = 'comprovante_endereço')/min(num_socios) as proof_personal_address,	
	sum(pontuacao) filter (where tipo in ('Extrato', 'Outros')) as score_documentation,
	sum(pontuacao) filter (where tipo in ('Física', 'Empresa'))/(4+5*min(num_socios)) percent_documentation
	from (select emprestimo_id, tipo,campo, case when pontuacao = '' then null else pontuacao end::double precision from (
		select emprestimo_id,tipo,jsonb_object_keys(formulario) as campo,formulario->jsonb_object_keys(formulario)->>'pontos' as pontuacao
		from credito_checklist cc
		join loan_requests lr on lr.loan_request_id = cc.emprestimo_id
		where lr.status = 'ACCEPTED') as t1) as t2
	left join (select emprestimo_id, count(*) num_socios from credito_checklist cc join loan_requests lr on lr.loan_request_id = cc.emprestimo_id where lr.status = 'ACCEPTED' and cc.tipo = 'Física' group by 1) as t1 on t1.emprestimo_id = t2.emprestimo_id
	group by 1) as checklist on checklist.emprestimo_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO
left join(select lr.loan_request_id, 
	max(case when fpp.status = 'Pago' then 1 else 0 end) as fully_paid,
	count(*) filter (where fp.status like 'Pago%') as paid_inst,
	count(*) filter (where fp.status = 'Ativo' and fp.vencimento_em < current_date) as late_inst, 
	count(*) filter (where fp.vencimento_em < current_date) as due_inst,	
	count(*) filter (where fp.vencimento_em >= current_date) as to_come_inst,	
	max(case when fp.pago_em is null then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end) filter (where fp.numero = 1) as fpd,
	max(fp.vencimento_em) filter (where fp.numero = 1 and fp.pago_em is null) as "1st_inst_due_to", 
	sum(fp.cobrado) filter (where fp.pago_em is null) as EAD, 
	sum(fp.cobrado) as curr_total_payment, 
	sum(fp.pago) filter (where fp.status like 'Pago%') as total_paid, 
	sum(fp.cobrado) filter (where pago_em is null and current_date - fp.vencimento_em > 0) as late_payment, 
	sum(fp.cobrado) filter (where pago_em is null and current_date - fp.vencimento_em > 180) as writeoff, 
	min(fp.vencimento_em) filter (where fp.pago_em is null) as current_inst_due_to,
	max(fp.cobrado) filter (where fp.status = 'Ativo') as pmt_atual,
	min(fp.pago_em) as first_paid_inst_paid_on
	from loan_requests lr  
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	where fp.status = 'Ativo' or fp.status like 'Pago%'
	group by 1) as Financeiro on Financeiro.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO ORIGINAL
left join (select fpp.emprestimo_id, sum(fp.cobrado) as original_total_payment, max(fp.vencimento_em) filter (where fp.numero = 1) as original_1st_pmt_due_to
	from financeiro_planopagamento fpp  
	join (select emprestimo_id, min(id) as plano_id from financeiro_planopagamento group by 1) as fpp_orig on fpp_orig.plano_id = fpp.id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	group by 1) as Financeiro_original on Financeiro_original.emprestimo_id = lr.loan_request_id
--------------------------TABLE DPD SOFT
left join(select lr.loan_request_id,
	count(distinct t2.plano_id) as count_renegotiation,
	min(fp.numero) filter (where fp.pago_em is null) ParcEmAberto,
	max(case when fp.numero <> 1 then null else case when ParcSubst = 1 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt1,
	max(case when fp.numero <> 2 then null else case when ParcSubst = 2 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt2,
	max(case when fp.numero <> 3 then null else case when ParcSubst = 3 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt3,
	max(case when fp.numero <> 4 then null else case when ParcSubst = 4 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt4,
	max(case when fp.numero <> 5 then null else case when ParcSubst = 5 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt5,
	max(case when fp.numero <> 6 then null else case when ParcSubst = 6 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt6,
	max(case when fp.numero <> 7 then null else case when ParcSubst = 7 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt7,
	max(case when fp.numero <> 8 then null else case when ParcSubst = 8 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt8,
	max(case when fp.numero <> 9 then null else case when ParcSubst = 9 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt9,
	max(case when fp.numero <> 10 then null else case when ParcSubst = 10 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt10,
	max(case when fp.numero <> 11 then null else case when ParcSubst = 11 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt11,
	max(case when fp.numero <> 12 then null else case when ParcSubst = 12 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt12
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada'
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	where fp.status not in ('Cancelada','EmEspera')
	group by 1) as DPD_Soft on DPD_Soft.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO PMT 9 PLANO MAIS ATUAL SOFT
left join(select lr.loan_request_id,
	max(fp.numero) filter (where fp.pago_em <= case when max_venc9p is null then max_venc9m else max_venc9p end) as ultparcpaga,
	count(*) filter (where fp.vencimento_em <= case when max_venc9p is null then max_venc9m else max_venc9p end) as qtd_parcelas,
	max((case when fp.numero <> 1 then null else (case when ParcSubst = 1 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt1,	
	max((case when fp.numero <> 2 then null else (case when ParcSubst = 2 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt2,	
	max((case when fp.numero <> 3 then null else (case when ParcSubst = 3 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt3,	
	max((case when fp.numero <> 4 then null else (case when ParcSubst = 4 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt4,	
	max((case when fp.numero <> 5 then null else (case when ParcSubst = 5 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt5,	
	max((case when fp.numero <> 6 then null else (case when ParcSubst = 6 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt6,
	max((case when fp.numero <> 7 then null else (case when ParcSubst = 7 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt7,
	max((case when fp.numero <> 8 then null else (case when ParcSubst = 8 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt8,
	max((case when fp.numero <> 9 then null else (case when ParcSubst = 9 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt9
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada' and fp.numero <= 9
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	join(select fpp.emprestimo_id, case when max_id is null then (lr.loan_date + interval '9 months')::date end as max_venc9M , max(fp.vencimento_em) filter (where fp.numero <= 9 and fpp.id = t1.max_id) as max_venc9P
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, max(fpp.id) filter (where fp.numero = 9 and fpp.status <> 'EmEspera') as max_id
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1,2) as max_venc_6M on max_venc_6M.emprestimo_id = fpp.emprestimo_id 
	where fp.status not in ('Cancelada','EmEspera') and case when max_venc9p is null then max_venc9m else max_venc9p end <= current_date
	group by 1) as PMT9_actual_soft on PMT9_actual_soft.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO PMT 6 PLANO MAIS ATUAL SOFT
left join(select lr.loan_request_id,
	max(fp.numero) filter (where fp.pago_em <= case when max_venc6p is null then max_venc6m else max_venc6p end) as ultparcpaga,
	count(*) filter (where fp.vencimento_em <= case when max_venc6p is null then max_venc6m else max_venc6p end) as qtd_parcelas,
	max((case when fp.numero <> 1 then null else (case when ParcSubst = 1 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt1,	
	max((case when fp.numero <> 2 then null else (case when ParcSubst = 2 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt2,	
	max((case when fp.numero <> 3 then null else (case when ParcSubst = 3 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt3,	
	max((case when fp.numero <> 4 then null else (case when ParcSubst = 4 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt4,	
	max((case when fp.numero <> 5 then null else (case when ParcSubst = 5 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt5,	
	max((case when fp.numero <> 6 then null else (case when ParcSubst = 6 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt6
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada' and fp.numero <= 6
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	join(select fpp.emprestimo_id, case when max_id is null then (lr.loan_date + interval '6 months')::date end as max_venc6M , max(fp.vencimento_em) filter (where fp.numero <= 6 and fpp.id = t1.max_id) as max_venc6P
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, max(fpp.id) filter (where fp.numero = 6 and fpp.status <> 'EmEspera') as max_id
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1,2) as max_venc_6M on max_venc_6M.emprestimo_id = fpp.emprestimo_id 
	where fp.status not in ('Cancelada','EmEspera') and case when max_venc6p is null then max_venc6m else max_venc6p end <= current_date
	group by 1) as PMT6_actual_soft on PMT6_actual_soft.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO PMT 3 PLANO MAIS ATUAL SOFT
left join(select lr.loan_request_id,
	max(fp.numero) filter (where fp.pago_em <= case when max_venc3p is null then max_venc3m else max_venc3p end) as ultparcpaga,
	count(*) filter (where fp.vencimento_em <= case when max_venc3p is null then max_venc3m else max_venc3p end) as qtd_parcelas,
	max((case when fp.numero <> 1 then null else (case when ParcSubst = 1 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - vencparcsubst else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em  then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - VencParcSubst end) else (case when (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt1,	
	max((case when fp.numero <> 2 then null else (case when ParcSubst = 2 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - vencparcsubst else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em  then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - VencParcSubst end) else (case when (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt2,	
	max((case when fp.numero <> 3 then null else (case when ParcSubst = 3 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - vencparcsubst else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em  then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - VencParcSubst end) else (case when (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt3
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada' and fp.numero <= 3
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	join(select fpp.emprestimo_id, case when max_id is null then (lr.loan_date + interval '3 months')::date end as max_venc3M , max(fp.vencimento_em) filter (where fp.numero <= 3 and fpp.id = t1.max_id) as max_venc3P
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, max(fpp.id) filter (where fp.numero = 3 and fpp.status <> 'EmEspera') as max_id
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1,2) as max_venc_3M on max_venc_3M.emprestimo_id = fpp.emprestimo_id 
	where fp.status not in ('Cancelada','EmEspera') and case when max_venc3p is null then max_venc3m else max_venc3p end <= current_date
	group by 1) as PMT3_Actual_soft on PMT3_Actual_soft.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO PMT 9 PLANO ORIGINAL SOFT
left join(select lr.loan_request_id,
	max(fp.numero) filter (where fp.pago_em <= case when max_venc9p is null then max_venc9m else max_venc9p end) as ultparcpaga,
	count(*) filter (where fp.vencimento_em <= case when max_venc9p is null then max_venc9m else max_venc9p end) as qtd_parcelas,
	max((case when fp.numero <> 1 then null else (case when ParcSubst = 1 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt1,	
	max((case when fp.numero <> 2 then null else (case when ParcSubst = 2 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt2,	
	max((case when fp.numero <> 3 then null else (case when ParcSubst = 3 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt3,	
	max((case when fp.numero <> 4 then null else (case when ParcSubst = 4 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt4,	
	max((case when fp.numero <> 5 then null else (case when ParcSubst = 5 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt5,	
	max((case when fp.numero <> 6 then null else (case when ParcSubst = 6 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt6,
	max((case when fp.numero <> 7 then null else (case when ParcSubst = 7 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt7,
	max((case when fp.numero <> 8 then null else (case when ParcSubst = 8 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt8,
	max((case when fp.numero <> 9 then null else (case when ParcSubst = 9 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt9
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada' and fp.numero <= 9
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	join(select fpp.emprestimo_id, case when min_id is null then (lr.loan_date + interval '9 months')::date end as max_venc9M , max(fp.vencimento_em) filter (where fp.numero <= 9 and fpp.id = t1.min_id) as max_venc9P
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, min(fpp.id) filter (where fp.numero = 9 and fpp.status <> 'EmEspera') as min_id
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1,2) as max_venc_6M on max_venc_6M.emprestimo_id = fpp.emprestimo_id 
	where fp.status not in ('Cancelada','EmEspera') and case when max_venc9p is null then max_venc9m else max_venc9p end <= current_date
	group by 1) as PMT9_orig_soft on PMT9_orig_soft.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO PMT 6 PLANO ORIGINAL SOFT
left join(select lr.loan_request_id,
	max(fp.numero) filter (where fp.pago_em <= case when max_venc6p is null then max_venc6m else max_venc6p end) as ultparcpaga,
	count(*) filter (where fp.vencimento_em <= case when max_venc6p is null then max_venc6m else max_venc6p end) as qtd_parcelas,
	max((case when fp.numero <> 1 then null else (case when ParcSubst = 1 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt1,	
	max((case when fp.numero <> 2 then null else (case when ParcSubst = 2 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt2,	
	max((case when fp.numero <> 3 then null else (case when ParcSubst = 3 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt3,	
	max((case when fp.numero <> 4 then null else (case when ParcSubst = 4 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt4,	
	max((case when fp.numero <> 5 then null else (case when ParcSubst = 5 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt5,	
	max((case when fp.numero <> 6 then null else (case when ParcSubst = 6 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt6
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada' and fp.numero <= 6
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	join(select fpp.emprestimo_id, case when min_id is null then (lr.loan_date + interval '6 months')::date end as max_venc6M , max(fp.vencimento_em) filter (where fp.numero <= 6 and fpp.id = t1.min_id) as max_venc6P
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, min(fpp.id) filter (where fp.numero = 6 and fpp.status <> 'EmEspera') as min_id
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1,2) as max_venc_6M on max_venc_6M.emprestimo_id = fpp.emprestimo_id 
	where fp.status not in ('Cancelada','EmEspera') and case when max_venc6p is null then max_venc6m else max_venc6p end <= current_date
	group by 1) as PMT6_orig_soft on PMT6_orig_soft.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO PMT 3 PLANO ORIGINAL SOFT
left join(select lr.loan_request_id,
	max(fp.numero) filter (where fp.pago_em <= case when max_venc3p is null then max_venc3m else max_venc3p end) as ultparcpaga,
	count(*) filter (where fp.vencimento_em <= case when max_venc3p is null then max_venc3m else max_venc3p end) as qtd_parcelas,
	max((case when fp.numero <> 1 then null else (case when ParcSubst = 1 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - vencparcsubst else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em  then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - VencParcSubst end) else (case when (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt1,	
	max((case when fp.numero <> 2 then null else (case when ParcSubst = 2 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - vencparcsubst else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em  then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - VencParcSubst end) else (case when (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt2,	
	max((case when fp.numero <> 3 then null else (case when ParcSubst = 3 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - vencparcsubst else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em  then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - VencParcSubst end) else (case when (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt3
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada' and fp.numero <= 3
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	join(select fpp.emprestimo_id, case when min_id is null then (lr.loan_date + interval '3 months')::date end as max_venc3M , max(fp.vencimento_em) filter (where fp.numero <= 3 and fpp.id = t1.min_id) as max_venc3P
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, min(fpp.id) filter (where fp.numero = 3 and fpp.status <> 'EmEspera') as min_id
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1,2) as max_venc_3M on max_venc_3M.emprestimo_id = fpp.emprestimo_id 
	where fp.status not in ('Cancelada','EmEspera') and case when max_venc3p is null then max_venc3m else max_venc3p end <= current_date
	group by 1) as PMT3_orig_soft on PMT3_orig_soft.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO PMT 6 PLANO ORIGINAL AGGRESSIVE
left join(select lr.loan_request_id,
	max(fp.numero) filter (where fp.pago_em <= case when max_venc6p is null then max_venc6m else max_venc6p end) as ultparcpaga,
	count(*) filter (where fp.vencimento_em <= case when max_venc6p is null then max_venc6m else max_venc6p end) as qtd_parcelas,
	max((case when fp.numero <> 1 then null else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < pmt1_venc then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt1_venc else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt1_venc end) end) end)) as max_late_pmt1,	
	max((case when fp.numero <> 2 then null else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < pmt2_venc then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt2_venc else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt2_venc end) end) end)) as max_late_pmt2,	
	max((case when fp.numero <> 3 then null else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < pmt3_venc then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt3_venc else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt3_venc end) end) end)) as max_late_pmt3,	
	max((case when fp.numero <> 4 then null else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < pmt4_venc then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt4_venc else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt4_venc end) end) end)) as max_late_pmt4,	
	max((case when fp.numero <> 5 then null else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < pmt5_venc then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt5_venc else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt5_venc end) end) end)) as max_late_pmt5,	
	max((case when fp.numero <> 6 then null else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < pmt6_venc then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt6_venc else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt6_venc end) end) end)) as max_late_pmt6
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	join(select fpp.emprestimo_id, 
			case when min_id is null then (lr.loan_date + interval '6 months')::date end as max_venc6M, 
			max(fp.vencimento_em) filter (where fp.numero = 6 and fpp.id = t1.min_id) as max_venc6P,
			min(fp.vencimento_em) filter (where fp.numero = 1 and fpp.id = case when t1.min_id is null then min_id2 else min_id end) as pmt1_venc,
			min(fp.vencimento_em) filter (where fp.numero = 2 and fpp.id = case when t1.min_id is null then min_id2 else min_id end) as pmt2_venc,
			min(fp.vencimento_em) filter (where fp.numero = 3 and fpp.id = case when t1.min_id is null then min_id2 else min_id end) as pmt3_venc,
			min(fp.vencimento_em) filter (where fp.numero = 4 and fpp.id = case when t1.min_id is null then min_id2 else min_id end) as pmt4_venc,
			min(fp.vencimento_em) filter (where fp.numero = 5 and fpp.id = case when t1.min_id is null then min_id2 else min_id end) as pmt5_venc,
			min(fp.vencimento_em) filter (where fp.numero = 6 and fpp.id = case when t1.min_id is null then min_id2 else min_id end) as pmt6_venc
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, min(fpp.id) filter (where fp.numero = 6) as min_id,min(fpp.id) as min_id2
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1,2) as max_venc_6M on max_venc_6M.emprestimo_id = fpp.emprestimo_id 
	where fp.status not in ('Cancelada','EmEspera') and case when max_venc6p is null then max_venc6m else max_venc6p end <= current_date
	group by 1) as PMT6_orig_aggressive on PMT6_orig_aggressive.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO PMT 5 PLANO ORIGINAL AGGRESSIVE
left join(select lr.loan_request_id,
	max(fp.numero) filter (where fp.pago_em <= case when max_venc5p is null then max_venc5m else max_venc5p end) as ultparcpaga,
	count(*) filter (where fp.vencimento_em <= case when max_venc5p is null then max_venc5m else max_venc5p end) as qtd_parcelas,
	max((case when fp.numero <> 1 then null else (case when (case when (case when max_venc5p is null then max_venc5m else max_venc5p end) > current_date then current_date else (case when max_venc5p is null then max_venc5m else max_venc5p end) end) < pmt1_venc then null else (case when fp.pago_em is null then (case when (case when max_venc5p is null then max_venc5m else max_venc5p end) > current_date then current_date else (case when max_venc5p is null then max_venc5m else max_venc5p end) end) - pmt1_venc else (case when (case when max_venc5p is null then max_venc5m else max_venc5p end) > fp.pago_em then fp.pago_em else (case when max_venc5p is null then max_venc5m else max_venc5p end) end) - pmt1_venc end) end) end)) as max_late_pmt1,	
	max((case when fp.numero <> 2 then null else (case when (case when (case when max_venc5p is null then max_venc5m else max_venc5p end) > current_date then current_date else (case when max_venc5p is null then max_venc5m else max_venc5p end) end) < pmt2_venc then null else (case when fp.pago_em is null then (case when (case when max_venc5p is null then max_venc5m else max_venc5p end) > current_date then current_date else (case when max_venc5p is null then max_venc5m else max_venc5p end) end) - pmt2_venc else (case when (case when max_venc5p is null then max_venc5m else max_venc5p end) > fp.pago_em then fp.pago_em else (case when max_venc5p is null then max_venc5m else max_venc5p end) end) - pmt2_venc end) end) end)) as max_late_pmt2,	
	max((case when fp.numero <> 3 then null else (case when (case when (case when max_venc5p is null then max_venc5m else max_venc5p end) > current_date then current_date else (case when max_venc5p is null then max_venc5m else max_venc5p end) end) < pmt3_venc then null else (case when fp.pago_em is null then (case when (case when max_venc5p is null then max_venc5m else max_venc5p end) > current_date then current_date else (case when max_venc5p is null then max_venc5m else max_venc5p end) end) - pmt3_venc else (case when (case when max_venc5p is null then max_venc5m else max_venc5p end) > fp.pago_em then fp.pago_em else (case when max_venc5p is null then max_venc5m else max_venc5p end) end) - pmt3_venc end) end) end)) as max_late_pmt3,	
	max((case when fp.numero <> 4 then null else (case when (case when (case when max_venc5p is null then max_venc5m else max_venc5p end) > current_date then current_date else (case when max_venc5p is null then max_venc5m else max_venc5p end) end) < pmt4_venc then null else (case when fp.pago_em is null then (case when (case when max_venc5p is null then max_venc5m else max_venc5p end) > current_date then current_date else (case when max_venc5p is null then max_venc5m else max_venc5p end) end) - pmt4_venc else (case when (case when max_venc5p is null then max_venc5m else max_venc5p end) > fp.pago_em then fp.pago_em else (case when max_venc5p is null then max_venc5m else max_venc5p end) end) - pmt4_venc end) end) end)) as max_late_pmt4,	
	max((case when fp.numero <> 5 then null else (case when (case when (case when max_venc5p is null then max_venc5m else max_venc5p end) > current_date then current_date else (case when max_venc5p is null then max_venc5m else max_venc5p end) end) < pmt5_venc then null else (case when fp.pago_em is null then (case when (case when max_venc5p is null then max_venc5m else max_venc5p end) > current_date then current_date else (case when max_venc5p is null then max_venc5m else max_venc5p end) end) - pmt5_venc else (case when (case when max_venc5p is null then max_venc5m else max_venc5p end) > fp.pago_em then fp.pago_em else (case when max_venc5p is null then max_venc5m else max_venc5p end) end) - pmt5_venc end) end) end)) as max_late_pmt5	
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	join(select fpp.emprestimo_id, 
			case when min_id is null then (lr.loan_date + interval '5 months')::date end as max_venc5M, 
			max(fp.vencimento_em) filter (where fp.numero = 5 and fpp.id = t1.min_id) as max_venc5P,
			min(fp.vencimento_em) filter (where fp.numero = 1 and fpp.id = case when t1.min_id is null then min_id2 else min_id end) as pmt1_venc,
			min(fp.vencimento_em) filter (where fp.numero = 2 and fpp.id = case when t1.min_id is null then min_id2 else min_id end) as pmt2_venc,
			min(fp.vencimento_em) filter (where fp.numero = 3 and fpp.id = case when t1.min_id is null then min_id2 else min_id end) as pmt3_venc,
			min(fp.vencimento_em) filter (where fp.numero = 4 and fpp.id = case when t1.min_id is null then min_id2 else min_id end) as pmt4_venc,
			min(fp.vencimento_em) filter (where fp.numero = 5 and fpp.id = case when t1.min_id is null then min_id2 else min_id end) as pmt5_venc
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, min(fpp.id) filter (where fp.numero = 5) as min_id,min(fpp.id) as min_id2
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1,2) as max_venc_5M on max_venc_5M.emprestimo_id = fpp.emprestimo_id 
	where fp.status not in ('Cancelada','EmEspera') and case when max_venc5p is null then max_venc5m else max_venc5p end <= current_date
	group by 1) as PMT5_orig_aggressive on PMT5_orig_aggressive.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO MOB 7 PLANO ORIGINAL SOFT
left join(select lr.loan_request_id,
	max(fp.numero) filter (where fp.pago_em <= (lr.loan_date + interval '7 months')::date) as ultparcpaga,
	count(*) filter (where fp.vencimento_em <= (lr.loan_date + interval '7 months')::date) as qtd_parcelas,
	max((case when fp.numero <> 1 then null else (case when ParcSubst = 1 and VencParcSubst is not null then (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - vencparcsubst else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em  then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - VencParcSubst end) else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em end) end) end) end)) as max_late_pmt1,	
	max((case when fp.numero <> 2 then null else (case when ParcSubst = 2 and VencParcSubst is not null then (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - vencparcsubst else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em  then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - VencParcSubst end) else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em end) end) end) end)) as max_late_pmt2,	
	max((case when fp.numero <> 3 then null else (case when ParcSubst = 3 and VencParcSubst is not null then (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - vencparcsubst else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em  then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - VencParcSubst end) else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em end) end) end) end)) as max_late_pmt3,	
	max((case when fp.numero <> 4 then null else (case when ParcSubst = 4 and VencParcSubst is not null then (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - vencparcsubst else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em  then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - VencParcSubst end) else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em end) end) end) end)) as max_late_pmt4,	
	max((case when fp.numero <> 5 then null else (case when ParcSubst = 5 and VencParcSubst is not null then (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - vencparcsubst else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em  then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - VencParcSubst end) else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em end) end) end) end)) as max_late_pmt5,	
	max((case when fp.numero <> 6 then null else (case when ParcSubst = 6 and VencParcSubst is not null then (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - vencparcsubst else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em  then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - VencParcSubst end) else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em end) end) end) end)) as max_late_pmt6
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada' and fp.numero <= 6
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	where fp.status not in ('Cancelada','EmEspera') and lr.loan_date + interval '7 months' <= current_date
	group by 1) as MOB7_soft on MOB7_soft.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO MOB 7 PLANO ORIGINAL AGGRESSIVE
left join(select lr.loan_request_id,
	max(fp.numero) filter (where fp.pago_em <= (lr.loan_date + interval '7 months')::date) as ultparcpaga,
	count(*) filter (where fp.vencimento_em <= (lr.loan_date + interval '7 months')::date) as qtd_parcelas,
	max((case when fp.numero <> 1 then null else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < pmt1_venc then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - pmt1_venc else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - pmt1_venc end) end) end)) as max_late_pmt1,	
	max((case when fp.numero <> 2 then null else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < pmt2_venc then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - pmt2_venc else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - pmt2_venc end) end) end)) as max_late_pmt2,	
	max((case when fp.numero <> 3 then null else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < pmt3_venc then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - pmt3_venc else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - pmt3_venc end) end) end)) as max_late_pmt3,	
	max((case when fp.numero <> 4 then null else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < pmt4_venc then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - pmt4_venc else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - pmt4_venc end) end) end)) as max_late_pmt4,	
	max((case when fp.numero <> 5 then null else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < pmt5_venc then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - pmt5_venc else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - pmt5_venc end) end) end)) as max_late_pmt5,	
	max((case when fp.numero <> 6 then null else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < pmt6_venc then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - pmt6_venc else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - pmt6_venc end) end) end)) as max_late_pmt6
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	join(select fpp.emprestimo_id, 
			min(fp.vencimento_em) filter (where fp.numero = 1 and fpp.id = case when min_id is null then min_id2 else min_id end) as pmt1_venc,
			min(fp.vencimento_em) filter (where fp.numero = 2 and fpp.id = case when min_id is null then min_id2 else min_id end) as pmt2_venc,
			min(fp.vencimento_em) filter (where fp.numero = 3 and fpp.id = case when min_id is null then min_id2 else min_id end) as pmt3_venc,
			min(fp.vencimento_em) filter (where fp.numero = 4 and fpp.id = case when min_id is null then min_id2 else min_id end) as pmt4_venc,
			min(fp.vencimento_em) filter (where fp.numero = 5 and fpp.id = case when min_id is null then min_id2 else min_id end) as pmt5_venc,
			min(fp.vencimento_em) filter (where fp.numero = 6 and fpp.id = case when min_id is null then min_id2 else min_id end) as pmt6_venc
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, min(fpp.id) filter (where fp.numero = 6) as min_id,min(fpp.id) as min_id2
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1) as max_venc_6M on max_venc_6M.emprestimo_id = fpp.emprestimo_id 
	where fp.status not in ('Cancelada','EmEspera') and lr.loan_date + interval '7 months' <= current_date
	group by 1) as MOB7_aggressive on MOB7_aggressive.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO MOB 6 PLANO ORIGINAL AGGRESSIVE
left join(select lr.loan_request_id,
	max(fp.numero) filter (where fp.pago_em <= (lr.loan_date + interval '6 months')::date) as ultparcpaga,
	count(*) filter (where fp.vencimento_em <= (lr.loan_date + interval '6 months')::date) as qtd_parcelas,
	max((case when fp.numero <> 1 then null else (case when (case when (lr.loan_date + interval '6 months')::date > current_date then current_date else (lr.loan_date + interval '6 months')::date end) < pmt1_venc then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '6 months')::date > current_date then current_date else (lr.loan_date + interval '6 months')::date end) - pmt1_venc else (case when (lr.loan_date + interval '6 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '6 months')::date end) - pmt1_venc end) end) end)) as max_late_pmt1,	
	max((case when fp.numero <> 2 then null else (case when (case when (lr.loan_date + interval '6 months')::date > current_date then current_date else (lr.loan_date + interval '6 months')::date end) < pmt2_venc then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '6 months')::date > current_date then current_date else (lr.loan_date + interval '6 months')::date end) - pmt2_venc else (case when (lr.loan_date + interval '6 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '6 months')::date end) - pmt2_venc end) end) end)) as max_late_pmt2,	
	max((case when fp.numero <> 3 then null else (case when (case when (lr.loan_date + interval '6 months')::date > current_date then current_date else (lr.loan_date + interval '6 months')::date end) < pmt3_venc then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '6 months')::date > current_date then current_date else (lr.loan_date + interval '6 months')::date end) - pmt3_venc else (case when (lr.loan_date + interval '6 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '6 months')::date end) - pmt3_venc end) end) end)) as max_late_pmt3,	
	max((case when fp.numero <> 4 then null else (case when (case when (lr.loan_date + interval '6 months')::date > current_date then current_date else (lr.loan_date + interval '6 months')::date end) < pmt4_venc then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '6 months')::date > current_date then current_date else (lr.loan_date + interval '6 months')::date end) - pmt4_venc else (case when (lr.loan_date + interval '6 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '6 months')::date end) - pmt4_venc end) end) end)) as max_late_pmt4,	
	max((case when fp.numero <> 5 then null else (case when (case when (lr.loan_date + interval '6 months')::date > current_date then current_date else (lr.loan_date + interval '6 months')::date end) < pmt5_venc then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '6 months')::date > current_date then current_date else (lr.loan_date + interval '6 months')::date end) - pmt5_venc else (case when (lr.loan_date + interval '6 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '6 months')::date end) - pmt5_venc end) end) end)) as max_late_pmt5,	
	max((case when fp.numero <> 6 then null else (case when (case when (lr.loan_date + interval '6 months')::date > current_date then current_date else (lr.loan_date + interval '6 months')::date end) < pmt6_venc then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '6 months')::date > current_date then current_date else (lr.loan_date + interval '6 months')::date end) - pmt6_venc else (case when (lr.loan_date + interval '6 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '6 months')::date end) - pmt6_venc end) end) end)) as max_late_pmt6
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	join(select fpp.emprestimo_id, 
			min(fp.vencimento_em) filter (where fp.numero = 1 and fpp.id = case when min_id is null then min_id2 else min_id end) as pmt1_venc,
			min(fp.vencimento_em) filter (where fp.numero = 2 and fpp.id = case when min_id is null then min_id2 else min_id end) as pmt2_venc,
			min(fp.vencimento_em) filter (where fp.numero = 3 and fpp.id = case when min_id is null then min_id2 else min_id end) as pmt3_venc,
			min(fp.vencimento_em) filter (where fp.numero = 4 and fpp.id = case when min_id is null then min_id2 else min_id end) as pmt4_venc,
			min(fp.vencimento_em) filter (where fp.numero = 5 and fpp.id = case when min_id is null then min_id2 else min_id end) as pmt5_venc,
			min(fp.vencimento_em) filter (where fp.numero = 6 and fpp.id = case when min_id is null then min_id2 else min_id end) as pmt6_venc
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, min(fpp.id) filter (where fp.numero = 6) as min_id,min(fpp.id) as min_id2
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1) as max_venc_6M on max_venc_6M.emprestimo_id = fpp.emprestimo_id 
	where fp.status not in ('Cancelada','EmEspera') and lr.loan_date + interval '6 months' <= current_date
	group by 1) as MOB6_aggressive on MOB6_aggressive.loan_request_id = lr.loan_request_id
--------------------------TABLE CLIENTS
left join (select lr.loan_request_id, t1.is_renew
	from offers o 
	join loan_requests lr on lr.offer_id = o.offer_id 
	join (select c.client_id, lr.loan_date, count(*) filter (where lr.loan_date > t1.request_date) as is_renew
		from clients c
		join offers o on o.client_id = c.client_id
		join loan_requests lr on lr.offer_id = o.offer_id
		join (select c.client_id, lr.loan_request_id, lr.loan_date as request_date
			from clients c
			join offers o on o.client_id = c.client_id
			join loan_requests lr on lr.offer_id = o.offer_id
			where lr.status = 'ACCEPTED') as t1 on t1.client_id = c.client_id
		where lr.status = 'ACCEPTED'
		group by 1,2) as t1 on t1.client_id = o.client_id and t1.loan_date = lr.loan_date) as isrenew on isrenew.loan_request_id = lr.loan_request_id
--------------------------TABLE TAGS
left join (select dp.direct_prospect_id, 
		max(case when t.nome = 'AA<=10-v1' then 1 else 0 end) as app_auto,
		max(case when t.nome = 'Automático' then 1 else 0 end) as offer_auto,
		max(case when t.nome = 'InterestRate:Test1' then 1 else 0 end) as interest_rate_test1
	from direct_prospects dp
	join direct_prospects_tag dpt on dpt.directprospect_id = dp.direct_prospect_id
	join tags t on t.id = dpt.tag_id
	group by 1) as tags on tags.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SIGNERS ADDRESS
left join(select dp.direct_prospect_id,max(replace(sa.zip_code,'-','')) as signer_zipcode
	from direct_prospects dp
	join offers o on o.direct_prospect_id = dp.direct_prospect_id
	join loan_requests lr on lr.offer_id = o.offer_id
	join loan_requests_signers lrs on lrs.loan_request_id = lr.loan_request_id
	join signers s on s.signer_id = lrs.signer_id
	join signer_addresses sa on sa.address_id = s.address_id
	where case when dp.zip_code = '' then null else dp.zip_code end is null
	group by 1) as signer_address on signer_address.direct_prospect_id = dp.direct_prospect_id
--------------------------
where lr.status = 'ACCEPTED';

 


select count(*)
from loan_requests lr
where lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')


CREATE TABLE sandbox."recent_loan_tape_v2.2.2_20181108" AS
--APENAS LOANS RECENTES
--BIZU 2.0
-- Versão sql
--------------------------LEAD
select dp.direct_prospect_id as lead_ID,
dp.opt_in_date as lead_opt_in_date,
extract(hour from dp.opt_in_date)::int as lead_opt_in_hour,
extract(day from dp.opt_in_date)::int as lead_opt_in_day,
extract(month from dp.opt_in_date)::int as lead_opt_in_month,
extract(year from dp.opt_in_date)::int as lead_opt_in_year,
case when amount_requested < 1000 then o.max_value else amount_requested end/month_revenue/12 as lead_incremental_debt_leverage,
o.max_value/case when amount_requested < 1000 then o.max_value else amount_requested end as lead_offer_over_application_value,
case when lr.valor_solicitado is not null then lr.valor_solicitado/o.max_value::float when requested_lessthan_offered = 1 then 1 else lr.value/o.max_value::float end as lead_loan_over_offer_value,
lr.number_of_installments/o.max_number_of_installments::double precision as lead_loan_over_offer_term,
case when case when lr.valor_solicitado is not null then lr.valor_solicitado/o.max_value::float when requested_lessthan_offered = 1 then 1 else lr.value/o.max_value::float end = lr.number_of_installments/o.max_number_of_installments::float then 1 else (@li.pmt) / ((o.interest_rate/100+0.009)/(1-(1+o.interest_rate/100+0.009)^(-o.max_number_of_installments)) * o.max_value) end as lead_loan_over_offer_pmt,
case when o.date_inserted<dp.opt_in_date then 0 else extract(day from o.date_inserted - dp.opt_in_date)*24 + extract(hour from o.date_inserted - dp.opt_in_date) + extract(minute from o.date_inserted - dp.opt_in_date)/60 end as lead_app_to_offer_time,
case when lr.date_inserted < o.date_inserted then 0 else extract(day from lr.date_inserted - o.date_inserted)*24 + extract(hour from lr.date_inserted - o.date_inserted) + extract(minute from lr.date_inserted - o.date_inserted)/60 end as lead_offer_to_request_time,
case when lr.loan_date < lr.date_inserted then 0 else extract(day from lr.loan_date - lr.date_inserted)*24 + extract(hour from lr.loan_date - lr.date_inserted) + extract(minute from lr.loan_date - lr.date_inserted)/60 end as lead_request_to_loan_time,
case when lr.loan_date < dp.opt_in_date then 0 else extract(day from lr.loan_date - dp.opt_in_date)*24 + extract(hour from lr.loan_date - dp.opt_in_date) + extract(minute from lr.loan_date - dp.opt_in_date)/60 end as lead_full_track_time,
replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(dp.reason_for_loan,'Capital de Giro','Working Capital'),'Expansão','Expansion'),'Compra de Estoque','Inventory'),'Outros','Others'),'Reformar o meu negócio','Refurbishment'),'Consolidação de Dívidas','Debt Consolidation'),'Marketing e Vendas','Sales and Marketing'),'Uso Pessoal','Personal Use'),'Compra de equipamentos','Machinery'),'Estoques','Inventory'),'Melhorar meu fluxo de caixa','Working Capital'),
	'Aumentar o volume
                        do meu estoque','Inventory'),'Comprar novos equipamentos','Machinery'),'Expandir minha empresa','Expansion'),'Quitar minhas dividas','Debt Consolidation'),'Reforma','Refurbishment'),'Compra de Equipamentos','Machinery') as lead_reason_for_loan,
replace(replace(replace(replace(dp.vinculo,'Administrador','Administrator'),'Outros','Other'),'Procurador','Attorney'),'Sócio','Shareholder') as lead_requester_relationship,
dp.month_revenue as lead_informed_month_revenue,
dp.revenue::double precision as lead_estimated_annual_revenue,
dp.amount_requested as lead_application_amount,
case when utm_source is null then 'NULL' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_source,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor',''),'-','') end as lead_marketing_channel,
case when dp.utm_medium is null then 'NULL' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(dp.utm_medium,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor','') end  as lead_marketing_medium,
case when utm_campaign is null then 'NULL' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_campaign,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'lookalike-maio','Adespresso Lookalike - Maio'),'mosaic-maio-top-revenue-big','Adespresso Mosaic Top Revenues (Big Ones) - Maio'),'%20',' '),'%3E','>'),'#howItWorksAnchor','') end as lead_marketing_campaign,
dp.opt_in_ip as lead_opt_in_ip,
case when dp.n_company_applied_before is null then 0 else dp.n_company_applied_before end as lead_times_company_applied_before,
case when dp.n_individual_applied_before is null then 0 when dp.utm_source in ('fdex','capitalemp') then null else dp.n_individual_applied_before end as lead_times_individual_applied_before,
to_date(dp.date_created,'yyyy-mm-dd') as lead_date_company_creation,
dp.age as lead_company_age,
case when (month_revenue >= 100000 and age < 2) or (month_revenue >= 200000 and age < 3) or (month_revenue >= 300000 and age < 4) then 1 else 0 end as lead_Age_Revenue_Discrepancy,
dp.employees as lead_estimate_number_of_employees,
dp.mei::int as lead_is_mei,
dp.is_simples::int as lead_simple_tribute_method_applied,
dp.cnae_code as lead_economic_activity_national_registration_code,
dp.cnae as lead_economic_activity_national_registration_name,
dp.social_capital::float as lead_social_capital,
dp.number_partners as lead_number_of_shareholders,
dp.number_coligadas as lead_number_of_related_companies,
dp.number_branches as lead_number_of_branches,
case when dp.phone = '' then null else substring(dp.phone,2,2)::int end as lead_phone_area_code,
case when (case when dp.zip_code = '' then null else dp.zip_code end) is null then signer_address.signer_zipcode else dp.zip_code end as lead_company_zip_code,
dp.neighbourhood as lead_company_neighbourhood,
dp.city as lead_company_city,
dp.state as lead_company_state,
p.populacao as lead_citys_population,
case when (dp.city = 'RIO BRANCO' and dp.state = 'AC') or (dp.city = 'MACEIO' and dp.state = 'AL') or (dp.city = 'MACAPA' and dp.state = 'AP') or (dp.city = 'MANAUS' and dp.state = 'AM') or (dp.city = 'SALVADOR' and dp.state = 'BA') or (dp.city = 'FORTALEZA' and dp.state = 'CE') or 
	(dp.city = 'BRASILIA' and dp.state = 'DF') or (dp.city = 'VITORIA' and dp.state = 'ES') or (dp.city = 'GOIANIA' and dp.state = 'GO') or (dp.city = 'SAO LUIS' and dp.state = 'MA') or (dp.city = 'CUIABA' and dp.state = 'MT') or (dp.city = 'CAMPO GRANDE' and dp.state = 'MS') or 
	(dp.city = 'BELO HORIZONTE' and dp.state = 'MG') or (dp.city = 'BELEM' and dp.state = 'PA') or (dp.city = 'JOAO PESSOA' and dp.state = 'PB') or (dp.city = 'CURITIBA' and dp.state = 'PR') or (dp.city = 'RECIFE' and dp.state = 'PE') or (dp.city = 'TERESINA' and dp.state = 'PI') or 
	(dp.city = 'RIO DE JANEIRO' and dp.state = 'RJ') or (dp.city = 'NATAL' and dp.state = 'RN') or (dp.city = 'PORTO VELHO' and dp.state = 'RO') or (dp.city = 'BOA VISTA' and dp.state = 'RR') or (dp.city = 'FLORIANOPOLIS' and dp.state = 'SC') or (dp.city = 'SAO PAULO' and dp.state = 'SP') or 
	(dp.city = 'ARACAJU' and dp.state = 'SE') or (dp.city = 'PALMAS' and dp.state = 'TO') or (dp.city = 'PORTO ALEGRE' and dp.state = 'RS') then 1 else 0 end as lead_is_city_capital,
case when dp.state in ('AM','RR','AP','PA','TO','RO','AC') then 'NORTE' when dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA') then 'NORDESTE' when dp.state in ('MT','MS','GO','DF') then 'CENTRO-OESTE' when dp.state in ('SP','RJ','MG','ES') then 'SUDESTE' when dp.state in ('PR','RS','SC') then 'SUL' else null end as lead_state_region,
dp.facebook::int lead_has_facebook_page,
dp.likes lead_number_likes_facebook,
dp.streetview::int lead_has_google_streetview,
dp.site::int lead_has_website,
replace(replace(replace(replace(replace(dp.tax_health,'VERDE','Green'),'AZUL','Blue'),'AMARELO','Yellow'),'LARANJA','Orange'),'CINZA','Grey') as lead_tax_health,
replace(replace(replace(replace(dp.level_activity,'MEDIA','Medium'),'ALTA','High'),'MUITO BAIXA','Very Low'),'BAIXA','Low') as lead_level_activity,
replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(dp.activity_sector,'VAREJO','Retail'),'SERVICOS DE ALOJAMENTO/ALIMENTACAO','Accomodation/Feeding'),
	'TELECOM','Telecommunication'),'TRANSPORTE','Transportation'),'SERVICOS ADMINISTRATIVOS','Administrative Services'),'SERVICOS DE SAUDE','Health services'),'SERVICOS PROFISSIONAIS E TECNICOS','Technical/Professional Services'),'INDUSTRIA DIGITAL','Digital Industry'),
	'QUIMICA-PETROQUIMICA','Chemistry/Petrochemistry'),'SERVICOS DE EDUCACAO','Education Services'),'BENS DE CONSUMO','Consumer goods'),'ATACADO','Wholesale'),'INDUSTRIA DA CONSTRUCAO','Construction Industry'),'SIDERURGICA-METALURGIA','Steel Industry'),'ELETROELETRONICOS','Eletronic goods'),
	'PRODUTOS DE AGROPECUARIA','Farming goods'),'TEXTEIS','Textiles'),'PAPEL E CELULOSE','Paper/Celulosis'),'SERVICOS DE SANEAMENTO BASICO','Sanitation Services'),'INDUSTRIA AUTOMOTIVA','Car Industry'),'ENERGIA','Energy'),'BENS DE CAPITAL','Capital goods'),'SERVICOS DIVERSOS','Miscellaneous Services'),
	'DIVERSOS','Miscellaneous'),'FARMACEUTICA','Farmaceuticals'),'MINERACAO','Mining') as lead_activity_sector,
replace(replace(replace(replace(replace(dp.sector,'COMERCIO','Retail'),'SERVICOS','Services'),'INDUSTRIA','Industry'),'AGROPECUARIA','Farming'),'CONSTRUCAO CIVIL','Construction') as lead_sector,
case when upper(left(right(dp.email, length(dp.email)-position('@' in dp.email)),position('.' in right(dp.email, length(dp.email)-position('@' in dp.email)-1))))  in ('GMAIL','HOTMAIL','YAHOO','OUTLOOK','UOL','TERRA','BOL','LIVE','ICLOUD','IG','MSN','GLOBO') then 
	replace(upper(left(right(dp.email, length(dp.email)-position('@' in dp.email)),position('.' in right(dp.email, length(dp.email)-position('@' in dp.email)-1)))),'GMAIIL','GMAIL') else 'OTHER' end as lead_email_server,
dp.pgfn_debt as lead_government_debt,
dp.number_protests as lead_notary_protests_unit,
dp.protests_amount lead_notary_protests_value,
dp.protests_amount/month_revenue/12 lead_notary_protests_value_byrevenue,
dp.number_spc lead_bureau_derogatory_unit,
dp.spc_amount lead_bureau_derogatory_value,
dp.spc_amount/month_revenue/12 lead_bureau_derogatory_value_byrevenue,
lead_score::float as lead_biz_lead_score,
bizu_score::float as lead_biz_bizu_score,
case when bizu_score >= 850 then 'A+' when bizu_score between 700 and 849.99 then 'A-' when bizu_score between 650 and 699.99 then 'B+' when bizu_score between 600 and 649.99 then 'B-' when bizu_score between 500 and 599.99 then 'C+'when bizu_score between 400 and 499.99 then 'C-'when bizu_score between 300 and 399.99 then 'D'else 'E' end as Rating_class_bizu,
case when serasa_coleta is null then o.rating::int else serasa_coleta end as lead_experian_score4,
case when case when serasa_coleta is null then o.rating::int else serasa_coleta end >= 750 then 'A+' when case when serasa_coleta is null then o.rating::int else serasa_coleta end between 600 and 749.99 then 'A-' when case when serasa_coleta is null then o.rating::int else serasa_coleta end between 450 and 599.99 then 'B+' when case when serasa_coleta is null then o.rating::int else serasa_coleta end between 350 and 449.99 then 'B-' when case when serasa_coleta is null then o.rating::int else serasa_coleta end between 250 and 349.99 then 'C+'when case when serasa_coleta is null then o.rating::int else serasa_coleta end between 150 and 249.99 then 'C-'when case when serasa_coleta is null then o.rating::int else serasa_coleta end between 100 and 149.99 then 'D' else 'E' end as Rating_class_serasa,
pd_serasa lead_experian_score4_associated_pd,
--------------------------INITIAL OFFER
initialoff.date_inserted as offer_initial_offer_date,
initialoff.interest_rate offer_initial_offer_interest_rate,
initialoff.max_number_of_installments offer_initial_offer_max_term,
initialoff.max_value offer_initial_offer_max_value,
initialoff.count_neg - 1 as offer_count_negotiations,
--------------------------FINAL OFFER
o.date_inserted as offer_final_offer_date,
extract(hour from o.date_inserted)::int as offer_final_offer_hour,
extract(day from o.date_inserted)::int as offer_final_offer_day,
extract(month from o.date_inserted)::int as offer_final_offer_month,
extract(year from o.date_inserted)::int as offer_final_offer_year,
o.interest_rate offer_final_offer_interest_rate,
o.max_number_of_installments offer_final_offer_max_term,
o.max_value offer_final_offer_max_value,
--------------------------LOAN
lr.date_inserted as loan_request_date,
extract(hour from lr.date_inserted)::int as loan_request_hour,
extract(day from lr.date_inserted)::int as loan_request_day,
extract(month from lr.date_inserted)::int as loan_request_month,
extract(year from lr.date_inserted)::int as loan_request_year,
is_renew as loan_is_renewal,
lr.value loan_net_value,
li.total_payment loan_gross_value,
li.monthly_cet loan_final_interest_rate,
lr.number_of_installments loan_original_final_term,
(@li.pmt) as loan_original_final_pmt,
(@li.pmt)/month_revenue as loan_original_final_pmt_leverage,
lr.loan_date as loan_contract_date,
extract(day from lr.loan_date)::int as loan_contract_day,
extract(month from lr.loan_date)::int as loan_contract_month,
extract(year from lr.loan_date)::int as loan_contract_year,
lr.portfolio_id::text loan_portfolio_id,
ba.bank_id::text loan_bank_id,
extract(day from case when lr.payment_day is null then original_1st_pmt_due_to else lr.payment_day end) as loan_1st_pmt_chosen_day_of_month,
case when lr.payment_day is null then original_1st_pmt_due_to else lr.payment_day end - lr.loan_date as loan_grace_period,
--------------------------COLLECTION
fully_paid collection_loan_fully_paid,
due_inst + to_come_inst as collection_loan_current_term,
to_come_inst as collection_to_be_due_installments,
due_inst as collection_total_due_installments,
paid_inst collection_number_of_installments_paid,
late_inst collection_number_of_installments_curr_late, 
count_renegotiation as collection_count_renegotiation_plans,
ParcEmAberto as collection_current_installment_number_due,
dpd_soft.max_late_pmt1 as collection_max_late_pmt1,
dpd_soft.max_late_pmt2 as collection_max_late_pmt2,
dpd_soft.max_late_pmt3 as collection_max_late_pmt3,
dpd_soft.max_late_pmt4 as collection_max_late_pmt4,
dpd_soft.max_late_pmt5 as collection_max_late_pmt5,
dpd_soft.max_late_pmt6 as collection_max_late_pmt6,
dpd_soft.max_late_pmt7 as collection_max_late_pmt7,
dpd_soft.max_late_pmt8 as collection_max_late_pmt8,
dpd_soft.max_late_pmt9 as collection_max_late_pmt9,
dpd_soft.max_late_pmt10 as collection_max_late_pmt10,
dpd_soft.max_late_pmt11 as collection_max_late_pmt11,
dpd_soft.max_late_pmt12 as collection_max_late_pmt12,
case when paid_inst > 0 then 0 else dpd_soft.max_late_pmt1 end as collection_curr_fpd,
case when case when paid_inst > 0 then first_paid_inst_paid_on - original_1st_pmt_due_to else dpd_soft.max_late_pmt1 end >= 30 then 1 else 0 end as collection_fpd_30,
case when case when paid_inst > 0 then first_paid_inst_paid_on - original_1st_pmt_due_to else dpd_soft.max_late_pmt1 end >= 60 then 1 else 0 end as collection_fpd_60,
case when case when paid_inst > 0 then first_paid_inst_paid_on - original_1st_pmt_due_to else dpd_soft.max_late_pmt1 end >= 90 then 1 else 0 end as collection_fpd_90,
case when current_inst_due_to is null then 0 else current_date - current_inst_due_to end as collection_loan_curr_late_by,
case when current_inst_due_to is null then 0 else case when current_date - current_inst_due_to > 30 then 1 else 0 end end as collection_curr_over_30,
case when current_inst_due_to is null then 0 else case when current_date - current_inst_due_to > 60 then 1 else 0 end end as collection_curr_over_60,
case when current_inst_due_to is null then 0 else case when current_date - current_inst_due_to > 90 then 1 else 0 end end as collection_curr_over_90,
c.atraso_max_dias collection_client_max_late_by,
lr.atraso_max_dias collection_loan_max_late_by,
case when lr.atraso_max_dias > 30 then 1 else 0 end as collection_Ever_30,
case when lr.atraso_max_dias > 60 then 1 else 0 end as collection_Ever_60,
case when lr.atraso_max_dias > 90 then 1 else 0 end as collection_Ever_90,
case when c.atraso_max_dias > 180 then EAD * 1
when c.atraso_max_dias > 150 then EAD * 0.7 
when c.atraso_max_dias > 120 then EAD * 0.5
when c.atraso_max_dias > 90 then EAD * 0.3
when c.atraso_max_dias > 60 then EAD * 0.1
when c.atraso_max_dias > 30 then EAD * 0.03
when c.atraso_max_dias > 14 then EAD * 0.01
when c.atraso_max_dias > 0 then EAD * 0.005
else 0 end as collection_Provision_for_loss_Central_Bank,
case when EAD is null then 0 else EAD end as collection_ead,
curr_total_payment as "colletion_loan_p+i_value_curr",
original_total_payment as "colletion_loan_p+i_value_original",
pmt_atual as collection_current_pmt,
case when total_paid is null then 0 else total_paid end collection_loan_amount_paid,
case when late_payment is null then 0 else late_payment end collection_loan_amount_late,
case when writeoff is null then 0 else writeoff end collection_loan_amount_writeoff,
case when dpd_soft.max_late_pmt1 >= 60 then 1 when dpd_soft.max_late_pmt2 >= 60 then 2 when dpd_soft.max_late_pmt3 >= 60 then 3 when dpd_soft.max_late_pmt4 >= 60 then 4 when dpd_soft.max_late_pmt5 >= 60 then 5 when dpd_soft.max_late_pmt6 >= 60 then 6 when dpd_soft.max_late_pmt7 >= 60 then 7 when dpd_soft.max_late_pmt8 >= 60 then 8 when dpd_soft.max_late_pmt9 >= 60 then 9 when dpd_soft.max_late_pmt10 >= 60 then 10 when dpd_soft.max_late_pmt11 >= 60 then 11 when dpd_soft.max_late_pmt12 >= 60 then 12 else 0 end as collection_pmt_of_over60,
case when pmt3_actual_soft.loan_request_id is null then null when pmt3_actual_soft.max_late_pmt1 >= 30 or pmt3_actual_soft.max_late_pmt2 >= 30 or pmt3_actual_soft.max_late_pmt3 >= 30 then 1 else 0 end as collection_ever_over30_pmt3_reneg,
case when pmt3_actual_soft.loan_request_id is null then null when pmt3_actual_soft.max_late_pmt1 >= 60 or pmt3_actual_soft.max_late_pmt2 >= 60 or pmt3_actual_soft.max_late_pmt3 >= 60 then 1 else 0 end as collection_ever_over60_pmt3_reneg,
case when pmt6_actual_soft.loan_request_id is null then null when pmt6_actual_soft.max_late_pmt1 >= 30 or pmt6_actual_soft.max_late_pmt2 >= 30 or pmt6_actual_soft.max_late_pmt3 >= 30 or pmt6_actual_soft.max_late_pmt4 >= 30 or pmt6_actual_soft.max_late_pmt5 >= 30 or pmt6_actual_soft.max_late_pmt6 >= 30 then 1 else 0 end as collection_ever_over30_pmt6_reneg,
case when pmt6_actual_soft.loan_request_id is null then null when pmt6_actual_soft.max_late_pmt1 >= 60 or pmt6_actual_soft.max_late_pmt2 >= 60 or pmt6_actual_soft.max_late_pmt3 >= 60 or pmt6_actual_soft.max_late_pmt4 >= 60 or pmt6_actual_soft.max_late_pmt5 >= 60 or pmt6_actual_soft.max_late_pmt6 >= 60 then 1 else 0 end as collection_ever_over60_pmt6_reneg,
case when pmt6_actual_soft.loan_request_id is null then null when pmt6_actual_soft.max_late_pmt1 >= 90 or pmt6_actual_soft.max_late_pmt2 >= 90 or pmt6_actual_soft.max_late_pmt3 >= 90 or pmt6_actual_soft.max_late_pmt4 >= 90 or pmt6_actual_soft.max_late_pmt5 >= 90 or pmt6_actual_soft.max_late_pmt6 >= 90 then 1 else 0 end as collection_ever_over90_pmt6_reneg,
case when pmt9_actual_soft.loan_request_id is null then null when pmt9_actual_soft.max_late_pmt1 >= 30 or pmt9_actual_soft.max_late_pmt2 >= 30 or pmt9_actual_soft.max_late_pmt3 >= 30 or pmt9_actual_soft.max_late_pmt4 >= 30 or pmt9_actual_soft.max_late_pmt5 >= 30 or pmt9_actual_soft.max_late_pmt6 >= 30 or pmt9_actual_soft.max_late_pmt7 >= 30 or pmt9_actual_soft.max_late_pmt8 >= 30 or pmt9_actual_soft.max_late_pmt9 >= 30 then 1 else 0 end as collection_ever_over30_pmt9_reneg,
case when pmt9_actual_soft.loan_request_id is null then null when pmt9_actual_soft.max_late_pmt1 >= 60 or pmt9_actual_soft.max_late_pmt2 >= 60 or pmt9_actual_soft.max_late_pmt3 >= 60 or pmt9_actual_soft.max_late_pmt4 >= 60 or pmt9_actual_soft.max_late_pmt5 >= 60 or pmt9_actual_soft.max_late_pmt6 >= 60 or pmt9_actual_soft.max_late_pmt7 >= 60 or pmt9_actual_soft.max_late_pmt8 >= 60 or pmt9_actual_soft.max_late_pmt9 >= 60 then 1 else 0 end as collection_ever_over60_pmt9_reneg,
case when pmt9_actual_soft.loan_request_id is null then null when pmt9_actual_soft.max_late_pmt1 >= 90 or pmt9_actual_soft.max_late_pmt2 >= 90 or pmt9_actual_soft.max_late_pmt3 >= 90 or pmt9_actual_soft.max_late_pmt4 >= 90 or pmt9_actual_soft.max_late_pmt5 >= 90 or pmt9_actual_soft.max_late_pmt6 >= 90 or pmt9_actual_soft.max_late_pmt7 >= 90 or pmt9_actual_soft.max_late_pmt8 >= 90 or pmt9_actual_soft.max_late_pmt9 >= 90 then 1 else 0 end as collection_ever_over90_pmt9_reneg,
case when pmt3_actual_soft.loan_request_id is null then null when (case when pmt3_actual_soft.ultparcpaga is null then pmt3_actual_soft.max_late_pmt1 when pmt3_actual_soft.ultparcpaga = 1 then pmt3_actual_soft.max_late_pmt2 when pmt3_actual_soft.ultparcpaga = 2 then pmt3_actual_soft.max_late_pmt3 when pmt3_actual_soft.ultparcpaga >= 3 then 0 end) >= 30 then 1 else 0 end as collection_curr_over30_pmt3_reneg,
case when pmt3_actual_soft.loan_request_id is null then null when (case when pmt3_actual_soft.ultparcpaga is null then pmt3_actual_soft.max_late_pmt1 when pmt3_actual_soft.ultparcpaga = 1 then pmt3_actual_soft.max_late_pmt2 when pmt3_actual_soft.ultparcpaga = 2 then pmt3_actual_soft.max_late_pmt3 when pmt3_actual_soft.ultparcpaga >= 3 then 0 end) >= 60 then 1 else 0 end as collection_curr_over60_pmt3_reneg,
case when pmt6_actual_soft.loan_request_id is null then null when (case when pmt6_actual_soft.ultparcpaga is null then pmt6_actual_soft.max_late_pmt1 when pmt6_actual_soft.ultparcpaga = 1 then pmt6_actual_soft.max_late_pmt2 when pmt6_actual_soft.ultparcpaga = 2 then pmt6_actual_soft.max_late_pmt3 when pmt6_actual_soft.ultparcpaga = 3 then pmt6_actual_soft.max_late_pmt4 when pmt6_actual_soft.ultparcpaga = 4 then pmt6_actual_soft.max_late_pmt5 when pmt6_actual_soft.ultparcpaga = 5 then pmt6_actual_soft.max_late_pmt6 when pmt6_actual_soft.ultparcpaga >= 6 then 0 end) >= 30 then 1 else 0 end as collection_curr_over30_pmt6_reneg,
case when pmt6_actual_soft.loan_request_id is null then null when (case when pmt6_actual_soft.ultparcpaga is null then pmt6_actual_soft.max_late_pmt1 when pmt6_actual_soft.ultparcpaga = 1 then pmt6_actual_soft.max_late_pmt2 when pmt6_actual_soft.ultparcpaga = 2 then pmt6_actual_soft.max_late_pmt3 when pmt6_actual_soft.ultparcpaga = 3 then pmt6_actual_soft.max_late_pmt4 when pmt6_actual_soft.ultparcpaga = 4 then pmt6_actual_soft.max_late_pmt5 when pmt6_actual_soft.ultparcpaga = 5 then pmt6_actual_soft.max_late_pmt6 when pmt6_actual_soft.ultparcpaga >= 6 then 0 end) >= 60 then 1 else 0 end as collection_curr_over60_pmt6_reneg,
case when pmt6_actual_soft.loan_request_id is null then null when (case when pmt6_actual_soft.ultparcpaga is null then pmt6_actual_soft.max_late_pmt1 when pmt6_actual_soft.ultparcpaga = 1 then pmt6_actual_soft.max_late_pmt2 when pmt6_actual_soft.ultparcpaga = 2 then pmt6_actual_soft.max_late_pmt3 when pmt6_actual_soft.ultparcpaga = 3 then pmt6_actual_soft.max_late_pmt4 when pmt6_actual_soft.ultparcpaga = 4 then pmt6_actual_soft.max_late_pmt5 when pmt6_actual_soft.ultparcpaga = 5 then pmt6_actual_soft.max_late_pmt6 when pmt6_actual_soft.ultparcpaga >= 6 then 0 end) >= 90 then 1 else 0 end as collection_curr_over90_pmt6_reneg,
case when pmt9_actual_soft.loan_request_id is null then null when (case when pmt9_actual_soft.ultparcpaga is null then pmt9_actual_soft.max_late_pmt1 when pmt9_actual_soft.ultparcpaga = 1 then pmt9_actual_soft.max_late_pmt2 when pmt9_actual_soft.ultparcpaga = 2 then pmt9_actual_soft.max_late_pmt3 when pmt9_actual_soft.ultparcpaga = 3 then pmt9_actual_soft.max_late_pmt4 when pmt9_actual_soft.ultparcpaga = 4 then pmt9_actual_soft.max_late_pmt5 when pmt9_actual_soft.ultparcpaga = 5 then pmt9_actual_soft.max_late_pmt6 when pmt9_actual_soft.ultparcpaga = 6 then pmt9_actual_soft.max_late_pmt7 when pmt9_actual_soft.ultparcpaga = 7 then pmt9_actual_soft.max_late_pmt8 when pmt9_actual_soft.ultparcpaga = 8 then pmt9_actual_soft.max_late_pmt9 when pmt9_actual_soft.ultparcpaga >= 9 then 0 end) >= 30 then 1 else 0 end as collection_curr_over30_pmt9_reneg,
case when pmt9_actual_soft.loan_request_id is null then null when (case when pmt9_actual_soft.ultparcpaga is null then pmt9_actual_soft.max_late_pmt1 when pmt9_actual_soft.ultparcpaga = 1 then pmt9_actual_soft.max_late_pmt2 when pmt9_actual_soft.ultparcpaga = 2 then pmt9_actual_soft.max_late_pmt3 when pmt9_actual_soft.ultparcpaga = 3 then pmt9_actual_soft.max_late_pmt4 when pmt9_actual_soft.ultparcpaga = 4 then pmt9_actual_soft.max_late_pmt5 when pmt9_actual_soft.ultparcpaga = 5 then pmt9_actual_soft.max_late_pmt6 when pmt9_actual_soft.ultparcpaga = 6 then pmt9_actual_soft.max_late_pmt7 when pmt9_actual_soft.ultparcpaga = 7 then pmt9_actual_soft.max_late_pmt8 when pmt9_actual_soft.ultparcpaga = 8 then pmt9_actual_soft.max_late_pmt9 when pmt9_actual_soft.ultparcpaga >= 9 then 0 end) >= 60 then 1 else 0 end as collection_curr_over60_pmt9_reneg,
case when pmt9_actual_soft.loan_request_id is null then null when (case when pmt9_actual_soft.ultparcpaga is null then pmt9_actual_soft.max_late_pmt1 when pmt9_actual_soft.ultparcpaga = 1 then pmt9_actual_soft.max_late_pmt2 when pmt9_actual_soft.ultparcpaga = 2 then pmt9_actual_soft.max_late_pmt3 when pmt9_actual_soft.ultparcpaga = 3 then pmt9_actual_soft.max_late_pmt4 when pmt9_actual_soft.ultparcpaga = 4 then pmt9_actual_soft.max_late_pmt5 when pmt9_actual_soft.ultparcpaga = 5 then pmt9_actual_soft.max_late_pmt6 when pmt9_actual_soft.ultparcpaga = 6 then pmt9_actual_soft.max_late_pmt7 when pmt9_actual_soft.ultparcpaga = 7 then pmt9_actual_soft.max_late_pmt8 when pmt9_actual_soft.ultparcpaga = 8 then pmt9_actual_soft.max_late_pmt9 when pmt9_actual_soft.ultparcpaga >= 9 then 0 end) >= 90 then 1 else 0 end as collection_curr_over90_pmt9_reneg,
case when pmt3_orig_soft.loan_request_id is null then null when pmt3_orig_soft.max_late_pmt1 >= 30 or pmt3_orig_soft.max_late_pmt2 >= 30 or pmt3_orig_soft.max_late_pmt3 >= 30 then 1 else 0 end as collection_ever_over30_pmt3_orig_soft,
case when pmt3_orig_soft.loan_request_id is null then null when pmt3_orig_soft.max_late_pmt1 >= 60 or pmt3_orig_soft.max_late_pmt2 >= 60 or pmt3_orig_soft.max_late_pmt3 >= 60 then 1 else 0 end as collection_ever_over60_pmt3_orig_soft,
case when pmt6_orig_soft.loan_request_id is null then null when pmt6_orig_soft.max_late_pmt1 >= 30 or pmt6_orig_soft.max_late_pmt2 >= 30 or pmt6_orig_soft.max_late_pmt3 >= 30 or pmt6_orig_soft.max_late_pmt4 >= 30 or pmt6_orig_soft.max_late_pmt5 >= 30 or pmt6_orig_soft.max_late_pmt6 >= 30 then 1 else 0 end as collection_ever_over30_pmt6_orig_soft,
case when pmt6_orig_soft.loan_request_id is null then null when pmt6_orig_soft.max_late_pmt1 >= 60 or pmt6_orig_soft.max_late_pmt2 >= 60 or pmt6_orig_soft.max_late_pmt3 >= 60 or pmt6_orig_soft.max_late_pmt4 >= 60 or pmt6_orig_soft.max_late_pmt5 >= 60 or pmt6_orig_soft.max_late_pmt6 >= 60 then 1 else 0 end as collection_ever_over60_pmt6_orig_soft,
case when pmt6_orig_soft.loan_request_id is null then null when pmt6_orig_soft.max_late_pmt1 >= 90 or pmt6_orig_soft.max_late_pmt2 >= 90 or pmt6_orig_soft.max_late_pmt3 >= 90 or pmt6_orig_soft.max_late_pmt4 >= 90 or pmt6_orig_soft.max_late_pmt5 >= 90 or pmt6_orig_soft.max_late_pmt6 >= 90 then 1 else 0 end as collection_ever_over90_pmt6_orig_soft,
case when pmt9_orig_soft.loan_request_id is null then null when pmt9_orig_soft.max_late_pmt1 >= 30 or pmt9_orig_soft.max_late_pmt2 >= 30 or pmt9_orig_soft.max_late_pmt3 >= 30 or pmt9_orig_soft.max_late_pmt4 >= 30 or pmt9_orig_soft.max_late_pmt5 >= 30 or pmt9_orig_soft.max_late_pmt6 >= 30 or pmt9_orig_soft.max_late_pmt7 >= 30 or pmt9_orig_soft.max_late_pmt8 >= 30 or pmt9_orig_soft.max_late_pmt9 >= 30 then 1 else 0 end as collection_ever_over30_pmt9_orig_soft,
case when pmt9_orig_soft.loan_request_id is null then null when pmt9_orig_soft.max_late_pmt1 >= 60 or pmt9_orig_soft.max_late_pmt2 >= 60 or pmt9_orig_soft.max_late_pmt3 >= 60 or pmt9_orig_soft.max_late_pmt4 >= 60 or pmt9_orig_soft.max_late_pmt5 >= 60 or pmt9_orig_soft.max_late_pmt6 >= 60 or pmt9_orig_soft.max_late_pmt7 >= 60 or pmt9_orig_soft.max_late_pmt8 >= 60 or pmt9_orig_soft.max_late_pmt9 >= 60 then 1 else 0 end as collection_ever_over60_pmt9_orig_soft,
case when pmt9_orig_soft.loan_request_id is null then null when pmt9_orig_soft.max_late_pmt1 >= 90 or pmt9_orig_soft.max_late_pmt2 >= 90 or pmt9_orig_soft.max_late_pmt3 >= 90 or pmt9_orig_soft.max_late_pmt4 >= 90 or pmt9_orig_soft.max_late_pmt5 >= 90 or pmt9_orig_soft.max_late_pmt6 >= 90 or pmt9_orig_soft.max_late_pmt7 >= 90 or pmt9_orig_soft.max_late_pmt8 >= 90 or pmt9_orig_soft.max_late_pmt9 >= 90 then 1 else 0 end as collection_ever_over90_pmt9_orig_soft,
case when pmt3_orig_soft.loan_request_id is null then null when (case when pmt3_orig_soft.ultparcpaga is null then pmt3_orig_soft.max_late_pmt1 when pmt3_orig_soft.ultparcpaga = 1 then pmt3_orig_soft.max_late_pmt2 when pmt3_orig_soft.ultparcpaga = 2 then pmt3_orig_soft.max_late_pmt3 when pmt3_orig_soft.ultparcpaga >= 3 then 0 end) >= 30 then 1 else 0 end as collection_curr_over30_pmt3_orig_soft,
case when pmt3_orig_soft.loan_request_id is null then null when (case when pmt3_orig_soft.ultparcpaga is null then pmt3_orig_soft.max_late_pmt1 when pmt3_orig_soft.ultparcpaga = 1 then pmt3_orig_soft.max_late_pmt2 when pmt3_orig_soft.ultparcpaga = 2 then pmt3_orig_soft.max_late_pmt3 when pmt3_orig_soft.ultparcpaga >= 3 then 0 end) >= 60 then 1 else 0 end as collection_curr_over60_pmt3_orig_soft,
case when pmt6_orig_soft.loan_request_id is null then null when (case when pmt6_orig_soft.ultparcpaga is null then pmt6_orig_soft.max_late_pmt1 when pmt6_orig_soft.ultparcpaga = 1 then pmt6_orig_soft.max_late_pmt2 when pmt6_orig_soft.ultparcpaga = 2 then pmt6_orig_soft.max_late_pmt3 when pmt6_orig_soft.ultparcpaga = 3 then pmt6_orig_soft.max_late_pmt4 when pmt6_orig_soft.ultparcpaga = 4 then pmt6_orig_soft.max_late_pmt5 when pmt6_orig_soft.ultparcpaga = 5 then pmt6_orig_soft.max_late_pmt6 when pmt6_orig_soft.ultparcpaga >= 6 then 0 end) >= 30 then 1 else 0 end as collection_curr_over30_pmt6_orig_soft,
case when pmt6_orig_soft.loan_request_id is null then null when (case when pmt6_orig_soft.ultparcpaga is null then pmt6_orig_soft.max_late_pmt1 when pmt6_orig_soft.ultparcpaga = 1 then pmt6_orig_soft.max_late_pmt2 when pmt6_orig_soft.ultparcpaga = 2 then pmt6_orig_soft.max_late_pmt3 when pmt6_orig_soft.ultparcpaga = 3 then pmt6_orig_soft.max_late_pmt4 when pmt6_orig_soft.ultparcpaga = 4 then pmt6_orig_soft.max_late_pmt5 when pmt6_orig_soft.ultparcpaga = 5 then pmt6_orig_soft.max_late_pmt6 when pmt6_orig_soft.ultparcpaga >= 6 then 0 end) >= 60 then 1 else 0 end as collection_curr_over60_pmt6_orig_soft,
case when pmt6_orig_soft.loan_request_id is null then null when (case when pmt6_orig_soft.ultparcpaga is null then pmt6_orig_soft.max_late_pmt1 when pmt6_orig_soft.ultparcpaga = 1 then pmt6_orig_soft.max_late_pmt2 when pmt6_orig_soft.ultparcpaga = 2 then pmt6_orig_soft.max_late_pmt3 when pmt6_orig_soft.ultparcpaga = 3 then pmt6_orig_soft.max_late_pmt4 when pmt6_orig_soft.ultparcpaga = 4 then pmt6_orig_soft.max_late_pmt5 when pmt6_orig_soft.ultparcpaga = 5 then pmt6_orig_soft.max_late_pmt6 when pmt6_orig_soft.ultparcpaga >= 6 then 0 end) >= 90 then 1 else 0 end as collection_curr_over90_pmt6_orig_soft,
case when pmt9_orig_soft.loan_request_id is null then null when (case when pmt9_orig_soft.ultparcpaga is null then pmt9_orig_soft.max_late_pmt1 when pmt9_orig_soft.ultparcpaga = 1 then pmt9_orig_soft.max_late_pmt2 when pmt9_orig_soft.ultparcpaga = 2 then pmt9_orig_soft.max_late_pmt3 when pmt9_orig_soft.ultparcpaga = 3 then pmt9_orig_soft.max_late_pmt4 when pmt9_orig_soft.ultparcpaga = 4 then pmt9_orig_soft.max_late_pmt5 when pmt9_orig_soft.ultparcpaga = 5 then pmt9_orig_soft.max_late_pmt6 when pmt9_orig_soft.ultparcpaga = 6 then pmt9_orig_soft.max_late_pmt7 when pmt9_orig_soft.ultparcpaga = 7 then pmt9_orig_soft.max_late_pmt8 when pmt9_orig_soft.ultparcpaga = 8 then pmt9_orig_soft.max_late_pmt9 when pmt9_orig_soft.ultparcpaga >= 9 then 0 end) >= 30 then 1 else 0 end as collection_curr_over30_pmt9_orig_soft,
case when pmt9_orig_soft.loan_request_id is null then null when (case when pmt9_orig_soft.ultparcpaga is null then pmt9_orig_soft.max_late_pmt1 when pmt9_orig_soft.ultparcpaga = 1 then pmt9_orig_soft.max_late_pmt2 when pmt9_orig_soft.ultparcpaga = 2 then pmt9_orig_soft.max_late_pmt3 when pmt9_orig_soft.ultparcpaga = 3 then pmt9_orig_soft.max_late_pmt4 when pmt9_orig_soft.ultparcpaga = 4 then pmt9_orig_soft.max_late_pmt5 when pmt9_orig_soft.ultparcpaga = 5 then pmt9_orig_soft.max_late_pmt6 when pmt9_orig_soft.ultparcpaga = 6 then pmt9_orig_soft.max_late_pmt7 when pmt9_orig_soft.ultparcpaga = 7 then pmt9_orig_soft.max_late_pmt8 when pmt9_orig_soft.ultparcpaga = 8 then pmt9_orig_soft.max_late_pmt9 when pmt9_orig_soft.ultparcpaga >= 9 then 0 end) >= 60 then 1 else 0 end as collection_curr_over60_pmt9_orig_soft,
case when pmt9_orig_soft.loan_request_id is null then null when (case when pmt9_orig_soft.ultparcpaga is null then pmt9_orig_soft.max_late_pmt1 when pmt9_orig_soft.ultparcpaga = 1 then pmt9_orig_soft.max_late_pmt2 when pmt9_orig_soft.ultparcpaga = 2 then pmt9_orig_soft.max_late_pmt3 when pmt9_orig_soft.ultparcpaga = 3 then pmt9_orig_soft.max_late_pmt4 when pmt9_orig_soft.ultparcpaga = 4 then pmt9_orig_soft.max_late_pmt5 when pmt9_orig_soft.ultparcpaga = 5 then pmt9_orig_soft.max_late_pmt6 when pmt9_orig_soft.ultparcpaga = 6 then pmt9_orig_soft.max_late_pmt7 when pmt9_orig_soft.ultparcpaga = 7 then pmt9_orig_soft.max_late_pmt8 when pmt9_orig_soft.ultparcpaga = 8 then pmt9_orig_soft.max_late_pmt9 when pmt9_orig_soft.ultparcpaga >= 9 then 0 end) >= 90 then 1 else 0 end as collection_curr_over90_pmt9_orig_soft,
case when pmt6_orig_aggressive.loan_request_id is null then null when pmt6_orig_aggressive.max_late_pmt1 >= 30 or pmt6_orig_aggressive.max_late_pmt2 >= 30 or pmt6_orig_aggressive.max_late_pmt3 >= 30 or pmt6_orig_aggressive.max_late_pmt4 >= 30 or pmt6_orig_aggressive.max_late_pmt5 >= 30 or pmt6_orig_aggressive.max_late_pmt6 >= 30 then 1 else 0 end as collection_ever_over30_pmt6_orig_aggressive,
case when pmt6_orig_aggressive.loan_request_id is null then null when pmt6_orig_aggressive.max_late_pmt1 >= 60 or pmt6_orig_aggressive.max_late_pmt2 >= 60 or pmt6_orig_aggressive.max_late_pmt3 >= 60 or pmt6_orig_aggressive.max_late_pmt4 >= 60 or pmt6_orig_aggressive.max_late_pmt5 >= 60 or pmt6_orig_aggressive.max_late_pmt6 >= 60 then 1 else 0 end as collection_ever_over60_pmt6_orig_aggressive,
case when pmt6_orig_aggressive.loan_request_id is null then null when pmt6_orig_aggressive.max_late_pmt1 >= 90 or pmt6_orig_aggressive.max_late_pmt2 >= 90 or pmt6_orig_aggressive.max_late_pmt3 >= 90 or pmt6_orig_aggressive.max_late_pmt4 >= 90 or pmt6_orig_aggressive.max_late_pmt5 >= 90 or pmt6_orig_aggressive.max_late_pmt6 >= 90 then 1 else 0 end as collection_ever_over90_pmt6_orig_aggressive,
case when pmt6_orig_aggressive.loan_request_id is null then null when (case when pmt6_orig_aggressive.ultparcpaga is null then pmt6_orig_aggressive.max_late_pmt1 when pmt6_orig_aggressive.ultparcpaga = 1 then pmt6_orig_aggressive.max_late_pmt2 when pmt6_orig_aggressive.ultparcpaga = 2 then pmt6_orig_aggressive.max_late_pmt3 when pmt6_orig_aggressive.ultparcpaga = 3 then pmt6_orig_aggressive.max_late_pmt4 when pmt6_orig_aggressive.ultparcpaga = 4 then pmt6_orig_aggressive.max_late_pmt5 when pmt6_orig_aggressive.ultparcpaga = 5 then pmt6_orig_aggressive.max_late_pmt6 when pmt6_orig_aggressive.ultparcpaga >= 6 then 0 end) >= 30 then 1 else 0 end as collection_curr_over30_pmt6_orig_aggressive,
case when pmt6_orig_aggressive.loan_request_id is null then null when (case when pmt6_orig_aggressive.ultparcpaga is null then pmt6_orig_aggressive.max_late_pmt1 when pmt6_orig_aggressive.ultparcpaga = 1 then pmt6_orig_aggressive.max_late_pmt2 when pmt6_orig_aggressive.ultparcpaga = 2 then pmt6_orig_aggressive.max_late_pmt3 when pmt6_orig_aggressive.ultparcpaga = 3 then pmt6_orig_aggressive.max_late_pmt4 when pmt6_orig_aggressive.ultparcpaga = 4 then pmt6_orig_aggressive.max_late_pmt5 when pmt6_orig_aggressive.ultparcpaga = 5 then pmt6_orig_aggressive.max_late_pmt6 when pmt6_orig_aggressive.ultparcpaga >= 6 then 0 end) >= 60 then 1 else 0 end as collection_curr_over60_pmt6_orig_aggressive,
case when pmt6_orig_aggressive.loan_request_id is null then null when (case when pmt6_orig_aggressive.ultparcpaga is null then pmt6_orig_aggressive.max_late_pmt1 when pmt6_orig_aggressive.ultparcpaga = 1 then pmt6_orig_aggressive.max_late_pmt2 when pmt6_orig_aggressive.ultparcpaga = 2 then pmt6_orig_aggressive.max_late_pmt3 when pmt6_orig_aggressive.ultparcpaga = 3 then pmt6_orig_aggressive.max_late_pmt4 when pmt6_orig_aggressive.ultparcpaga = 4 then pmt6_orig_aggressive.max_late_pmt5 when pmt6_orig_aggressive.ultparcpaga = 5 then pmt6_orig_aggressive.max_late_pmt6 when pmt6_orig_aggressive.ultparcpaga >= 6 then 0 end) >= 90 then 1 else 0 end as collection_curr_over90_pmt6_orig_aggressive,
case when mob7_soft.loan_request_id is null then null when mob7_soft.max_late_pmt1 >= 30 or mob7_soft.max_late_pmt2 >= 30 or mob7_soft.max_late_pmt3 >= 30 or mob7_soft.max_late_pmt4 >= 30 or mob7_soft.max_late_pmt5 >= 30 or mob7_soft.max_late_pmt6 >= 30 then 1 else 0 end as collection_ever_over30_mob7_soft,
case when mob7_soft.loan_request_id is null then null when mob7_soft.max_late_pmt1 >= 60 or mob7_soft.max_late_pmt2 >= 60 or mob7_soft.max_late_pmt3 >= 60 or mob7_soft.max_late_pmt4 >= 60 or mob7_soft.max_late_pmt5 >= 60 or mob7_soft.max_late_pmt6 >= 60 then 1 else 0 end as collection_ever_over60_mob7_soft,
case when mob7_soft.loan_request_id is null then null when mob7_soft.max_late_pmt1 >= 90 or mob7_soft.max_late_pmt2 >= 90 or mob7_soft.max_late_pmt3 >= 90 or mob7_soft.max_late_pmt4 >= 90 or mob7_soft.max_late_pmt5 >= 90 or mob7_soft.max_late_pmt6 >= 90 then 1 else 0 end as collection_ever_over90_mob7_soft,
case when mob7_soft.loan_request_id is null then null when (case when mob7_soft.ultparcpaga is null then mob7_soft.max_late_pmt1 when mob7_soft.ultparcpaga = 1 then mob7_soft.max_late_pmt2 when mob7_soft.ultparcpaga = 2 then mob7_soft.max_late_pmt3 when mob7_soft.ultparcpaga = 3 then mob7_soft.max_late_pmt4 when mob7_soft.ultparcpaga = 4 then mob7_soft.max_late_pmt5 when mob7_soft.ultparcpaga = 5 then mob7_soft.max_late_pmt6 when mob7_soft.ultparcpaga >= 6 then 0 end) >= 30 then 1 else 0 end as collection_curr_over30_mob7_soft,
case when mob7_soft.loan_request_id is null then null when (case when mob7_soft.ultparcpaga is null then mob7_soft.max_late_pmt1 when mob7_soft.ultparcpaga = 1 then mob7_soft.max_late_pmt2 when mob7_soft.ultparcpaga = 2 then mob7_soft.max_late_pmt3 when mob7_soft.ultparcpaga = 3 then mob7_soft.max_late_pmt4 when mob7_soft.ultparcpaga = 4 then mob7_soft.max_late_pmt5 when mob7_soft.ultparcpaga = 5 then mob7_soft.max_late_pmt6 when mob7_soft.ultparcpaga >= 6 then 0 end) >= 60 then 1 else 0 end as collection_curr_over60_mob7_soft,
case when mob7_soft.loan_request_id is null then null when (case when mob7_soft.ultparcpaga is null then mob7_soft.max_late_pmt1 when mob7_soft.ultparcpaga = 1 then mob7_soft.max_late_pmt2 when mob7_soft.ultparcpaga = 2 then mob7_soft.max_late_pmt3 when mob7_soft.ultparcpaga = 3 then mob7_soft.max_late_pmt4 when mob7_soft.ultparcpaga = 4 then mob7_soft.max_late_pmt5 when mob7_soft.ultparcpaga = 5 then mob7_soft.max_late_pmt6 when mob7_soft.ultparcpaga >= 6 then 0 end) >= 90 then 1 else 0 end as collection_curr_over90_mob7_soft,
case when mob7_aggressive.loan_request_id is null then null when mob7_aggressive.max_late_pmt1 >= 30 or mob7_aggressive.max_late_pmt2 >= 30 or mob7_aggressive.max_late_pmt3 >= 30 or mob7_aggressive.max_late_pmt4 >= 30 or mob7_aggressive.max_late_pmt5 >= 30 or mob7_aggressive.max_late_pmt6 >= 30 then 1 else 0 end as collection_ever_over30_mob7_aggressive,
case when mob7_aggressive.loan_request_id is null then null when mob7_aggressive.max_late_pmt1 >= 60 or mob7_aggressive.max_late_pmt2 >= 60 or mob7_aggressive.max_late_pmt3 >= 60 or mob7_aggressive.max_late_pmt4 >= 60 or mob7_aggressive.max_late_pmt5 >= 60 or mob7_aggressive.max_late_pmt6 >= 60 then 1 else 0 end as collection_ever_over60_mob7_aggressive,
case when mob7_aggressive.loan_request_id is null then null when mob7_aggressive.max_late_pmt1 >= 90 or mob7_aggressive.max_late_pmt2 >= 90 or mob7_aggressive.max_late_pmt3 >= 90 or mob7_aggressive.max_late_pmt4 >= 90 or mob7_aggressive.max_late_pmt5 >= 90 or mob7_aggressive.max_late_pmt6 >= 90 then 1 else 0 end as collection_ever_over90_mob7_aggressive,
case when mob7_aggressive.loan_request_id is null then null when (case when mob7_aggressive.ultparcpaga is null then mob7_aggressive.max_late_pmt1 when mob7_aggressive.ultparcpaga = 1 then mob7_aggressive.max_late_pmt2 when mob7_aggressive.ultparcpaga = 2 then mob7_aggressive.max_late_pmt3 when mob7_aggressive.ultparcpaga = 3 then mob7_aggressive.max_late_pmt4 when mob7_aggressive.ultparcpaga = 4 then mob7_aggressive.max_late_pmt5 when mob7_aggressive.ultparcpaga = 5 then mob7_aggressive.max_late_pmt6 when mob7_aggressive.ultparcpaga >= 6 then 0 end) >= 30 then 1 else 0 end as collection_curr_over30_mob7_aggressive,
case when mob7_aggressive.loan_request_id is null then null when (case when mob7_aggressive.ultparcpaga is null then mob7_aggressive.max_late_pmt1 when mob7_aggressive.ultparcpaga = 1 then mob7_aggressive.max_late_pmt2 when mob7_aggressive.ultparcpaga = 2 then mob7_aggressive.max_late_pmt3 when mob7_aggressive.ultparcpaga = 3 then mob7_aggressive.max_late_pmt4 when mob7_aggressive.ultparcpaga = 4 then mob7_aggressive.max_late_pmt5 when mob7_aggressive.ultparcpaga = 5 then mob7_aggressive.max_late_pmt6 when mob7_aggressive.ultparcpaga >= 6 then 0 end) >= 60 then 1 else 0 end as collection_curr_over60_mob7_aggressive,
case when mob7_aggressive.loan_request_id is null then null when (case when mob7_aggressive.ultparcpaga is null then mob7_aggressive.max_late_pmt1 when mob7_aggressive.ultparcpaga = 1 then mob7_aggressive.max_late_pmt2 when mob7_aggressive.ultparcpaga = 2 then mob7_aggressive.max_late_pmt3 when mob7_aggressive.ultparcpaga = 3 then mob7_aggressive.max_late_pmt4 when mob7_aggressive.ultparcpaga = 4 then mob7_aggressive.max_late_pmt5 when mob7_aggressive.ultparcpaga = 5 then mob7_aggressive.max_late_pmt6 when mob7_aggressive.ultparcpaga >= 6 then 0 end) >= 90 then 1 else 0 end as collection_curr_over90_mob7_aggressive,
--------------------------SCR HISTORICO PJ
--Divida longo prazo
case when tem_scr = 0 then null else case when Ever_long_term_debt_PJ > 0 then 1 else 0 end end as scr_Has_Debt_History_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_Curr*1000 end as scr_curr_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_1M*1000 end as scr_2M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_2M*1000 end as scr_3M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_3M*1000 end as scr_4M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_4M*1000 end as scr_5M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_5M*1000 end as scr_6M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_6M*1000 end as scr_7M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_7M*1000 end as scr_8M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_8M*1000 end as scr_9M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_9M*1000 end as scr_10M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_10M*1000 end as scr_11M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_11M*1000 end as scr_12M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Ever_long_term_debt_PJ*1000 end as scr_ever_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Max_Long_Term_Debt_PJ*1000 end as scr_max_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Long_Term_Debt_PJ_Curr/month_revenue/12*1000 end as scr_long_term_debt_PJ_over_revenue,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else (Long_Term_Debt_PJ_Curr - Long_Term_Debt_PJ_5M)/month_revenue/12*1000 end as scr_var_abs_6M_long_term_debt_PJ_over_revenue,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else (Long_Term_Debt_PJ_Curr - Long_Term_Debt_PJ_11M)/month_revenue/12*1000 end as scr_var_abs_12M_long_term_debt_PJ_over_revenue,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 when Long_Term_Debt_PJ_Curr = 0 then 0 when Long_Term_Debt_PJ_5M = 0 then 2019 else Long_Term_Debt_PJ_Curr / Long_Term_Debt_PJ_5M-1 end as scr_var_rel_6M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 when Long_Term_Debt_PJ_Curr = 0 then 0 when Long_Term_Debt_PJ_11M = 0 then 2019 else Long_Term_Debt_PJ_Curr / Long_Term_Debt_PJ_11M-1 end as scr_var_rel_12M_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Qtd_meses_escopo_pj end as scr_number_of_months_in_report_pj,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Meses_Aumento_DividaPJ end as scr_Months_Rise_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Meses_Aumento_DividaPJ/(case when Qtd_meses_escopo_pj <= 1 then 1 else Qtd_meses_escopo_pj-1 end) end as scr_Months_Rise_long_term_debt_PJ_over_total_months,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Meses_Reducao_DividaPJ end as scr_Months_Dive_long_term_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Meses_Reducao_DividaPJ/(case when Qtd_meses_escopo_pj <= 1 then 1 else Qtd_meses_escopo_pj-1 end) end as scr_Months_Dive_long_term_debt_PJ_over_total_months,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else months_sth_pj end as scr_month_consecutive_dive_long_term_debt_pj,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else months_sth_pj/(case when Qtd_meses_escopo_pj <= 1 then 1 else Qtd_meses_escopo_pj-1 end) end as scr_month_consecutive_dive_long_term_debt_pj_over_total_months,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else months_hth_pj end as scr_month_consecutive_rise_long_term_debt_pj,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else months_hth_pj/(case when Qtd_meses_escopo_pj <= 1 then 1 else Qtd_meses_escopo_pj-1 end) end as scr_month_consecutive_rise_long_term_debt_pj_over_total_months,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 when Max_Long_Term_Debt_PJ = 0 then 0 else Long_Term_Debt_PJ_Curr / Max_Long_Term_Debt_PJ end as scr_curr_over_max_long_term_debt_pj,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else case when Long_Term_Debt_PJ_Curr = Max_Long_Term_Debt_PJ then 1 else 0 end end as scr_All_TimeHigh_long_term_debt_PJ,
--Vencido
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Overdue_PJ_Curr*1000 end as scr_curr_overdue_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Max_Overdue_PJ*1000 end as scr_max_overdue_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Months_Overdue_PJ end as scr_Months_Overdue_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Months_Overdue_PJ/Qtd_meses_escopo_pj end as scr_Months_Overdue_PJ_over_total_months,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then -1 when Months_Since_Last_Overdue_PJ is null then - 1 else Months_Since_Last_Overdue_PJ end as scr_Months_since_last_Overdue_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Max_Overdue_PJ/month_revenue/12*1000 end as scr_Max_Overdue_PJ_over_Revenue,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Overdue_PJ_Curr/month_revenue/12*1000 end as scr_Curr_Overdue_PJ_over_Revenue,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 when Overdue_PJ_Curr = 0 then 0 when Long_Term_Debt_PJ_Curr = 0 then 2019 else Overdue_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_Curr_Overdue_PJ_long_term_debt,
--Prejuizo
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Default_PJ_Curr*1000 end as scr_curr_default_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Max_Default_PJ*1000 end as scr_max_default_debt_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Months_Default_PJ end as scr_Months_Default_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Months_Default_PJ/Qtd_meses_escopo_pj end as scr_Months_Default_PJ_over_total_months,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then -1 when Months_Since_Last_Default_PJ is null then - 1 else Months_Since_Last_Default_PJ end as scr_Months_since_last_Default_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Max_Default_PJ/month_revenue/12*1000 end as scr_Max_Default_PJ_over_revenue,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Default_PJ_Curr/month_revenue/12*1000 end as scr_Curr_Default_PJ_over_revenue,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 when Default_PJ_Curr = 0 then 0 when Long_Term_Debt_PJ_Curr = 0 then 2019 else Default_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_Curr_Default_PJ_over_long_term_debt,
--Saldo
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 else Saldo_Amort_DividaPJ * 1000 end as scr_long_term_debt_amortization_PJ,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 when Saldo_Amort_DividaPJ = 0 then 0 else Saldo_Amort_DividaPJ/Meses_Reducao_DividaPJ/month_revenue/12*1000 end as scr_Avg_Amortization_LTDebt_PJ_over_revenue,
case when tem_scr = 0 then null when Long_Term_Debt_PJ_Curr is null then 0 when Saldo_Amort_DividaPJ = 0 then 0 else Saldo_Amort_DividaPJ/Meses_Reducao_DividaPJ/((@li.pmt))*1000 end as scr_Avg_Amortization_LTDebt_PJ_over_loan_PMT,
--Limite
case when tem_scr = 0 then null when Ever_Lim_Cred_pj is null then 0 else Ever_Lim_Cred_pj * 1000 end as scr_ever_credit_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_Curr * 1000 end as scr_curr_credit_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_1M * 1000 end as scr_2M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_2M * 1000 end as scr_3M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_3M * 1000 end as scr_4M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_4M * 1000 end as scr_5M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_5M * 1000 end as scr_6M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_6M * 1000 end as scr_7M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_7M * 1000 end as scr_8M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_8M * 1000 end as scr_9M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_9M * 1000 end as scr_10M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_10M * 1000 end as scr_11M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_11M * 1000 end as scr_12M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_Curr / month_revenue / 12 * 1000 end as scr_Curr_Credit_Limit_pj_over_revenue,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_Curr / case when amount_requested < 1000 then o.max_value else amount_requested end*1000 end as scr_Curr_Credit_Limit_pj_over_application_value,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Lim_Cred_pj_Curr / li.total_payment * 1000 end as scr_Curr_Credit_Limit_pj_over_loan_value,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 when Lim_Cred_pj_Curr = 0 then 0 when Long_Term_Debt_pj_Curr = 0 then 2019 else Lim_Cred_pj_Curr/Long_Term_Debt_pj_Curr end as scr_Curr_Credit_Limit_pj_over_long_term_debt,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Max_Lim_Cred_pj * 1000 end as scr_max_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 when Max_Lim_Cred_pj = 0 then 0 else Lim_Cred_pj_Curr / Max_Lim_Cred_pj end as scr_curr_over_max_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else case when Lim_Cred_pj_Curr = Max_Lim_Cred_pj then 1 else 0 end end as scr_All_TimeHigh_credit_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else (Lim_Cred_pj_Curr - Long_Term_Debt_pj_5M) / month_revenue / 12 * 1000 end as scr_var_abs_6M_cred_limit_pj_over_revenue,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else (Lim_Cred_pj_Curr - Long_Term_Debt_pj_11M)/month_revenue / 12 * 1000 end as scr_var_abs_12M_cred_limit_pj_over_revenue,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 when Long_Term_Debt_pj_Curr = 0 then 0 when Long_Term_Debt_pj_5M = 0 then 2019 else Long_Term_Debt_pj_Curr / Long_Term_Debt_pj_5M - 1 end as scr_var_rel_6M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 when Long_Term_Debt_pj_Curr = 0 then 0 when Long_Term_Debt_pj_11M = 0 then 2019 else Long_Term_Debt_pj_Curr / Long_Term_Debt_pj_11M - 1 end as scr_var_rel_12M_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Meses_Aumento_Lim_Cred_pj end as scr_Months_Rise_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Meses_Aumento_Lim_Cred_pj / (case when Qtd_meses_escopo_pj <= 1 then 1 else Qtd_meses_escopo_pj - 1 end) end as scr_Months_Rise_cred_limit_pj_over_total_months,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Meses_Reducao_Lim_Cred_pj end as scr_Months_Dive_cred_limit_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else Meses_Reducao_Lim_Cred_pj / (case when Qtd_meses_escopo_pj <= 1 then 1 else Qtd_meses_escopo_pj - 1 end) end as scr_Months_Dive_cred_limit_pj_over_total_months,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else months_sth_lim_cred_pj end as scr_month_consecutive_dive_cred_limit_debt_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else months_sth_lim_cred_pj / (case when Qtd_meses_escopo_pj <= 1 then 1 else Qtd_meses_escopo_pj - 1 end) end as scr_month_consecutive_dive_cred_limit_pj_over_total_months,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else months_hth_lim_cred_pj end as scr_month_consecutive_rise_cred_limit_debt_pj,
case when tem_scr = 0 then null when Lim_Cred_pj_Curr is null then 0 else months_hth_lim_cred_pj / (case when Qtd_meses_escopo_pj <= 1 then 1 else Qtd_meses_escopo_pj - 1 end) end as scr_month_consecutive_rise_cred_limit_pj_over_total_months,
--Operacoes
case when tem_scr = 0 then null else case when Num_Ops_PJ is null then 0 else Num_Ops_PJ end end as scr_Num_Fin_Ops_PJ,
case when tem_scr = 0 then null else case when Num_FIs_PJ is null then 0 else Num_FIs_PJ end end as scr_Num_Fin_Inst_PJ,
case when tem_scr = 0 then null else case when First_Relation_FI_PJ is null then 0 else First_Relation_FI_PJ end end as scr_First_Relation_Fin_Inst_PJ,
--------------------------SCR HISTORICO PF
--Divida longo prazo
case when tem_scr_pf = 0 then null else case when Ever_long_term_debt_PF > 0 then 1 else 0 end end as scr_Has_Debt_History_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_Curr * 1000 end as scr_curr_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_1M * 1000 end as scr_2M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_2M * 1000 end as scr_3M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_3M * 1000 end as scr_4M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_4M * 1000 end as scr_5M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_5M * 1000 end as scr_6M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_6M * 1000 end as scr_7M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_7M * 1000 end as scr_8M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_8M * 1000 end as scr_9M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_9M * 1000 end as scr_10M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_10M * 1000 end as scr_11M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_11M * 1000 end as scr_12M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Ever_long_term_debt_PF * 1000 end as scr_ever_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Max_Long_Term_Debt_PF * 1000 end as scr_max_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Long_Term_Debt_PF_Curr / month_revenue / 12 * 1000 end as scr_long_term_debt_PF_over_revenue,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else (Long_Term_Debt_PF_Curr - Long_Term_Debt_PF_5M) / month_revenue / 12 * 1000 end as scr_var_abs_6M_long_term_debt_PF_over_revenue,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else (Long_Term_Debt_PF_Curr - Long_Term_Debt_PF_11M) / month_revenue / 12 * 1000 end as scr_var_abs_12M_long_term_debt_PF_over_revenue,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 when Long_Term_Debt_PF_Curr = 0 then 0 when Long_Term_Debt_PF_5M = 0 then 2019 else Long_Term_Debt_PF_Curr / Long_Term_Debt_PF_5M - 1 end as scr_var_rel_6M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 when Long_Term_Debt_PF_Curr = 0 then 0 when Long_Term_Debt_PF_11M = 0 then 2019 else Long_Term_Debt_PF_Curr / Long_Term_Debt_PF_11M - 1 end as scr_var_rel_12M_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Qtd_meses_escopo_PF end as scr_number_of_months_in_report_pf,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Meses_Aumento_DividaPF end as scr_Months_Rise_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Meses_Aumento_DividaPF / (case when Qtd_meses_escopo_PF <= 1 then 1 else Qtd_meses_escopo_PF - 1 end) end as scr_Months_Rise_long_term_debt_PF_over_total_months,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Meses_Reducao_DividaPF end as scr_Months_Dive_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Meses_Reducao_DividaPF / (case when Qtd_meses_escopo_PF <= 1 then 1 else Qtd_meses_escopo_PF - 1 end) end as scr_Months_Dive_long_term_debt_PF_over_total_months,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else months_sth_PF end as scr_month_consecutive_dive_long_term_debt_pf,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else months_sth_PF / (case when Qtd_meses_escopo_PF <= 1 then 1 else Qtd_meses_escopo_PF - 1 end) end as scr_month_consecutive_dive_long_term_debt_pf_over_total_months,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else months_hth_PF end as scr_month_consecutive_rise_long_term_debt_pf,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else months_hth_PF / (case when Qtd_meses_escopo_PF <= 1 then 1 else Qtd_meses_escopo_PF - 1 end) end as scr_month_consecutive_rise_long_term_debt_pf_over_total_months,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 when Max_Long_Term_Debt_PF = 0 then 0 else Long_Term_Debt_PF_Curr / Max_Long_Term_Debt_PF end as scr_curr_over_max_long_term_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else case when Long_Term_Debt_PF_Curr = Max_Long_Term_Debt_PF then 1 else 0 end end as scr_All_TimeHigh_long_term_debt_PF,
--Vencido
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Overdue_PF_Curr * 1000 end as scr_curr_overdue_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Max_Overdue_PF * 1000 end as scr_max_overdue_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Months_Overdue_PF end as scr_Months_Overdue_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else (Months_Overdue_PF / Qtd_meses_escopo_PF)::float end as scr_Months_Overdue_PF_over_total_months,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then -1 when Months_Since_Last_Overdue_PF is null then - 1 else Months_Since_Last_Overdue_PF end as scr_Months_since_last_Overdue_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Max_Overdue_PF / month_revenue / 12 * 1000 end as scr_Max_Overdue_PF_over_Revenue,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Overdue_PF_Curr / month_revenue / 12 * 1000 end as scr_Curr_Overdue_PF_over_Revenue,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 when Overdue_PF_Curr = 0 then 0 when Long_Term_Debt_PF_Curr = 0 then 2019 else Overdue_PF_Curr/Long_Term_Debt_PF_Curr end as scr_Curr_Overdue_PF_long_term_debt,
--Prejuizo
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Default_PF_Curr * 1000 end as scr_curr_default_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Max_Default_PF * 1000 end as scr_max_default_debt_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Months_Default_PF end as scr_Months_Default_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else (Months_Default_PF / Qtd_meses_escopo_PF)::float end as scr_Months_Default_PF_over_total_months,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then -1 when Months_Since_Last_Default_PF is null then - 1 else Months_Since_Last_Default_PF end as scr_Months_since_last_Default_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Max_Default_PF / month_revenue / 12 * 1000 end as scr_Max_Default_PF_over_revenue,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Default_PF_Curr / month_revenue / 12 * 1000 end as scr_Curr_Default_PF_over_revenue,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 when Default_PF_Curr = 0 then 0 when Long_Term_Debt_PF_Curr = 0 then 2019 else Default_PF_Curr/Long_Term_Debt_PF_Curr end as scr_Curr_Default_PF_over_long_term_debt,
--Saldo
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 else Saldo_Amort_DividaPF * 1000 end as scr_long_term_debt_amortization_PF,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 when Saldo_Amort_DividaPF = 0 then 0 else Saldo_Amort_DividaPF / Meses_Reducao_DividaPF / month_revenue / 12 * 1000 end as scr_Avg_Amortization_LTDebt_PF_over_revenue,
case when tem_scr_pf = 0 then null when Long_Term_Debt_PF_Curr is null then 0 when Saldo_Amort_DividaPF = 0 then 0 else Saldo_Amort_DividaPF / Meses_Reducao_DividaPF / ((@li.pmt)) * 1000 end as scr_Avg_Amortization_LTDebt_PF_over_loan_PMT,
--Limite
case when tem_scr_pf = 0 then null when Ever_Lim_Cred_PF is null then 0 else Ever_Lim_Cred_PF * 1000 end as scr_ever_credit_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_Curr * 1000 end as scr_curr_credit_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_1M * 1000 end as scr_2M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_2M * 1000 end as scr_3M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_3M * 1000 end as scr_4M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_4M * 1000 end as scr_5M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_5M * 1000 end as scr_6M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_6M * 1000 end as scr_7M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_7M * 1000 end as scr_8M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_8M * 1000 end as scr_9M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_9M * 1000 end as scr_10M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_10M * 1000 end as scr_11M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_11M * 1000 end as scr_12M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_Curr / month_revenue / 12 * 1000 end as scr_Curr_Credit_Limit_PF_over_revenue,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_Curr / case when amount_requested < 1000 then o.max_value else amount_requested end * 1000 end as scr_Curr_Credit_Limit_PF_over_application_value,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Lim_Cred_PF_Curr / li.total_payment * 1000 end as scr_Curr_Credit_Limit_PF_over_loan_value,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 when Lim_Cred_PF_Curr = 0 then 0 when Long_Term_Debt_PF_Curr = 0 then 2019 else Lim_Cred_PF_Curr/Long_Term_Debt_PF_Curr end as scr_Curr_Credit_Limit_PF_over_long_term_debt,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Max_Lim_Cred_PF * 1000 end as scr_max_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 when Max_Lim_Cred_PF = 0 then 0 else Lim_Cred_PF_Curr / Max_Lim_Cred_PF end as scr_curr_over_max_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else case when Lim_Cred_PF_Curr = Max_Lim_Cred_PF then 1 else 0 end end as scr_All_TimeHigh_credit_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else (Lim_Cred_PF_Curr - Long_Term_Debt_PF_5M)/month_revenue/ 12 * 1000 end as scr_var_abs_6M_cred_limit_PF_over_revenue,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else (Lim_Cred_PF_Curr - Long_Term_Debt_PF_11M)/month_revenue / 12 * 1000 end as scr_var_abs_12M_cred_limit_PF_over_revenue,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 when Long_Term_Debt_PF_Curr = 0 then 0 when Long_Term_Debt_PF_5M = 0 then 2019 else Long_Term_Debt_PF_Curr / Long_Term_Debt_PF_5M - 1 end as scr_var_rel_6M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 when Long_Term_Debt_PF_Curr = 0 then 0 when Long_Term_Debt_PF_11M = 0 then 2019 else Long_Term_Debt_PF_Curr / Long_Term_Debt_PF_11M - 1 end as scr_var_rel_12M_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Meses_Aumento_Lim_Cred_PF end as scr_Months_Rise_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Meses_Aumento_Lim_Cred_PF / (case when Qtd_meses_escopo_PF <= 1 then 1 else Qtd_meses_escopo_PF - 1 end) end as scr_Months_Rise_cred_limit_PF_over_total_months,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Meses_Reducao_Lim_Cred_PF end as scr_Months_Dive_cred_limit_PF,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else Meses_Reducao_Lim_Cred_PF / (case when Qtd_meses_escopo_PF <= 1 then 1 else Qtd_meses_escopo_PF - 1 end) end as scr_Months_Dive_cred_limit_PF_over_total_months,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else months_sth_lim_cred_PF end as scr_month_consecutive_dive_cred_limit_debt_pf,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else months_sth_lim_cred_PF / (case when Qtd_meses_escopo_PF <= 1 then 1 else Qtd_meses_escopo_PF - 1 end) end as scr_month_consecutive_dive_cred_limit_pf_over_total_months,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else months_hth_lim_cred_PF end as scr_month_consecutive_rise_cred_limit_debt_pf,
case when tem_scr_pf = 0 then null when Lim_Cred_PF_Curr is null then 0 else months_hth_lim_cred_PF / (case when Qtd_meses_escopo_PF <= 1 then 1 else Qtd_meses_escopo_PF - 1 end) end as scr_month_consecutive_rise_cred_limit_pf_over_total_months,
--Operacoes
case when tem_scr_pf = 0 then null else case when Num_Ops_PF is null then 0 else Num_Ops_PF end end as scr_Num_Fin_Ops_PF,
case when tem_scr_pf = 0 then null else case when Num_FIs_PF is null then 0 else Num_FIs_PF end end as scr_Num_Fin_Inst_PF,
case when tem_scr_pf = 0 then null else case when First_Relation_FI_PF is null then 0 else First_Relation_FI_PF end end as scr_First_Relation_Fin_Inst_PF,
--------------------------SCR DIREITOS CREDITORIOS
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Ever_invoice_disc * 1000 end as scr_ever_invoice_disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_Curr * 1000 end as scr_curr_invoice_disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_1M * 1000 end as scr_2M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_2M * 1000 end as scr_3M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_3M * 1000 end as scr_4M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_4M * 1000 end as scr_5M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_5M * 1000 end as scr_6M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_6M * 1000 end as scr_7M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_7M * 1000 end as scr_8M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_8M * 1000 end as scr_9M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_9M * 1000 end as scr_10M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_10M * 1000 end as scr_11M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_11M * 1000 end as scr_12M_InvoiceDisc,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Invoice_Disc_PJ_Curr / month_revenue / 12 * 1000 end as scr_Curr_InvoiceDisc_PJ_over_revenue,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 when Invoice_Disc_PJ_Curr = 0 then 0 when Long_Term_Debt_PJ_Curr = 0 then 2019 when Invoice_Disc_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 else Invoice_Disc_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_Curr_invoice_disc_debt_over_long_term_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else max_invoice_disc * 1000 end as scr_max_invoice_disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 when max_invoice_disc = 0 then 0 else Invoice_Disc_PJ_Curr / max_invoice_disc end as scr_curr_over_max_invoice_disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else case when Invoice_Disc_PJ_Curr = max_invoice_disc then 1 else 0 end end as scr_All_TimeHigh_invoice_disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else (Invoice_Disc_PJ_Curr - Invoice_Disc_PJ_5M) / month_revenue / 12 * 1000 end as scr_var_abs_6M_invoice_disc_debt_over_revenue,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else (Invoice_Disc_PJ_Curr - Invoice_Disc_PJ_11M) / month_revenue / 12 * 1000 end as scr_var_abs_12M_invoice_disc_debt_over_revenue,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 when Invoice_Disc_PJ_Curr = 0 then 0 when Invoice_Disc_PJ_5M = 0 then 2019 else Invoice_Disc_PJ_Curr / Invoice_Disc_PJ_5M - 1 end as scr_var_rel_6M_invoice_disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 when Invoice_Disc_PJ_Curr = 0 then 0 when Invoice_Disc_PJ_11M = 0 then 2019 else Invoice_Disc_PJ_Curr / Invoice_Disc_PJ_11M - 1 end as scr_var_rel_12M_invoice_disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Meses_Aumento_invoice_disc end as scr_Months_Rise_Invoice_Disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Meses_Aumento_invoice_disc / (case when qtd_meses_invoice_disc <= 1 then 1 else qtd_meses_invoice_disc - 1 end) end as scr_Months_Rise_Invoice_Disc_debt_over_total_months,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Meses_Reducao_invoice_disc end as scr_Months_Dive_Invoice_Disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else Meses_Reducao_invoice_disc / (case when qtd_meses_invoice_disc <= 1 then 1 else qtd_meses_invoice_disc - 1 end) end as scr_Months_Dive_Invoice_Disc_debt_over_total_months,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else months_sth_invoice_disc end as scr_month_consecutive_dive_invoice_disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else months_sth_invoice_disc / (case when qtd_meses_invoice_disc <= 1 then 1 else qtd_meses_invoice_disc - 1 end) end as scr_month_consecutive_dive_invoice_disc_debt_over_total_months,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else months_hth_invoice_disc end as scr_month_consecutive_rise_invoice_disc_debt,
case when tem_scr = 0 then null when Invoice_Disc_PJ_Curr is null then 0 else months_hth_invoice_disc / (case when qtd_meses_invoice_disc <= 1 then 1 else qtd_meses_invoice_disc - 1 end) end as scr_month_consecutive_rise_invoice_disc_debt_over_total_months,
--------------------------SCR EMPRESTIMOS ROTATIVO
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else Ever_overdraft * 1000 end as scr_ever_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_Curr*1000 end as scr_curr_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_1M * 1000 end as scr_2M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_2M * 1000 end as scr_3M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_3M * 1000 end as scr_4M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_4M * 1000 end as scr_5M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_5M * 1000 end as scr_6M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_6M * 1000 end as scr_7M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_7M * 1000 end as scr_8M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_8M * 1000 end as scr_9M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_9M * 1000 end as scr_10M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_10M * 1000 end as scr_11M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_11M * 1000 end as scr_12M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else overdraft_PJ_Curr / month_revenue / 12 * 1000 end as scr_Curr_overdraft_debt_PJ_over_revenue,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 when overdraft_PJ_Curr = 0 then 0 when Long_Term_Debt_PJ_Curr = 0 then 2019 when overdraft_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 else overdraft_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_Curr_overdraft_debt_over_long_term_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 when overdraft_PJ_Curr = 0 then 0 when divida_curto_prazo = 0 then 2019 else overdraft_PJ_Curr/divida_curto_prazo end as scr_Curr_overdraft_debt_over_short_term_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else max_overdraft * 1000 end as scr_max_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 when max_overdraft = 0 then 0 else overdraft_PJ_Curr / max_overdraft end as scr_curr_over_max_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else case when overdraft_PJ_Curr = max_overdraft then 1 else 0 end end as scr_All_TimeHigh_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else (overdraft_PJ_Curr - overdraft_PJ_5M)/month_revenue / 12 * 1000 end as scr_var_abs_6M_overdraft_debt_over_revenue,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else (overdraft_PJ_Curr - overdraft_PJ_11M)/month_revenue / 12 * 1000 end as scr_var_abs_12M_overdraft_debt_over_revenue,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 when overdraft_PJ_Curr  = 0 then 0 when overdraft_PJ_5M = 0 then 2019 else overdraft_PJ_Curr / overdraft_PJ_5M - 1 end as scr_var_rel_6M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 when overdraft_PJ_Curr  = 0 then 0 when overdraft_PJ_11M = 0 then 2019 else overdraft_PJ_Curr / overdraft_PJ_11M - 1 end as scr_var_rel_12M_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else Meses_Aumento_overdraft end as scr_Months_Rise_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else Meses_Aumento_overdraft / (case when qtd_meses_overdraft <= 1 then 1 else qtd_meses_overdraft-1 end) end as scr_Months_Rise_overdraft_debt_over_total_months,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else Meses_Reducao_overdraft end as scr_Months_Dive_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else Meses_Reducao_overdraft / (case when qtd_meses_overdraft <= 1 then 1 else qtd_meses_overdraft-1 end) end as scr_Months_Dive_overdraft_debt_over_total_months,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else months_sth_overdraft end as scr_month_consecutive_dive_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else months_sth_overdraft / (case when qtd_meses_overdraft <= 1 then 1 else qtd_meses_overdraft-1 end) end as scr_month_consecutive_dive_overdraft_debt_over_total_months,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else months_hth_overdraft end as scr_month_consecutive_rise_overdraft_debt,
case when tem_scr = 0 then null when overdraft_PJ_Curr is null then 0 else months_hth_overdraft / (case when qtd_meses_overdraft <= 1 then 1 else qtd_meses_overdraft-1 end) end as scr_month_consecutive_rise_overdraft_debt_over_total_months,
--------------------------SCR PJ DIVIDA CONCORRENTE
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else divida_curto_prazo end as scr_short_term_debt_PJ,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else Ever_competitive_debt * 1000 end as scr_ever_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_Curr * 1000 end as scr_curr_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_1M * 1000 end as scr_2M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_2M * 1000 end as scr_3M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_3M * 1000 end as scr_4M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_4M * 1000 end as scr_5M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_5M * 1000 end as scr_6M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_6M * 1000 end as scr_7M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_7M * 1000 end as scr_8M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_8M * 1000 end as scr_9M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_9M * 1000 end as scr_10M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_10M * 1000 end as scr_11M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_11M * 1000 end as scr_12M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else competitive_debt_PJ_Curr / month_revenue / 12 * 1000 end as scr_Curr_competitive_debt_PJ_over_revenue,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 when competitive_debt_PJ_Curr = 0 then 0 when Long_Term_Debt_PJ_Curr = 0 then 2019 when competitive_debt_PJ_Curr > Long_Term_Debt_PJ_Curr then 1 else competitive_debt_PJ_Curr/Long_Term_Debt_PJ_Curr end as scr_Curr_competitive_debt_over_long_term_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 when competitive_debt_PJ_Curr = 0 then 0 when divida_curto_prazo = 0 then 2019 else competitive_debt_PJ_Curr/divida_curto_prazo end as scr_Curr_competitive_debt_over_short_term_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else max_competitive_debt * 1000 end as scr_max_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 when max_competitive_debt = 0 then 0 else competitive_debt_PJ_Curr / max_competitive_debt end as scr_curr_over_max_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else case when competitive_debt_PJ_Curr = max_competitive_debt then 1 else 0 end end as scr_All_TimeHigh_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else (competitive_debt_PJ_Curr - competitive_debt_PJ_5M) / month_revenue / 12 * 1000 end as scr_var_abs_6M_competitive_debt_over_revenue,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else (competitive_debt_PJ_Curr - competitive_debt_PJ_11M) / month_revenue / 12 * 1000 end as scr_var_abs_12M_competitive_debt_over_revenue,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 when competitive_debt_PJ_Curr = 0 then 0 when competitive_debt_PJ_5M = 0 then 2019 else competitive_debt_PJ_Curr / competitive_debt_PJ_5M - 1 end as scr_var_rel_6M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 when competitive_debt_PJ_Curr = 0 then 0 when competitive_debt_PJ_11M = 0 then 2019 else competitive_debt_PJ_Curr / competitive_debt_PJ_11M - 1 end as scr_var_rel_12M_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else Meses_Aumento_competitive_debt end as scr_Months_Rise_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else Meses_Aumento_competitive_debt / (case when qtd_meses_competitive_debt <= 1 then 1 else qtd_meses_competitive_debt-1 end) end as scr_Months_Rise_competitive_debt_over_total_months,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else Meses_Reducao_competitive_debt end as scr_Months_Dive_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else Meses_Reducao_competitive_debt / (case when qtd_meses_competitive_debt <= 1 then 1 else qtd_meses_competitive_debt-1 end) end as scr_Months_Dive_competitive_debt_over_total_months,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else months_sth_competitive_debt end as scr_month_consecutive_dive_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else months_sth_competitive_debt / (case when qtd_meses_competitive_debt <= 1 then 1 else qtd_meses_competitive_debt-1 end) end as scr_month_consecutive_dive_competitive_debt_over_total_months,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else months_hth_competitive_debt end as scr_month_consecutive_rise_competitive_debt,
case when tem_scr = 0 then null when competitive_debt_PJ_Curr is null then 0 else months_hth_competitive_debt / (case when qtd_meses_competitive_debt <= 1 then 1 else qtd_meses_competitive_debt-1 end) end as scr_month_consecutive_rise_competitive_debt_over_total_months,
--------------------------SCR PJ DIVIDA CONCORRENTE PF
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else divida_curto_prazo_pf end as scr_short_term_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else Ever_competitive_debt_pf * 1000 end as scr_ever_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_Curr * 1000 end as scr_curr_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_1M * 1000 end as scr_2M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_2M * 1000 end as scr_3M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_3M * 1000 end as scr_4M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_4M * 1000 end as scr_5M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_5M * 1000 end as scr_6M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_6M * 1000 end as scr_7M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_7M * 1000 end as scr_8M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_8M * 1000 end as scr_9M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_9M * 1000 end as scr_10M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_10M * 1000 end as scr_11M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_11M * 1000 end as scr_12M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else competitive_debt_pf_Curr/month_revenue/12*1000 end as scr_Curr_competitive_debt_pf_over_revenue,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 when competitive_debt_pf_Curr = 0 then 0 when Long_Term_Debt_pf_Curr = 0 then 2019 when competitive_debt_pf_Curr > Long_Term_Debt_pf_Curr then 1 else competitive_debt_pf_Curr/Long_Term_Debt_pf_Curr end as scr_Curr_competitive_debt_over_long_term_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 when competitive_debt_pf_Curr = 0 then 0 when divida_curto_prazo_pf = 0 then 2019 else competitive_debt_pf_Curr/divida_curto_prazo_pf end as scr_Curr_competitive_debt_over_short_term_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else max_competitive_debt_pf * 1000 end as scr_max_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 when max_competitive_debt_pf = 0 then 0 else competitive_debt_pf_Curr / max_competitive_debt_pf end as scr_curr_over_max_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else case when competitive_debt_pf_Curr = max_competitive_debt_pf then 1 else 0 end end as scr_All_TimeHigh_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else (competitive_debt_pf_Curr - competitive_debt_pf_5M)/month_revenue / 12 * 1000 end as scr_var_abs_6M_competitive_debt_over_revenue_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else (competitive_debt_pf_Curr - competitive_debt_pf_11M)/month_revenue / 12 * 1000 end as scr_var_abs_12M_competitive_debt_over_revenue_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 when competitive_debt_pf_Curr = 0 then 0 when competitive_debt_pf_5M = 0 then 2019 else competitive_debt_pf_Curr / competitive_debt_pf_5M - 1 end as scr_var_rel_6M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 when competitive_debt_pf_Curr = 0 then 0 when competitive_debt_pf_11M = 0 then 2019 else competitive_debt_pf_Curr / competitive_debt_pf_11M - 1 end as scr_var_rel_12M_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else Meses_Aumento_competitive_debt_pf end as scr_Months_Rise_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else Meses_Aumento_competitive_debt_pf / (case when qtd_meses_competitive_debt_pf <= 1 then 1 else qtd_meses_competitive_debt_pf-1 end) end as scr_Months_Rise_competitive_debt_over_total_months_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else Meses_Reducao_competitive_debt_pf end as scr_Months_Dive_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else Meses_Reducao_competitive_debt_pf / (case when qtd_meses_competitive_debt_pf <= 1 then 1 else qtd_meses_competitive_debt_pf-1 end) end as scr_Months_Dive_competitive_debt_over_total_months_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else months_sth_competitive_debt_pf end as scr_month_consecutive_dive_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else months_sth_competitive_debt_pf / (case when qtd_meses_competitive_debt_pf <= 1 then 1 else qtd_meses_competitive_debt_pf-1 end) end as scr_month_consecutive_dive_competitive_debt_over_total_months_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else months_hth_competitive_debt_pf end as scr_month_consecutive_rise_competitive_debt_pf,
case when tem_scr_pf = 0 then null when competitive_debt_pf_Curr is null then 0 else months_hth_competitive_debt_pf / (case when qtd_meses_competitive_debt_pf <= 1 then 1 else qtd_meses_competitive_debt_pf-1 end) end as scr_month_consecutive_rise_competitive_debt_over_total_months_pf,
case when tem_scr_pf = 0 then null when vehicle_debt_pf_Curr is null then 0 else vehicle_debt_pf_Curr end as scr_Curr_vehicle_debt_pf,
case when tem_scr_pf = 0 then null when vehicle_debt_pf_Curr is null then 0 else vehicle_debt_pf_1M end as scr_2M_vehicle_debt_pf,
case when tem_scr_pf = 0 then null when vehicle_debt_pf_Curr is null then 0 else vehicle_debt_pf_2m end as scr_3M_vehicle_debt_pf,
case when tem_scr_pf = 0 then null when vehicle_debt_pf_Curr is null then 0 else vehicle_debt_pf_3m end as scr_4M_vehicle_debt_pf,
case when tem_scr_pf = 0 then null when vehicle_debt_pf_Curr is null then 0 else vehicle_debt_pf_4m end as scr_5M_vehicle_debt_pf,
case when tem_scr_pf = 0 then null when vehicle_debt_pf_Curr is null then 0 else vehicle_debt_pf_5m end as scr_6M_vehicle_debt_pf,
case when tem_scr_pf = 0 then null when vehicle_debt_pf_Curr is null then 0 else (vehicle_debt_pf_Curr - vehicle_debt_pf_2M)/month_revenue / 12 * 1000 end as scr_var_abs_3M_vehicle_debt_over_revenue_pf,
case when tem_scr_pf = 0 then null when vehicle_debt_pf_Curr is null then 0 else (vehicle_debt_pf_Curr - vehicle_debt_pf_5M)/month_revenue / 12 * 1000 end as scr_var_abs_6M_vehicle_debt_over_revenue_pf,
case when tem_scr_pf = 0 then null when vehicle_debt_pf_Curr is null then 0 when vehicle_debt_pf_Curr = 0 then 0 when vehicle_debt_pf_2M = 0 then 2019 else vehicle_debt_pf_Curr / vehicle_debt_pf_2M - 1 end as scr_var_rel_3M_vehicle_debt_pf,
case when tem_scr_pf = 0 then null when vehicle_debt_pf_Curr is null then 0 when vehicle_debt_pf_Curr = 0 then 0 when vehicle_debt_pf_5M = 0 then 2019 else vehicle_debt_pf_Curr / vehicle_debt_pf_5M - 1 end as scr_var_rel_6M_vehicle_debt_pf,
--------------------------SERASA
is_shareholder as experian_is_shareholder,
count_socios as experian_count_shareholders,
case when tem_serasa = 0 then null when empresas_problema is null then 0 else case when empresas_problema is null then -1 else empresas_problema end end as experian_derogatory_marked_related_companies,
case when tem_serasa = 0 then null when empresa_divida_valor is null then 0 else empresa_divida_valor end as experian_company_overdue_debt_value,
case when tem_serasa = 0 then null when empresa_divida_unit is null then 0 else empresa_divida_unit end as experian_company_overdue_debt_unit,
case when tem_serasa = 0 then null when empresa_ccf_valor is null then 0 else empresa_ccf_valor end as experian_company_bad_check_value,
case when tem_serasa = 0 then null when empresa_ccf_unit is null then 0 else empresa_ccf_unit end as experian_company_bad_check_unit,
case when tem_serasa = 0 then null when empresa_protesto_valor is null then 0 else empresa_protesto_valor end as experian_company_notary_registry_protests_value,
case when tem_serasa = 0 then null when empresa_protesto_unit is null then 0 else empresa_protesto_unit end as experian_company_notary_registry_protests_unit,
case when tem_serasa = 0 then null when empresa_acao_valor is null then 0 else empresa_acao_valor end as experian_company_legal_action_value,
case when tem_serasa = 0 then null when empresa_acao_unit is null then 0 else empresa_acao_unit end as experian_company_legal_action_unit,
case when tem_serasa = 0 then null when empresa_pefin_valor is null then 0 else empresa_pefin_valor end as experian_company_negative_mark_by_non_fin_companies_value,
case when tem_serasa = 0 then null when empresa_pefin_unit is null then 0 else empresa_pefin_unit end as experian_company_negative_mark_by_non_fin_companies_unit,
case when tem_serasa = 0 then null when empresa_refin_valor is null then 0 else empresa_refin_valor end as experian_company_negative_mark_by_fin_companies_value,
case when tem_serasa = 0 then null when empresa_refin_unit is null then 0 else empresa_refin_unit end as experian_company_negative_mark_by_fin_companies_unit,
case when tem_serasa = 0 then null when empresa_spc_valor is null then 0 else empresa_spc_valor end as experian_company_bureau_derogatory_value,
case when tem_serasa = 0 then null when empresa_spc_unit is null then 0 else empresa_spc_unit end as experian_company_bureau_derogatory_unit,
case when tem_serasa = 0 then null when empresa_total_valor is null then 0 else empresa_total_valor end as experian_company_derogatory_mark_total_value,
case when tem_serasa = 0 then null when empresa_total_unit is null then 0 else empresa_total_unit end as experian_company_derogatory_mark_total_unit,
case when tem_serasa = 0 then null when socio_divida_valor is null then 0 else socio_divida_valor end as experian_sh_overdue_debt_value,
case when tem_serasa = 0 then null when socio_divida_unit is null then 0 else socio_divida_unit end as experian_sh_overdue_debt_unit,
case when tem_serasa = 0 then null when socio_ccf_valor is null then 0 else socio_ccf_valor end as experian_sh_bad_check_value,
case when tem_serasa = 0 then null when socio_ccf_unit is null then 0 else socio_ccf_unit end as experian_sh_bad_check_unit,
case when tem_serasa = 0 then null when socio_protesto_valor is null then 0 else socio_protesto_valor end as experian_sh_notary_registry_protests_value,
case when tem_serasa = 0 then null when socio_protesto_unit is null then 0 else socio_protesto_unit end as experian_sh_notary_registry_protests_unit,
case when tem_serasa = 0 then null when socio_acao_valor is null then 0 else socio_acao_valor end as experian_sh_legal_action_value,
case when tem_serasa = 0 then null when socio_acao_unit is null then 0 else socio_acao_unit end as experian_sh_legal_action_unit,
case when tem_serasa = 0 then null when socio_pefin_valor is null then 0 else socio_pefin_valor end as experian_sh_negative_mark_by_non_fin_companies_value,
case when tem_serasa = 0 then null when socio_pefin_unit is null then 0 else socio_pefin_unit end as experian_sh_negative_mark_by_non_fin_companies_unit,
case when tem_serasa = 0 then null when socio_refin_valor is null then 0 else socio_refin_valor end as experian_sh_negative_mark_by_fin_companies_value,
case when tem_serasa = 0 then null when socio_refin_unit is null then 0 else socio_refin_unit end as experian_sh_negative_mark_by_fin_companies_unit,
case when tem_serasa = 0 then null when socio_spc_valor is null then 0 else socio_spc_valor end as experian_sh_bureau_derogatory_value,
case when tem_serasa = 0 then null when socio_spc_unit is null then 0 else socio_spc_unit end as experian_sh_bureau_derogatory_unit,
case when tem_serasa = 0 then null when socios_total_valor is null then 0 else socios_total_valor end as experian_sh_derogatory_mark_total_value,
case when tem_serasa = 0 then null when socios_total_unit is null then 0 else socios_total_unit end as experian_sh_derogatory_mark_total_unit,
case when tem_serasa = 0 then null when socio_major_divida_valor is null then 0 else socio_major_divida_valor end as experian_major_sh_overdue_debt_value,
case when tem_serasa = 0 then null when socio_major_divida_unit is null then 0 else socio_major_divida_unit end as experian_major_sh_overdue_debt_unit,
case when tem_serasa = 0 then null when socio_major_ccf_valor is null then 0 else socio_major_ccf_valor end as experian_major_sh_bad_check_value,
case when tem_serasa = 0 then null when socio_major_ccf_unit is null then 0 else socio_major_ccf_unit end as experian_major_sh_bad_check_unit,
case when tem_serasa = 0 then null when socio_major_protesto_valor is null then 0 else socio_major_protesto_valor end as experian_major_sh_notary_registry_protests_value,
case when tem_serasa = 0 then null when socio_major_protesto_unit is null then 0 else socio_major_protesto_unit end as experian_major_sh_notary_registry_protests_unit,
case when tem_serasa = 0 then null when socio_major_acao_valor is null then 0 else socio_major_acao_valor end as experian_major_sh_legal_action_value,
case when tem_serasa = 0 then null when socio_major_acao_unit is null then 0 else socio_major_acao_unit end as experian_major_sh_legal_action_unit,
case when tem_serasa = 0 then null when socio_major_pefin_valor is null then 0 else socio_major_pefin_valor end as experian_major_sh_negative_mark_by_non_fin_companies_value,
case when tem_serasa = 0 then null when socio_major_pefin_unit is null then 0 else socio_major_pefin_unit end as experian_major_sh_negative_mark_by_non_fin_companies_unit,
case when tem_serasa = 0 then null when socio_major_refin_valor is null then 0 else socio_major_refin_valor end as experian_major_sh_negative_mark_by_fin_companies_value,
case when tem_serasa = 0 then null when socio_major_refin_unit is null then 0 else socio_major_refin_unit end as experian_major_sh_negative_mark_by_fin_companies_unit,
case when tem_serasa = 0 then null when socio_major_spc_valor is null then 0 else socio_major_spc_valor end as experian_major_sh_bureau_derogatory_value,
case when tem_serasa = 0 then null when socio_major_spc_unit is null then 0 else socio_major_spc_unit end as experian_major_sh_bureau_derogatory_unit,
case when tem_serasa = 0 then null when socios_major_total_valor is null then 0 else socios_major_total_valor end as experian_major_sh_derogatory_mark_total_value,
case when tem_serasa = 0 then null when socios_major_total_unit is null then 0 else socios_major_total_unit end as experian_major_sh_derogatory_mark_total_unit,
case when tem_serasa = 0 then null when all_valor is null then 0 else all_valor end as "experian_company+shareholders_derogatory_mark_total_value",
case when tem_serasa = 0 then null when all_unit is null then 0 else all_unit end as "experian_company+shareholders_derogatory_mark_total_unit",
case when tem_serasa = 0 then null else case when consulta_factoring_serasa + consulta_factoring_spc > 0 then 1 else 0 end end experian_report_recently_taken_by_factoring,
case when tem_serasa = 0 or consultas_atual is null then null else consultas_atual end as experian_inquiries_curr, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_1m end as experian_inquiries_2m, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_2m end as experian_inquiries_3m, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_3m end as experian_inquiries_4m, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_4m end as experian_inquiries_5m, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_5m end as experian_inquiries_6m, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_6m end as experian_inquiries_7m, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_7m end as experian_inquiries_8m, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_8m end as experian_inquiries_9m, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_9m end as experian_inquiries_10m, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_10m end as experian_inquiries_11m, 
case when tem_serasa = 0 or consultas_atual is null then null else consultas_11m end as experian_inquiries_12m, 
--------------------------SIGNERS
MajorSigner_Age as signers_MajorShareholder_Age, 
MajorSigner_Revenue as signers_MajorShareholder_Revenue,
MajorSigner_civil_status as signers_MajorShareholder_Maritial_Status,
MajorSigner_gender as signers_MajorShareholdergender,
SolicSignersCPF_Age as signers_RequesterSigner_Age,
SolicSignersCPF_Revenue as signers_RequesterSigner_Revenue,
SolicSignerCPF_civil_status as signers_RequesterSigner_Maritial_Status,
SolicSignerCPF_gender as signers_RequesterSigner_gender,
OldestSh_Age as signers_OldestSh_Age,
OldestSh_Revenue as signers_OldestSh_Revenue,
OldestSh_civil_status as signers_OldestSh_Maritial_status,
OldestSh_gender as signers_OldestSh_gender,
AdminSigner_Age as signers_AdminSigner_Age,
AdminSigner_Revenue as signers_AdminSigner_Revenue,
AdminSigner_civil_status as signers_AdminSigner_Maritial_status,
AdminSigner_gender as signers_AdminSigner_gender,
avg_age_partners as signers_avg_age_partners,
avg_revenue_partners as signers_avg_revenue_partners,
male_partners as signers_male_shareholders,
female_partners as signers_female_shareholders,
--------------------------CHECKLIST
social_contract_confirmed as checklist_social_contract_confirmed,
company_address_confirmed as checklist_company_address_confirmed,
bank_statements_confirmed as checklist_bank_statements_confirmed,
company_account_confirmed as checklist_company_account_confirmed,
personal_rg as checklist_shareholder_rg,
personal_cpf as checklist_shareholder_cpf,
proof_personal_address as checklist_personal_address_confirmed,
proof_income as checklist_shareholder_income_confirmed,
spouse_info as checklist_shareholder_spouse_info,
proved_informed_revenue as checklist_proved_informed_revenue,
positive_balance_account as checklist_positive_balance_account,
low_negative_balance as checklist_low_negative_balance,
largest_cashflow_company_account as checklist_largest_cashflow_company_account,
continuous_income as checklist_continuous_income,
income_unrelated_government as checklist_income_unrelated_government,
diversified_clients as checklist_diversified_clients,
only_company_expenses as checklist_only_company_expenses,
different_personal_company_address as checklist_different_personal_company_address,
dilligent_credit_simulation as checklist_dilligent_credit_simulation,
requested_lessthan_offered as checklist_requested_lessthan_offered,
company_usedto_credit as checklist_company_usedto_credit,
partner_has_assets as checklist_partner_has_assets,
low_personal_debt as checklist_low_personal_debt,
percent_documentation as checklist_percent_documentation,
score_documentation as checklist_score,
--------------------------TAGS
app_auto as tags_approved_automatically,
offer_auto as tags_offered_automatically,
interest_rate_test1 as tags_interest_rate_test_scope
--------------------------
FROM direct_prospects dp
join clients c on c.client_id = dp.client_id
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join (select o.* ,count_neg from offers o join (select direct_prospect_id,min(offer_id) as min_id,count(*) as count_neg from offers group by 1) as initial_off on initial_off.min_id = o.offer_id) as initialoff on initialoff.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
join bank_accounts ba on ba.bank_account_id = lr.bank_account_id
left join populacao p on p.municipio = dp.city and p.uf = dp.state
--------------------------TABLE TEM SCR, SCR PF E SERASA
left join (select direct_prospect_id, 
		case when max_scr is null and min_scr is null then 0 else 1 end as tem_scr, 
		case when max_scr_pf is null and min_scr_pf is null then 0 else 1 end as tem_scr_pf, 
		case when max_serasa is null then 0 else 1 end as tem_serasa 
		from (select dp.direct_prospect_id,
				max(id) filter (where to_date(left(cc.data->>'criado_em',10),'yyyy-mm-dd') <= lr.loan_date and (cc.data->'historico' is not null or cc.data->'erro' is not null) and tipo = 'SCR' and documento_tipo = 'CNPJ') as max_scr, 
				min(id) filter (where (cc.data->'historico' is not null or cc.data->'erro' is not null) and tipo = 'SCR' and documento_tipo = 'CNPJ') as min_scr,
				max(id) filter (where to_date(left(cc.data->>'criado_em',10),'yyyy-mm-dd') <= lr.loan_date and (cc.data->'historico' is not null or cc.data->'erro' is not null) and tipo = 'SCR' and documento_tipo = 'CPF') as max_scr_pf, 
				min(id) filter (where (cc.data->'historico' is not null or cc.data->'erro' is not null) and tipo = 'SCR' and documento_tipo = 'CPF') as min_scr_pf,
				max(id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date and tipo = 'Serasa') as max_serasa
			from credito_coleta cc 
			join direct_prospects dp on case when cc.documento_tipo = 'CPF' then dp.cpf = cc.documento when cc.documento_tipo = 'CNPJ' then left(dp.cnpj,8) = left(cc.documento,8) end
			join offers o on o.direct_prospect_id = dp.direct_prospect_id 
			join loan_requests lr on lr.offer_id = o.offer_id 
			where lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18') and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18') group by 1) as t1) as ValidSCRSerasa on ValidSCRSerasa.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SCR PJ
left join(select direct_prospect_id,
	Num_Ops_PJ::float,
	Num_FIs_PJ::float,
	First_Relation_FI_PJ::float,
	Long_Term_Debt_PJ_Curr::float,
	Long_Term_Debt_PJ_1M::float,
	Long_Term_Debt_PJ_2M::float,
	Long_Term_Debt_PJ_3M::float,
	Long_Term_Debt_PJ_4M::float,
	Long_Term_Debt_PJ_5M::float,
	Long_Term_Debt_PJ_6M::float,
	Long_Term_Debt_PJ_7M::float,
	Long_Term_Debt_PJ_8M::float,
	Long_Term_Debt_PJ_9M::float,
	Long_Term_Debt_PJ_10M::float,
	Long_Term_Debt_PJ_11M::float,
	Ever_long_term_debt_PJ::float,
	Max_Long_Term_Debt_PJ::float,
	Overdue_PJ_Curr::float,
	Months_Since_Last_Overdue_PJ::float,
	Months_Overdue_PJ::float,
	Max_Overdue_PJ::float,
	Default_PJ_Curr::float,
	Months_Since_Last_Default_PJ::float,
	Months_Default_PJ::float,
	Max_Default_PJ::float,
	Qtd_meses_escopo_pj::float,
	case when Long_Term_Debt_PJ_22M > Long_Term_Debt_PJ_23M then 1 else 0 end +
	case when Long_Term_Debt_PJ_21M > Long_Term_Debt_PJ_22M then 1 else 0 end +
	case when Long_Term_Debt_PJ_20M > Long_Term_Debt_PJ_21M then 1 else 0 end +
	case when Long_Term_Debt_PJ_19M > Long_Term_Debt_PJ_20M then 1 else 0 end +
	case when Long_Term_Debt_PJ_18M > Long_Term_Debt_PJ_19M then 1 else 0 end +
	case when Long_Term_Debt_PJ_17M > Long_Term_Debt_PJ_18M then 1 else 0 end +
	case when Long_Term_Debt_PJ_16M > Long_Term_Debt_PJ_17M then 1 else 0 end +
	case when Long_Term_Debt_PJ_15M > Long_Term_Debt_PJ_16M then 1 else 0 end +
	case when Long_Term_Debt_PJ_14M > Long_Term_Debt_PJ_15M then 1 else 0 end +
	case when Long_Term_Debt_PJ_13M > Long_Term_Debt_PJ_14M then 1 else 0 end +
	case when Long_Term_Debt_PJ_12M > Long_Term_Debt_PJ_13M then 1 else 0 end +
	case when Long_Term_Debt_PJ_11M > Long_Term_Debt_PJ_12M then 1 else 0 end +
	case when Long_Term_Debt_PJ_10M > Long_Term_Debt_PJ_11M then 1 else 0 end +
	case when Long_Term_Debt_PJ_9M > Long_Term_Debt_PJ_10M then 1 else 0 end +
	case when Long_Term_Debt_PJ_8M > Long_Term_Debt_PJ_9M then 1 else 0 end +
	case when Long_Term_Debt_PJ_7M > Long_Term_Debt_PJ_8M then 1 else 0 end +
	case when Long_Term_Debt_PJ_6M > Long_Term_Debt_PJ_7M then 1 else 0 end +
	case when Long_Term_Debt_PJ_5M > Long_Term_Debt_PJ_6M then 1 else 0 end +
	case when Long_Term_Debt_PJ_4M > Long_Term_Debt_PJ_5M then 1 else 0 end +
	case when Long_Term_Debt_PJ_3M > Long_Term_Debt_PJ_4M then 1 else 0 end +
	case when Long_Term_Debt_PJ_2M > Long_Term_Debt_PJ_3M then 1 else 0 end +
	case when Long_Term_Debt_PJ_1M > Long_Term_Debt_PJ_2M then 1 else 0 end +
	case when Long_Term_Debt_PJ_Curr > Long_Term_Debt_PJ_1M then 1 else 0 end::float as Meses_Aumento_DividaPJ,
	case when Long_Term_Debt_PJ_22M < Long_Term_Debt_PJ_23M then 1 else 0 end +
	case when Long_Term_Debt_PJ_21M < Long_Term_Debt_PJ_22M then 1 else 0 end +
	case when Long_Term_Debt_PJ_20M < Long_Term_Debt_PJ_21M then 1 else 0 end +
	case when Long_Term_Debt_PJ_19M < Long_Term_Debt_PJ_20M then 1 else 0 end +
	case when Long_Term_Debt_PJ_18M < Long_Term_Debt_PJ_19M then 1 else 0 end +
	case when Long_Term_Debt_PJ_17M < Long_Term_Debt_PJ_18M then 1 else 0 end +
	case when Long_Term_Debt_PJ_16M < Long_Term_Debt_PJ_17M then 1 else 0 end +
	case when Long_Term_Debt_PJ_15M < Long_Term_Debt_PJ_16M then 1 else 0 end +
	case when Long_Term_Debt_PJ_14M < Long_Term_Debt_PJ_15M then 1 else 0 end +
	case when Long_Term_Debt_PJ_13M < Long_Term_Debt_PJ_14M then 1 else 0 end +
	case when Long_Term_Debt_PJ_12M < Long_Term_Debt_PJ_13M then 1 else 0 end +
	case when Long_Term_Debt_PJ_11M < Long_Term_Debt_PJ_12M then 1 else 0 end +
	case when Long_Term_Debt_PJ_10M < Long_Term_Debt_PJ_11M then 1 else 0 end +
	case when Long_Term_Debt_PJ_9M < Long_Term_Debt_PJ_10M then 1 else 0 end +
	case when Long_Term_Debt_PJ_8M < Long_Term_Debt_PJ_9M then 1 else 0 end +
	case when Long_Term_Debt_PJ_7M < Long_Term_Debt_PJ_8M then 1 else 0 end +
	case when Long_Term_Debt_PJ_6M < Long_Term_Debt_PJ_7M then 1 else 0 end +
	case when Long_Term_Debt_PJ_5M < Long_Term_Debt_PJ_6M then 1 else 0 end +
	case when Long_Term_Debt_PJ_4M < Long_Term_Debt_PJ_5M then 1 else 0 end +
	case when Long_Term_Debt_PJ_3M < Long_Term_Debt_PJ_4M then 1 else 0 end +
	case when Long_Term_Debt_PJ_2M < Long_Term_Debt_PJ_3M then 1 else 0 end +
	case when Long_Term_Debt_PJ_1M < Long_Term_Debt_PJ_2M then 1 else 0 end +
	case when Long_Term_Debt_PJ_Curr < Long_Term_Debt_PJ_1M then 1 else 0 end::float as Meses_Reducao_DividaPJ,
	(case when Long_Term_Debt_PJ_22M < Long_Term_Debt_PJ_23M then Long_Term_Debt_PJ_22M - Long_Term_Debt_PJ_23M else 0 end +
	case when Long_Term_Debt_PJ_21M < Long_Term_Debt_PJ_22M then Long_Term_Debt_PJ_21M - Long_Term_Debt_PJ_22M else 0 end +
	case when Long_Term_Debt_PJ_20M < Long_Term_Debt_PJ_21M then Long_Term_Debt_PJ_20M - Long_Term_Debt_PJ_21M else 0 end +
	case when Long_Term_Debt_PJ_19M < Long_Term_Debt_PJ_20M then Long_Term_Debt_PJ_19M - Long_Term_Debt_PJ_20M else 0 end +
	case when Long_Term_Debt_PJ_18M < Long_Term_Debt_PJ_19M then Long_Term_Debt_PJ_18M - Long_Term_Debt_PJ_19M else 0 end +
	case when Long_Term_Debt_PJ_17M < Long_Term_Debt_PJ_18M then Long_Term_Debt_PJ_17M - Long_Term_Debt_PJ_18M else 0 end +
	case when Long_Term_Debt_PJ_16M < Long_Term_Debt_PJ_17M then Long_Term_Debt_PJ_16M - Long_Term_Debt_PJ_17M else 0 end +
	case when Long_Term_Debt_PJ_15M < Long_Term_Debt_PJ_16M then Long_Term_Debt_PJ_15M - Long_Term_Debt_PJ_16M else 0 end +
	case when Long_Term_Debt_PJ_14M < Long_Term_Debt_PJ_15M then Long_Term_Debt_PJ_14M - Long_Term_Debt_PJ_15M else 0 end +
	case when Long_Term_Debt_PJ_13M < Long_Term_Debt_PJ_14M then Long_Term_Debt_PJ_13M - Long_Term_Debt_PJ_14M else 0 end +
	case when Long_Term_Debt_PJ_12M < Long_Term_Debt_PJ_13M then Long_Term_Debt_PJ_12M - Long_Term_Debt_PJ_13M else 0 end +
	case when Long_Term_Debt_PJ_11M < Long_Term_Debt_PJ_12M then Long_Term_Debt_PJ_11M - Long_Term_Debt_PJ_12M else 0 end +
	case when Long_Term_Debt_PJ_10M < Long_Term_Debt_PJ_11M then Long_Term_Debt_PJ_10M - Long_Term_Debt_PJ_11M else 0 end +
	case when Long_Term_Debt_PJ_9M < Long_Term_Debt_PJ_10M then Long_Term_Debt_PJ_9M - Long_Term_Debt_PJ_10M else 0 end +
	case when Long_Term_Debt_PJ_8M < Long_Term_Debt_PJ_9M then Long_Term_Debt_PJ_8M - Long_Term_Debt_PJ_9M else 0 end +
	case when Long_Term_Debt_PJ_7M < Long_Term_Debt_PJ_8M then Long_Term_Debt_PJ_7M - Long_Term_Debt_PJ_8M else 0 end +
	case when Long_Term_Debt_PJ_6M < Long_Term_Debt_PJ_7M then Long_Term_Debt_PJ_6M - Long_Term_Debt_PJ_7M else 0 end +
	case when Long_Term_Debt_PJ_5M < Long_Term_Debt_PJ_6M then Long_Term_Debt_PJ_5M - Long_Term_Debt_PJ_6M else 0 end +
	case when Long_Term_Debt_PJ_4M < Long_Term_Debt_PJ_5M then Long_Term_Debt_PJ_4M - Long_Term_Debt_PJ_5M else 0 end +
	case when Long_Term_Debt_PJ_3M < Long_Term_Debt_PJ_4M then Long_Term_Debt_PJ_3M - Long_Term_Debt_PJ_4M else 0 end +
	case when Long_Term_Debt_PJ_2M < Long_Term_Debt_PJ_3M then Long_Term_Debt_PJ_2M - Long_Term_Debt_PJ_3M else 0 end +
	case when Long_Term_Debt_PJ_1M < Long_Term_Debt_PJ_2M then Long_Term_Debt_PJ_1M - Long_Term_Debt_PJ_2M else 0 end +
	case when Long_Term_Debt_PJ_Curr < Long_Term_Debt_PJ_1M then Long_Term_Debt_PJ_Curr - Long_Term_Debt_PJ_1M else 0 end)*(-1)::float as Saldo_Amort_DividaPJ,
	case when Long_Term_Debt_PJ_1M is null then null else
	case when Long_Term_Debt_PJ_Curr >= Long_Term_Debt_PJ_1M then 0 else
	case when Long_Term_Debt_PJ_1M >= Long_Term_Debt_PJ_2M or Long_Term_Debt_PJ_2M is null then 1 else
	case when Long_Term_Debt_PJ_2M >= Long_Term_Debt_PJ_3M or Long_Term_Debt_PJ_3M is null then 2 else
	case when Long_Term_Debt_PJ_3M >= Long_Term_Debt_PJ_4M or Long_Term_Debt_PJ_4M is null then 3 else
	case when Long_Term_Debt_PJ_4M >= Long_Term_Debt_PJ_5M or Long_Term_Debt_PJ_5M is null then 4 else
	case when Long_Term_Debt_PJ_5M >= Long_Term_Debt_PJ_6M or Long_Term_Debt_PJ_6M is null then 5 else
	case when Long_Term_Debt_PJ_6M >= Long_Term_Debt_PJ_7M or Long_Term_Debt_PJ_7M is null then 6 else
	case when Long_Term_Debt_PJ_7M >= Long_Term_Debt_PJ_8M or Long_Term_Debt_PJ_8M is null then 7 else
	case when Long_Term_Debt_PJ_8M >= Long_Term_Debt_PJ_9M or Long_Term_Debt_PJ_9M is null then 8 else
	case when Long_Term_Debt_PJ_9M >= Long_Term_Debt_PJ_10M or Long_Term_Debt_PJ_10M is null then 9 else
	case when Long_Term_Debt_PJ_10M >= Long_Term_Debt_PJ_11M or Long_Term_Debt_PJ_11M is null then 10 else
	case when Long_Term_Debt_PJ_11M >= Long_Term_Debt_PJ_12M or Long_Term_Debt_PJ_12M is null then 11 else
	case when Long_Term_Debt_PJ_12M >= Long_Term_Debt_PJ_13M or Long_Term_Debt_PJ_13M is null then 12 else
	case when Long_Term_Debt_PJ_13M >= Long_Term_Debt_PJ_14M or Long_Term_Debt_PJ_14M is null then 13 else
	case when Long_Term_Debt_PJ_14M >= Long_Term_Debt_PJ_15M or Long_Term_Debt_PJ_15M is null then 14 else
	case when Long_Term_Debt_PJ_15M >= Long_Term_Debt_PJ_16M or Long_Term_Debt_PJ_16M is null then 15 else
	case when Long_Term_Debt_PJ_16M >= Long_Term_Debt_PJ_17M or Long_Term_Debt_PJ_17M is null then 16 else
	case when Long_Term_Debt_PJ_17M >= Long_Term_Debt_PJ_18M or Long_Term_Debt_PJ_18M is null then 17 else
	case when Long_Term_Debt_PJ_18M >= Long_Term_Debt_PJ_19M or Long_Term_Debt_PJ_19M is null then 18 else
	case when Long_Term_Debt_PJ_19M >= Long_Term_Debt_PJ_20M or Long_Term_Debt_PJ_20M is null then 19 else
	case when Long_Term_Debt_PJ_20M >= Long_Term_Debt_PJ_21M or Long_Term_Debt_PJ_21M is null then 20 else
	case when Long_Term_Debt_PJ_21M >= Long_Term_Debt_PJ_22M or Long_Term_Debt_PJ_22M is null then 21 else
	case when Long_Term_Debt_PJ_22M >= Long_Term_Debt_PJ_23M or Long_Term_Debt_PJ_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_sth_pj,
	case when Long_Term_Debt_PJ_1M is null then null else
	case when Long_Term_Debt_PJ_Curr <= Long_Term_Debt_PJ_1M then 0 else
	case when Long_Term_Debt_PJ_1M <= Long_Term_Debt_PJ_2M or Long_Term_Debt_PJ_2M is null then 1 else
	case when Long_Term_Debt_PJ_2M <= Long_Term_Debt_PJ_3M or Long_Term_Debt_PJ_3M is null then 2 else
	case when Long_Term_Debt_PJ_3M <= Long_Term_Debt_PJ_4M or Long_Term_Debt_PJ_4M is null then 3 else
	case when Long_Term_Debt_PJ_4M <= Long_Term_Debt_PJ_5M or Long_Term_Debt_PJ_5M is null then 4 else
	case when Long_Term_Debt_PJ_5M <= Long_Term_Debt_PJ_6M or Long_Term_Debt_PJ_6M is null then 5 else
	case when Long_Term_Debt_PJ_6M <= Long_Term_Debt_PJ_7M or Long_Term_Debt_PJ_7M is null then 6 else
	case when Long_Term_Debt_PJ_7M <= Long_Term_Debt_PJ_8M or Long_Term_Debt_PJ_8M is null then 7 else
	case when Long_Term_Debt_PJ_8M <= Long_Term_Debt_PJ_9M or Long_Term_Debt_PJ_9M is null then 8 else
	case when Long_Term_Debt_PJ_9M <= Long_Term_Debt_PJ_10M or Long_Term_Debt_PJ_10M is null then 9 else
	case when Long_Term_Debt_PJ_10M <= Long_Term_Debt_PJ_11M or Long_Term_Debt_PJ_11M is null then 10 else
	case when Long_Term_Debt_PJ_11M <= Long_Term_Debt_PJ_12M or Long_Term_Debt_PJ_12M is null then 11 else
	case when Long_Term_Debt_PJ_12M <= Long_Term_Debt_PJ_13M or Long_Term_Debt_PJ_13M is null then 12 else
	case when Long_Term_Debt_PJ_13M <= Long_Term_Debt_PJ_14M or Long_Term_Debt_PJ_14M is null then 13 else
	case when Long_Term_Debt_PJ_14M <= Long_Term_Debt_PJ_15M or Long_Term_Debt_PJ_15M is null then 14 else
	case when Long_Term_Debt_PJ_15M <= Long_Term_Debt_PJ_16M or Long_Term_Debt_PJ_16M is null then 15 else
	case when Long_Term_Debt_PJ_16M <= Long_Term_Debt_PJ_17M or Long_Term_Debt_PJ_17M is null then 16 else
	case when Long_Term_Debt_PJ_17M <= Long_Term_Debt_PJ_18M or Long_Term_Debt_PJ_18M is null then 17 else
	case when Long_Term_Debt_PJ_18M <= Long_Term_Debt_PJ_19M or Long_Term_Debt_PJ_19M is null then 18 else
	case when Long_Term_Debt_PJ_19M <= Long_Term_Debt_PJ_20M or Long_Term_Debt_PJ_20M is null then 19 else
	case when Long_Term_Debt_PJ_20M <= Long_Term_Debt_PJ_21M or Long_Term_Debt_PJ_21M is null then 20 else
	case when Long_Term_Debt_PJ_21M <= Long_Term_Debt_PJ_22M or Long_Term_Debt_PJ_22M is null then 21 else
	case when Long_Term_Debt_PJ_22M <= Long_Term_Debt_PJ_23M or Long_Term_Debt_PJ_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_hth_pj,
	Lim_Cred_PJ_Curr::float,
	Lim_Cred_PJ_1M::float,
	Lim_Cred_PJ_2M::float,
	Lim_Cred_PJ_3M::float,
	Lim_Cred_PJ_4M::float,
	Lim_Cred_PJ_5M::float,
	Lim_Cred_PJ_6M::float,
	Lim_Cred_PJ_7M::float,
	Lim_Cred_PJ_8M::float,
	Lim_Cred_PJ_9M::float,
	Lim_Cred_PJ_10M::float,
	Lim_Cred_PJ_11M::float,
	Ever_Lim_Cred_PJ::float,
	Max_Lim_Cred_PJ::float,
	case when Lim_cred_PJ_22M > Lim_cred_PJ_23M then 1 else 0 end +
	case when Lim_cred_PJ_21M > Lim_cred_PJ_22M then 1 else 0 end +
	case when Lim_cred_PJ_20M > Lim_cred_PJ_21M then 1 else 0 end +
	case when Lim_cred_PJ_19M > Lim_cred_PJ_20M then 1 else 0 end +
	case when Lim_cred_PJ_18M > Lim_cred_PJ_19M then 1 else 0 end +
	case when Lim_cred_PJ_17M > Lim_cred_PJ_18M then 1 else 0 end +
	case when Lim_cred_PJ_16M > Lim_cred_PJ_17M then 1 else 0 end +
	case when Lim_cred_PJ_15M > Lim_cred_PJ_16M then 1 else 0 end +
	case when Lim_cred_PJ_14M > Lim_cred_PJ_15M then 1 else 0 end +
	case when Lim_cred_PJ_13M > Lim_cred_PJ_14M then 1 else 0 end +
	case when Lim_cred_PJ_12M > Lim_cred_PJ_13M then 1 else 0 end +
	case when Lim_cred_PJ_11M > Lim_cred_PJ_12M then 1 else 0 end +
	case when Lim_cred_PJ_10M > Lim_cred_PJ_11M then 1 else 0 end +
	case when Lim_cred_PJ_9M > Lim_cred_PJ_10M then 1 else 0 end +
	case when Lim_cred_PJ_8M > Lim_cred_PJ_9M then 1 else 0 end +
	case when Lim_cred_PJ_7M > Lim_cred_PJ_8M then 1 else 0 end +
	case when Lim_cred_PJ_6M > Lim_cred_PJ_7M then 1 else 0 end +
	case when Lim_cred_PJ_5M > Lim_cred_PJ_6M then 1 else 0 end +
	case when Lim_cred_PJ_4M > Lim_cred_PJ_5M then 1 else 0 end +
	case when Lim_cred_PJ_3M > Lim_cred_PJ_4M then 1 else 0 end +
	case when Lim_cred_PJ_2M > Lim_cred_PJ_3M then 1 else 0 end +
	case when Lim_cred_PJ_1M > Lim_cred_PJ_2M then 1 else 0 end +
	case when Lim_cred_PJ_Curr > Lim_cred_PJ_1M then 1 else 0 end::float as Meses_Aumento_Lim_Cred_PJ,
	case when Lim_cred_PJ_22M < Lim_cred_PJ_23M then 1 else 0 end +
	case when Lim_cred_PJ_21M < Lim_cred_PJ_22M then 1 else 0 end +
	case when Lim_cred_PJ_20M < Lim_cred_PJ_21M then 1 else 0 end +
	case when Lim_cred_PJ_19M < Lim_cred_PJ_20M then 1 else 0 end +
	case when Lim_cred_PJ_18M < Lim_cred_PJ_19M then 1 else 0 end +
	case when Lim_cred_PJ_17M < Lim_cred_PJ_18M then 1 else 0 end +
	case when Lim_cred_PJ_16M < Lim_cred_PJ_17M then 1 else 0 end +
	case when Lim_cred_PJ_15M < Lim_cred_PJ_16M then 1 else 0 end +
	case when Lim_cred_PJ_14M < Lim_cred_PJ_15M then 1 else 0 end +
	case when Lim_cred_PJ_13M < Lim_cred_PJ_14M then 1 else 0 end +
	case when Lim_cred_PJ_12M < Lim_cred_PJ_13M then 1 else 0 end +
	case when Lim_cred_PJ_11M < Lim_cred_PJ_12M then 1 else 0 end +
	case when Lim_cred_PJ_10M < Lim_cred_PJ_11M then 1 else 0 end +
	case when Lim_cred_PJ_9M < Lim_cred_PJ_10M then 1 else 0 end +
	case when Lim_cred_PJ_8M < Lim_cred_PJ_9M then 1 else 0 end +
	case when Lim_cred_PJ_7M < Lim_cred_PJ_8M then 1 else 0 end +
	case when Lim_cred_PJ_6M < Lim_cred_PJ_7M then 1 else 0 end +
	case when Lim_cred_PJ_5M < Lim_cred_PJ_6M then 1 else 0 end +
	case when Lim_cred_PJ_4M < Lim_cred_PJ_5M then 1 else 0 end +
	case when Lim_cred_PJ_3M < Lim_cred_PJ_4M then 1 else 0 end +
	case when Lim_cred_PJ_2M < Lim_cred_PJ_3M then 1 else 0 end +
	case when Lim_cred_PJ_1M < Lim_cred_PJ_2M then 1 else 0 end +
	case when Lim_cred_PJ_Curr < Lim_cred_PJ_1M then 1 else 0 end::float as Meses_Reducao_Lim_Cred_PJ,
	case when Lim_cred_PJ_1M is null then null else
	case when Lim_cred_PJ_Curr >= Lim_cred_PJ_1M or Lim_cred_PJ_1M is null then 0 else
	case when Lim_cred_PJ_1M >= Lim_cred_PJ_2M or Lim_cred_PJ_2M is null then 1 else
	case when Lim_cred_PJ_2M >= Lim_cred_PJ_3M or Lim_cred_PJ_3M is null then 2 else
	case when Lim_cred_PJ_3M >= Lim_cred_PJ_4M or Lim_cred_PJ_4M is null then 3 else
	case when Lim_cred_PJ_4M >= Lim_cred_PJ_5M or Lim_cred_PJ_5M is null then 4 else
	case when Lim_cred_PJ_5M >= Lim_cred_PJ_6M or Lim_cred_PJ_6M is null then 5 else
	case when Lim_cred_PJ_6M >= Lim_cred_PJ_7M or Lim_cred_PJ_7M is null then 6 else
	case when Lim_cred_PJ_7M >= Lim_cred_PJ_8M or Lim_cred_PJ_8M is null then 7 else
	case when Lim_cred_PJ_8M >= Lim_cred_PJ_9M or Lim_cred_PJ_9M is null then 8 else
	case when Lim_cred_PJ_9M >= Lim_cred_PJ_10M or Lim_cred_PJ_10M is null then 9 else
	case when Lim_cred_PJ_10M >= Lim_cred_PJ_11M or Lim_cred_PJ_11M is null then 10 else
	case when Lim_cred_PJ_11M >= Lim_cred_PJ_12M or Lim_cred_PJ_12M is null then 11 else
	case when Lim_cred_PJ_12M >= Lim_cred_PJ_13M or Lim_cred_PJ_13M is null then 12 else
	case when Lim_cred_PJ_13M >= Lim_cred_PJ_14M or Lim_cred_PJ_14M is null then 13 else
	case when Lim_cred_PJ_14M >= Lim_cred_PJ_15M or Lim_cred_PJ_15M is null then 14 else
	case when Lim_cred_PJ_15M >= Lim_cred_PJ_16M or Lim_cred_PJ_16M is null then 15 else
	case when Lim_cred_PJ_16M >= Lim_cred_PJ_17M or Lim_cred_PJ_17M is null then 16 else
	case when Lim_cred_PJ_17M >= Lim_cred_PJ_18M or Lim_cred_PJ_18M is null then 17 else
	case when Lim_cred_PJ_18M >= Lim_cred_PJ_19M or Lim_cred_PJ_19M is null then 18 else
	case when Lim_cred_PJ_19M >= Lim_cred_PJ_20M or Lim_cred_PJ_20M is null then 19 else
	case when Lim_cred_PJ_20M >= Lim_cred_PJ_21M or Lim_cred_PJ_21M is null then 20 else
	case when Lim_cred_PJ_21M >= Lim_cred_PJ_22M or Lim_cred_PJ_22M is null then 21 else
	case when Lim_cred_PJ_22M >= Lim_cred_PJ_23M or Lim_cred_PJ_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_sth_lim_cred_pj,
	case when Lim_cred_PJ_1M is null then null else
	case when Lim_cred_PJ_Curr <= Lim_cred_PJ_1M or Lim_cred_PJ_1M is null then 0 else
	case when Lim_cred_PJ_1M <= Lim_cred_PJ_2M or Lim_cred_PJ_2M is null then 1 else
	case when Lim_cred_PJ_2M <= Lim_cred_PJ_3M or Lim_cred_PJ_3M is null then 2 else
	case when Lim_cred_PJ_3M <= Lim_cred_PJ_4M or Lim_cred_PJ_4M is null then 3 else
	case when Lim_cred_PJ_4M <= Lim_cred_PJ_5M or Lim_cred_PJ_5M is null then 4 else
	case when Lim_cred_PJ_5M <= Lim_cred_PJ_6M or Lim_cred_PJ_6M is null then 5 else
	case when Lim_cred_PJ_6M <= Lim_cred_PJ_7M or Lim_cred_PJ_7M is null then 6 else
	case when Lim_cred_PJ_7M <= Lim_cred_PJ_8M or Lim_cred_PJ_8M is null then 7 else
	case when Lim_cred_PJ_8M <= Lim_cred_PJ_9M or Lim_cred_PJ_9M is null then 8 else
	case when Lim_cred_PJ_9M <= Lim_cred_PJ_10M or Lim_cred_PJ_10M is null then 9 else
	case when Lim_cred_PJ_10M <= Lim_cred_PJ_11M or Lim_cred_PJ_11M is null then 10 else
	case when Lim_cred_PJ_11M <= Lim_cred_PJ_12M or Lim_cred_PJ_12M is null then 11 else
	case when Lim_cred_PJ_12M <= Lim_cred_PJ_13M or Lim_cred_PJ_13M is null then 12 else
	case when Lim_cred_PJ_13M <= Lim_cred_PJ_14M or Lim_cred_PJ_14M is null then 13 else
	case when Lim_cred_PJ_14M <= Lim_cred_PJ_15M or Lim_cred_PJ_15M is null then 14 else
	case when Lim_cred_PJ_15M <= Lim_cred_PJ_16M or Lim_cred_PJ_16M is null then 15 else
	case when Lim_cred_PJ_16M <= Lim_cred_PJ_17M or Lim_cred_PJ_17M is null then 16 else
	case when Lim_cred_PJ_17M <= Lim_cred_PJ_18M or Lim_cred_PJ_18M is null then 17 else
	case when Lim_cred_PJ_18M <= Lim_cred_PJ_19M or Lim_cred_PJ_19M is null then 18 else
	case when Lim_cred_PJ_19M <= Lim_cred_PJ_20M or Lim_cred_PJ_20M is null then 19 else
	case when Lim_cred_PJ_20M <= Lim_cred_PJ_21M or Lim_cred_PJ_21M is null then 20 else
	case when Lim_cred_PJ_21M <= Lim_cred_PJ_22M or Lim_cred_PJ_22M is null then 21 else
	case when Lim_cred_PJ_22M <= Lim_cred_PJ_23M or Lim_cred_PJ_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_hth_lim_cred_pj
from(select direct_prospect_id,
	Num_Ops_PJ,
	Num_FIs_PJ,
	First_Relation_FI_PJ,
	sum(DebtPJ) filter (where data = data_referencia) as Long_Term_Debt_PJ_Curr,
	sum(DebtPJ) filter (where data = data_referencia - interval '1 month') as Long_Term_Debt_PJ_1M,
	sum(DebtPJ) filter (where data = data_referencia - interval '2 months') as Long_Term_Debt_PJ_2M,
	sum(DebtPJ) filter (where data = data_referencia - interval '3 months') as Long_Term_Debt_PJ_3M,
	sum(DebtPJ) filter (where data = data_referencia - interval '4 months') as Long_Term_Debt_PJ_4M,
	sum(DebtPJ) filter (where data = data_referencia - interval '5 months') as Long_Term_Debt_PJ_5M,
	sum(DebtPJ) filter (where data = data_referencia - interval '6 months') as Long_Term_Debt_PJ_6M,
	sum(DebtPJ) filter (where data = data_referencia - interval '7 months') as Long_Term_Debt_PJ_7M,
	sum(DebtPJ) filter (where data = data_referencia - interval '8 months') as Long_Term_Debt_PJ_8M,
	sum(DebtPJ) filter (where data = data_referencia - interval '9 months') as Long_Term_Debt_PJ_9M,
	sum(DebtPJ) filter (where data = data_referencia - interval '10 months') as Long_Term_Debt_PJ_10M,
	sum(DebtPJ) filter (where data = data_referencia - interval '11 months') as Long_Term_Debt_PJ_11M,
	sum(DebtPJ) filter (where data = data_referencia - interval '12 months') as Long_Term_Debt_PJ_12M,
	sum(DebtPJ) filter (where data = data_referencia - interval '13 months') as Long_Term_Debt_PJ_13M,
	sum(DebtPJ) filter (where data = data_referencia - interval '14 months') as Long_Term_Debt_PJ_14M,
	sum(DebtPJ) filter (where data = data_referencia - interval '15 months') as Long_Term_Debt_PJ_15M,
	sum(DebtPJ) filter (where data = data_referencia - interval '16 months') as Long_Term_Debt_PJ_16M,
	sum(DebtPJ) filter (where data = data_referencia - interval '17 months') as Long_Term_Debt_PJ_17M,
	sum(DebtPJ) filter (where data = data_referencia - interval '18 months') as Long_Term_Debt_PJ_18M,
	sum(DebtPJ) filter (where data = data_referencia - interval '19 months') as Long_Term_Debt_PJ_19M,
	sum(DebtPJ) filter (where data = data_referencia - interval '20 months') as Long_Term_Debt_PJ_20M,
	sum(DebtPJ) filter (where data = data_referencia - interval '21 months') as Long_Term_Debt_PJ_21M,
	sum(DebtPJ) filter (where data = data_referencia - interval '22 months') as Long_Term_Debt_PJ_22M,
	sum(DebtPJ) filter (where data = data_referencia - interval '23 months') as Long_Term_Debt_PJ_23M,
	sum(DebtPJ) filter (where data <= data_referencia) as Ever_long_term_debt_PJ,
	max(DebtPJ) filter (where data <= data_referencia) as Max_Long_Term_Debt_PJ,
	sum(VencidoPJ) filter (where data = data_referencia) as Overdue_PJ_Curr,
	extract(year from age(min(data_referencia), max(data) filter (where data <= data_referencia and VencidoPJ > 0)))*12 + extract(month from age(min(data_referencia), max(data) filter (where data <= data_referencia and VencidoPJ > 0))) as Months_Since_Last_Overdue_PJ,
	count(VencidoPJ) filter (where data <= data_referencia and VencidoPJ > 0) as Months_Overdue_PJ,
	max(VencidoPJ) filter (where data <= data_referencia) as Max_Overdue_PJ,
	sum(PrejuizoPJ) filter (where data = data_referencia) as Default_PJ_Curr,
	extract(year from age(min(data_referencia), max(data) filter (where data <= data_referencia and PrejuizoPJ > 0)))*12 + extract(month from age(min(data_referencia), max(data) filter (where data <= data_referencia and PrejuizoPJ > 0)))  as Months_Since_Last_Default_PJ,
	count(PrejuizoPJ) filter (where data <= data_referencia and PrejuizoPJ > 0) as Months_Default_PJ,
	max(PrejuizoPJ) filter (where data <= data_referencia) as Max_Default_PJ,
	count(*) filter (where data <= data_referencia) as Qtd_meses_escopo_pj,
	sum(LimCredPJ) filter (where data = data_referencia) as Lim_Cred_PJ_Curr,
	sum(LimCredPJ) filter (where data = data_referencia - interval '1 month') as Lim_Cred_PJ_1M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '2 months') as Lim_Cred_PJ_2M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '3 months') as Lim_Cred_PJ_3M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '4 months') as Lim_Cred_PJ_4M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '5 months') as Lim_Cred_PJ_5M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '6 months') as Lim_Cred_PJ_6M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '7 months') as Lim_Cred_PJ_7M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '8 months') as Lim_Cred_PJ_8M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '9 months') as Lim_Cred_PJ_9M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '10 months') as Lim_Cred_PJ_10M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '11 months') as Lim_Cred_PJ_11M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '12 months') as Lim_Cred_PJ_12M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '13 months') as Lim_Cred_PJ_13M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '14 months') as Lim_Cred_PJ_14M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '15 months') as Lim_Cred_PJ_15M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '16 months') as Lim_Cred_PJ_16M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '17 months') as Lim_Cred_PJ_17M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '18 months') as Lim_Cred_PJ_18M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '19 months') as Lim_Cred_PJ_19M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '20 months') as Lim_Cred_PJ_20M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '21 months') as Lim_Cred_PJ_21M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '22 months') as Lim_Cred_PJ_22M,
	sum(LimCredPJ) filter (where data = data_referencia - interval '23 months') as Lim_Cred_PJ_23M,
	sum(LimCredPJ) filter (where data <= data_referencia) as Ever_Lim_Cred_PJ,
	max(LimCredPJ) filter (where data <= data_referencia) as Max_Lim_Cred_PJ
	from(select direct_prospect_id,
			case when extract(day from loan_date) >= 16 then case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end else case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end end as data_referencia,
			to_date(jsonb_array_elements(cc.data->'historico'->'Carteira de Crédito')->>'data','mm/yyyy') as data,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Carteira de Crédito')->>'valor','-','0'),'.',''),',','.')::float DebtPJ,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Vencido')->>'valor','-','0'),'.',''),',','.')::float VencidoPJ,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Prejuízo')->>'valor','-','0'),'.',''),',','.')::float PrejuizoPJ,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Limite de Crédito')->>'valor','-','0'),'.',''),',','.')::float LimCredPJ,
			replace(replace(case when position('Quantidade de Operações' in cc.raw) > 0 then substring(cc.raw,position('Quantidade de Operações' in cc.raw)+length('Quantidade de Operações</div></td><td class="fu3" width="50%"><b>'),3) else null end,'<',''),'/','')::int as Num_Ops_PJ,
			replace(replace(case when position('Quantidade de IFs em que o Cliente possui Operações' in cc.raw) > 0 then substring(cc.raw,position('Quantidade de IFs em que o Cliente possui Operações' in cc.raw)+
				length('Quantidade de IFs em que o Cliente possui Operações</div></td><td class="fu3" width="50%"><b>'),3) else null end,'<',''),'/','')::int as Num_FIs_PJ,
			extract(year from age(loan_date,to_date(replace(replace(replace(case when position('Data de Início de Relacionamento com a IF' in cc.raw) > 0 then substring(cc.raw,position('Data de Início de Relacionamento com a IF' in cc.raw)+
				length('Data de Início de Relacionamento com a IF</div></td><td class="fu3" width="50%"><b>'),10) else null end,'<',''),'/',''),'-','0'),'ddmmyyyy')))::int as First_Relation_FI_PJ
		from credito_coleta cc
		join(select dp.direct_prospect_id, lr.loan_date,cnpj,
				max(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id) filter (where to_date(left(data->>'criado_em',10),'yyyy-mm-dd') <= lr.loan_date and (cc.data->'historico' is not null or cc.data->'erro' is not null)) as max_date, 
				min(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id) filter (where data->'historico' is not null or cc.data->'erro' is not null) as min_date
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join offers o on o.direct_prospect_id = dp.direct_prospect_id
			join loan_requests lr on lr.offer_id = o.offer_id 
			where documento_tipo = 'CNPJ' and tipo = 'SCR' and lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')
			group by 1,2,3) as t1 on left(cc.documento,8) = left(cnpj,8) and case when max_date is null then to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id = min_date else to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id = max_date end) as t2
	group by 1,2,3,4) as t3) as SCRHistPJ on SCRHistPJ.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SCR PF
left join(select direct_prospect_id,
	Num_Ops_pf::float,
	Num_FIs_pf::float,
	First_Relation_FI_pf::float,
	Long_Term_Debt_pf_Curr::float,
	Long_Term_Debt_pf_1M::float,
	Long_Term_Debt_pf_2M::float,
	Long_Term_Debt_pf_3M::float,
	Long_Term_Debt_pf_4M::float,
	Long_Term_Debt_pf_5M::float,
	Long_Term_Debt_pf_6M::float,
	Long_Term_Debt_pf_7M::float,
	Long_Term_Debt_pf_8M::float,
	Long_Term_Debt_pf_9M::float,
	Long_Term_Debt_pf_10M::float,
	Long_Term_Debt_pf_11M::float,
	Ever_long_term_debt_pf::float,
	Max_Long_Term_Debt_pf::float,
	Overdue_pf_Curr::float,
	Months_Since_Last_Overdue_pf::float,
	Months_Overdue_pf::float,
	Max_Overdue_pf::float,
	Default_pf_Curr::float,
	Months_Since_Last_Default_pf::float,
	Months_Default_pf::float,
	Max_Default_pf::float,
	Qtd_meses_escopo_pf::float,
	case when Long_Term_Debt_pf_22M > Long_Term_Debt_pf_23M then 1 else 0 end +
	case when Long_Term_Debt_pf_21M > Long_Term_Debt_pf_22M then 1 else 0 end +
	case when Long_Term_Debt_pf_20M > Long_Term_Debt_pf_21M then 1 else 0 end +
	case when Long_Term_Debt_pf_19M > Long_Term_Debt_pf_20M then 1 else 0 end +
	case when Long_Term_Debt_pf_18M > Long_Term_Debt_pf_19M then 1 else 0 end +
	case when Long_Term_Debt_pf_17M > Long_Term_Debt_pf_18M then 1 else 0 end +
	case when Long_Term_Debt_pf_16M > Long_Term_Debt_pf_17M then 1 else 0 end +
	case when Long_Term_Debt_pf_15M > Long_Term_Debt_pf_16M then 1 else 0 end +
	case when Long_Term_Debt_pf_14M > Long_Term_Debt_pf_15M then 1 else 0 end +
	case when Long_Term_Debt_pf_13M > Long_Term_Debt_pf_14M then 1 else 0 end +
	case when Long_Term_Debt_pf_12M > Long_Term_Debt_pf_13M then 1 else 0 end +
	case when Long_Term_Debt_pf_11M > Long_Term_Debt_pf_12M then 1 else 0 end +
	case when Long_Term_Debt_pf_10M > Long_Term_Debt_pf_11M then 1 else 0 end +
	case when Long_Term_Debt_pf_9M > Long_Term_Debt_pf_10M then 1 else 0 end +
	case when Long_Term_Debt_pf_8M > Long_Term_Debt_pf_9M then 1 else 0 end +
	case when Long_Term_Debt_pf_7M > Long_Term_Debt_pf_8M then 1 else 0 end +
	case when Long_Term_Debt_pf_6M > Long_Term_Debt_pf_7M then 1 else 0 end +
	case when Long_Term_Debt_pf_5M > Long_Term_Debt_pf_6M then 1 else 0 end +
	case when Long_Term_Debt_pf_4M > Long_Term_Debt_pf_5M then 1 else 0 end +
	case when Long_Term_Debt_pf_3M > Long_Term_Debt_pf_4M then 1 else 0 end +
	case when Long_Term_Debt_pf_2M > Long_Term_Debt_pf_3M then 1 else 0 end +
	case when Long_Term_Debt_pf_1M > Long_Term_Debt_pf_2M then 1 else 0 end +
	case when Long_Term_Debt_pf_Curr > Long_Term_Debt_pf_1M then 1 else 0 end::float as Meses_Aumento_Dividapf,
	case when Long_Term_Debt_pf_22M < Long_Term_Debt_pf_23M then 1 else 0 end +
	case when Long_Term_Debt_pf_21M < Long_Term_Debt_pf_22M then 1 else 0 end +
	case when Long_Term_Debt_pf_20M < Long_Term_Debt_pf_21M then 1 else 0 end +
	case when Long_Term_Debt_pf_19M < Long_Term_Debt_pf_20M then 1 else 0 end +
	case when Long_Term_Debt_pf_18M < Long_Term_Debt_pf_19M then 1 else 0 end +
	case when Long_Term_Debt_pf_17M < Long_Term_Debt_pf_18M then 1 else 0 end +
	case when Long_Term_Debt_pf_16M < Long_Term_Debt_pf_17M then 1 else 0 end +
	case when Long_Term_Debt_pf_15M < Long_Term_Debt_pf_16M then 1 else 0 end +
	case when Long_Term_Debt_pf_14M < Long_Term_Debt_pf_15M then 1 else 0 end +
	case when Long_Term_Debt_pf_13M < Long_Term_Debt_pf_14M then 1 else 0 end +
	case when Long_Term_Debt_pf_12M < Long_Term_Debt_pf_13M then 1 else 0 end +
	case when Long_Term_Debt_pf_11M < Long_Term_Debt_pf_12M then 1 else 0 end +
	case when Long_Term_Debt_pf_10M < Long_Term_Debt_pf_11M then 1 else 0 end +
	case when Long_Term_Debt_pf_9M < Long_Term_Debt_pf_10M then 1 else 0 end +
	case when Long_Term_Debt_pf_8M < Long_Term_Debt_pf_9M then 1 else 0 end +
	case when Long_Term_Debt_pf_7M < Long_Term_Debt_pf_8M then 1 else 0 end +
	case when Long_Term_Debt_pf_6M < Long_Term_Debt_pf_7M then 1 else 0 end +
	case when Long_Term_Debt_pf_5M < Long_Term_Debt_pf_6M then 1 else 0 end +
	case when Long_Term_Debt_pf_4M < Long_Term_Debt_pf_5M then 1 else 0 end +
	case when Long_Term_Debt_pf_3M < Long_Term_Debt_pf_4M then 1 else 0 end +
	case when Long_Term_Debt_pf_2M < Long_Term_Debt_pf_3M then 1 else 0 end +
	case when Long_Term_Debt_pf_1M < Long_Term_Debt_pf_2M then 1 else 0 end +
	case when Long_Term_Debt_pf_Curr < Long_Term_Debt_pf_1M then 1 else 0 end::float as Meses_Reducao_Dividapf,
	(case when Long_Term_Debt_pf_22M < Long_Term_Debt_pf_23M then Long_Term_Debt_pf_22M - Long_Term_Debt_pf_23M else 0 end +
	case when Long_Term_Debt_pf_21M < Long_Term_Debt_pf_22M then Long_Term_Debt_pf_21M - Long_Term_Debt_pf_22M else 0 end +
	case when Long_Term_Debt_pf_20M < Long_Term_Debt_pf_21M then Long_Term_Debt_pf_20M - Long_Term_Debt_pf_21M else 0 end +
	case when Long_Term_Debt_pf_19M < Long_Term_Debt_pf_20M then Long_Term_Debt_pf_19M - Long_Term_Debt_pf_20M else 0 end +
	case when Long_Term_Debt_pf_18M < Long_Term_Debt_pf_19M then Long_Term_Debt_pf_18M - Long_Term_Debt_pf_19M else 0 end +
	case when Long_Term_Debt_pf_17M < Long_Term_Debt_pf_18M then Long_Term_Debt_pf_17M - Long_Term_Debt_pf_18M else 0 end +
	case when Long_Term_Debt_pf_16M < Long_Term_Debt_pf_17M then Long_Term_Debt_pf_16M - Long_Term_Debt_pf_17M else 0 end +
	case when Long_Term_Debt_pf_15M < Long_Term_Debt_pf_16M then Long_Term_Debt_pf_15M - Long_Term_Debt_pf_16M else 0 end +
	case when Long_Term_Debt_pf_14M < Long_Term_Debt_pf_15M then Long_Term_Debt_pf_14M - Long_Term_Debt_pf_15M else 0 end +
	case when Long_Term_Debt_pf_13M < Long_Term_Debt_pf_14M then Long_Term_Debt_pf_13M - Long_Term_Debt_pf_14M else 0 end +
	case when Long_Term_Debt_pf_12M < Long_Term_Debt_pf_13M then Long_Term_Debt_pf_12M - Long_Term_Debt_pf_13M else 0 end +
	case when Long_Term_Debt_pf_11M < Long_Term_Debt_pf_12M then Long_Term_Debt_pf_11M - Long_Term_Debt_pf_12M else 0 end +
	case when Long_Term_Debt_pf_10M < Long_Term_Debt_pf_11M then Long_Term_Debt_pf_10M - Long_Term_Debt_pf_11M else 0 end +
	case when Long_Term_Debt_pf_9M < Long_Term_Debt_pf_10M then Long_Term_Debt_pf_9M - Long_Term_Debt_pf_10M else 0 end +
	case when Long_Term_Debt_pf_8M < Long_Term_Debt_pf_9M then Long_Term_Debt_pf_8M - Long_Term_Debt_pf_9M else 0 end +
	case when Long_Term_Debt_pf_7M < Long_Term_Debt_pf_8M then Long_Term_Debt_pf_7M - Long_Term_Debt_pf_8M else 0 end +
	case when Long_Term_Debt_pf_6M < Long_Term_Debt_pf_7M then Long_Term_Debt_pf_6M - Long_Term_Debt_pf_7M else 0 end +
	case when Long_Term_Debt_pf_5M < Long_Term_Debt_pf_6M then Long_Term_Debt_pf_5M - Long_Term_Debt_pf_6M else 0 end +
	case when Long_Term_Debt_pf_4M < Long_Term_Debt_pf_5M then Long_Term_Debt_pf_4M - Long_Term_Debt_pf_5M else 0 end +
	case when Long_Term_Debt_pf_3M < Long_Term_Debt_pf_4M then Long_Term_Debt_pf_3M - Long_Term_Debt_pf_4M else 0 end +
	case when Long_Term_Debt_pf_2M < Long_Term_Debt_pf_3M then Long_Term_Debt_pf_2M - Long_Term_Debt_pf_3M else 0 end +
	case when Long_Term_Debt_pf_1M < Long_Term_Debt_pf_2M then Long_Term_Debt_pf_1M - Long_Term_Debt_pf_2M else 0 end +
	case when Long_Term_Debt_pf_Curr < Long_Term_Debt_pf_1M then Long_Term_Debt_pf_Curr - Long_Term_Debt_pf_1M else 0 end)*(-1)::float as Saldo_Amort_Dividapf,
	case when Long_Term_Debt_pf_1M is null then null else
	case when Long_Term_Debt_pf_Curr >= Long_Term_Debt_pf_1M then 0 else
	case when Long_Term_Debt_pf_1M >= Long_Term_Debt_pf_2M or Long_Term_Debt_pf_2M is null then 1 else
	case when Long_Term_Debt_pf_2M >= Long_Term_Debt_pf_3M or Long_Term_Debt_pf_3M is null then 2 else
	case when Long_Term_Debt_pf_3M >= Long_Term_Debt_pf_4M or Long_Term_Debt_pf_4M is null then 3 else
	case when Long_Term_Debt_pf_4M >= Long_Term_Debt_pf_5M or Long_Term_Debt_pf_5M is null then 4 else
	case when Long_Term_Debt_pf_5M >= Long_Term_Debt_pf_6M or Long_Term_Debt_pf_6M is null then 5 else
	case when Long_Term_Debt_pf_6M >= Long_Term_Debt_pf_7M or Long_Term_Debt_pf_7M is null then 6 else
	case when Long_Term_Debt_pf_7M >= Long_Term_Debt_pf_8M or Long_Term_Debt_pf_8M is null then 7 else
	case when Long_Term_Debt_pf_8M >= Long_Term_Debt_pf_9M or Long_Term_Debt_pf_9M is null then 8 else
	case when Long_Term_Debt_pf_9M >= Long_Term_Debt_pf_10M or Long_Term_Debt_pf_10M is null then 9 else
	case when Long_Term_Debt_pf_10M >= Long_Term_Debt_pf_11M or Long_Term_Debt_pf_11M is null then 10 else
	case when Long_Term_Debt_pf_11M >= Long_Term_Debt_pf_12M or Long_Term_Debt_pf_12M is null then 11 else
	case when Long_Term_Debt_pf_12M >= Long_Term_Debt_pf_13M or Long_Term_Debt_pf_13M is null then 12 else
	case when Long_Term_Debt_pf_13M >= Long_Term_Debt_pf_14M or Long_Term_Debt_pf_14M is null then 13 else
	case when Long_Term_Debt_pf_14M >= Long_Term_Debt_pf_15M or Long_Term_Debt_pf_15M is null then 14 else
	case when Long_Term_Debt_pf_15M >= Long_Term_Debt_pf_16M or Long_Term_Debt_pf_16M is null then 15 else
	case when Long_Term_Debt_pf_16M >= Long_Term_Debt_pf_17M or Long_Term_Debt_pf_17M is null then 16 else
	case when Long_Term_Debt_pf_17M >= Long_Term_Debt_pf_18M or Long_Term_Debt_pf_18M is null then 17 else
	case when Long_Term_Debt_pf_18M >= Long_Term_Debt_pf_19M or Long_Term_Debt_pf_19M is null then 18 else
	case when Long_Term_Debt_pf_19M >= Long_Term_Debt_pf_20M or Long_Term_Debt_pf_20M is null then 19 else
	case when Long_Term_Debt_pf_20M >= Long_Term_Debt_pf_21M or Long_Term_Debt_pf_21M is null then 20 else
	case when Long_Term_Debt_pf_21M >= Long_Term_Debt_pf_22M or Long_Term_Debt_pf_22M is null then 21 else
	case when Long_Term_Debt_pf_22M >= Long_Term_Debt_pf_23M or Long_Term_Debt_pf_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_sth_pf,
	case when Long_Term_Debt_pf_1M is null then null else
	case when Long_Term_Debt_pf_Curr <= Long_Term_Debt_pf_1M then 0 else
	case when Long_Term_Debt_pf_1M <= Long_Term_Debt_pf_2M or Long_Term_Debt_pf_2M is null then 1 else
	case when Long_Term_Debt_pf_2M <= Long_Term_Debt_pf_3M or Long_Term_Debt_pf_3M is null then 2 else
	case when Long_Term_Debt_pf_3M <= Long_Term_Debt_pf_4M or Long_Term_Debt_pf_4M is null then 3 else
	case when Long_Term_Debt_pf_4M <= Long_Term_Debt_pf_5M or Long_Term_Debt_pf_5M is null then 4 else
	case when Long_Term_Debt_pf_5M <= Long_Term_Debt_pf_6M or Long_Term_Debt_pf_6M is null then 5 else
	case when Long_Term_Debt_pf_6M <= Long_Term_Debt_pf_7M or Long_Term_Debt_pf_7M is null then 6 else
	case when Long_Term_Debt_pf_7M <= Long_Term_Debt_pf_8M or Long_Term_Debt_pf_8M is null then 7 else
	case when Long_Term_Debt_pf_8M <= Long_Term_Debt_pf_9M or Long_Term_Debt_pf_9M is null then 8 else
	case when Long_Term_Debt_pf_9M <= Long_Term_Debt_pf_10M or Long_Term_Debt_pf_10M is null then 9 else
	case when Long_Term_Debt_pf_10M <= Long_Term_Debt_pf_11M or Long_Term_Debt_pf_11M is null then 10 else
	case when Long_Term_Debt_pf_11M <= Long_Term_Debt_pf_12M or Long_Term_Debt_pf_12M is null then 11 else
	case when Long_Term_Debt_pf_12M <= Long_Term_Debt_pf_13M or Long_Term_Debt_pf_13M is null then 12 else
	case when Long_Term_Debt_pf_13M <= Long_Term_Debt_pf_14M or Long_Term_Debt_pf_14M is null then 13 else
	case when Long_Term_Debt_pf_14M <= Long_Term_Debt_pf_15M or Long_Term_Debt_pf_15M is null then 14 else
	case when Long_Term_Debt_pf_15M <= Long_Term_Debt_pf_16M or Long_Term_Debt_pf_16M is null then 15 else
	case when Long_Term_Debt_pf_16M <= Long_Term_Debt_pf_17M or Long_Term_Debt_pf_17M is null then 16 else
	case when Long_Term_Debt_pf_17M <= Long_Term_Debt_pf_18M or Long_Term_Debt_pf_18M is null then 17 else
	case when Long_Term_Debt_pf_18M <= Long_Term_Debt_pf_19M or Long_Term_Debt_pf_19M is null then 18 else
	case when Long_Term_Debt_pf_19M <= Long_Term_Debt_pf_20M or Long_Term_Debt_pf_20M is null then 19 else
	case when Long_Term_Debt_pf_20M <= Long_Term_Debt_pf_21M or Long_Term_Debt_pf_21M is null then 20 else
	case when Long_Term_Debt_pf_21M <= Long_Term_Debt_pf_22M or Long_Term_Debt_pf_22M is null then 21 else
	case when Long_Term_Debt_pf_22M <= Long_Term_Debt_pf_23M or Long_Term_Debt_pf_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_hth_pf,
	Lim_Cred_pf_Curr::float,
	Lim_Cred_pf_1M::float,
	Lim_Cred_pf_2M::float,
	Lim_Cred_pf_3M::float,
	Lim_Cred_pf_4M::float,
	Lim_Cred_pf_5M::float,
	Lim_Cred_pf_6M::float,
	Lim_Cred_pf_7M::float,
	Lim_Cred_pf_8M::float,
	Lim_Cred_pf_9M::float,
	Lim_Cred_pf_10M::float,
	Lim_Cred_pf_11M::float,
	Ever_Lim_Cred_pf::float,
	Max_Lim_Cred_pf::float,
	case when Lim_cred_pf_22M > Lim_cred_pf_23M then 1 else 0 end +
	case when Lim_cred_pf_21M > Lim_cred_pf_22M then 1 else 0 end +
	case when Lim_cred_pf_20M > Lim_cred_pf_21M then 1 else 0 end +
	case when Lim_cred_pf_19M > Lim_cred_pf_20M then 1 else 0 end +
	case when Lim_cred_pf_18M > Lim_cred_pf_19M then 1 else 0 end +
	case when Lim_cred_pf_17M > Lim_cred_pf_18M then 1 else 0 end +
	case when Lim_cred_pf_16M > Lim_cred_pf_17M then 1 else 0 end +
	case when Lim_cred_pf_15M > Lim_cred_pf_16M then 1 else 0 end +
	case when Lim_cred_pf_14M > Lim_cred_pf_15M then 1 else 0 end +
	case when Lim_cred_pf_13M > Lim_cred_pf_14M then 1 else 0 end +
	case when Lim_cred_pf_12M > Lim_cred_pf_13M then 1 else 0 end +
	case when Lim_cred_pf_11M > Lim_cred_pf_12M then 1 else 0 end +
	case when Lim_cred_pf_10M > Lim_cred_pf_11M then 1 else 0 end +
	case when Lim_cred_pf_9M > Lim_cred_pf_10M then 1 else 0 end +
	case when Lim_cred_pf_8M > Lim_cred_pf_9M then 1 else 0 end +
	case when Lim_cred_pf_7M > Lim_cred_pf_8M then 1 else 0 end +
	case when Lim_cred_pf_6M > Lim_cred_pf_7M then 1 else 0 end +
	case when Lim_cred_pf_5M > Lim_cred_pf_6M then 1 else 0 end +
	case when Lim_cred_pf_4M > Lim_cred_pf_5M then 1 else 0 end +
	case when Lim_cred_pf_3M > Lim_cred_pf_4M then 1 else 0 end +
	case when Lim_cred_pf_2M > Lim_cred_pf_3M then 1 else 0 end +
	case when Lim_cred_pf_1M > Lim_cred_pf_2M then 1 else 0 end +
	case when Lim_cred_pf_Curr > Lim_cred_pf_1M then 1 else 0 end::float as Meses_Aumento_Lim_Cred_pf,
	case when Lim_cred_pf_22M < Lim_cred_pf_23M then 1 else 0 end +
	case when Lim_cred_pf_21M < Lim_cred_pf_22M then 1 else 0 end +
	case when Lim_cred_pf_20M < Lim_cred_pf_21M then 1 else 0 end +
	case when Lim_cred_pf_19M < Lim_cred_pf_20M then 1 else 0 end +
	case when Lim_cred_pf_18M < Lim_cred_pf_19M then 1 else 0 end +
	case when Lim_cred_pf_17M < Lim_cred_pf_18M then 1 else 0 end +
	case when Lim_cred_pf_16M < Lim_cred_pf_17M then 1 else 0 end +
	case when Lim_cred_pf_15M < Lim_cred_pf_16M then 1 else 0 end +
	case when Lim_cred_pf_14M < Lim_cred_pf_15M then 1 else 0 end +
	case when Lim_cred_pf_13M < Lim_cred_pf_14M then 1 else 0 end +
	case when Lim_cred_pf_12M < Lim_cred_pf_13M then 1 else 0 end +
	case when Lim_cred_pf_11M < Lim_cred_pf_12M then 1 else 0 end +
	case when Lim_cred_pf_10M < Lim_cred_pf_11M then 1 else 0 end +
	case when Lim_cred_pf_9M < Lim_cred_pf_10M then 1 else 0 end +
	case when Lim_cred_pf_8M < Lim_cred_pf_9M then 1 else 0 end +
	case when Lim_cred_pf_7M < Lim_cred_pf_8M then 1 else 0 end +
	case when Lim_cred_pf_6M < Lim_cred_pf_7M then 1 else 0 end +
	case when Lim_cred_pf_5M < Lim_cred_pf_6M then 1 else 0 end +
	case when Lim_cred_pf_4M < Lim_cred_pf_5M then 1 else 0 end +
	case when Lim_cred_pf_3M < Lim_cred_pf_4M then 1 else 0 end +
	case when Lim_cred_pf_2M < Lim_cred_pf_3M then 1 else 0 end +
	case when Lim_cred_pf_1M < Lim_cred_pf_2M then 1 else 0 end +
	case when Lim_cred_pf_Curr < Lim_cred_pf_1M then 1 else 0 end::float as Meses_Reducao_Lim_Cred_pf,
	case when Lim_cred_pf_1M is null then null else
	case when Lim_cred_pf_Curr >= Lim_cred_pf_1M or Lim_cred_pf_1M is null then 0 else
	case when Lim_cred_pf_1M >= Lim_cred_pf_2M or Lim_cred_pf_2M is null then 1 else
	case when Lim_cred_pf_2M >= Lim_cred_pf_3M or Lim_cred_pf_3M is null then 2 else
	case when Lim_cred_pf_3M >= Lim_cred_pf_4M or Lim_cred_pf_4M is null then 3 else
	case when Lim_cred_pf_4M >= Lim_cred_pf_5M or Lim_cred_pf_5M is null then 4 else
	case when Lim_cred_pf_5M >= Lim_cred_pf_6M or Lim_cred_pf_6M is null then 5 else
	case when Lim_cred_pf_6M >= Lim_cred_pf_7M or Lim_cred_pf_7M is null then 6 else
	case when Lim_cred_pf_7M >= Lim_cred_pf_8M or Lim_cred_pf_8M is null then 7 else
	case when Lim_cred_pf_8M >= Lim_cred_pf_9M or Lim_cred_pf_9M is null then 8 else
	case when Lim_cred_pf_9M >= Lim_cred_pf_10M or Lim_cred_pf_10M is null then 9 else
	case when Lim_cred_pf_10M >= Lim_cred_pf_11M or Lim_cred_pf_11M is null then 10 else
	case when Lim_cred_pf_11M >= Lim_cred_pf_12M or Lim_cred_pf_12M is null then 11 else
	case when Lim_cred_pf_12M >= Lim_cred_pf_13M or Lim_cred_pf_13M is null then 12 else
	case when Lim_cred_pf_13M >= Lim_cred_pf_14M or Lim_cred_pf_14M is null then 13 else
	case when Lim_cred_pf_14M >= Lim_cred_pf_15M or Lim_cred_pf_15M is null then 14 else
	case when Lim_cred_pf_15M >= Lim_cred_pf_16M or Lim_cred_pf_16M is null then 15 else
	case when Lim_cred_pf_16M >= Lim_cred_pf_17M or Lim_cred_pf_17M is null then 16 else
	case when Lim_cred_pf_17M >= Lim_cred_pf_18M or Lim_cred_pf_18M is null then 17 else
	case when Lim_cred_pf_18M >= Lim_cred_pf_19M or Lim_cred_pf_19M is null then 18 else
	case when Lim_cred_pf_19M >= Lim_cred_pf_20M or Lim_cred_pf_20M is null then 19 else
	case when Lim_cred_pf_20M >= Lim_cred_pf_21M or Lim_cred_pf_21M is null then 20 else
	case when Lim_cred_pf_21M >= Lim_cred_pf_22M or Lim_cred_pf_22M is null then 21 else
	case when Lim_cred_pf_22M >= Lim_cred_pf_23M or Lim_cred_pf_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_sth_lim_cred_pf,
	case when Lim_cred_pf_1M is null then null else
	case when Lim_cred_pf_Curr <= Lim_cred_pf_1M or Lim_cred_pf_1M is null then 0 else
	case when Lim_cred_pf_1M <= Lim_cred_pf_2M or Lim_cred_pf_2M is null then 1 else
	case when Lim_cred_pf_2M <= Lim_cred_pf_3M or Lim_cred_pf_3M is null then 2 else
	case when Lim_cred_pf_3M <= Lim_cred_pf_4M or Lim_cred_pf_4M is null then 3 else
	case when Lim_cred_pf_4M <= Lim_cred_pf_5M or Lim_cred_pf_5M is null then 4 else
	case when Lim_cred_pf_5M <= Lim_cred_pf_6M or Lim_cred_pf_6M is null then 5 else
	case when Lim_cred_pf_6M <= Lim_cred_pf_7M or Lim_cred_pf_7M is null then 6 else
	case when Lim_cred_pf_7M <= Lim_cred_pf_8M or Lim_cred_pf_8M is null then 7 else
	case when Lim_cred_pf_8M <= Lim_cred_pf_9M or Lim_cred_pf_9M is null then 8 else
	case when Lim_cred_pf_9M <= Lim_cred_pf_10M or Lim_cred_pf_10M is null then 9 else
	case when Lim_cred_pf_10M <= Lim_cred_pf_11M or Lim_cred_pf_11M is null then 10 else
	case when Lim_cred_pf_11M <= Lim_cred_pf_12M or Lim_cred_pf_12M is null then 11 else
	case when Lim_cred_pf_12M <= Lim_cred_pf_13M or Lim_cred_pf_13M is null then 12 else
	case when Lim_cred_pf_13M <= Lim_cred_pf_14M or Lim_cred_pf_14M is null then 13 else
	case when Lim_cred_pf_14M <= Lim_cred_pf_15M or Lim_cred_pf_15M is null then 14 else
	case when Lim_cred_pf_15M <= Lim_cred_pf_16M or Lim_cred_pf_16M is null then 15 else
	case when Lim_cred_pf_16M <= Lim_cred_pf_17M or Lim_cred_pf_17M is null then 16 else
	case when Lim_cred_pf_17M <= Lim_cred_pf_18M or Lim_cred_pf_18M is null then 17 else
	case when Lim_cred_pf_18M <= Lim_cred_pf_19M or Lim_cred_pf_19M is null then 18 else
	case when Lim_cred_pf_19M <= Lim_cred_pf_20M or Lim_cred_pf_20M is null then 19 else
	case when Lim_cred_pf_20M <= Lim_cred_pf_21M or Lim_cred_pf_21M is null then 20 else
	case when Lim_cred_pf_21M <= Lim_cred_pf_22M or Lim_cred_pf_22M is null then 21 else
	case when Lim_cred_pf_22M <= Lim_cred_pf_23M or Lim_cred_pf_23M is null then 22 else 23 end end end end end end end end end end end end end end end end end end end end end end end end::float as months_hth_lim_cred_pf
from(select direct_prospect_id,
	Num_Ops_pf,
	Num_FIs_pf,
	First_Relation_FI_pf,
	sum(Debtpf) filter (where data = data_referencia) as Long_Term_Debt_pf_Curr,
	sum(Debtpf) filter (where data = data_referencia - interval '1 month') as Long_Term_Debt_pf_1M,
	sum(Debtpf) filter (where data = data_referencia - interval '2 months') as Long_Term_Debt_pf_2M,
	sum(Debtpf) filter (where data = data_referencia - interval '3 months') as Long_Term_Debt_pf_3M,
	sum(Debtpf) filter (where data = data_referencia - interval '4 months') as Long_Term_Debt_pf_4M,
	sum(Debtpf) filter (where data = data_referencia - interval '5 months') as Long_Term_Debt_pf_5M,
	sum(Debtpf) filter (where data = data_referencia - interval '6 months') as Long_Term_Debt_pf_6M,
	sum(Debtpf) filter (where data = data_referencia - interval '7 months') as Long_Term_Debt_pf_7M,
	sum(Debtpf) filter (where data = data_referencia - interval '8 months') as Long_Term_Debt_pf_8M,
	sum(Debtpf) filter (where data = data_referencia - interval '9 months') as Long_Term_Debt_pf_9M,
	sum(Debtpf) filter (where data = data_referencia - interval '10 months') as Long_Term_Debt_pf_10M,
	sum(Debtpf) filter (where data = data_referencia - interval '11 months') as Long_Term_Debt_pf_11M,
	sum(Debtpf) filter (where data = data_referencia - interval '12 months') as Long_Term_Debt_pf_12M,
	sum(Debtpf) filter (where data = data_referencia - interval '13 months') as Long_Term_Debt_pf_13M,
	sum(Debtpf) filter (where data = data_referencia - interval '14 months') as Long_Term_Debt_pf_14M,
	sum(Debtpf) filter (where data = data_referencia - interval '15 months') as Long_Term_Debt_pf_15M,
	sum(Debtpf) filter (where data = data_referencia - interval '16 months') as Long_Term_Debt_pf_16M,
	sum(Debtpf) filter (where data = data_referencia - interval '17 months') as Long_Term_Debt_pf_17M,
	sum(Debtpf) filter (where data = data_referencia - interval '18 months') as Long_Term_Debt_pf_18M,
	sum(Debtpf) filter (where data = data_referencia - interval '19 months') as Long_Term_Debt_pf_19M,
	sum(Debtpf) filter (where data = data_referencia - interval '20 months') as Long_Term_Debt_pf_20M,
	sum(Debtpf) filter (where data = data_referencia - interval '21 months') as Long_Term_Debt_pf_21M,
	sum(Debtpf) filter (where data = data_referencia - interval '22 months') as Long_Term_Debt_pf_22M,
	sum(Debtpf) filter (where data = data_referencia - interval '23 months') as Long_Term_Debt_pf_23M,
	sum(Debtpf) filter (where data <= data_referencia) as Ever_long_term_debt_pf,
	max(Debtpf) filter (where data <= data_referencia) as Max_Long_Term_Debt_pf,
	sum(Vencidopf) filter (where data = data_referencia) as Overdue_pf_Curr,
	extract(year from age(min(data_referencia), max(data) filter (where data <= data_referencia and Vencidopf > 0)))*12 + extract(month from age(min(data_referencia), max(data) filter (where data <= data_referencia and Vencidopf > 0))) as Months_Since_Last_Overdue_pf,
	count(Vencidopf) filter (where data <= data_referencia and Vencidopf > 0) as Months_Overdue_pf,
	max(Vencidopf) filter (where data <= data_referencia) as Max_Overdue_pf,
	sum(Prejuizopf) filter (where data = data_referencia) as Default_pf_Curr,
	extract(year from age(min(data_referencia), max(data) filter (where data <= data_referencia and Prejuizopf > 0)))*12 + extract(month from age(min(data_referencia), max(data) filter (where data <= data_referencia and Prejuizopf > 0)))  as Months_Since_Last_Default_pf,
	count(Prejuizopf) filter (where data <= data_referencia and Prejuizopf > 0) as Months_Default_pf,
	max(Prejuizopf) filter (where data <= data_referencia) as Max_Default_pf,
	count(*) filter (where data <= data_referencia) as Qtd_meses_escopo_pf,
	sum(LimCredpf) filter (where data = data_referencia) as Lim_Cred_pf_Curr,
	sum(LimCredpf) filter (where data = data_referencia - interval '1 month') as Lim_Cred_pf_1M,
	sum(LimCredpf) filter (where data = data_referencia - interval '2 months') as Lim_Cred_pf_2M,
	sum(LimCredpf) filter (where data = data_referencia - interval '3 months') as Lim_Cred_pf_3M,
	sum(LimCredpf) filter (where data = data_referencia - interval '4 months') as Lim_Cred_pf_4M,
	sum(LimCredpf) filter (where data = data_referencia - interval '5 months') as Lim_Cred_pf_5M,
	sum(LimCredpf) filter (where data = data_referencia - interval '6 months') as Lim_Cred_pf_6M,
	sum(LimCredpf) filter (where data = data_referencia - interval '7 months') as Lim_Cred_pf_7M,
	sum(LimCredpf) filter (where data = data_referencia - interval '8 months') as Lim_Cred_pf_8M,
	sum(LimCredpf) filter (where data = data_referencia - interval '9 months') as Lim_Cred_pf_9M,
	sum(LimCredpf) filter (where data = data_referencia - interval '10 months') as Lim_Cred_pf_10M,
	sum(LimCredpf) filter (where data = data_referencia - interval '11 months') as Lim_Cred_pf_11M,
	sum(LimCredpf) filter (where data = data_referencia - interval '12 months') as Lim_Cred_pf_12M,
	sum(LimCredpf) filter (where data = data_referencia - interval '13 months') as Lim_Cred_pf_13M,
	sum(LimCredpf) filter (where data = data_referencia - interval '14 months') as Lim_Cred_pf_14M,
	sum(LimCredpf) filter (where data = data_referencia - interval '15 months') as Lim_Cred_pf_15M,
	sum(LimCredpf) filter (where data = data_referencia - interval '16 months') as Lim_Cred_pf_16M,
	sum(LimCredpf) filter (where data = data_referencia - interval '17 months') as Lim_Cred_pf_17M,
	sum(LimCredpf) filter (where data = data_referencia - interval '18 months') as Lim_Cred_pf_18M,
	sum(LimCredpf) filter (where data = data_referencia - interval '19 months') as Lim_Cred_pf_19M,
	sum(LimCredpf) filter (where data = data_referencia - interval '20 months') as Lim_Cred_pf_20M,
	sum(LimCredpf) filter (where data = data_referencia - interval '21 months') as Lim_Cred_pf_21M,
	sum(LimCredpf) filter (where data = data_referencia - interval '22 months') as Lim_Cred_pf_22M,
	sum(LimCredpf) filter (where data = data_referencia - interval '23 months') as Lim_Cred_pf_23M,
	sum(LimCredpf) filter (where data <= data_referencia) as Ever_Lim_Cred_pf,
	max(LimCredpf) filter (where data <= data_referencia) as Max_Lim_Cred_pf
	from(select direct_prospect_id,
			case when extract(day from loan_date) >= 16 then case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end else case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end end as data_referencia,
			to_date(jsonb_array_elements(cc.data->'historico'->'Carteira de Crédito')->>'data','mm/yyyy') as data,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Carteira de Crédito')->>'valor','-','0'),'.',''),',','.')::float Debtpf,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Vencido')->>'valor','-','0'),'.',''),',','.')::float Vencidopf,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Prejuízo')->>'valor','-','0'),'.',''),',','.')::float Prejuizopf,
			replace(replace(replace(jsonb_array_elements(cc.data->'historico'->'Limite de Crédito')->>'valor','-','0'),'.',''),',','.')::float LimCredpf,
			replace(replace(case when position('Quantidade de Operações' in cc.raw) > 0 then substring(cc.raw,position('Quantidade de Operações' in cc.raw)+length('Quantidade de Operações</div></td><td class="fu3" width="50%"><b>'),3) else null end,'<',''),'/','')::int as Num_Ops_pf,
			replace(replace(case when position('Quantidade de IFs em que o Cliente possui Operações' in cc.raw) > 0 then substring(cc.raw,position('Quantidade de IFs em que o Cliente possui Operações' in cc.raw)+
				length('Quantidade de IFs em que o Cliente possui Operações</div></td><td class="fu3" width="50%"><b>'),3) else null end,'<',''),'/','')::int as Num_FIs_pf,
			extract(year from age(loan_date,to_date(replace(replace(replace(case when position('Data de Início de Relacionamento com a IF' in cc.raw) > 0 then substring(cc.raw,position('Data de Início de Relacionamento com a IF' in cc.raw)+
				length('Data de Início de Relacionamento com a IF</div></td><td class="fu3" width="50%"><b>'),10) else null end,'<',''),'/',''),'-','0'),'ddmmyyyy')))::int as First_Relation_FI_pf
		from credito_coleta cc
		join(select dp.direct_prospect_id, lr.loan_date,cpf,
				max(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id) filter (where to_date(left(data->>'criado_em',10),'yyyy-mm-dd') <= lr.loan_date and (cc.data->'historico' is not null or cc.data->'erro' is not null)) as max_date, 
				min(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id) filter (where data->'historico' is not null or cc.data->'erro' is not null) as min_date
			from credito_coleta cc
			join direct_prospects dp on cc.documento = dp.cpf
			join offers o on o.direct_prospect_id = dp.direct_prospect_id
			join loan_requests lr on lr.offer_id = o.offer_id 
			where documento_tipo = 'CPF' and tipo = 'SCR' and lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')
			group by 1,2,3) as t1 on t1.cpf = cc.documento and case when max_date is null then to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id = min_date else to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id = max_date end) as t2
	group by 1,2,3,4) as t3) as SCRHistPF on SCRHistPF.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SCR DIREITOS CREDITORIOS PJ
left join (select direct_prospect_id,
	Invoice_Disc_PJ_Curr::float,
	Invoice_Disc_PJ_1M::float,
	Invoice_Disc_PJ_2M::float,
	Invoice_Disc_PJ_3M::float,
	Invoice_Disc_PJ_4M::float,
	Invoice_Disc_PJ_5M::float,
	Invoice_Disc_PJ_6M::float,
	Invoice_Disc_PJ_7M::float,
	Invoice_Disc_PJ_8M::float,
	Invoice_Disc_PJ_9M::float,
	Invoice_Disc_PJ_10M::float,
	Invoice_Disc_PJ_11M::float,
	max_invoice_disc::float,
	Ever_invoice_disc::float,
	case when invoice_disc_pj_10M < invoice_disc_pj_11M then 1 else 0 end +
	case when invoice_disc_pj_9M < invoice_disc_pj_10M then 1 else 0 end +
	case when invoice_disc_pj_8M < invoice_disc_pj_9M then 1 else 0 end +
	case when invoice_disc_pj_7M < invoice_disc_pj_8M then 1 else 0 end +
	case when invoice_disc_pj_6M < invoice_disc_pj_7M then 1 else 0 end +
	case when invoice_disc_pj_5M < invoice_disc_pj_6M then 1 else 0 end +
	case when invoice_disc_pj_4M < invoice_disc_pj_5M then 1 else 0 end +
	case when invoice_disc_pj_3M < invoice_disc_pj_4M then 1 else 0 end +
	case when invoice_disc_pj_2M < invoice_disc_pj_3M then 1 else 0 end +
	case when invoice_disc_pj_1M < invoice_disc_pj_2M then 1 else 0 end +
	case when invoice_disc_pj_Curr < invoice_disc_pj_1M then 1 else 0 end::float as Meses_Reducao_invoice_disc,
	case when invoice_disc_pj_10M > invoice_disc_pj_11M then 1 else 0 end +
	case when invoice_disc_pj_9M > invoice_disc_pj_10M then 1 else 0 end +
	case when invoice_disc_pj_8M > invoice_disc_pj_9M then 1 else 0 end +
	case when invoice_disc_pj_7M > invoice_disc_pj_8M then 1 else 0 end +
	case when invoice_disc_pj_6M > invoice_disc_pj_7M then 1 else 0 end +
	case when invoice_disc_pj_5M > invoice_disc_pj_6M then 1 else 0 end +
	case when invoice_disc_pj_4M > invoice_disc_pj_5M then 1 else 0 end +
	case when invoice_disc_pj_3M > invoice_disc_pj_4M then 1 else 0 end +
	case when invoice_disc_pj_2M > invoice_disc_pj_3M then 1 else 0 end +
	case when invoice_disc_pj_1M > invoice_disc_pj_2M then 1 else 0 end +
	case when invoice_disc_pj_Curr > invoice_disc_pj_1M then 1 else 0 end::float as Meses_Aumento_invoice_disc,
	case when invoice_disc_pj_1M is null then null else
	case when invoice_disc_pj_Curr >= invoice_disc_pj_1M or invoice_disc_pj_1M is null then 0 else
	case when invoice_disc_pj_1M >= invoice_disc_pj_2M or invoice_disc_pj_2M is null then 1 else
	case when invoice_disc_pj_2M >= invoice_disc_pj_3M or invoice_disc_pj_3M is null then 2 else
	case when invoice_disc_pj_3M >= invoice_disc_pj_4M or invoice_disc_pj_4M is null then 3 else
	case when invoice_disc_pj_4M >= invoice_disc_pj_5M or invoice_disc_pj_5M is null then 4 else
	case when invoice_disc_pj_5M >= invoice_disc_pj_6M or invoice_disc_pj_6M is null then 5 else
	case when invoice_disc_pj_6M >= invoice_disc_pj_7M or invoice_disc_pj_7M is null then 6 else
	case when invoice_disc_pj_7M >= invoice_disc_pj_8M or invoice_disc_pj_8M is null then 7 else
	case when invoice_disc_pj_8M >= invoice_disc_pj_9M or invoice_disc_pj_9M is null then 8 else
	case when invoice_disc_pj_9M >= invoice_disc_pj_10M or invoice_disc_pj_10M is null then 9 else
	case when invoice_disc_pj_10M >= invoice_disc_pj_11M or invoice_disc_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_invoice_disc,
	case when invoice_disc_pj_1M is null then null else
	case when invoice_disc_pj_Curr <= invoice_disc_pj_1M or invoice_disc_pj_1M is null then 0 else
	case when invoice_disc_pj_1M <= invoice_disc_pj_2M or invoice_disc_pj_2M is null then 1 else
	case when invoice_disc_pj_2M <= invoice_disc_pj_3M or invoice_disc_pj_3M is null then 2 else
	case when invoice_disc_pj_3M <= invoice_disc_pj_4M or invoice_disc_pj_4M is null then 3 else
	case when invoice_disc_pj_4M <= invoice_disc_pj_5M or invoice_disc_pj_5M is null then 4 else
	case when invoice_disc_pj_5M <= invoice_disc_pj_6M or invoice_disc_pj_6M is null then 5 else
	case when invoice_disc_pj_6M <= invoice_disc_pj_7M or invoice_disc_pj_7M is null then 6 else
	case when invoice_disc_pj_7M <= invoice_disc_pj_8M or invoice_disc_pj_8M is null then 7 else
	case when invoice_disc_pj_8M <= invoice_disc_pj_9M or invoice_disc_pj_9M is null then 8 else
	case when invoice_disc_pj_9M <= invoice_disc_pj_10M or invoice_disc_pj_10M is null then 9 else
	case when invoice_disc_pj_10M <= invoice_disc_pj_11M or invoice_disc_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_invoice_disc,
	qtd_meses_invoice_disc::float
from(select direct_prospect_id,
	sum(valor) filter (where to_date = data_referencia) as Invoice_Disc_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months') as Invoice_Disc_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months') as Invoice_Disc_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months') as Invoice_Disc_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months') as Invoice_Disc_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months') as Invoice_Disc_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months') as Invoice_Disc_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months') as Invoice_Disc_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months') as Invoice_Disc_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months') as Invoice_Disc_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months') as Invoice_Disc_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months') as Invoice_Disc_PJ_11M,
	max(valor) filter (where to_date <= data_referencia) as max_invoice_disc,
	sum(valor) filter (where to_date <= data_referencia) as Ever_invoice_disc,
	count(*) filter (where to_date <= data_referencia) as qtd_meses_invoice_disc
	from(select direct_prospect_id, to_date(data,'mm/yyyy'),case when valor is null then 0 else valor end as valor,data_referencia
		from (select direct_prospect_id,(jsonb_populate_recordset(null::meu2, serie)).*,data_referencia
			from (select direct_prospect_id,(jsonb_populate_recordset( NULL ::meu, data #> '{por_modalidade}')).*,
					case when extract(day from loan_date) >= 16 then case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end else case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end end as data_referencia
				from credito_coleta cc
				join(select dp.direct_prospect_id, lr.loan_date,cnpj,
						max(to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS')) filter (where to_date(left(cc.data->>'criado_em',10),'yyyy-mm-dd') <= lr.loan_date and cc.data->'historico' is not null) as max_date, 
						min(to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS')) filter (where cc.data->'historico' is not null) as min_date
					from credito_coleta cc
					join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
					join offers o on o.direct_prospect_id = dp.direct_prospect_id
					join loan_requests lr on lr.offer_id = o.offer_id 
					where documento_tipo = 'CNPJ' and tipo = 'SCR' and lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')
					group by 1,2,3) as t1 on left(cc.documento,8) = left(cnpj,8) and case when max_date is null then to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS') = min_date else 
						to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS') = max_date end) as t2
		where nome = 'Direitos Creditórios Descontados') as t3) as t4
		group by 1) as t5) as SCRDirCred on SCRDirCred.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SCR EMPRESTIMOS ROTATIVO PJ
left join (select direct_prospect_id,
	overdraft_PJ_Curr::float,
	overdraft_PJ_1M::float,
	overdraft_PJ_2M::float,
	overdraft_PJ_3M::float,
	overdraft_PJ_4M::float,
	overdraft_PJ_5M::float,
	overdraft_PJ_6M::float,
	overdraft_PJ_7M::float,
	overdraft_PJ_8M::float,
	overdraft_PJ_9M::float,
	overdraft_PJ_10M::float,
	overdraft_PJ_11M::float,
	max_overdraft::float,
	Ever_overdraft::float,
	case when overdraft_pj_10M < overdraft_pj_11M then 1 else 0 end +
	case when overdraft_pj_9M < overdraft_pj_10M then 1 else 0 end +
	case when overdraft_pj_8M < overdraft_pj_9M then 1 else 0 end +
	case when overdraft_pj_7M < overdraft_pj_8M then 1 else 0 end +
	case when overdraft_pj_6M < overdraft_pj_7M then 1 else 0 end +
	case when overdraft_pj_5M < overdraft_pj_6M then 1 else 0 end +
	case when overdraft_pj_4M < overdraft_pj_5M then 1 else 0 end +
	case when overdraft_pj_3M < overdraft_pj_4M then 1 else 0 end +
	case when overdraft_pj_2M < overdraft_pj_3M then 1 else 0 end +
	case when overdraft_pj_1M < overdraft_pj_2M then 1 else 0 end +
	case when overdraft_pj_Curr < overdraft_pj_1M then 1 else 0 end::float as Meses_Reducao_overdraft,
	case when overdraft_pj_10M > overdraft_pj_11M then 1 else 0 end +
	case when overdraft_pj_9M > overdraft_pj_10M then 1 else 0 end +
	case when overdraft_pj_8M > overdraft_pj_9M then 1 else 0 end +
	case when overdraft_pj_7M > overdraft_pj_8M then 1 else 0 end +
	case when overdraft_pj_6M > overdraft_pj_7M then 1 else 0 end +
	case when overdraft_pj_5M > overdraft_pj_6M then 1 else 0 end +
	case when overdraft_pj_4M > overdraft_pj_5M then 1 else 0 end +
	case when overdraft_pj_3M > overdraft_pj_4M then 1 else 0 end +
	case when overdraft_pj_2M > overdraft_pj_3M then 1 else 0 end +
	case when overdraft_pj_1M > overdraft_pj_2M then 1 else 0 end +
	case when overdraft_pj_Curr > overdraft_pj_1M then 1 else 0 end::float as Meses_Aumento_overdraft,
	case when overdraft_pj_1M is null then null else
	case when overdraft_pj_Curr >= overdraft_pj_1M or overdraft_pj_1M is null then 0 else
	case when overdraft_pj_1M >= overdraft_pj_2M or overdraft_pj_2M is null then 1 else
	case when overdraft_pj_2M >= overdraft_pj_3M or overdraft_pj_3M is null then 2 else
	case when overdraft_pj_3M >= overdraft_pj_4M or overdraft_pj_4M is null then 3 else
	case when overdraft_pj_4M >= overdraft_pj_5M or overdraft_pj_5M is null then 4 else
	case when overdraft_pj_5M >= overdraft_pj_6M or overdraft_pj_6M is null then 5 else
	case when overdraft_pj_6M >= overdraft_pj_7M or overdraft_pj_7M is null then 6 else
	case when overdraft_pj_7M >= overdraft_pj_8M or overdraft_pj_8M is null then 7 else
	case when overdraft_pj_8M >= overdraft_pj_9M or overdraft_pj_9M is null then 8 else
	case when overdraft_pj_9M >= overdraft_pj_10M or overdraft_pj_10M is null then 9 else
	case when overdraft_pj_10M >= overdraft_pj_11M or overdraft_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_overdraft,
	case when overdraft_pj_1M is null then null else
	case when overdraft_pj_Curr <= overdraft_pj_1M or overdraft_pj_1M is null then 0 else
	case when overdraft_pj_1M <= overdraft_pj_2M or overdraft_pj_2M is null then 1 else
	case when overdraft_pj_2M <= overdraft_pj_3M or overdraft_pj_3M is null then 2 else
	case when overdraft_pj_3M <= overdraft_pj_4M or overdraft_pj_4M is null then 3 else
	case when overdraft_pj_4M <= overdraft_pj_5M or overdraft_pj_5M is null then 4 else
	case when overdraft_pj_5M <= overdraft_pj_6M or overdraft_pj_6M is null then 5 else
	case when overdraft_pj_6M <= overdraft_pj_7M or overdraft_pj_7M is null then 6 else
	case when overdraft_pj_7M <= overdraft_pj_8M or overdraft_pj_8M is null then 7 else
	case when overdraft_pj_8M <= overdraft_pj_9M or overdraft_pj_9M is null then 8 else
	case when overdraft_pj_9M <= overdraft_pj_10M or overdraft_pj_10M is null then 9 else
	case when overdraft_pj_10M <= overdraft_pj_11M or overdraft_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_overdraft,
	qtd_meses_overdraft::float
from(select direct_prospect_id,
	sum(valor) filter (where to_date = data_referencia) as overdraft_PJ_Curr,
	sum(valor) filter (where to_date = data_referencia - interval '1 months') as overdraft_PJ_1M,
	sum(valor) filter (where to_date = data_referencia - interval '2 months') as overdraft_PJ_2M,
	sum(valor) filter (where to_date = data_referencia - interval '3 months') as overdraft_PJ_3M,
	sum(valor) filter (where to_date = data_referencia - interval '4 months') as overdraft_PJ_4M,
	sum(valor) filter (where to_date = data_referencia - interval '5 months') as overdraft_PJ_5M,
	sum(valor) filter (where to_date = data_referencia - interval '6 months') as overdraft_PJ_6M,
	sum(valor) filter (where to_date = data_referencia - interval '7 months') as overdraft_PJ_7M,
	sum(valor) filter (where to_date = data_referencia - interval '8 months') as overdraft_PJ_8M,
	sum(valor) filter (where to_date = data_referencia - interval '9 months') as overdraft_PJ_9M,
	sum(valor) filter (where to_date = data_referencia - interval '10 months') as overdraft_PJ_10M,
	sum(valor) filter (where to_date = data_referencia - interval '11 months') as overdraft_PJ_11M,
	max(valor) filter (where to_date <= data_referencia) as max_overdraft,
	sum(valor) filter (where to_date <= data_referencia) as Ever_overdraft,
	count(*) filter (where to_date <= data_referencia) as qtd_meses_overdraft
	from(select direct_prospect_id, data_referencia, Lv1, to_date(data,'mm/yyyy'),sum(case when valor is null then 0 else valor end) as valor from (
		select direct_prospect_id,data_referencia, Lv1, nome as Lv2, (jsonb_populate_recordset(null::meu2, serie)).* from (	
		select direct_prospect_id,data_referencia, nome as Lv1, (jsonb_populate_recordset(null::meu, detalhes)).* from (
		select direct_prospect_id, (jsonb_populate_recordset( NULL ::lucas, data #> '{por_modalidade}')).*,
			case when extract(day from loan_date) >= 16 then case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end else case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end end as data_referencia
		from credito_coleta cc
		join(select dp.direct_prospect_id, lr.loan_date,cnpj,
				max(to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS')) filter (where to_date(left(cc.data->>'criado_em',10),'yyyy-mm-dd') <= lr.loan_date and cc.data->'historico' is not null) as max_date, 
				min(to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS')) filter (where cc.data->'historico' is not null) as min_date
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join offers o on o.direct_prospect_id = dp.direct_prospect_id
			join loan_requests lr on lr.offer_id = o.offer_id 
			where documento_tipo = 'CNPJ' and tipo = 'SCR' and lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')
			group by 1,2,3) as t1 on left(cc.documento,8) = left(cnpj,8) and case when max_date is null then to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS') = min_date else 
				to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS') = max_date end) as t2) as t3) as t4
		where lv1 = 'Empréstimos' and (lv2 = 'Cheque Especial' or lv2 = 'Conta Garantida')
		group by 1,2,3,4) as t5
	group by 1) as t6) as SCREmprest on SCREmprest.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SCR DIVIDA CONCORRENTE PF
left join(select direct_prospect_id, 
	qtd_meses_competitive_debt_pf::float,
	divida_atual::float as divida_curto_prazo_pf,
	max_competitive_debt_pf::float,
	competitive_debt_pf_Curr::float,
	competitive_debt_pf_1m::float,
	competitive_debt_pf_2m::float,
	competitive_debt_pf_3m::float,
	competitive_debt_pf_4m::float,
	competitive_debt_pf_5m::float,
	competitive_debt_pf_6m::float,
	competitive_debt_pf_7m::float,
	competitive_debt_pf_8m::float,
	competitive_debt_pf_9m::float,
	competitive_debt_pf_10m::float,
	competitive_debt_pf_11m::float,
	Ever_competitive_debt_pf::float,
	case when competitive_debt_pf_10M < competitive_debt_pf_11M then 1 else 0 end +
	case when competitive_debt_pf_9M < competitive_debt_pf_10M then 1 else 0 end +
	case when competitive_debt_pf_8M < competitive_debt_pf_9M then 1 else 0 end +
	case when competitive_debt_pf_7M < competitive_debt_pf_8M then 1 else 0 end +
	case when competitive_debt_pf_6M < competitive_debt_pf_7M then 1 else 0 end +
	case when competitive_debt_pf_5M < competitive_debt_pf_6M then 1 else 0 end +
	case when competitive_debt_pf_4M < competitive_debt_pf_5M then 1 else 0 end +
	case when competitive_debt_pf_3M < competitive_debt_pf_4M then 1 else 0 end +
	case when competitive_debt_pf_2M < competitive_debt_pf_3M then 1 else 0 end +
	case when competitive_debt_pf_1M < competitive_debt_pf_2M then 1 else 0 end +
	case when competitive_debt_pf_Curr < competitive_debt_pf_1M then 1 else 0 end::float as Meses_Reducao_competitive_debt_pf,
	case when competitive_debt_pf_10M > competitive_debt_pf_11M then 1 else 0 end +
	case when competitive_debt_pf_9M > competitive_debt_pf_10M then 1 else 0 end +
	case when competitive_debt_pf_8M > competitive_debt_pf_9M then 1 else 0 end +
	case when competitive_debt_pf_7M > competitive_debt_pf_8M then 1 else 0 end +
	case when competitive_debt_pf_6M > competitive_debt_pf_7M then 1 else 0 end +
	case when competitive_debt_pf_5M > competitive_debt_pf_6M then 1 else 0 end +
	case when competitive_debt_pf_4M > competitive_debt_pf_5M then 1 else 0 end +
	case when competitive_debt_pf_3M > competitive_debt_pf_4M then 1 else 0 end +
	case when competitive_debt_pf_2M > competitive_debt_pf_3M then 1 else 0 end +
	case when competitive_debt_pf_1M > competitive_debt_pf_2M then 1 else 0 end +
	case when competitive_debt_pf_Curr > competitive_debt_pf_1M then 1 else 0 end::float as Meses_Aumento_competitive_debt_pf,
	case when competitive_debt_pf_1M is null then null else
	case when competitive_debt_pf_Curr >= competitive_debt_pf_1M or competitive_debt_pf_1M is null then 0 else
	case when competitive_debt_pf_1M >= competitive_debt_pf_2M or competitive_debt_pf_2M is null then 1 else
	case when competitive_debt_pf_2M >= competitive_debt_pf_3M or competitive_debt_pf_3M is null then 2 else
	case when competitive_debt_pf_3M >= competitive_debt_pf_4M or competitive_debt_pf_4M is null then 3 else
	case when competitive_debt_pf_4M >= competitive_debt_pf_5M or competitive_debt_pf_5M is null then 4 else
	case when competitive_debt_pf_5M >= competitive_debt_pf_6M or competitive_debt_pf_6M is null then 5 else
	case when competitive_debt_pf_6M >= competitive_debt_pf_7M or competitive_debt_pf_7M is null then 6 else
	case when competitive_debt_pf_7M >= competitive_debt_pf_8M or competitive_debt_pf_8M is null then 7 else
	case when competitive_debt_pf_8M >= competitive_debt_pf_9M or competitive_debt_pf_9M is null then 8 else
	case when competitive_debt_pf_9M >= competitive_debt_pf_10M or competitive_debt_pf_10M is null then 9 else
	case when competitive_debt_pf_10M >= competitive_debt_pf_11M or competitive_debt_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_competitive_debt_pf,
	case when competitive_debt_pf_1M is null then null else
	case when competitive_debt_pf_Curr <= competitive_debt_pf_1M or competitive_debt_pf_1M is null then 0 else
	case when competitive_debt_pf_1M <= competitive_debt_pf_2M or competitive_debt_pf_2M is null then 1 else
	case when competitive_debt_pf_2M <= competitive_debt_pf_3M or competitive_debt_pf_3M is null then 2 else
	case when competitive_debt_pf_3M <= competitive_debt_pf_4M or competitive_debt_pf_4M is null then 3 else
	case when competitive_debt_pf_4M <= competitive_debt_pf_5M or competitive_debt_pf_5M is null then 4 else
	case when competitive_debt_pf_5M <= competitive_debt_pf_6M or competitive_debt_pf_6M is null then 5 else
	case when competitive_debt_pf_6M <= competitive_debt_pf_7M or competitive_debt_pf_7M is null then 6 else
	case when competitive_debt_pf_7M <= competitive_debt_pf_8M or competitive_debt_pf_8M is null then 7 else
	case when competitive_debt_pf_8M <= competitive_debt_pf_9M or competitive_debt_pf_9M is null then 8 else
	case when competitive_debt_pf_9M <= competitive_debt_pf_10M or competitive_debt_pf_10M is null then 9 else
	case when competitive_debt_pf_10M <= competitive_debt_pf_11M or competitive_debt_pf_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_competitive_debt_pf,
	vehicle_debt_pf_Curr::float,
	vehicle_debt_pf_1M::float,
	vehicle_debt_pf_2m::float,
	vehicle_debt_pf_3m::float,
	vehicle_debt_pf_4m::float,
	vehicle_debt_pf_5m::float,
	Ever_vehicle_debt_pf
from(select direct_prospect_id, 
		divida_atual,
		count(*) filter (where data <= data_referencia) as qtd_meses_competitive_debt_pf,
		sum(financiamento_carro) filter (where data = data_referencia) as vehicle_debt_pf_Curr,
		sum(financiamento_carro) filter (where data = data_referencia - interval '1 month') as vehicle_debt_pf_1M,
		sum(financiamento_carro) filter (where data = data_referencia - interval '2 months') as vehicle_debt_pf_2m,
		sum(financiamento_carro) filter (where data = data_referencia - interval '3 months') as vehicle_debt_pf_3m,
		sum(financiamento_carro) filter (where data = data_referencia - interval '4 months') as vehicle_debt_pf_4m,
		sum(financiamento_carro) filter (where data = data_referencia - interval '5 months') as vehicle_debt_pf_5m,
		sum(financiamento_carro) filter (where data = data_referencia - interval '6 months') as vehicle_debt_pf_6m,
		sum(financiamento_carro) filter (where data = data_referencia - interval '7 months') as vehicle_debt_pf_7m,
		sum(financiamento_carro) filter (where data = data_referencia - interval '8 months') as vehicle_debt_pf_8m,
		sum(financiamento_carro) filter (where data = data_referencia - interval '9 months') as vehicle_debt_pf_9m,
		sum(financiamento_carro) filter (where data = data_referencia - interval '10 months') as vehicle_debt_pf_10m,
		sum(financiamento_carro) filter (where data = data_referencia - interval '11 months') as vehicle_debt_pf_11m,
		sum(financiamento_carro) filter (where data <= data_referencia) as Ever_vehicle_debt_pf,		
		max(divida_concorrente_pf) filter (where data <= data_referencia) as max_competitive_debt_pf,
		sum(divida_concorrente_pf) filter (where data = data_referencia) as competitive_debt_pf_Curr,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '1 month') as competitive_debt_pf_1M,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '2 months') as competitive_debt_pf_2m,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '3 months') as competitive_debt_pf_3m,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '4 months') as competitive_debt_pf_4m,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '5 months') as competitive_debt_pf_5m,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '6 months') as competitive_debt_pf_6m,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '7 months') as competitive_debt_pf_7m,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '8 months') as competitive_debt_pf_8m,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '9 months') as competitive_debt_pf_9m,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '10 months') as competitive_debt_pf_10m,
		sum(divida_concorrente_pf) filter (where data = data_referencia - interval '11 months') as competitive_debt_pf_11m,
		sum(divida_concorrente_pf) filter (where data <= data_referencia) as Ever_competitive_debt_pf
		from(select direct_prospect_id,data_referencia,to_date as data,divida_atual,sum(valor) filter (where lv2 = 'Aquisição de Bens  Veículos Automotores') as financiamento_carro,
				sum(valor) filter (where lv1 not in ('Financiamentos Imobiliários','Direitos Creditórios Descontados','Adiantamentos a Depositantes','Outros Créditos') and lv1 not like '%(%' and lv2 not in('Aquisição de Bens  Veículos Automotores','Aquisição de Bens  Outros Bens')) as divida_concorrente_pf
			from(select direct_prospect_id, data_referencia,divida_atual,Lv1, Lv2, to_date(data,'mm/yyyy'), case when valor is null then 0 else valor end::float
				from(select direct_prospect_id, data_referencia,divida_atual,Lv1, nome as Lv2, (jsonb_populate_recordset(null::meu2, serie)).*
					from (select direct_prospect_id, data_referencia,case when data_base = data_referencia then divida_atual else null end as divida_atual,nome as Lv1, (jsonb_populate_recordset(null::meu, detalhes)).* 
						from (select direct_prospect_id, (data->'indicadores'->>'dividaAtual')::float as divida_atual, to_date(data->'data_base'->>'Data-Base','mm/yyyy') as data_base,data_referencia,(jsonb_populate_recordset( NULL ::lucas, data #> '{por_modalidade}')).*
						from (select direct_prospect_id, data,case when extract(day from loan_date) >= 16 then case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end else case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end end as data_referencia
							from credito_coleta cc
							join(select dp.direct_prospect_id, lr.loan_date,cpf,
									max(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id) filter (where to_date(left(data->>'criado_em',10),'yyyy-mm-dd') <= lr.loan_date and (cc.data->'historico' is not null or cc.data->'erro' is not null)) as max_date, 
									min(to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id) filter (where data->'historico' is not null or cc.data->'erro' is not null) as min_date
								from credito_coleta cc
								join direct_prospects dp on cc.documento = dp.cpf
								join offers o on o.direct_prospect_id = dp.direct_prospect_id
								join loan_requests lr on lr.offer_id = o.offer_id 
								where documento_tipo = 'CPF' and tipo = 'SCR' and lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')
								group by 1,2,3) as t1 on t1.cpf = cc.documento and case when max_date is null then to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id = min_date else to_timestamp(replace(data->>'criado_em','T',' '),'yyyy-mm-dd HH24:MI:SS:MS') + interval '1 millisecond' * id = max_date end) as t2) as t3) as t4) as t5) as t6
		group by 1,2,3,4) as t7
	group by 1,2) as t8) as SCRDivnaoconc_pf on SCRDivnaoconc_pf.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SCR DIVIDA CONCORRENTE PJ
left join(select direct_prospect_id, 
	divida_curto_prazo::float,
	max_competitive_debt::float,
	qtd_meses_competitive_debt::float,
	competitive_debt_pj_Curr::float,
	competitive_debt_pj_1m::float,
	competitive_debt_pj_2m::float,
	competitive_debt_pj_3m::float,
	competitive_debt_pj_4m::float,
	competitive_debt_pj_5m::float,
	competitive_debt_pj_6m::float,
	competitive_debt_pj_7m::float,
	competitive_debt_pj_8m::float,
	competitive_debt_pj_9m::float,
	competitive_debt_pj_10m::float,
	competitive_debt_pj_11m::float,
	Ever_competitive_debt::float,
	case when competitive_debt_pj_10M < competitive_debt_pj_11M then 1 else 0 end +
	case when competitive_debt_pj_9M < competitive_debt_pj_10M then 1 else 0 end +
	case when competitive_debt_pj_8M < competitive_debt_pj_9M then 1 else 0 end +
	case when competitive_debt_pj_7M < competitive_debt_pj_8M then 1 else 0 end +
	case when competitive_debt_pj_6M < competitive_debt_pj_7M then 1 else 0 end +
	case when competitive_debt_pj_5M < competitive_debt_pj_6M then 1 else 0 end +
	case when competitive_debt_pj_4M < competitive_debt_pj_5M then 1 else 0 end +
	case when competitive_debt_pj_3M < competitive_debt_pj_4M then 1 else 0 end +
	case when competitive_debt_pj_2M < competitive_debt_pj_3M then 1 else 0 end +
	case when competitive_debt_pj_1M < competitive_debt_pj_2M then 1 else 0 end +
	case when competitive_debt_pj_Curr < competitive_debt_pj_1M then 1 else 0 end::float as Meses_Reducao_competitive_debt,
	case when competitive_debt_pj_10M > competitive_debt_pj_11M then 1 else 0 end +
	case when competitive_debt_pj_9M > competitive_debt_pj_10M then 1 else 0 end +
	case when competitive_debt_pj_8M > competitive_debt_pj_9M then 1 else 0 end +
	case when competitive_debt_pj_7M > competitive_debt_pj_8M then 1 else 0 end +
	case when competitive_debt_pj_6M > competitive_debt_pj_7M then 1 else 0 end +
	case when competitive_debt_pj_5M > competitive_debt_pj_6M then 1 else 0 end +
	case when competitive_debt_pj_4M > competitive_debt_pj_5M then 1 else 0 end +
	case when competitive_debt_pj_3M > competitive_debt_pj_4M then 1 else 0 end +
	case when competitive_debt_pj_2M > competitive_debt_pj_3M then 1 else 0 end +
	case when competitive_debt_pj_1M > competitive_debt_pj_2M then 1 else 0 end +
	case when competitive_debt_pj_Curr > competitive_debt_pj_1M then 1 else 0 end::float as Meses_Aumento_competitive_debt,
	case when competitive_debt_pj_1M is null then null else
	case when competitive_debt_pj_Curr >= competitive_debt_pj_1M or competitive_debt_pj_1M is null then 0 else
	case when competitive_debt_pj_1M >= competitive_debt_pj_2M or competitive_debt_pj_2M is null then 1 else
	case when competitive_debt_pj_2M >= competitive_debt_pj_3M or competitive_debt_pj_3M is null then 2 else
	case when competitive_debt_pj_3M >= competitive_debt_pj_4M or competitive_debt_pj_4M is null then 3 else
	case when competitive_debt_pj_4M >= competitive_debt_pj_5M or competitive_debt_pj_5M is null then 4 else
	case when competitive_debt_pj_5M >= competitive_debt_pj_6M or competitive_debt_pj_6M is null then 5 else
	case when competitive_debt_pj_6M >= competitive_debt_pj_7M or competitive_debt_pj_7M is null then 6 else
	case when competitive_debt_pj_7M >= competitive_debt_pj_8M or competitive_debt_pj_8M is null then 7 else
	case when competitive_debt_pj_8M >= competitive_debt_pj_9M or competitive_debt_pj_9M is null then 8 else
	case when competitive_debt_pj_9M >= competitive_debt_pj_10M or competitive_debt_pj_10M is null then 9 else
	case when competitive_debt_pj_10M >= competitive_debt_pj_11M or competitive_debt_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_sth_competitive_debt,
	case when competitive_debt_pj_1M is null then null else
	case when competitive_debt_pj_Curr <= competitive_debt_pj_1M or competitive_debt_pj_1M is null then 0 else
	case when competitive_debt_pj_1M <= competitive_debt_pj_2M or competitive_debt_pj_2M is null then 1 else
	case when competitive_debt_pj_2M <= competitive_debt_pj_3M or competitive_debt_pj_3M is null then 2 else
	case when competitive_debt_pj_3M <= competitive_debt_pj_4M or competitive_debt_pj_4M is null then 3 else
	case when competitive_debt_pj_4M <= competitive_debt_pj_5M or competitive_debt_pj_5M is null then 4 else
	case when competitive_debt_pj_5M <= competitive_debt_pj_6M or competitive_debt_pj_6M is null then 5 else
	case when competitive_debt_pj_6M <= competitive_debt_pj_7M or competitive_debt_pj_7M is null then 6 else
	case when competitive_debt_pj_7M <= competitive_debt_pj_8M or competitive_debt_pj_8M is null then 7 else
	case when competitive_debt_pj_8M <= competitive_debt_pj_9M or competitive_debt_pj_9M is null then 8 else
	case when competitive_debt_pj_9M <= competitive_debt_pj_10M or competitive_debt_pj_10M is null then 9 else
	case when competitive_debt_pj_10M <= competitive_debt_pj_11M or competitive_debt_pj_11M is null then 10 else 11 end end end end end end end end end end end end::float as months_hth_competitive_debt
from(select direct_prospect_id, 
	divida_curto_prazo,
	max(competitive_debt) filter (where data <= data_referencia) as max_competitive_debt,
	sum(competitive_debt) filter (where data = data_referencia) as competitive_debt_pj_Curr,
	sum(competitive_debt) filter (where data = data_referencia - interval '1 month') as competitive_debt_pj_1M,
	sum(competitive_debt) filter (where data = data_referencia - interval '2 months') as competitive_debt_pj_2m,
	sum(competitive_debt) filter (where data = data_referencia - interval '3 months') as competitive_debt_pj_3m,
	sum(competitive_debt) filter (where data = data_referencia - interval '4 months') as competitive_debt_pj_4m,
	sum(competitive_debt) filter (where data = data_referencia - interval '5 months') as competitive_debt_pj_5m,
	sum(competitive_debt) filter (where data = data_referencia - interval '6 months') as competitive_debt_pj_6m,
	sum(competitive_debt) filter (where data = data_referencia - interval '7 months') as competitive_debt_pj_7m,
	sum(competitive_debt) filter (where data = data_referencia - interval '8 months') as competitive_debt_pj_8m,
	sum(competitive_debt) filter (where data = data_referencia - interval '9 months') as competitive_debt_pj_9m,
	sum(competitive_debt) filter (where data = data_referencia - interval '10 months') as competitive_debt_pj_10m,
	sum(competitive_debt) filter (where data = data_referencia - interval '11 months') as competitive_debt_pj_11m,
	sum(competitive_debt) filter (where data <= data_referencia) as Ever_competitive_debt,
	count(*) filter (where data <= data_referencia) as qtd_meses_competitive_debt
from(select direct_prospect_id, case when extract(day from loan_date) >= 16 then case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '1 month' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end else case when to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') > to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') then to_date(to_char(loan_date - interval '2 months' ,'Mon/yy'),'Mon/yy') else to_date(cc.data->'historico'->'Carteira de Crédito'->0->>'data','mm-yyyy') end end as data_referencia,
		to_date(jsonb_array_elements(data->'por_modalidade'->0->'serie')->>'data','mm/yyyy') as data,
		(jsonb_array_elements(cc.data->'indicadores'->'dividasMes'))::text::float as competitive_debt,
		(cc.data->'indicadores'->>'dividaAtual')::float as divida_curto_prazo
	from credito_coleta cc
	join(select dp.direct_prospect_id, lr.loan_date,cnpj,
			max(to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS')) filter (where to_date(left(cc.data->>'criado_em',10),'yyyy-mm-dd') <= lr.loan_date and cc.data->'historico' is not null) as max_date, 
			min(to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS')) filter (where cc.data->'historico' is not null) as min_date
		from credito_coleta cc
		join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
		join offers o on o.direct_prospect_id = dp.direct_prospect_id
		join loan_requests lr on lr.offer_id = o.offer_id 
		where documento_tipo = 'CNPJ' and tipo = 'SCR' and lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')
		group by 1,2,3) as t1 on left(cc.documento,8) = left(cnpj,8) and case when max_date is null then to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS') = min_date else 
			to_timestamp(left(data->>'criado_em',10)::text || ' ' || substring(data->>'criado_em',position('T' in data->>'criado_em')+1,8)::text,'yyyy-mm-dd HH24:MI:SS') = max_date end) as t2
	group by 1,2) as t3) as SCRDivnaoconc on SCRDivnaoconc.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SERASA SOCIOS
left join (select direct_prospect_id, count(*) filter (where cpf = cpf_socios) is_shareholder,count(*) count_socios
	from(select direct_prospect_id, cpf, data->'acionistas'->jsonb_object_keys(data->'acionistas')->>'cpf' as cpf_socios
		from credito_coleta cc
		join(select dp.direct_prospect_id, dp.cnpj, dp.cpf, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join offers o on o.direct_prospect_id = dp.direct_prospect_id
			join loan_requests lr on lr.offer_id = o.offer_id 
			where tipo = 'Serasa' and lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')
			group by 1,2) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t2
	group by 1) as SerasaCPF on SerasaCPF.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SERASA ANOTACOES
left join (select dp.direct_prospect_id,
	--Empresa
	case when empresa_divida_valor is null then 0 else empresa_divida_valor end as empresa_divida_valor,
	case when empresa_divida_unit is null then 0 else empresa_divida_unit end as empresa_divida_unit,
	case when empresa_ccf_valor is null then 0 else empresa_ccf_valor end as empresa_ccf_valor,
	case when empresa_ccf_unit is null then 0 else empresa_ccf_unit end as empresa_ccf_unit,
	case when empresa_protesto_valor is null then 0 else empresa_protesto_valor end as empresa_protesto_valor,
	case when empresa_protesto_unit is null then 0 else empresa_protesto_unit end as empresa_protesto_unit,
	case when empresa_acao_valor is null then 0 else empresa_acao_valor end as empresa_acao_valor,
	case when empresa_acao_unit is null then 0 else empresa_acao_unit end as empresa_acao_unit,
	case when empresa_pefin_valor is null then 0 else empresa_pefin_valor end as empresa_pefin_valor,
	case when empresa_pefin_unit is null then 0 else empresa_pefin_unit end as empresa_pefin_unit,
	case when empresa_refin_valor is null then 0 else empresa_refin_valor end as empresa_refin_valor,
	case when empresa_refin_unit is null then 0 else empresa_refin_unit end as empresa_refin_unit,
	case when empresa_spc_valor is null then 0 else empresa_spc_valor end as empresa_spc_valor,
	case when empresa_spc_unit is null then 0 else empresa_spc_unit end as empresa_spc_unit,
	case when empresa_divida_valor is null then 0 else empresa_divida_valor end +
		case when empresa_ccf_valor is null then 0 else empresa_ccf_valor end +
		case when empresa_protesto_valor is null then 0 else empresa_protesto_valor end +
		case when empresa_acao_valor is null then 0 else empresa_acao_valor end +
		case when empresa_pefin_valor is null then 0 else empresa_pefin_valor end +
		case when empresa_refin_valor is null then 0 else empresa_refin_valor end +
		case when empresa_spc_valor is null then 0 else empresa_spc_valor end empresa_total_valor,
	case when empresa_divida_unit is null then 0 else empresa_divida_unit end +
		case when empresa_ccf_unit is null then 0 else empresa_ccf_unit end +
		case when empresa_protesto_unit is null then 0 else empresa_protesto_unit end +
		case when empresa_acao_unit is null then 0 else empresa_acao_unit end +
		case when empresa_pefin_unit is null then 0 else empresa_pefin_unit end +
		case when empresa_refin_unit is null then 0 else empresa_refin_unit end +
		case when empresa_spc_unit is null then 0 else empresa_spc_unit end as empresa_total_unit,
	--Socios
	case when socio_divida_valor is null then 0 else socio_divida_valor end as socio_divida_valor,
	case when socio_divida_unit is null then 0 else socio_divida_unit end as socio_divida_unit,
	case when socio_ccf_valor is null then 0 else socio_ccf_valor end as socio_ccf_valor,
	case when socio_ccf_unit is null then 0 else socio_ccf_unit end as socio_ccf_unit,
	case when socio_protesto_valor is null then 0 else socio_protesto_valor end as socio_protesto_valor,
	case when socio_protesto_unit is null then 0 else socio_protesto_unit end as socio_protesto_unit,
	case when socio_acao_valor is null then 0 else socio_acao_valor end as socio_acao_valor,
	case when socio_acao_unit is null then 0 else socio_acao_unit end as socio_acao_unit,
	case when socio_pefin_valor is null then 0 else socio_pefin_valor end as socio_pefin_valor,
	case when socio_pefin_unit is null then 0 else socio_pefin_unit end as socio_pefin_unit,
	case when socio_refin_valor is null then 0 else socio_refin_valor end as socio_refin_valor,
	case when socio_refin_unit is null then 0 else socio_refin_unit end as socio_refin_unit,
	case when socio_spc_valor is null then 0 else socio_spc_valor end as socio_spc_valor,
	case when socio_spc_unit is null then 0 else socio_spc_unit end as socio_spc_unit,
	case when socio_divida_valor is null then 0 else socio_divida_valor end + 
		case when socio_ccf_valor is null then 0 else socio_ccf_valor end +
		case when socio_protesto_valor is null then 0 else socio_protesto_valor end +
		case when socio_acao_valor is null then 0 else socio_acao_valor end +
		case when socio_pefin_valor is null then 0 else socio_pefin_valor end +
		case when socio_refin_valor is null then 0 else socio_refin_valor end +
		case when socio_spc_valor is null then 0 else socio_spc_valor end  as socios_total_valor,
	case when socio_divida_unit is null then 0 else socio_divida_unit end + 
		case when socio_ccf_unit is null then 0 else socio_ccf_unit end +
		case when socio_protesto_unit is null then 0 else socio_protesto_unit end +
		case when socio_acao_unit is null then 0 else socio_acao_unit end +
		case when socio_pefin_unit is null then 0 else socio_pefin_unit end +
		case when socio_refin_unit is null then 0 else socio_refin_unit end +
		case when socio_spc_unit is null then 0 else socio_spc_unit end as socios_total_unit,
	--Socios major
	case when socio_major_divida_valor is null then 0 else socio_major_divida_valor end as socio_major_divida_valor,
	case when socio_major_divida_unit is null then 0 else socio_major_divida_unit end as socio_major_divida_unit,
	case when socio_major_ccf_valor is null then 0 else socio_major_ccf_valor end as socio_major_ccf_valor,
	case when socio_major_ccf_unit is null then 0 else socio_major_ccf_unit end as socio_major_ccf_unit,
	case when socio_major_protesto_valor is null then 0 else socio_major_protesto_valor end as socio_major_protesto_valor,
	case when socio_major_protesto_unit is null then 0 else socio_major_protesto_unit end as socio_major_protesto_unit,
	case when socio_major_acao_valor is null then 0 else socio_major_acao_valor end as socio_major_acao_valor,
	case when socio_major_acao_unit is null then 0 else socio_major_acao_unit end as socio_major_acao_unit,
	case when socio_major_pefin_valor is null then 0 else socio_major_pefin_valor end as socio_major_pefin_valor,
	case when socio_major_pefin_unit is null then 0 else socio_major_pefin_unit end as socio_major_pefin_unit,
	case when socio_major_refin_valor is null then 0 else socio_major_refin_valor end as socio_major_refin_valor,
	case when socio_major_refin_unit is null then 0 else socio_major_refin_unit end as socio_major_refin_unit,
	case when socio_major_spc_valor is null then 0 else socio_major_spc_valor end as socio_major_spc_valor,
	case when socio_major_spc_unit is null then 0 else socio_major_spc_unit end as socio_major_spc_unit,
	case when socio_major_divida_valor is null then 0 else socio_major_divida_valor end +case when socio_major_ccf_valor is null then 0 else socio_major_ccf_valor end +case when socio_major_protesto_valor is null then 0 else socio_major_protesto_valor end +case when socio_major_acao_valor is null then 0 else socio_major_acao_valor end +case when socio_major_pefin_valor is null then 0 else socio_major_pefin_valor end +case when socio_major_refin_valor is null then 0 else socio_major_refin_valor end +case when socio_major_spc_valor is null then 0 else socio_major_spc_valor end as socios_major_total_valor,
	case when socio_major_divida_unit is null then 0 else socio_major_divida_unit end +case when socio_major_ccf_unit is null then 0 else socio_major_ccf_unit end +case when socio_major_protesto_unit is null then 0 else socio_major_protesto_unit end +case when socio_major_acao_unit is null then 0 else socio_major_acao_unit end +case when socio_major_pefin_unit is null then 0 else socio_major_pefin_unit end +case when socio_major_refin_unit is null then 0 else socio_major_refin_unit end +case when socio_major_spc_unit is null then 0 else socio_major_spc_unit end as socios_major_total_unit,
	--Total (empresa + socios)
	case when empresa_divida_valor is null then 0 else empresa_divida_valor end +
		case when empresa_ccf_valor is null then 0 else empresa_ccf_valor end +
		case when empresa_protesto_valor is null then 0 else empresa_protesto_valor end +
		case when empresa_acao_valor is null then 0 else empresa_acao_valor end +
		case when empresa_pefin_valor is null then 0 else empresa_pefin_valor end +
		case when empresa_refin_valor is null then 0 else empresa_refin_valor end +
		case when empresa_spc_valor is null then 0 else empresa_spc_valor end +
		case when socio_divida_valor is null then 0 else socio_divida_valor end + 
		case when socio_ccf_valor is null then 0 else socio_ccf_valor end +
		case when socio_protesto_valor is null then 0 else socio_protesto_valor end +
		case when socio_acao_valor is null then 0 else socio_acao_valor end +
		case when socio_pefin_valor is null then 0 else socio_pefin_valor end +
		case when socio_refin_valor is null then 0 else socio_refin_valor end +
		case when socio_spc_valor is null then 0 else socio_spc_valor end as all_valor,
	case when empresa_divida_unit is null then 0 else empresa_divida_unit end +
		case when empresa_ccf_unit is null then 0 else empresa_ccf_unit end +
		case when empresa_protesto_unit is null then 0 else empresa_protesto_unit end +
		case when empresa_acao_unit is null then 0 else empresa_acao_unit end +
		case when empresa_pefin_unit is null then 0 else empresa_pefin_unit end +
		case when empresa_refin_unit is null then 0 else empresa_refin_unit end +
		case when empresa_spc_unit is null then 0 else empresa_spc_unit end +
		case when socio_divida_unit is null then 0 else socio_divida_unit end + 
		case when socio_ccf_unit is null then 0 else socio_ccf_unit end +
		case when socio_protesto_unit is null then 0 else socio_protesto_unit end +
		case when socio_acao_unit is null then 0 else socio_acao_unit end +
		case when socio_pefin_unit is null then 0 else socio_pefin_unit end +
		case when socio_refin_unit is null then 0 else socio_refin_unit end +
		case when socio_spc_unit is null then 0 else socio_spc_unit end as all_unit
from(select dp.direct_prospect_id from direct_prospects dp join offers o on o.direct_prospect_id = dp.direct_prospect_id join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')) as dp
left join(select direct_prospect_id,
		--Empresa
		sum(case when divida_valor is null then 0 else divida_valor end) filter (where serasa_nome = 'empresa') as empresa_divida_valor,
		sum(case when divida_unit is null then 0 else divida_unit end) filter (where serasa_nome = 'empresa') as empresa_divida_unit,
		sum(case when ccf_valor is null then 0 else ccf_valor end) filter (where serasa_nome = 'empresa') as empresa_ccf_valor,
		sum(case when ccf_unit is null then 0 else ccf_unit end) filter (where serasa_nome = 'empresa') as empresa_ccf_unit,
		sum(case when protesto_valor is null then 0 else protesto_valor end) filter (where serasa_nome = 'empresa') as empresa_protesto_valor,
		sum(case when protesto_unit is null then 0 else protesto_unit end) filter (where serasa_nome = 'empresa') as empresa_protesto_unit,
		sum(case when acao_valor is null then 0 else acao_valor end) filter (where serasa_nome = 'empresa') as empresa_acao_valor,
		sum(case when acao_unit is null then 0 else acao_unit end) filter (where serasa_nome = 'empresa') as empresa_acao_unit,
		sum(case when pefin_valor is null then 0 else pefin_valor end) filter (where serasa_nome = 'empresa') as empresa_pefin_valor,
		sum(case when pefin_unit is null then 0 else pefin_unit end) filter (where serasa_nome = 'empresa') as empresa_pefin_unit,
		sum(case when refin_valor is null then 0 else refin_valor end) filter (where serasa_nome = 'empresa') as empresa_refin_valor,
		sum(case when refin_unit is null then 0 else refin_unit end) filter (where serasa_nome = 'empresa') as empresa_refin_unit,
		--Socios
		sum(case when divida_valor is null then 0 else divida_valor end) filter (where serasa_nome <> 'empresa') as socio_divida_valor,
		sum(case when divida_unit is null then 0 else divida_unit end) filter (where serasa_nome <> 'empresa') as socio_divida_unit,
		sum(case when ccf_valor is null then 0 else ccf_valor end) filter (where serasa_nome <> 'empresa') as socio_ccf_valor,
		sum(case when ccf_unit is null then 0 else ccf_unit end) filter (where serasa_nome <> 'empresa') as socio_ccf_unit,
		sum(case when protesto_valor is null then 0 else protesto_valor end) filter (where serasa_nome <> 'empresa') as socio_protesto_valor,
		sum(case when protesto_unit is null then 0 else protesto_unit end) filter (where serasa_nome <> 'empresa') as socio_protesto_unit,
		sum(case when acao_valor is null then 0 else acao_valor end) filter (where serasa_nome <> 'empresa') as socio_acao_valor,
		sum(case when acao_unit is null then 0 else acao_unit end) filter (where serasa_nome <> 'empresa') as socio_acao_unit,
		sum(case when pefin_valor is null then 0 else pefin_valor end) filter (where serasa_nome <> 'empresa') as socio_pefin_valor,
		sum(case when pefin_unit is null then 0 else pefin_unit end) filter (where serasa_nome <> 'empresa') as socio_pefin_unit,
		sum(case when refin_valor is null then 0 else refin_valor end) filter (where serasa_nome <> 'empresa') as socio_refin_valor,
		sum(case when refin_unit is null then 0 else refin_unit end) filter (where serasa_nome <> 'empresa') as socio_refin_unit,
		--Socios majoritarios
		sum(case when divida_valor is null then 0 else divida_valor end) filter (where serasa_share >= 0.2) as socio_major_divida_valor,
		sum(case when divida_unit is null then 0 else divida_unit end) filter (where serasa_share >= 0.2) as socio_major_divida_unit,
		sum(case when ccf_valor is null then 0 else ccf_valor end) filter (where serasa_share >= 0.2) as socio_major_ccf_valor,
		sum(case when ccf_unit is null then 0 else ccf_unit end) filter (where serasa_share >= 0.2) as socio_major_ccf_unit,
		sum(case when protesto_valor is null then 0 else protesto_valor end) filter (where serasa_share >= 0.2) as socio_major_protesto_valor,
		sum(case when protesto_unit is null then 0 else protesto_unit end) filter (where serasa_share >= 0.2) as socio_major_protesto_unit,
		sum(case when acao_valor is null then 0 else acao_valor end) filter (where serasa_share >= 0.2) as socio_major_acao_valor,
		sum(case when acao_unit is null then 0 else acao_unit end) filter (where serasa_share >= 0.2) as socio_major_acao_unit,
		sum(case when pefin_valor is null then 0 else pefin_valor end) filter (where serasa_share >= 0.2) as socio_major_pefin_valor,
		sum(case when pefin_unit is null then 0 else pefin_unit end) filter (where serasa_share >= 0.2) as socio_major_pefin_unit,
		sum(case when refin_valor is null then 0 else refin_valor end) filter (where serasa_share >= 0.2) as socio_major_refin_valor,
		sum(case when refin_unit is null then 0 else refin_unit end) filter (where serasa_share >= 0.2) as socio_major_refin_unit
	from(select t2.*,t3.serasa_share
		from(select direct_prospect_id,
				jsonb_object_keys(cc."data"->'anotacoes') as serasa_nome,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'DIVIDA_VENCIDA'->>'valor')::float as divida_valor,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'DIVIDA_VENCIDA'->>'quantidade')::float as divida_unit,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'CHEQUE'->>'valor')::float as ccf_valor,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'CHEQUE'->>'quantidade')::float as ccf_unit,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'PROTESTO'->>'valor')::float as protesto_valor,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'PROTESTO'->>'quantidade')::float as protesto_unit,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'ACAO_JUDICIAL'->>'valor')::float as acao_valor,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Resumo'->'ACAO_JUDICIAL'->>'quantidade')::float as acao_unit,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Pefin'->>'valor_total')::float as pefin_valor,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Pefin'->>'quantidade')::float as pefin_unit,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Refin'->>'valor_total')::float as refin_valor,
				("data"->'anotacoes'->jsonb_object_keys(cc."data"->'anotacoes')->'Refin'->>'quantidade')::float as refin_unit
			from credito_coleta cc
			join(select dp.direct_prospect_id, dp.cnpj, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
				from credito_coleta cc
				join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
				join offers o on o.direct_prospect_id = dp.direct_prospect_id
				join loan_requests lr on lr.offer_id = o.offer_id 
				where tipo = 'Serasa' and lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')
				group by 1,2) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t2
		left join(select direct_prospect_id,
					jsonb_object_keys("data"->'acionistas') as nome,
					("data"->'acionistas'->jsonb_object_keys("data"->'acionistas')->>'percentual')::float/100 as serasa_share
				from credito_coleta cc
				join(select dp.direct_prospect_id, dp.cnpj, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
					from credito_coleta cc
					join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
					join offers o on o.direct_prospect_id = dp.direct_prospect_id
					join loan_requests lr on lr.offer_id = o.offer_id 
					where tipo = 'Serasa' and lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')
					group by 1,2) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t3 on t3.nome = t2.serasa_nome and t3.direct_prospect_id = t2.direct_prospect_id) as t4
	group by 1) as serasa on serasa.direct_prospect_id = dp.direct_prospect_id
left join(select direct_prospect_id,
		--Socios
		sum(case when spc_socios_valor is null then 0 else spc_socios_valor end) as socio_spc_valor,
		sum(case when spc_socios_unit is null then 0 else spc_socios_unit end) as socio_spc_unit,
		--Socios majoritarios
		sum(case when spc_socios_valor is null then 0 else spc_socios_valor end) filter (where spc_share >= 0.2) as socio_major_spc_valor,
		sum(case when spc_socios_unit is null then 0 else spc_socios_unit end) filter (where spc_share >= 0.2) as socio_major_spc_unit
	from(select t2.direct_prospect_id,spc_nome,spc_share,
				case when spc_socios_valor is null or spc_socios_valor = '' then 0 else spc_socios_valor::float end as spc_socios_valor,
				case when spc_socios_unit is null or spc_socios_unit = '' then 0 else spc_socios_unit::float end as spc_socios_unit
			from(select direct_prospect_id,
					data->'spc'->jsonb_object_keys(data->'spc')->>'nome' as spc_nome,
					data->'spc'->jsonb_object_keys(data->'spc')->>'valor' as spc_socios_valor,
					data->'spc'->jsonb_object_keys(data->'spc')->>'quantidade' as spc_socios_unit
				from credito_coleta cc
				join(select dp.direct_prospect_id, dp.cnpj, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
					from credito_coleta cc
					join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
					join offers o on o.direct_prospect_id = dp.direct_prospect_id
					join loan_requests lr on lr.offer_id = o.offer_id 
					where tipo = 'Serasa' and lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')
					group by 1,2) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t2
			left join(select direct_prospect_id,
						jsonb_object_keys("data"->'acionistas') as nome,
						("data"->'acionistas'->jsonb_object_keys("data"->'acionistas')->>'percentual')::float/100 as spc_share
					from credito_coleta cc
					join(select dp.direct_prospect_id, dp.cnpj, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
						from credito_coleta cc
						join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
						join offers o on o.direct_prospect_id = dp.direct_prospect_id
						join loan_requests lr on lr.offer_id = o.offer_id 
						where tipo = 'Serasa' and lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')
						group by 1,2) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t3 on t3.nome = t2.spc_nome and t3.direct_prospect_id = t2.direct_prospect_id) as t4
	group by 1) as spc_socios on spc_socios.direct_prospect_id = dp.direct_prospect_id
left join(select direct_prospect_id,
		sum(case when spc_valor is null then 0 else spc_valor end) as empresa_spc_valor,
		sum(case when spc_unit is null then 0 else spc_unit end) as empresa_spc_unit	
	from(select direct_prospect_id,
			("data"->'anotacao_spc'->>'total_debito')::float as spc_valor,
			("data"->'anotacao_spc'->>'total_ocr')::float as spc_unit
		from credito_coleta cc
		join(select dp.direct_prospect_id, dp.cnpj, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join offers o on o.direct_prospect_id = dp.direct_prospect_id
			join loan_requests lr on lr.offer_id = o.offer_id 
			where tipo = 'Serasa' and lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')
			group by 1,2) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t2
	group by 1) as spc_empresa on spc_empresa.direct_prospect_id = dp.direct_prospect_id) as SerasaRestr on SerasaRestr.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SERASA PROBLEMA COLIGADAS
left join(select direct_prospect_id, count(*) filter (where empresas_problemas = 'S') as empresas_problema
	from(select direct_prospect_id,
			"data"->'participacoes'->jsonb_object_keys("data"->'participacoes')->>'restricao' as empresas_problemas
		from credito_coleta cc
		join(select dp.direct_prospect_id, dp.cnpj, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join offers o on o.direct_prospect_id = dp.direct_prospect_id
			join loan_requests lr on lr.offer_id = o.offer_id 
			where tipo = 'Serasa' and lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')
			group by 1,2) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t1
	group by 1) as serasaColig on serasaColig.direct_prospect_id = dp.direct_prospect_id 
--------------------------TABLE SERASA CONSULTA FACTORING SERASA
left join (select direct_prospect_id,sum(case when position('INVEST DIR' in consultas_serasa) > 0 or 
	position('CREDITORIOS' in consultas_serasa) > 0 or
	position('FUNDO DE INVE' in consultas_serasa) > 0 or
	position('SECURITIZADORA' in consultas_serasa) > 0 or
	position('FACTORING'in consultas_serasa) > 0 or
	position('FOMENTO'in consultas_serasa) > 0 or
	position('FIDC' in consultas_serasa) > 0 or
	position('NOVA S R M' in consultas_serasa) > 0 or
	position('SERVICOS FINANCEIROS' in consultas_serasa) > 0 then 1 else 0 end) as consulta_factoring_serasa
	from (select direct_prospect_id,cc.data->'consulta_empresas'->jsonb_object_keys(data->'consulta_empresas')->>'nome' as consultas_serasa
		from credito_coleta cc
		join(select dp.direct_prospect_id, dp.cnpj, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join offers o on o.direct_prospect_id = dp.direct_prospect_id
			join loan_requests lr on lr.offer_id = o.offer_id 
			where tipo = 'Serasa' and lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')
			group by 1,2) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t2 
	group by 1) as ConsultaSerasa on ConsultaSerasa.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SERASA CONSULTA FACTORING SPC
left join (select direct_prospect_id,sum(case when position('INVEST DIR' in consultas_spc) > 0 or 
	position('CREDITORIOS' in consultas_spc) > 0 or
	position('FUNDO DE INVE' in consultas_spc) > 0 or
	position('SECURITIZADORA' in consultas_spc) > 0 or
	position('FACTORING'in consultas_spc) > 0 or
	position('FOMENTO'in consultas_spc) > 0 or
	position('FIDC' in consultas_spc) > 0 or
	position('NOVA S R M' in consultas_spc) > 0 or
	position('SERVICOS FINANCEIROS' in consultas_spc) > 0 then 1 else 0 end) as consulta_factoring_spc
	from (select direct_prospect_id,cc.data->'consulta_spc'->jsonb_object_keys(data->'consulta_spc')->>'nome' as consultas_spc
		from credito_coleta cc
		join(select dp.direct_prospect_id, dp.cnpj, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join offers o on o.direct_prospect_id = dp.direct_prospect_id
			join loan_requests lr on lr.offer_id = o.offer_id 
			where tipo = 'Serasa' and lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')
			group by 1,2) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t2
	group by 1) as ConsultaSPC on ConsultaSPC.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SERASA CONSULTAS
left join (select direct_prospect_id,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base)+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base) as consultas_atual,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '1 month')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '1 month') as consultas_1m,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '2 months')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '2 months') as consultas_2m,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '3 months')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '3 months') as consultas_3m,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '4 months')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '4 months') as consultas_4m,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '5 months')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '5 months') as consultas_5m,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '6 months')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '6 months') as consultas_6m,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '7 months')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '7 months') as consultas_7m,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '8 months')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '8 months') as consultas_8m,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '9 months')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '9 months') as consultas_9m,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '10 months')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '10 months') as consultas_10m,
	sum(case when empresas = '' or empresas is null then '0' else empresas end::int) filter (where mes = data_base - interval '11 months')+sum(case when spc = '' or spc is null then '0' else spc end::int) filter (where mes = data_base - interval '11 months') as consultas_11m
	from (select direct_prospect_id,
			to_date(to_char(to_date(cc.data->'datetime'->>'data','yyyymmdd'),'Mon/yy'),'Mon/yy') as data_base,
			to_date(jsonb_object_keys(data->'consultas'),'yy/mm') as mes,data->'consultas'->jsonb_object_keys(data->'consultas')->>'pesquisaEmpresas' empresas, 
		replace(data->'registro_spc'->jsonb_object_keys(data->'registro_spc')->>'quantidade',' ','') spc
		from credito_coleta cc
		join(select dp.direct_prospect_id, dp.cnpj, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join offers o on o.direct_prospect_id = dp.direct_prospect_id
			join loan_requests lr on lr.offer_id = o.offer_id 
			where tipo = 'Serasa' and lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')
			group by 1,2) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t2
	group by 1) as serasaconsultas on serasaconsultas.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta,("data"->>'pd')::float as pd_serasa
	from credito_coleta cc
	join(select dp.direct_prospect_id, dp.cnpj, dp.serasa_4,o.rating,max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= lr.loan_date) as max_date
		from credito_coleta cc
		join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
		join offers o on o.direct_prospect_id = dp.direct_prospect_id
		join loan_requests lr on lr.offer_id = o.offer_id 
		where tipo = 'Serasa' and lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')
		group by 1,2,3,4) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SÓCIO MAJORITÁRIO MAIS VELHO
left join (select lrs.loan_request_id,extract(year from age(loan_date,s.date_of_birth))::int as MajorSigner_Age, s.revenue as MajorSigner_Revenue,s.civil_status as MajorSigner_civil_status,s.gender as MajorSigner_gender,avg_age_partners,avg_revenue_partners,male_partners,female_partners
	from signers s
	join loan_requests_signers lrs on lrs.signer_id = s.signer_id
	join(select lrs.loan_request_id, signer_percentage,avg_age_partners,avg_revenue_partners,male_partners,female_partners,loan_date,min(date_of_birth) as data_nasc
		from signers s
		join loan_requests_signers lrs on lrs.signer_id = s.signer_id
		join(select lr.loan_request_id, 
				lr.loan_date, 
				max(s.share_percentage) as signer_percentage,
				avg(extract(year from age(lr.loan_date,s.date_of_birth))) filter (where s.share_percentage > 0) as avg_age_partners,
				avg(s.revenue) filter (where s.share_percentage > 0) as avg_revenue_partners,
				count(*) filter (where s.gender = 'MALE' and s.share_percentage > 0) as male_partners, 
				count(*) filter (where s.gender = 'FEMALE' and s.share_percentage > 0) as female_partners
			from signers s
			join loan_requests_signers as lrs on lrs.signer_id = s.signer_id
			join loan_requests lr on lr.loan_request_id = lrs.loan_request_id
			where lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')
			group by 1,2) as t1 on t1.loan_request_id = lrs.loan_request_id and s.share_percentage = t1. signer_percentage
		group by 1,2,3,4,5,6,7) as t2 on t2.loan_request_id = lrs.loan_request_id and s.date_of_birth = t2.data_nasc and s.share_percentage = t2.signer_percentage) as MajSigners on MajSigners.loan_request_id = lr.loan_request_id
--------------------------TABLE SOLICITANTE POR CPF
left join(select lrs.loan_request_id, extract(year from age(lr.loan_date,s.date_of_birth))::int as SolicSignersCPF_Age, s.revenue as SolicSignersCPF_Revenue,s.civil_status as SolicSignerCPF_civil_status,s.gender as SolicSignerCPF_gender
	from direct_prospects dp
	join offers o on o.direct_prospect_id = dp.direct_prospect_id
	join loan_requests lr on lr.offer_id = o.offer_id
	join loan_requests_signers lrs on lrs.loan_request_id = lr.loan_request_id
	join signers s on s.signer_id = lrs.signer_id
	where lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18') and case when dp.cpf is null then null else case when dp.cpf = s.cpf then 1 else 0 end end = 1) as SolicCPFSigners on SolicCPFSigners.loan_request_id = lr.loan_request_id 
--------------------------TABLE SÓCIO MAIS VELHO
left join(select lrs.loan_request_id,max(extract(year from age(loan_date,s.date_of_birth)))::int as OldestSh_Age, max(s.revenue) as OldestSh_Revenue,max(s.civil_status) as OldestSh_civil_status,max(s.gender) as OldestSh_gender
	from signers s
	join loan_requests_signers lrs on lrs.signer_id = s.signer_id
	join(select lr.loan_request_id, lr.loan_date, min(s.date_of_birth) as min_date
		from signers s
		join loan_requests_signers as lrs on lrs.signer_id = s.signer_id
		join loan_requests lr on lr.loan_request_id = lrs.loan_request_id
		where lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18') and s.share_percentage > 0
		group by 1,2) as t1 on t1.loan_request_id = lrs.loan_request_id and s.date_of_birth = t1.min_date 
	group by 1) as Signers on Signers.loan_request_id = lr.loan_request_id
--------------------------TABLE ADMNISTRADOR MAIS VELHO
left join(select lrs.loan_request_id, max(extract(year from age(loan_date,s.date_of_birth)))::int as AdminSigner_Age, max(s.revenue) as AdminSigner_Revenue,max(s.civil_status) as AdminSigner_civil_status,max(s.gender) as AdminSigner_gender 
	from signers s
	join loan_requests_signers lrs on lrs.signer_id = s.signer_id
	join(select lrs.loan_request_id,lr.loan_date, min(s.date_of_birth) data_nasc
		from signers s
		join loan_requests_signers lrs on lrs.signer_id = s.signer_id
		join loan_requests lr on lr.loan_request_id = lrs.loan_request_id
		where s.administrator = true
		group by 1,2) as t2 on t2.loan_request_id = lrs.loan_request_id and t2.data_nasc = s.date_of_birth
	group by 1) as AdminSigners on AdminSigners.loan_request_id = lr.loan_request_id
--------------------------TABLE CHECKLIST
left join(select t2.emprestimo_id,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'valor_pj') as largest_cashflow_company_account,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'faturamento') as proved_informed_revenue,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'empreza_azul') as positive_balance_account,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'fluxo_entradas') as continuous_income,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'saldo_negativo') as low_negative_balance,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'entradas_desvinculadas') as income_unrelated_government,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'diversificacao_clientes') as diversified_clients,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'somente_despesas_empresa') as only_company_expenses,
	sum(pontuacao) filter (where tipo = 'Outros' and campo = 'endereco') as different_personal_company_address,
	sum(pontuacao) filter (where tipo = 'Outros' and campo = 'socio_bens') as partner_has_assets,
	sum(pontuacao) filter (where tipo = 'Outros' and campo = 'empresa_credito') as company_usedto_credit,
	sum(pontuacao) filter (where tipo = 'Outros' and campo = 'solicitou_menos') as requested_lessthan_offered,
	sum(pontuacao) filter (where tipo = 'Outros' and campo = 'endividamento_baixo') as low_personal_debt,
	sum(pontuacao) filter (where tipo = 'Outros' and campo = 'simulacao_diligente') as dilligent_credit_simulation,
	sum(pontuacao) filter (where tipo = 'Empresa' and campo = 'conta_pj') as company_account_confirmed,
	sum(pontuacao) filter (where tipo = 'Empresa' and campo = 'contrato_social') as social_contract_confirmed,
	sum(pontuacao) filter (where tipo = 'Empresa' and campo = 'extratos_bancarios') as bank_statements_confirmed,
	sum(pontuacao) filter (where tipo = 'Empresa' and campo = 'comprovante_endereço') as company_address_confirmed,
	sum(pontuacao) filter (where tipo = 'Física' and campo = 'cpf')/min(num_socios) as personal_cpf,
	sum(pontuacao) filter (where tipo = 'Física' and campo = 'identidade')/min(num_socios) as personal_rg,
	sum(pontuacao) filter (where tipo = 'Física' and campo = 'conjuge_info')/min(num_socios) as spouse_info,
	sum(pontuacao) filter (where tipo = 'Física' and campo = 'comprovante_renda')/min(num_socios) as proof_income,
	sum(pontuacao) filter (where tipo = 'Física' and campo = 'comprovante_endereço')/min(num_socios) as proof_personal_address,	
	sum(pontuacao) filter (where tipo in ('Extrato', 'Outros')) as score_documentation,
	sum(pontuacao) filter (where tipo in ('Física', 'Empresa'))/(4+5*min(num_socios)) percent_documentation
	from (select emprestimo_id, tipo,campo, case when pontuacao = '' then null else pontuacao end::double precision from (
		select emprestimo_id,tipo,jsonb_object_keys(formulario) as campo,formulario->jsonb_object_keys(formulario)->>'pontos' as pontuacao
		from credito_checklist cc
		join loan_requests lr on lr.loan_request_id = cc.emprestimo_id
		where lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')) as t1) as t2
	left join (select emprestimo_id, count(*) num_socios from credito_checklist cc join loan_requests lr on lr.loan_request_id = cc.emprestimo_id where lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18') and cc.tipo = 'Física' group by 1) as t1 on t1.emprestimo_id = t2.emprestimo_id
	group by 1) as checklist on checklist.emprestimo_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO
left join(select lr.loan_request_id, 
	max(case when fpp.status = 'Pago' then 1 else 0 end) as fully_paid,
	count(*) filter (where fp.status like 'Pago%') as paid_inst,
	count(*) filter (where fp.status = 'Ativo' and fp.vencimento_em < current_date) as late_inst, 
	count(*) filter (where fp.vencimento_em < current_date) as due_inst,	
	count(*) filter (where fp.vencimento_em >= current_date) as to_come_inst,	
	max(case when fp.pago_em is null then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end) filter (where fp.numero = 1) as fpd,
	max(fp.vencimento_em) filter (where fp.numero = 1 and fp.pago_em is null) as "1st_inst_due_to", 
	sum(fp.cobrado) filter (where fp.pago_em is null) as EAD, 
	sum(fp.cobrado) as curr_total_payment, 
	sum(fp.pago) filter (where fp.status like 'Pago%') as total_paid, 
	sum(fp.cobrado) filter (where pago_em is null and current_date - fp.vencimento_em > 0) as late_payment, 
	sum(fp.cobrado) filter (where pago_em is null and current_date - fp.vencimento_em > 180) as writeoff, 
	min(fp.vencimento_em) filter (where fp.pago_em is null) as current_inst_due_to,
	max(fp.cobrado) filter (where fp.status = 'Ativo') as pmt_atual,
	min(fp.pago_em) as first_paid_inst_paid_on
	from loan_requests lr  
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	where fp.status = 'Ativo' or fp.status like 'Pago%'
	group by 1) as Financeiro on Financeiro.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO ORIGINAL
left join (select fpp.emprestimo_id, sum(fp.cobrado) as original_total_payment, max(fp.vencimento_em) filter (where fp.numero = 1) as original_1st_pmt_due_to
	from financeiro_planopagamento fpp  
	join (select emprestimo_id, min(id) as plano_id from financeiro_planopagamento group by 1) as fpp_orig on fpp_orig.plano_id = fpp.id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	group by 1) as Financeiro_original on Financeiro_original.emprestimo_id = lr.loan_request_id
--------------------------TABLE DPD SOFT
left join(select lr.loan_request_id,
	count(distinct t2.plano_id) as count_renegotiation,
	min(fp.numero) filter (where fp.pago_em is null) ParcEmAberto,
	max(case when fp.numero <> 1 then null else case when ParcSubst = 1 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt1,
	max(case when fp.numero <> 2 then null else case when ParcSubst = 2 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt2,
	max(case when fp.numero <> 3 then null else case when ParcSubst = 3 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt3,
	max(case when fp.numero <> 4 then null else case when ParcSubst = 4 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt4,
	max(case when fp.numero <> 5 then null else case when ParcSubst = 5 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt5,
	max(case when fp.numero <> 6 then null else case when ParcSubst = 6 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt6,
	max(case when fp.numero <> 7 then null else case when ParcSubst = 7 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt7,
	max(case when fp.numero <> 8 then null else case when ParcSubst = 8 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt8,
	max(case when fp.numero <> 9 then null else case when ParcSubst = 9 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt9,
	max(case when fp.numero <> 10 then null else case when ParcSubst = 10 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt10,
	max(case when fp.numero <> 11 then null else case when ParcSubst = 11 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt11,
	max(case when fp.numero <> 12 then null else case when ParcSubst = 12 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt12
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada'
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	where fp.status not in ('Cancelada','EmEspera')
	group by 1) as DPD_Soft on DPD_Soft.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO PMT 9 PLANO MAIS ATUAL SOFT
left join(select lr.loan_request_id,
	max(fp.numero) filter (where fp.pago_em <= case when max_venc9p is null then max_venc9m else max_venc9p end) as ultparcpaga,
	count(*) filter (where fp.vencimento_em <= case when max_venc9p is null then max_venc9m else max_venc9p end) as qtd_parcelas,
	max((case when fp.numero <> 1 then null else (case when ParcSubst = 1 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt1,	
	max((case when fp.numero <> 2 then null else (case when ParcSubst = 2 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt2,	
	max((case when fp.numero <> 3 then null else (case when ParcSubst = 3 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt3,	
	max((case when fp.numero <> 4 then null else (case when ParcSubst = 4 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt4,	
	max((case when fp.numero <> 5 then null else (case when ParcSubst = 5 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt5,	
	max((case when fp.numero <> 6 then null else (case when ParcSubst = 6 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt6,
	max((case when fp.numero <> 7 then null else (case when ParcSubst = 7 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt7,
	max((case when fp.numero <> 8 then null else (case when ParcSubst = 8 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt8,
	max((case when fp.numero <> 9 then null else (case when ParcSubst = 9 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt9
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada' and fp.numero <= 9
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	join(select fpp.emprestimo_id, case when max_id is null then (lr.loan_date + interval '9 months')::date end as max_venc9M , max(fp.vencimento_em) filter (where fp.numero <= 9 and fpp.id = t1.max_id) as max_venc9P
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, max(fpp.id) filter (where fp.numero = 9 and fpp.status <> 'EmEspera') as max_id
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1,2) as max_venc_6M on max_venc_6M.emprestimo_id = fpp.emprestimo_id 
	where fp.status not in ('Cancelada','EmEspera') and case when max_venc9p is null then max_venc9m else max_venc9p end <= current_date
	group by 1) as PMT9_actual_soft on PMT9_actual_soft.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO PMT 6 PLANO MAIS ATUAL SOFT
left join(select lr.loan_request_id,
	max(fp.numero) filter (where fp.pago_em <= case when max_venc6p is null then max_venc6m else max_venc6p end) as ultparcpaga,
	count(*) filter (where fp.vencimento_em <= case when max_venc6p is null then max_venc6m else max_venc6p end) as qtd_parcelas,
	max((case when fp.numero <> 1 then null else (case when ParcSubst = 1 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt1,	
	max((case when fp.numero <> 2 then null else (case when ParcSubst = 2 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt2,	
	max((case when fp.numero <> 3 then null else (case when ParcSubst = 3 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt3,	
	max((case when fp.numero <> 4 then null else (case when ParcSubst = 4 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt4,	
	max((case when fp.numero <> 5 then null else (case when ParcSubst = 5 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt5,	
	max((case when fp.numero <> 6 then null else (case when ParcSubst = 6 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt6
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada' and fp.numero <= 6
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	join(select fpp.emprestimo_id, case when max_id is null then (lr.loan_date + interval '6 months')::date end as max_venc6M , max(fp.vencimento_em) filter (where fp.numero <= 6 and fpp.id = t1.max_id) as max_venc6P
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, max(fpp.id) filter (where fp.numero = 6 and fpp.status <> 'EmEspera') as max_id
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1,2) as max_venc_6M on max_venc_6M.emprestimo_id = fpp.emprestimo_id 
	where fp.status not in ('Cancelada','EmEspera') and case when max_venc6p is null then max_venc6m else max_venc6p end <= current_date
	group by 1) as PMT6_actual_soft on PMT6_actual_soft.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO PMT 3 PLANO MAIS ATUAL SOFT
left join(select lr.loan_request_id,
	max(fp.numero) filter (where fp.pago_em <= case when max_venc3p is null then max_venc3m else max_venc3p end) as ultparcpaga,
	count(*) filter (where fp.vencimento_em <= case when max_venc3p is null then max_venc3m else max_venc3p end) as qtd_parcelas,
	max((case when fp.numero <> 1 then null else (case when ParcSubst = 1 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - vencparcsubst else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em  then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - VencParcSubst end) else (case when (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt1,	
	max((case when fp.numero <> 2 then null else (case when ParcSubst = 2 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - vencparcsubst else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em  then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - VencParcSubst end) else (case when (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt2,	
	max((case when fp.numero <> 3 then null else (case when ParcSubst = 3 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - vencparcsubst else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em  then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - VencParcSubst end) else (case when (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt3
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada' and fp.numero <= 3
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	join(select fpp.emprestimo_id, case when max_id is null then (lr.loan_date + interval '3 months')::date end as max_venc3M , max(fp.vencimento_em) filter (where fp.numero <= 3 and fpp.id = t1.max_id) as max_venc3P
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, max(fpp.id) filter (where fp.numero = 3 and fpp.status <> 'EmEspera') as max_id
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1,2) as max_venc_3M on max_venc_3M.emprestimo_id = fpp.emprestimo_id 
	where fp.status not in ('Cancelada','EmEspera') and case when max_venc3p is null then max_venc3m else max_venc3p end <= current_date
	group by 1) as PMT3_Actual_soft on PMT3_Actual_soft.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO PMT 9 PLANO ORIGINAL SOFT
left join(select lr.loan_request_id,
	max(fp.numero) filter (where fp.pago_em <= case when max_venc9p is null then max_venc9m else max_venc9p end) as ultparcpaga,
	count(*) filter (where fp.vencimento_em <= case when max_venc9p is null then max_venc9m else max_venc9p end) as qtd_parcelas,
	max((case when fp.numero <> 1 then null else (case when ParcSubst = 1 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt1,	
	max((case when fp.numero <> 2 then null else (case when ParcSubst = 2 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt2,	
	max((case when fp.numero <> 3 then null else (case when ParcSubst = 3 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt3,	
	max((case when fp.numero <> 4 then null else (case when ParcSubst = 4 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt4,	
	max((case when fp.numero <> 5 then null else (case when ParcSubst = 5 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt5,	
	max((case when fp.numero <> 6 then null else (case when ParcSubst = 6 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt6,
	max((case when fp.numero <> 7 then null else (case when ParcSubst = 7 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt7,
	max((case when fp.numero <> 8 then null else (case when ParcSubst = 8 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt8,
	max((case when fp.numero <> 9 then null else (case when ParcSubst = 9 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - vencparcsubst else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em  then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - VencParcSubst end) else (case when (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > current_date then current_date else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em else (case when (case when max_venc9p is null then max_venc9m else max_venc9p end) > fp.pago_em then fp.pago_em else (case when max_venc9p is null then max_venc9m else max_venc9p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt9
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada' and fp.numero <= 9
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	join(select fpp.emprestimo_id, case when min_id is null then (lr.loan_date + interval '9 months')::date end as max_venc9M , max(fp.vencimento_em) filter (where fp.numero <= 9 and fpp.id = t1.min_id) as max_venc9P
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, min(fpp.id) filter (where fp.numero = 9 and fpp.status <> 'EmEspera') as min_id
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1,2) as max_venc_6M on max_venc_6M.emprestimo_id = fpp.emprestimo_id 
	where fp.status not in ('Cancelada','EmEspera') and case when max_venc9p is null then max_venc9m else max_venc9p end <= current_date
	group by 1) as PMT9_orig_soft on PMT9_orig_soft.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO PMT 6 PLANO ORIGINAL SOFT
left join(select lr.loan_request_id,
	max(fp.numero) filter (where fp.pago_em <= case when max_venc6p is null then max_venc6m else max_venc6p end) as ultparcpaga,
	count(*) filter (where fp.vencimento_em <= case when max_venc6p is null then max_venc6m else max_venc6p end) as qtd_parcelas,
	max((case when fp.numero <> 1 then null else (case when ParcSubst = 1 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt1,	
	max((case when fp.numero <> 2 then null else (case when ParcSubst = 2 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt2,	
	max((case when fp.numero <> 3 then null else (case when ParcSubst = 3 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt3,	
	max((case when fp.numero <> 4 then null else (case when ParcSubst = 4 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt4,	
	max((case when fp.numero <> 5 then null else (case when ParcSubst = 5 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt5,	
	max((case when fp.numero <> 6 then null else (case when ParcSubst = 6 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - vencparcsubst else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em  then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - VencParcSubst end) else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt6
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada' and fp.numero <= 6
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	join(select fpp.emprestimo_id, case when min_id is null then (lr.loan_date + interval '6 months')::date end as max_venc6M , max(fp.vencimento_em) filter (where fp.numero <= 6 and fpp.id = t1.min_id) as max_venc6P
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, min(fpp.id) filter (where fp.numero = 6 and fpp.status <> 'EmEspera') as min_id
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1,2) as max_venc_6M on max_venc_6M.emprestimo_id = fpp.emprestimo_id 
	where fp.status not in ('Cancelada','EmEspera') and case when max_venc6p is null then max_venc6m else max_venc6p end <= current_date
	group by 1) as PMT6_orig_soft on PMT6_orig_soft.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO PMT 3 PLANO ORIGINAL SOFT
left join(select lr.loan_request_id,
	max(fp.numero) filter (where fp.pago_em <= case when max_venc3p is null then max_venc3m else max_venc3p end) as ultparcpaga,
	count(*) filter (where fp.vencimento_em <= case when max_venc3p is null then max_venc3m else max_venc3p end) as qtd_parcelas,
	max((case when fp.numero <> 1 then null else (case when ParcSubst = 1 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - vencparcsubst else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em  then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - VencParcSubst end) else (case when (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt1,	
	max((case when fp.numero <> 2 then null else (case when ParcSubst = 2 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - vencparcsubst else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em  then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - VencParcSubst end) else (case when (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt2,	
	max((case when fp.numero <> 3 then null else (case when ParcSubst = 3 and VencParcSubst is not null then (case when fp.pago_em is null  then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - vencparcsubst else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em  then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - VencParcSubst end) else (case when (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > current_date then current_date else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em else (case when (case when max_venc3p is null then max_venc3m else max_venc3p end) > fp.pago_em then fp.pago_em else (case when max_venc3p is null then max_venc3m else max_venc3p end) end) - fp.vencimento_em end) end) end) end)) as max_late_pmt3
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada' and fp.numero <= 3
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	join(select fpp.emprestimo_id, case when min_id is null then (lr.loan_date + interval '3 months')::date end as max_venc3M , max(fp.vencimento_em) filter (where fp.numero <= 3 and fpp.id = t1.min_id) as max_venc3P
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, min(fpp.id) filter (where fp.numero = 3 and fpp.status <> 'EmEspera') as min_id
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1,2) as max_venc_3M on max_venc_3M.emprestimo_id = fpp.emprestimo_id 
	where fp.status not in ('Cancelada','EmEspera') and case when max_venc3p is null then max_venc3m else max_venc3p end <= current_date
	group by 1) as PMT3_orig_soft on PMT3_orig_soft.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO PMT 6 PLANO ORIGINAL AGGRESSIVE
left join(select lr.loan_request_id,
	max(fp.numero) filter (where fp.pago_em <= case when max_venc6p is null then max_venc6m else max_venc6p end) as ultparcpaga,
	count(*) filter (where fp.vencimento_em <= case when max_venc6p is null then max_venc6m else max_venc6p end) as qtd_parcelas,
	max((case when fp.numero <> 1 then null else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < pmt1_venc then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt1_venc else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt1_venc end) end) end)) as max_late_pmt1,	
	max((case when fp.numero <> 2 then null else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < pmt2_venc then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt2_venc else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt2_venc end) end) end)) as max_late_pmt2,	
	max((case when fp.numero <> 3 then null else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < pmt3_venc then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt3_venc else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt3_venc end) end) end)) as max_late_pmt3,	
	max((case when fp.numero <> 4 then null else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < pmt4_venc then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt4_venc else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt4_venc end) end) end)) as max_late_pmt4,	
	max((case when fp.numero <> 5 then null else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < pmt5_venc then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt5_venc else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt5_venc end) end) end)) as max_late_pmt5,	
	max((case when fp.numero <> 6 then null else (case when (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) < pmt6_venc then null else (case when fp.pago_em is null then (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > current_date then current_date else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt6_venc else (case when (case when max_venc6p is null then max_venc6m else max_venc6p end) > fp.pago_em then fp.pago_em else (case when max_venc6p is null then max_venc6m else max_venc6p end) end) - pmt6_venc end) end) end)) as max_late_pmt6
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	join(select fpp.emprestimo_id, 
			case when min_id is null then (lr.loan_date + interval '6 months')::date end as max_venc6M, 
			max(fp.vencimento_em) filter (where fp.numero = 6 and fpp.id = t1.min_id) as max_venc6P,
			min(fp.vencimento_em) filter (where fp.numero = 1 and fpp.id = case when t1.min_id is null then min_id2 else min_id end) as pmt1_venc,
			min(fp.vencimento_em) filter (where fp.numero = 2 and fpp.id = case when t1.min_id is null then min_id2 else min_id end) as pmt2_venc,
			min(fp.vencimento_em) filter (where fp.numero = 3 and fpp.id = case when t1.min_id is null then min_id2 else min_id end) as pmt3_venc,
			min(fp.vencimento_em) filter (where fp.numero = 4 and fpp.id = case when t1.min_id is null then min_id2 else min_id end) as pmt4_venc,
			min(fp.vencimento_em) filter (where fp.numero = 5 and fpp.id = case when t1.min_id is null then min_id2 else min_id end) as pmt5_venc,
			min(fp.vencimento_em) filter (where fp.numero = 6 and fpp.id = case when t1.min_id is null then min_id2 else min_id end) as pmt6_venc
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, min(fpp.id) filter (where fp.numero = 6) as min_id,min(fpp.id) as min_id2
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1,2) as max_venc_6M on max_venc_6M.emprestimo_id = fpp.emprestimo_id 
	where fp.status not in ('Cancelada','EmEspera') and case when max_venc6p is null then max_venc6m else max_venc6p end <= current_date
	group by 1) as PMT6_orig_aggressive on PMT6_orig_aggressive.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO MOB 6 PLANO ORIGINAL SOFT
left join(select lr.loan_request_id,
	max(fp.numero) filter (where fp.pago_em <= (lr.loan_date + interval '7 months')::date) as ultparcpaga,
	count(*) filter (where fp.vencimento_em <= (lr.loan_date + interval '7 months')::date) as qtd_parcelas,
	max((case when fp.numero <> 1 then null else (case when ParcSubst = 1 and VencParcSubst is not null then (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - vencparcsubst else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em  then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - VencParcSubst end) else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em end) end) end) end)) as max_late_pmt1,	
	max((case when fp.numero <> 2 then null else (case when ParcSubst = 2 and VencParcSubst is not null then (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - vencparcsubst else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em  then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - VencParcSubst end) else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em end) end) end) end)) as max_late_pmt2,	
	max((case when fp.numero <> 3 then null else (case when ParcSubst = 3 and VencParcSubst is not null then (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - vencparcsubst else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em  then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - VencParcSubst end) else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em end) end) end) end)) as max_late_pmt3,	
	max((case when fp.numero <> 4 then null else (case when ParcSubst = 4 and VencParcSubst is not null then (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - vencparcsubst else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em  then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - VencParcSubst end) else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em end) end) end) end)) as max_late_pmt4,	
	max((case when fp.numero <> 5 then null else (case when ParcSubst = 5 and VencParcSubst is not null then (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - vencparcsubst else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em  then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - VencParcSubst end) else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em end) end) end) end)) as max_late_pmt5,	
	max((case when fp.numero <> 6 then null else (case when ParcSubst = 6 and VencParcSubst is not null then (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - vencparcsubst else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em  then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - VencParcSubst end) else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < fp.vencimento_em then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - fp.vencimento_em end) end) end) end)) as max_late_pmt6
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada' and fp.numero <= 6
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	where fp.status not in ('Cancelada','EmEspera') and lr.loan_date + interval '7 months' <= current_date
	group by 1) as MOB7_soft on MOB7_soft.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO MOB 6 PLANO ORIGINAL AGGRESSIVE
left join(select lr.loan_request_id,
	max(fp.numero) filter (where fp.pago_em <= (lr.loan_date + interval '7 months')::date) as ultparcpaga,
	count(*) filter (where fp.vencimento_em <= (lr.loan_date + interval '7 months')::date) as qtd_parcelas,
	max((case when fp.numero <> 1 then null else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < pmt1_venc then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - pmt1_venc else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - pmt1_venc end) end) end)) as max_late_pmt1,	
	max((case when fp.numero <> 2 then null else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < pmt2_venc then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - pmt2_venc else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - pmt2_venc end) end) end)) as max_late_pmt2,	
	max((case when fp.numero <> 3 then null else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < pmt3_venc then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - pmt3_venc else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - pmt3_venc end) end) end)) as max_late_pmt3,	
	max((case when fp.numero <> 4 then null else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < pmt4_venc then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - pmt4_venc else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - pmt4_venc end) end) end)) as max_late_pmt4,	
	max((case when fp.numero <> 5 then null else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < pmt5_venc then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - pmt5_venc else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - pmt5_venc end) end) end)) as max_late_pmt5,	
	max((case when fp.numero <> 6 then null else (case when (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) < pmt6_venc then null else (case when fp.pago_em is null then (case when (lr.loan_date + interval '7 months')::date > current_date then current_date else (lr.loan_date + interval '7 months')::date end) - pmt6_venc else (case when (lr.loan_date + interval '7 months')::date > fp.pago_em then fp.pago_em else (lr.loan_date + interval '7 months')::date end) - pmt6_venc end) end) end)) as max_late_pmt6
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	join(select fpp.emprestimo_id, 
			min(fp.vencimento_em) filter (where fp.numero = 1 and fpp.id = case when min_id is null then min_id2 else min_id end) as pmt1_venc,
			min(fp.vencimento_em) filter (where fp.numero = 2 and fpp.id = case when min_id is null then min_id2 else min_id end) as pmt2_venc,
			min(fp.vencimento_em) filter (where fp.numero = 3 and fpp.id = case when min_id is null then min_id2 else min_id end) as pmt3_venc,
			min(fp.vencimento_em) filter (where fp.numero = 4 and fpp.id = case when min_id is null then min_id2 else min_id end) as pmt4_venc,
			min(fp.vencimento_em) filter (where fp.numero = 5 and fpp.id = case when min_id is null then min_id2 else min_id end) as pmt5_venc,
			min(fp.vencimento_em) filter (where fp.numero = 6 and fpp.id = case when min_id is null then min_id2 else min_id end) as pmt6_venc
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, min(fpp.id) filter (where fp.numero = 6) as min_id,min(fpp.id) as min_id2
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1) as max_venc_6M on max_venc_6M.emprestimo_id = fpp.emprestimo_id 
	where fp.status not in ('Cancelada','EmEspera') and lr.loan_date + interval '7 months' <= current_date
	group by 1) as MOB7_aggressive on MOB7_aggressive.loan_request_id = lr.loan_request_id
--------------------------TABLE CLIENTS
left join (select lr.loan_request_id, t1.is_renew
	from offers o 
	join loan_requests lr on lr.offer_id = o.offer_id 
	join (select c.client_id, lr.loan_date, count(*) filter (where lr.loan_date > t1.request_date) as is_renew
		from clients c
		join offers o on o.client_id = c.client_id
		join loan_requests lr on lr.offer_id = o.offer_id
		join (select c.client_id, lr.loan_request_id, lr.loan_date as request_date
			from clients c
			join offers o on o.client_id = c.client_id
			join loan_requests lr on lr.offer_id = o.offer_id
			where lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')) as t1 on t1.client_id = c.client_id
		where lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18')
		group by 1,2) as t1 on t1.client_id = o.client_id and t1.loan_date = lr.loan_date) as isrenew on isrenew.loan_request_id = lr.loan_request_id
--------------------------TABLE TAGS
left join (select dp.direct_prospect_id, 
		max(case when t.nome = 'AA<=10-v1' then 1 else 0 end) as app_auto,
		max(case when t.nome = 'Automático' then 1 else 0 end) as offer_auto,
		max(case when t.nome = 'InterestRate:Test1' then 1 else 0 end) as interest_rate_test1
	from direct_prospects dp
	join direct_prospects_tag dpt on dpt.directprospect_id = dp.direct_prospect_id
	join tags t on t.id = dpt.tag_id
	group by 1) as tags on tags.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SIGNERS ADDRESS
left join(select dp.direct_prospect_id,max(replace(sa.zip_code,'-','')) as signer_zipcode
	from direct_prospects dp
	join offers o on o.direct_prospect_id = dp.direct_prospect_id
	join loan_requests lr on lr.offer_id = o.offer_id
	join loan_requests_signers lrs on lrs.loan_request_id = lr.loan_request_id
	join signers s on s.signer_id = lrs.signer_id
	join signer_addresses sa on sa.address_id = s.address_id
	where case when dp.zip_code = '' then null else dp.zip_code end is null
	group by 1) as signer_address on signer_address.direct_prospect_id = dp.direct_prospect_id
--------------------------
where lr.status = 'ACCEPTED' and to_char(lr.loan_date,'Mon/yy') in ('Sep/18','Oct/18');
















