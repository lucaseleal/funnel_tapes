select count(*),count(distinct direct_prospect_id),count(distinct loan_request_id)
from bizcredit_approval_tape_full

create or replace view bizcredit_approval_tape_full as
select
--CADASTRAIS_FUNIL
	t1.direct_prospect_id,
	t1.loan_request_id,
	t1.status,
	t1.valor_pedido,
	t1.data_criacao,
	t1.target_reject::text,
	case dp.utm_source
		when 'estimulo2020' then 'estimulo2020' 
		when 'sebrae' then 'sebrae' 
		when 'credito_mca' then 'mca' 
		when 'magalu' then 'magalu'
		else 'biz'
	end as esteira,
	t1.periodo_controle,
	t1.renovacao,
	t20.lead_marketing_channel_group,
	t1.collection_total_due_installments,
	t1.collection_count_postponing_restructures,
	t1.collection_i_pmt_of_first_postponing,
	t1.collection_current_max_late_pmt1,
	t1.collection_current_max_late_pmt2,
	t1.collection_ever_by_pmt3_soft,
	t1.collection_ever_by_pmt4_aggressive,
	t1.collection_ever_by_pmt5_aggressive,
	t1.collection_ever_by_pmt6_aggressive,
--NOVO CHECKLIST
	case t6.is_shareholder when 1 then 'socio direto' when 0 then 'nao socio' end as new_identidade_relacao_solicitante,
	t4.score_emailage > 500 as new_identidade_email_fraude,
	coalesce(t5.distancia_ipstack >= 200, false) as new_identidade_problema_geolocalizacao,
	t19.levenshtein_distancia as new_materialidade_endereco_distintos,
	coalesce(t10.consulta_seguradora,0) > 0 as new_consulta_seguradora,
	coalesce(t10.consulta_fornecedores,0) > 0 as new_consulta_fornecedores,
	coalesce(t10.consulta_cobranca,0) > 0 as new_consulta_cobranca,
	coalesce(t10.consulta_factoring,0) > 0 as new_consulta_factoring,
	coalesce(t10.consulta_indefinido,0) > 0 as new_consulta_indefinido,
	coalesce(t10.consulta_proprio,0) > 0 as new_consulta_proprio,
	coalesce(t10.consulta_provedor_dado,0) > 0 as new_consulta_provedor_dado,
	coalesce(t10.consulta_financeira,0) > 0 as new_consulta_financeira,
	(greatest(t10.consulta_financeira,t10.consulta_cobranca,0) > 0 and greatest(t10.consulta_factoring,t10.consulta_fornecedores,t10.consulta_indefinido,t10.consulta_provedor_dado,t10.consulta_seguradora,0) = 0) as new_apenas_consulta_financeira,
	dp.employees > 0 as new_tem_empregados,
	greatest(t11.count_imoveis,t11.count_inpi_marcas,t11.count_obras,t11.count_pat,t11.tem_aeronave,t11.tem_exportacao,t11.tem_licenca_ambiental,t11.total_veiculos) > 0 as new_materialidade_neoway,
	(coalesce((t10.consulta_seguradora > 0)::int,0) +
		coalesce((t10.consulta_fornecedores > 0)::int,0) +
		coalesce((dp.employees > 0)::int,0) +
		coalesce((t9.imposto_saida > 0)::int,0) +
		coalesce((t9.salario_saida > 0)::int,0) +
		coalesce((t12.total_tribut_trab > 0)::int,0) +
		coalesce((greatest(t11.count_imoveis,t11.count_inpi_marcas,t11.count_obras,t11.count_pat,t11.tem_aeronave,t11.tem_exportacao,t11.tem_licenca_ambiental,t11.total_veiculos) > 0)::int,0)) as new_score_materialidade,
	coalesce(t6.signer_idade,extract(year from age(t1.data_criacao,t22.data_nascimento))) - dp.age < 24 as new_socio_major_iniciou_menos24,
	(dp.month_revenue >= 100000 and dp.age < 2) or (dp.month_revenue >= 200000 and dp.age < 3) as new_financeiro_receita_desproporcional,
	case when t8.thin_file = 1 then 'Sem carteira'
		when t8.long_term_debt_pf_curr * 1000 / dp.month_revenue >= 0.1 then 'Adequado ao porte'
		when t8.long_term_debt_pf_curr * 1000 / dp.month_revenue < 0.1 then 'Inferior ao porte'
	end as new_socio_major_carteira_credito,
	t6.signer_renda_mensal as new_socio_major_renda_mensal,
	(t13.TotalLawsuitsAsDefendant > 0)::int as new_processo_judicial_socios,
	coalesce(t6.signer_nivel_pep,'NAO IDENTIFICADO') in ('PRIMARIO','SECUNDARIO') as new_socio_major_pep,
	t14.funcionario_empresa_ativa > 0 as new_socio_major_funcionario_outra,
	case when t16.direct_prospect_id is not null then coalesce(t16.bens_capital_social_irpf_major_cpf,t16.bens_capital_social_irpf,0) end as new_socio_major_bens_informados_capital_social,
	case when t16.direct_prospect_id is not null then coalesce(t16.bens_imoveis_irpf_major_cpf,t16.bens_imoveis_irpf,0) end as new_socio_major_bens_informados_imoveis,
	case when t16.direct_prospect_id is not null then coalesce(t16.bens_dinheiro_irpf_major_cpf,t16.bens_dinheiro_irpf,0) end as new_socio_major_bens_informados_dinheiro,
	case when t16.direct_prospect_id is not null then coalesce(t16.bens_outros_irpf_major_cpf,t16.bens_outros_irpf,0) end as new_socio_major_bens_informados_outros,
	case when t16.direct_prospect_id is not null then coalesce(t16.bens_investimento_irpf_major_cpf,t16.bens_investimento_irpf,0) end as new_socio_major_bens_informados_investimento,
	case when t16.direct_prospect_id is not null then coalesce(t16.bens_moveis_irpf_major_cpf,t16.bens_moveis_irpf,0) end as new_socio_major_bens_informados_moveis,
	case when t16.direct_prospect_id is not null then coalesce(t16.bens_qualquer_irpf_major_cpf,t16.bens_qualquer_irpf,0) end as new_socio_major_bens_informados_qualquer,
	case when t16.direct_prospect_id is not null then 
		case 
			when coalesce(t16.valor_irpf_major_cpf,valor_irpf,0) > 1000000 then '> 1MM'
			when coalesce(t16.valor_irpf_major_cpf,valor_irpf,0) > 500000 then '> 500k'
			when coalesce(t16.valor_irpf_major_cpf,valor_irpf,0) > 100000 then '> 100k'
			when coalesce(t16.valor_irpf_major_cpf,valor_irpf,0) > 50000 then '> 50k'
			when coalesce(t16.valor_irpf_major_cpf,valor_irpf,0) > 0 then '> 0'
			when coalesce(t16.valor_irpf_major_cpf,valor_irpf,0) <= 0 then '<= 0'
		end 
	end as new_socio_major_valor_ativos,
	case when t16.direct_prospect_id is not null then coalesce(t16.dependentes_irpf_major_cpf,dependentes_irpf,0) > 0 end as new_socio_major_dependentes,
	coalesce(t10.consulta_factoring,0) > 0 as new_financeiro_factoring,
	case when t1.renovacao > 0 or t15.count_capital_giro_c_fin_s_veiculo_c_micro >= 6 then 'Sim' 
		when t15.count_capital_giro_c_fin_s_veiculo_c_micro >= 3 then 'Parcial'
		when t15.count_capital_giro_c_fin_s_veiculo_c_micro < 3 then 'Nao'
	end as new_financeiro_acostumado_credito,
	(case when t9.loan_request_id is not null then
		(case
			(case when coalesce(t9.entradas_operacional1_30,0) / greatest(t9.entradas_operacional1_60,1) -1  > 0.25 then 'C'
				when coalesce(t9.entradas_operacional1_30,0) / greatest(t9.entradas_operacional1_60,1) -1  < -0.25 then 'D'
				when abs(coalesce(t9.entradas_operacional1_30,0) / greatest(t9.entradas_operacional1_60,1) -1) < 0.25 then 'E'
				else 'O' end)
				||
			(case when coalesce(t9.entradas_operacional1_60,0) / greatest(t9.entradas_operacional1_over60,1) -1  > 0.25 then 'C'
				when coalesce(t9.entradas_operacional1_60,0) / greatest(t9.entradas_operacional1_over60,1) -1  < -0.25 then 'D'
				when abs(coalesce(t9.entradas_operacional1_60,0) / greatest(t9.entradas_operacional1_over60,1) -1) < 0.25 then 'E'
				else 'O' end)
			when 'CC' then 'Crescente'
			when 'DD' then 'Decrescente'
			when 'EC' then 'Estavel'
			when 'ED' then 'Estavel'
			when 'EO' then 'Estavel'
			when 'CE' then 'Estavel'
			when 'DE' then 'Estavel'
			when 'OE' then 'Estavel'
			when 'EE' then 'Estavel'
			else 'Oscilante'
		end) 
	end) as new_financeiro_comportamento_faturamento,
	case when t9.loan_request_id is not null then (coalesce(t9.entradas_operacional1_30,0) + coalesce(t9.entradas_operacional1_60,0) + coalesce(t9.entradas_operacional1_over60,0)) / 3 end::double precision as new_financeiro_faturamento_comprovado,
	t9.imposto_saida > 0 as new_financeiro_pagamento_impostos,
	t9.salario_saida > 0 as new_financeiro_pgto_salarios,
	t9.ccf_entrada > 0 as new_financeiro_ccf,
	t9.governo_entrada > 0 as new_sacado_governo,
	t17.household_activity as new_complexidade_cnpj_mesmo_endereco,
	t17.household_owners as new_complexidade_empresas_endereco_socio_major,
	case coalesce(dp.phantasy_name,'') when '' then 2 else t17.tradename_city end as new_complexidade_cnpj_mesmo_nome,
	(t18.TotalCompanies > 1)::int as new_complexidade_socios_relacionados_mesmo_endereco,
	t12.total_tribut_trab > 0 as new_processo_tribut_trabal_pj,
	t12.total_trabalhista > 0  as new_processo_trabalhista_pj,
	t12.total_tributaria > 0 as new_processo_tributaria_pj,
	t12.total_criminal > 0 as new_processo_criminal_pj,
	t12.total_civel_admin > 0 as new_processo_civil_admin_pj,
	t12.reu_total as new_processo_judicial_reu_pj,
	t13.TotalLawsuitsAsDefendant + t12.reu_total as new_score_processe_judicial,
	(coalesce((t6.signer_idade - dp.age < 24)::int,0) + 
		coalesce(((dp.month_revenue >= 100000 and dp.age < 2) or (dp.month_revenue >= 200000 and dp.age < 3))::int,0) + 
		coalesce((t8.thin_file = 1)::int,0) +
		coalesce((t6.is_shareholder = 0)::int,0) +
		coalesce(t17.household_owners,0)) as new_score_complexidade,
	case when left(t20.vinculo,5) in ('Sócio','Sócia') or t20.vinculo = 'Presidente' then 'Socio'
		when t20.vinculo in ('Administrador','Procurador') or (t20.vinculo = 'Outros' and t20.lead_marketing_channel_group != 'agent') then 'Admin/procurador'
		else 'Outros'
	end as new_solicitante_vinculo_admin,
	t2.loan_approved_by_bizbot,
	case when dp.opt_in_date::date < '2018-03-13' then 
			(case when bizu_score >= 850 then 'A+' when bizu_score >= 700 then 'A-' when bizu_score >= 650 then 'B+' when bizu_score >= 600 then 'B-' when bizu_score >= 500 then 'C+' when bizu_score >= 400 then 'C-' when bizu_score >= 300 then 'D' when bizu_score < 300 then 'E' end)
		when dp.opt_in_date::date < '2019-10-15' then 
			(case when dp.bizu2_score >= 1100 then 'A+' when dp.bizu2_score >= 850 then 'A-' when dp.bizu2_score >= 720 then 'B+' when dp.bizu2_score >= 600 then 'B-' when dp.bizu2_score >= 500 then 'C+' when dp.bizu2_score >= 400 then 'C-' when dp.bizu2_score >= 300 then 'D' when dp.bizu2_score >= 200 then 'E' when dp.bizu2_score < 200 then 'F' end)
		when dp.opt_in_date::date = '2019-10-15' then 
			coalesce(case when dp.bizu21_score >= 1200 then 'A+' when dp.bizu21_score >= 950 then 'A-' when dp.bizu21_score >= 750 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end,
				case when dp.bizu2_score >= 1100 then 'A+' when dp.bizu2_score >= 850 then 'A-' when dp.bizu2_score >= 720 then 'B+' when dp.bizu2_score >= 600 then 'B-' when dp.bizu2_score >= 500 then 'C+' when dp.bizu2_score >= 400 then 'C-' when dp.bizu2_score >= 300 then 'D' when dp.bizu2_score >= 200 then 'E' when dp.bizu2_score < 200 then 'F' end)
		when dp.direct_prospect_id < 503019 then
			(case when dp.bizu21_score >= 1200 then 'A+' when dp.bizu21_score >= 950 then 'A-' when dp.bizu21_score >= 750 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end)
		else
			(case when dp.bizu21_score >= 1100 then 'A+' when dp.bizu21_score >= 850 then 'A-' when dp.bizu21_score >= 720 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end)
	end	as lead_classe_Bizux,
	t8.long_term_debt_pf_curr * 1000 as carteira_credito_socio_major,
	coalesce(t6.signer_data_nascimento,t22.data_nascimento) as data_nascimento_socio_major,
	dp.age as neoway_company_age,
	lr.portfolio_id,
	t1.decisao_final,
	t15.carteiracredito_pj_curr * 1000 as carteira_credito_pj,
	coalesce(t21.serasa_coleta,dp.serasa_4) as serasa_score,
	dp.month_revenue as lead_informed_month_revenue,
	dp.client_id,
	row_number() over (partition by dp.client_id order by case t1.status when 'ACCEPTED' then 1 else 2 end,t1.renovacao,t1.data_criacao desc) as indice_pedido,
	t23.ever_mob12 as bad_scr_ever_mob12,
	t23.over_mob12 as bad_scr_over_mob12,
	lt.collection_ever_by_pmt9_aggressive,
	lt.collection_ever_by_pmt12_aggressive,
	lt.collection_over_by_pmt9_aggressive,
	lt.collection_over_by_pmt12_aggressive,
	coalesce(lt.collection_over_30_pmt12_aggressive,
		lt.collection_over_30_pmt11_aggressive,
		lt.collection_over_30_pmt10_aggressive,
		lt.collection_over_30_pmt9_aggressive,
		t23.over_mob12) as bad_oficial,
	lt.collection_ever_30_pmt8_aggressive,
	lt.collection_ever_30_pmt7_aggressive,
	lt.collection_amount_paid_pmt7,
	lt.collection_amount_paid_pmt8,
	lt.collection_amount_paid_pmt9,
	lt.collection_amount_paid_pmt10,
	lt.collection_amount_paid_pmt11,
	lt.collection_amount_paid_pmt12,
	lt.collection_original_p_plus_i,
	case lt.collection_i_pmt_of_first_postponing when 1 then lt.collection_current_max_late_pmt2 else lt.collection_current_max_late_pmt1 end as fpd_by,
	case when dp.opt_in_date::date < '2018-03-13' then dp.bizu_score when dp.opt_in_date::date < '2019-10-15' then dp.bizu2_score when dp.opt_in_date::date = '2019-10-15' then coalesce(dp.bizu21_score,dp.bizu2_score) else dp.bizu21_score end as lead_biz_bizux_score,
	case when lt.collection_over_30_pmt12_aggressive is not null then lt.collection_amount_paid_pmt12
		when lt.collection_over_30_pmt11_aggressive is not null then lt.collection_amount_paid_pmt11
		when lt.collection_over_30_pmt10_aggressive is not null then lt.collection_amount_paid_pmt10
		when lt.collection_over_30_pmt9_aggressive is not null then lt.collection_amount_paid_pmt9
	end as bad_oficial_amount_paid,
	coalesce(t24.prob,t32.bizu3)::numeric as bizu3_prob,
	case when dp.month_revenue <= 40000 then 
		case when coalesce(t24.prob,t32.bizu3) > 0.733226265 then 'F'
            when coalesce(t24.prob,t32.bizu3) <= 0.186673261 then 'A+'
            when coalesce(t24.prob,t32.bizu3) <= 0.269462858 then 'A-'
            when coalesce(t24.prob,t32.bizu3) <= 0.337087008 then 'B+'
            when coalesce(t24.prob,t32.bizu3) <= 0.431420484 then 'B-'
            when coalesce(t24.prob,t32.bizu3) <= 0.499934728 then 'C+'
            when coalesce(t24.prob,t32.bizu3) <= 0.559555658 then 'C-'
            when coalesce(t24.prob,t32.bizu3) <= 0.648852408 then 'D'
            when coalesce(t24.prob,t32.bizu3) <= 0.733226265 then 'E' end
	else
		case when coalesce(t24.prob,t32.bizu3) > 0.5362138 then 'F'
            when coalesce(t24.prob,t32.bizu3) <= 0.1300238 then 'A+'
            when coalesce(t24.prob,t32.bizu3) <= 0.2016423 then 'A-'
            when coalesce(t24.prob,t32.bizu3) <= 0.2583115 then 'B+'
            when coalesce(t24.prob,t32.bizu3) <= 0.3287529 then 'B-'
            when coalesce(t24.prob,t32.bizu3) <= 0.3721131 then 'C+'
            when coalesce(t24.prob,t32.bizu3) <= 0.4230161 then 'C-'
            when coalesce(t24.prob,t32.bizu3) <= 0.4789702 then 'D'
            when coalesce(t24.prob,t32.bizu3) <= 0.5362138 then 'E' end
	end as bizu3_classe9,
	t24.model as bizu3_model,
	coalesce(case when dp.sector = 'COMERCIO' then t33.prob else t34.prob end,t32.extrato,t25.prob)::numeric as extrato_score_prob,
	case when dp.sector = 'COMERCIO' then 
		coalesce(case t33.classe when 'A+' then 'G1' when 'A-' then 'G2' when 'B+' then 'G3' when 'B-' then 'G4' when 'C+' then 'G5' when 'C-' then 'G6' when 'D' then 'G7' when 'E' then 'G8' when 'F' then 'G9' end,
			case when coalesce(t33.prob,t32.extrato,t25.prob) > 0.648142059362271 then 'G9'
	            when coalesce(t33.prob,t32.extrato,t25.prob) <= 0.069296387755634 then 'G1'
	            when coalesce(t33.prob,t32.extrato,t25.prob) <= 0.139838204653436 then 'G2'
	            when coalesce(t33.prob,t32.extrato,t25.prob) <= 0.197658684467052 then 'G3'
	            when coalesce(t33.prob,t32.extrato,t25.prob) <= 0.279450022802669 then 'G4'
	            when coalesce(t33.prob,t32.extrato,t25.prob) <= 0.339935047581950 then 'G5'
	            when coalesce(t33.prob,t32.extrato,t25.prob) <= 0.449091990560027 then 'G6'
	            when coalesce(t33.prob,t32.extrato,t25.prob) <= 0.569230980598843 then 'G7'
	            when coalesce(t33.prob,t32.extrato,t25.prob) <= 0.648142059362271 then 'G8' end)
	else
		coalesce(case t34.classe when 'A+' then 'G1' when 'A-' then 'G2' when 'B+' then 'G3' when 'B-' then 'G4' when 'C+' then 'G5' when 'C-' then 'G6' when 'D' then 'G7' when 'E' then 'G8' when 'F' then 'G9' end,
			case when coalesce(t34.prob,t32.extrato,t25.prob) > 0.5867140590036290 then 'G9'
	            when coalesce(t34.prob,t32.extrato,t25.prob) <= 0.0799723232985681 then 'G1'
	            when coalesce(t34.prob,t32.extrato,t25.prob) <= 0.1298918309166860 then 'G2'
	            when coalesce(t34.prob,t32.extrato,t25.prob) <= 0.1899916318112840 then 'G3'
	            when coalesce(t34.prob,t32.extrato,t25.prob) <= 0.2593764971028610 then 'G4'
	            when coalesce(t34.prob,t32.extrato,t25.prob) <= 0.3098592249347670 then 'G5'
	            when coalesce(t34.prob,t32.extrato,t25.prob) <= 0.3996748667231790 then 'G6'
	            when coalesce(t34.prob,t32.extrato,t25.prob) <= 0.4692453762916790 then 'G7'
	            when coalesce(t34.prob,t32.extrato,t25.prob) <= 0.5867140590036290 then 'G8' end)
	end as extrato_score_classe9,
	t26.prob_m2 as underwrite_score_m2_prob,
	t26.prob_m3 as underwrite_score_m3_prob,
	t26.classe7_m2 as underwrite_score_m2_classe7,
	t26.classe5_m3 as underwrite_score_m3_classe5,
	coalesce(lt.loan_original_final_term,lr.number_of_installments) as prazo_pedido,
	case when dp.opt_in_date::date < '2019-10-15' then dp.bizu2_score when dp.opt_in_date::date = '2019-10-15' then coalesce(dp.bizu21_score,dp.bizu2_score) else dp.bizu21_score end as lead_biz_bizu2_score,
	case when dp.opt_in_date::date < '2019-10-15' then 
			(case when dp.bizu2_score >= 1100 then 'A+' when dp.bizu2_score >= 850 then 'A-' when dp.bizu2_score >= 720 then 'B+' when dp.bizu2_score >= 600 then 'B-' when dp.bizu2_score >= 500 then 'C+' when dp.bizu2_score >= 400 then 'C-' when dp.bizu2_score >= 300 then 'D' when dp.bizu2_score >= 200 then 'E' when dp.bizu2_score < 200 then 'F' end)
		when dp.opt_in_date::date = '2019-10-15' then 
			coalesce(case when dp.bizu21_score >= 1200 then 'A+' when dp.bizu21_score >= 950 then 'A-' when dp.bizu21_score >= 750 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end,
				case when dp.bizu2_score >= 1100 then 'A+' when dp.bizu2_score >= 850 then 'A-' when dp.bizu2_score >= 720 then 'B+' when dp.bizu2_score >= 600 then 'B-' when dp.bizu2_score >= 500 then 'C+' when dp.bizu2_score >= 400 then 'C-' when dp.bizu2_score >= 300 then 'D' when dp.bizu2_score >= 200 then 'E' when dp.bizu2_score < 200 then 'F' end)
		when dp.direct_prospect_id < 503019 then
			(case when dp.bizu21_score >= 1200 then 'A+' when dp.bizu21_score >= 950 then 'A-' when dp.bizu21_score >= 750 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end)
		else
			(case when dp.bizu21_score >= 1100 then 'A+' when dp.bizu21_score >= 850 then 'A-' when dp.bizu21_score >= 720 then 'B+' when dp.bizu21_score >= 580 then 'B-' when dp.bizu21_score >= 500 then 'C+' when dp.bizu21_score >= 400 then 'C-' when dp.bizu21_score >= 300 then 'D' when dp.bizu21_score >= 200  then 'E' when dp.bizu21_score < 200 then 'F' end)
	end	as lead_classe_Bizu2,
	t23.got_credit_super_extended as bad_scr_got_credit,
	lr.valor_solicitado as valor_solicitado_balcao,
	t1.valor_pedido / lr.valor_solicitado as valor_balcao_sobre_valor_final,
	case when dp.opt_in_date::date < '2019-10-15' then dp.pd_bizu2 when dp.opt_in_date::date = '2019-10-15' then coalesce(dp.pd_bizu21,dp.pd_bizu2) else dp.pd_bizu21 end as lead_biz_bizu2_pd,
	t13.total_tribut_trab > 0 as new_processo_tribut_trabal_socio_major,
	t13.total_trabalhista > 0  as new_processo_trabalhista_socio_major,
	t13.total_tributaria > 0 as new_processo_tributaria_socio_major,
	t13.total_criminal > 0 as new_processo_criminal_socio_major,
	t13.total_civel_admin > 0 as new_processo_civil_admin_socio_major,
	t13.reu_total as new_processo_judicial_reu_socio_major,
	t13.reu_tribut_trab > 0 as new_processo_tribut_trabal_reu_socio_major,
	t13.reu_trabalhista > 0  as new_processo_trabalhista_reu_socio_major,
	t13.reu_tributaria > 0 as new_processo_tributaria_reu_socio_major,
	t13.reu_criminal > 0 as new_processo_criminal_reu_socio_major,
	t13.reu_civel_admin > 0 as new_processo_civil_admin_reu_socio_major,
	t12.reu_tribut_trab > 0 as new_processo_tribut_trabal_reu_pj,
	t12.reu_trabalhista > 0  as new_processo_trabalhista_reu_pj,
	t12.reu_tributaria > 0 as new_processo_tributaria_reu_pj,
	t12.reu_criminal > 0 as new_processo_criminal_reu_pj,
	t12.reu_civel_admin > 0 as new_processo_civil_admin_reu_pj,
	t14.funcionario_empresa_ativa as new_socio_major_funcionario_outra_numeric,
	case when t9.loan_request_id is not null then (coalesce(t9.entradas_operacional1_30,0) + coalesce(t9.entradas_operacional1_60,0) + coalesce(t9.entradas_operacional1_over60,0)) / 3 end::numeric / coalesce(case when position(',' in t30.financeiro_faturamento_errado) > 0 then regexp_replace(t30.financeiro_faturamento_errado,'.*,','') end::numeric,dp.month_revenue) as new_financeiro_faturamento_comprovado_perc,
	coalesce(t10.consulta_fornecedores,0)::int as new_consulta_fornecedores_numeric,
	dp.amount_requested as lead_application_amount,
	greatest((greatest(t10.consulta_fornecedores,0) > 0 and greatest(t10.consulta_factoring,t10.consulta_indefinido,t10.consulta_provedor_dado,t10.consulta_seguradora,t10.consulta_financeira,t10.consulta_cobranca,t10.consulta_proprio,0) = 0)::int ,
		(greatest(t10.consulta_seguradora,0) > 0 and greatest(t10.consulta_factoring,t10.consulta_indefinido,t10.consulta_fornecedores,t10.consulta_provedor_dado,t10.consulta_financeira,t10.consulta_cobranca,t10.consulta_proprio,0) = 0)::int ,		
		coalesce((greatest(t11.count_imoveis,t11.count_inpi_marcas,t11.count_obras,t11.count_pat,t11.tem_aeronave,t11.tem_exportacao,t11.tem_licenca_ambiental,t11.total_veiculos) > 0)::int,0)) as new_bem_consultado,
	t14.funcionario_empresa_ativa > 1 as new_socio_major_funcionario_2_ou_mais_outras,
	t27.m5v20_prob as mesa_score_m5v20_prob,
	t27.m5v20_classe as mesa_score_m5v20_classe10,
	o.interest_rate as final_offer_interest_rate,
	o.max_number_of_installments as final_offer_max_term,
	o.max_value as final_offer_max_value,
	lr.prazo_solicitado as loan_requested_term,
	li.total_payment loan_gross_value,
	lr.taxa_final as loan_final_net_interest_rate,
	lr.number_of_installments as loan_original_final_term,
	lt.collection_original_p_plus_i - 
		case when lt.collection_over_30_pmt12_aggressive is not null then lt.collection_amount_paid_pmt12
			when lt.collection_over_30_pmt11_aggressive is not null then lt.collection_amount_paid_pmt11
			when lt.collection_over_30_pmt10_aggressive is not null then lt.collection_amount_paid_pmt10
			when lt.collection_over_30_pmt9_aggressive is not null then lt.collection_amount_paid_pmt9
		end as collection_bad_oficial_ead,
	dp.sector,
--	t27.m5v21_prob as underwrite_final_score_1_prob,
--	t27.m5v21_classe as underwrite_final_score_1_classe,
	lucas_leal.mesa_score_prob((t13.TotalLawsuitsAsDefendant > 0),
			coalesce(t10.consulta_cobranca,0) > 0,
			coalesce(t10.consulta_factoring,0) > 0,
			coalesce(t10.consulta_financeira,0) > 0,
			coalesce(t10.consulta_fornecedores,0) > 0,
			coalesce(t10.consulta_indefinido,0) > 0,
			coalesce(t10.consulta_proprio,0) > 0,
			coalesce(t10.consulta_provedor_dado,0) > 0,
			coalesce(t10.consulta_seguradora,0) > 0,
			greatest(t11.count_imoveis,t11.count_inpi_marcas,t11.count_obras,t11.count_pat,t11.tem_aeronave,t11.tem_exportacao,t11.tem_licenca_ambiental,t11.total_veiculos) > 0,
			coalesce(t24.prob,t32.bizu3)::numeric,
			case when dp.opt_in_date::date < '2019-10-15' then dp.pd_bizu2 when dp.opt_in_date::date = '2019-10-15' then coalesce(dp.pd_bizu21,dp.pd_bizu2) else dp.pd_bizu21 end::numeric,
			coalesce(case when dp.sector = 'COMERCIO' then t33.prob else t34.prob end,t32.extrato,t25.prob)::numeric) as underwrite_final_score_1_prob,
	lucas_leal.mesa_score_class(lucas_leal.mesa_score_prob((t13.TotalLawsuitsAsDefendant > 0),
			coalesce(t10.consulta_cobranca,0) > 0,
			coalesce(t10.consulta_factoring,0) > 0,
			coalesce(t10.consulta_financeira,0) > 0,
			coalesce(t10.consulta_fornecedores,0) > 0,
			coalesce(t10.consulta_indefinido,0) > 0,
			coalesce(t10.consulta_proprio,0) > 0,
			coalesce(t10.consulta_provedor_dado,0) > 0,
			coalesce(t10.consulta_seguradora,0) > 0,
			greatest(t11.count_imoveis,t11.count_inpi_marcas,t11.count_obras,t11.count_pat,t11.tem_aeronave,t11.tem_exportacao,t11.tem_licenca_ambiental,t11.total_veiculos) > 0,
			coalesce(t24.prob,t32.bizu3)::numeric,
			case when dp.opt_in_date::date < '2019-10-15' then dp.pd_bizu2 when dp.opt_in_date::date = '2019-10-15' then coalesce(dp.pd_bizu21,dp.pd_bizu2) else dp.pd_bizu21 end::numeric,
			coalesce(case when dp.sector = 'COMERCIO' then t33.prob else t34.prob end,t32.extrato,t25.prob)::numeric)) as underwrite_final_score_1_classe,
	t27.m5v20_clean_prob as underwrite_final_score_2_prob,
	t27.m5v20_clean_classe as underwrite_final_score_2_classe,
	t27.m5v21_sem_extrato_prob as underwrite_m5v20_sem_extrato_prob,
	1 / (1 + EXP(-(-ln((1 / t27.m5v21_sem_extrato_prob) - 1) + -0.788829002883016))) as prob_v21_minimo,
	1 / (1 + EXP(-(-ln((1 / t27.m5v21_sem_extrato_prob) - 1) + 0.375689953352867))) as prob_v21_maximo,
	coalesce(t28.motivo_alteracao,case round(t29.proved_informed_revenue::numeric,2) when 1 then 'Faturamento comprovado' when 0 then 'Faturamento nao comprovado' when 0.5 then 'Faturamento parcialmente comprovado' end) as motivo_alteracao,
	round(t30.financeiro_faturamento_comprovado::numeric,2) / coalesce(case when position(',' in t30.financeiro_faturamento_errado) > 0 then regexp_replace(t30.financeiro_faturamento_errado,'.*,','') end::numeric,dp.month_revenue) as perc_bizcredit_faturamento_comprovado,
	round(t31.faturamento_medio::numeric,2) / coalesce(case when position(',' in t30.financeiro_faturamento_errado) > 0 then regexp_replace(t30.financeiro_faturamento_errado,'.*,','') end::numeric,dp.month_revenue) as perc_bizextrato_faturamento_comprovado,
	round(t30.financeiro_faturamento_comprovado::numeric,2) as bizcredit_faturamento_comprovado,
	round(t31.faturamento_medio::numeric,2) as bizextrato_faturamento_comprovado,
	case lt.collection_i_pmt_of_first_postponing when 2 then lt.collection_current_max_late_pmt3 else lt.collection_current_max_late_pmt2 end as spd_by,
	round(greatest(t31.faturamento_medio,case when t9.loan_request_id is not null then (coalesce(t9.entradas_operacional1_30,0) + coalesce(t9.entradas_operacional1_60,0) + coalesce(t9.entradas_operacional1_over60,0)) / 3 end)::numeric,2) as faturamento_comprovado_oficial,
	round(greatest(t31.faturamento_medio,case when t9.loan_request_id is not null then (coalesce(t9.entradas_operacional1_30,0) + coalesce(t9.entradas_operacional1_60,0) + coalesce(t9.entradas_operacional1_over60,0)) / 3 end)::numeric,2) / coalesce(case when position(',' in t30.financeiro_faturamento_errado) > 0 then regexp_replace(t30.financeiro_faturamento_errado,'.*,','') end::numeric,dp.month_revenue) as faturamento_comprovado_oficial_perc,
	t27.m5v21_s_ext_s_jud_pob as underwrite_m5v20_s_ext_s_proc_jud_socio_prob,
	coalesce(case when position(',' in t30.financeiro_faturamento_errado) > 0 then regexp_replace(t30.financeiro_faturamento_errado,'.*,','') end::numeric,dp.month_revenue) as faturamento_informado_corrigido,
	case lt.collection_i_pmt_of_first_postponing when 3 then lt.collection_current_max_late_pmt4 else lt.collection_current_max_late_pmt3 end as tpd_by,
	dp.behaviour_score,
	dp.behaviour_zone,
	dp.renewal_zone	
from t_bizcredit_approval_tape_full t1
join public.direct_prospects dp on dp.direct_prospect_id = t1.direct_prospect_id
join t_bizcredit_parecer_full t2 on t2.emprestimo_id = t1.loan_request_id
join public.loan_requests lr on lr.loan_request_id = t2.emprestimo_id
join public.offers o on o.offer_id = lr.offer_id
left join public.loan_infos li on li.loan_info_id = lr.loan_request_id
left join loan_tape_full lt on lt.lead_id = t1.direct_prospect_id
left join t_bizcredit_emailage_full t4 on t4.emprestimo_id = t1.loan_request_id
left join t_bizcredit_ipstack_full t5 on t5.emprestimo_id = t1.loan_request_id
left join t_bizcredit_signers_full t6 on t6.loan_request_id = t1.loan_request_id
left join t_bizcredit_endereco_balcao_full t7 on t7.direct_prospect_id = t1.direct_prospect_id
left join t_bizcredit_scr_cpf_major_full t8 on t8.lead_id = t1.direct_prospect_id
left join lucas_leal.t_bizcredit_transactions_corrigida t9 on t9.loan_request_id = t1.loan_request_id --base com filtro de data para analises posteriores ao emprestimo
--left join lucas_leal.mv_bizcredit_transactions t9 on t9.loan_request_id = t1.loan_request_id
left join t_bizcredit_consultas_serasa_full t10 on t10.direct_prospect_id = t1.direct_prospect_id
left join t_bizcredit_materialidade_neoway_full t11 on t11.emprestimo_id = t1.loan_request_id
left join (select
		t2.direct_prospect_id,
		count(distinct id_processo) filter (where tipo_parte in ('DEFENDANT','CLAIMED') and t1.cnpj = t1.doc_parte and t1.data_notificacao_processo::date <= t2.data_criacao::date) as reu_total,
		count(distinct id_processo) filter (where t1.tribunal_processo in ('TRABALHISTA','TRIBUTARIA','FAZENDA','PREVIDENCIARIA') and t1.cnpj = t1.doc_parte and t1.data_notificacao_processo::date <= t2.data_criacao::date) as total_tribut_trab,
		count(distinct id_processo) filter (where t1.tribunal_processo in ('TRABALHISTA','TRIBUTARIA','FAZENDA','PREVIDENCIARIA') and t1.cnpj = t1.doc_parte and t1.data_notificacao_processo::date <= t2.data_criacao::date and t1.tipo_parte in ('DEFENDANT','CLAIMED')) as reu_tribut_trab,
		count(distinct id_processo) filter (where t1.tribunal_processo = 'TRABALHISTA' and t1.cnpj = t1.doc_parte and t1.data_notificacao_processo::date <= t2.data_criacao::date) as total_trabalhista,
		count(distinct id_processo) filter (where t1.tribunal_processo = 'TRABALHISTA' and t1.cnpj = t1.doc_parte and t1.data_notificacao_processo::date <= t2.data_criacao::date and t1.tipo_parte in ('DEFENDANT','CLAIMED')) as reu_trabalhista,
		count(distinct id_processo) filter (where t1.tribunal_processo in ('TRIBUTARIA','FAZENDA','PREVIDENCIARIA') and t1.cnpj = t1.doc_parte and t1.data_notificacao_processo::date <= t2.data_criacao::date) as total_tributaria,
		count(distinct id_processo) filter (where t1.tribunal_processo in ('TRIBUTARIA','FAZENDA','PREVIDENCIARIA') and t1.cnpj = t1.doc_parte and t1.data_notificacao_processo::date <= t2.data_criacao::date and t1.tipo_parte in ('DEFENDANT','CLAIMED')) as reu_tributaria,
		count(distinct id_processo) filter (where t1.tribunal_processo like '%CRIMINAL%' and t1.cnpj = t1.doc_parte and t1.data_notificacao_processo::date <= t2.data_criacao::date) as total_criminal,
		count(distinct id_processo) filter (where t1.tribunal_processo like '%CRIMINAL%' and t1.cnpj = t1.doc_parte and t1.data_notificacao_processo::date <= t2.data_criacao::date and t1.tipo_parte in ('DEFENDANT','CLAIMED')) as reu_criminal,
		count(distinct id_processo) filter (where t1.tribunal_processo in ('ADMINISTRATIVA','CIVEL','ESPECIAL CIVEL') and t1.cnpj = t1.doc_parte and t1.data_notificacao_processo::date <= t2.data_criacao::date) as total_civel_admin,
		count(distinct id_processo) filter (where t1.tribunal_processo in ('ADMINISTRATIVA','CIVEL','ESPECIAL CIVEL') and t1.cnpj = t1.doc_parte and t1.data_notificacao_processo::date <= t2.data_criacao::date and t1.tipo_parte in ('DEFENDANT','CLAIMED')) as reu_civel_admin
	from t_bizcredit_approval_tape_full t2
	join public.direct_prospects dp on t2.direct_prospect_id = dp.direct_prospect_id
	join t_bizcredit_processos_judiciais_pj t1 on t1.cnpj = dp.cnpj
	group by 1) as t12 on t12.direct_prospect_id = t1.direct_prospect_id
left join (select
		t2.loan_request_id,
		max(TotalLawsuitsAsDefendant) as TotalLawsuitsAsDefendant,
		count(distinct t1.id_processo) filter (where t1.tipo_parte in ('DEFENDANT','CLAIMED') and t1.cpf = t1.doc_parte and t1.data_notificacao_processo::date <= t3.data_criacao::date) as reu_total,
		count(distinct t1.id_processo) filter (where t1.tribunal_processo in ('TRABALHISTA','TRIBUTARIA','FAZENDA','PREVIDENCIARIA') and t1.cpf = t1.doc_parte and t1.data_notificacao_processo::date <= t3.data_criacao::date) as total_tribut_trab,
		count(distinct t1.id_processo) filter (where t1.tribunal_processo in ('TRABALHISTA','TRIBUTARIA','FAZENDA','PREVIDENCIARIA') and t1.cpf = t1.doc_parte and t1.data_notificacao_processo::date <= t3.data_criacao::date and t1.tipo_parte in ('DEFENDANT','CLAIMED')) as reu_tribut_trab,
		count(distinct t1.id_processo) filter (where t1.tribunal_processo = 'TRABALHISTA' and t1.cpf = t1.doc_parte and t1.data_notificacao_processo::date <= t3.data_criacao::date) as total_trabalhista,
		count(distinct t1.id_processo) filter (where t1.tribunal_processo = 'TRABALHISTA' and t1.cpf = t1.doc_parte and t1.data_notificacao_processo::date <= t3.data_criacao::date and t1.tipo_parte in ('DEFENDANT','CLAIMED')) as reu_trabalhista,
		count(distinct t1.id_processo) filter (where t1.tribunal_processo in ('TRIBUTARIA','FAZENDA','PREVIDENCIARIA') and t1.cpf = t1.doc_parte and t1.data_notificacao_processo::date <= t3.data_criacao::date) as total_tributaria,
		count(distinct t1.id_processo) filter (where t1.tribunal_processo in ('TRIBUTARIA','FAZENDA','PREVIDENCIARIA') and t1.cpf = t1.doc_parte and t1.data_notificacao_processo::date <= t3.data_criacao::date and t1.tipo_parte in ('DEFENDANT','CLAIMED')) as reu_tributaria,
		count(distinct t1.id_processo) filter (where t1.tribunal_processo like '%CRIMINAL%' and t1.cpf = t1.doc_parte and t1.data_notificacao_processo::date <= t3.data_criacao::date) as total_criminal,
		count(distinct t1.id_processo) filter (where t1.tribunal_processo like '%CRIMINAL%' and t1.cpf = t1.doc_parte and t1.data_notificacao_processo::date <= t3.data_criacao::date and t1.tipo_parte in ('DEFENDANT','CLAIMED')) as reu_criminal,
		count(distinct t1.id_processo) filter (where t1.tribunal_processo in ('ADMINISTRATIVA','CIVEL','ESPECIAL CIVEL') and t1.cpf = t1.doc_parte and t1.data_notificacao_processo::date <= t3.data_criacao::date) as total_civel_admin,
		count(distinct t1.id_processo) filter (where t1.tribunal_processo in ('ADMINISTRATIVA','CIVEL','ESPECIAL CIVEL') and t1.cpf = t1.doc_parte and t1.data_notificacao_processo::date <= t3.data_criacao::date and t1.tipo_parte in ('DEFENDANT','CLAIMED')) as reu_civel_admin
	from t_bizcredit_signers_full t2
	join t_bizcredit_approval_tape_full t3 on t3.loan_request_id = t2.loan_request_id
	join t_bizcredit_processos_judiciais_cpf t1 on t1.cpf = t2.major_signer_cpf
	group by 1) t13 on t13.loan_request_id = t1.loan_request_id
left join (select
		t1.cpf,
		count(distinct t1.cnpj) filter (where t1.status = 'ACTIVE' and t1.cnpj != dp.cnpj) as funcionario_empresa_ativa
	from t_bizcredit_bigdata_occupation_data t1
	join t_bizcredit_signers_full t2 on t2.major_signer_cpf = t1.cpf
	join t_bizcredit_approval_tape_full t3 on t3.loan_request_id = t2.loan_request_id
	join public.direct_prospects dp on dp.direct_prospect_id = t3.direct_prospect_id
	group by 1) t14 on t14.cpf = t6.major_signer_cpf
left join t_bizcredit_scr_pj_full t15 on t15.emprestimo_id = t1.loan_request_id
left join (select
		t1.direct_prospect_id,
		count(*) filter (where t4.major_signer_cpf is not null and t3.grupo_tratado = 'capital social') as bens_capital_social_irpf_major_cpf,
		count(*) filter (where t4.major_signer_cpf is not null and t3.grupo_tratado = 'bens im�veis') as bens_imoveis_irpf_major_cpf,
		count(*) filter (where t4.major_signer_cpf is not null and t3.grupo_tratado = 'dinheiro em posse') as bens_dinheiro_irpf_major_cpf,
		count(*) filter (where t4.major_signer_cpf is not null and t3.grupo_tratado = 'outros') as bens_outros_irpf_major_cpf,
		count(*) filter (where t4.major_signer_cpf is not null and t3.grupo_tratado = 'investimento') as bens_investimento_irpf_major_cpf,
		count(*) filter (where t4.major_signer_cpf is not null and t3.grupo_tratado = 'bens m�veis') as bens_moveis_irpf_major_cpf,
		count(*) filter (where t4.major_signer_cpf is not null and t3.grupo_tratado is not null) as bens_qualquer_irpf_major_cpf,
		sum(t1.valor_bens_direitos) filter (where t4.major_signer_cpf is not null) as valor_irpf_major_cpf,
		max(t1.count_dependentes) filter (where t4.major_signer_cpf is not null) as dependentes_irpf_major_cpf,
		count(*) filter (where t3.grupo_tratado = 'capital social') as bens_capital_social_irpf,
		count(*) filter (where t3.grupo_tratado = 'bens im�veis') as bens_imoveis_irpf,
		count(*) filter (where t3.grupo_tratado = 'dinheiro em posse') as bens_dinheiro_irpf,
		count(*) filter (where t3.grupo_tratado = 'outros') as bens_outros_irpf,
		count(*) filter (where t3.grupo_tratado = 'investimento') as bens_investimento_irpf,
		count(*) filter (where t3.grupo_tratado = 'bens m�veis') as bens_moveis_irpf,
		count(*) filter (where t3.grupo_tratado is not null) as bens_qualquer_irpf,
		sum(t1.valor_bens_direitos) as valor_irpf,
		max(t1.count_dependentes) as dependentes_irpf
	from lucas_leal.t_bizcredit_irpf_full t1
	join lucas_leal.t_bizcredit_approval_tape_full t2 on t2.direct_prospect_id = t1.direct_prospect_id
	left join t_codigos_irpf t3 on t3.codigo = t1.codigo_bens_direitos
	left join lucas_leal.t_bizcredit_signers_full t4 on t4.loan_request_id = t2.loan_request_id and t4.major_signer_cpf = t1.cpf
	group by 1) t16 on t16.direct_prospect_id = t1.direct_prospect_id
left join data_science.carga_3_rf as t17 on t17.direct_prospect_id = t1.direct_prospect_id
left join (select 
		cnpj,
		(jsonb_array_elements(data_output::jsonb)->'CompanyGroups'->0->>'TotalCompanies')::int as TotalCompanies,
		row_number() over (partition by cnpj order by (jsonb_array_elements(data_output::jsonb)->'CompanyGroups'->0->>'TotalCompanies')::int desc) as indice
	from lucas_leal.t_bigdata_company_group_household_owners_surname) as t18 on t18.cnpj = dp.cnpj and t18.indice = 1
left join data_science.compara_end_socio_balcao t19 on t19.direct_prospect_id = t1.direct_prospect_id
left join (select
		t.loan_request_id,
		dp.vinculo,
		coalesce(case upper(lucas_leal.treat_source_name(lucas_leal.translate_char_encode_uft8(dp.utm_source))) 
			when 'ADWORDS' then 'adwords'
			when 'AGENT' then 'agent'
			when 'BIDU' then 'bidu'
			when 'BING' then 'Bing'
			when 'CREDITA' then 'credita'
			when 'FACEBOOK' then 'facebook'
			when 'FACEBOOK_ORG' then 'facebook'
			when 'FINANZERO' then 'finanzero'
			when 'FINPASS' then 'finpass'
			when 'GERU' then 'geru'
			when 'INSTAGRAM' then 'instagram'
			when 'JUROSBAIXOS' then 'jurosbaixos'
			when 'KONKERO' then 'konkero'
			when 'LINKEDIN' then 'linkedin'
			when 'MGM' then 'member-get-member'
			when 'ORGANICO' then 'organico'
			when 'BLOG' then 'organico'
			when 'SITE' then 'organico'
			when 'DIRECT_CHANNEL' then 'organico'
			when 'ANDROID' then 'organico'
			when 'RENEWAL' then 'organico'
			when 'EMAIL' then 'organico'
			when 'RD' then 'organico'
			when 'RD#/' then 'organico'
			when 'RDSTATION' then 'organico'
			when 'RD+STATION' then 'organico'
			when 'AMERICANAS' then 'other' 
			when 'IFOOD' then 'other' 
			when 'PEIXE-URBANO' then 'other' 
			when 'OUTBRAIN' then 'outbrain'
			when 'TABOOLA' then 'taboola'
			when 'CHARLES/' then 'agent'
			when 'MARCELOROMERA' then 'agent'
			when 'PBCONSULTORIA' then 'agent'
			when 'HMSEGUROS' then 'agent'
			when 'COMPARAONLINE' then 'agent'
			when 'CREDEXPRESS#/' then 'agent'
			else case when pp.identificacao is not null then 'agent' else dp.utm_source end
		end,'other') as lead_marketing_channel_group
	from public.direct_prospects dp
	join t_bizcredit_approval_tape_full t on t.direct_prospect_id = dp.direct_prospect_id
	left join (select distinct identificacao from parceiros_parceiro) pp on pp.identificacao = replace(dp.utm_source,'#/','')) as t20 on t20.loan_request_id = t1.loan_request_id
left join t_bizcredit_serasa_score as t21 on t21.direct_prospect_id = t1.direct_prospect_id
left join t_bigdata_dados_basicos_pf as t22 on t22.cpf = t6.major_signer_cpf
left join t_backtest_bad_scr as t23 on t23.loan_request_id = t1.loan_request_id
left join t_anexo_bizu3_approval_tape_full as t24 on t24.loan_request_id = t1.loan_request_id
left join t_anexo_extrato_score_approval_tape_full t25 on t25.loan_request_id = t1.loan_request_id
left join t_anexo_m2_m3_score_approval_tape_full t26 on t26.loan_request_id = t1.loan_request_id
left join t_anexo_modelos_approval_tape t27 on t27.loan_request_id = t1.loan_request_id
left join t_bizcredit_motivo_rejeicao_full t28 on t28.lead_id = t1.direct_prospect_id and t28.indice_alcada = 1
left join t_get_old_checklist_data t29 on t29.emprestimo_id = t1.loan_request_id
left join mv_bizcredit_checklist t30 on t30.lead_id = t1.direct_prospect_id
left join t_bizcredit_bizextratos_full t31 on t31.loan_request_id = t1.loan_request_id
left join data_science.scores_approval_tape_completa t32 on t32.loan_request_id = t1.loan_request_id
left join (select 
		loan_request_id, 
		prob,
		classe,
		row_number () over (partition by loan_request_id order by criado_em desc) as indice
	from backtests.modelo_extrato_comercio_v2_log
	) t33 on t33.loan_request_id = t1.loan_request_id and t33.indice = 1
left join (select 
		loan_request_id, 
		prob,
		classe,
		row_number () over (partition by loan_request_id order by criado_em desc) as indice
	from backtests.modelo_extrato_nao_comercio_v2_log
	) t34 on t34.loan_request_id = t1.loan_request_id and t34.indice = 1
	


grant select on table lucas_leal.bizcredit_approval_tape_full to cristiano_rocha;
grant select on table lucas_leal.bizcredit_approval_tape_full to lms;
grant select on table lucas_leal.bizcredit_approval_tape_full to pedro_meirelles;
grant select on table lucas_leal.bizcredit_approval_tape_full to giulia_killer;
grant select on table lucas_leal.bizcredit_approval_tape_full to rodrigo_pinho;
grant select on table lucas_leal.bizcredit_approval_tape_full to valesca_jannis;
grant select on table lucas_leal.bizcredit_approval_tape_full to chicao;
grant select on table lucas_leal.bizcredit_approval_tape_full to marcelo_prado;
grant select on table lucas_leal.bizcredit_approval_tape_full to lead_automation;
grant select on table lucas_leal.bizcredit_approval_tape_full to wellerson_silva;
grant select on table lucas_leal.bizcredit_approval_tape_full to caio_camara;



create table t_bizcredit_motivo_rejeicao_full as
select
	lead_id,
	alcada,
	string_agg(motivo_alteracao,',') as motivo_alteracao,
	row_number() over (partition by lead_id order by alcada desc) as indice_alcada
from(select 
		ca.legacy_target_id as lead_id,
		opinions->>'AuthorityLevel' as alcada,
		opinions->>'CreditOpinionSuggestionType' as decisao,
		motivos_alteracao->>'Title' as motivo_alteracao
	from (select 
			legacy_target_id,
			data_output,
			row_number() over (partition by legacy_target_id order by id desc) as indice_analise
		from sync_credit_analysis) ca,
		jsonb_array_elements(data_output ->'CreditOpinions') opinions,
		jsonb_array_elements(opinions->'SelectedMotives') as motivos_alteracao
	where indice_analise = 1) as t1
group by 1,2



insert into t_bizcredit_parecer_full
select
	cp.emprestimo_id,
--------------------------CREDIT UNDERWRITE
	case when cp.bizbot > 0 and cp.nao_bizbot = 0 then 1 else 0 end as loan_approved_by_bizbot,
	cp.data_a1 as credit_underwrite_a1_date,
	trim(initcap(translate(cp.analista_a1,'._',' '))) as credit_underwrite_a1_name,
	cp.status_a1 as credit_underwrite_a1_status,
	cp.nota_a1 as credit_underwrite_a1_score,
	cp.valor_a1 as credit_underwrite_a1_value,
	cp.taxa_a1 as credit_underwrite_a1_interest,
	cp.prazo_a1 as credit_underwrite_a1_term,
	cp.data_a2 as credit_underwrite_a2_date,
	trim(initcap(translate(cp.analista_a2,'._',' '))) as credit_underwrite_a2_name,
	cp.status_a2 as credit_underwrite_a2_status,
	cp.nota_a2 as credit_underwrite_a2_score,
	cp.valor_a2 as credit_underwrite_a2_value,
	cp.taxa_a2 as credit_underwrite_a2_interest,
	cp.prazo_a2 as credit_underwrite_a2_term,
	cp.data_a3 as credit_underwrite_a3_date,
	trim(initcap(translate(cp.analista_a3,'._',' '))) as credit_underwrite_a3_name,
	cp.status_a3 as credit_underwrite_a3_status,
	cp.nota_a3 as credit_underwrite_a3_score,
	cp.valor_a3 as credit_underwrite_a3_value,
	cp.taxa_a3 as credit_underwrite_a3_interest,
	cp.prazo_a3 as credit_underwrite_a3_term
from(SELECT cp.emprestimo_id,
	    count(*) FILTER (WHERE cp.criado_por = 67) AS bizbot,
	    count(*) FILTER (WHERE cp.criado_por <> 67) AS nao_bizbot,
	    min(cp.criado_em) FILTER (WHERE cp.indice = 1)::date AS data_a1,
	    max(regexp_replace(au.first_name::text || '_'::text || au.last_name::text,'\_$','')) FILTER (WHERE cp.indice = 1) AS analista_a1,
	    max(cp.status::text) FILTER (WHERE cp.indice = 1) AS status_a1,
	    max(cp.confianca) FILTER (WHERE cp.indice = 1) AS nota_a1,
	    max(cp.limite) FILTER (WHERE cp.indice = 1) AS valor_a1,
	    max(cp.juros) FILTER (WHERE cp.indice = 1)::numeric(10,2) AS taxa_a1,
	    max(cp.prazo) FILTER (WHERE cp.indice = 1)::numeric(10,2) AS prazo_a1,
	    min(cp.criado_em) FILTER (WHERE cp.indice = 2)::date AS data_a2,
	    max((au.first_name::text || '_'::text) || au.last_name::text) FILTER (WHERE cp.indice = 2) AS analista_a2,
	    max(cp.status::text) FILTER (WHERE cp.indice = 2) AS status_a2,
	    max(cp.confianca) FILTER (WHERE cp.indice = 2) AS nota_a2,
	    max(cp.limite) FILTER (WHERE cp.indice = 2) AS valor_a2,
	    max(cp.juros) FILTER (WHERE cp.indice = 2)::numeric(10,2) AS taxa_a2,
	    max(cp.prazo) FILTER (WHERE cp.indice = 2)::numeric(10,2) AS prazo_a2,
	    min(cp.criado_em) FILTER (WHERE cp.indice_inv = 1)::date AS data_a3,
	    max((au.first_name::text || '_'::text) || au.last_name::text) FILTER (WHERE cp.indice_inv = 1) AS analista_a3,
	    max(cp.status::text) FILTER (WHERE cp.indice_inv = 1) AS status_a3,
	    max(cp.confianca) FILTER (WHERE cp.indice_inv = 1) AS nota_a3,
	    max(cp.limite) FILTER (WHERE cp.indice_inv = 1) AS valor_a3,
	    max(cp.juros) FILTER (WHERE cp.indice_inv = 1)::numeric(10,2) AS taxa_a3,
	    max(cp.prazo) FILTER (WHERE cp.indice_inv = 1)::numeric(10,2) AS prazo_a3
	FROM ( SELECT credito_parecer.id,
            credito_parecer.confianca,
            credito_parecer.status,
            credito_parecer.tipo,
            credito_parecer.texto,
            credito_parecer.juros,
            credito_parecer.prazo,
            credito_parecer.criado_em,
            credito_parecer.criado_por,
            credito_parecer.modificado_em,
            credito_parecer.modificado_por,
            credito_parecer.emprestimo_id,
            credito_parecer.limite,
            dense_rank() OVER (PARTITION BY credito_parecer.emprestimo_id ORDER BY (to_timestamp(to_char(credito_parecer.criado_em, 'yyyy-mm-dd HH24:MI:SS'::text), 'yyyy-mm-dd HH24:MI:SS'::text)), credito_parecer.tipo) AS indice,
            dense_rank() OVER (PARTITION BY credito_parecer.emprestimo_id ORDER BY (to_timestamp(to_char(credito_parecer.criado_em, 'yyyy-mm-dd HH24:MI:SS'::text), 'yyyy-mm-dd HH24:MI:SS'::text)) desc, credito_parecer.tipo) AS indice_inv
        FROM credito_parecer) cp
    	JOIN loan_requests lr ON lr.loan_request_id = cp.emprestimo_id
    	JOIN offers o ON o.offer_id = lr.offer_id
    	LEFT JOIN public_new.auth_user au ON au.id = cp.criado_por
       	where o.direct_prospect_id not in (select legacy_target_id from public.sync_credit_analysis where coalesce(((data_output -> 'CreditOpinions'::text) -> 2) ->> 'Status'::text, ((data_output -> 'CreditOpinions'::text) -> 1) ->> 'Status'::text, ((data_output -> 'CreditOpinions'::text) -> 0) ->> 'Status'::text) = 'SUBMITTED')
       		and lr.status in ('Expirado','REJECTED','ACCEPTED','LOST','Cancelado') and lr.loan_request_id not in (select emprestimo_id from t_bizcredit_parecer_full)
	  group by cp.emprestimo_id
	union
		select lr.loan_request_id AS emprestimo_id,
	    	0 as bizbot,
	    	(((c.data_output -> 'CreditOpinions'::text) -> 0) IS NOT NULL)::integer + (((c.data_output -> 'CreditOpinions'::text) -> 1) IS NOT NULL)::integer + (((c.data_output -> 'CreditOpinions'::text) -> 2) IS NOT NULL)::integer AS nao_bizbot,
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 0) -> 'LastChange'::text) ->> 'Date'::text)::date
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 1) -> 'LastChange'::text) ->> 'Date'::text)::date
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 2) -> 'LastChange'::text) ->> 'Date'::text)::date
	            ELSE NULL::date
	        END AS data_a1,
	        CASE
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'Username'::text
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'Username'::text
	            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'Username'::text
	            ELSE NULL::text
	        END AS analista_a1,
		    replace(replace(replace(
		        CASE
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'CreditOpinionSuggestionType'::text
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'CreditOpinionSuggestionType'::text
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'CreditOpinionSuggestionType'::text
		            ELSE NULL::text
		        END, 'APPROVE_WITH_CHANGES'::text, 'AprovarAlteracao'::text), 'APPROVE'::text, 'Aprovar'::text), 'REPROVE'::text, 'Rejeitar'::text) AS status_a1,
		        CASE
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'TrustLevel'::text)::integer
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'TrustLevel'::text)::integer
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'TrustLevel'::text)::integer
		            ELSE NULL::integer
		        END AS nota_a1,
		        CASE
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'NewAmount'::text))::numeric(100,2)
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'NewAmount'::text))::numeric(100,2)
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'NewAmount'::text))::numeric(100,2)
		            ELSE NULL::numeric(100,2)
		        END AS valor_a1,
		        CASE
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'NewInterestRate'::text))::numeric(10,2)
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'NewInterestRate'::text))::numeric(10,2)
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'NewInterestRate'::text))::numeric(10,2)
		            ELSE NULL::numeric(10,2)
		        END AS taxa_a1,
		        CASE
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'NewTenure'::text)::integer
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'NewTenure'::text)::integer
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'FIRST_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'NewTenure'::text)::integer
		            ELSE NULL::integer
		        END AS prazo_a1,
		        CASE
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 0) -> 'LastChange'::text) ->> 'Date'::text)::date
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 1) -> 'LastChange'::text) ->> 'Date'::text)::date
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 2) -> 'LastChange'::text) ->> 'Date'::text)::date
		            ELSE NULL::date
		        END AS data_a2,
		        CASE
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'Username'::text
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'Username'::text
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'Username'::text
		            ELSE NULL::text
		        END AS analista_a2,
		    replace(replace(replace(
		        CASE
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'CreditOpinionSuggestionType'::text
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'CreditOpinionSuggestionType'::text
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'CreditOpinionSuggestionType'::text
		            ELSE NULL::text
		        END, 'APPROVE_WITH_CHANGES'::text, 'AprovarAlteracao'::text), 'APPROVE'::text, 'Aprovar'::text), 'REPROVE'::text, 'Rejeitar'::text) AS status_a2,
		        CASE
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'TrustLevel'::text)::integer
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'TrustLevel'::text)::integer
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'TrustLevel'::text)::integer
		            ELSE NULL::integer
		        END AS nota_a2,
		        CASE
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'NewAmount'::text))::numeric(100,2)
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'NewAmount'::text))::numeric(100,2)
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'NewAmount'::text))::numeric(100,2)
		            ELSE NULL::numeric(100,2)
		        END AS valor_a2,
		        CASE
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'NewInterestRate'::text))::numeric(10,2)
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'NewInterestRate'::text))::numeric(10,2)
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'NewInterestRate'::text))::numeric(10,2)
		            ELSE NULL::numeric(10,2)
		        END AS taxa_a2,
		        CASE
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'NewTenure'::text)::integer
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'NewTenure'::text)::integer
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'SECOND_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'NewTenure'::text)::integer
		            ELSE NULL::integer
		        END AS prazo_a2,
		        CASE
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 0) -> 'LastChange'::text) ->> 'Date'::text)::date
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 1) -> 'LastChange'::text) ->> 'Date'::text)::date
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 2) -> 'LastChange'::text) ->> 'Date'::text)::date
		            ELSE NULL::date
		        END AS data_a3,
		        CASE
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'Username'::text
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'Username'::text
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'Username'::text
		            ELSE NULL::text
		        END AS analista_a3,
		    replace(replace(replace(
		        CASE
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'CreditOpinionSuggestionType'::text
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'CreditOpinionSuggestionType'::text
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'CreditOpinionSuggestionType'::text
		            ELSE NULL::text
		        END, 'APPROVE_WITH_CHANGES'::text, 'AprovarAlteracao'::text), 'APPROVE'::text, 'Aprovar'::text), 'REPROVE'::text, 'Rejeitar'::text) AS status_a3,
		        CASE
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'TrustLevel'::text)::integer
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'TrustLevel'::text)::integer
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'TrustLevel'::text)::integer
		            ELSE NULL::integer
		        END AS nota_a3,
		        CASE
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'NewAmount'::text))::numeric(100,2)
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'NewAmount'::text))::numeric(100,2)
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'NewAmount'::text))::numeric(100,2)
		            ELSE NULL::numeric(100,2)
		        END AS valor_a3,
		        CASE
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'NewInterestRate'::text))::numeric(10,2)
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'NewInterestRate'::text))::numeric(10,2)
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN ((((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'NewInterestRate'::text))::numeric(10,2)
		            ELSE NULL::numeric(10,2)
		        END AS taxa_a3,
		        CASE
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 0) ->> 'NewTenure'::text)::integer
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 1) ->> 'NewTenure'::text)::integer
		            WHEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'AuthorityLevel'::text) = 'THIRD_AUTHORITY'::text THEN (((c.data_output -> 'CreditOpinions'::text) -> 2) ->> 'NewTenure'::text)::integer
		            ELSE NULL::integer
		        END AS prazo_a3
		FROM (select 
				legacy_target_id,
				data_output,
				row_number() over (partition by legacy_target_id order by id desc) as indice_analise
			from sync_credit_analysis) c
		JOIN offers o ON o.direct_prospect_id = c.legacy_target_id
		JOIN loan_requests lr ON lr.offer_id = o.offer_id
		where c.indice_analise = 1 and coalesce(((data_output -> 'CreditOpinions'::text) -> 2) ->> 'Status'::text, ((data_output -> 'CreditOpinions'::text) -> 1) ->> 'Status'::text, ((data_output -> 'CreditOpinions'::text) -> 0) ->> 'Status'::text) = 'SUBMITTED'
			and lr.status in ('Expirado','REJECTED','ACCEPTED','LOST','Cancelado') and lr.loan_request_id not in (select emprestimo_id from t_bizcredit_parecer_full)) as cp



--TABELA REAL
insert into t_bizcredit_approval_tape_full
select
	dp.direct_prospect_id,
	lr.loan_request_id,
	lr.status,
	lr.value as valor_pedido,
	coalesce(t.credit_underwrite_a3_status,t.credit_underwrite_a2_status,t.credit_underwrite_a1_status) as decisao_final,
	case when coalesce(t.credit_underwrite_a3_status,t.credit_underwrite_a2_status,t.credit_underwrite_a1_status) = 'Rejeitar' then 1 when coalesce(t.credit_underwrite_a3_status,t.credit_underwrite_a2_status,t.credit_underwrite_a1_status) like 'Aprovar%' then 0 end as target_reject,
	case when dp.utm_source = 'sebrae' then 'sebrae' when dp.utm_source = 'estimulo2020' then 'estimulo2020' else 'biz' end as esteira,
	coalesce(t.credit_underwrite_a3_date,t.credit_underwrite_a2_date,t.credit_underwrite_a1_date)::date as data_criacao,
	case when coalesce(t.credit_underwrite_a3_date,t.credit_underwrite_a2_date,t.credit_underwrite_a1_date)::date < '2020-03-15' then 'Pre covid' when coalesce(t.credit_underwrite_a3_date,t.credit_underwrite_a2_date,t.credit_underwrite_a1_date)::date between '2020-03-15' and '2020-08-01' then 'Covid' else 'Pos covid' end as periodo_controle,
	dp.previous_loans_count as renovacao,
	lt.collection_total_due_installments,
	lt.collection_count_postponing_restructures,
	lt.collection_i_pmt_of_first_postponing,
	lt.collection_current_max_late_pmt1,
	lt.collection_current_max_late_pmt2,
	lt.collection_ever_by_pmt3_soft,
	lt.collection_ever_by_pmt4_aggressive,
	lt.collection_ever_by_pmt5_aggressive,
	lt.collection_ever_by_pmt6_aggressive
---DADOS DO FUNIL
from public.direct_prospects dp
join lucas_leal.v_funil_completo_sem_duplicacao vfcsd on vfcsd.direct_prospect_id = dp.direct_prospect_id 
join public.loan_requests lr on lr.loan_request_id = vfcsd.loan_request_id 
join lucas_leal.t_bizcredit_parecer_full t on t.emprestimo_id = lr.loan_request_id
left join lucas_leal.loan_tape_full lt on lt.lead_id = dp.direct_prospect_id
where lr.loan_request_id not in (select loan_request_id from t_bizcredit_approval_tape_full)



insert into t_bizcredit_endereco_balcao_full 
select
	t.direct_prospect_id,
	a.zip_code,
	a.street
from(select 
		row_number() over (partition by a.client_id order by a.address_id) as indice_endereco,
		a.*
	from public.client_addresses a) as a
join(select
		dp.client_id,
		dp.direct_prospect_id,
		row_number() over (partition by dp.client_id order by vfcsd.loan_request_id) as indice_pedido
	from public.direct_prospects dp 
	join lucas_leal.v_funil_completo_sem_duplicacao vfcsd on vfcsd.direct_prospect_id = dp.direct_prospect_id 
	join t_bizcredit_parecer_full t on t.emprestimo_id = vfcsd.loan_request_id) as t on t.client_id = a.client_id and t.indice_pedido = a.indice_endereco
where t.direct_prospect_id not in (select direct_prospect_id from t_bizcredit_endereco_balcao_full)


insert into t_bizcredit_signers_full
select
	loan_request_id,
	max(lead_id) as lead_id,
	count(*) filter (where signer_cpf = cpf_solicitante and participacao > 0) as is_shareholder,
	count(*) filter (where signer_cpf = cpf_solicitante and participacao > 0 and name_of_responsible = signer_name) as is_shareholder_mesmo_nome,
	max(revenue) filter (where indice_socio = 1) as signer_renda_mensal,
	max(signer_age) filter (where indice_socio = 1) as signer_idade,
	max(signer_date_of_birth) filter (where indice_socio = 1) as signer_data_nascimento,
	max(nivel_pep) filter (where indice_socio = 1) as signer_nivel_pep,
	max(signer_street_address) filter (where indice_socio = 1) as signer_street_address,
	max(signer_city) filter (where indice_socio = 1) as signer_city,
	max(signer_zip_code) filter (where indice_socio = 1) as signer_zip_code,
	max(administrator) filter (where indice_socio = 1) as administrator,
	max(qualificacao) filter (where indice_socio = 1) as qualificacao,
	max(repeat('0', 11 - length(signer_cpf)) || signer_cpf) filter (where indice_socio = 1) as major_signer_cpf,
	string_agg(signer_cpf::text,',') as list_signer_cpf,
	string_agg(signer_age::text,',') as list_signer_idade,	
	max(number) filter (where indice_socio = 1) as signer_street_number_address,
	max(complement) filter (where indice_socio = 1) as signer_street_complement_address
from(select
		lr.lead_id,
		lr.loan_request_id,
		lr.cpf as cpf_solicitante,
		upper(case when position(' ' in trim(lr.name_of_responsible)) > 0 then left(trim(lr.name_of_responsible),position(' ' in trim(lr.name_of_responsible)) - 1) when trim(lr.name_of_responsible) = '' then null else trim(lr.name_of_responsible) end) as name_of_responsible,
		coalesce(upper(case when position(' ' in trim(s."name")) > 0 then left(trim(s."name"),position(' ' in trim(s."name")) - 1) when trim(s."name") = '' then null else trim(s."name") end),
			upper(case when position(' ' in trim(es.nome)) > 0 then left(trim(es.nome),position(' ' in trim(es.nome)) - 1) when trim(es.nome) = '' then null else trim(es.nome) end),
			upper(case when position(' ' in trim(ser.nome)) > 0 then left(trim(ser.nome),position(' ' in trim(ser.nome)) - 1) when trim(ser.nome) = '' then null else trim(ser.nome) end)) as signer_name,
		coalesce(case when s.cpf = '' then null else s.cpf end,case when es.cpf = '' then null else es.cpf end, case when ser.cpf_socios = '' then null else ser.cpf_socios end,
			case when left(lr.vinculo,5) in ('Sócio','Sócia') or lr.vinculo = 'Presidente' then lr.cpf end) as signer_cpf,
		coalesce(s.share_percentage,es.participacao,ser.share,case when left(lr.vinculo,5) in ('Sócio','Sócia') or lr.vinculo = 'Presidente' then 100 end) as participacao,
		extract(year from age(lr.date_inserted,s.date_of_birth))::int as signer_age,
		s.date_of_birth::date as signer_date_of_birth,
		s.revenue::numeric,
		s.street as signer_street_address,
		s.city as signer_city,
		s.zip_code as signer_zip_code,
		s.administrator::int,
		es.nivel_pep,
		es.qualificacao,
		row_number() over (partition by lr.loan_request_id order by coalesce(s.share_percentage,es.participacao) desc,age(lr.date_inserted,s.date_of_birth) desc) as indice_socio,
		s.number,
		s.complement,
		lr.vinculo,
		s.loan_request_id as id_signer,
		es.cnpj as id_neoway,
		ser.direct_prospect_id as id_serasa
	from(select
			dp.direct_prospect_id as lead_id,
			lr.loan_request_id,
			dp.cnpj,
			dp.cpf,
			dp.name_of_responsible,
			lr.date_inserted::date,
			dp.vinculo
		from direct_prospects dp
		join lucas_leal.v_funil_completo_sem_duplicacao vfcsd on vfcsd.direct_prospect_id = dp.direct_prospect_id 
		join public.loan_requests lr on lr.loan_request_id = vfcsd.loan_request_id
		join t_bizcredit_approval_tape_full t1 on t1.loan_request_id = lr.loan_request_id
		where lr.loan_request_id not in (select loan_request_id from t_bizcredit_signers_full)) as lr		
	left join(select 
			row_number() over (partition by lrs.loan_request_id,s.cpf order by 
				case when s.email is null then 1 else 0 end +
				case when s.civil_status is null then 1 else 0 end +
				case when s.profession is null then 1 else 0 end +
				case when s.rg is null then 1 else 0 end +
				case when s.issuing_body is null then 1 else 0 end +
				case when s.country_of_birth is null then 1 else 0 end +
				case when s.city_of_birth is null then 1 else 0 end +
				case when s.date_of_birth is null then 1 else 0 end) as indice,
			lrs.loan_request_id,
			s.cpf,
			s.share_percentage,
			s."name",
			sa.street,
			c."name" as city,
			sa.zip_code,
			s.revenue,
			s.administrator,
			s.date_of_birth,
			sa."number",
			sa.complement,
			dp.cnpj
		from public.loan_requests_signers lrs
		join public.signers s on s.signer_id = lrs.signer_id
		left join public.signer_addresses sa on sa.address_id = s.address_id
		left join public.cities c on c.city_id = sa.city_id
		join public.loan_requests lr on lr.loan_request_id = lrs.loan_request_id
		join t_bizcredit_parecer_full t1 on t1.emprestimo_id = lr.loan_request_id
		join public.offers o on o.offer_id = lr.offer_id
		join public.direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
		where s.share_percentage > 0 and lrs.loan_request_id not in (select loan_request_id from t_bizcredit_signers_full)) s on s.loan_request_id = lr.loan_request_id
	full outer join (select
			es.cnpj,
			es.cpf,
			es.nome,
			es.nivel_pep,
			es.qualificacao,
			es.participacao,
			row_number() over (partition by es.cnpj,es.cpf order by es.id) as indice
		from public.empresas_socios es
		join public.direct_prospects dp on dp.cnpj = es.cnpj
		join t_bizcredit_approval_tape_full t1 on t1.direct_prospect_id = dp.direct_prospect_id
		left join (select distinct 
				dp.cnpj
			from t_bizcredit_approval_tape_full t1
			join public.loan_requests_signers lrs on lrs.loan_request_id = t1.loan_request_id
			join public.signers s on s.signer_id = lrs.signer_id
			join public.loan_requests lr on lr.loan_request_id = lrs.loan_request_id
			join public.offers o on o.offer_id = lr.offer_id
			join public.direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
			where s.share_percentage > 0) as t2 on t2.cnpj = es.cnpj
		where participacao > 0 and t2.cnpj is null and t1.loan_request_id not in (select loan_request_id from t_bizcredit_signers_full)) es on es.cnpj = lr.cnpj 
	full outer join (SELECT 
			t1.direct_prospect_id,
			t1.cnpj,
			t1.cpf,
			((cc.data -> 'acionistas'::text) -> jsonb_object_keys(cc.data -> 'acionistas'::text) ->> 'cpf')::text AS cpf_socios,
			jsonb_object_keys(cc.data -> 'acionistas'::text) AS nome,
			(((cc.data -> 'acionistas'::text) -> jsonb_object_keys(cc.data -> 'acionistas'::text)) ->> 'percentual'::text)::numeric AS share
		FROM credito_coleta cc
		JOIN (SELECT dp.direct_prospect_id,
				max(dp.cnpj) as cnpj,
				max(dp.cpf) as cpf,
				max(cc.id) AS max_id
			FROM credito_coleta cc
			JOIN direct_prospects dp ON "left"(cc.documento::text, 8) = "left"(dp.cnpj::text, 8)
			join t_bizcredit_approval_tape_full t1 on t1.direct_prospect_id = dp.direct_prospect_id
			join public.loan_requests lr on lr.loan_request_id = t1.loan_request_id
			left join public.empresas_socios es on es.cnpj = dp.cnpj and es.participacao > 0
			left join (select distinct 
					t1.direct_prospect_id
				from t_bizcredit_approval_tape_full t1
				join public.loan_requests_signers lrs on lrs.loan_request_id = t1.loan_request_id
				join public.signers s on s.signer_id = lrs.signer_id
				where s.share_percentage > 0) as t2 on t2.direct_prospect_id = dp.direct_prospect_id
			WHERE to_date((cc.data -> 'datetime'::text) ->> 'data'::text, 'yyyymmdd'::text) <= coalesce(lr.loan_date,lr.date_inserted::date + '10 days'::interval) 
				AND (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying]::text[]))
				and es.cnpj is null
				and t2.direct_prospect_id is null
				and t1.loan_request_id not in (select loan_request_id from t_bizcredit_signers_full)
			GROUP BY 1) t1 ON cc.id = t1.max_id) ser on ser.direct_prospect_id = lr.lead_id and ser.share > 0
	where lr.cnpj is not null and coalesce(s.indice,es.indice,1) = 1
	) as t1
group by 1



insert into t_bizcredit_ipstack_full
select 
	emprestimo_id,
	(data->>'distancia')::numeric as distancia_ipstack
from public.credito_coleta cc
join(select 
		t.emprestimo_id,
		max(cc.id) as max_id
	from public.credito_coleta cc
	join public.direct_prospects dp on left(dp.cnpj,8) = left(cc.documento,8)
	join public.offers o on o.direct_prospect_id = dp.direct_prospect_id
	join public.loan_requests lr on lr.offer_id = o.offer_id
	join t_bizcredit_parecer_full t on t.emprestimo_id = lr.loan_request_id
	where cc.tipo = 'IpStack' and cc.data_coleta::date <= coalesce(lr.loan_date,lr.date_inserted::date + '10 days'::interval)
		and t.emprestimo_id not in (select emprestimo_id from t_bizcredit_ipstack_full)
	group by 1) as max_id on cc.id = max_id.max_id
where tipo = 'IpStack'


insert into t_bizcredit_emailage_full
select 
	max_id.emprestimo_id,
	left(resultados->>'fraudRisk',3)::bigint as score_emailage,
	right(resultados->>'fraudRisk',length(resultados->>'fraudRisk') - position(' ' in resultados->>'fraudRisk')) as risco_emailage
from public.credito_coleta cc,
	jsonb_array_elements(data->'query'->'results') as resultados,
	(select 
		t.emprestimo_id,
		max(cc.id) as max_id
	from public.credito_coleta cc
	join public.direct_prospects dp on left(dp.cnpj,8) = left(cc.documento,8)
	join public.offers o on o.direct_prospect_id = dp.direct_prospect_id
	join public.loan_requests lr on lr.offer_id = o.offer_id
	join t_bizcredit_parecer_full t on t.emprestimo_id = lr.loan_request_id
	where cc.tipo = 'EmailAge' and cc.data_coleta::date <= coalesce(lr.loan_date,lr.date_inserted::date + '10 days'::interval)
		and t.emprestimo_id not in (select emprestimo_id from t_bizcredit_emailage_full)
	group by 1) as max_id  
where tipo = 'EmailAge' and cc.id = max_id.max_id


insert into t_bizcredit_scr_cpf_major_full
select
	t1.lead_id,
	t1.major_signer_cpf,
	case when thin_file is null then long_term_debt_pf_curr else 0 end long_term_debt_pf_curr,
	case when thin_file is null then long_term_debt_pf_1m else 0 end long_term_debt_pf_1m,
	case when thin_file is null then long_term_debt_pf_2m else 0 end long_term_debt_pf_2m,
	case when thin_file is null then long_term_debt_pf_3m else 0 end long_term_debt_pf_3m,
	case when thin_file is null then long_term_debt_pf_4m else 0 end long_term_debt_pf_4m,
	case when thin_file is null then long_term_debt_pf_5m else 0 end long_term_debt_pf_5m,
	case when thin_file is null then long_term_debt_pf_6m else 0 end long_term_debt_pf_6m,
	case when thin_file is null then coalesce(months_since_last_overdue_pf, -1) else - 1 end months_since_last_overdue_pf,
	case when thin_file is null then max_overdue_pf else 0 end max_overdue_pf,
	case when thin_file is null then coalesce(months_since_last_default_pf, -1) else - 1 end months_since_last_default_pf,
	case when thin_file is null then max_default_pf else 0 end max_default_pf,
	(thin_file is not null)::int as thin_file
from t_bizcredit_signers_full t1
left join(SELECT 
		t2.direct_prospect_id,
		t2.cpf,
	    date_part('year'::text, age(min(t2.data_referencia)::timestamp with time zone, max(t2.data) FILTER (WHERE t2.data <= t2.data_referencia AND t2.vencidopf > 0::double precision)::timestamp with time zone)) * 12::double precision + date_part('month'::text, age(min(t2.data_referencia)::timestamp with time zone, max(t2.data) FILTER (WHERE t2.data <= t2.data_referencia AND t2.vencidopf > 0::double precision)::timestamp with time zone)) AS months_since_last_overdue_pf,
	    max(t2.vencidopf) FILTER (WHERE t2.data <= t2.data_referencia) AS max_overdue_pf,
	    date_part('year'::text, age(min(t2.data_referencia)::timestamp with time zone, max(t2.data) FILTER (WHERE t2.data <= t2.data_referencia AND t2.prejuizopf > 0::double precision)::timestamp with time zone)) * 12::double precision + date_part('month'::text, age(min(t2.data_referencia)::timestamp with time zone, max(t2.data) FILTER (WHERE t2.data <= t2.data_referencia AND t2.prejuizopf > 0::double precision)::timestamp with time zone)) AS months_since_last_default_pf,
	    max(t2.prejuizopf) FILTER (WHERE t2.data <= t2.data_referencia) AS max_default_pf,
	    sum(t2.debtpf) FILTER (WHERE t2.data = t2.data_referencia) AS long_term_debt_pf_curr,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 mon'::interval)) AS long_term_debt_pf_1m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '2 mons'::interval)) AS long_term_debt_pf_2m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '3 mons'::interval)) AS long_term_debt_pf_3m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '4 mons'::interval)) AS long_term_debt_pf_4m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '5 mons'::interval)) AS long_term_debt_pf_5m,
	    sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '6 mons'::interval)) AS long_term_debt_pf_6m
	FROM(SELECT t1.direct_prospect_id,
				t1.cpf,
               CASE
                    WHEN t1.max_id IS NOT NULL THEN to_date((((cc.data -> 'historico'::text) -> 'Carteira de Crédito'::text) -> 0) ->> 'data'::text, 'mm-yyyy'::text)
                    ELSE
                    CASE
                        WHEN date_part('day'::text, t1.data_pedido) >= 16::double precision THEN to_date(to_char(t1.data_pedido - '1 mon'::interval, 'Mon/yy'::text), 'Mon/yy'::text)
                        ELSE to_date(to_char(t1.data_pedido - '2 mons'::interval, 'Mon/yy'::text), 'Mon/yy'::text)
                    END
                END AS data_referencia,
            to_date(jsonb_array_elements((cc.data -> 'historico'::text) -> 'Carteira de Crédito'::text) ->> 'data'::text, 'mm/yyyy'::text) AS data,
            replace(replace(replace(jsonb_array_elements((cc.data -> 'historico'::text) -> 'Carteira de Crédito'::text) ->> 'valor'::text, '-'::text, '0'::text), '.'::text, ''::text), ','::text, '.'::text)::double precision AS debtpf,
            replace(replace(replace(jsonb_array_elements((cc.data -> 'historico'::text) -> 'Vencido'::text) ->> 'valor'::text, '-'::text, '0'::text), '.'::text, ''::text), ','::text, '.'::text)::double precision AS vencidopf,
            replace(replace(replace(jsonb_array_elements((cc.data -> 'historico'::text) -> 'Prejuízo'::text) ->> 'valor'::text, '-'::text, '0'::text), '.'::text, ''::text), ','::text, '.'::text)::double precision AS prejuizopf
        FROM credito_coleta cc
		JOIN (SELECT 
				t.lead_id as direct_prospect_id,
			    t.major_signer_cpf as cpf,
			    max(coalesce(lr.loan_date,lr.date_inserted::date)) as data_pedido,
			    max(cc.id) FILTER (WHERE COALESCE(to_timestamp(regexp_replace(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text), '\..*', ''), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted::date + '10 days'::interval)) AS max_id,
			    min(cc.id) AS min_id
		   	FROM credito_coleta cc
			join t_bizcredit_signers_full t on cc.documento = t.major_signer_cpf
			join public.loan_requests lr on lr.loan_request_id = t.loan_request_id
			WHERE cc.documento_tipo::text = 'CPF'::text AND cc.tipo::text = 'SCR'::text
				and t.lead_id not in (select lead_id from t_bizcredit_scr_cpf_major_full)
			GROUP BY 1,2) t1 ON cc.id = coalesce(t1.max_id, t1.min_id)) t2
		GROUP BY 1,2) as hist_json on hist_json.direct_prospect_id = t1.lead_id
left join(SELECT 
			direct_prospect_id,
			cpf,
			coalesce(cc."data"->> 'erro',cc."data"->> 'error') as thin_file
       	FROM credito_coleta cc
		JOIN (SELECT 
				t.lead_id as direct_prospect_id,
			    t.major_signer_cpf as cpf,
			    max(cc.id) FILTER (WHERE COALESCE(to_timestamp(regexp_replace(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text), '\..*', ''), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted::date + '10 days'::interval)) AS max_id,
			    min(cc.id) AS min_id
		   	FROM credito_coleta cc
			join t_bizcredit_signers_full t on cc.documento = t.major_signer_cpf
			join public.loan_requests lr on lr.loan_request_id = t.loan_request_id
			WHERE cc.documento_tipo::text = 'CPF'::text AND cc.tipo::text = 'SCR'::text
				and t.lead_id not in (select lead_id from t_bizcredit_scr_cpf_major_full)
			GROUP BY 1,2) t1 ON cc.id = COALESCE(t1.max_id, t1.min_id)
		) as hist_raw on hist_raw.direct_prospect_id = t1.lead_id
where t1.lead_id not in (select lead_id from t_bizcredit_scr_cpf_major_full)


		
--SCR PJ
insert into t_bizcredit_scr_pj_full 
select
	t1.emprestimo_id,
	case when thin_file is not null then 0
		else (greatest(emprestimos_giro_longo_pj_curr,emprestimos_giro_curto_pj_curr,emprestimos_giro_rotativo_pj_curr) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_1m,emprestimos_giro_curto_pj_1m,emprestimos_giro_rotativo_pj_1m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_2m,emprestimos_giro_curto_pj_2m,emprestimos_giro_rotativo_pj_2m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_3m,emprestimos_giro_curto_pj_3m,emprestimos_giro_rotativo_pj_3m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_4m,emprestimos_giro_curto_pj_4m,emprestimos_giro_rotativo_pj_4m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_5m,emprestimos_giro_curto_pj_5m,emprestimos_giro_rotativo_pj_5m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_6m,emprestimos_giro_curto_pj_6m,emprestimos_giro_rotativo_pj_6m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_7m,emprestimos_giro_curto_pj_7m,emprestimos_giro_rotativo_pj_7m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_8m,emprestimos_giro_curto_pj_8m,emprestimos_giro_rotativo_pj_8m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_9m,emprestimos_giro_curto_pj_9m,emprestimos_giro_rotativo_pj_9m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_10m,emprestimos_giro_curto_pj_10m,emprestimos_giro_rotativo_pj_10m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_11m,emprestimos_giro_curto_pj_11m,emprestimos_giro_rotativo_pj_11m) > 0)::int end as count_capital_giro,
	case when thin_file is not null then 0
		else (greatest(emprestimos_giro_longo_pj_curr,emprestimos_giro_curto_pj_curr,emprestimos_giro_rotativo_pj_curr,financiamentos_pj_curr,outrosfinanciamentos_pj_curr) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_1m,emprestimos_giro_curto_pj_1m,emprestimos_giro_rotativo_pj_1m,financiamentos_pj_1m,outrosfinanciamentos_pj_1m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_2m,emprestimos_giro_curto_pj_2m,emprestimos_giro_rotativo_pj_2m,financiamentos_pj_2m,outrosfinanciamentos_pj_2m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_3m,emprestimos_giro_curto_pj_3m,emprestimos_giro_rotativo_pj_3m,financiamentos_pj_3m,outrosfinanciamentos_pj_3m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_4m,emprestimos_giro_curto_pj_4m,emprestimos_giro_rotativo_pj_4m,financiamentos_pj_4m,outrosfinanciamentos_pj_4m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_5m,emprestimos_giro_curto_pj_5m,emprestimos_giro_rotativo_pj_5m,financiamentos_pj_5m,outrosfinanciamentos_pj_5m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_6m,emprestimos_giro_curto_pj_6m,emprestimos_giro_rotativo_pj_6m,financiamentos_pj_6m,outrosfinanciamentos_pj_6m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_7m,emprestimos_giro_curto_pj_7m,emprestimos_giro_rotativo_pj_7m,financiamentos_pj_7m,outrosfinanciamentos_pj_7m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_8m,emprestimos_giro_curto_pj_8m,emprestimos_giro_rotativo_pj_8m,financiamentos_pj_8m,outrosfinanciamentos_pj_8m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_9m,emprestimos_giro_curto_pj_9m,emprestimos_giro_rotativo_pj_9m,financiamentos_pj_9m,outrosfinanciamentos_pj_9m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_10m,emprestimos_giro_curto_pj_10m,emprestimos_giro_rotativo_pj_10m,financiamentos_pj_10m,outrosfinanciamentos_pj_10m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_11m,emprestimos_giro_curto_pj_11m,emprestimos_giro_rotativo_pj_11m,financiamentos_pj_11m,outrosfinanciamentos_pj_11m) > 0)::int end as count_capital_giro_financiamento,
	case when thin_file is not null then 0
		else (greatest(emprestimos_giro_longo_pj_curr,emprestimos_giro_curto_pj_curr,emprestimos_giro_rotativo_pj_curr,financiamentos_pj_curr - financiamento_veiculo_pj_curr,outrosfinanciamentos_pj_curr) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_1m,emprestimos_giro_curto_pj_1m,emprestimos_giro_rotativo_pj_1m,financiamentos_pj_1m - financiamento_veiculo_pj_1m,outrosfinanciamentos_pj_1m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_2m,emprestimos_giro_curto_pj_2m,emprestimos_giro_rotativo_pj_2m,financiamentos_pj_2m - financiamento_veiculo_pj_2m,outrosfinanciamentos_pj_2m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_3m,emprestimos_giro_curto_pj_3m,emprestimos_giro_rotativo_pj_3m,financiamentos_pj_3m - financiamento_veiculo_pj_3m,outrosfinanciamentos_pj_3m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_4m,emprestimos_giro_curto_pj_4m,emprestimos_giro_rotativo_pj_4m,financiamentos_pj_4m - financiamento_veiculo_pj_4m,outrosfinanciamentos_pj_4m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_5m,emprestimos_giro_curto_pj_5m,emprestimos_giro_rotativo_pj_5m,financiamentos_pj_5m - financiamento_veiculo_pj_5m,outrosfinanciamentos_pj_5m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_6m,emprestimos_giro_curto_pj_6m,emprestimos_giro_rotativo_pj_6m,financiamentos_pj_6m - financiamento_veiculo_pj_6m,outrosfinanciamentos_pj_6m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_7m,emprestimos_giro_curto_pj_7m,emprestimos_giro_rotativo_pj_7m,financiamentos_pj_7m - financiamento_veiculo_pj_7m,outrosfinanciamentos_pj_7m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_8m,emprestimos_giro_curto_pj_8m,emprestimos_giro_rotativo_pj_8m,financiamentos_pj_8m - financiamento_veiculo_pj_8m,outrosfinanciamentos_pj_8m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_9m,emprestimos_giro_curto_pj_9m,emprestimos_giro_rotativo_pj_9m,financiamentos_pj_9m - financiamento_veiculo_pj_9m,outrosfinanciamentos_pj_9m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_10m,emprestimos_giro_curto_pj_10m,emprestimos_giro_rotativo_pj_10m,financiamentos_pj_10m - financiamento_veiculo_pj_10m,outrosfinanciamentos_pj_10m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_11m,emprestimos_giro_curto_pj_11m,emprestimos_giro_rotativo_pj_11m,financiamentos_pj_11m - financiamento_veiculo_pj_11m,outrosfinanciamentos_pj_11m) > 0)::int end as count_capital_giro_financiamento_sem_veiculo,
	case when thin_file is not null then 0
		else (greatest(emprestimos_giro_longo_pj_curr,emprestimos_giro_curto_pj_curr,emprestimos_giro_rotativo_pj_curr,financiamentos_pj_curr - financiamento_veiculo_pj_curr,outrosfinanciamentos_pj_curr,emprestimos_microcredito_pj_curr) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_1m,emprestimos_giro_curto_pj_1m,emprestimos_giro_rotativo_pj_1m,financiamentos_pj_1m - financiamento_veiculo_pj_1m,outrosfinanciamentos_pj_1m,emprestimos_microcredito_pj_1m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_2m,emprestimos_giro_curto_pj_2m,emprestimos_giro_rotativo_pj_2m,financiamentos_pj_2m - financiamento_veiculo_pj_2m,outrosfinanciamentos_pj_2m,emprestimos_microcredito_pj_2m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_3m,emprestimos_giro_curto_pj_3m,emprestimos_giro_rotativo_pj_3m,financiamentos_pj_3m - financiamento_veiculo_pj_3m,outrosfinanciamentos_pj_3m,emprestimos_microcredito_pj_3m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_4m,emprestimos_giro_curto_pj_4m,emprestimos_giro_rotativo_pj_4m,financiamentos_pj_4m - financiamento_veiculo_pj_4m,outrosfinanciamentos_pj_4m,emprestimos_microcredito_pj_4m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_5m,emprestimos_giro_curto_pj_5m,emprestimos_giro_rotativo_pj_5m,financiamentos_pj_5m - financiamento_veiculo_pj_5m,outrosfinanciamentos_pj_5m,emprestimos_microcredito_pj_5m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_6m,emprestimos_giro_curto_pj_6m,emprestimos_giro_rotativo_pj_6m,financiamentos_pj_6m - financiamento_veiculo_pj_6m,outrosfinanciamentos_pj_6m,emprestimos_microcredito_pj_6m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_7m,emprestimos_giro_curto_pj_7m,emprestimos_giro_rotativo_pj_7m,financiamentos_pj_7m - financiamento_veiculo_pj_7m,outrosfinanciamentos_pj_7m,emprestimos_microcredito_pj_7m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_8m,emprestimos_giro_curto_pj_8m,emprestimos_giro_rotativo_pj_8m,financiamentos_pj_8m - financiamento_veiculo_pj_8m,outrosfinanciamentos_pj_8m,emprestimos_microcredito_pj_8m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_9m,emprestimos_giro_curto_pj_9m,emprestimos_giro_rotativo_pj_9m,financiamentos_pj_9m - financiamento_veiculo_pj_9m,outrosfinanciamentos_pj_9m,emprestimos_microcredito_pj_9m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_10m,emprestimos_giro_curto_pj_10m,emprestimos_giro_rotativo_pj_10m,financiamentos_pj_10m - financiamento_veiculo_pj_10m,outrosfinanciamentos_pj_10m,emprestimos_microcredito_pj_10m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_11m,emprestimos_giro_curto_pj_11m,emprestimos_giro_rotativo_pj_11m,financiamentos_pj_11m - financiamento_veiculo_pj_11m,outrosfinanciamentos_pj_11m,emprestimos_microcredito_pj_11m) > 0)::int end as count_capital_giro_c_fin_s_veiculo_c_micro,
	case when thin_file is not null then 0
		else (greatest(emprestimos_giro_longo_pj_curr,emprestimos_giro_curto_pj_curr,emprestimos_giro_rotativo_pj_curr,financiamentos_pj_curr,outrosfinanciamentos_pj_curr,emprestimos_microcredito_pj_curr) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_1m,emprestimos_giro_curto_pj_1m,emprestimos_giro_rotativo_pj_1m,financiamentos_pj_1m,outrosfinanciamentos_pj_1m,emprestimos_microcredito_pj_1m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_2m,emprestimos_giro_curto_pj_2m,emprestimos_giro_rotativo_pj_2m,financiamentos_pj_2m,outrosfinanciamentos_pj_2m,emprestimos_microcredito_pj_2m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_3m,emprestimos_giro_curto_pj_3m,emprestimos_giro_rotativo_pj_3m,financiamentos_pj_3m,outrosfinanciamentos_pj_3m,emprestimos_microcredito_pj_3m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_4m,emprestimos_giro_curto_pj_4m,emprestimos_giro_rotativo_pj_4m,financiamentos_pj_4m,outrosfinanciamentos_pj_4m,emprestimos_microcredito_pj_4m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_5m,emprestimos_giro_curto_pj_5m,emprestimos_giro_rotativo_pj_5m,financiamentos_pj_5m,outrosfinanciamentos_pj_5m,emprestimos_microcredito_pj_5m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_6m,emprestimos_giro_curto_pj_6m,emprestimos_giro_rotativo_pj_6m,financiamentos_pj_6m,outrosfinanciamentos_pj_6m,emprestimos_microcredito_pj_6m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_7m,emprestimos_giro_curto_pj_7m,emprestimos_giro_rotativo_pj_7m,financiamentos_pj_7m,outrosfinanciamentos_pj_7m,emprestimos_microcredito_pj_7m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_8m,emprestimos_giro_curto_pj_8m,emprestimos_giro_rotativo_pj_8m,financiamentos_pj_8m,outrosfinanciamentos_pj_8m,emprestimos_microcredito_pj_8m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_9m,emprestimos_giro_curto_pj_9m,emprestimos_giro_rotativo_pj_9m,financiamentos_pj_9m,outrosfinanciamentos_pj_9m,emprestimos_microcredito_pj_9m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_10m,emprestimos_giro_curto_pj_10m,emprestimos_giro_rotativo_pj_10m,financiamentos_pj_10m,outrosfinanciamentos_pj_10m,emprestimos_microcredito_pj_10m) > 0)::int +
			(greatest(emprestimos_giro_longo_pj_11m,emprestimos_giro_curto_pj_11m,emprestimos_giro_rotativo_pj_11m,financiamentos_pj_11m,outrosfinanciamentos_pj_11m,emprestimos_microcredito_pj_11m) > 0)::int end as count_capital_giro_c_fin_c_micro,
		case when thin_file is null then carteiracredito_pj_curr else 0 end carteiracredito_pj_curr
from lucas_leal.t_bizcredit_parecer_full as t1
left join(SELECT t6.emprestimo_id,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  emprestimos_giro_longo_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  ever_emprestimos_giro_longo_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text),0) as  count_emprestimos_giro_longo_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  emprestimos_giro_curto_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  ever_emprestimos_giro_curto_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text),0) as  count_emprestimos_giro_curto_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  emprestimos_giro_rotativo_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  ever_emprestimos_giro_rotativo_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text),0) as  count_emprestimos_giro_rotativo_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Financiamentos'::text),0) as  financiamentos_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Financiamentos'::text),0) as  ever_financiamentos_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Financiamentos'::text),0) as  count_financiamentos_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  outrosfinanciamentos_pj_11m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  ever_outrosfinanciamentos_pj,
	coalesce(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))),0) as  count_outrosfinanciamentos_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  financiamento_veiculo_pj_11m,
	coalesce(max(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  max_financiamento_veiculo_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  ever_financiamento_veiculo_pj,
	coalesce(count(*) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text),0) as  count_financiamento_veiculo_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text),0) as emprestimos_microcredito_pj_curr,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text),0) as emprestimos_microcredito_pj_1m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text),0) as emprestimos_microcredito_pj_2m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text),0) as emprestimos_microcredito_pj_3m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text),0) as emprestimos_microcredito_pj_4m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text),0) as emprestimos_microcredito_pj_5m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text),0) as emprestimos_microcredito_pj_6m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text),0) as emprestimos_microcredito_pj_7m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text),0) as emprestimos_microcredito_pj_8m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text),0) as emprestimos_microcredito_pj_9m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text),0) as emprestimos_microcredito_pj_10m,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text),0) as emprestimos_microcredito_pj_11m,
	coalesce(max(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text),0) as max_emprestimos_microcredito_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text),0) as ever_emprestimos_microcredito_pj,
	coalesce(count(*) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text),0) as count_emprestimos_microcredito_pj,
	coalesce(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND (t6.lv1 <> ALL (ARRAY['Repasses Interfinanceiros (D)'::text, 'Coobrigação (E)'::text, 'Créditos a Liberar (G)'::text, 'Limite de Crédito (H)'::text, 'Risco Indireto (I)'::text]))),0) as  carteiracredito_pj_curr
   FROM ( SELECT t5.emprestimo_id,
            regexp_replace(t5.lv1, '\d{2}\s-\s'::text, ''::text) AS lv1,
            replace(replace(replace(replace(regexp_replace(t5.lv2, '\d{4}\s-\s'::text, ''::text), '  '::text, ' - '::text), ' -- '::text, ' - '::text), ' - '::text, ' - '::text), 'interfinanceiros'::text, 'Interfinanceiros'::text) AS lv2,
            to_date(t5.data, 'mm/yyyy'::text) AS to_date,
                CASE
                    WHEN t5.valor IS NULL THEN 0::double precision
                    ELSE t5.valor
                END AS valor,
                CASE
                    WHEN t5.max_id IS NULL THEN
                    CASE
                        WHEN date_part('day'::text, t5.data_pedido) >= 16::double precision THEN to_date(to_char(t5.data_pedido - '1 mon'::interval, 'Mon/yy'::text), 'Mon/yy'::text)
                        ELSE to_date(to_char(t5.data_pedido - '2 mons'::interval, 'Mon/yy'::text), 'Mon/yy'::text)
                    END
                    ELSE first_value(to_date(t5.data, 'mm/yyyy'::text)) OVER (PARTITION BY t5.emprestimo_id ORDER BY (to_date(t5.data, 'mm/yyyy'::text)) DESC)
                END AS data_referencia
           FROM ( SELECT t4.emprestimo_id,
                    t4.data_pedido,
                    t4.data_doc,
                    t4.max_id,
                    t4.lv1,
                    t4.nome AS lv2,
                    (jsonb_populate_recordset(NULL::meu2, t4.serie)).data AS data,
                    (jsonb_populate_recordset(NULL::meu2, t4.serie)).valor AS valor
                   FROM ( SELECT t3.emprestimo_id,
                            t3.data_pedido,
                            t3.data_doc,
                            t3.max_id,
                            t3.nome AS lv1,
                            (jsonb_populate_recordset(NULL::meu, t3.detalhes)).nome AS nome,
                            (jsonb_populate_recordset(NULL::meu, t3.detalhes)).serie AS serie
                           FROM ( SELECT t2.emprestimo_id,
                                    to_date((t2.data -> 'data_base'::text) ->> 'Data-Base'::text, 'mm/yyyy'::text) AS data_base,
                                    t2.data_pedido,
									t2.data_doc,
									t2.max_id,
                                    (jsonb_populate_recordset(NULL::lucas, t2.data #> '{por_modalidade}'::text[])).nome AS nome,
                                    (jsonb_populate_recordset(NULL::lucas, t2.data #> '{por_modalidade}'::text[])).detalhes AS detalhes
                                   FROM ( SELECT 
                                   		t1.emprestimo_id,
									    cc.data,
									    t1.data_pedido,
									    COALESCE(to_timestamp(regexp_replace(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text), '\..*', ''), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date as data_doc,
									    t1.max_id
										FROM credito_coleta cc
										JOIN (SELECT 
												t.emprestimo_id,
												max(coalesce(lr.loan_date,lr.date_inserted::date)) as data_pedido,
												max(dp.cnpj) as cnpj,
												max(cc.id) FILTER (WHERE COALESCE(to_timestamp(regexp_replace(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text), '\..*', ''), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted::date + '10 days'::interval)) AS max_id,
												min(cc.id) AS min_id
											FROM credito_coleta cc
											JOIN direct_prospects dp ON "left"(cc.documento::text, 8) = "left"(dp.cnpj::text, 8)
											JOIN offers o ON o.direct_prospect_id = dp.direct_prospect_id
											JOIN loan_requests lr ON lr.offer_id = o.offer_id
											join t_bizcredit_parecer_full t on t.emprestimo_id = lr.loan_request_id
										WHERE cc.documento_tipo::text = 'CNPJ'::text AND cc.tipo::text = 'SCR'::text
											and t.emprestimo_id not in (select emprestimo_id from t_bizcredit_scr_pj_full)
										GROUP BY 1) t1 ON cc.id = COALESCE(t1.max_id, t1.min_id)
									     ) t2) t3) t4) t5) t6
  GROUP BY 1) as modal_json on modal_json.emprestimo_id = t1.emprestimo_id
left join(SELECT t1.emprestimo_id,
		coalesce(cc."data"-> 'erro',cc."data"-> 'error')::text as thin_file
	FROM credito_coleta cc
	JOIN (SELECT t.emprestimo_id,
			max(coalesce(lr.loan_date,lr.date_inserted::date)) as data_pedido,
			max(dp.cnpj) as cnpj,
			max(cc.id) FILTER (WHERE COALESCE(to_timestamp(regexp_replace(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text), '\..*', ''), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date <= coalesce(lr.loan_date,lr.date_inserted::date + '10 days'::interval)) AS max_id,
			min(cc.id) AS min_id
		FROM credito_coleta cc
		JOIN direct_prospects dp ON "left"(cc.documento::text, 8) = "left"(dp.cnpj::text, 8)
		JOIN offers o ON o.direct_prospect_id = dp.direct_prospect_id
		JOIN loan_requests lr ON lr.offer_id = o.offer_id
		join t_bizcredit_parecer_full t on t.emprestimo_id = lr.loan_request_id
	WHERE cc.documento_tipo::text = 'CNPJ'::text AND cc.tipo::text = 'SCR'::text
		and t.emprestimo_id not in (select emprestimo_id from t_bizcredit_scr_pj_full)	
	GROUP BY 1) t1 ON cc.id = COALESCE(t1.max_id, t1.min_id)
	) as modal_erro on modal_erro.emprestimo_id = t1.emprestimo_id		
where t1.emprestimo_id not in (select emprestimo_id from t_bizcredit_scr_pj_full)
		

--CONSULTAS SERASA
insert into t_bizcredit_consultas_serasa_full
(select
	t1.direct_prospect_id,
	coalesce(count(*) filter (where consulta_proprio = 1),0) as consulta_proprio,
	coalesce(count(*) filter (where consulta_factoring = 1 and consulta_proprio = 0),0) as consulta_factoring,
	coalesce(count(*) filter (where consulta_seguradora = 1 and greatest(consulta_proprio,consulta_factoring) = 0),0) as consulta_seguradora,
	coalesce(count(*) filter (where consulta_financeira = 1 and greatest(consulta_proprio,consulta_factoring,consulta_seguradora) = 0),0) as consulta_financeira,	
	coalesce(count(*) filter (where consulta_cobranca = 1 and greatest(consulta_proprio,consulta_factoring,consulta_seguradora,consulta_financeira) = 0),0) as consulta_cobranca,
	coalesce(count(*) filter (where consulta_provedor_dado = 1 and greatest(consulta_proprio,consulta_factoring,consulta_seguradora,consulta_financeira,consulta_cobranca) = 0),0) as consulta_provedor_dado,
	coalesce(count(*) filter (where consulta_fornecedores = 1 and greatest(consulta_proprio,consulta_factoring,consulta_seguradora,consulta_financeira,consulta_cobranca,consulta_provedor_dado) = 0),0) as consulta_fornecedores,
	coalesce(count(*) filter (where greatest(consulta_proprio,consulta_factoring,consulta_seguradora,consulta_financeira,consulta_cobranca,consulta_provedor_dado,consulta_fornecedores) = 0),0) as consulta_indefinido
from t_bizcredit_approval_tape_full t1
left join (SELECT 
	direct_prospect_id,
	consultas,
    CASE
        WHEN ("position"(t2.consultas,'INVEST DIR'::text) > 0 OR 
        	"position"(t2.consultas,'CREDITORIO'::text) > 0 OR 
        	"position"(t2.consultas,'FUNDO DE INVE'::text) > 0 OR 
        	"position"(t2.consultas,'SECURITIZADORA'::text) > 0 OR 
        	"position"(t2.consultas,'FACTORING'::text) > 0 OR 
        	("position"(t2.consultas,'FOMENTO'::text) > 0 and "position"(t2.consultas,'AGENCIA'::text) = 0) OR 
           	"position"(t2.consultas,'FIDC'::text) > 0 OR 
        	"position"(t2.consultas,'NOVA S R M'::text) > 0 OR 
        	"position"(t2.consultas,'RED SA'::text) > 0 OR 
        	"position"(t2.consultas,'DEL MONTE SERVICOS'::text) > 0 OR 
        	"position"(t2.consultas,'123QRED'::text) > 0 OR 
        	"position"(t2.consultas,'SERVICOS FINANCEIRO'::text) > 0 or
        	"position"(t2.consultas,'ADIANTA PAGAMENTO '::text) > 0 OR
			"position"(t2.consultas,'ASAAS GESTAO'::text) > 0 OR
			"position"(t2.consultas,'BRA GESTORA '::text) > 0 OR
			"position"(t2.consultas,'DUNAS SERVICOS FINANCEIROS'::text) > 0 OR
			"position"(t2.consultas,'FEDERAL INVEST'::text) > 0 OR
			"position"(t2.consultas,'HOOME CREDIT'::text) > 0 OR
			"position"(t2.consultas,'INVISTA CREDITO'::text) > 0 OR
			"position"(t2.consultas,'LIDERFAC TECNOLOGIA'::text) > 0 OR
			"position"(t2.consultas,'INVEST DIREITOS'::text) > 0 OR
			"position"(t2.consultas,'PLATAFORMA ANTECIPA'::text) > 0 OR
			"position"(t2.consultas,'PREMIUM SERVICOS FINANCEIROS'::text) > 0 OR
			"position"(t2.consultas,'PRIX EMPRESARIAL'::text) > 0 OR
			"position"(t2.consultas,'TIRRENO'::text) > 0 OR
			"position"(t2.consultas,'ADIANTA'::text) > 0 OR
			"position"(t2.consultas,'VIA CAPITAL GESTAO'::text) > 0) THEN 1
        ELSE 0
    END AS consulta_factoring,
    CASE
        WHEN ("position"(t2.consultas,'SEGURADORA'::text) > 0 OR
			"position"(t2.consultas,'AMIL ASSISTENCIA'::text) > 0 OR
			"position"(t2.consultas,'CONSORCIOS'::text) > 0 OR
			"position"(t2.consultas,'BARIGUI COMPANHIA HIPOTECARIA'::text) > 0 OR
			"position"(t2.consultas,'BERKLEY INTERNATIONAL'::text) > 0 OR
			"position"(t2.consultas,'BRADESCO AUTO '::text) > 0 OR
			"position"(t2.consultas,'CESCE BRASIL'::text) > 0 OR
			"position"(t2.consultas,'CIA DE ARRENDAMENTO MERCANTIL'::text) > 0 OR
			"position"(t2.consultas,'CONSORCIO'::text) > 0 OR
			"position"(t2.consultas,'EULER HERMES'::text) > 0 OR
			"position"(t2.consultas,'SEGURO'::text) > 0 OR
			"position"(t2.consultas,'PORTOSEG'::text) > 0 OR
			"position"(t2.consultas,'RODOBENS ADMINISTRACAO'::text) > 0 OR
			"position"(t2.consultas,'SAMETRADE OPERADORA'::text) > 0 OR
			"position"(t2.consultas,'SUL AMERICA SEG'::text) > 0 OR
			"position"(t2.consultas,'SULMED ASSISTENCIA'::text) > 0 OR
			"position"(t2.consultas,'GARANTIA AFIANCADORA'::text) > 0 OR
			"position"(t2.consultas,'ODONTOPREV'::text) > 0 OR
			"position"(t2.consultas,'UNIVIDA'::text) > 0 OR
			"position"(t2.consultas,'UNIMED'::text) > 0) THEN 1
        ELSE 0
    END AS consulta_seguradora,
    CASE
        WHEN ("position"(t2.consultas,'COBRANCA'::text) > 0 or
        	"position"(t2.consultas,'ASSECAD BRASIL'::text) > 0 or
        	"position"(t2.consultas,'CONVENIO CADASTRAL'::text) > 0 or
        	"position"(t2.consultas,'INFOR-LINE'::text) > 0 or
			"position"(t2.consultas,'OFFICE SOLUCOES &'::text) > 0) THEN 1
        ELSE 0
    END AS consulta_cobranca,
    CASE
        WHEN ("position"(t2.consultas,'ASSERTIVA TECNOLOGIA'::text) > 0 OR
			"position"(t2.consultas,'BANCODOC'::text) > 0 OR
			"position"(t2.consultas,'BIG DATA SOLUCOES'::text) > 0 OR
			"position"(t2.consultas,'BOMPARA TECNOLOGIA'::text) > 0 OR
			"position"(t2.consultas,'CHEQUE-PRE'::text) > 0 OR
			"position"(t2.consultas,'DIRECT SMART DATA'::text) > 0 OR
			"position"(t2.consultas,'DUN & BRAD'::text) > 0 OR
			"position"(t2.consultas,'FD DO BRASIL'::text) > 0 OR
			"position"(t2.consultas,'OMNI'::text) > 0 OR
			"position"(t2.consultas,'METADADOS'::text) > 0 OR
			"position"(t2.consultas,'OBVIO BRASIL'::text) > 0 OR
			"position"(t2.consultas,'CANAL DA INTERNET'::text) > 0 OR
			"position"(t2.consultas,'CENIN CENTRO'::text) > 0 OR
			"position"(t2.consultas,'CHECKLOG'::text) > 0 OR
			"position"(t2.consultas,'COFACE BRASIL'::text) > 0 OR
			"position"(t2.consultas,'MR MARQUES SERVICES'::text) > 0 OR
			"position"(t2.consultas,'UPLEXIS'::text) > 0) THEN 1
        ELSE 0
    END AS consulta_provedor_dado,
    CASE
        WHEN ("position"(t2.consultas,'CREDITO'::text) > 0 OR
			"position"(t2.consultas,'AGENCIA DE FOMENTO'::text) > 0 OR
			"position"(t2.consultas,'AGENCIA FOMENTO'::text) > 0 OR
			"position"(t2.consultas,'INVESTIMEN'::text) > 0 OR
			"position"(t2.consultas,'BANCO '::text) > 0 OR
			"position"(t2.consultas,'BANESE'::text) > 0 OR
			"position"(t2.consultas,'BCO NACIONAL'::text) > 0 OR
			"position"(t2.consultas,'CREDIT'::text) > 0 OR
			"position"(t2.consultas,'CAIXA ESTADUAL S/A AGENCIA'::text) > 0 OR
			"position"(t2.consultas,'CEF'::text) > 0 OR
			"position"(t2.consultas,'MUTUO'::text) > 0 OR
			"position"(t2.consultas,'CEN COOPERATIVAS CRED'::text) > 0 OR
			"position"(t2.consultas,'CENTRO DE APOIO AOS PEQUENOS'::text) > 0 OR
			"position"(t2.consultas,'UNICRED'::text) > 0 OR
			"position"(t2.consultas,'COOP CENTRAL BASE DE SERV DE RESPON'::text) > 0 OR
			"position"(t2.consultas,'COOPERATIVA CEN CRED'::text) > 0 OR
			"position"(t2.consultas,'COOPERATIVA CRED'::text) > 0 OR
			"position"(t2.consultas,'COOPERATIVA CREDITO'::text) > 0 OR
			"position"(t2.consultas,'COOPERATIVA POUPANCA '::text) > 0 OR
			"position"(t2.consultas,'CRED MUT'::text) > 0 OR
			"position"(t2.consultas,'CORRESPONDENTE'::text) > 0 OR
			"position"(t2.consultas,'CREDUNI'::text) > 0 OR
			"position"(t2.consultas,'CONSORCIOS'::text) > 0 OR
			"position"(t2.consultas,'SERV FINANCEIROS'::text) > 0 OR
			"position"(t2.consultas,'SERVICOS FINANCEIROS'::text) > 0 OR
			"position"(t2.consultas,'LENDING'::text) > 0 OR
			"position"(t2.consultas,'EMPRESTIMO'::text) > 0 OR
			"position"(t2.consultas,'NEXOOS'::text) > 0 OR
			"position"(t2.consultas,'TUTU DIGITAL'::text) > 0 OR
			"position"(t2.consultas,'CREDIUNI'::text) > 0 OR
			"position"(t2.consultas,'CARTOES CRED'::text) > 0 OR
			"position"(t2.consultas,'FINANC'::text) > 0) THEN 1
        ELSE 0
    END AS consulta_financeira,
    CASE
        WHEN ("position"(t2.consultas,'INFORMATICA'::text) > 0 OR
			"position"(t2.consultas,'TELECOM'::text) > 0 OR
			"position"(t2.consultas,'3M DO BRASIL'::text) > 0 OR
			"position"(t2.consultas,'ARTIGOS'::text) > 0 OR
			"position"(t2.consultas,'DISTRIB'::text) > 0 OR
			"position"(t2.consultas,'VEICU'::text) > 0 OR
			"position"(t2.consultas,'CONFEC'::text) > 0 OR
			"position"(t2.consultas,'CALCA'::text) > 0 OR
			"position"(t2.consultas,'COMERC'::text) > 0 OR
			"position"(t2.consultas,'HOTE'::text) > 0 OR
			"position"(t2.consultas,'COMBU'::text) > 0 OR
			"position"(t2.consultas,'AUTO'::text) > 0 OR
			"position"(t2.consultas,'TRANSP'::text) > 0 OR
			"position"(t2.consultas,'FERR'::text) > 0 OR
			"position"(t2.consultas,'INDUSTR'::text) > 0 OR
			"position"(t2.consultas,'EMBALA'::text) > 0 OR
			"position"(t2.consultas,'PLAST'::text) > 0 OR
			"position"(t2.consultas,'ADUBO'::text) > 0 OR
			"position"(t2.consultas,'TECNOLOGIA'::text) > 0 OR
			"position"(t2.consultas,'CONSULT'::text) > 0 OR
			"position"(t2.consultas,'VIDR'::text) > 0 OR
			"position"(t2.consultas,'AGRIC'::text) > 0 OR
			"position"(t2.consultas,'PROD'::text) > 0 OR
			"position"(t2.consultas,'S/A'::text) > 0 OR
			"position"(t2.consultas,'ARAMADOS'::text) > 0 OR
			"position"(t2.consultas,'COMEX'::text) > 0 OR
			"position"(t2.consultas,'COM DE'::text) > 0 OR
			"position"(t2.consultas,'ATACAD'::text) > 0 OR
			"position"(t2.consultas,'EXPORT'::text) > 0 OR
			"position"(t2.consultas,'MAQUINA'::text) > 0 OR
			"position"(t2.consultas,'RECAUC'::text) > 0 OR
			"position"(t2.consultas,'IMOV'::text) > 0 OR
			"position"(t2.consultas,'LOCAC'::text) > 0 OR
			"position"(t2.consultas,'METAL'::text) > 0 OR
			"position"(t2.consultas,'ALUMI'::text) > 0 OR
			"position"(t2.consultas,'ELETR'::text) > 0 OR
			"position"(t2.consultas,'IMPORT'::text) > 0 OR
			"position"(t2.consultas,'AMBEV'::text) > 0 OR
			"position"(t2.consultas,'ASSOCI'::text) > 0 OR
			"position"(t2.consultas,'TEXTIL'::text) > 0 OR
			"position"(t2.consultas,'FARMA'::text) > 0 OR
			"position"(t2.consultas,'CHUMBA'::text) > 0 OR
			"position"(t2.consultas,'EQUIP'::text) > 0 OR
			"position"(t2.consultas,'PECAS'::text) > 0 OR
			"position"(t2.consultas,'COMPANHIA'::text) > 0 OR
			"position"(t2.consultas,'MODA'::text) > 0 OR
			"position"(t2.consultas,'SUPRI'::text) > 0 OR
			"position"(t2.consultas,'ACELORMITTAL'::text) > 0 OR
			"position"(t2.consultas,'ARMAZ'::text) > 0 OR
			"position"(t2.consultas,'CIMENTO'::text) > 0 OR
			"position"(t2.consultas,'SHOP'::text) > 0 OR
			"position"(t2.consultas,'QUIM'::text) > 0 OR
			"position"(t2.consultas,'MEDIC'::text) > 0 OR
			"position"(t2.consultas,'EMPREE'::text) > 0 OR
			"position"(t2.consultas,'CARNE'::text) > 0 OR
			"position"(t2.consultas,'ALIMENT'::text) > 0 OR
			"position"(t2.consultas,'& CIA'::text) > 0 OR
			"position"(t2.consultas,'IND E COM'::text) > 0 OR
			"position"(t2.consultas,'ETIQUE'::text) > 0 OR
			"position"(t2.consultas,'BIGCARG'::text) > 0 OR
			"position"(t2.consultas,'BIMBO'::text) > 0 OR
			"position"(t2.consultas,'ALARM'::text) > 0 OR
			"position"(t2.consultas,'IMOB'::text) > 0 OR
			"position"(t2.consultas,'DIESEL'::text) > 0 OR
			"position"(t2.consultas,'LOGIST'::text) > 0 OR
			"position"(t2.consultas,'BRF S/A'::text) > 0 OR
			"position"(t2.consultas,'CARGAS'::text) > 0 OR
			"position"(t2.consultas,'ARQUIT'::text) > 0 OR
			"position"(t2.consultas,'CONSTRU'::text) > 0 OR
			"position"(t2.consultas,'CABOS'::text) > 0 OR
			"position"(t2.consultas,'PNEU'::text) > 0 OR
			"position"(t2.consultas,'LOJISTA'::text) > 0 OR
			"position"(t2.consultas,'TRANSMI'::text) > 0 OR
			"position"(t2.consultas,'LUBRIFIC'::text) > 0 OR
			"position"(t2.consultas,'CARTONA'::text) > 0 OR
			"position"(t2.consultas,'CASA ALADIM'::text) > 0 OR
			"position"(t2.consultas,'TINT'::text) > 0 OR
			"position"(t2.consultas,'COMPENSADOS'::text) > 0 OR
			"position"(t2.consultas,'HIDRA'::text) > 0 OR
			"position"(t2.consultas,'CERAMICA'::text) > 0 OR
			"position"(t2.consultas,'AGRO'::text) > 0 OR
			"position"(t2.consultas,'CERVEJ'::text) > 0 OR
			"position"(t2.consultas,'FABRIL'::text) > 0 OR
			"position"(t2.consultas,'IND COM'::text) > 0 OR
			"position"(t2.consultas,'CLARO SA'::text) > 0 OR
			"position"(t2.consultas,'COLCHO'::text) > 0 OR
			"position"(t2.consultas,'GRAF'::text) > 0 OR
			"position"(t2.consultas,'COM E IND'::text) > 0 OR
			"position"(t2.consultas,'COML'::text) > 0 OR
			"position"(t2.consultas,'COMPRE FACIL'::text) > 0 OR
			"position"(t2.consultas,'SOFTWARE'::text) > 0 OR
			"position"(t2.consultas,'CONCRET'::text) > 0 OR
			"position"(t2.consultas,'FABRIC'::text) > 0 OR
			"position"(t2.consultas,'CONDOM'::text) > 0 OR
			"position"(t2.consultas,'PROJET'::text) > 0 OR
			"position"(t2.consultas,'CROMAG'::text) > 0 OR
			"position"(t2.consultas,'DDTOTAL'::text) > 0 OR
			"position"(t2.consultas,'CONDICI'::text) > 0 OR
			"position"(t2.consultas,'DHL WORLD'::text) > 0 OR
			"position"(t2.consultas,'MALHA'::text) > 0 OR
			"position"(t2.consultas,'DURATEX'::text) > 0 OR
			"position"(t2.consultas,'MOBILI'::text) > 0 OR
			"position"(t2.consultas,'MATER'::text) > 0 OR
			"position"(t2.consultas,'BOMBAS'::text) > 0 OR
			"position"(t2.consultas,'EDITOR'::text) > 0 OR
			"position"(t2.consultas,'ELASTIC'::text) > 0 OR
			"position"(t2.consultas,'PORCELAN'::text) > 0 OR
			"position"(t2.consultas,'ENERG'::text) > 0 OR
			"position"(t2.consultas,'ESTOF'::text) > 0 OR
			"position"(t2.consultas,'ESTRUTURA'::text) > 0 OR
			"position"(t2.consultas,'PRINT'::text) > 0 OR
			"position"(t2.consultas,'TURISM'::text) > 0 OR
			"position"(t2.consultas,'COMUNIC'::text) > 0 OR
			"position"(t2.consultas,'FERTILI'::text) > 0 OR
			"position"(t2.consultas,'TECIDOS'::text) > 0 OR
			"position"(t2.consultas,'NUTRIC'::text) > 0 OR
			"position"(t2.consultas,'FORMAS'::text) > 0 OR
			"position"(t2.consultas,'PETROL'::text) > 0 OR
			"position"(t2.consultas,'MOTO'::text) > 0 OR
			"position"(t2.consultas,'FRIGO'::text) > 0 OR
			"position"(t2.consultas,'FUNDICAO'::text) > 0 OR
			"position"(t2.consultas,'JOIA'::text) > 0 OR
			"position"(t2.consultas,'ELECTR'::text) > 0 OR
			"position"(t2.consultas,'SORVETE'::text) > 0 OR
			"position"(t2.consultas,'GERDAU'::text) > 0 OR
			"position"(t2.consultas,'PROVEDOR'::text) > 0 OR
			"position"(t2.consultas,'GOOGLE'::text) > 0 OR
			"position"(t2.consultas,'ADESIVO'::text) > 0 OR
			"position"(t2.consultas,'TREINA'::text) > 0 OR
			"position"(t2.consultas,'IMUNIZ'::text) > 0 OR
			"position"(t2.consultas,'COMPUT'::text) > 0 OR
			"position"(t2.consultas,'INNOVA'::text) > 0 OR
			"position"(t2.consultas,'ENSIN'::text) > 0 OR
			"position"(t2.consultas,'COM IMP MAT'::text) > 0 OR
			"position"(t2.consultas,'MOVEIS'::text) > 0 OR
			"position"(t2.consultas,'ARTEF'::text) > 0 OR
			"position"(t2.consultas,'BRINDE'::text) > 0 OR
			"position"(t2.consultas,'COMPONENTES'::text) > 0 OR
			"position"(t2.consultas,'LABOR'::text) > 0 OR
			"position"(t2.consultas,'LATICI'::text) > 0 OR
			"position"(t2.consultas,'LOCALIZA'::text) > 0 OR
			"position"(t2.consultas,'GERADORES'::text) > 0 OR
			"position"(t2.consultas,'BORR'::text) > 0 OR
			"position"(t2.consultas,'ACESSOR'::text) > 0 OR
			"position"(t2.consultas,'VESTUA'::text) > 0 OR
			"position"(t2.consultas,'MADEIR'::text) > 0 OR
			"position"(t2.consultas,'CONSTR'::text) > 0 OR
			"position"(t2.consultas,'MERCAD'::text) > 0 OR
			"position"(t2.consultas,'MECANICA'::text) > 0 OR
			"position"(t2.consultas,'SUCO'::text) > 0 OR
			"position"(t2.consultas,'GASTRO'::text) > 0 OR
			"position"(t2.consultas,'ENGEN'::text) > 0 OR
			"position"(t2.consultas,'COPIADO'::text) > 0 OR
			"position"(t2.consultas,'MOLAS'::text) > 0 OR
			"position"(t2.consultas,'CONTABILI'::text) > 0 OR
			"position"(t2.consultas,'COMEST'::text) > 0 OR
			"position"(t2.consultas,'REFRI'::text) > 0 OR
			"position"(t2.consultas,'SEMENTES'::text) > 0 OR
			"position"(t2.consultas,'ROLAMEN'::text) > 0 OR
			"position"(t2.consultas,'PESCADO'::text) > 0 OR
			"position"(t2.consultas,'PEPSICO'::text) > 0 OR
			"position"(t2.consultas,'POSTO'::text) > 0 OR
			"position"(t2.consultas,'RESINAS'::text) > 0 OR
			"position"(t2.consultas,'MAGAZINE'::text) > 0 OR
			"position"(t2.consultas,'DOCES'::text) > 0 OR
			"position"(t2.consultas,'RADIO'::text) > 0 OR
			"position"(t2.consultas,'CELUL'::text) > 0 OR
			"position"(t2.consultas,'ROBERT BOSH'::text) > 0 OR
			"position"(t2.consultas,'SANITARIOS'::text) > 0 OR
			"position"(t2.consultas,'RODOVIAR'::text) > 0 OR
			"position"(t2.consultas,'OPTIC'::text) > 0 OR
			"position"(t2.consultas,'CAMINHO'::text) > 0 OR
			"position"(t2.consultas,'SOUZA CRUZ'::text) > 0 OR
			"position"(t2.consultas,'TAMBASA'::text) > 0 OR
			"position"(t2.consultas,'TECELAGEM'::text) > 0 OR
			"position"(t2.consultas,'TECNIC'::text) > 0 OR
			"position"(t2.consultas,'SISTEMAS'::text) > 0 OR
			"position"(t2.consultas,'TOTVS'::text) > 0 OR
			"position"(t2.consultas,'REFRES'::text) > 0 OR
			"position"(t2.consultas,'UNIVEN HEALTHCARE'::text) > 0 OR
			"position"(t2.consultas,'MARMOR'::text) > 0 OR
			"position"(t2.consultas,'AROMA'::text) > 0 OR
			"position"(t2.consultas,'VINICO'::text) > 0 OR
			"position"(t2.consultas,'WHIRPOOL'::text) > 0 OR
			"position"(t2.consultas,'OTICA'::text) > 0 OR
			"position"(t2.consultas,'ARTES'::text) > 0 OR
			"position"(t2.consultas,'ARGAMASSA'::text) > 0 OR
			"position"(t2.consultas,'IMPRESSAO'::text) > 0 OR
			"position"(t2.consultas,'VIAGENS'::text) > 0 OR
			"position"(t2.consultas,'FABER CATELL'::text) > 0 OR
			"position"(t2.consultas,'INOXI'::text) > 0 OR
			"position"(t2.consultas,'BARBA'::text) > 0 OR
			"position"(t2.consultas,'COSMETIC'::text) > 0 OR
			"position"(t2.consultas,'CACHORRO'::text) > 0 OR
			"position"(t2.consultas,'BR MALLS'::text) > 0 OR
			"position"(t2.consultas,'CONTABIL'::text) > 0 OR
			"position"(t2.consultas,'COMPONENTS'::text) > 0 OR
			"position"(t2.consultas,'CEMEAP CEN MED'::text) > 0 OR
			"position"(t2.consultas,'DEBUTANTES'::text) > 0 OR
			"position"(t2.consultas,'CESTA BASICA'::text) > 0 OR
			"position"(t2.consultas,'MEIOS'::text) > 0 OR
			"position"(t2.consultas,'CIMCAL LTDA'::text) > 0 OR
			"position"(t2.consultas,'COLORCON'::text) > 0 OR
			"position"(t2.consultas,'COLSON'::text) > 0 OR
			"position"(t2.consultas,'PROPAGANDA'::text) > 0 OR
			"position"(t2.consultas,'CONCENTRADOS'::text) > 0 OR
			"position"(t2.consultas,'CONVES WEB'::text) > 0 OR
			"position"(t2.consultas,'CAFE'::text) > 0 OR
			"position"(t2.consultas,'COOPERVISION'::text) > 0 OR
			"position"(t2.consultas,'CORA PAGAMENTOS'::text) > 0 OR
			"position"(t2.consultas,'COMPONENTE'::text) > 0 OR
			"position"(t2.consultas,'DENTAL'::text) > 0 OR
			"position"(t2.consultas,'ENDUTEX BRASIL'::text) > 0 OR
			"position"(t2.consultas,'ENERBRAX'::text) > 0 OR
			"position"(t2.consultas,'ENGARRAFAMENTO'::text) > 0 OR
			"position"(t2.consultas,'PERFUMARIA'::text) > 0 OR
			"position"(t2.consultas,'VAREJISTA'::text) > 0 OR
			"position"(t2.consultas,'BATTERIES'::text) > 0 OR
			"position"(t2.consultas,'GSI CREOS'::text) > 0 OR
			"position"(t2.consultas,'HORTI'::text) > 0 OR
			"position"(t2.consultas,'I M P LTDA'::text) > 0 OR
			"position"(t2.consultas,'ESPORTE'::text) > 0 OR
			"position"(t2.consultas,'INGRAM'::text) > 0 OR
			"position"(t2.consultas,'INTERNATIONAL PAPER'::text) > 0 OR
			"position"(t2.consultas,'INSUMOS'::text) > 0 OR
			"position"(t2.consultas,'KNAUF DO BRASIL'::text) > 0 OR
			"position"(t2.consultas,'LAVANDERIA'::text) > 0 OR
			"position"(t2.consultas,'LANCHO'::text) > 0 OR
			"position"(t2.consultas,'LOSINOX'::text) > 0 OR
			"position"(t2.consultas,'ILUMINA'::text) > 0 OR
			"position"(t2.consultas,'REVESTIMENTO'::text) > 0 OR
			"position"(t2.consultas,'FRANQUIA'::text) > 0 OR
			"position"(t2.consultas,'PAPEIS'::text) > 0 OR
			"position"(t2.consultas,'MERCANTIL MOR'::text) > 0 OR
			"position"(t2.consultas,'JEANS'::text) > 0 OR
			"position"(t2.consultas,'MINERACAO'::text) > 0 OR
			"position"(t2.consultas,'FESTA'::text) > 0 OR
			"position"(t2.consultas,'LIXO'::text) > 0 OR
			"position"(t2.consultas,'NEOPRENE'::text) > 0 OR
			"position"(t2.consultas,'NESTLE'::text) > 0 OR
			"position"(t2.consultas,'MAT PARA'::text) > 0 OR
			"position"(t2.consultas,'PAO DE'::text) > 0 OR
			"position"(t2.consultas,'EDUCATION'::text) > 0 OR
			"position"(t2.consultas,'POUSADA'::text) > 0 OR
			"position"(t2.consultas,'GRAPH'::text) > 0 OR
			"position"(t2.consultas,'ENCOMENDAS'::text) > 0 OR
			"position"(t2.consultas,'SPORTS'::text) > 0 OR
			"position"(t2.consultas,'RANDSTAD'::text) > 0 OR
			"position"(t2.consultas,'RAPPI'::text) > 0 OR
			"position"(t2.consultas,'RECAPAGE'::text) > 0 OR
			"position"(t2.consultas,'FOOD'::text) > 0 OR
			"position"(t2.consultas,'NOBREAK'::text) > 0 OR
			"position"(t2.consultas,'ROBERT BOSCH'::text) > 0 OR
			"position"(t2.consultas,'EVENTOS'::text) > 0 OR
			"position"(t2.consultas,'RUSSER BRASIL'::text) > 0 OR
			"position"(t2.consultas,'OPERADOR'::text) > 0 OR
			"position"(t2.consultas,'GAS'::text) > 0 OR
			"position"(t2.consultas,'FIOS'::text) > 0 OR
			"position"(t2.consultas,'ANTENAS'::text) > 0 OR
			"position"(t2.consultas,'PISOS'::text) > 0 OR
			"position"(t2.consultas,'SARTORIUS'::text) > 0 OR
			"position"(t2.consultas,'COMPON ELET'::text) > 0 OR
			"position"(t2.consultas,'SODEXHO'::text) > 0 OR
			"position"(t2.consultas,'LIMPE'::text) > 0 OR
			"position"(t2.consultas,'SOMAFERTIL'::text) > 0 OR
			"position"(t2.consultas,'COMPRESS'::text) > 0 OR
			"position"(t2.consultas,'MOLDADOS'::text) > 0 OR
			"position"(t2.consultas,'CARTUCHOS'::text) > 0 OR
			"position"(t2.consultas,'TENCOREALTY'::text) > 0 OR
			"position"(t2.consultas,'TRIVALE'::text) > 0 OR
			"position"(t2.consultas,'UNIFORT LTDA'::text) > 0 OR
			"position"(t2.consultas,'UNISYS'::text) > 0 OR
			"position"(t2.consultas,'UNIVAR'::text) > 0 OR
			"position"(t2.consultas,'USECORP LTDA'::text) > 0 OR
			"position"(t2.consultas,'MANGUEIRAS'::text) > 0 OR
			"position"(t2.consultas,'VELLO DIGITAL'::text) > 0 OR
			"position"(t2.consultas,'UTILIDADES'::text) > 0 OR
			"position"(t2.consultas,'VIAPARK'::text) > 0 OR
			"position"(t2.consultas,'DECORA'::text) > 0 OR
			"position"(t2.consultas,'SAPATARIA'::text) > 0) THEN 1
        ELSE 0
    END AS consulta_fornecedores,
    CASE
        WHEN ("position"(t2.razao_social,t2.consultas) > 0) THEN 1
        ELSE 0
    END AS consulta_proprio
   FROM ( SELECT t1.direct_prospect_id,
            ((cc.data -> 'consulta_empresas'::text) -> jsonb_object_keys(cc.data -> 'consulta_empresas'::text)) ->> 'nome'::text AS consultas,
            t1.razao_social
           FROM credito_coleta cc
           JOIN (SELECT 
					dp.direct_prospect_id,
					max(dp."name") as razao_social,
				    max(dp.cnpj) as cnpj,
				    max(dp.cpf) as cpf,
				    max(id) AS max_id
				FROM credito_coleta cc
				JOIN direct_prospects dp ON "left"(cc.documento::text, 8) = "left"(dp.cnpj::text, 8)
				JOIN offers o ON o.direct_prospect_id = dp.direct_prospect_id
				JOIN loan_requests lr ON lr.offer_id = o.offer_id
				join t_bizcredit_parecer_full t on t.emprestimo_id = lr.loan_request_id
				WHERE to_date((cc.data -> 'datetime'::text) ->> 'data'::text, 'yyyymmdd'::text) <= coalesce(lr.loan_date,lr.date_inserted::date + '10 days'::interval) AND (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying, 'RelatoBasico'::character varying]::text[]))
					and dp.direct_prospect_id not in (select direct_prospect_id from t_bizcredit_consultas_serasa_full)
				GROUP BY dp.direct_prospect_id) t1 ON cc.id = t1.max_id
        union all
         SELECT t1.direct_prospect_id,
            ((cc.data -> 'consulta_spc'::text) -> jsonb_object_keys(cc.data -> 'consulta_spc'::text)) ->> 'nome'::text AS consultas,
            t1.razao_social
           FROM credito_coleta cc
           JOIN (SELECT 
					dp.direct_prospect_id,
					max(dp."name") as razao_social,
				    max(dp.cnpj) as cnpj,
				    max(dp.cpf) as cpf,
				    max(id) AS max_id
				FROM credito_coleta cc
				JOIN direct_prospects dp ON "left"(cc.documento::text, 8) = "left"(dp.cnpj::text, 8)
				JOIN offers o ON o.direct_prospect_id = dp.direct_prospect_id
				JOIN loan_requests lr ON lr.offer_id = o.offer_id
				join t_bizcredit_parecer_full t on t.emprestimo_id = lr.loan_request_id
				WHERE to_date((cc.data -> 'datetime'::text) ->> 'data'::text, 'yyyymmdd'::text) <= coalesce(lr.loan_date,lr.date_inserted::date + '10 days'::interval) AND (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying, 'RelatoBasico'::character varying]::text[]))
					and dp.direct_prospect_id not in (select direct_prospect_id from t_bizcredit_consultas_serasa_full)
				GROUP BY dp.direct_prospect_id) t1 ON cc.id = t1.max_id
            ) t2
	where consultas != 'CREDITLOOP CORRESPONDENTE BANCARIO' and consultas != ''
	) as t3 on t3.direct_prospect_id = t1.direct_prospect_id
where t1.direct_prospect_id not in (select direct_prospect_id from t_bizcredit_consultas_serasa_full)
group by 1)


insert into t_bizcredit_materialidade_neoway_full
select 
	*
from(select
		t.emprestimo_id,
		row_number() over (partition by t.emprestimo_id order by cc.data_coleta::date desc) indice_coleta,
		(cc.data->'_metadata'->'empresas'->'_metadata'->'licencasAmbientais'->'source' is not null)::int as tem_licenca_ambiental,
		(cc.data->'_metadata'->'empresas'->'_metadata'->'aeronaves'->'source' is not null)::int as tem_aeronave,
		coalesce(jsonb_array_length(cc.data->'inpiMarcas'),'0')::bigint as count_inpi_marcas,
		coalesce((cc.data->'detran'->>'totalVeiculos'),'0')::bigint as total_veiculos,
		coalesce(jsonb_array_length(cc.data->'imoveis'),'0')::bigint as count_imoveis,
		coalesce((cc.data->'totalObras'->>'quantidadeArts'),'0')::bigint as count_obras,
		(coalesce(position('EXPORTACAO' in cc.data->>'indicadoresFinanceiros'),0) > 0)::int as tem_exportacao,
		coalesce(jsonb_array_length(cc.data->'pat'),'0')::bigint as count_pat
	from public.credito_coleta cc
	join public.direct_prospects dp on left(dp.cnpj,8) = left(cc.documento,8)
	join public.offers o on o.direct_prospect_id = dp.direct_prospect_id
	join public.loan_requests lr on lr.offer_id = o.offer_id
	join t_bizcredit_parecer_full t on t.emprestimo_id = lr.loan_request_id
	where cc.tipo = 'Neoway' and cc.data_coleta::date <= coalesce(lr.loan_date,lr.date_inserted::date + '10 days'::interval)
		and emprestimo_id not in (select emprestimo_id from t_bizcredit_materialidade_neoway_full)) as t1
where indice_coleta = 1	
	

insert into t_bizcredit_serasa_score
SELECT 
	t1.direct_prospect_id,
	(cc.data ->> 'score'::text)::double precision AS serasa_coleta,
	(cc.data ->> 'pd'::text)::double precision AS pd_serasa
FROM credito_coleta cc
JOIN (SELECT 
		dp.direct_prospect_id,
		max(dp."name") as razao_social,
	    max(dp.cnpj) as cnpj,
	    max(dp.cpf) as cpf,
	    max(id) AS max_id
	FROM credito_coleta cc
	JOIN direct_prospects dp ON "left"(cc.documento::text, 8) = "left"(dp.cnpj::text, 8)
	JOIN offers o ON o.direct_prospect_id = dp.direct_prospect_id
	JOIN loan_requests lr ON lr.offer_id = o.offer_id
	join t_bizcredit_parecer_full t on t.emprestimo_id = lr.loan_request_id
	WHERE to_date((cc.data -> 'datetime'::text) ->> 'data'::text, 'yyyymmdd'::text) <= coalesce(lr.loan_date,lr.date_inserted::date + '10 days'::interval) AND (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying, 'RelatoBasico'::character varying]::text[]))
		and dp.direct_prospect_id not in (select direct_prospect_id from t_bizcredit_serasa_score)
	GROUP BY 1) t1 ON cc.id = t1.max_id

	
create table t_bizcredit_bigdata_endereco_data as
select 
	t1.cpf,
	coalesce(max(upper(t1.tipo || ' ' || t1.rua || ' ' || t1.numero)) filter (where tipo_endereco = 'HOME' and t1.cep != dp.zip_code),
		max(upper(t1.tipo || ' ' || t1.rua || ' ' || t1.numero)) filter (where indice_endereco = 1))as endereco1
from (select 
		cpf, 
		enderecos->>'Typology' as tipo,
		enderecos->>'AddressMain' as rua,
		enderecos->>'Number' as numero,
		enderecos->>'Complement' as complemento,
		enderecos->>'ZipCode' as cep,
		enderecos->>'Type' as tipo_endereco,
		row_number() over (partition by t1.cpf order by enderecos->>'Type') as indice_endereco
	from t_bigdata_endereco_pf t1,
		jsonb_array_elements(data_output) as resultado,
		jsonb_array_elements(resultado->'ExtendedAddresses'->'Addresses') as enderecos) as t1
join t_bizcredit_signers_full t2 on t2.major_signer_cpf = t1.cpf
join t_bizcredit_approval_tape_full t3 on t3.loan_request_id = t2.loan_request_id
join public.direct_prospects dp on dp.direct_prospect_id = t3.direct_prospect_id
group by 1



insert into t_bizcredit_irpf_full
select 
	t.direct_prospect_id,
	coalesce(p1.url,p2.url) as url,
	t2.cpf,
	t2.codigo_bens_direitos,
	t2.valor_bens_direitos,
	t2.count_dependentes
from data_science.irpf_full_sem_duplicados as p1
full outer join data_science.carga_3_irpf as p2 on p2.url = p1.url
join public.documents d on d.url = coalesce(p1.url,p2.url)
join public.direct_prospects dp on dp.client_id = d.client_id
join lucas_leal.t_bizcredit_approval_tape_full t on t.direct_prospect_id = dp.direct_prospect_id and d.datetime_inserted::date between t.data_criacao::date - '15 days'::interval and t.data_criacao::date + '15 days'::interval 
left join(select
		coalesce(p1.url,p2.url) as url,
		regexp_replace(translate(coalesce(p1.retorno_irpf,p2.retorno_irpf)->'data'->'identificacao'->>'cpf','.-',''),'.*\s','') as cpf,
		(jsonb_array_elements(coalesce(p1.retorno_irpf,p2.retorno_irpf)->'data'->'bens_direitos'->'registros')->>'normalizado_codigo')::int as codigo_bens_direitos,
		(jsonb_array_elements(coalesce(p1.retorno_irpf,p2.retorno_irpf)->'data'->'bens_direitos'->'registros')->>'normalizado_valor_exercicio_atual')::numeric as valor_bens_direitos,
		jsonb_array_length(coalesce(p1.retorno_irpf,p2.retorno_irpf)->'data'->'dependentes')::int as count_dependentes
	from data_science.irpf_full_sem_duplicados as p1
	full outer join data_science.carga_3_irpf as p2 on p2.url = p1.url) as t2 on t2.url = coalesce(p1.url,p2.url)


select
	t.direct_prospect_id 
from lucas_leal.bizcredit_approval_tape_full t
join public.direct_prospects dp on dp.direct_prospect_id = t.direct_prospect_id
left join lucas_leal.t_bigdata_processos_judiciais_cnpj t1 on t1.cnpj = dp.cnpj
where t1.cnpj is null	
	
insert into t_bizcredit_processos_judiciais_pj
(select
	t1.cnpj,
	t3.TotalLawsuits,
	t3.TotalLawsuitsAsAuthor,
	t3.TotalLawsuitsAsDefendant,
	t3.TotalLawsuitsAsOther,
	t2.id_processo,
	t2.tribunal_processo,
	t2.valor_processo,
	t2.data_publicacao_processo,
	t2.data_notificacao_processo,
	t2.doc_parte,
	t2.nome_parte,
	t2.tipo_parte,
	t1.query_id
from t_bigdata_processos_judiciais_cnpj t1
left join (select 
	query_id,
	(json_array_elements(t1.data_output) ->'Lawsuits'->>'TotalLawsuits')::int as TotalLawsuits,
	(json_array_elements(t1.data_output) ->'Lawsuits'->>'TotalLawsuitsAsAuthor')::int as TotalLawsuitsAsAuthor,
	(json_array_elements(t1.data_output) ->'Lawsuits'->>'TotalLawsuitsAsDefendant')::int as TotalLawsuitsAsDefendant,
	(json_array_elements(t1.data_output) ->'Lawsuits'->>'TotalLawsuitsAsOther')::int as TotalLawsuitsAsOther,
	row_number() over (partition by t1.cnpj order by length(t1.data_output::text) desc) as indice_consulta
	from t_bigdata_processos_judiciais_cnpj t1
	join (select
			distinct t1.cnpj,length(t1.data_output::text)
		from t_bigdata_processos_judiciais_cnpj t1
		left join t_bizcredit_processos_judiciais_pj t2 on t2.cnpj = t1.cnpj
		where t2.cnpj is null --and length(t1.data_output::text) < 5000000
		--limit 10
		) as t2 on t2.cnpj = t1.cnpj) t3 on t3.query_id = t1.query_id
left join(select
		query_id,
		json_array_elements(json_array_elements(data_output) ->'Lawsuits'->'Lawsuits')->>'Number' as id_processo,
		json_array_elements(json_array_elements(data_output) ->'Lawsuits'->'Lawsuits')->>'CourtType' as tribunal_processo,
		(json_array_elements(json_array_elements(data_output) ->'Lawsuits'->'Lawsuits')->>'Value')::numeric as valor_processo,
		(json_array_elements(json_array_elements(data_output) ->'Lawsuits'->'Lawsuits')->>'PublicationDate')::date as data_publicacao_processo,
		(json_array_elements(json_array_elements(data_output) ->'Lawsuits'->'Lawsuits')->>'NoticeDate')::date as data_notificacao_processo,
		json_array_elements(json_array_elements(json_array_elements(data_output) ->'Lawsuits'->'Lawsuits')->'Parties')->>'Doc' as doc_parte, 
		json_array_elements(json_array_elements(json_array_elements(data_output) ->'Lawsuits'->'Lawsuits')->'Parties')->>'Name' as nome_parte,
		json_array_elements(json_array_elements(json_array_elements(data_output) ->'Lawsuits'->'Lawsuits')->'Parties')->>'Type' as tipo_parte
	from t_bigdata_processos_judiciais_cnpj t1
	join (select
			distinct t1.cnpj,length(t1.data_output::text)
		from t_bigdata_processos_judiciais_cnpj t1
		left join t_bizcredit_processos_judiciais_pj t2 on t2.cnpj = t1.cnpj
		where t2.cnpj is null --and length(t1.data_output::text) < 5000000
		--limit 1
		) as t2 on t2.cnpj = t1.cnpj) t2 on t2.query_id = t1.query_id
where t3.indice_consulta = 1)
	

insert into t_bizcredit_processos_judiciais_cpf
(select
	t1.cpf,
	coalesce(t3.TotalLawsuits,0) as TotalLawsuits,
	coalesce(t3.TotalLawsuitsAsAuthor,0) as TotalLawsuitsAsAuthor,
	coalesce(t3.TotalLawsuitsAsDefendant,0) as TotalLawsuitsAsDefendant,
	coalesce(t3.TotalLawsuitsAsOther,0) as TotalLawsuitsAsOther,
	t2.id_processo,
	t2.tribunal_processo,
	t2.valor_processo,
	t2.data_publicacao_processo,
	t2.data_notificacao_processo,
	t2.doc_parte,
	t2.nome_parte,
	t2.tipo_parte,
	t1.query_id
from t_bigdata_processos_judiciais_cpf t1
left join (select 
		query_id,
		(jsonb_array_elements(data_output) ->'Processes'->>'TotalLawsuits')::int as TotalLawsuits,
		(jsonb_array_elements(data_output) ->'Processes'->>'TotalLawsuitsAsAuthor')::int as TotalLawsuitsAsAuthor,
		(jsonb_array_elements(data_output) ->'Processes'->>'TotalLawsuitsAsDefendant')::int as TotalLawsuitsAsDefendant,
		(jsonb_array_elements(data_output) ->'Processes'->>'TotalLawsuitsAsOther')::int as TotalLawsuitsAsOther,
		row_number() over (partition by cpf order by length(data_output::text) desc) as indice_consulta
	from t_bigdata_processos_judiciais_cpf
	where cpf not in (select cpf from t_bizcredit_processos_judiciais_cpf)) t3 on t3.query_id = t1.query_id --and length(data_output::text) < 5000000
left join(select
		query_id,
		jsonb_array_elements(jsonb_array_elements(data_output) ->'Processes'->'Lawsuits')->>'Number' as id_processo,
		jsonb_array_elements(jsonb_array_elements(data_output) ->'Processes'->'Lawsuits')->>'CourtType' as tribunal_processo,
		(jsonb_array_elements(jsonb_array_elements(data_output) ->'Processes'->'Lawsuits')->>'Value')::numeric as valor_processo,
		(jsonb_array_elements(jsonb_array_elements(data_output) ->'Processes'->'Lawsuits')->>'PublicationDate')::date as data_publicacao_processo,
		(jsonb_array_elements(jsonb_array_elements(data_output) ->'Processes'->'Lawsuits')->>'NoticeDate')::date as data_notificacao_processo,
		jsonb_array_elements(jsonb_array_elements(jsonb_array_elements(data_output) ->'Processes'->'Lawsuits')->'Parties')->>'Doc' as doc_parte, 
		jsonb_array_elements(jsonb_array_elements(jsonb_array_elements(data_output) ->'Processes'->'Lawsuits')->'Parties')->>'Name' as nome_parte,
		jsonb_array_elements(jsonb_array_elements(jsonb_array_elements(data_output) ->'Processes'->'Lawsuits')->'Parties')->>'Type' as tipo_parte
	from t_bigdata_processos_judiciais_cpf
	where cpf not in (select cpf from t_bizcredit_processos_judiciais_cpf)) t2 on t2.query_id = t1.query_id --and length(data_output::text) < 5000000
where t3.indice_consulta = 1 and cpf not in (select cpf from t_bizcredit_processos_judiciais_cpf)) --and length(t1.data_output::text) < 5000000


update lucas_leal.t_bizcredit_bizextratos_full
set faturamento_total_15du = vl_ever_faturamento_comprovado_15_du
from t_extrato_tape t1
where t1.id = analysis_id;
insert into lucas_leal.t_bizcredit_bizextratos_full
select
	t1.loan_request_id,
	(t1."data"->>'averageIncome')::numeric as faturamento_medio,
	t1.id as analysis_id,
	coalesce(((t1."data"->'monthlyIndicators'->0->>'workDays')::smallint >= 15)::int * (t1."data"->'monthlyIndicators'->0->>'verifiedRevenue')::numeric,0) +
		coalesce(((t1."data"->'monthlyIndicators'->1->>'workDays')::smallint >= 15)::int * (t1."data"->'monthlyIndicators'->1->>'verifiedRevenue')::numeric,0) +
		coalesce(((t1."data"->'monthlyIndicators'->2->>'workDays')::smallint >= 15)::int * (t1."data"->'monthlyIndicators'->2->>'verifiedRevenue')::numeric,0) +
		coalesce(((t1."data"->'monthlyIndicators'->3->>'workDays')::smallint >= 15)::int * (t1."data"->'monthlyIndicators'->3->>'verifiedRevenue')::numeric,0) +
		coalesce(((t1."data"->'monthlyIndicators'->4->>'workDays')::smallint >= 15)::int * (t1."data"->'monthlyIndicators'->4->>'verifiedRevenue')::numeric,0) as vl_ever_faturamento_comprovado_15_du,
	t3.faturamento_total_15du
from biz_credit.analysis t1
/*
join(select
		t1.loan_request_id,
		t1.id,
		row_number() over (partition by t1.loan_request_id order by t1.id desc) as indice_analise
	from biz_credit.analysis t1
	join public.loan_requests lr on lr.loan_request_id = t1.loan_request_id
	where t1.created_on::date <= coalesce(lr.loan_date,'2100-01-01'::date)
		and created_by not in ('Eduardo Varella','Pedro Castro','Vanessa Medeiros')
		and last_updated_by not in ('Eduardo Varella','Pedro Castro','Vanessa Medeiros')
		and t1.status not in ('IMPORTED','DELETED') 
		and lr.status in ('ACCEPTED','REJECTED','LOST','Expirado','Cancelado','Substituido')
		and (coalesce("data"->'monthlyIndicators'->0->>'totalIncome','0')::numeric + 
			coalesce("data"->'monthlyIndicators'->1->>'totalIncome','0')::numeric + 
			coalesce("data"->'monthlyIndicators'->2->>'totalIncome','0')::numeric + 
			coalesce("data"->'monthlyIndicators'->3->>'totalIncome','0')::numeric + 
			coalesce("data"->'monthlyIndicators'->4->>'totalIncome','0')::numeric) > 0) t2 on t2.id = t1.id and t2.indice_analise = 1
*/
join t_bizcredit_bizextratos_full t3 on t3.analysis_id = t1.id
where t3.faturamento_total_15du is not null
			
	
truncate t_anexo_modelos_approval_tape;
insert into t_anexo_modelos_approval_tape
select
	loan_request_id,
	prob_v20 as m5v20_prob,
	null::int as m5v20_classe,	
	prob_v21 as m5v21_prob,
	faixa_v21 as m5v21_classe,
	prob_v20_so_scores as m5v20_clean_prob,
	faixa_v20_so_score as m5v20_clean_classe,
	prob_v21_sem_ext as m5v21_sem_extrato_prob,
	faixa_v21_sem_ext as m5v21_sem_extrato_classe,
	prob_v21_s_ext_s_jud as m5v21_s_ext_s_jud_pob
from giulia.mv_teste_probs_underwrite

select
	count(*)
from giulia.mv_teste_probs_underwrite t1
join bizcredit_approval_tape_full t2 on t2.loan_request_id = t1.loan_request_id
where t2.underwrite_final_score_1_prob != t1.prob_v21


insert into t_bizcredit_bigdata_occupation_data
select 
	t1.cpf,
	repeat('0',14 - length(t3.cnpj)) || t3.cnpj as cnpj,
	t3.status,
	t1.query_id
from (select distinct repeat('0',11 - length(cpf)) || cpf as cpf from lucas_leal.t_bigdata_occupation_data) t1
left join(select
		repeat('0',11 - length(t1.cpf)) || t1.cpf as cpf,
		json_array_elements(json_array_elements(t1.data_output_new)->'ProfessionData'->'Professions')->>'CompanyIdNumber' as cnpj,
		json_array_elements(json_array_elements(t1.data_output_new)->'ProfessionData'->'Professions')->>'Status' as status	
	from lucas_leal.t_bigdata_occupation_data t1
	join (select 
			repeat('0',11 - length(cpf)) || cpf as cpf,
			max(query_id) as max_query_id
		from lucas_leal.t_bigdata_occupation_data
		group by 1) t2 on t2.max_query_id = t1.query_id) as t3 on t3.cpf = t1.cpf

		
		
truncate t_bizcredit_transactions_corrigida;
alter table t_bizcredit_transactions_corrigida add column imposto_saida_sem_iof int;
insert into t_bizcredit_transactions_corrigida 
select 
	loan_request_id,
	count(*) filter (where tag = 'salario') as salario,
	count(*) filter (where tag = 'salario' and value < 0) as salario_saida,
	count(*) filter (where tag = 'imposto') as imposto,
	count(*) filter (where tag = 'imposto' and value < 0) as imposto_saida,
	count(*) filter (where tag = 'ccf') as ccf,
	count(*) filter (where tag = 'ccf' and value > 0) as ccf_entrada,
	count(*) filter (where tag = 'sacado_governo' and value > 0) as governo_entrada,
	round(sum(value) filter (where tag in ('boletos','tbi_ted','dinheiro','sacado_governo','adquirencia','marketplace') and intervalo = '_30' and movimentacao = 'positivo')::numeric,2) as entradas_operacional1_30,
	round(sum(value) filter (where tag in ('boletos','tbi_ted','dinheiro','sacado_governo','adquirencia','marketplace') and intervalo = '_60' and movimentacao = 'positivo')::numeric,2) as entradas_operacional1_60,
	round(sum(value) filter (where tag in ('boletos','tbi_ted','dinheiro','sacado_governo','adquirencia','marketplace') and intervalo = '_over60' and movimentacao = 'positivo')::numeric,2) as entradas_operacional1_over60,
	count(*) filter (where tag = 'adquirencia' and value > 0) as adquirencia_entrada,			
	count(*) filter (where tag = 'imposto' and value < 0 and tratado not like '%iof%') as imposto_saida_sem_iof
from t_bizcredit_transactions_corrigida_full
group by 1

