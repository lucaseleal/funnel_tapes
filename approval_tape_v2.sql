select count(*) from mv_bizcredit_approval_tape;
select count(*) from mv_bizcredit_parecer_full;
select count(*) from mv_bizcredit_emailage;
select count(*) from mv_bizcredit_ipstack;
select count(*) from mv_bizcredit_signers;
select count(*) from mv_bizcredit_endereco_balcao;
select count(*) from mv_bizcredit_scr_cpf_major;
select count(*) from mv_bizcredit_transactions;
select count(*) from mv_bizcredit_consultas_serasa;
select count(*) from mv_bizcredit_materialidade_neoway;
select count(*) from mv_bizcredit_processos_judiciais_pj;
select count(*) from mv_bizcredit_bigdata_occupation_data2;
select count(*) from mv_bizcredit_scr_pj;
select count(*) from mv_bizcredit_irpf;

refresh materialized view lucas_leal.mv_bizcredit_checklist; 
refresh materialized view lucas_leal.mv_bizcredit_parecer;
refresh materialized view mv_bizcredit_parecer_full;
refresh materialized view mv_bizcredit_emailage;
refresh materialized view mv_bizcredit_ipstack;
refresh materialized view mv_bizcredit_signers;
refresh materialized view mv_bizcredit_endereco_balcao;
refresh materialized view mv_bizcredit_scr_cpf_major;
refresh materialized view mv_bizcredit_transactions;
refresh materialized view mv_bizcredit_consultas_serasa;
refresh materialized view mv_bizcredit_materialidade_neoway;
refresh materialized view mv_bizcredit_processos_judiciais_pj;
refresh materialized view mv_bizcredit_bigdata_occupation_data;
refresh materialized view mv_bizcredit_scr_pj;
refresh materialized view mv_bizcredit_irpf;
refresh materialized view mv_bizcredit_approval_tape;

drop materialized view mv_bizcredit_consultas_serasa

CREATE OR REPLACE VIEW lucas_leal.bizcredit_approval_tape_v2
AS SELECT t1.direct_prospect_id,
    t1.loan_request_id,
    t1.status,
    t1.valor_pedido,
        CASE t2.decisao_final
            WHEN 'Rejeitar'::text THEN 1
            ELSE 0
        END AS target_reject,
    t1.esteira,
    t1.periodo_controle,
    t1.renovacao,
    t1.collection_total_due_installments,
    t1.collection_count_postponing_restructures,
    t1.collection_i_pmt_of_first_postponing,
    t1.collection_current_max_late_pmt1,
    t1.collection_current_max_late_pmt2,
    t1.collection_ever_by_pmt3_soft,
    t1.collection_ever_by_pmt4_aggressive,
    t1.collection_ever_by_pmt5_aggressive,
    t1.collection_ever_by_pmt6_aggressive,
    t1.data_criacao,
    t4.score_emailage > 500 AS new_identidade_email_fraude,
    COALESCE(t5.distancia_ipstack >= 200::numeric, false) AS new_identidade_problema_geolocalizacao,
        CASE t6.is_shareholder
            WHEN 1 THEN 'socio direto'::text
            WHEN 0 THEN 'nao socio'::text
            ELSE NULL::text
        END AS new_identidade_relacao_solicitante,
    COALESCE(t10.consulta_seguradora, 0::bigint) > 0 AS new_consulta_seguradora,
    COALESCE(t10.consulta_fornecedores, 0::bigint) > 0 AS new_consulta_fornecedores,
    dp.employees > 0 AS new_tem_empregados,
    t9.imposto_saida > 0 AS new_financeiro_pagamento_impostos,
    t9.salario_saida > 0 AS new_financeiro_pgto_salarios,
    t12.total_tribut_trab > 0 AS new_processo_tribut_trabal_pj,
    GREATEST(t11.count_imoveis, t11.count_inpi_marcas, t11.count_obras, t11.count_pat, t11.tem_aeronave::bigint, t11.tem_exportacao::bigint, t11.tem_licenca_ambiental::bigint, t11.total_veiculos) > 0 AS new_materialidade_neoway,
    COALESCE((t10.consulta_seguradora > 0)::integer, 0) + COALESCE((t10.consulta_fornecedores > 0)::integer, 0) + COALESCE((dp.employees > 0)::integer, 0) + COALESCE((t9.imposto_saida > 0)::integer, 0) + COALESCE((t9.salario_saida > 0)::integer, 0) + COALESCE((t12.total_tribut_trab > 0)::integer, 0) + COALESCE((GREATEST(t11.count_imoveis, t11.count_inpi_marcas, t11.count_obras, t11.count_pat, t11.tem_aeronave::bigint, t11.tem_exportacao::bigint, t11.tem_licenca_ambiental::bigint, t11.total_veiculos) > 0)::integer, 0) AS new_score_materialidade,
    (t6.signer_idade::double precision - dp.age) < 24::double precision AS new_socio_major_iniciou_menos24,
    dp.month_revenue >= 100000::double precision AND dp.age < 2::double precision OR dp.month_revenue >= 200000::double precision AND dp.age < 3::double precision AS new_financeiro_receita_desproporcional,
        CASE
            WHEN t8.thin_file = 1 THEN 'Sem carteira'::text
            WHEN (t8.long_term_debt_pf_curr * 1000::double precision / dp.month_revenue) >= 0.1::double precision THEN 'Adequado ao porte'::text
            WHEN (t8.long_term_debt_pf_curr * 1000::double precision / dp.month_revenue) < 0.1::double precision THEN 'Inferior ao porte'::text
            ELSE NULL::text
        END AS new_socio_major_carteira_credito,
    t6.signer_renda_mensal AS new_socio_major_renda_mensal,
    (t13.totallawsuitsasdefendant > 0)::integer AS new_processo_judicial_socios,
    t12.reu_total AS new_processo_judicial_reu_pj,
    t13.totallawsuitsasdefendant + t12.reu_total AS new_score_processe_judicial,
    COALESCE(t6.signer_nivel_pep, 'NAO IDENTIFICADO'::text) = ANY (ARRAY['PRIMARIO'::text, 'SECUNDARIO'::text]) AS new_socio_major_pep,
    t14.funcionario_empresa_ativa > 1 AS new_socio_major_funcionario_outra,
    COALESCE(t10.consulta_factoring, 0::bigint) > 0 AS new_financeiro_factoring,
        CASE
            WHEN t1.renovacao > 0 OR t15.count_capital_giro_c_fin_s_veiculo_c_micro >= 6 THEN 'Sim'::text
            WHEN t15.count_capital_giro_c_fin_s_veiculo_c_micro >= 3 THEN 'Parcial'::text
            WHEN t15.count_capital_giro_c_fin_s_veiculo_c_micro < 3 THEN 'Nao'::text
            ELSE NULL::text
        END AS new_financeiro_acostumado_credito,
        CASE
            CASE
                WHEN (COALESCE(t9.entradas_operacional1_30, 0::double precision) / GREATEST(t9.entradas_operacional1_60, 1::double precision) - 1::double precision) > 0.25::double precision THEN 'C'::text
                WHEN (COALESCE(t9.entradas_operacional1_30, 0::double precision) / GREATEST(t9.entradas_operacional1_60, 1::double precision) - 1::double precision) < '-0.25'::numeric::double precision THEN 'D'::text
                WHEN abs(COALESCE(t9.entradas_operacional1_30, 0::double precision) / GREATEST(t9.entradas_operacional1_60, 1::double precision) - 1::double precision) < 0.25::double precision THEN 'E'::text
                ELSE 'O'::text
            END ||
            CASE
                WHEN (COALESCE(t9.entradas_operacional1_60, 0::double precision) / GREATEST(t9.entradas_operacional1_over60, 1::double precision) - 1::double precision) > 0.25::double precision THEN 'C'::text
                WHEN (COALESCE(t9.entradas_operacional1_60, 0::double precision) / GREATEST(t9.entradas_operacional1_over60, 1::double precision) - 1::double precision) < '-0.25'::numeric::double precision THEN 'D'::text
                WHEN abs(COALESCE(t9.entradas_operacional1_60, 0::double precision) / GREATEST(t9.entradas_operacional1_over60, 1::double precision) - 1::double precision) < 0.25::double precision THEN 'E'::text
                ELSE 'O'::text
            END
            WHEN 'CC'::text THEN 'Crescente'::text
            WHEN 'DD'::text THEN 'Decrescente'::text
            WHEN 'EC'::text THEN 'Estavel'::text
            WHEN 'ED'::text THEN 'Estavel'::text
            WHEN 'EO'::text THEN 'Estavel'::text
            WHEN 'CE'::text THEN 'Estavel'::text
            WHEN 'DE'::text THEN 'Estavel'::text
            WHEN 'OE'::text THEN 'Estavel'::text
            WHEN 'EE'::text THEN 'Estavel'::text
            ELSE 'Oscilante'::text
        END AS new_financeiro_comportamento_faturamento,
    t9.ccf_entrada > 0 AS new_financeiro_ccf,
    t9.governo_entrada > 0 AS new_sacado_governo,
    COALESCE(t16.bens_capital_social_irpf_major_cpf, t16.bens_capital_social_irpf, 0::bigint) AS new_socio_major_bens_informados_capital_social,
    COALESCE(t16.bens_imoveis_irpf_major_cpf, t16.bens_imoveis_irpf, 0::bigint) AS new_socio_major_bens_informados_imoveis,
    COALESCE(t16.bens_dinheiro_irpf_major_cpf, t16.bens_dinheiro_irpf, 0::bigint) AS new_socio_major_bens_informados_dinheiro,
    COALESCE(t16.bens_outros_irpf_major_cpf, t16.bens_outros_irpf, 0::bigint) AS new_socio_major_bens_informados_outros,
    COALESCE(t16.bens_investimento_irpf_major_cpf, t16.bens_investimento_irpf, 0::bigint) AS new_socio_major_bens_informados_investimento,
    COALESCE(t16.bens_moveis_irpf_major_cpf, t16.bens_moveis_irpf, 0::bigint) AS new_socio_major_bens_informados_moveis,
    COALESCE(t16.bens_qualquer_irpf_major_cpf, t16.bens_qualquer_irpf, 0::bigint) AS new_socio_major_bens_informados_qualquer,
        CASE
            WHEN COALESCE(t16.valor_irpf_major_cpf, t16.valor_irpf, 0::numeric) > 1000000::numeric THEN '> 1MM'::text
            WHEN COALESCE(t16.valor_irpf_major_cpf, t16.valor_irpf, 0::numeric) > 500000::numeric THEN '> 500k'::text
            WHEN COALESCE(t16.valor_irpf_major_cpf, t16.valor_irpf, 0::numeric) > 100000::numeric THEN '> 100k'::text
            WHEN COALESCE(t16.valor_irpf_major_cpf, t16.valor_irpf, 0::numeric) > 50000::numeric THEN '> 50k'::text
            WHEN COALESCE(t16.valor_irpf_major_cpf, t16.valor_irpf, 0::numeric) > 0::numeric THEN '> 0'::text
            WHEN COALESCE(t16.valor_irpf_major_cpf, t16.valor_irpf, 0::numeric) <= 0::numeric THEN '<= 0'::text
            ELSE NULL::text
        END AS new_socio_major_valor_ativos,
    COALESCE(t16.dependentes_irpf_major_cpf, t16.dependentes_irpf, 0) > 0 AS new_socio_major_dependentes,
    t17.household_activity AS new_complexidade_cnpj_mesmo_endereco,
    t17.household_owners AS new_complexidade_empresas_endereco_socio_major,
        CASE COALESCE(dp.phantasy_name, ''::character varying)
            WHEN ''::text THEN 2
            ELSE t17.tradename_city
        END AS new_complexidade_cnpj_mesmo_nome,
    (t18.totalcompanies > 1)::integer AS new_complexidade_socios_relacionados_mesmo_endereco,
    t19.levenshtein_distancia AS new_materialidade_endereco_distintos,
    t1.identidade_relacao_solicitante,
    t1.identidade_problema_geolocalizacao,
    t1.identidade_email_fraude,
    t1.materialidade_endereco_distintos,
    t1.materialidade_tipo_negocio,
    t1.materialidade_tipo_sacado,
    t1.materialidade_materialidade_fisica,
    t1.materialidade_materialidade_online,
    t1.materialidade_materialidade_comercial,
    t1.materialidade_materialidade_duvidosa,
    t1.complexidade_cnpj_mesmo_endereco,
    t1.complexidade_cnpj_mesmo_nome,
    t1.complexidade_pessoa_oculta,
    t1.financeiro_bad_behavior,
    t1.financeiro_factoring,
    t1.financeiro_receita_desproporcional,
    t1.financeiro_faturamento_errado,
    t1.financeiro_faturamento_comprovado,
    t1.financeiro_acostumado_credito,
    t1.financeiro_comportamento_faturamento,
    t1.financeiro_maior_parte_conta_pj,
    t1.financeiro_somente_despesas_pj,
    t1.financeiro_pagamento_impostos,
    t1.financeiro_concentracao_sacado,
    t1.financeiro_ccf,
    t1.financeiro_pgto_salarios,
    t1.socio_major_funcionario_outra,
    t1.socio_major_iniciou_menos24,
    t1.socio_major_renda_mensal,
    t1.socio_major_problemas_juridicos,
    t1.socio_major_pep,
    t1.socio_major_carteira_credito,
    t1.socio_major_bens_informados,
    t1.socio_major_valor_ativos,
    t1.socio_major_dependentes,
    (COALESCE(t9.entradas_operacional1_30, 0::double precision) + COALESCE(t9.entradas_operacional1_60, 0::double precision) + COALESCE(t9.entradas_operacional1_over60, 0::double precision)) / 3::double precision AS new_financeiro_faturamento_comprovado,
    COALESCE(((t6.signer_idade::double precision - dp.age) < 24::double precision)::integer, 0) + COALESCE((dp.month_revenue >= 100000::double precision AND dp.age < 2::double precision OR dp.month_revenue >= 200000::double precision AND dp.age < 3::double precision)::integer, 0) + COALESCE((t8.thin_file = 1)::integer, 0) + COALESCE((t6.is_shareholder = 0)::integer, 0) + COALESCE(t17.household_owners, 0) AS new_score_complexidade,
    COALESCE(t10.consulta_cobranca, 0::bigint) > 0 AS new_consulta_cobranca,
    COALESCE(t10.consulta_factoring, 0::bigint) > 0 AS new_consulta_factoring,
    COALESCE(t10.consulta_indefinido, 0::bigint) > 0 AS new_consulta_indefinido,
    COALESCE(t10.consulta_proprio, 0::bigint) > 0 AS new_consulta_proprio,
    COALESCE(t10.consulta_provedor_dado, 0::bigint) > 0 AS new_consulta_provedor_dado,
        CASE
            WHEN "left"(t20.vinculo::text, 5) = 'Sócio'::text OR t20.vinculo::text = 'Presidente'::text THEN 'Socio'::text
            WHEN (t20.vinculo::text = ANY (ARRAY['Administrador'::character varying::text, 'Procurador'::character varying::text])) OR t20.vinculo::text = 'Outros'::text AND t20.lead_marketing_channel_group::text <> 'agent'::text THEN 'Admin/procurador'::text
            ELSE 'Outros'::text
        END AS new_solicitante_vinculo_admin,
    GREATEST(t10.consulta_financeira, t10.consulta_cobranca, 0::bigint) > 0 AND GREATEST(t10.consulta_factoring, t10.consulta_fornecedores, t10.consulta_indefinido, t10.consulta_provedor_dado, t10.consulta_seguradora, 0::bigint) = 0 AS new_apenas_consulta_financeira,
    t20.lead_marketing_channel_group,
    t12.total_trabalhista,
    t12.total_tributaria,
    t12.total_criminal,
    t12.total_civel_admin,
    COALESCE(t10.consulta_financeira, 0::bigint) > 0 AS new_consulta_financeira
   FROM lucas_leal.mv_bizcredit_approval_tape t1
     JOIN direct_prospects dp ON dp.direct_prospect_id = t1.direct_prospect_id
     JOIN ( SELECT mv_bizcredit_parecer_full.emprestimo_id,
            COALESCE(mv_bizcredit_parecer_full.credit_underwrite_a3_status, mv_bizcredit_parecer_full.credit_underwrite_a2_status, mv_bizcredit_parecer_full.credit_underwrite_a1_status) AS decisao_final
           FROM lucas_leal.mv_bizcredit_parecer_full
          WHERE COALESCE(mv_bizcredit_parecer_full.credit_underwrite_a3_status, mv_bizcredit_parecer_full.credit_underwrite_a2_status, mv_bizcredit_parecer_full.credit_underwrite_a1_status) IS NOT NULL) t2 ON t2.emprestimo_id = t1.loan_request_id
     LEFT JOIN lucas_leal.loan_tape_full lt ON lt.lead_id = t1.direct_prospect_id
     LEFT JOIN lucas_leal.mv_bizcredit_emailage t4 ON t4.lead_id = t1.direct_prospect_id
     LEFT JOIN lucas_leal.mv_bizcredit_ipstack t5 ON t5.lead_id = t1.direct_prospect_id
     LEFT JOIN lucas_leal.mv_bizcredit_signers t6 ON t6.lead_id = t1.direct_prospect_id
     LEFT JOIN lucas_leal.mv_bizcredit_endereco_balcao t7 ON t7.direct_prospect_id = t1.direct_prospect_id
     LEFT JOIN lucas_leal.mv_bizcredit_scr_cpf_major t8 ON t8.lead_id = t1.direct_prospect_id
     LEFT JOIN lucas_leal.mv_bizcredit_transactions t9 ON t9.loan_request_id = t1.loan_request_id
     LEFT JOIN lucas_leal.mv_bizcredit_consultas_serasa t10 ON t10.direct_prospect_id = t1.direct_prospect_id
     LEFT JOIN lucas_leal.mv_bizcredit_materialidade_neoway t11 ON t11.lead_id = t1.direct_prospect_id
     LEFT JOIN ( SELECT t2_1.direct_prospect_id,
            count(DISTINCT t1_1.id_processo) FILTER (WHERE t1_1.tipo_parte = ANY (ARRAY['DEFENDANT'::text, 'CLAIMED'::text])) AS reu_total,
            count(DISTINCT t1_1.id_processo) FILTER (WHERE t1_1.tribunal_processo = ANY (ARRAY['TRABALHISTA'::text, 'TRIBUTARIA'::text, 'FAZENDA'::text, 'PREVIDENCIARIA'::text])) AS total_tribut_trab,
            count(DISTINCT t1_1.id_processo) FILTER (WHERE t1_1.tribunal_processo = 'TRABALHISTA'::text) AS total_trabalhista,
            count(DISTINCT t1_1.id_processo) FILTER (WHERE t1_1.tribunal_processo = ANY (ARRAY['TRIBUTARIA'::text, 'FAZENDA'::text, 'PREVIDENCIARIA'::text])) AS total_tributaria,
            count(DISTINCT t1_1.id_processo) FILTER (WHERE t1_1.tribunal_processo ~~ '%CRIMINAL%'::text) AS total_criminal,
            count(DISTINCT t1_1.id_processo) FILTER (WHERE t1_1.tribunal_processo = ANY (ARRAY['ADMINISTRATIVA'::text, 'CIVEL'::text, 'ESPECIAL CIVEL'::text])) AS total_civel_admin
           FROM lucas_leal.mv_bizcredit_approval_tape t2_1
             JOIN direct_prospects dp_1 ON t2_1.direct_prospect_id = dp_1.direct_prospect_id
             LEFT JOIN lucas_leal.mv_bizcredit_processos_judiciais_pj t1_1 ON t1_1.cnpj = dp_1.cnpj::text AND t1_1.cnpj = t1_1.doc_parte AND t1_1.data_notificacao_processo <= t2_1.data_criacao
          GROUP BY t2_1.direct_prospect_id) t12 ON t12.direct_prospect_id = t1.direct_prospect_id
     LEFT JOIN ( SELECT t_bigdata_processos_judiciais_cpf.cpf,
            ((jsonb_array_elements(t_bigdata_processos_judiciais_cpf.data_output) -> 'Processes'::text) ->> 'TotalLawsuitsAsDefendant'::text)::integer AS totallawsuitsasdefendant,
            row_number() OVER (PARTITION BY t_bigdata_processos_judiciais_cpf.cpf ORDER BY (length(t_bigdata_processos_judiciais_cpf.data_output::text)) DESC) AS indice_consulta
           FROM lucas_leal.t_bigdata_processos_judiciais_cpf) t13 ON t13.cpf = t6.major_signer_cpf AND t13.indice_consulta = 1
     LEFT JOIN ( SELECT t1_1.cpf,
            count(DISTINCT t1_1.cnpj) FILTER (WHERE t1_1.status = 'ACTIVE'::text AND t1_1.cnpj <> dp_1.cnpj::text) AS funcionario_empresa_ativa
           FROM lucas_leal.mv_bizcredit_bigdata_occupation_data t1_1
             JOIN lucas_leal.mv_bizcredit_signers t2_1 ON t2_1.major_signer_cpf = t1_1.cpf
             JOIN lucas_leal.mv_bizcredit_approval_tape t3 ON t3.loan_request_id = t2_1.loan_request_id
             JOIN direct_prospects dp_1 ON dp_1.direct_prospect_id = t3.direct_prospect_id
          GROUP BY t1_1.cpf) t14 ON t14.cpf = t6.major_signer_cpf
     LEFT JOIN lucas_leal.mv_bizcredit_scr_pj t15 ON t15.lead_id = t1.direct_prospect_id
     LEFT JOIN ( SELECT t1_1.direct_prospect_id,
            count(*) FILTER (WHERE t4_1.major_signer_cpf IS NOT NULL AND t3.grupo_tratado = 'capital social'::text) AS bens_capital_social_irpf_major_cpf,
            count(*) FILTER (WHERE t4_1.major_signer_cpf IS NOT NULL AND t3.grupo_tratado = 'bens imóveis'::text) AS bens_imoveis_irpf_major_cpf,
            count(*) FILTER (WHERE t4_1.major_signer_cpf IS NOT NULL AND t3.grupo_tratado = 'dinheiro em posse'::text) AS bens_dinheiro_irpf_major_cpf,
            count(*) FILTER (WHERE t4_1.major_signer_cpf IS NOT NULL AND t3.grupo_tratado = 'outros'::text) AS bens_outros_irpf_major_cpf,
            count(*) FILTER (WHERE t4_1.major_signer_cpf IS NOT NULL AND t3.grupo_tratado = 'investimento'::text) AS bens_investimento_irpf_major_cpf,
            count(*) FILTER (WHERE t4_1.major_signer_cpf IS NOT NULL AND t3.grupo_tratado = 'bens móveis'::text) AS bens_moveis_irpf_major_cpf,
            count(*) FILTER (WHERE t4_1.major_signer_cpf IS NOT NULL AND t3.grupo_tratado IS NOT NULL) AS bens_qualquer_irpf_major_cpf,
            sum(t1_1.valor_bens_direitos) FILTER (WHERE t4_1.major_signer_cpf IS NOT NULL) AS valor_irpf_major_cpf,
            max(t1_1.count_dependentes) FILTER (WHERE t4_1.major_signer_cpf IS NOT NULL) AS dependentes_irpf_major_cpf,
            count(*) FILTER (WHERE t3.grupo_tratado = 'capital social'::text) AS bens_capital_social_irpf,
            count(*) FILTER (WHERE t3.grupo_tratado = 'bens imóveis'::text) AS bens_imoveis_irpf,
            count(*) FILTER (WHERE t3.grupo_tratado = 'dinheiro em posse'::text) AS bens_dinheiro_irpf,
            count(*) FILTER (WHERE t3.grupo_tratado = 'outros'::text) AS bens_outros_irpf,
            count(*) FILTER (WHERE t3.grupo_tratado = 'investimento'::text) AS bens_investimento_irpf,
            count(*) FILTER (WHERE t3.grupo_tratado = 'bens móveis'::text) AS bens_moveis_irpf,
            count(*) FILTER (WHERE t3.grupo_tratado IS NOT NULL) AS bens_qualquer_irpf,
            sum(t1_1.valor_bens_direitos) AS valor_irpf,
            max(t1_1.count_dependentes) AS dependentes_irpf
           FROM lucas_leal.mv_bizcredit_irpf t1_1
             JOIN lucas_leal.mv_bizcredit_approval_tape t2_1 ON t2_1.direct_prospect_id = t1_1.direct_prospect_id
             LEFT JOIN lucas_leal.t_codigos_irpf t3 ON t3.codigo = t1_1.codigo_bens_direitos
             LEFT JOIN lucas_leal.mv_bizcredit_signers t4_1 ON t4_1.loan_request_id = t2_1.loan_request_id AND t4_1.major_signer_cpf = t1_1.cpf
          GROUP BY t1_1.direct_prospect_id) t16 ON t16.direct_prospect_id = t1.direct_prospect_id
     LEFT JOIN data_science.carga_3_rf t17 ON t17.direct_prospect_id = t1.direct_prospect_id
     LEFT JOIN ( SELECT t_bigdata_company_group_household_owners_surname.cnpj,
            (((jsonb_array_elements(t_bigdata_company_group_household_owners_surname.data_output::jsonb) -> 'CompanyGroups'::text) -> 0) ->> 'TotalCompanies'::text)::integer AS totalcompanies,
            row_number() OVER (PARTITION BY t_bigdata_company_group_household_owners_surname.cnpj ORDER BY ((((jsonb_array_elements(t_bigdata_company_group_household_owners_surname.data_output::jsonb) -> 'CompanyGroups'::text) -> 0) ->> 'TotalCompanies'::text)::integer) DESC) AS indice
           FROM lucas_leal.t_bigdata_company_group_household_owners_surname) t18 ON t18.cnpj = dp.cnpj::text AND t18.indice = 1
     LEFT JOIN data_science.compara_end_socio_balcao t19 ON t19.direct_prospect_id = t1.direct_prospect_id
     LEFT JOIN ( SELECT dp_1.direct_prospect_id,
            dp_1.vinculo,
            COALESCE(
                CASE upper(lucas_leal.treat_source_name(lucas_leal.translate_char_encode_uft8(dp_1.utm_source::text)))
                    WHEN 'ADWORDS'::text THEN 'adwords'::character varying
                    WHEN 'AGENT'::text THEN 'agent'::character varying
                    WHEN 'BIDU'::text THEN 'bidu'::character varying
                    WHEN 'BING'::text THEN 'Bing'::character varying
                    WHEN 'CREDITA'::text THEN 'credita'::character varying
                    WHEN 'FACEBOOK'::text THEN 'facebook'::character varying
                    WHEN 'FACEBOOK_ORG'::text THEN 'facebook'::character varying
                    WHEN 'FINANZERO'::text THEN 'finanzero'::character varying
                    WHEN 'FINPASS'::text THEN 'finpass'::character varying
                    WHEN 'GERU'::text THEN 'geru'::character varying
                    WHEN 'INSTAGRAM'::text THEN 'instagram'::character varying
                    WHEN 'JUROSBAIXOS'::text THEN 'jurosbaixos'::character varying
                    WHEN 'KONKERO'::text THEN 'konkero'::character varying
                    WHEN 'LINKEDIN'::text THEN 'linkedin'::character varying
                    WHEN 'MGM'::text THEN 'member-get-member'::character varying
                    WHEN 'ORGANICO'::text THEN 'organico'::character varying
                    WHEN 'BLOG'::text THEN 'organico'::character varying
                    WHEN 'SITE'::text THEN 'organico'::character varying
                    WHEN 'DIRECT_CHANNEL'::text THEN 'organico'::character varying
                    WHEN 'ANDROID'::text THEN 'organico'::character varying
                    WHEN 'RENEWAL'::text THEN 'organico'::character varying
                    WHEN 'EMAIL'::text THEN 'organico'::character varying
                    WHEN 'RD'::text THEN 'organico'::character varying
                    WHEN 'RD#/'::text THEN 'organico'::character varying
                    WHEN 'RDSTATION'::text THEN 'organico'::character varying
                    WHEN 'RD+STATION'::text THEN 'organico'::character varying
                    WHEN 'AMERICANAS'::text THEN 'other'::character varying
                    WHEN 'IFOOD'::text THEN 'other'::character varying
                    WHEN 'PEIXE-URBANO'::text THEN 'other'::character varying
                    WHEN 'OUTBRAIN'::text THEN 'outbrain'::character varying
                    WHEN 'TABOOLA'::text THEN 'taboola'::character varying
                    WHEN 'CHARLES/'::text THEN 'agent'::character varying
                    WHEN 'MARCELOROMERA'::text THEN 'agent'::character varying
                    WHEN 'PBCONSULTORIA'::text THEN 'agent'::character varying
                    WHEN 'HMSEGUROS'::text THEN 'agent'::character varying
                    WHEN 'COMPARAONLINE'::text THEN 'agent'::character varying
                    WHEN 'CREDEXPRESS#/'::text THEN 'agent'::character varying
                    ELSE
                    CASE
                        WHEN pp.identificacao IS NOT NULL THEN 'agent'::character varying
                        ELSE dp_1.utm_source
                    END
                END, 'other'::character varying) AS lead_marketing_channel_group
           FROM direct_prospects dp_1
             JOIN lucas_leal.mv_bizcredit_approval_tape t ON t.direct_prospect_id = dp_1.direct_prospect_id
             LEFT JOIN ( SELECT DISTINCT parceiros_parceiro.identificacao
                   FROM parceiros_parceiro) pp ON pp.identificacao::text = replace(dp_1.utm_source::text, '#/'::text, ''::text)) t20 ON t20.direct_prospect_id = t1.direct_prospect_id;


	
--TABELA REAL
create materialized view mv_bizcredit_approval_tape as
select
	dp.direct_prospect_id,
	lr.loan_request_id,
	lr.status,
	lr.value as valor_pedido,
	case tp.decisao_final when 'REPROVE' then 1 else 0 end as target_reject,
	case when dp.utm_source = 'sebrae' then 'sebrae' when dp.utm_source = 'estimulo2020' then 'estimulo2020' else 'biz' end as esteira,
	case when tc.data_criacao < '2020-03-15' then 'Pre covid' when tc.data_criacao between '2020-03-15' and '2020-08-01' then 'Covid' else 'Pos covid' end as periodo_controle,
	dp.previous_loans_count as renovacao,
	lt.collection_total_due_installments,
	lt.collection_count_postponing_restructures,
	lt.collection_i_pmt_of_first_postponing,
	lt.collection_current_max_late_pmt1,
	lt.collection_current_max_late_pmt2,
	lt.collection_ever_by_pmt3_soft,
	lt.collection_ever_by_pmt4_aggressive,
	lt.collection_ever_by_pmt5_aggressive,
	lt.collection_ever_by_pmt6_aggressive,
	tc.data_criacao,
	tc.identidade_relacao_solicitante,
	tc.identidade_problema_geolocalizacao,
	tc.identidade_email_fraude,
	tc.materialidade_endereco_distintos,
	tc.materialidade_tipo_negocio,
	tc.materialidade_tipo_sacado,
	tc.materialidade_materialidade_fisica,
	tc.materialidade_materialidade_online,
	tc.materialidade_materialidade_comercial,
	tc.materialidade_materialidade_duvidosa,
	tc.complexidade_cnpj_mesmo_endereco,
	tc.complexidade_cnpj_mesmo_nome,
	tc.complexidade_pessoa_oculta,
	tc.financeiro_bad_behavior,
	tc.financeiro_factoring,
	tc.financeiro_receita_desproporcional,
	tc.financeiro_faturamento_errado,
	tc.financeiro_faturamento_comprovado::numeric,
	tc.financeiro_acostumado_credito,
	tc.financeiro_comportamento_faturamento,
	tc.financeiro_maior_parte_conta_pj,
	tc.financeiro_somente_despesas_pj,
	tc.financeiro_pagamento_impostos,
	tc.financeiro_concentracao_sacado,
	tc.financeiro_ccf,
	tc.financeiro_pgto_salarios,
	tc.socio_major_funcionario_outra,
	tc.socio_major_iniciou_menos24,
	tc.socio_major_renda_mensal::numeric,
	tc.socio_major_problemas_juridicos,
	tc.socio_major_pep,
	tc.socio_major_carteira_credito,
	tc.socio_major_bens_informados,
	tc.socio_major_valor_ativos,
	tc.socio_major_dependentes
---DADOS DO FUNIL
from public.direct_prospects dp
join(select
		o.direct_prospect_id,
		max(o.offer_id) filter (where lr.status = 'ACCEPTED') as ultima_oferta_aprovada,
		max(o.offer_id) filter (where lr.offer_id is not null and lr.status != 'Substituido') as ultima_oferta_com_pedido_nao_substituido,
		max(o.offer_id) filter (where lr.offer_id is not null) as ultima_oferta_com_pedido,
		max(o.offer_id) as ultima_oferta
	from public.offers o
	left join public.loan_requests lr on lr.offer_id = o.offer_id
	group by 1) as filtro_oferta on filtro_oferta.direct_prospect_id = dp.direct_prospect_id
join public.offers o on o.offer_id = coalesce(ultima_oferta_aprovada,ultima_oferta_com_pedido_nao_substituido,ultima_oferta_com_pedido,ultima_oferta)
join(select
		offer_id,
		max(loan_request_id) filter (where status = 'ACCEPTED') as ultimo_pedido_aprovado,
		max(loan_request_id) filter (where status != 'Substituido') as ultimo_pedido_nao_substituido,
		max(loan_request_id) as ultimo_pedido
	from public.loan_requests
	group by 1) as filtro_pedido on filtro_pedido.offer_id = coalesce(ultima_oferta_aprovada,ultima_oferta_com_pedido_nao_substituido,ultima_oferta_com_pedido,ultima_oferta)
join public.loan_requests lr on lr.loan_request_id = coalesce(ultimo_pedido_aprovado,ultimo_pedido_nao_substituido,ultimo_pedido)
join lucas_leal.mv_bizcredit_checklist tc on tc.lead_id = dp.direct_prospect_id
join lucas_leal.mv_bizcredit_parecer tp on tp.lead_id = tc.lead_id
left join lucas_leal.loan_tape_full lt on lt.lead_id = dp.direct_prospect_id


--CHECKLIST
create materialized view mv_bizcredit_checklist as 
select
	lead_id,
	max(data_criacao) as data_criacao,
	string_agg(resposta,',') filter (where pergunta = 'Qual relaÃ§Ã£o real do solicitante com o negÃ³cio?') as identidade_relacao_solicitante,
	string_agg(resposta,',') filter (where pergunta = 'Algum problema na geolocalizaÃ§Ã£o?') as identidade_problema_geolocalizacao,
	string_agg(resposta,',') filter (where pergunta = 'E-mail apontou risco de fraude?') as identidade_email_fraude,
	string_agg(resposta,',') filter (where pergunta = 'Possui endereÃ§o comercial e residencial distintos?') as materialidade_endereco_distintos,
	string_agg(resposta,',') filter (where pergunta = 'Tipo de negÃ³cio?') as materialidade_tipo_negocio,
	string_agg(resposta,',') filter (where pergunta = 'Tipo de sacado?') as materialidade_tipo_sacado,
	string_agg(resposta,',') filter (where pergunta = 'Existe materialidade fÃ­sica?') as materialidade_materialidade_fisica,
	string_agg(resposta,',') filter (where pergunta = 'Existe materialidade online?') as materialidade_materialidade_online,
	string_agg(resposta,',') filter (where pergunta = 'Existe materialidade comercial?') as materialidade_materialidade_comercial,
	string_agg(resposta,',') filter (where pergunta = 'A materialidade Ã© duvidosa?') as materialidade_materialidade_duvidosa,
	string_agg(resposta,',') filter (where pergunta = 'Existem CNPJs da mesma atividade no endereÃ§o?') as complexidade_cnpj_mesmo_endereco,
	string_agg(resposta,',') filter (where pergunta = 'Existem CNPJs com mesmo nome fantasia e cidade?') as complexidade_cnpj_mesmo_nome,
	string_agg(resposta,',') filter (where pergunta = 'Pessoa fÃ­sica ou jurÃ­dica oculta?') as complexidade_pessoa_oculta,
	string_agg(resposta,',') filter (where pergunta = 'Mau pagamento Serasa/SCR (PJ/PF)?') as financeiro_bad_behavior,
	string_agg(resposta,',') filter (where pergunta = 'Consultado por factorings na Serasa?') as financeiro_factoring,
	string_agg(resposta,',') filter (where pergunta = 'Receita desproporcional ao tempo de business?') as financeiro_receita_desproporcional,
	string_agg(resposta,',') filter (where pergunta = 'Cliente informou o faturamento errado?') as financeiro_faturamento_errado,
	string_agg(resposta,',') filter (where pergunta = 'Qual o faturamento comprovado?') as financeiro_faturamento_comprovado,
	string_agg(resposta,',') filter (where pergunta = 'Empresa acostumada com crÃ©dito?') as financeiro_acostumado_credito,
	string_agg(resposta,',') filter (where pergunta = 'Qual o comportamento do faturamento?') as financeiro_comportamento_faturamento,
	string_agg(resposta,',') filter (where pergunta = 'Maior parte do faturamento conta na PJ?') as financeiro_maior_parte_conta_pj,
	string_agg(resposta,',') filter (where pergunta = 'Extrato - Somente despesas da empresa?') as financeiro_somente_despesas_pj,
	string_agg(resposta,',') filter (where pergunta = 'Extrato - Pagamento de impostos?') as financeiro_pagamento_impostos,
	string_agg(resposta,',') filter (where pergunta = 'Extrato - ConcentraÃ§Ã£o sacado?') as financeiro_concentracao_sacado,
	string_agg(resposta,',') filter (where pergunta = 'Extrato - EmissÃ£o de cheque sem fundos?') as financeiro_ccf,
	string_agg(resposta,',') filter (where pergunta = 'Extrato - Pagamento de salÃ¡rios?') as financeiro_pgto_salarios,
	string_agg(resposta,',') filter (where pergunta = 'Ã‰ funcionÃ¡rio de outra empresa?') as socio_major_funcionario_outra,
	string_agg(resposta,',') filter (where pergunta = 'Iniciou o negÃ³cio com menos de 24 anos?') as socio_major_iniciou_menos24,
	string_agg(resposta,',') filter (where pergunta = 'Qual Ã© a renda mensal?') as socio_major_renda_mensal,
	string_agg(resposta,',') filter (where pergunta = 'Possui problemas jurÃ­dicos?') as socio_major_problemas_juridicos,
	string_agg(resposta,',') filter (where pergunta = 'Pessoa politicamente exposta?') as socio_major_pep,
	string_agg(resposta,',') filter (where pergunta = 'Possui carteira de crÃ©dito?') as socio_major_carteira_credito,
	string_agg(resposta,',') filter (where pergunta = 'Bens informados no Imposto de Renda?') as socio_major_bens_informados,
	string_agg(resposta,',') filter (where pergunta = 'Valor dos ativos fÃ­sicos?') as socio_major_valor_ativos,
	string_agg(resposta,',') filter (where pergunta = 'Dependentes no Imposto de Renda?') as socio_major_dependentes
from(select
		ca.legacy_target_id as lead_id,
		(ca.data_output ->> 'CreationDate')::date as data_criacao, 
		checklist ->> 'QuestionnaireType' as Parte,
		checklist ->> 'Asking' as Pergunta,
		checklist ->> 'Order' as Ordem,
		case when answer->>'TypeOfAnswer' = 'DISCURSIVE' then answer ->> 'DiscursiveReply' else answer ->> 'AnswerText' end as Resposta,
		answer->>'TypeOfAnswer' as tipoResposta
	from
		(select 
			legacy_target_id,
			data_output,
			row_number() over (partition by legacy_target_id order by id desc) as indice_analise
		from sync_credit_analysis) ca,
		public.direct_prospects dp,
		jsonb_array_elements(data_output ->'CreditChecklist') checklist,
		jsonb_array_elements(checklist -> 'Answers') answer
	where 
		ca.legacy_target_id = dp.direct_prospect_id and ca.indice_analise = 1 and (answer->>'BooleanReply' = 'true' or (answer->>'DiscursiveReply' is not null and answer->>'DiscursiveReply' <> ''))) as t1
group by 1


--PARECER
create materialized view mv_bizcredit_parecer as 
select
	lead_id,
	max(confianca) filter (where alcada = 'FIRST_AUTHORITY') as alcada1_confianca,
	max(decisao) filter (where alcada = 'FIRST_AUTHORITY') as alcada1_decisao,
	max(confianca) filter (where alcada = 'SECOND_AUTHORITY') as alcada2_confianca,
	max(decisao) filter (where alcada = 'SECOND_AUTHORITY') as alcada2_decisao,
	max(confianca) filter (where alcada = 'THIRD_AUTHORITY') as alcada3_confianca,
	max(decisao) filter (where alcada = 'THIRD_AUTHORITY') as alcada3_decisao,
	coalesce(max(decisao) filter (where alcada = 'THIRD_AUTHORITY'),max(decisao) filter (where alcada = 'SECOND_AUTHORITY'),max(decisao) filter (where alcada = 'FIRST_AUTHORITY')) as decisao_final
from(select
		ca.legacy_target_id as lead_id,
		opinions->>'AuthorityLevel' as alcada,
		opinions->>'TrustLevel' as confianca,
		opinions->>'Username' as username,
		opinions->>'CreditOpinionSuggestionType' as decisao
	from
		(select 
			legacy_target_id,
			data_output,
			row_number() over (partition by legacy_target_id order by id desc) as indice_analise
		from sync_credit_analysis) ca,
		jsonb_array_elements(data_output ->'CreditOpinions') opinions
	where indice_analise = 1) as t1
group by 1

create materialized view mv_bizcredit_endereco_balcao as 
select
	t.direct_prospect_id,
	a.zip_code,
	a.street
from(select 
		row_number() over (partition by a.client_id order by a.address_id) as indice_endereco,
		a.*
	from public.client_addresses a) as a
join(select
		dp.client_id,
		dp.direct_prospect_id,
		row_number() over (partition by dp.client_id order by lr.loan_request_id) as indice_pedido
	from public.direct_prospects dp 
	join(select
			o.direct_prospect_id,
			max(o.offer_id) filter (where lr.status = 'ACCEPTED') as ultima_oferta_aprovada,
			max(o.offer_id) filter (where lr.offer_id is not null and lr.status != 'Substituido') as ultima_oferta_com_pedido_nao_substituido,
			max(o.offer_id) filter (where lr.offer_id is not null) as ultima_oferta_com_pedido,
			max(o.offer_id) as ultima_oferta,
			count(*) filter (where o.status = 'Substituido') as ofertas_substituidas,
			count(*) filter (where lr.status = 'Substituido') as pedidos_substituidos
		from public.offers o
		left join public.loan_requests lr on lr.offer_id = o.offer_id
		group by 1) as filtro_oferta on filtro_oferta.direct_prospect_id = dp.direct_prospect_id
	join public.offers o on o.offer_id = coalesce(ultima_oferta_aprovada,ultima_oferta_com_pedido_nao_substituido,ultima_oferta_com_pedido,ultima_oferta)
	join(select
			offer_id,
			max(loan_request_id) filter (where status = 'ACCEPTED') as ultimo_pedido_aprovado,
			max(loan_request_id) filter (where status != 'Substituido') as ultimo_pedido_nao_substituido,
			max(loan_request_id) as ultimo_pedido
		from public.loan_requests
		group by 1) as filtro_pedido on filtro_pedido.offer_id = coalesce(ultima_oferta_aprovada,ultima_oferta_com_pedido_nao_substituido,ultima_oferta_com_pedido,ultima_oferta)
	join public.loan_requests lr on lr.loan_request_id = coalesce(ultimo_pedido_aprovado,ultimo_pedido_nao_substituido,ultimo_pedido)
	join mv_bizcredit_checklist t on t.lead_id = dp.direct_prospect_id) as t on t.client_id = a.client_id and t.indice_pedido = a.indice_endereco

	

create materialized view mv_bizcredit_signers as
select
	loan_request_id,
	max(lead_id) as lead_id,
	count(*) filter (where signer_cpf = cpf_solicitante and participacao > 0) as is_shareholder,
	count(*) filter (where signer_cpf = cpf_solicitante and participacao > 0 and name_of_responsible = signer_name) as is_shareholder_mesmo_nome,
	max(revenue) filter (where indice_socio = 1) as signer_renda_mensal,
	max(signer_age) filter (where indice_socio = 1) as signer_idade,
	max(signer_date_of_birth) filter (where indice_socio = 1) as signer_data_nascimento,
	max(nivel_pep) filter (where indice_socio = 1) as signer_nivel_pep,
	max(signer_street_address) filter (where indice_socio = 1) as signer_street_address,
	max(signer_city) filter (where indice_socio = 1) as signer_city,
	max(signer_zip_code) filter (where indice_socio = 1) as signer_zip_code,
	max(administrator) filter (where indice_socio = 1) as administrator,
	max(qualificacao) filter (where indice_socio = 1) as qualificacao,
	max(signer_cpf) filter (where indice_socio = 1) as major_signer_cpf,
	string_agg(signer_cpf::text,',') as list_signer_cpf,
	string_agg(signer_age::text,',') as list_signer_idade,	
	max(number) filter (where indice_socio = 1) as signer_street_number_address,
	max(complement) filter (where indice_socio = 1) as signer_street_complement_address
from(select
		lr.lead_id,
		lr.loan_request_id,
		lr.cpf as cpf_solicitante,
		upper(case when position(' ' in trim(lr.name_of_responsible)) > 0 then left(trim(lr.name_of_responsible),position(' ' in trim(lr.name_of_responsible)) - 1) when trim(lr.name_of_responsible) = '' then null else trim(lr.name_of_responsible) end) as name_of_responsible,
		coalesce(upper(case when position(' ' in trim(s."name")) > 0 then left(trim(s."name"),position(' ' in trim(s."name")) - 1) when trim(s."name") = '' then null else trim(s."name") end),
			upper(case when position(' ' in trim(es.nome)) > 0 then left(trim(es.nome),position(' ' in trim(es.nome)) - 1) when trim(es.nome) = '' then null else trim(es.nome) end)) as signer_name,
		coalesce(case when es.cpf = '' then null else es.cpf end,case when s.cpf = '' then null else s.cpf end) as signer_cpf,
		coalesce(s.share_percentage,es.participacao) as participacao,
		extract(year from age(lr.date_inserted,s.date_of_birth))::int as signer_age,
		s.date_of_birth::date as signer_date_of_birth,
		s.revenue::numeric,
		s.street as signer_street_address,
		s.city as signer_city,
		s.zip_code as signer_zip_code,
		s.administrator::int,
		es.nivel_pep,
		es.qualificacao,
		row_number() over (partition by lr.loan_request_id order by coalesce(s.share_percentage,es.participacao) desc,extract(year from age(lr.date_inserted,s.date_of_birth))::int desc) as indice_socio,
		s.number,
		s.complement
	from(select
			t.lead_id,
			lr.loan_request_id,
			dp.cnpj,
			dp.cpf,
			dp.name_of_responsible,
			lr.date_inserted::date
		from public.direct_prospects dp
		join public.offers o on o.direct_prospect_id = dp.direct_prospect_id
		join public.loan_requests lr on lr.offer_id = o.offer_id
		join mv_bizcredit_checklist t on t.lead_id = dp.direct_prospect_id) lr
	join(select 
			row_number() over (partition by lrs.loan_request_id,s.cpf order by 
				case when s.email is null then 1 else 0 end +
				case when s.civil_status is null then 1 else 0 end +
				case when s.profession is null then 1 else 0 end +
				case when s.rg is null then 1 else 0 end +
				case when s.issuing_body is null then 1 else 0 end +
				case when s.country_of_birth is null then 1 else 0 end +
				case when s.city_of_birth is null then 1 else 0 end +
				case when s.date_of_birth is null then 1 else 0 end) as indice,
			lrs.loan_request_id,
			s.cpf,
			s.share_percentage,
			s."name",
			sa.street,
			c."name" as city,
			sa.zip_code,
			s.revenue,
			s.administrator,
			s.date_of_birth,
			sa."number",
			sa.complement
		from public.loan_requests_signers lrs
		join mv_bizcredit_approval_tape t on t.loan_request_id = lrs.loan_request_id
		join public.signers s on s.signer_id = lrs.signer_id
		left join public.signer_addresses sa on sa.address_id = s.address_id
		left join public.cities c on c.city_id = sa.city_id
		where s.share_percentage > 0) s on s.loan_request_id = lr.loan_request_id
	full outer join (select
			cnpj,
			cpf,
			nome,
			nivel_pep,
			qualificacao,
			participacao,
			row_number() over (partition by cnpj,cpf order by id) as indice
		from public.empresas_socios
		where participacao > 0) es on es.cnpj = lr.cnpj and case es.cpf when '' then null else es.cpf end = case s.cpf when '' then null else s.cpf end and es.indice = 1
	where lr.cnpj is not null and case when s.cpf = '' then null else s.cpf end is not null and s.indice = 1) as t1
group by 1



CREATE MATERIALIZED VIEW lucas_leal.mv_bizcredit_ipstack
TABLESPACE pg_default
AS SELECT max_id.lead_id,
    (cc.data ->> 'distancia'::text)::numeric AS distancia_ipstack
   FROM credito_coleta cc
     JOIN ( SELECT t.lead_id,
            max(cc_1.id) AS max_id
           FROM credito_coleta cc_1
             JOIN direct_prospects dp ON "left"(dp.cnpj::text, 8) = "left"(cc_1.documento::text, 8)
             JOIN lucas_leal.mv_bizcredit_checklist t ON t.lead_id = dp.direct_prospect_id
          WHERE cc_1.tipo::text = 'IpStack'::text
          GROUP BY t.lead_id) max_id ON cc.id = max_id.max_id
  WHERE cc.tipo::text = 'IpStack'::text
WITH DATA;


CREATE MATERIALIZED VIEW lucas_leal.mv_bizcredit_emailage
TABLESPACE pg_default
AS SELECT max_id.lead_id,
    "left"(resultados.value ->> 'fraudRisk'::text, 3)::bigint AS score_emailage,
    "right"(resultados.value ->> 'fraudRisk'::text, length(resultados.value ->> 'fraudRisk'::text) - "position"(resultados.value ->> 'fraudRisk'::text, ' '::text)) AS risco_emailage
   FROM credito_coleta cc,
    LATERAL jsonb_array_elements((cc.data -> 'query'::text) -> 'results'::text) resultados(value),
    ( SELECT t.lead_id,
            min(cc_1.id) AS max_id
           FROM credito_coleta cc_1
             JOIN direct_prospects dp ON "left"(dp.cnpj::text, 8) = "left"(cc_1.documento::text, 8)
             JOIN lucas_leal.mv_bizcredit_checklist t ON t.lead_id = dp.direct_prospect_id
          WHERE cc_1.tipo::text = 'EmailAge'::text
          GROUP BY t.lead_id) max_id
  WHERE cc.tipo::text = 'EmailAge'::text AND cc.id = max_id.max_id
WITH DATA;


CREATE MATERIALIZED VIEW lucas_leal.mv_bizcredit_scr_cpf_major
TABLESPACE pg_default
AS SELECT t1.lead_id,
    t1.major_signer_cpf,
        CASE
            WHEN hist_raw.thin_file IS NULL THEN hist_json.long_term_debt_pf_curr
            ELSE 0::double precision
        END AS long_term_debt_pf_curr,
        CASE
            WHEN hist_raw.thin_file IS NULL THEN hist_json.long_term_debt_pf_1m
            ELSE 0::double precision
        END AS long_term_debt_pf_1m,
        CASE
            WHEN hist_raw.thin_file IS NULL THEN hist_json.long_term_debt_pf_2m
            ELSE 0::double precision
        END AS long_term_debt_pf_2m,
        CASE
            WHEN hist_raw.thin_file IS NULL THEN hist_json.long_term_debt_pf_3m
            ELSE 0::double precision
        END AS long_term_debt_pf_3m,
        CASE
            WHEN hist_raw.thin_file IS NULL THEN hist_json.long_term_debt_pf_4m
            ELSE 0::double precision
        END AS long_term_debt_pf_4m,
        CASE
            WHEN hist_raw.thin_file IS NULL THEN hist_json.long_term_debt_pf_5m
            ELSE 0::double precision
        END AS long_term_debt_pf_5m,
        CASE
            WHEN hist_raw.thin_file IS NULL THEN hist_json.long_term_debt_pf_6m
            ELSE 0::double precision
        END AS long_term_debt_pf_6m,
        CASE
            WHEN hist_raw.thin_file IS NULL THEN COALESCE(hist_json.months_since_last_overdue_pf, '-1'::integer::double precision)
            ELSE '-1'::integer::double precision
        END AS months_since_last_overdue_pf,
        CASE
            WHEN hist_raw.thin_file IS NULL THEN hist_json.max_overdue_pf
            ELSE 0::double precision
        END AS max_overdue_pf,
        CASE
            WHEN hist_raw.thin_file IS NULL THEN COALESCE(hist_json.months_since_last_default_pf, '-1'::integer::double precision)
            ELSE '-1'::integer::double precision
        END AS months_since_last_default_pf,
        CASE
            WHEN hist_raw.thin_file IS NULL THEN hist_json.max_default_pf
            ELSE 0::double precision
        END AS max_default_pf,
    (hist_raw.thin_file IS NOT NULL)::integer AS thin_file
   FROM lucas_leal.mv_bizcredit_signers t1
     LEFT JOIN ( SELECT t2.direct_prospect_id,
            t2.cpf,
            date_part('year'::text, age(min(t2.data_referencia)::timestamp with time zone, max(t2.data) FILTER (WHERE t2.data <= t2.data_referencia AND t2.vencidopf > 0::double precision)::timestamp with time zone)) * 12::double precision + date_part('month'::text, age(min(t2.data_referencia)::timestamp with time zone, max(t2.data) FILTER (WHERE t2.data <= t2.data_referencia AND t2.vencidopf > 0::double precision)::timestamp with time zone)) AS months_since_last_overdue_pf,
            max(t2.vencidopf) FILTER (WHERE t2.data <= t2.data_referencia) AS max_overdue_pf,
            date_part('year'::text, age(min(t2.data_referencia)::timestamp with time zone, max(t2.data) FILTER (WHERE t2.data <= t2.data_referencia AND t2.prejuizopf > 0::double precision)::timestamp with time zone)) * 12::double precision + date_part('month'::text, age(min(t2.data_referencia)::timestamp with time zone, max(t2.data) FILTER (WHERE t2.data <= t2.data_referencia AND t2.prejuizopf > 0::double precision)::timestamp with time zone)) AS months_since_last_default_pf,
            max(t2.prejuizopf) FILTER (WHERE t2.data <= t2.data_referencia) AS max_default_pf,
            sum(t2.debtpf) FILTER (WHERE t2.data = t2.data_referencia) AS long_term_debt_pf_curr,
            sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '1 mon'::interval)) AS long_term_debt_pf_1m,
            sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '2 mons'::interval)) AS long_term_debt_pf_2m,
            sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '3 mons'::interval)) AS long_term_debt_pf_3m,
            sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '4 mons'::interval)) AS long_term_debt_pf_4m,
            sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '5 mons'::interval)) AS long_term_debt_pf_5m,
            sum(t2.debtpf) FILTER (WHERE t2.data = (t2.data_referencia - '6 mons'::interval)) AS long_term_debt_pf_6m
           FROM ( SELECT t1_1.direct_prospect_id,
                    t1_1.cpf,
                        CASE
                            WHEN t1_1.max_id IS NOT NULL THEN to_date((((cc.data -> 'historico'::text) -> 'Carteira de Crédito'::text) -> 0) ->> 'data'::text, 'mm-yyyy'::text)
                            ELSE
                            CASE
                                WHEN date_part('day'::text, t1_1.data_pedido) >= 16::double precision THEN to_date(to_char(t1_1.data_pedido - '1 mon'::interval, 'Mon/yy'::text), 'Mon/yy'::text)
                                ELSE to_date(to_char(t1_1.data_pedido - '2 mons'::interval, 'Mon/yy'::text), 'Mon/yy'::text)
                            END
                        END AS data_referencia,
                    to_date(jsonb_array_elements((cc.data -> 'historico'::text) -> 'Carteira de Crédito'::text) ->> 'data'::text, 'mm/yyyy'::text) AS data,
                    replace(replace(replace(jsonb_array_elements((cc.data -> 'historico'::text) -> 'Carteira de Crédito'::text) ->> 'valor'::text, '-'::text, '0'::text), '.'::text, ''::text), ','::text, '.'::text)::double precision AS debtpf,
                    replace(replace(replace(jsonb_array_elements((cc.data -> 'historico'::text) -> 'Vencido'::text) ->> 'valor'::text, '-'::text, '0'::text), '.'::text, ''::text), ','::text, '.'::text)::double precision AS vencidopf,
                    replace(replace(replace(jsonb_array_elements((cc.data -> 'historico'::text) -> 'Prejuízo'::text) ->> 'valor'::text, '-'::text, '0'::text), '.'::text, ''::text), ','::text, '.'::text)::double precision AS prejuizopf
                   FROM credito_coleta cc
                     JOIN ( SELECT t.lead_id AS direct_prospect_id,
                            t.major_signer_cpf AS cpf,
                            max(COALESCE(lr.loan_date, lr.date_inserted::date)) AS data_pedido,
                            max(cc_1.id) FILTER (WHERE COALESCE(to_timestamp(regexp_replace(replace(cc_1.data ->> 'criado_em'::text, 'T'::text, ' '::text), '\..*'::text, ''::text), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc_1.data_coleta)::date <= COALESCE(lr.loan_date::timestamp without time zone, lr.date_inserted::date + '10 days'::interval)) AS max_id,
                            min(cc_1.id) AS min_id
                           FROM credito_coleta cc_1
                             JOIN lucas_leal.mv_bizcredit_signers t ON cc_1.documento::text = t.major_signer_cpf
                             JOIN loan_requests lr ON lr.loan_request_id = t.loan_request_id
                          WHERE cc_1.documento_tipo::text = 'CPF'::text AND cc_1.tipo::text = 'SCR'::text
                          GROUP BY t.lead_id, t.major_signer_cpf) t1_1 ON cc.id = COALESCE(t1_1.max_id, t1_1.min_id)) t2
          GROUP BY t2.direct_prospect_id, t2.cpf) hist_json ON hist_json.direct_prospect_id = t1.lead_id
     LEFT JOIN ( SELECT t1_1.direct_prospect_id,
            t1_1.cpf,
            COALESCE(cc.data ->> 'erro'::text, cc.data ->> 'error'::text) AS thin_file
           FROM credito_coleta cc
             JOIN ( SELECT t.lead_id AS direct_prospect_id,
                    t.major_signer_cpf AS cpf,
                    max(cc_1.id) FILTER (WHERE COALESCE(to_timestamp(regexp_replace(replace(cc_1.data ->> 'criado_em'::text, 'T'::text, ' '::text), '\..*'::text, ''::text), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc_1.data_coleta)::date <= COALESCE(lr.loan_date::timestamp without time zone, lr.date_inserted::date + '10 days'::interval)) AS max_id,
                    min(cc_1.id) AS min_id
                   FROM credito_coleta cc_1
                     JOIN lucas_leal.mv_bizcredit_signers t ON cc_1.documento::text = t.major_signer_cpf
                     JOIN loan_requests lr ON lr.loan_request_id = t.loan_request_id
                  WHERE cc_1.documento_tipo::text = 'CPF'::text AND cc_1.tipo::text = 'SCR'::text
                  GROUP BY t.lead_id, t.major_signer_cpf) t1_1 ON cc.id = COALESCE(t1_1.max_id, t1_1.min_id)) hist_raw ON hist_raw.direct_prospect_id = t1.lead_id
WITH DATA;

		
create materialized view mv_bizcredit_serasa_acionistas as		
SELECT t1.direct_prospect_id,
    t1.cpf,
        CASE
            WHEN (cc.data ->> 'acionistas'::text) = '{}'::text THEN NULL::text
            ELSE ((cc.data -> 'acionistas'::text) -> jsonb_object_keys(cc.data -> 'acionistas'::text)) ->> 'cpf'::text
        END AS cpf_socios,
    jsonb_object_keys(cc.data -> 'acionistas'::text) AS nome,
    (((cc.data -> 'acionistas'::text) -> jsonb_object_keys(cc.data -> 'acionistas'::text)) ->> 'percentual'::text)::numeric AS share,
	case when cc.data->>'acionistas' = '{}' then null else to_date(cc.data->'acionistas'->jsonb_object_keys(cc.data->'acionistas')->>'data_entrada','yyyymmdd') end as data_entrada,
	case when cc.data->'controle_adm'->>'adms' = '{}' then null else cc.data->'controle_adm'->'adms'->jsonb_object_keys(cc.data->'controle_adm'->'adms')->>'cargo' end as cargo_atual,
	case when cc.data->'controle_adm'->>'adms' = '{}' then null else cc.data->'controle_adm'->'adms'->jsonb_object_keys(cc.data->'controle_adm'->'adms')->>'vinculo' end as info_partic_atual
FROM credito_coleta cc
JOIN (SELECT 
		dp.direct_prospect_id,
	    max(dp.cnpj) as cnpj,
	    max(dp.cpf) as cpf,
	    max(id) AS max_id
	FROM credito_coleta cc
	JOIN direct_prospects dp ON "left"(cc.documento::text, 8) = "left"(dp.cnpj::text, 8)
	JOIN offers o ON o.direct_prospect_id = dp.direct_prospect_id
	JOIN loan_requests lr ON lr.offer_id = o.offer_id
	join mv_bizcredit_checklist t on t.lead_id = dp.direct_prospect_id
	WHERE to_date((cc.data -> 'datetime'::text) ->> 'data'::text, 'yyyymmdd'::text) <= coalesce(lr.loan_date::date,lr.date_inserted::date + '10 days'::interval) AND (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying]::text[]))
	GROUP BY dp.direct_prospect_id) t1 ON cc.id = t1.max_id

	
--TRANSACTIONS
create materialized view mv_bizcredit_transactions as		
select
	*
from(select 
			e.loan_request_id,
			count(*) filter (where e.tag = 'salario') as salario,
			count(*) filter (where e.tag = 'salario' and value < 0) as salario_saida,
			count(*) filter (where e.tag = 'imposto') as imposto,
			count(*) filter (where e.tag = 'imposto' and value < 0) as imposto_saida,
			count(*) filter (where e.tag = 'imposto' and value < 0 and tratado not like '%iof%') as imposto_saida_sem_iof,
			count(*) filter (where e.tag = 'ccf') as ccf,
			count(*) filter (where e.tag = 'ccf' and value > 0) as ccf_entrada,
			count(*) filter (where e.tag = 'sacado_governo' and value > 0) as governo_entrada,
			sum(e.value) filter (where e.tag in ('boletos','tbi_ted','dinheiro','sacado_governo','adquirencia','marketplace') and e.intervalo = '_30' and e.movimentacao = 'positivo') as entradas_operacional1_30,
			sum(e.value) filter (where e.tag in ('boletos','tbi_ted','dinheiro','sacado_governo','adquirencia','marketplace') and e.intervalo = '_60' and e.movimentacao = 'positivo') as entradas_operacional1_60,
			sum(e.value) filter (where e.tag in ('boletos','tbi_ted','dinheiro','sacado_governo','adquirencia','marketplace') and e.intervalo = '_over60' and e.movimentacao = 'positivo') as entradas_operacional1_over60,
			count(*) filter (where e.tag = 'adquirencia' and value > 0) as adquirencia_entrada			
		from data_science.extratos_apos_amostra e
		where e.value != 'NaN' 
		group by 1		
	union
		select 
			e.loan_request_id,
			count(*) filter (where e.tag = 'salario') as salario,
			count(*) filter (where e.tag = 'salario' and value < 0) as salario_saida,
			count(*) filter (where e.tag = 'imposto') as imposto,
			count(*) filter (where e.tag = 'imposto' and value < 0) as imposto_saida,
			count(*) filter (where e.tag = 'imposto' and value < 0 and tratado not like '%iof%') as imposto_saida_sem_iof,
			count(*) filter (where e.tag = 'ccf') as ccf,
			count(*) filter (where e.tag = 'ccf' and value > 0) as ccf_entrada,
			count(*) filter (where e.tag = 'sacado_governo' and value > 0) as governo_entrada,
			sum(e.value) filter (where e.tag in ('boletos','tbi_ted','dinheiro','sacado_governo','adquirencia','marketplace') and e.intervalo = '_30' and e.movimentacao = 'positivo') as entradas_operacional1_30,
			sum(e.value) filter (where e.tag in ('boletos','tbi_ted','dinheiro','sacado_governo','adquirencia','marketplace') and e.intervalo = '_60' and e.movimentacao = 'positivo') as entradas_operacional1_60,
			sum(e.value) filter (where e.tag in ('boletos','tbi_ted','dinheiro','sacado_governo','adquirencia','marketplace') and e.intervalo = '_over60' and e.movimentacao = 'positivo') as entradas_operacional1_over60,
			count(*) filter (where e.tag = 'adquirencia' and value > 0) as adquirencia_entrada			
		from data_science.extratos_v2 e
		where e.value != 'NaN' and e.loan_request_id not in (select distinct loan_request_id from data_science.extratos_apos_amostra)
		group by 1
	union
		select 
			e.loan_request_id,
			count(*) filter (where e.tag = 'salario') as salario,
			count(*) filter (where e.tag = 'salario' and value < 0) as salario_saida,
			count(*) filter (where e.tag = 'imposto') as imposto,
			count(*) filter (where e.tag = 'imposto' and value < 0) as imposto_saida,
			count(*) filter (where e.tag = 'imposto' and value < 0 and tratado not like '%iof%') as imposto_saida_sem_iof,
			count(*) filter (where e.tag = 'ccf') as ccf,
			count(*) filter (where e.tag = 'ccf' and value > 0) as ccf_entrada,
			count(*) filter (where e.tag = 'sacado_governo' and value > 0) as governo_entrada,
			sum(e.value) filter (where e.tag in ('boletos','tbi_ted','dinheiro','sacado_governo','adquirencia','marketplace') and e.intervalo = '_30' and e.movimentacao = 'positivo') as entradas_operacional1_30,
			sum(e.value) filter (where e.tag in ('boletos','tbi_ted','dinheiro','sacado_governo','adquirencia','marketplace') and e.intervalo = '_60' and e.movimentacao = 'positivo') as entradas_operacional1_60,
			sum(e.value) filter (where e.tag in ('boletos','tbi_ted','dinheiro','sacado_governo','adquirencia','marketplace') and e.intervalo = '_over60' and e.movimentacao = 'positivo') as entradas_operacional1_over60,
			count(*) filter (where e.tag = 'adquirencia' and value > 0) as adquirencia_entrada			
		from t_bizcredit_transactions_anexo_oot e
		where e.value != 'NaN' and e.loan_request_id not in (select distinct loan_request_id from data_science.extratos_apos_amostra)
		group by 1) as transactions



insert into t_bizcredit_transactions_anexo_oot
with t as (
	select 
    	a.loan_request_id, 
    	tr.value as value, 
    	dp.month_revenue, 
    	dp.sector, 
    	tr.description as chave_transacao,
		case
			when (dp.opt_in_date::date - tr.date::date)::int <= 30 then '_30'
			when (dp.opt_in_daete::date - tr.date::date)::int <= 60 then '_60'
			else '_over60' end as intervalo,
		case
        	when extract('day' from tr.date::date) > 20 then '_fim'
			when extract('day' from tr.date::date) > 10 then '_meio'
			else '_inicio' end as periodo_mes,
		case
        	when tr.value < 0 then 'negativo'
        	when tr.value > 0 then 'positivo'
			else 'zero' end as movimentacao,
        translate(lower(replace(trim(regexp_replace(tr.description, '[^[:alpha:]\s]', '', 'g')), ' ', '')),'Ã¡Ã Ã¢Ã£Ã¤Ã©Ã¨ÃªÃ«Ã­Ã¬Ã®Ã¯Ã³Ã²Ã´ÃµÃ¶ÃºÃ¹Ã»Ã¼Ã½Ã±Ã§','aaaaaeeeeiiiiooooouuuuync') as tratado,
    	tr."date",
    	tr.id as transaction_id
    from direct_prospects dp
    join offers o on dp.direct_prospect_id = o.direct_prospect_id
    join loan_requests lr on o.offer_id = lr.offer_id
    join biz_credit.analysis a on a.loan_request_id = lr.loan_request_id
    join biz_credit.analysis_transactions at on a.id = at.analysis_id
    join biz_credit.transactions tr on tr.id = at.transaction_id
    where tr.value is not null
        and tr.value <> 0
        and abs(tr.value) < 2000000
        and tr.description is not null
		and a.loan_request_id in (select 
				t1.loan_request_id
			from mv_bizcredit_approval_tape_full t1
			left join mv_bizcredit_transactions t2 on t2.loan_request_id = t1.loan_request_id
			where renovacao = 0 and status in ('Expirado','REJECTED','ACCEPTED','LOST','Cancelado') and esteira = 'biz' and periodo_controle in ('Pre covid', 'Pos covid') and t2.loan_request_id is null))
select t1.*, coalesce(t1.tag_descricao, t1.tag_tratado_da_query) as tag from (
    select t.*,
           case
           when tratado similar to ('%(municipal|camara|companhiabrasileira)%') then 'sacado_governo'
           when tratado like 'tar%' then 'servico_bancario'
           when tratado similar to ('%(aplicacao|apli)%') then 'aplicacao'
           when tratado like 'recebimentofornecedorpagsegurointernetsa%' then 'adquirencia'
           when tratado like '%fornecedor%' then 'boletos'
           when tratado like 'ted%' then 'tbi_ted'
           when tratado like 'tb%' then 'tbi_ted'
           when tratado like 'credtransf%' then 'credito'
           when tratado like '%intercredis%' then 'credito'
           when tratado = 'sispagpagsegurointlt' then 'servico_bancario'
           when tratado like '%pagseguro%' then 'adquirencia'
           when tratado similar to ('%(tarifa|seguradora|seguros|seguro)%') then 'servico_bancario'
           when tratado like 'pagamentodetitulobancobradescosa%' then 'antecipacao'
           when tratado like 'recebimentofornecedorbancobradesco%' then 'antecipacao'
           when tratado like 'pagamentodeboletobancobradesco%' then 'antecipacao'
           when tratado like 'pagamentorefpagseguro%' then 'boletos'
           when tratado like 'recebimentofornecedor%' then 'boletos'
           when tratado like 'sispagfornecedorested%' then 'boletos'
           when tratado like 'debitotransferencia%' then 'boletos'
           when tratado like 'sispagtransfccitau%' then 'boletos'
           when tratado like 'debtransf%' then 'boletos'
           when tratado similar to ('%(pagamentocheque|pagamentodeboleto)%') then 'boletos'
           when tratado similar to ('%(depdinh|dinheiro|deposito|depsito|cheque|chequecomp|cheqcomp|compensado|dpch|' ||
                                    'depch|depemdinh|dpdinh|dinlot|dpcxaqui|dpdinatm|chqcmp)%') then 'dinheiro'
           when tratado like '%tev' then 'tbi_ted'
           when tratado like '%doc' then 'tbi_ted'
           when tratado similar to '%(saque|avulso|sqatm|sqcartao|saqcartao|saquenota)%' then 'saque'
           when tratado similar to ('%(saldo|resgate)%') then 'saldo'
           when tratado similar to ('%(transf|tbicc|tbcc|tblicc|ted|doccredito)%') then 'tbi_ted'
           when tratado like 'pagamentodebol%' then 'dinheiro'
           when tratado similar to ('%(giro|credito|cred|emprestimo)%') then 'credito'
           when tratado similar to ('%(comunicac|copel|cpel|embratel|light|telecom|cemig|ampla|cedae|nextel|sanepar|enel|sabesp)%') then 'custo_fixo'
           when tratado similar to ('%(salario|salar|salÃ¡r)%') then 'salario'
           when tratado similar to ('%(imposto|ipva|iof|iptu)%') then 'imposto'
           when tratado similar to ('%(multa|juros|mora)%') then 'mora_juros'
           when tratado similar to ('%(desconto|desc|antecipacao)%') then 'antecipacao'
           when tratado similar to ('%(movt|cobranca|cobranÃ§a|intdas|dobito|dbito|debito|pagamento|pgto|pgtfat|' ||
                                            'pagar|agto|pagto|titpag|pagtit|compra|ompra|paguefacil|debt|gasto|cobrana|' ||
                                            'cob|sispag|fornecedor|boleto|bloqueto)%') then 'boletos'
           when tratado similar to ('%(cielo|getnet|safra|stone|cciieelloo|ssttoonnee|tone|stelo|redemast|' ||
                                            'credmaster|visa|alelo|ciel|sodexo|rede|itaucard|pagseguro|rshop|' ||
                                            'cpmaestro)%') then 'adquirencia'
           when tratado like 'chq%' then 'dinheiro' end as tag_tratado_da_query,
case
           when chave_transacao similar to ('%(AplicaÃ§Ã£o PoupanÃ§a|Transferido da poupanÃ§a|BB RF CP|RESG AUT|RES AUT|' ||
                                          'CONTAMAX EMPRESARIAL|INVEST FACIL|APLIC AUT|BB GIRO|SDO CTA/APL|' ||
                                          'SALDO APLIC AUT|INVEST FACIL)%') then 'aplicacao'
           when chave_transacao similar to ('%(24206917000105|22912939000165|ANTECIPACAO|DESCONTO ESCRITURAL|' ||
                                          'DESCONTO DE CHEQUES|DESCONTO DE TÃ�TULOS|CHEQ SAC|ANTECIP|26648789000185|' ||
                                          '05957034000192|25072820000110|20529090000110|18062770000124|67229260000124|' ||
                                          '14368548000101|18488755000142|10503215000189|23293595000116|08186146000185|' ||
                                          '12768409000131|28311103000191|21053720000195|21511446000150|01482208000157|' ||
                                          '01158373000158|22459023000100|15512948000101|28057482000135|67229260000104|' ||
                                          '27699472000130|13098860000151|04379829000106|LIQUIDO DE DESCONTO|' ||
                                          'CRÃ‰D.LIBERAÃ‡ÃƒO|LIBERACAO CREDITO)%') then 'antecipacao'
           when chave_transacao similar to ('%(MERCADOPAGO|10573521000191|00776574000156|B2W|CNOVA|SLB|WMB|AMAZON)%') then 'marketplace'
           when chave_transacao similar to ('%(CH DEV M12|CH DEV M11|CCF|MOTIVO 11|SEM FUNDO)%') then 'ccf'
           when chave_transacao similar to ('%(EMPR/FIN|OPERACAO DE CAMBIO|01-FIN COMPRA|CARTAO CREDITO|' ||
                                            'CAPITALIZACAO|PREST. DE EMPREST.|EMPREST/FINANCIAMENTO|PREST CDC|' ||
                                            'CAPITAL GIRO|PARCELA GIRO|GIRO RÃ�PIDO|EMPRÃ‰STIMO|PREST.EMPREST|' ||
                                            'AMORTIZACAO CONTRATO|LIQUIDACAO DE PARCELA|PREST EMPR|PARCELAMENTO|CREDITO TRANSFERENCIA)%') then 'credito'
           when chave_transacao similar to ('%(DEVOLVIDA|EST LIS/JUROS|DEV TED|CH DEV|DEVOLVIDO|MESMA TITULARIDADE|' ||
                                            'OPERACAO DE CAMBIO|01-FIN COMPRA|RESG|ESTORNO|BLOQ |BLOQ.|DEVOLVID|RSG FUNDO)%') then 'ignorado'
           when chave_transacao similar to ('%(VIVO|CPFL|CLARO|OI |TIM|ELETROPAULO|CONTA DE TELEFONE|AGUA|LUZ|SABESP' ||
                                            '|PAG FONE|PAG AGUA|PG LUZ/GAS|INT NET SERV COMUNICAC|PEDAGIO|TELEFONE' ||
                                            '|CELULAR|PRE-PAGO|TELEFONICA|GVT|CELESC|ENERGISA|TV A CABO|ELÃ‰TRICA E GÃ�S' ||
                                            '|SANEAMENTO|CONTA GAS|BANDEIRANTE ENERGI|TELEMAR|SANEAGO|CELG|BRASIL TELECOM)%') then 'custo_fixo'
           when chave_transacao similar to ('%(FGTS|INSS|SISPAG SALARIOS|FOLHA PAGTO|GPS|PAG GPS|GPS|SALARIO|PREV SOCIAL|FOLHA PGTO|FOL PAGTO|SalÃ¡rios)%') then 'salario'
           when chave_transacao similar to ('%(DARF|TRIBUTO|SIMPLES NACIONA|IMPOSTO|SISPAG TRIBUTOS|PG ORG GOV|SEFAZ|IOF|PAG GPS)%') then 'imposto'
           when chave_transacao similar to ('%(SISPAG FORNECEDORES|PAGAMENTO DE TITULOS|PGTO FORNECEDORES|PAG TIT|' ||
                                            'PGTO TITULO|FAT.CARTAO|PAGTO ELETRON COBRANCA|DÃ‰B.TIT COMPE|' ||
                                            'PAGAMENTO A FORNECEDORES|PAG TÃ�T|PAG BOLETO|PG.P/INTERNET|LIQUIDACAO DE BLOQUETOS|PAGAMENTO DE TÃ�TULO)%') then 'boletos'
           when chave_transacao similar to ('%(AplicaÃ§Ã£o PoupanÃ§a|Transferido da poupanÃ§a|BB RF CP|RESG AUT|RES AUT|' ||
                                            'CONTAMAX EMPRESARIAL|INVEST FACIL|APLIC AUT|BB GIRO|SDO CTA/APL|SALDO APLIC AUT|' ||
                                            'INVEST FACIL|S A L D O|SALDO)%') then 'saldo'
           when chave_transacao similar to ('%(JUROS|LIS|MORA|MULTA|ENCARGOS|MORATORIA|NAO PAGA P/ SACADO)%') then 'mora_juros'
           when chave_transacao similar to ('%(JUSTICA|JUSTIÃ‡A|MINISTÃ‰RIO|MINISTERIO|PM.|PM )%') then 'sacado_governo'
           when chave_transacao similar to ('%(SDO CTA/APL|SALDO APLIC AUT)%') then 'saldo_aplicacao'
           when chave_transacao similar to ('%(INVEST DIR|CREDITORIOS|FUNDO DE INVE|SECURITIZADORA|FACTORING|FOMENTO|' ||
                                            'FOM MERCANTIL|FIDC|NOVA S R M|SERVICOS FINANCEIROS|RED AS)%') then 'consulta_factor' end as tag_descricao
    from t
    ) as t1
where (t1.tag_descricao is not null or t1.tag_tratado_da_query is not null)
		
		
--SCR PJ
CREATE MATERIALIZED VIEW lucas_leal.mv_bizcredit_scr_pj
TABLESPACE pg_default
AS SELECT t1.lead_id,
        CASE
            WHEN modal_erro.thin_file IS NOT NULL THEN 0
            ELSE (GREATEST(modal_json.emprestimos_giro_longo_pj_curr, modal_json.emprestimos_giro_curto_pj_curr, modal_json.emprestimos_giro_rotativo_pj_curr) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_1m, modal_json.emprestimos_giro_curto_pj_1m, modal_json.emprestimos_giro_rotativo_pj_1m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_2m, modal_json.emprestimos_giro_curto_pj_2m, modal_json.emprestimos_giro_rotativo_pj_2m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_3m, modal_json.emprestimos_giro_curto_pj_3m, modal_json.emprestimos_giro_rotativo_pj_3m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_4m, modal_json.emprestimos_giro_curto_pj_4m, modal_json.emprestimos_giro_rotativo_pj_4m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_5m, modal_json.emprestimos_giro_curto_pj_5m, modal_json.emprestimos_giro_rotativo_pj_5m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_6m, modal_json.emprestimos_giro_curto_pj_6m, modal_json.emprestimos_giro_rotativo_pj_6m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_7m, modal_json.emprestimos_giro_curto_pj_7m, modal_json.emprestimos_giro_rotativo_pj_7m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_8m, modal_json.emprestimos_giro_curto_pj_8m, modal_json.emprestimos_giro_rotativo_pj_8m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_9m, modal_json.emprestimos_giro_curto_pj_9m, modal_json.emprestimos_giro_rotativo_pj_9m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_10m, modal_json.emprestimos_giro_curto_pj_10m, modal_json.emprestimos_giro_rotativo_pj_10m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_11m, modal_json.emprestimos_giro_curto_pj_11m, modal_json.emprestimos_giro_rotativo_pj_11m) > 0::double precision)::integer
        END AS count_capital_giro,
        CASE
            WHEN modal_erro.thin_file IS NOT NULL THEN 0
            ELSE (GREATEST(modal_json.emprestimos_giro_longo_pj_curr, modal_json.emprestimos_giro_curto_pj_curr, modal_json.emprestimos_giro_rotativo_pj_curr, modal_json.financiamentos_pj_curr, modal_json.outrosfinanciamentos_pj_curr) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_1m, modal_json.emprestimos_giro_curto_pj_1m, modal_json.emprestimos_giro_rotativo_pj_1m, modal_json.financiamentos_pj_1m, modal_json.outrosfinanciamentos_pj_1m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_2m, modal_json.emprestimos_giro_curto_pj_2m, modal_json.emprestimos_giro_rotativo_pj_2m, modal_json.financiamentos_pj_2m, modal_json.outrosfinanciamentos_pj_2m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_3m, modal_json.emprestimos_giro_curto_pj_3m, modal_json.emprestimos_giro_rotativo_pj_3m, modal_json.financiamentos_pj_3m, modal_json.outrosfinanciamentos_pj_3m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_4m, modal_json.emprestimos_giro_curto_pj_4m, modal_json.emprestimos_giro_rotativo_pj_4m, modal_json.financiamentos_pj_4m, modal_json.outrosfinanciamentos_pj_4m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_5m, modal_json.emprestimos_giro_curto_pj_5m, modal_json.emprestimos_giro_rotativo_pj_5m, modal_json.financiamentos_pj_5m, modal_json.outrosfinanciamentos_pj_5m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_6m, modal_json.emprestimos_giro_curto_pj_6m, modal_json.emprestimos_giro_rotativo_pj_6m, modal_json.financiamentos_pj_6m, modal_json.outrosfinanciamentos_pj_6m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_7m, modal_json.emprestimos_giro_curto_pj_7m, modal_json.emprestimos_giro_rotativo_pj_7m, modal_json.financiamentos_pj_7m, modal_json.outrosfinanciamentos_pj_7m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_8m, modal_json.emprestimos_giro_curto_pj_8m, modal_json.emprestimos_giro_rotativo_pj_8m, modal_json.financiamentos_pj_8m, modal_json.outrosfinanciamentos_pj_8m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_9m, modal_json.emprestimos_giro_curto_pj_9m, modal_json.emprestimos_giro_rotativo_pj_9m, modal_json.financiamentos_pj_9m, modal_json.outrosfinanciamentos_pj_9m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_10m, modal_json.emprestimos_giro_curto_pj_10m, modal_json.emprestimos_giro_rotativo_pj_10m, modal_json.financiamentos_pj_10m, modal_json.outrosfinanciamentos_pj_10m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_11m, modal_json.emprestimos_giro_curto_pj_11m, modal_json.emprestimos_giro_rotativo_pj_11m, modal_json.financiamentos_pj_11m, modal_json.outrosfinanciamentos_pj_11m) > 0::double precision)::integer
        END AS count_capital_giro_financiamento,
        CASE
            WHEN modal_erro.thin_file IS NOT NULL THEN 0
            ELSE (GREATEST(modal_json.emprestimos_giro_longo_pj_curr, modal_json.emprestimos_giro_curto_pj_curr, modal_json.emprestimos_giro_rotativo_pj_curr, modal_json.financiamentos_pj_curr - modal_json.financiamento_veiculo_pj_curr, modal_json.outrosfinanciamentos_pj_curr) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_1m, modal_json.emprestimos_giro_curto_pj_1m, modal_json.emprestimos_giro_rotativo_pj_1m, modal_json.financiamentos_pj_1m - modal_json.financiamento_veiculo_pj_1m, modal_json.outrosfinanciamentos_pj_1m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_2m, modal_json.emprestimos_giro_curto_pj_2m, modal_json.emprestimos_giro_rotativo_pj_2m, modal_json.financiamentos_pj_2m - modal_json.financiamento_veiculo_pj_2m, modal_json.outrosfinanciamentos_pj_2m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_3m, modal_json.emprestimos_giro_curto_pj_3m, modal_json.emprestimos_giro_rotativo_pj_3m, modal_json.financiamentos_pj_3m - modal_json.financiamento_veiculo_pj_3m, modal_json.outrosfinanciamentos_pj_3m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_4m, modal_json.emprestimos_giro_curto_pj_4m, modal_json.emprestimos_giro_rotativo_pj_4m, modal_json.financiamentos_pj_4m - modal_json.financiamento_veiculo_pj_4m, modal_json.outrosfinanciamentos_pj_4m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_5m, modal_json.emprestimos_giro_curto_pj_5m, modal_json.emprestimos_giro_rotativo_pj_5m, modal_json.financiamentos_pj_5m - modal_json.financiamento_veiculo_pj_5m, modal_json.outrosfinanciamentos_pj_5m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_6m, modal_json.emprestimos_giro_curto_pj_6m, modal_json.emprestimos_giro_rotativo_pj_6m, modal_json.financiamentos_pj_6m - modal_json.financiamento_veiculo_pj_6m, modal_json.outrosfinanciamentos_pj_6m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_7m, modal_json.emprestimos_giro_curto_pj_7m, modal_json.emprestimos_giro_rotativo_pj_7m, modal_json.financiamentos_pj_7m - modal_json.financiamento_veiculo_pj_7m, modal_json.outrosfinanciamentos_pj_7m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_8m, modal_json.emprestimos_giro_curto_pj_8m, modal_json.emprestimos_giro_rotativo_pj_8m, modal_json.financiamentos_pj_8m - modal_json.financiamento_veiculo_pj_8m, modal_json.outrosfinanciamentos_pj_8m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_9m, modal_json.emprestimos_giro_curto_pj_9m, modal_json.emprestimos_giro_rotativo_pj_9m, modal_json.financiamentos_pj_9m - modal_json.financiamento_veiculo_pj_9m, modal_json.outrosfinanciamentos_pj_9m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_10m, modal_json.emprestimos_giro_curto_pj_10m, modal_json.emprestimos_giro_rotativo_pj_10m, modal_json.financiamentos_pj_10m - modal_json.financiamento_veiculo_pj_10m, modal_json.outrosfinanciamentos_pj_10m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_11m, modal_json.emprestimos_giro_curto_pj_11m, modal_json.emprestimos_giro_rotativo_pj_11m, modal_json.financiamentos_pj_11m - modal_json.financiamento_veiculo_pj_11m, modal_json.outrosfinanciamentos_pj_11m) > 0::double precision)::integer
        END AS count_capital_giro_financiamento_sem_veiculo,
        CASE
            WHEN modal_erro.thin_file IS NOT NULL THEN 0
            ELSE (GREATEST(modal_json.emprestimos_giro_longo_pj_curr, modal_json.emprestimos_giro_curto_pj_curr, modal_json.emprestimos_giro_rotativo_pj_curr, modal_json.financiamentos_pj_curr - modal_json.financiamento_veiculo_pj_curr, modal_json.outrosfinanciamentos_pj_curr, modal_json.emprestimos_microcredito_pj_curr) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_1m, modal_json.emprestimos_giro_curto_pj_1m, modal_json.emprestimos_giro_rotativo_pj_1m, modal_json.financiamentos_pj_1m - modal_json.financiamento_veiculo_pj_1m, modal_json.outrosfinanciamentos_pj_1m, modal_json.emprestimos_microcredito_pj_1m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_2m, modal_json.emprestimos_giro_curto_pj_2m, modal_json.emprestimos_giro_rotativo_pj_2m, modal_json.financiamentos_pj_2m - modal_json.financiamento_veiculo_pj_2m, modal_json.outrosfinanciamentos_pj_2m, modal_json.emprestimos_microcredito_pj_2m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_3m, modal_json.emprestimos_giro_curto_pj_3m, modal_json.emprestimos_giro_rotativo_pj_3m, modal_json.financiamentos_pj_3m - modal_json.financiamento_veiculo_pj_3m, modal_json.outrosfinanciamentos_pj_3m, modal_json.emprestimos_microcredito_pj_3m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_4m, modal_json.emprestimos_giro_curto_pj_4m, modal_json.emprestimos_giro_rotativo_pj_4m, modal_json.financiamentos_pj_4m - modal_json.financiamento_veiculo_pj_4m, modal_json.outrosfinanciamentos_pj_4m, modal_json.emprestimos_microcredito_pj_4m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_5m, modal_json.emprestimos_giro_curto_pj_5m, modal_json.emprestimos_giro_rotativo_pj_5m, modal_json.financiamentos_pj_5m - modal_json.financiamento_veiculo_pj_5m, modal_json.outrosfinanciamentos_pj_5m, modal_json.emprestimos_microcredito_pj_5m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_6m, modal_json.emprestimos_giro_curto_pj_6m, modal_json.emprestimos_giro_rotativo_pj_6m, modal_json.financiamentos_pj_6m - modal_json.financiamento_veiculo_pj_6m, modal_json.outrosfinanciamentos_pj_6m, modal_json.emprestimos_microcredito_pj_6m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_7m, modal_json.emprestimos_giro_curto_pj_7m, modal_json.emprestimos_giro_rotativo_pj_7m, modal_json.financiamentos_pj_7m - modal_json.financiamento_veiculo_pj_7m, modal_json.outrosfinanciamentos_pj_7m, modal_json.emprestimos_microcredito_pj_7m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_8m, modal_json.emprestimos_giro_curto_pj_8m, modal_json.emprestimos_giro_rotativo_pj_8m, modal_json.financiamentos_pj_8m - modal_json.financiamento_veiculo_pj_8m, modal_json.outrosfinanciamentos_pj_8m, modal_json.emprestimos_microcredito_pj_8m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_9m, modal_json.emprestimos_giro_curto_pj_9m, modal_json.emprestimos_giro_rotativo_pj_9m, modal_json.financiamentos_pj_9m - modal_json.financiamento_veiculo_pj_9m, modal_json.outrosfinanciamentos_pj_9m, modal_json.emprestimos_microcredito_pj_9m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_10m, modal_json.emprestimos_giro_curto_pj_10m, modal_json.emprestimos_giro_rotativo_pj_10m, modal_json.financiamentos_pj_10m - modal_json.financiamento_veiculo_pj_10m, modal_json.outrosfinanciamentos_pj_10m, modal_json.emprestimos_microcredito_pj_10m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_11m, modal_json.emprestimos_giro_curto_pj_11m, modal_json.emprestimos_giro_rotativo_pj_11m, modal_json.financiamentos_pj_11m - modal_json.financiamento_veiculo_pj_11m, modal_json.outrosfinanciamentos_pj_11m, modal_json.emprestimos_microcredito_pj_11m) > 0::double precision)::integer
        END AS count_capital_giro_c_fin_s_veiculo_c_micro,
        CASE
            WHEN modal_erro.thin_file IS NOT NULL THEN 0
            ELSE (GREATEST(modal_json.emprestimos_giro_longo_pj_curr, modal_json.emprestimos_giro_curto_pj_curr, modal_json.emprestimos_giro_rotativo_pj_curr, modal_json.financiamentos_pj_curr, modal_json.outrosfinanciamentos_pj_curr, modal_json.emprestimos_microcredito_pj_curr) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_1m, modal_json.emprestimos_giro_curto_pj_1m, modal_json.emprestimos_giro_rotativo_pj_1m, modal_json.financiamentos_pj_1m, modal_json.outrosfinanciamentos_pj_1m, modal_json.emprestimos_microcredito_pj_1m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_2m, modal_json.emprestimos_giro_curto_pj_2m, modal_json.emprestimos_giro_rotativo_pj_2m, modal_json.financiamentos_pj_2m, modal_json.outrosfinanciamentos_pj_2m, modal_json.emprestimos_microcredito_pj_2m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_3m, modal_json.emprestimos_giro_curto_pj_3m, modal_json.emprestimos_giro_rotativo_pj_3m, modal_json.financiamentos_pj_3m, modal_json.outrosfinanciamentos_pj_3m, modal_json.emprestimos_microcredito_pj_3m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_4m, modal_json.emprestimos_giro_curto_pj_4m, modal_json.emprestimos_giro_rotativo_pj_4m, modal_json.financiamentos_pj_4m, modal_json.outrosfinanciamentos_pj_4m, modal_json.emprestimos_microcredito_pj_4m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_5m, modal_json.emprestimos_giro_curto_pj_5m, modal_json.emprestimos_giro_rotativo_pj_5m, modal_json.financiamentos_pj_5m, modal_json.outrosfinanciamentos_pj_5m, modal_json.emprestimos_microcredito_pj_5m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_6m, modal_json.emprestimos_giro_curto_pj_6m, modal_json.emprestimos_giro_rotativo_pj_6m, modal_json.financiamentos_pj_6m, modal_json.outrosfinanciamentos_pj_6m, modal_json.emprestimos_microcredito_pj_6m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_7m, modal_json.emprestimos_giro_curto_pj_7m, modal_json.emprestimos_giro_rotativo_pj_7m, modal_json.financiamentos_pj_7m, modal_json.outrosfinanciamentos_pj_7m, modal_json.emprestimos_microcredito_pj_7m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_8m, modal_json.emprestimos_giro_curto_pj_8m, modal_json.emprestimos_giro_rotativo_pj_8m, modal_json.financiamentos_pj_8m, modal_json.outrosfinanciamentos_pj_8m, modal_json.emprestimos_microcredito_pj_8m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_9m, modal_json.emprestimos_giro_curto_pj_9m, modal_json.emprestimos_giro_rotativo_pj_9m, modal_json.financiamentos_pj_9m, modal_json.outrosfinanciamentos_pj_9m, modal_json.emprestimos_microcredito_pj_9m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_10m, modal_json.emprestimos_giro_curto_pj_10m, modal_json.emprestimos_giro_rotativo_pj_10m, modal_json.financiamentos_pj_10m, modal_json.outrosfinanciamentos_pj_10m, modal_json.emprestimos_microcredito_pj_10m) > 0::double precision)::integer + (GREATEST(modal_json.emprestimos_giro_longo_pj_11m, modal_json.emprestimos_giro_curto_pj_11m, modal_json.emprestimos_giro_rotativo_pj_11m, modal_json.financiamentos_pj_11m, modal_json.outrosfinanciamentos_pj_11m, modal_json.emprestimos_microcredito_pj_11m) > 0::double precision)::integer
        END AS count_capital_giro_c_fin_c_micro
   FROM lucas_leal.mv_bizcredit_checklist t1
     LEFT JOIN ( SELECT t6.direct_prospect_id,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text), 0::double precision) AS emprestimos_giro_longo_pj_curr,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text), 0::double precision) AS emprestimos_giro_longo_pj_1m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text), 0::double precision) AS emprestimos_giro_longo_pj_2m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text), 0::double precision) AS emprestimos_giro_longo_pj_3m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text), 0::double precision) AS emprestimos_giro_longo_pj_4m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text), 0::double precision) AS emprestimos_giro_longo_pj_5m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text), 0::double precision) AS emprestimos_giro_longo_pj_6m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text), 0::double precision) AS emprestimos_giro_longo_pj_7m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text), 0::double precision) AS emprestimos_giro_longo_pj_8m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text), 0::double precision) AS emprestimos_giro_longo_pj_9m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text), 0::double precision) AS emprestimos_giro_longo_pj_10m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text), 0::double precision) AS emprestimos_giro_longo_pj_11m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text), 0::double precision) AS ever_emprestimos_giro_longo_pj,
            COALESCE(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo vencimento superior 365 d'::text), 0::bigint) AS count_emprestimos_giro_longo_pj,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text), 0::double precision) AS emprestimos_giro_curto_pj_curr,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text), 0::double precision) AS emprestimos_giro_curto_pj_1m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text), 0::double precision) AS emprestimos_giro_curto_pj_2m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text), 0::double precision) AS emprestimos_giro_curto_pj_3m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text), 0::double precision) AS emprestimos_giro_curto_pj_4m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text), 0::double precision) AS emprestimos_giro_curto_pj_5m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text), 0::double precision) AS emprestimos_giro_curto_pj_6m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text), 0::double precision) AS emprestimos_giro_curto_pj_7m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text), 0::double precision) AS emprestimos_giro_curto_pj_8m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text), 0::double precision) AS emprestimos_giro_curto_pj_9m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text), 0::double precision) AS emprestimos_giro_curto_pj_10m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text), 0::double precision) AS emprestimos_giro_curto_pj_11m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text), 0::double precision) AS ever_emprestimos_giro_curto_pj,
            COALESCE(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de Giro com prazo de vencimento até 365 d'::text), 0::bigint) AS count_emprestimos_giro_curto_pj,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text), 0::double precision) AS emprestimos_giro_rotativo_pj_curr,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text), 0::double precision) AS emprestimos_giro_rotativo_pj_1m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text), 0::double precision) AS emprestimos_giro_rotativo_pj_2m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text), 0::double precision) AS emprestimos_giro_rotativo_pj_3m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text), 0::double precision) AS emprestimos_giro_rotativo_pj_4m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text), 0::double precision) AS emprestimos_giro_rotativo_pj_5m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text), 0::double precision) AS emprestimos_giro_rotativo_pj_6m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text), 0::double precision) AS emprestimos_giro_rotativo_pj_7m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text), 0::double precision) AS emprestimos_giro_rotativo_pj_8m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text), 0::double precision) AS emprestimos_giro_rotativo_pj_9m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text), 0::double precision) AS emprestimos_giro_rotativo_pj_10m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text), 0::double precision) AS emprestimos_giro_rotativo_pj_11m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text), 0::double precision) AS ever_emprestimos_giro_rotativo_pj,
            COALESCE(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Capital de giro com teto rotativo'::text), 0::bigint) AS count_emprestimos_giro_rotativo_pj,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Financiamentos'::text), 0::double precision) AS financiamentos_pj_curr,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Financiamentos'::text), 0::double precision) AS financiamentos_pj_1m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Financiamentos'::text), 0::double precision) AS financiamentos_pj_2m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Financiamentos'::text), 0::double precision) AS financiamentos_pj_3m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Financiamentos'::text), 0::double precision) AS financiamentos_pj_4m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Financiamentos'::text), 0::double precision) AS financiamentos_pj_5m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Financiamentos'::text), 0::double precision) AS financiamentos_pj_6m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Financiamentos'::text), 0::double precision) AS financiamentos_pj_7m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Financiamentos'::text), 0::double precision) AS financiamentos_pj_8m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Financiamentos'::text), 0::double precision) AS financiamentos_pj_9m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Financiamentos'::text), 0::double precision) AS financiamentos_pj_10m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Financiamentos'::text), 0::double precision) AS financiamentos_pj_11m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Financiamentos'::text), 0::double precision) AS ever_financiamentos_pj,
            COALESCE(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND t6.lv1 = 'Financiamentos'::text), 0::bigint) AS count_financiamentos_pj,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))), 0::double precision) AS outrosfinanciamentos_pj_curr,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))), 0::double precision) AS outrosfinanciamentos_pj_1m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))), 0::double precision) AS outrosfinanciamentos_pj_2m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))), 0::double precision) AS outrosfinanciamentos_pj_3m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))), 0::double precision) AS outrosfinanciamentos_pj_4m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))), 0::double precision) AS outrosfinanciamentos_pj_5m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))), 0::double precision) AS outrosfinanciamentos_pj_6m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))), 0::double precision) AS outrosfinanciamentos_pj_7m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))), 0::double precision) AS outrosfinanciamentos_pj_8m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))), 0::double precision) AS outrosfinanciamentos_pj_9m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))), 0::double precision) AS outrosfinanciamentos_pj_10m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))), 0::double precision) AS outrosfinanciamentos_pj_11m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))), 0::double precision) AS ever_outrosfinanciamentos_pj,
            COALESCE(count(DISTINCT t6.to_date) FILTER (WHERE t6.valor > 0::double precision AND t6.to_date <= t6.data_referencia AND (t6.lv1 = ANY (ARRAY['Financiamentos Rurais'::text, 'Financiamentos à Importação'::text, 'Financiamentos à Exportação'::text, 'Financiamentos Imobiliários'::text]))), 0::bigint) AS count_outrosfinanciamentos_pj,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text), 0::double precision) AS financiamento_veiculo_pj_curr,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text), 0::double precision) AS financiamento_veiculo_pj_1m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text), 0::double precision) AS financiamento_veiculo_pj_2m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text), 0::double precision) AS financiamento_veiculo_pj_3m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text), 0::double precision) AS financiamento_veiculo_pj_4m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text), 0::double precision) AS financiamento_veiculo_pj_5m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text), 0::double precision) AS financiamento_veiculo_pj_6m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text), 0::double precision) AS financiamento_veiculo_pj_7m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text), 0::double precision) AS financiamento_veiculo_pj_8m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text), 0::double precision) AS financiamento_veiculo_pj_9m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text), 0::double precision) AS financiamento_veiculo_pj_10m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text), 0::double precision) AS financiamento_veiculo_pj_11m,
            COALESCE(max(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text), 0::double precision) AS max_financiamento_veiculo_pj,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text), 0::double precision) AS ever_financiamento_veiculo_pj,
            COALESCE(count(*) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv2 = 'Aquisição de Bens - Veículos Automotores'::text), 0::bigint) AS count_financiamento_veiculo_pj,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text), 0::double precision) AS emprestimos_microcredito_pj_curr,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '1 mon'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text), 0::double precision) AS emprestimos_microcredito_pj_1m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '2 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text), 0::double precision) AS emprestimos_microcredito_pj_2m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '3 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text), 0::double precision) AS emprestimos_microcredito_pj_3m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '4 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text), 0::double precision) AS emprestimos_microcredito_pj_4m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '5 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text), 0::double precision) AS emprestimos_microcredito_pj_5m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '6 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text), 0::double precision) AS emprestimos_microcredito_pj_6m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '7 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text), 0::double precision) AS emprestimos_microcredito_pj_7m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '8 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text), 0::double precision) AS emprestimos_microcredito_pj_8m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '9 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text), 0::double precision) AS emprestimos_microcredito_pj_9m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '10 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text), 0::double precision) AS emprestimos_microcredito_pj_10m,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date = (t6.data_referencia - '11 mons'::interval) AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text), 0::double precision) AS emprestimos_microcredito_pj_11m,
            COALESCE(max(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text), 0::double precision) AS max_emprestimos_microcredito_pj,
            COALESCE(sum(t6.valor) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text), 0::double precision) AS ever_emprestimos_microcredito_pj,
            COALESCE(count(*) FILTER (WHERE t6.to_date <= t6.data_referencia AND t6.lv1 = 'Empréstimos'::text AND t6.lv2 = 'Microcrédito'::text), 0::bigint) AS count_emprestimos_microcredito_pj
           FROM ( SELECT t5.direct_prospect_id,
                    t5.divida_atual_pj,
                    regexp_replace(t5.lv1, '\d{2}\s-\s'::text, ''::text) AS lv1,
                    replace(replace(replace(replace(regexp_replace(t5.lv2, '\d{4}\s-\s'::text, ''::text), '  '::text, ' - '::text), ' -- '::text, ' - '::text), ' - '::text, ' - '::text), 'interfinanceiros'::text, 'Interfinanceiros'::text) AS lv2,
                    to_date(t5.data, 'mm/yyyy'::text) AS to_date,
                        CASE
                            WHEN t5.valor IS NULL THEN 0::double precision
                            ELSE t5.valor
                        END AS valor,
                        CASE
                            WHEN t5.max_id IS NULL THEN
                            CASE
                                WHEN date_part('day'::text, t5.data_pedido) >= 16::double precision THEN to_date(to_char(t5.data_pedido - '1 mon'::interval, 'Mon/yy'::text), 'Mon/yy'::text)
                                ELSE to_date(to_char(t5.data_pedido - '2 mons'::interval, 'Mon/yy'::text), 'Mon/yy'::text)
                            END
                            ELSE first_value(to_date(t5.data, 'mm/yyyy'::text)) OVER (PARTITION BY t5.direct_prospect_id ORDER BY (to_date(t5.data, 'mm/yyyy'::text)) DESC)
                        END AS data_referencia
                   FROM ( SELECT t4.direct_prospect_id,
                            t4.data_pedido,
                            t4.data_doc,
                            t4.max_id,
                            t4.divida_atual_pj,
                            t4.lv1,
                            t4.nome AS lv2,
                            (jsonb_populate_recordset(NULL::meu2, t4.serie)).data AS data,
                            (jsonb_populate_recordset(NULL::meu2, t4.serie)).valor AS valor
                           FROM ( SELECT t3.direct_prospect_id,
                                    t3.data_pedido,
                                    t3.data_doc,
                                    t3.max_id,
CASE
 WHEN t3.data_doc <= (t3.data_pedido + '6 mons'::interval) THEN t3.divida_atual_pj
 ELSE NULL::double precision
END AS divida_atual_pj,
                                    t3.nome AS lv1,
                                    (jsonb_populate_recordset(NULL::meu, t3.detalhes)).nome AS nome,
                                    (jsonb_populate_recordset(NULL::meu, t3.detalhes)).serie AS serie
                                   FROM ( SELECT t2.direct_prospect_id,
    ((t2.data -> 'indicadores'::text) ->> 'dividaAtual'::text)::double precision AS divida_atual_pj,
    to_date((t2.data -> 'data_base'::text) ->> 'Data-Base'::text, 'mm/yyyy'::text) AS data_base,
    t2.data_pedido,
    t2.data_doc,
    t2.max_id,
    (jsonb_populate_recordset(NULL::lucas, t2.data #> '{por_modalidade}'::text[])).nome AS nome,
    (jsonb_populate_recordset(NULL::lucas, t2.data #> '{por_modalidade}'::text[])).detalhes AS detalhes
   FROM ( SELECT t1_1.direct_prospect_id,
      cc.data,
      t1_1.data_pedido,
      COALESCE(to_timestamp(regexp_replace(replace(cc.data ->> 'criado_em'::text, 'T'::text, ' '::text), '\..*'::text, ''::text), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc.data_coleta)::date AS data_doc,
      t1_1.max_id
     FROM credito_coleta cc
       JOIN ( SELECT dp.direct_prospect_id,
        max(COALESCE(lr.loan_date, lr.date_inserted::date)) AS data_pedido,
        max(dp.cnpj::text) AS cnpj,
        max(cc_1.id) FILTER (WHERE COALESCE(to_timestamp(regexp_replace(replace(cc_1.data ->> 'criado_em'::text, 'T'::text, ' '::text), '\..*'::text, ''::text), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc_1.data_coleta)::date <= COALESCE(lr.loan_date::timestamp without time zone, lr.date_inserted::date + '10 days'::interval)) AS max_id,
        min(cc_1.id) AS min_id
       FROM credito_coleta cc_1
         JOIN direct_prospects dp ON "left"(cc_1.documento::text, 8) = "left"(dp.cnpj::text, 8)
         JOIN offers o ON o.direct_prospect_id = dp.direct_prospect_id
         JOIN loan_requests lr ON lr.offer_id = o.offer_id
         JOIN lucas_leal.mv_bizcredit_checklist t ON t.lead_id = dp.direct_prospect_id
      WHERE cc_1.documento_tipo::text = 'CNPJ'::text AND cc_1.tipo::text = 'SCR'::text
      GROUP BY dp.direct_prospect_id) t1_1 ON cc.id = COALESCE(t1_1.max_id, t1_1.min_id)) t2) t3) t4) t5) t6
          GROUP BY t6.direct_prospect_id, t6.divida_atual_pj) modal_json ON modal_json.direct_prospect_id = t1.lead_id
     LEFT JOIN ( SELECT t1_1.direct_prospect_id,
            COALESCE(cc.data -> 'erro'::text, cc.data -> 'error'::text)::text AS thin_file
           FROM credito_coleta cc
             JOIN ( SELECT dp.direct_prospect_id,
                    max(COALESCE(lr.loan_date, lr.date_inserted::date)) AS data_pedido,
                    max(dp.cnpj::text) AS cnpj,
                    max(cc_1.id) FILTER (WHERE COALESCE(to_timestamp(regexp_replace(replace(cc_1.data ->> 'criado_em'::text, 'T'::text, ' '::text), '\..*'::text, ''::text), 'yyyy-mm-dd HH24:MI:SS:MS'::text), cc_1.data_coleta)::date <= COALESCE(lr.loan_date::timestamp without time zone, lr.date_inserted::date + '10 days'::interval)) AS max_id,
                    min(cc_1.id) AS min_id
                   FROM credito_coleta cc_1
                     JOIN direct_prospects dp ON "left"(cc_1.documento::text, 8) = "left"(dp.cnpj::text, 8)
                     JOIN offers o ON o.direct_prospect_id = dp.direct_prospect_id
                     JOIN loan_requests lr ON lr.offer_id = o.offer_id
                     JOIN lucas_leal.mv_bizcredit_checklist t ON t.lead_id = dp.direct_prospect_id
                  WHERE cc_1.documento_tipo::text = 'CNPJ'::text AND cc_1.tipo::text = 'SCR'::text
                  GROUP BY dp.direct_prospect_id) t1_1 ON cc.id = COALESCE(t1_1.max_id, t1_1.min_id)) modal_erro ON modal_erro.direct_prospect_id = t1.lead_id
WITH DATA;


CREATE MATERIALIZED VIEW lucas_leal.mv_bizcredit_materialidade_neoway
TABLESPACE pg_default
AS SELECT t1.lead_id,
    t1.indice_coleta,
    t1.tem_licenca_ambiental,
    t1.tem_aeronave,
    t1.count_inpi_marcas,
    t1.total_veiculos,
    t1.count_imoveis,
    t1.count_obras,
    t1.tem_exportacao,
    t1.count_pat
   FROM ( SELECT t.lead_id,
            row_number() OVER (PARTITION BY t.lead_id ORDER BY (cc.data_coleta::date) DESC) AS indice_coleta,
            ((((((cc.data -> '_metadata'::text) -> 'empresas'::text) -> '_metadata'::text) -> 'licencasAmbientais'::text) -> 'source'::text) IS NOT NULL)::integer AS tem_licenca_ambiental,
            ((((((cc.data -> '_metadata'::text) -> 'empresas'::text) -> '_metadata'::text) -> 'aeronaves'::text) -> 'source'::text) IS NOT NULL)::integer AS tem_aeronave,
            COALESCE(jsonb_array_length(cc.data -> 'inpiMarcas'::text), 0)::bigint AS count_inpi_marcas,
            COALESCE((cc.data -> 'detran'::text) ->> 'totalVeiculos'::text, '0'::text)::bigint AS total_veiculos,
            COALESCE(jsonb_array_length(cc.data -> 'imoveis'::text), 0)::bigint AS count_imoveis,
            COALESCE((cc.data -> 'totalObras'::text) ->> 'quantidadeArts'::text, '0'::text)::bigint AS count_obras,
            (COALESCE("position"(cc.data ->> 'indicadoresFinanceiros'::text, 'EXPORTACAO'::text), 0) > 0)::integer AS tem_exportacao,
            COALESCE(jsonb_array_length(cc.data -> 'pat'::text), 0)::bigint AS count_pat
           FROM credito_coleta cc
             JOIN direct_prospects dp ON "left"(dp.cnpj::text, 8) = "left"(cc.documento::text, 8)
             JOIN lucas_leal.mv_bizcredit_checklist t ON t.lead_id = dp.direct_prospect_id
          WHERE cc.tipo::text = 'Neoway'::text AND cc.data_coleta::date <= t.data_criacao) t1
  WHERE t1.indice_coleta = 1
WITH DATA;

	


create materialized view mv_bizcredit_irpf as

select 
	t.direct_prospect_id,
	regexp_replace(translate(retorno_irpf->'data'->'identificacao'->>'cpf','.-',''),'.*\s','') as cpf,
	(jsonb_array_elements(retorno_irpf->'data'->'bens_direitos'->'registros')->>'normalizado_codigo')::int as codigo_bens_direitos,
	(jsonb_array_elements(retorno_irpf->'data'->'bens_direitos'->'registros')->>'normalizado_valor_exercicio_atual')::numeric as valor_bens_direitos,
	jsonb_array_length(retorno_irpf->'data'->'dependentes')::int as count_dependentes
from data_science.carga_3_irpf p
join public.documents d on d.url = p.url
join public.direct_prospects dp on dp.client_id = d.client_id
join lucas_leal.mv_bizcredit_approval_tape t on t.direct_prospect_id = dp.direct_prospect_id and d.datetime_inserted::date between t.data_criacao::date - '10 days'::interval and t.data_criacao::date + '15 days'::interval 

select 
	t.direct_prospect_id,
	regexp_replace(translate(retorno_irpf->'data'->'identificacao'->>'cpf','.-',''),'.*\s','') as cpf,
	(jsonb_array_elements(retorno_irpf->'data'->'bens_direitos'->'registros')->>'normalizado_codigo')::int as codigo_bens_direitos,
	(jsonb_array_elements(retorno_irpf->'data'->'bens_direitos'->'registros')->>'normalizado_valor_exercicio_atual')::numeric as valor_bens_direitos,
	jsonb_array_length(retorno_irpf->'data'->'dependentes')::int as count_dependentes
from data_science.irpf_full_sem_duplicados p
join public.documents d on d.url = p.url
join public.direct_prospects dp on dp.client_id = d.client_id
join lucas_leal.mv_bizcredit_approval_tape t on t.direct_prospect_id = dp.direct_prospect_id and d.datetime_inserted::date between t.data_criacao::date - '10 days'::interval and t.data_criacao::date + '15 days'::interval 

select count(*)
from data_science.irpf_full_sem_duplicados p
join public.documents d on d.
where retorno_irpf ->> 'code' = '200'





CREATE MATERIALIZED VIEW lucas_leal.mv_bizcredit_consultas_serasa
TABLESPACE pg_default
AS SELECT t3.direct_prospect_id,
    count(*) FILTER (WHERE t3.consulta_proprio = 1) AS consulta_proprio,
    count(*) FILTER (WHERE t3.consulta_factoring = 1 AND t3.consulta_proprio = 0) AS consulta_factoring,
    count(*) FILTER (WHERE t3.consulta_seguradora = 1 AND GREATEST(t3.consulta_proprio, t3.consulta_factoring) = 0) AS consulta_seguradora,
    count(*) FILTER (WHERE t3.consulta_financeira = 1 AND GREATEST(t3.consulta_proprio, t3.consulta_factoring, t3.consulta_seguradora) = 0) AS consulta_financeira,
    count(*) FILTER (WHERE t3.consulta_cobranca = 1 AND GREATEST(t3.consulta_proprio, t3.consulta_factoring, t3.consulta_seguradora, t3.consulta_financeira) = 0) AS consulta_cobranca,
    count(*) FILTER (WHERE t3.consulta_provedor_dado = 1 AND GREATEST(t3.consulta_proprio, t3.consulta_factoring, t3.consulta_seguradora, t3.consulta_financeira, t3.consulta_cobranca) = 0) AS consulta_provedor_dado,
    count(*) FILTER (WHERE t3.consulta_fornecedores = 1 AND GREATEST(t3.consulta_proprio, t3.consulta_factoring, t3.consulta_seguradora, t3.consulta_financeira, t3.consulta_cobranca, t3.consulta_provedor_dado) = 0) AS consulta_fornecedores,
    count(*) FILTER (WHERE GREATEST(t3.consulta_proprio, t3.consulta_factoring, t3.consulta_seguradora, t3.consulta_financeira, t3.consulta_cobranca, t3.consulta_provedor_dado, t3.consulta_fornecedores) = 0) AS consulta_indefinido
   FROM ( SELECT t2.direct_prospect_id,
            t2.consultas,
                CASE
                    WHEN "position"(t2.consultas, 'INVEST DIR'::text) > 0 OR "position"(t2.consultas, 'CREDITORIO'::text) > 0 OR "position"(t2.consultas, 'FUNDO DE INVE'::text) > 0 OR "position"(t2.consultas, 'SECURITIZADORA'::text) > 0 OR "position"(t2.consultas, 'FACTORING'::text) > 0 OR "position"(t2.consultas, 'FOMENTO'::text) > 0 AND "position"(t2.consultas, 'AGENCIA'::text) = 0 OR "position"(t2.consultas, 'FIDC'::text) > 0 OR "position"(t2.consultas, 'NOVA S R M'::text) > 0 OR "position"(t2.consultas, 'RED SA'::text) > 0 OR "position"(t2.consultas, 'DEL MONTE SERVICOS'::text) > 0 OR "position"(t2.consultas, '123QRED'::text) > 0 OR "position"(t2.consultas, 'SERVICOS FINANCEIRO'::text) > 0 OR "position"(t2.consultas, 'ADIANTA PAGAMENTO '::text) > 0 OR "position"(t2.consultas, 'ASAAS GESTAO'::text) > 0 OR "position"(t2.consultas, 'BRA GESTORA '::text) > 0 OR "position"(t2.consultas, 'DUNAS SERVICOS FINANCEIROS'::text) > 0 OR "position"(t2.consultas, 'FEDERAL INVEST'::text) > 0 OR "position"(t2.consultas, 'HOOME CREDIT'::text) > 0 OR "position"(t2.consultas, 'INVISTA CREDITO'::text) > 0 OR "position"(t2.consultas, 'LIDERFAC TECNOLOGIA'::text) > 0 OR "position"(t2.consultas, 'INVEST DIREITOS'::text) > 0 OR "position"(t2.consultas, 'PLATAFORMA ANTECIPA'::text) > 0 OR "position"(t2.consultas, 'PREMIUM SERVICOS FINANCEIROS'::text) > 0 OR "position"(t2.consultas, 'PRIX EMPRESARIAL'::text) > 0 OR "position"(t2.consultas, 'TIRRENO'::text) > 0 OR "position"(t2.consultas, 'ADIANTA'::text) > 0 OR "position"(t2.consultas, 'VIA CAPITAL GESTAO'::text) > 0 THEN 1
                    ELSE 0
                END AS consulta_factoring,
                CASE
                    WHEN "position"(t2.consultas, 'SEGURADORA'::text) > 0 OR "position"(t2.consultas, 'AMIL ASSISTENCIA'::text) > 0 OR "position"(t2.consultas, 'CONSORCIOS'::text) > 0 OR "position"(t2.consultas, 'BARIGUI COMPANHIA HIPOTECARIA'::text) > 0 OR "position"(t2.consultas, 'BERKLEY INTERNATIONAL'::text) > 0 OR "position"(t2.consultas, 'BRADESCO AUTO '::text) > 0 OR "position"(t2.consultas, 'CESCE BRASIL'::text) > 0 OR "position"(t2.consultas, 'CIA DE ARRENDAMENTO MERCANTIL'::text) > 0 OR "position"(t2.consultas, 'CONSORCIO'::text) > 0 OR "position"(t2.consultas, 'EULER HERMES'::text) > 0 OR "position"(t2.consultas, 'SEGURO'::text) > 0 OR "position"(t2.consultas, 'PORTOSEG'::text) > 0 OR "position"(t2.consultas, 'RODOBENS ADMINISTRACAO'::text) > 0 OR "position"(t2.consultas, 'SAMETRADE OPERADORA'::text) > 0 OR "position"(t2.consultas, 'SUL AMERICA SEG'::text) > 0 OR "position"(t2.consultas, 'SULMED ASSISTENCIA'::text) > 0 OR "position"(t2.consultas, 'GARANTIA AFIANCADORA'::text) > 0 OR "position"(t2.consultas, 'ODONTOPREV'::text) > 0 OR "position"(t2.consultas, 'UNIVIDA'::text) > 0 OR "position"(t2.consultas, 'UNIMED'::text) > 0 THEN 1
                    ELSE 0
                END AS consulta_seguradora,
                CASE
                    WHEN "position"(t2.consultas, 'COBRANCA'::text) > 0 OR "position"(t2.consultas, 'ASSECAD BRASIL'::text) > 0 OR "position"(t2.consultas, 'CONVENIO CADASTRAL'::text) > 0 OR "position"(t2.consultas, 'INFOR-LINE'::text) > 0 OR "position"(t2.consultas, 'OFFICE SOLUCOES &'::text) > 0 THEN 1
                    ELSE 0
                END AS consulta_cobranca,
                CASE
                    WHEN "position"(t2.consultas, 'ASSERTIVA TECNOLOGIA'::text) > 0 OR "position"(t2.consultas, 'BANCODOC'::text) > 0 OR "position"(t2.consultas, 'BIG DATA SOLUCOES'::text) > 0 OR "position"(t2.consultas, 'BOMPARA TECNOLOGIA'::text) > 0 OR "position"(t2.consultas, 'CHEQUE-PRE'::text) > 0 OR "position"(t2.consultas, 'DIRECT SMART DATA'::text) > 0 OR "position"(t2.consultas, 'DUN & BRAD'::text) > 0 OR "position"(t2.consultas, 'FD DO BRASIL'::text) > 0 OR "position"(t2.consultas, 'OMNI'::text) > 0 OR "position"(t2.consultas, 'METADADOS'::text) > 0 OR "position"(t2.consultas, 'OBVIO BRASIL'::text) > 0 OR "position"(t2.consultas, 'CANAL DA INTERNET'::text) > 0 OR "position"(t2.consultas, 'CENIN CENTRO'::text) > 0 OR "position"(t2.consultas, 'CHECKLOG'::text) > 0 OR "position"(t2.consultas, 'COFACE BRASIL'::text) > 0 OR "position"(t2.consultas, 'MR MARQUES SERVICES'::text) > 0 OR "position"(t2.consultas, 'UPLEXIS'::text) > 0 THEN 1
                    ELSE 0
                END AS consulta_provedor_dado,
                CASE
                    WHEN "position"(t2.consultas, 'CREDITO'::text) > 0 OR "position"(t2.consultas, 'AGENCIA DE FOMENTO'::text) > 0 OR "position"(t2.consultas, 'AGENCIA FOMENTO'::text) > 0 OR "position"(t2.consultas, 'INVESTIMEN'::text) > 0 OR "position"(t2.consultas, 'BANCO '::text) > 0 OR "position"(t2.consultas, 'BANESE'::text) > 0 OR "position"(t2.consultas, 'BCO NACIONAL'::text) > 0 OR "position"(t2.consultas, 'CREDIT'::text) > 0 OR "position"(t2.consultas, 'CAIXA ESTADUAL S/A AGENCIA'::text) > 0 OR "position"(t2.consultas, 'CEF'::text) > 0 OR "position"(t2.consultas, 'MUTUO'::text) > 0 OR "position"(t2.consultas, 'CEN COOPERATIVAS CRED'::text) > 0 OR "position"(t2.consultas, 'CENTRO DE APOIO AOS PEQUENOS'::text) > 0 OR "position"(t2.consultas, 'UNICRED'::text) > 0 OR "position"(t2.consultas, 'COOP CENTRAL BASE DE SERV DE RESPON'::text) > 0 OR "position"(t2.consultas, 'COOPERATIVA CEN CRED'::text) > 0 OR "position"(t2.consultas, 'COOPERATIVA CRED'::text) > 0 OR "position"(t2.consultas, 'COOPERATIVA CREDITO'::text) > 0 OR "position"(t2.consultas, 'COOPERATIVA POUPANCA '::text) > 0 OR "position"(t2.consultas, 'CRED MUT'::text) > 0 OR "position"(t2.consultas, 'CORRESPONDENTE'::text) > 0 OR "position"(t2.consultas, 'CREDUNI'::text) > 0 OR "position"(t2.consultas, 'CONSORCIOS'::text) > 0 OR "position"(t2.consultas, 'SERV FINANCEIROS'::text) > 0 OR "position"(t2.consultas, 'SERVICOS FINANCEIROS'::text) > 0 OR "position"(t2.consultas, 'LENDING'::text) > 0 OR "position"(t2.consultas, 'EMPRESTIMO'::text) > 0 OR "position"(t2.consultas, 'NEXOOS'::text) > 0 OR "position"(t2.consultas, 'TUTU DIGITAL'::text) > 0 OR "position"(t2.consultas, 'CREDIUNI'::text) > 0 OR "position"(t2.consultas, 'CARTOES CRED'::text) > 0 OR "position"(t2.consultas, 'FINANC'::text) > 0 THEN 1
                    ELSE 0
                END AS consulta_financeira,
                CASE
                    WHEN "position"(t2.consultas, 'INFORMATICA'::text) > 0 OR "position"(t2.consultas, 'TELECOM'::text) > 0 OR "position"(t2.consultas, '3M DO BRASIL'::text) > 0 OR "position"(t2.consultas, 'ARTIGOS'::text) > 0 OR "position"(t2.consultas, 'DISTRIB'::text) > 0 OR "position"(t2.consultas, 'VEICU'::text) > 0 OR "position"(t2.consultas, 'CONFEC'::text) > 0 OR "position"(t2.consultas, 'CALCA'::text) > 0 OR "position"(t2.consultas, 'COMERC'::text) > 0 OR "position"(t2.consultas, 'HOTE'::text) > 0 OR "position"(t2.consultas, 'COMBU'::text) > 0 OR "position"(t2.consultas, 'AUTO'::text) > 0 OR "position"(t2.consultas, 'TRANSP'::text) > 0 OR "position"(t2.consultas, 'FERR'::text) > 0 OR "position"(t2.consultas, 'INDUSTR'::text) > 0 OR "position"(t2.consultas, 'EMBALA'::text) > 0 OR "position"(t2.consultas, 'PLAST'::text) > 0 OR "position"(t2.consultas, 'ADUBO'::text) > 0 OR "position"(t2.consultas, 'TECNOLOGIA'::text) > 0 OR "position"(t2.consultas, 'CONSULT'::text) > 0 OR "position"(t2.consultas, 'VIDR'::text) > 0 OR "position"(t2.consultas, 'AGRIC'::text) > 0 OR "position"(t2.consultas, 'PROD'::text) > 0 OR "position"(t2.consultas, 'S/A'::text) > 0 OR "position"(t2.consultas, 'ARAMADOS'::text) > 0 OR "position"(t2.consultas, 'COMEX'::text) > 0 OR "position"(t2.consultas, 'COM DE'::text) > 0 OR "position"(t2.consultas, 'ATACAD'::text) > 0 OR "position"(t2.consultas, 'EXPORT'::text) > 0 OR "position"(t2.consultas, 'MAQUINA'::text) > 0 OR "position"(t2.consultas, 'RECAUC'::text) > 0 OR "position"(t2.consultas, 'IMOV'::text) > 0 OR "position"(t2.consultas, 'LOCAC'::text) > 0 OR "position"(t2.consultas, 'METAL'::text) > 0 OR "position"(t2.consultas, 'ALUMI'::text) > 0 OR "position"(t2.consultas, 'ELETR'::text) > 0 OR "position"(t2.consultas, 'IMPORT'::text) > 0 OR "position"(t2.consultas, 'AMBEV'::text) > 0 OR "position"(t2.consultas, 'ASSOCI'::text) > 0 OR "position"(t2.consultas, 'TEXTIL'::text) > 0 OR "position"(t2.consultas, 'FARMA'::text) > 0 OR "position"(t2.consultas, 'CHUMBA'::text) > 0 OR "position"(t2.consultas, 'EQUIP'::text) > 0 OR "position"(t2.consultas, 'PECAS'::text) > 0 OR "position"(t2.consultas, 'COMPANHIA'::text) > 0 OR "position"(t2.consultas, 'MODA'::text) > 0 OR "position"(t2.consultas, 'SUPRI'::text) > 0 OR "position"(t2.consultas, 'ACELORMITTAL'::text) > 0 OR "position"(t2.consultas, 'ARMAZ'::text) > 0 OR "position"(t2.consultas, 'CIMENTO'::text) > 0 OR "position"(t2.consultas, 'SHOP'::text) > 0 OR "position"(t2.consultas, 'QUIM'::text) > 0 OR "position"(t2.consultas, 'MEDIC'::text) > 0 OR "position"(t2.consultas, 'EMPREE'::text) > 0 OR "position"(t2.consultas, 'CARNE'::text) > 0 OR "position"(t2.consultas, 'ALIMENT'::text) > 0 OR "position"(t2.consultas, '& CIA'::text) > 0 OR "position"(t2.consultas, 'IND E COM'::text) > 0 OR "position"(t2.consultas, 'ETIQUE'::text) > 0 OR "position"(t2.consultas, 'BIGCARG'::text) > 0 OR "position"(t2.consultas, 'BIMBO'::text) > 0 OR "position"(t2.consultas, 'ALARM'::text) > 0 OR "position"(t2.consultas, 'IMOB'::text) > 0 OR "position"(t2.consultas, 'DIESEL'::text) > 0 OR "position"(t2.consultas, 'LOGIST'::text) > 0 OR "position"(t2.consultas, 'BRF S/A'::text) > 0 OR "position"(t2.consultas, 'CARGAS'::text) > 0 OR "position"(t2.consultas, 'ARQUIT'::text) > 0 OR "position"(t2.consultas, 'CONSTRU'::text) > 0 OR "position"(t2.consultas, 'CABOS'::text) > 0 OR "position"(t2.consultas, 'PNEU'::text) > 0 OR "position"(t2.consultas, 'LOJISTA'::text) > 0 OR "position"(t2.consultas, 'TRANSMI'::text) > 0 OR "position"(t2.consultas, 'LUBRIFIC'::text) > 0 OR "position"(t2.consultas, 'CARTONA'::text) > 0 OR "position"(t2.consultas, 'CASA ALADIM'::text) > 0 OR "position"(t2.consultas, 'TINT'::text) > 0 OR "position"(t2.consultas, 'COMPENSADOS'::text) > 0 OR "position"(t2.consultas, 'HIDRA'::text) > 0 OR "position"(t2.consultas, 'CERAMICA'::text) > 0 OR "position"(t2.consultas, 'AGRO'::text) > 0 OR "position"(t2.consultas, 'CERVEJ'::text) > 0 OR "position"(t2.consultas, 'FABRIL'::text) > 0 OR "position"(t2.consultas, 'IND COM'::text) > 0 OR "position"(t2.consultas, 'CLARO SA'::text) > 0 OR "position"(t2.consultas, 'COLCHO'::text) > 0 OR "position"(t2.consultas, 'GRAF'::text) > 0 OR "position"(t2.consultas, 'COM E IND'::text) > 0 OR "position"(t2.consultas, 'COML'::text) > 0 OR "position"(t2.consultas, 'COMPRE FACIL'::text) > 0 OR "position"(t2.consultas, 'SOFTWARE'::text) > 0 OR "position"(t2.consultas, 'CONCRET'::text) > 0 OR "position"(t2.consultas, 'FABRIC'::text) > 0 OR "position"(t2.consultas, 'CONDOM'::text) > 0 OR "position"(t2.consultas, 'PROJET'::text) > 0 OR "position"(t2.consultas, 'CROMAG'::text) > 0 OR "position"(t2.consultas, 'DDTOTAL'::text) > 0 OR "position"(t2.consultas, 'CONDICI'::text) > 0 OR "position"(t2.consultas, 'DHL WORLD'::text) > 0 OR "position"(t2.consultas, 'MALHA'::text) > 0 OR "position"(t2.consultas, 'DURATEX'::text) > 0 OR "position"(t2.consultas, 'MOBILI'::text) > 0 OR "position"(t2.consultas, 'MATER'::text) > 0 OR "position"(t2.consultas, 'BOMBAS'::text) > 0 OR "position"(t2.consultas, 'EDITOR'::text) > 0 OR "position"(t2.consultas, 'ELASTIC'::text) > 0 OR "position"(t2.consultas, 'PORCELAN'::text) > 0 OR "position"(t2.consultas, 'ENERG'::text) > 0 OR "position"(t2.consultas, 'ESTOF'::text) > 0 OR "position"(t2.consultas, 'ESTRUTURA'::text) > 0 OR "position"(t2.consultas, 'PRINT'::text) > 0 OR "position"(t2.consultas, 'TURISM'::text) > 0 OR "position"(t2.consultas, 'COMUNIC'::text) > 0 OR "position"(t2.consultas, 'FERTILI'::text) > 0 OR "position"(t2.consultas, 'TECIDOS'::text) > 0 OR "position"(t2.consultas, 'NUTRIC'::text) > 0 OR "position"(t2.consultas, 'FORMAS'::text) > 0 OR "position"(t2.consultas, 'PETROL'::text) > 0 OR "position"(t2.consultas, 'MOTO'::text) > 0 OR "position"(t2.consultas, 'FRIGO'::text) > 0 OR "position"(t2.consultas, 'FUNDICAO'::text) > 0 OR "position"(t2.consultas, 'JOIA'::text) > 0 OR "position"(t2.consultas, 'ELECTR'::text) > 0 OR "position"(t2.consultas, 'SORVETE'::text) > 0 OR "position"(t2.consultas, 'GERDAU'::text) > 0 OR "position"(t2.consultas, 'PROVEDOR'::text) > 0 OR "position"(t2.consultas, 'GOOGLE'::text) > 0 OR "position"(t2.consultas, 'ADESIVO'::text) > 0 OR "position"(t2.consultas, 'TREINA'::text) > 0 OR "position"(t2.consultas, 'IMUNIZ'::text) > 0 OR "position"(t2.consultas, 'COMPUT'::text) > 0 OR "position"(t2.consultas, 'INNOVA'::text) > 0 OR "position"(t2.consultas, 'ENSIN'::text) > 0 OR "position"(t2.consultas, 'COM IMP MAT'::text) > 0 OR "position"(t2.consultas, 'MOVEIS'::text) > 0 OR "position"(t2.consultas, 'ARTEF'::text) > 0 OR "position"(t2.consultas, 'BRINDE'::text) > 0 OR "position"(t2.consultas, 'COMPONENTES'::text) > 0 OR "position"(t2.consultas, 'LABOR'::text) > 0 OR "position"(t2.consultas, 'LATICI'::text) > 0 OR "position"(t2.consultas, 'LOCALIZA'::text) > 0 OR "position"(t2.consultas, 'GERADORES'::text) > 0 OR "position"(t2.consultas, 'BORR'::text) > 0 OR "position"(t2.consultas, 'ACESSOR'::text) > 0 OR "position"(t2.consultas, 'VESTUA'::text) > 0 OR "position"(t2.consultas, 'MADEIR'::text) > 0 OR "position"(t2.consultas, 'CONSTR'::text) > 0 OR "position"(t2.consultas, 'MERCAD'::text) > 0 OR "position"(t2.consultas, 'MECANICA'::text) > 0 OR "position"(t2.consultas, 'SUCO'::text) > 0 OR "position"(t2.consultas, 'GASTRO'::text) > 0 OR "position"(t2.consultas, 'ENGEN'::text) > 0 OR "position"(t2.consultas, 'COPIADO'::text) > 0 OR "position"(t2.consultas, 'MOLAS'::text) > 0 OR "position"(t2.consultas, 'CONTABILI'::text) > 0 OR "position"(t2.consultas, 'COMEST'::text) > 0 OR "position"(t2.consultas, 'REFRI'::text) > 0 OR "position"(t2.consultas, 'SEMENTES'::text) > 0 OR "position"(t2.consultas, 'ROLAMEN'::text) > 0 OR "position"(t2.consultas, 'PESCADO'::text) > 0 OR "position"(t2.consultas, 'PEPSICO'::text) > 0 OR "position"(t2.consultas, 'POSTO'::text) > 0 OR "position"(t2.consultas, 'RESINAS'::text) > 0 OR "position"(t2.consultas, 'MAGAZINE'::text) > 0 OR "position"(t2.consultas, 'DOCES'::text) > 0 OR "position"(t2.consultas, 'RADIO'::text) > 0 OR "position"(t2.consultas, 'CELUL'::text) > 0 OR "position"(t2.consultas, 'ROBERT BOSH'::text) > 0 OR "position"(t2.consultas, 'SANITARIOS'::text) > 0 OR "position"(t2.consultas, 'RODOVIAR'::text) > 0 OR "position"(t2.consultas, 'OPTIC'::text) > 0 OR "position"(t2.consultas, 'CAMINHO'::text) > 0 OR "position"(t2.consultas, 'SOUZA CRUZ'::text) > 0 OR "position"(t2.consultas, 'TAMBASA'::text) > 0 OR "position"(t2.consultas, 'TECELAGEM'::text) > 0 OR "position"(t2.consultas, 'TECNIC'::text) > 0 OR "position"(t2.consultas, 'SISTEMAS'::text) > 0 OR "position"(t2.consultas, 'TOTVS'::text) > 0 OR "position"(t2.consultas, 'REFRES'::text) > 0 OR "position"(t2.consultas, 'UNIVEN HEALTHCARE'::text) > 0 OR "position"(t2.consultas, 'MARMOR'::text) > 0 OR "position"(t2.consultas, 'AROMA'::text) > 0 OR "position"(t2.consultas, 'VINICO'::text) > 0 OR "position"(t2.consultas, 'WHIRPOOL'::text) > 0 OR "position"(t2.consultas, 'OTICA'::text) > 0 OR "position"(t2.consultas, 'ARTES'::text) > 0 OR "position"(t2.consultas, 'ARGAMASSA'::text) > 0 OR "position"(t2.consultas, 'IMPRESSAO'::text) > 0 OR "position"(t2.consultas, 'VIAGENS'::text) > 0 OR "position"(t2.consultas, 'FABER CATELL'::text) > 0 OR "position"(t2.consultas, 'INOXI'::text) > 0 OR "position"(t2.consultas, 'BARBA'::text) > 0 OR "position"(t2.consultas, 'COSMETIC'::text) > 0 OR "position"(t2.consultas, 'CACHORRO'::text) > 0 OR "position"(t2.consultas, 'BR MALLS'::text) > 0 OR "position"(t2.consultas, 'CONTABIL'::text) > 0 OR "position"(t2.consultas, 'COMPONENTS'::text) > 0 OR "position"(t2.consultas, 'CEMEAP CEN MED'::text) > 0 OR "position"(t2.consultas, 'DEBUTANTES'::text) > 0 OR "position"(t2.consultas, 'CESTA BASICA'::text) > 0 OR "position"(t2.consultas, 'MEIOS'::text) > 0 OR "position"(t2.consultas, 'CIMCAL LTDA'::text) > 0 OR "position"(t2.consultas, 'COLORCON'::text) > 0 OR "position"(t2.consultas, 'COLSON'::text) > 0 OR "position"(t2.consultas, 'PROPAGANDA'::text) > 0 OR "position"(t2.consultas, 'CONCENTRADOS'::text) > 0 OR "position"(t2.consultas, 'CONVES WEB'::text) > 0 OR "position"(t2.consultas, 'CAFE'::text) > 0 OR "position"(t2.consultas, 'COOPERVISION'::text) > 0 OR "position"(t2.consultas, 'CORA PAGAMENTOS'::text) > 0 OR "position"(t2.consultas, 'COMPONENTE'::text) > 0 OR "position"(t2.consultas, 'DENTAL'::text) > 0 OR "position"(t2.consultas, 'ENDUTEX BRASIL'::text) > 0 OR "position"(t2.consultas, 'ENERBRAX'::text) > 0 OR "position"(t2.consultas, 'ENGARRAFAMENTO'::text) > 0 OR "position"(t2.consultas, 'PERFUMARIA'::text) > 0 OR "position"(t2.consultas, 'VAREJISTA'::text) > 0 OR "position"(t2.consultas, 'BATTERIES'::text) > 0 OR "position"(t2.consultas, 'GSI CREOS'::text) > 0 OR "position"(t2.consultas, 'HORTI'::text) > 0 OR "position"(t2.consultas, 'I M P LTDA'::text) > 0 OR "position"(t2.consultas, 'ESPORTE'::text) > 0 OR "position"(t2.consultas, 'INGRAM'::text) > 0 OR "position"(t2.consultas, 'INTERNATIONAL PAPER'::text) > 0 OR "position"(t2.consultas, 'INSUMOS'::text) > 0 OR "position"(t2.consultas, 'KNAUF DO BRASIL'::text) > 0 OR "position"(t2.consultas, 'LAVANDERIA'::text) > 0 OR "position"(t2.consultas, 'LANCHO'::text) > 0 OR "position"(t2.consultas, 'LOSINOX'::text) > 0 OR "position"(t2.consultas, 'ILUMINA'::text) > 0 OR "position"(t2.consultas, 'REVESTIMENTO'::text) > 0 OR "position"(t2.consultas, 'FRANQUIA'::text) > 0 OR "position"(t2.consultas, 'PAPEIS'::text) > 0 OR "position"(t2.consultas, 'MERCANTIL MOR'::text) > 0 OR "position"(t2.consultas, 'JEANS'::text) > 0 OR "position"(t2.consultas, 'MINERACAO'::text) > 0 OR "position"(t2.consultas, 'FESTA'::text) > 0 OR "position"(t2.consultas, 'LIXO'::text) > 0 OR "position"(t2.consultas, 'NEOPRENE'::text) > 0 OR "position"(t2.consultas, 'NESTLE'::text) > 0 OR "position"(t2.consultas, 'MAT PARA'::text) > 0 OR "position"(t2.consultas, 'PAO DE'::text) > 0 OR "position"(t2.consultas, 'EDUCATION'::text) > 0 OR "position"(t2.consultas, 'POUSADA'::text) > 0 OR "position"(t2.consultas, 'GRAPH'::text) > 0 OR "position"(t2.consultas, 'ENCOMENDAS'::text) > 0 OR "position"(t2.consultas, 'SPORTS'::text) > 0 OR "position"(t2.consultas, 'RANDSTAD'::text) > 0 OR "position"(t2.consultas, 'RAPPI'::text) > 0 OR "position"(t2.consultas, 'RECAPAGE'::text) > 0 OR "position"(t2.consultas, 'FOOD'::text) > 0 OR "position"(t2.consultas, 'NOBREAK'::text) > 0 OR "position"(t2.consultas, 'ROBERT BOSCH'::text) > 0 OR "position"(t2.consultas, 'EVENTOS'::text) > 0 OR "position"(t2.consultas, 'RUSSER BRASIL'::text) > 0 OR "position"(t2.consultas, 'OPERADOR'::text) > 0 OR "position"(t2.consultas, 'GAS'::text) > 0 OR "position"(t2.consultas, 'FIOS'::text) > 0 OR "position"(t2.consultas, 'ANTENAS'::text) > 0 OR "position"(t2.consultas, 'PISOS'::text) > 0 OR "position"(t2.consultas, 'SARTORIUS'::text) > 0 OR "position"(t2.consultas, 'COMPON ELET'::text) > 0 OR "position"(t2.consultas, 'SODEXHO'::text) > 0 OR "position"(t2.consultas, 'LIMPE'::text) > 0 OR "position"(t2.consultas, 'SOMAFERTIL'::text) > 0 OR "position"(t2.consultas, 'COMPRESS'::text) > 0 OR "position"(t2.consultas, 'MOLDADOS'::text) > 0 OR "position"(t2.consultas, 'CARTUCHOS'::text) > 0 OR "position"(t2.consultas, 'TENCOREALTY'::text) > 0 OR "position"(t2.consultas, 'TRIVALE'::text) > 0 OR "position"(t2.consultas, 'UNIFORT LTDA'::text) > 0 OR "position"(t2.consultas, 'UNISYS'::text) > 0 OR "position"(t2.consultas, 'UNIVAR'::text) > 0 OR "position"(t2.consultas, 'USECORP LTDA'::text) > 0 OR "position"(t2.consultas, 'MANGUEIRAS'::text) > 0 OR "position"(t2.consultas, 'VELLO DIGITAL'::text) > 0 OR "position"(t2.consultas, 'UTILIDADES'::text) > 0 OR "position"(t2.consultas, 'VIAPARK'::text) > 0 OR "position"(t2.consultas, 'DECORA'::text) > 0 OR "position"(t2.consultas, 'SAPATARIA'::text) > 0 THEN 1
                    ELSE 0
                END AS consulta_fornecedores,
                CASE
                    WHEN "position"(t2.razao_social, t2.consultas) > 0 THEN 1
                    ELSE 0
                END AS consulta_proprio
           FROM ( SELECT t1.direct_prospect_id,
                    ((cc.data -> 'consulta_empresas'::text) -> jsonb_object_keys(cc.data -> 'consulta_empresas'::text)) ->> 'nome'::text AS consultas,
                    t1.razao_social
                   FROM credito_coleta cc
                     JOIN ( SELECT dp.direct_prospect_id,
                            max(dp.name::text) AS razao_social,
                            max(dp.cnpj::text) AS cnpj,
                            max(dp.cpf::text) AS cpf,
                            max(cc_1.id) AS max_id
                           FROM credito_coleta cc_1
                             JOIN direct_prospects dp ON "left"(cc_1.documento::text, 8) = "left"(dp.cnpj::text, 8)
                             JOIN offers o ON o.direct_prospect_id = dp.direct_prospect_id
                             JOIN loan_requests lr ON lr.offer_id = o.offer_id
                             JOIN lucas_leal.mv_bizcredit_checklist t ON t.lead_id = dp.direct_prospect_id
                          WHERE to_date((cc_1.data -> 'datetime'::text) ->> 'data'::text, 'yyyymmdd'::text) <= COALESCE(lr.loan_date::timestamp without time zone, lr.date_inserted::date + '10 days'::interval) AND (cc_1.tipo::text = ANY (ARRAY['Serasa'::character varying::text, 'RelatoSocios'::character varying::text, 'RelatoBasico'::character varying::text]))
                          GROUP BY dp.direct_prospect_id) t1 ON cc.id = t1.max_id
                UNION ALL
                 SELECT t1.direct_prospect_id,
                    ((cc.data -> 'consulta_spc'::text) -> jsonb_object_keys(cc.data -> 'consulta_spc'::text)) ->> 'nome'::text AS consultas,
                    t1.razao_social
                   FROM credito_coleta cc
                     JOIN ( SELECT dp.direct_prospect_id,
                            max(dp.name::text) AS razao_social,
                            max(dp.cnpj::text) AS cnpj,
                            max(dp.cpf::text) AS cpf,
                            max(cc_1.id) AS max_id
                           FROM credito_coleta cc_1
                             JOIN direct_prospects dp ON "left"(cc_1.documento::text, 8) = "left"(dp.cnpj::text, 8)
                             JOIN offers o ON o.direct_prospect_id = dp.direct_prospect_id
                             JOIN loan_requests lr ON lr.offer_id = o.offer_id
                             JOIN lucas_leal.mv_bizcredit_checklist t ON t.lead_id = dp.direct_prospect_id
                          WHERE to_date((cc_1.data -> 'datetime'::text) ->> 'data'::text, 'yyyymmdd'::text) <= COALESCE(lr.loan_date::timestamp without time zone, lr.date_inserted::date + '10 days'::interval) AND (cc_1.tipo::text = ANY (ARRAY['Serasa'::character varying::text, 'RelatoSocios'::character varying::text, 'RelatoBasico'::character varying::text]))
                          GROUP BY dp.direct_prospect_id) t1 ON cc.id = t1.max_id) t2
          WHERE t2.consultas <> 'CREDITLOOP CORRESPONDENTE BANCARIO'::text AND t2.consultas <> ''::text) t3
  GROUP BY t3.direct_prospect_id
WITH DATA;



