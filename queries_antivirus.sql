-------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------NEOWAY---------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------

insert into lucas_leal.t_neoway_pj_choose_report_full 
(select dp.direct_prospect_id,
    	cc.id,
		row_number() over (partition by dp.direct_prospect_id order by cc.data_coleta desc) as indice_ultimo_antes_data_referencia
	from credito_coleta cc
	join direct_prospects dp ON "left"(cc.documento::text, 8) = "left"(dp.cnpj::text, 8)
	join lucas_leal.v_funil_completo_sem_duplicacao vfcsd on vfcsd.direct_prospect_id = dp.direct_prospect_id 
    left join offers o on o.offer_id = vfcsd.offer_id 
    left join loan_requests lr on lr.loan_request_id = vfcsd.loan_request_id
	left join (select distinct direct_prospect_id from lucas_leal.t_neoway_pj_choose_report_full) t1 on t1.direct_prospect_id = dp.direct_prospect_id 
	where cc.documento_tipo::text = 'CNPJ'::text 
		and cc.tipo::text = 'Neoway'
		and cc.data_coleta::date <= coalesce(lr.loan_date,coalesce(lr.date_inserted,o.date_inserted,dp.opt_in_date)::date + interval '10 days')::date
		and t1.direct_prospect_id is null
	)

insert into lucas_leal.t_neoway_pj_empresas_coligadas_full
(select 
	t1.direct_prospect_id,
	t1.id as coleta_id,
	coligadas->>'uf' as uf,
	coligadas->>'cnae' as cnae,
	coligadas->>'cnpj' as cnpj,
	coligadas->>'municipio' as municipio,
	coligadas->>'codigoCnae' as codigoCnae,
	coligadas->>'razaoSocial' as razaoSocial,
	(coligadas->>'dataAbertura')::date as dataAbertura
from credito_coleta cc,
	jsonb_array_elements(cc.data->'empresasColigadas') as coligadas,
	lucas_leal.t_neoway_pj_choose_report_full t1
left join (select distinct 
		direct_prospect_id 
	from lucas_leal.t_neoway_pj_empresas_coligadas_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and t1.id = cc.id
	and t2.direct_prospect_id is null
	)

	
insert into lucas_leal.t_neoway_pj_socios_full
(select 
	t1.direct_prospect_id,
	t1.id as coleta_id,
	socios->>'nome' as nome,
	socios->>'falecido' as falecido,
	socios->>'nivelPep' as nivelPep,
	socios->>'documento' as documento,
	socios->>'qualificacao' as qualificacao,
	(socios->>'participacaoSocietaria')::numeric as participacaoSocietaria
from credito_coleta cc,
	jsonb_array_elements(cc.data->'socios') as socios,
	lucas_leal.t_neoway_pj_choose_report_full t1
left join (select distinct 
		direct_prospect_id 
	from lucas_leal.t_neoway_pj_socios_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and t1.id = cc.id
	and t2.direct_prospect_id is null
	)
	
	
insert into lucas_leal.t_neoway_pf_choose_report_full
(select dp.direct_prospect_id,
    	cc.id,
    	cc.documento,
		row_number() over (partition by dp.direct_prospect_id,cc.documento order by cc.data_coleta desc) as indice_ultimo_antes_data_referencia
	from credito_coleta cc
	join public.loan_requests lr on lr.loan_request_id = cc.origem_id
	join public.offers o on o.offer_id = lr.offer_id 
	join public.direct_prospects dp on dp.client_id = o.client_id 
	join lucas_leal.v_funil_completo_sem_duplicacao vfcsd on vfcsd.direct_prospect_id = dp.direct_prospect_id 
	join public.offers o2 on o2.offer_id = vfcsd.offer_id 
	join public.loan_requests lr2 on lr2.loan_request_id = vfcsd.loan_request_id 
	left join (select distinct direct_prospect_id from lucas_leal.t_neoway_pf_choose_report_full) tspcrf on tspcrf.direct_prospect_id = dp.direct_prospect_id 
    where cc.documento_tipo::text = 'CPF'::text 
		and cc.tipo::text = 'Neoway'
		and cc.data_coleta::date <= coalesce(lr2.loan_date,coalesce(lr2.date_inserted,o2.date_inserted,dp.opt_in_date)::date + interval '10 days')::date
		and tspcrf.direct_prospect_id is null
	)
	
	
insert into lucas_leal.t_neoway_pf_identity_data_full 
(select 
	t1.direct_prospect_id,
	t1.documento,
	(cc.data->>'cpf') as cpf,
	(cc.data->>'pis') as pis,
	(cc.data->>'nome') as nome,
	(cc.data->>'sexo') as genero,
	(cc.data->'cafir'->>'areaTotal')::numeric as cafir_area_total,
	(cc.data->'cafir'->>'numeroImoveisTitular')::int as cafir_numero_imoveis_titular,
	(cc.data->'cafir'->>'numeroImoveisCondomino')::int as cafir_numero_imoveis_condominio,
	(cc.data->>'idade')::int as idade,
	(cc.data->>'nomeMae') as nome_mae,
	(cc.data->'endereco'->>'uf') as endereco_uf,
	(cc.data->'endereco'->>'cep') as endereco_cep,
	(cc.data->'endereco'->>'municipio') as endereco_municipio,
	(cc.data->>'falecido')::bool as falecido,
	(cc.data->>'situacaoCpf') as situacao_cpf,
	(cc.data->>'dataNascimento')::date as data_nascimento,
	(cc.data->>'cpfDataInscricao') as cpf_data_inscricao,
	(cc.data->>'falecidoConfirmado')::bool as falecido_confirmado,
	(cc.data->>'qtdVeiculosPesados')::int as qtd_veiculos_pesados,
	cc.data->'enderecoEmpregoRaisNovo'->> 'cnpj' as emprego_rais_cnpj,
	cc.data->'enderecoEmpregoRaisNovo'->> 'razaoSocial' as emprego_rais_razao_social,
	cc.data->'enderecoEmpregoRaisNovo'->> 'uf' as emprego_rais_uf,
	cc.data->'enderecoEmpregoRaisNovo'->> 'municipio' as emprego_rais_municipio,
	cc.data->'enderecoEmpregoRaisNovo'->> 'logradouro' as emprego_rais_logradouro,
	cc.data->'enderecoEmpregoRaisNovo'->> 'numero' as emprego_rais_numero,
	cc.data->'enderecoEmpregoRaisNovo'->> 'complemento' as emprego_rais_complemento,
	cc.data->'enderecoEmpregoRaisNovo'->> 'bairro' as emprego_rais_bairro,
	cc.data->'enderecoEmpregoRaisNovo'->> 'cep' as emprego_rais_cep,
	cc.data->'enderecoEmpregoRaisNovo'->> 'descricaoCnae' as emprego_rais_cnae,
	(cc.data->'enderecoEmpregoRaisNovo'->> 'EnderecoResidencial')::bool as emprego_rais_endereco_residencial,
	cc.data->'enderecoEmpregoRaisNovo'->> 'faixaFaturamento' as emprego_rais_faixa_faturamento,
	cc.data->'enderecoEmpregoRaisNovo'->> 'precisaoGeo' as emprego_rais_precisao_geo,
	(cc.data->'enderecoEmpregoRaisNovo'->> 'quantidadeFuncionarios')::bigint as emprego_rais_qtd_funcionarios,
	cc.data->'enderecoEmpregoRaisNovo'->> 'ramoAtividade' as emprego_rais_ramo_atividade,
	cc.data->'enderecoEmpregoRaisNovo'->> 'telefone' as emprego_rais_telefone
from credito_coleta cc
join lucas_leal.t_neoway_pf_choose_report_full t1 on t1.id = cc.id
left join lucas_leal.t_neoway_pf_identity_data_full t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and t2.direct_prospect_id is null
)


insert into lucas_leal.t_neoway_pf_telephone_data_full 
(select 
	t1.direct_prospect_id,
	t1.documento,
	(telefone::jsonb->>'numero') as telefone
from credito_coleta cc
join lucas_leal.t_neoway_pf_choose_report_full t1 on t1.id = cc.id
left join jsonb_array_elements(cc.data->'telefones') as telefone on true
left join (select distinct direct_prospect_id  from lucas_leal.t_neoway_pf_telephone_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and (telefone::jsonb->>'numero') is not null
	and t2.direct_prospect_id is null
)


insert into lucas_leal.t_neoway_pf_pariticipacao_societaria_data_full 
(select 
	t1.direct_prospect_id,
	t1.documento,
	(participacaosocietaria->>'cnpj') as participacao_societaria_cnpj,
	(participacaosocietaria->>'situacao') as participacao_societaria_situacao
from credito_coleta cc
join lucas_leal.t_neoway_pf_choose_report_full t1 on t1.id = cc.id
left join jsonb_array_elements(cc.data->'participacaoSocietaria') as participacaoSocietaria on true
left join (select direct_prospect_id from lucas_leal.t_neoway_pf_pariticipacao_societaria_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and (participacaosocietaria->>'cnpj') is not null
	and t2.direct_prospect_id is null
)


insert into lucas_leal.t_neoway_pf_historico_funcional_data_full 
(select 
	t1.direct_prospect_id,
	t1.documento,
	historicoFuncional->>'cnpj' as cnpj,
	historicoFuncional->>'razaoSocial' as razaoSocial,
	(historicoFuncional->>'dataAdmissao')::date as dataAdmissao,
	(historicoFuncional->>'dataDesligamento')::date as dataDesligamento,
	(historicoFuncional->>'numeroMesesEmpresa')::bigint as numeroMesesEmpresa	
from credito_coleta cc
join lucas_leal.t_neoway_pf_choose_report_full t1 on t1.id = cc.id
left join jsonb_array_elements(cc.data->'historicoFuncional') as historicoFuncional on true
left join (select distinct direct_prospect_id from lucas_leal.t_neoway_pf_historico_funcional_data_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and historicoFuncional->>'cnpj' is not null
	and t2.direct_prospect_id is null
)




-------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------SERASA---------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------

insert into lucas_leal.t_experian_coligadas_full
select t1.direct_prospect_id,
	((cc.data -> 'participacoes'::text) -> jsonb_object_keys(cc.data -> 'participacoes'::text)) ->> 'cnpj'::text AS coligadas_cnpj,
	((cc.data -> 'participacoes'::text) -> jsonb_object_keys(cc.data -> 'participacoes'::text)) ->> 'restricao'::text AS coligadas_restricao
from credito_coleta cc
join t_experian_choose_report_full t1 ON t1.id = cc.id
left join (select distinct direct_prospect_id from lucas_leal.t_experian_coligadas_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and t2.direct_prospect_id is null

	
insert into lucas_leal.t_experian_controle_adm_full
select t1.direct_prospect_id,
	((cc.data -> 'controle_adm'::text -> 'adms') -> jsonb_object_keys(cc.data -> 'controle_adm'::text -> 'adms')) ->> 'cpf'::text AS controle_adm_cpf,
	((cc.data -> 'controle_adm'::text -> 'adms') -> jsonb_object_keys(cc.data -> 'controle_adm'::text -> 'adms')) ->> 'pais'::text AS controle_adm_pais,
	((cc.data -> 'controle_adm'::text -> 'adms') -> jsonb_object_keys(cc.data -> 'controle_adm'::text -> 'adms')) ->> 'cargo'::text AS controle_adm_cargo,
	((cc.data -> 'controle_adm'::text -> 'adms') -> jsonb_object_keys(cc.data -> 'controle_adm'::text -> 'adms')) ->> 'vinculo'::text AS controle_adm_vinculo,
	((cc.data -> 'controle_adm'::text -> 'adms') -> jsonb_object_keys(cc.data -> 'controle_adm'::text -> 'adms')) ->> 'data_entrada'::text AS controle_adm_data_entrada,
	((cc.data -> 'controle_adm'::text -> 'adms') -> jsonb_object_keys(cc.data -> 'controle_adm'::text -> 'adms')) ->> 'estado_civil'::text AS controle_adm_estado_civil,
	((cc.data -> 'controle_adm'::text -> 'adms') -> jsonb_object_keys(cc.data -> 'controle_adm'::text -> 'adms')) ->> 'identificacao'::text AS controle_adm_identificacao
from public.credito_coleta cc
join lucas_leal.t_experian_choose_report_full t1 ON t1.id = cc.id
left join (select distinct direct_prospect_id from lucas_leal.t_experian_controle_adm_full) t2 on t2.direct_prospect_id = t1.direct_prospect_id 
where t1.indice_ultimo_antes_data_referencia = 1
	and t2.direct_prospect_id is null

-------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------SIGNERS---------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------

	
insert into lucas_leal.t_signers_data_full 
(select 
		dp.direct_prospect_id,
		lr.loan_request_id,
		coalesce(s.share_percentage,es.participacao,ser.share) as signer_share_percentage,
		to_char(age(dp.opt_in_date::date, s.date_of_birth::date), 'yyyy'::text)::integer AS signer_age,
		s.revenue::numeric as signer_revenue,
		s.gender::text::character varying(50) as signer_gender,
		s.guarantor as signer_is_avalista,
		coalesce(s.cpf,es.cpf,ser.cpf_socios) as signer_cpf,
		coalesce(s.cpf,es.cpf,ser.cpf_socios) = dp.cpf as signer_is_solicitante,
		s.civil_status as signer_civil_status,
		coalesce(s.name,es.nome,ser.nome) as signer_name,
		s.email as signer_email,
		s.profession as signer_profession,
		s.rg as signer_rg,
		s.issuing_body as signer_issuing_body,
		s.country_of_birth as signer_country_of_birth,
		s.city_of_birth as signer_city_of_birth,
		s.date_of_birth as signer_date_of_bith,
		sa.street as signer_street_address,
		sa."number" as signer_number_address,
		sa.complement as signer_complement_address,
		sa.zip_code as signer_zip_code_address,
		c."name" as signer_city_address,
		ss."name" as signer_state_address,
		sa.country as signer_country_address,
		sa.neighbourhood as signer_neighbourhood_address,
		s.sign_spouse as signer_sign_spouse,
		s.shareholder as signer_is_shareholder,
		s.administrator as signer_is_administrator,
		s.spouse->>'cpf' as signer_spouse_cpf,
		s.spouse->>'name' as signer_spouse_name,
		s.spouse->>'email' as signer_spouse_email,
		s.signer_id as signer_id,
		s.client_id as signer_client_id,
		es_info.qualificacao as signer_neoway_qualificao,
		es_info.nivel_pep as signer_neoway_nivel_pep,
		es_info.ultima_atualizacao::date as signer_neoway_ultima_atualizacao,
		es_info.falecido as siger_neoway_falecido,
		row_number() over (partition by dp.direct_prospect_id order by case when s.administrator then 1 else 2 end, s.date_of_birth) as indice_administrador,
		row_number() over (partition by dp.direct_prospect_id order by coalesce(s.share_percentage,es.participacao,ser.share,0) desc, s.date_of_birth) as indice_socio_majoritario,
		row_number() over (partition by dp.direct_prospect_id order by case coalesce(s.share_percentage,es.participacao,ser.share,0) when 0 then null else s.date_of_birth end, coalesce(s.share_percentage,es.participacao,ser.share,0) desc) as indice_socio_mais_velho
	from public.direct_prospects dp
	join data_science.v_funil_completo_sem_duplicacao vfcsd on vfcsd.direct_prospect_id = dp.direct_prospect_id 
	left join public.loan_requests lr on lr.loan_request_id = vfcsd.loan_request_id 
	left join public.loan_requests_signers lrs on lrs.loan_request_id = lr.loan_request_id
	left join public.signers s on s.signer_id = lrs.signer_id
	left join public.signer_addresses sa on sa.address_id = s.address_id
	left join public.cities c on c.city_id = sa.city_id
	left join public.states ss on ss.state_id = c.state_id
	left join (select
			es.cnpj,
			es.cpf,
			es.nome,
			es.participacao,
			row_number() over (partition by es.cnpj,es.cpf order by es.id) as indice
		from public.empresas_socios es
		left join (select distinct 
				dp.cnpj
			from public.signers s
			join public.direct_prospects dp on dp.client_id = s.client_id) as s on s.cnpj = es.cnpj
		where s.cnpj is null) es on es.cnpj = dp.cnpj and es.indice = 1
	left join (SELECT 
			t1.direct_prospect_id,
			t1.loan_request_id,
			t1.cnpj,
			t1.cpf,
		    ((cc.data -> 'acionistas'::text) -> jsonb_object_keys(cc.data -> 'acionistas'::text)) ->> 'cpf'::text AS cpf_socios,
			jsonb_object_keys(cc.data -> 'acionistas'::text) AS nome,
			(((cc.data -> 'acionistas'::text) -> jsonb_object_keys(cc.data -> 'acionistas'::text)) ->> 'percentual'::text)::numeric AS share
		FROM credito_coleta cc
		JOIN (SELECT dp.direct_prospect_id,
				max(lr.loan_request_id) as loan_request_id,
				max(dp.cnpj) as cnpj,
				max(dp.cpf) as cpf,
				max(cc.id) AS max_id
			FROM public.credito_coleta cc
			JOIN public.direct_prospects dp ON "left"(cc.documento::text, 8) = "left"(dp.cnpj::text, 8)
			join data_science.v_funil_completo_sem_duplicacao vfcsd on vfcsd.direct_prospect_id = dp.direct_prospect_id 
			left join public.loan_requests lr on lr.loan_request_id = vfcsd.loan_request_id 
			left join (select 
					cnpj,
					max(participacao) as max_part 
				from public.empresas_socios
				group by 1) es on es.cnpj = dp.cnpj
			left join public.loan_requests_signers lrs on lrs.loan_request_id = lr.loan_request_id
			left join public.signers s on s.signer_id = lrs.signer_id
			left join lucas_leal.t_signers_indicators_full t1 on t1.direct_prospect_id = dp.direct_prospect_id
			WHERE to_date((cc.data -> 'datetime'::text) ->> 'data'::text, 'yyyymmdd'::text) <= lr.loan_date 
				AND (cc.tipo::text = ANY (ARRAY['Serasa'::character varying, 'RelatoSocios'::character varying]::text[]))
				and es.cnpj is null
				and s.signer_id is null
				and t1.direct_prospect_id is null			
			GROUP BY 1) t1 ON cc.id = t1.max_id) ser on ser.direct_prospect_id = dp.direct_prospect_id
	left join (select
			cnpj,
			cpf,
			nome,
			nivel_pep,
			qualificacao,
			participacao,
			ultima_atualizacao,
			falecido,
			row_number() over (partition by cnpj,cpf order by id) as indice
		from public.empresas_socios
		where participacao > 0) es_info on es_info.cnpj = dp.cnpj and es_info.cpf = coalesce(s.cpf,es.cpf,ser.cpf_socios) and es_info.indice = 1
	left join lucas_leal.t_signers_indicators_full t1 on t1.direct_prospect_id = dp.direct_prospect_id
		where t1.direct_prospect_id is null
)
	
-------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------ADMIN---------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------


insert into lucas_leal.t_antifraude_cnpjs_relacionados_admin 
SELECT dp.direct_prospect_id,
	unnest(string_to_array(replace(replace(dp.cnpjs_relacionados::text, '{'::text, ''::text), '}'::text, ''::text), ','::text)) AS cnpjs_relacionados
FROM direct_prospects dp
left join (select distinct direct_prospect_id from lucas_leal.t_antifraude_cnpjs_relacionados_admin) t2 on t2.direct_prospect_id = dp.direct_prospect_id 
where dp.cnpjs_relacionados::text != '{}'::text
	and t2.direct_prospect_id is null

	
insert into lucas_leal.t_antifraude_cpfs_relacionados_admin 
SELECT dp.direct_prospect_id,
	unnest(string_to_array(replace(replace(dp.cpfs_relacionados::text, '{'::text, ''::text), '}'::text, ''::text), ','::text)) AS cpfs_relacionados
FROM direct_prospects dp
left join (select distinct direct_prospect_id from lucas_leal.t_antifraude_cpfs_relacionados_admin) t2 on t2.direct_prospect_id = dp.direct_prospect_id 
where dp.cpfs_relacionados::text != '{}'::text
	and t2.direct_prospect_id is null
	