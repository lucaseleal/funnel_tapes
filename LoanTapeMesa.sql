grant all privileges on table lucas_leal.loan_tape_mesa2 to rodrigo_pinho;
grant all privileges on table lucas_leal.loan_tape_mesa2 to giulia_killer;
grant all privileges on table lucas_leal.loan_tape_mesa2 to cristiano_rocha;

alter table lucas_leal.loan_tape_mesa rename to loan_tape_mesa2

drop table lucas_leal.loan_tape_mesa2;

select max(loan_contract_date)
from loan_tape_mesa2


create table lucas_leal.loan_tape_mesa as
--------------------------DIRETO
select 
	dp.direct_prospect_id as lead_ID,
	dp.opt_in_date as lead_opt_in_date,
	extract(hour from dp.opt_in_date)::int as lead_opt_in_hour,
	extract(day from dp.opt_in_date)::int as lead_opt_in_day,
	extract(dow from dp.opt_in_date)::int as lead_opt_in_dow,
	(extract('day' from date_trunc('week', case when extract(isodow from dp.opt_in_date::date) = 7 then dp.opt_in_date::date::date + 1 when extract(isodow from dp.opt_in_date::date) = 6 then dp.opt_in_date::date::date - 1 else dp.opt_in_date::date::date end) - date_trunc('week', case when extract(isodow from date_trunc('month', dp.opt_in_date::date)) = 7 then date_trunc('month', dp.opt_in_date::date)::date + 1 when extract(isodow from date_trunc('month', dp.opt_in_date::date)) = 6 then date_trunc('month', dp.opt_in_date::date)::date - 1 else date_trunc('month', dp.opt_in_date::date)::date end)) / 7 + 1)::int as lead_opt_in_wom,
	extract(month from dp.opt_in_date)::int as lead_opt_in_month,
	extract(year from dp.opt_in_date)::int as lead_opt_in_year,
	case when amount_requested < 1000 then o.max_value else amount_requested end / case month_revenue when 0 then 1 else month_revenue end / 12 as lead_admin_incremental_leverage,
	o.max_value/case when amount_requested < 1000 then o.max_value else amount_requested end as lead_offer_over_application_value,
	lr.valor_solicitado/o.max_value::float as lead_requested_over_offer_value,
	lr.value/lr.valor_solicitado::float as lead_loan_over_requested_value,
	lr.prazo_solicitado/o.max_number_of_installments::double precision as lead_requested_over_offer_term,
	lr.number_of_installments/lr.prazo_solicitado::float as lead_loan_over_requested_term,
	case when lr.value/o.max_value::float = lr.number_of_installments/o.max_number_of_installments::float then 1 else (@li.pmt) / ((o.interest_rate / 100 + 0.009) / (1 - ( 1 + o.interest_rate / 100 + 0.009) ^ (- o.max_number_of_installments)) * o.max_value) end as lead_loan_over_offer_pmt,
	case when lr.value / lr.valor_solicitado::float = lr.number_of_installments / lr.prazo_solicitado::float then 1 else (@li.pmt) / ((o.interest_rate / 100 + 0.009) / (1 - ( 1 + o.interest_rate / 100 + 0.009) ^ (- lr.prazo_solicitado)) * lr.valor_solicitado) end as lead_loan_over_requested_pmt,
	case when o.date_inserted < dp.opt_in_date then 0 else extract(day from o.date_inserted - dp.opt_in_date) * 24 + extract(hour from o.date_inserted - dp.opt_in_date) + extract(minute from o.date_inserted - dp.opt_in_date) / 60 end as lead_app_to_offer_time,
	case when lr.date_inserted < o.date_inserted then 0 else extract(day from lr.date_inserted - o.date_inserted) * 24 + extract(hour from lr.date_inserted - o.date_inserted) + extract(minute from lr.date_inserted - o.date_inserted) / 60 end as lead_offer_to_request_time,
	case when lr.loan_date < lr.date_inserted then 0 else extract(day from lr.loan_date - lr.date_inserted) * 24 + extract(hour from lr.loan_date - lr.date_inserted) + extract(minute from lr.loan_date - lr.date_inserted) / 60 end as lead_request_to_loan_time,
	case when lr.loan_date < dp.opt_in_date then 0 else extract(day from lr.loan_date - dp.opt_in_date) * 24 + extract(hour from lr.loan_date - dp.opt_in_date) + extract(minute from lr.loan_date - dp.opt_in_date) / 60 end as lead_full_track_time,
	replace(replace(replace(replace(replace(replace(replace(replace(replace(dp.reason_for_loan,'Capital de Giro','Working Capital'),'Expansão','Expansion'),'Compra de Estoque','Inventory'),'Outros','Others'),'Consolidação de Dívidas','Debt Consolidation'),'Marketing e Vendas','Sales and Marketing'),'Uso Pessoal','Personal Use'),'Reforma','Refurbishment'),'Compra de Equipamentos','Machinery') as lead_reason_for_loan,
	replace(replace(replace(replace(replace(replace(replace(replace(replace(dp.vinculo,'Administrador','Administrator'),'Outros','Other'),'Procurador','Attorney'),'Sócio','Shareholder'),'Administradora','Administrator'),'Sócia','Shareholder'),'Diretor','Director'),'Gerente Financeiro','CFO'),'Presidente','CEO') as lead_requester_relationship,
	dp.month_revenue as lead_informed_month_revenue,
	dp.amount_requested as lead_application_amount,
	coalesce(case upper(dp.utm_source) 
		when 'ADWORDS' then 'adwords'
		when 'AGENT' then 'agent'
		when 'BIDU' then 'bidu'
		when 'BING' then 'Bing'
		when 'CREDITA' then 'credita'
		when 'FACEBOOK' then 'facebook'
		when 'FACEBOOK_ORG' then 'facebook'
		when 'FINANZERO' then 'finanzero'
		when 'FINPASS' then 'finpass'
		when 'GERU' then 'geru'
		when 'INSTAGRAM' then 'instagram'
		when 'JUROSBAIXOS' then 'jurosbaixos'
		when 'KONKERO' then 'konkero'
		when 'LINKEDIN' then 'linkedin'
		when 'MGM' then 'member-get-member'
		when 'ORGANICO' then 'organico'
		when 'BLOG' then 'organico'
		when 'SITE' then 'organico'
		when 'DIRECT_CHANNEL' then 'organico'
		when 'ANDROID' then 'organico'
		when 'RENEWAL' then 'organico'
		when 'EMAIL' then 'organico'
		when 'RD' then 'organico'
		when 'RD#/' then 'organico'
		when 'RDSTATION' then 'organico'
		when 'RD+STATION' then 'organico'
		when 'AMERICANAS' then 'other' 
		when 'IFOOD' then 'other' 
		when 'PEIXE-URBANO' then 'other' 
		when 'OUTBRAIN' then 'outbrain'
		when 'TABOOLA' then 'taboola'
		when 'CHARLES/' then 'agent'
		when 'MARCELOROMERA' then 'agent'
		when 'PBCONSULTORIA' then 'agent'
		when 'HMSEGUROS' then 'agent'
		when 'COMPARAONLINE' then 'agent'
		when 'CREDEXPRESS#/' then 'agent'
	else case when pp.identificacao is not null then 'agent' else dp.utm_source end
	end,'other') as lead_marketing_channel,
	coalesce(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_medium,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor',''),'NULL')  as lead_marketing_medium,
	coalesce(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(
		utm_campaign,'Agradecemos','')
		,'agilizar-processo','')
		,'erro','')
		,'%C3%A1','á')
		,'%23/','')
		,'formulario','')
		,'+',' ')
		,'%28','(')
		,'%29',')')
		,'%C3%BA','ú')
		,'%C3%A7','ç')
		,'%2B',' ')
		,'%2F','/'),'%C3%A9','é')
		,'%C3%A3','ã'),'#/','')
		,'lookalike-maio','Adespresso Lookalike - Maio')
		,'mosaic-maio-top-revenue-big','Adespresso Mosaic Top Revenues (Big Ones) - Maio')
		,'%20',' '),'%3E','>')
		,'howItWorksAnchor','')
		,'#','')
		,'Adespresso Mosaic Top Revenues (Big Ones) - Agosto','Adespresso Mosaic Top Revenues (Big Ones)')
		,'adespresso-geral-dezembro-2','Campanha Geral - Dezembro - Segunda Quinzena')
		,'adespresso-mobile-maio-br','Adespresso Maio - Mobile BR')
		,'adespresso-piloto','AdEspresso Piloto')
		,'campanha-sudeste-sul-nsp-julho','nova-campanha-brasil-julho-2')
		,'campanha-sudeste-sul-nsp-junho','serasa-b2b-campanha-sp-junho')
		,'campanha-sudeste-sul-nsp-maio-1','serasa-b2b-campanha-sp-maio-1')
		,'campanha-sudeste-sul-nsp-maio-2','serasa-b2b-campanha-sp-maio-2')
		,'jobstobedone-geral','Piloto Adespresso - Jobs to be done Geral')
		,'jobstobedone-varejo-dezembro','Piloto Varejo e Comércio - Jobs to be done'),'lookalike-origina','lookalike-original-setembro')
		,'mensagem-simples-outubro','mensagem-pr-outubro')
		,'mensagem-pr-setembro','mensagem-pr-outubro')
		,'mensagem-simples-setembro','mensagem-pr-outubro')
		,'mosaic-abril-lookalike','Adespresso Lookalike - Abril')
		,'mosaic-abril-nova','Adespresso Mosaic G22-G23-G24 Abril')
		,'mosaic-outubro','mosaic-outubro-v1')
		,'mosaic-marco-2','Adespresso Mosaic G22-G23-G24 Março')
		,'mosaic-janeiro-top-revenue','Adespresso Mosaic Top Revenues')
		,'mosaic-dezembro','Adespresso Audiencia facebook - Dezembro')
		,'mosaic-fevereiro2q-top-revenue-big','Adespresso Mosaic Top Revenues (Big Ones) - Fevereiro - Segunda Quinzena')
		,'mosaic-fevereiro-b2b','Adespresso Mosaic Top Revenues (Big Ones) - Fevereiro - Segunda Quinzena')
		,'mosaic-fevereiro-top-revenue-big-1','Adespresso Mosaic Top Revenues (Big Ones) - Fevereiro - Segunda Quinzena')
		,'mosaic-janeiro-b2b','Adespresso Mosaic Top Revenues')
		,'mosaic-janeiro-primeira-quinzena','Adespresso Mosaic Top Revenues')
		,'mosaic-janeiro-top-revenue','Adespresso Mosaic Top Revenues')
		,'mosaic-janeiro-top-revenue-big','Adespresso Mosaic Top Revenues')
		,'mosaic-março','Adespresso Mosaic G22-G23-G24 Março')
		,'mosaic-marco-lookalike','Adespresso Mosaic Top Revenues (Big Ones) - Maio')
		,'mosaic-marco-top-revenue-big','Adespresso Mosaic Top Revenues (Big Ones) - Marco - Primeira Quinzena BR s/ txt')
		,'mosaic-marco-top-revenue-big-2','Adespresso Mosaic Top Revenues (Big Ones) - Marco - Segunda Quinzena BR s/ txt')
		,'nova-campanha-brasil-julho-2-v1','serasa-b2b-campanha-sp-julho-2')
		,'nova-campanha-brasil-julho-2-v2','serasa-b2b-campanha-sp-julho-2')
		,'serasa-b2b-campanha-brasil-agosto-donos-v1','serasa-b2b-campanha-brasil-agosto-donos')
		,'serasa-b2b-campanha-brasil-agosto-donos-v2','serasa-b2b-campanha-brasil-agosto-donos')
		,'serasa-b2b-campanha-brasil-julho-2-donos-v1','serasa-b2b-campanha-brasil-julho-2-donos')
		,'serasa-b2b-campanha-brasil-julho-2-donos-v2','serasa-b2b-campanha-brasil-julho-2-donos')
		,'serasa-b2b-campanha-brasil-julho-2-v1','serasa-b2b-campanha-sp-julho-2')
		,'serasa-b2b-campanha-brasil-julho-2-v2','serasa-b2b-campanha-sp-julho-2')
		,'serasa-b2b-campanha-sp-julho-2-v1','serasa-b2b-campanha-sp-julho-2')
		,'serasa-b2b-campanha-sp-julho-2-v2','serasa-b2b-campanha-sp-julho-2')
		,'serasa-b2b-campanha-sp-julho-v1','serasa-b2b-campanha-sp-julho-2')
		,'serasa-b2b-campanha-sp-julho-v2','serasa-b2b-campanha-sp-julho-2')
		,'serasa-b2b-campanha-sp-junho-v1','serasa-b2b-campanha-sp-junho')
		,'serasa-b2b-campanha-sp-junho-v2','serasa-b2b-campanha-sp-junho')
		,'serasa-b2b-campanha-sp-maio-1-v1','serasa-b2b-campanha-sp-maio-1')
		,'serasa-b2b-campanha-sp-maio-2-v1','serasa-b2b-campanha-sp-maio-2')
		,'serasa-b2b-campanha-sp-maio-2-v2','serasa-b2b-campanha-sp-maio-2')
		,'otimizado-nao-mei-sp-abril-2','otimizado-nao-mei-nsp-abril-2')
		,'setembrol-','')
		,'mosaic-outubro-v1-v2','mosaic-outubro-v2')
		,'mosaic-outubro-v1-v1','mosaic-outubro-v1')
		,'Adespresso Mosaic Top Revenues   Lookalike - Maio','Adespresso Mosaic Top Revenues (Big Ones) - Maio')
		,'Adespresso Mosaic Top Revenues (Big Ones) Noite','Adespresso Mosaic Top Revenues (Big Ones) - Agosto Noite')
		,'otimizado-nao-mei-sp-fevereiro-3','otimizado-nao-mei-sp')
		,'Adespresso Mosaic Top Revenues-big','Adespresso Mosaic Top Revenues')
		,'otimizado-nao-mei-sp-abril-fds-1','otimizado-nao-mei-sp-mar-4')
		,'saude_fevereiro','Saúde - Fevereiro - Segunda Quinzena')
		,'saude_janeiro','Saúde - Janeiro - Segunda Quinzena')
		,'otimizado-nao-mei-cidades-selecionadas-abril-3','otimizado-nao-mei-sp-nsp-abril-3e4')
		,'otimizado-nao-mei-nsp-abril-fds-1','otimizado-nao-mei-nsp-abril-2')
		,'nna-simples','mosaic-outubro-v1')
		,'testepaypal','Mosaic - 1 a 14 de Fevereiro')
		,'Adespresso Mosaic Top Revenues (Big Ones) - Marco - Primeira Quinzena BR s/ txt-2','Adespresso Mosaic Top Revenues (Big Ones) - Marco - Primeira Quinzena BR s/ txt')
		,'materia_dci','Adespresso Mosaic G22-G23-G24 Março')
		,'folha','Adespresso Mosaic Top Revenues (Big Ones) - Maio')
		,'NULL') as lead_marketing_campaign,
	dp.cnpj as lead_cnpj,
	dp.cpf as lead_requester_cpf,
	dp.opt_in_ip as lead_opt_in_ip,
	case when dp.n_company_applied_before is null then 0 else dp.n_company_applied_before end as lead_times_company_applied_before,
	case when dp.n_individual_applied_before is null then 0 when coalesce(case when utm_source = 'Bing' then 'bing' when left(utm_source,4) = 'blog' then 'blog' when utm_source = 'website' or utm_source = 'direct_channel' or utm_source = 'site' then 'organico' when utm_source like '%RD%' then 'rd' when utm_source like '%andreozziz%' then 'andreozzi' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_source,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor',''),'%23/',''),'-',''),'agilizarprocesso','') end,'NULL') in ('fdex','capitalemp','finpass','IDEA','IMPERIO','ACESSE','andreozzi','ARAMAYO','AZUL','BONUS','CHARLES','CREDRAPIDO','CREDEXPRESS','DIGNESS','DUOCAPITAL','ISF','JEITONOVO','MONETAE','MRS','PLANOCAPITAL','R2A','VIETTO','vipac','VIRGINIA','ALMIRGUEDES','CREDPRIME','HMSEGUROS','CFC','REALCRED','ESPACOLIMA','FRIGO') then null else dp.n_individual_applied_before end as lead_times_individual_applied_before,
	dp.facebook::int lead_has_facebook_page,
	dp.likes lead_number_likes_facebook,
	dp.streetview::int lead_has_google_streetview,
	dp.site::int lead_has_website,
	case when upper(left(right(dp.email, length(dp.email)-position('@' in dp.email)),position('.' in right(dp.email, length(dp.email)-position('@' in dp.email)-1))))  in ('GMAIL','HOTMAIL','YAHOO','OUTLOOK','UOL','TERRA','BOL','LIVE','ICLOUD','IG','MSN','GLOBO') then replace(upper(left(right(dp.email, length(dp.email)-position('@' in dp.email)),position('.' in right(dp.email, length(dp.email)-position('@' in dp.email)-1)))),'GMAIIL','GMAIL') else 'OTHER' end as lead_email_server,
	case when (month_revenue >= 100000 and age < 2) or (month_revenue >= 200000 and age < 3) or (month_revenue >= 300000 and age < 4) then 1 else 0 end as lead_Age_Revenue_Discrepancy,
	case when dp.phone = '' then null else substring(dp.phone,2,2) end as lead_phone_area_code,
--NEOWAY
	dp.revenue::double precision as neoway_estimated_annual_revenue,
	to_date(dp.date_created,'yyyy-mm-dd') as neoway_date_company_creation,
	dp.age as neoway_company_age,
	dp.employees as neoway_estimate_number_of_employees,
	dp.mei::int as neoway_is_mei,
	dp.is_simples::int as neoway_simple_tribute_method_applied,
	dp.cnae_code as neoway_economic_activity_national_registration_code,
	dp.cnae as neoway_economic_activity_national_registration_name,
	dp.social_capital::float as neoway_social_capital,
	dp.number_partners as neoway_estimated_number_of_shareholders,
	dp.number_coligadas as neoway_estimated_number_of_related_companies,
	dp.number_branches as neoway_estimated_number_of_branches,
	case when dp.zip_code = '' then null else dp.zip_code end as neoway_company_zip_code,
--	coalesce(case when dp.zip_code = '' then null else dp.zip_code end, signer_address.signer_zipcode) as neoway_company_zip_code,
	dp.neighbourhood as neoway_company_neighbourhood,
	dp.city as neoway_company_city,
	dp.state as neoway_company_state,
	case when (dp.city = 'RIO BRANCO' and dp.state = 'AC') or (dp.city = 'MACEIO' and dp.state = 'AL') or (dp.city = 'MACAPA' and dp.state = 'AP') or (dp.city = 'MANAUS' and dp.state = 'AM') or (dp.city = 'SALVADOR' and dp.state = 'BA') or (dp.city = 'FORTALEZA' and dp.state = 'CE') or 
		(dp.city = 'BRASILIA' and dp.state = 'DF') or (dp.city = 'VITORIA' and dp.state = 'ES') or (dp.city = 'GOIANIA' and dp.state = 'GO') or (dp.city = 'SAO LUIS' and dp.state = 'MA') or (dp.city = 'CUIABA' and dp.state = 'MT') or (dp.city = 'CAMPO GRANDE' and dp.state = 'MS') or 
		(dp.city = 'BELO HORIZONTE' and dp.state = 'MG') or (dp.city = 'BELEM' and dp.state = 'PA') or (dp.city = 'JOAO PESSOA' and dp.state = 'PB') or (dp.city = 'CURITIBA' and dp.state = 'PR') or (dp.city = 'RECIFE' and dp.state = 'PE') or (dp.city = 'TERESINA' and dp.state = 'PI') or 
		(dp.city = 'RIO DE JANEIRO' and dp.state = 'RJ') or (dp.city = 'NATAL' and dp.state = 'RN') or (dp.city = 'PORTO VELHO' and dp.state = 'RO') or (dp.city = 'BOA VISTA' and dp.state = 'RR') or (dp.city = 'FLORIANOPOLIS' and dp.state = 'SC') or (dp.city = 'SAO PAULO' and dp.state = 'SP') or 
		(dp.city = 'ARACAJU' and dp.state = 'SE') or (dp.city = 'PALMAS' and dp.state = 'TO') or (dp.city = 'PORTO ALEGRE' and dp.state = 'RS') then 1 else 0 end as neoway_is_city_capital,
	case when dp.state in ('AM','RR','AP','PA','TO','RO','AC') then 'NORTE' when dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA') then 'NORDESTE' when dp.state in ('MT','MS','GO','DF') then 'CENTRO-OESTE' when dp.state in ('SP','RJ','MG','ES') then 'SUDESTE' when dp.state in ('PR','RS','SC') then 'SUL' else null end as neoway_state_region,
	replace(replace(replace(replace(replace(dp.tax_health,'VERDE','Green'),'AZUL','Blue'),'AMARELO','Yellow'),'LARANJA','Orange'),'CINZA','Grey') as neoway_tax_health,
	replace(replace(replace(replace(dp.level_activity,'MEDIA','Medium'),'ALTA','High'),'MUITO BAIXA','Very Low'),'BAIXA','Low') as neoway_level_activity,
	replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(dp.activity_sector,'VAREJO','Retail'),'SERVICOS DE ALOJAMENTO/ALIMENTACAO','Accomodation/Feeding'),
		'TELECOM','Telecommunication'),'TRANSPORTE','Transportation'),'SERVICOS ADMINISTRATIVOS','Administrative Services'),'SERVICOS DE SAUDE','Health services'),'SERVICOS PROFISSIONAIS E TECNICOS','Technical/Professional Services'),'INDUSTRIA DIGITAL','Digital Industry'),
		'QUIMICA-PETROQUIMICA','Chemistry/Petrochemistry'),'SERVICOS DE EDUCACAO','Education Services'),'BENS DE CONSUMO','Consumer goods'),'ATACADO','Wholesale'),'INDUSTRIA DA CONSTRUCAO','Construction Industry'),'SIDERURGICA-METALURGIA','Steel Industry'),'ELETROELETRONICOS','Eletronic goods'),
		'PRODUTOS DE AGROPECUARIA','Farming goods'),'TEXTEIS','Textiles'),'PAPEL E CELULOSE','Paper/Celulosis'),'SERVICOS DE SANEAMENTO BASICO','Sanitation Services'),'INDUSTRIA AUTOMOTIVA','Car Industry'),'ENERGIA','Energy'),'BENS DE CAPITAL','Capital goods'),'SERVICOS DIVERSOS','Miscellaneous Services'),
		'DIVERSOS','Miscellaneous'),'FARMACEUTICA','Farmaceuticals'),'MINERACAO','Mining') as neoway_activity_sector,
	replace(replace(replace(replace(replace(dp.sector,'COMERCIO','Retail'),'SERVICOS','Services'),'INDUSTRIA','Industry'),'AGROPECUARIA','Farming'),'CONSTRUCAO CIVIL','Construction') as neoway_sector,
	dp.pgfn_debt as neoway_government_debt,
--CREDNET
	dp.number_protests as crednet_notary_registry_protests_unit,
	dp.protests_amount crednet_notary_registry_protests_value,
	dp.protests_amount / case month_revenue when 0 then 1 else month_revenue end / 12 crednet_notary_registry_protests_value_byrevenue,
	dp.number_spc as crednet_company_negative_mark_by_non_fin_companies_unit,
	dp.spc_amount as crednet_company_negative_mark_by_non_fin_companies_value,
	dp.spc_amount / case month_revenue when 0 then 1 else month_revenue end / 12 as crednet_company_negative_mark_by_non_fin_companies_value_byrevenue,
	dp.number_acoes as crednet_company_legal_action_unit,
	dp.acoes_amount as crednet_company_legal_action_value,
	dp.acoes_amount / case month_revenue when 0 then 1 else month_revenue end / 12 crednet_legal_action_value_byrevenue,
	dp.number_refins as crednet_company_negative_mark_by_fin_companies_unit,
	dp.refins_amount as crednet_company_negative_mark_by_fin_companies_value,
	dp.refins_amount / case month_revenue when 0 then 1 else month_revenue end / 12 crednet_company_negative_mark_by_fin_companies_value_byrevenue,
	dp.number_protests + dp.number_spc + dp.number_acoes + dp.number_refins as crednet_total_derogatory_mark_units,
	dp.protests_amount + dp.spc_amount + dp.acoes_amount + dp.refins_amount as crednet_total_derogatory_mark_value,
	(dp.protests_amount + dp.spc_amount + dp.acoes_amount + dp.refins_amount) / case month_revenue when 0 then 1 else month_revenue end / 12 as crednet_total_derogatory_mark_value_byrevenue,
--SCORES
	lead_score::float as lead_biz_lead_score,
	bizu_score::float as lead_biz_bizu1_score,
	case when bizu_score >= 850 then 'A+' when bizu_score >= 700 then 'A-' when bizu_score >= 650 then 'B+' when bizu_score >= 600 then 'B-' when bizu_score >= 500 then 'C+'when bizu_score >= 400 then 'C-' when bizu_score >= 300 then 'D' when bizu_score < 300 then 'E' else 'No Bizu1' end as lead_rating_class_bizu1,
	coalesce(serasa_coleta,o.rating,dp.serasa_4) as lead_experian_score4,
	case when coalesce(serasa_coleta,o.rating,dp.serasa_4) >= 750 then 'A+' when coalesce(serasa_coleta,o.rating,dp.serasa_4) >= 600 then 'A-' when coalesce(serasa_coleta,o.rating,dp.serasa_4) >= 450 then 'B+' when coalesce(serasa_coleta,o.rating,dp.serasa_4) >= 350 then 'B-' when coalesce(serasa_coleta,o.rating,dp.serasa_4) >= 250 then 'C+'when coalesce(serasa_coleta,o.rating,dp.serasa_4) >= 150 then 'C-'when coalesce(serasa_coleta,o.rating,dp.serasa_4) >= 100 then 'D' when coalesce(serasa_coleta,o.rating,dp.serasa_4) < 100 then 'E' else 'No Serasa' end as lead_class_experian_score4,
	pd_serasa lead_experian_score4_associated_pd,
	bktest.s6 as lead_experian_score6,
	dp.proposal_score as lead_proposal_score,
	proposal_amount as lead_proposal_limit,
	proposal_interest as lead_proposal_interest_rate,
	dp.low_ticket_score as lead_low_ticket_score,
	dp.bizu2_score::float as lead_biz_bizu2_score,
	dp.pd_bizu2::float as lead_biz_bizu2_associated_pd,
	case when dp.bizu2_score >= 1100 then 'A+' when dp.bizu2_score >= 850 then 'A-' when dp.bizu2_score >= 720 then 'B+' when dp.bizu2_score >= 600 then 'B-' when dp.bizu2_score >= 500 then 'C+' when dp.bizu2_score >= 400 then 'C-' when dp.bizu2_score >= 300 then 'D' when dp.bizu2_score >= 200 then 'E' when dp.bizu2_score < 200 then 'F' else 'No Bizu2' end as lead_classe_Bizu2,
	credita_score_mei as lead_experian_score_mei,
	case when credita_score_mei >= 800 then 'A' when credita_score_mei >= 700 then 'B+' when credita_score_mei >= 600 then 'B-' when credita_score_mei >= 500 then 'C+' when credita_score_mei >= 400 then 'C-' when credita_score_mei >= 300 then 'D' when credita_score_mei < 200 then 'E' else 'No Score MEI' end as lead_classe_Score_MEI,
	dp.bizu21_score::float as lead_biz_bizu21_score,
	dp.pd_bizu21::float as lead_biz_bizu21_associated_pd,
	dp.bizu21_class as lead_classe_Bizu21,
	dp.bizu_x_score::float as lead_biz_bizux_score,
	dp.bizu_x_class as lead_classe_Bizux,
--------------------------INITIAL OFFER
	initialoff.offer_id as initialoffer_id,
	initialoff.date_inserted as initial_offer_date,
	initialoff.interest_rate initial_offer_interest_rate,
	initialoff.max_number_of_installments initial_offer_max_term,
	initialoff.max_value initial_offer_max_value,
	initialoff.count_neg - 1 as final_offer_count_negotiations,
--------------------------FINAL OFFER
	o.offer_id as final_offer_id,
	o.date_inserted as final_offer_date,
	o.interest_rate as final_offer_interest_rate,
	o.max_number_of_installments as final_offer_max_term,
	o.max_value as final_offer_max_value,
----------------------LOAN
	lr.loan_request_id as loan_request_id,
	lr.date_inserted as loan_request_date,
	lr.valor_solicitado as loan_requested_value,
	lr.prazo_solicitado as loan_requested_term,
	dp.previous_loans_count as loan_is_renewal,
	lr.value loan_net_value,
	li.total_payment loan_gross_value,
	lr.taxa_final as loan_final_net_interest_rate,
	li.monthly_cet loan_final_gross_interest_rate,
	lr.number_of_installments loan_original_final_term,
	(@li.pmt) as loan_original_final_pmt,
	(@li.pmt)/case month_revenue when 0 then 1 else month_revenue end as loan_original_final_pmt_leverage,
	li.commission as loan_commission_fee,
	lr.loan_date as loan_contract_date,
	extract (year from age(current_date,lr.loan_date)) * 12 + extract(month from age(current_date,lr.loan_date)) as loan_months_on_book,
	lr.portfolio_id::text loan_portfolio_id,
	ba.bank_id::text loan_bank_id,
	extract(day from case when lr.payment_day is null then original_1st_pmt_due_to else lr.payment_day end) as loan_1st_pmt_chosen_day_of_month,
	case when lr.payment_day is null then original_1st_pmt_due_to else lr.payment_day end - lr.loan_date as loan_grace_period,
	lr.analista_responsavel as loan_analyst_responsible,
	("Alçada 1 - analista" = 'BizBot')::int as loan_approved_by_bizbot,
	lr.status as loan_status,
--------------------------COLLECTION-PLANOS
	fully_paid as collection_loan_fully_paid,
	count_renegotiation as collection_count_restructures,
	count_unbroken_renegotiations as collection_count_unbroken_restructures,
	count_broken_renegotiation as collection_count_broken_restructures,
	primeira_parcela_renegociada as collection_pmt_number_of_1st_restructure,
	data_primeira_renegociacao as collection_date_1st_restructure,
--------------------------COLLECTION-PARCELAS
	current_term as collection_loan_current_term,
	current_term / lr.number_of_installments as collection_current_over_original_term,
	due_inst as collection_total_due_installments,
	to_come_inst as collection_to_be_due_installments,
	paid_inst as collection_number_of_installments_paid,
	late_inst as collection_number_of_installments_curr_late,
	coalesce(total_paid_amount,0) as collection_loan_amount_paid,
	coalesce(total_late_amount,0) as collection_loan_amount_late,
	original_p_and_i as collection_original_p_plus_i,
	curr_p_and_i as collection_current_p_plus_i,
	curr_p_and_i / original_p_and_i as collection_current_over_original_p_plus_i,
	coalesce(EAD,0) as collection_current_ead,
	original_p_and_i - coalesce(total_paid_amount,0) as collection_original_ead,
	pmt_atual as collection_current_pmt_value,
	parc_em_aberto as collection_current_pmt_number,
	current_inst_due_to as collection_current_pmt_due_to,
	coalesce(total_amount_over_180,0) as collection_loan_amount_over180,
	dpd_soft.max_late_pmt1 as collection_current_max_late_pmt1,
	dpd_soft.max_late_pmt2 as collection_current_max_late_pmt2,
	dpd_soft.max_late_pmt3 as collection_current_max_late_pmt3,
	dpd_soft.max_late_pmt4 as collection_current_max_late_pmt4,
	dpd_soft.max_late_pmt5 as collection_current_max_late_pmt5,
	dpd_soft.max_late_pmt6 as collection_current_max_late_pmt6,
	dpd_soft.max_late_pmt7 as collection_current_max_late_pmt7,
	dpd_soft.max_late_pmt8 as collection_current_max_late_pmt8,
	dpd_soft.max_late_pmt9 as collection_current_max_late_pmt9,
	dpd_soft.max_late_pmt10 as collection_current_max_late_pmt10,
	dpd_soft.max_late_pmt11 as collection_current_max_late_pmt11,
	dpd_soft.max_late_pmt12 as collection_current_max_late_pmt12,
	dpd_soft.max_late_pmt13 as collection_current_max_late_pmt13,
	dpd_soft.max_late_pmt14 as collection_current_max_late_pmt14,
	dpd_soft.max_late_pmt15 as collection_current_max_late_pmt15,
	dpd_soft.max_late_pmt16 as collection_current_max_late_pmt16,
	dpd_soft.max_late_pmt17 as collection_current_max_late_pmt17,
	dpd_soft.max_late_pmt18 as collection_current_max_late_pmt18,
	dpd_soft.max_late_pmt19 as collection_current_max_late_pmt19,
	dpd_soft.max_late_pmt20 as collection_current_max_late_pmt20,
	dpd_soft.max_late_pmt21 as collection_current_max_late_pmt21,
	dpd_soft.max_late_pmt22 as collection_current_max_late_pmt22,
	dpd_soft.max_late_pmt23 as collection_current_max_late_pmt23,
	dpd_soft.max_late_pmt24 as collection_current_max_late_pmt24,
	case when paid_inst = 0 and dpd_soft.max_late_pmt1 > 0 then 1 else 0 end as collection_is_current_fpd,
	dpd_soft.max_late_pmt1 as collection_fpd_by,
	(dpd_soft.max_late_pmt1 > 15)::int as collection_fpd_15,
	(dpd_soft.max_late_pmt1 > 30)::int as collection_fpd_30,
	(dpd_soft.max_late_pmt1 > 60)::int as collection_fpd_60,
	(dpd_soft.max_late_pmt1 > 90)::int as collection_fpd_90,
	case when paid_inst <= 1 and dpd_soft.max_late_pmt2 > 0 then 1 else 0 end as collection_is_current_spd,
	dpd_soft.max_late_pmt2 as collection_spd_by,
	(dpd_soft.max_late_pmt2 > 15)::int as collection_spd_15,
	(dpd_soft.max_late_pmt2 > 30)::int as collection_spd_30,
	(dpd_soft.max_late_pmt2 > 60)::int as collection_spd_60,
	(dpd_soft.max_late_pmt2 > 90)::int as collection_spd_90,
--Current
	case when current_inst_due_to is null then 0 else current_date - current_inst_due_to end as collection_loan_curr_late_by,
	(case when current_inst_due_to is null then 0 else current_date - current_inst_due_to end > 30)::int as collection_loan_curr_late_over_30,
	(case when current_inst_due_to is null then 0 else current_date - current_inst_due_to end > 60)::int as collection_loan_curr_late_over_60,
	(case when current_inst_due_to is null then 0 else current_date - current_inst_due_to end > 90)::int as collection_loan_curr_late_over_90,
	greatest(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24) as collection_loan_max_late_by,	
	case when greatest(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24) > 360 then '360 +'
		when greatest(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24) >= 331 then '331 to 360'
		when greatest(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24) >= 301 then '301 to 330'
		when greatest(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24) >= 271 then '271 to 300'
		when greatest(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24) >= 241 then '241 to 270'
		when greatest(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24) >= 211 then '211 to 240'
		when greatest(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24) >= 181 then '18 to 210'
		when greatest(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24) >= 151 then '151 to 180'
		when greatest(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24) >= 121 then '121 to 150'
		when greatest(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24) >= 91 then '91  to 120'
		when greatest(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24) >= 61 then '61 to 90'
		when greatest(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24) >= 31 then '31 to 60'
		when greatest(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24) >= 15 then '15 to 30'
		when greatest(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24) >= 1 then '1 to 14'
		else 'On time' end as collection_current_roll_band,
	coalesce(dpd_soft.max_late_pmt1,0) + coalesce(dpd_soft.max_late_pmt2,0) + coalesce(dpd_soft.max_late_pmt3,0) + coalesce(dpd_soft.max_late_pmt4,0) + coalesce(dpd_soft.max_late_pmt5,0) + coalesce(dpd_soft.max_late_pmt6,0) + coalesce(dpd_soft.max_late_pmt7,0) + coalesce(dpd_soft.max_late_pmt8,0) + coalesce(dpd_soft.max_late_pmt9,0) + coalesce(dpd_soft.max_late_pmt10,0) + coalesce(dpd_soft.max_late_pmt11,0) + coalesce(dpd_soft.max_late_pmt12,0) + coalesce(dpd_soft.max_late_pmt13,0) + coalesce(dpd_soft.max_late_pmt14,0) + coalesce(dpd_soft.max_late_pmt15,0) + coalesce(dpd_soft.max_late_pmt16,0) + coalesce(dpd_soft.max_late_pmt17,0) + coalesce(dpd_soft.max_late_pmt18,0) + coalesce(dpd_soft.max_late_pmt19,0) + coalesce(dpd_soft.max_late_pmt20,0) + coalesce(dpd_soft.max_late_pmt21,0) + coalesce(dpd_soft.max_late_pmt22,0) + coalesce(dpd_soft.max_late_pmt23,0) + coalesce(dpd_soft.max_late_pmt24,0) as collection_sum_payment_history_curve,
	case when greatest(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24) <= 0 then 0 
		else case greatest(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24)
				when dpd_soft.max_late_pmt1 then 1
				when dpd_soft.max_late_pmt2 then 2
				when dpd_soft.max_late_pmt3 then 3
				when dpd_soft.max_late_pmt4 then 4
				when dpd_soft.max_late_pmt5 then 5
				when dpd_soft.max_late_pmt6 then 6
				when dpd_soft.max_late_pmt7 then 7
				when dpd_soft.max_late_pmt8 then 8
				when dpd_soft.max_late_pmt9 then 9
				when dpd_soft.max_late_pmt10 then 10
				when dpd_soft.max_late_pmt11 then 11
				when dpd_soft.max_late_pmt12 then 12
				when dpd_soft.max_late_pmt13 then 13
				when dpd_soft.max_late_pmt14 then 14
				when dpd_soft.max_late_pmt15 then 15
				when dpd_soft.max_late_pmt16 then 16
				when dpd_soft.max_late_pmt17 then 17
				when dpd_soft.max_late_pmt18 then 18
				when dpd_soft.max_late_pmt19 then 19
				when dpd_soft.max_late_pmt20 then 20
				when dpd_soft.max_late_pmt21 then 21
				when dpd_soft.max_late_pmt22 then 22
				when dpd_soft.max_late_pmt23 then 23
				when dpd_soft.max_late_pmt24 then 24
			end 
		end as collection_pmt_of_max_late,
	(greatest(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24,0) > 30)::int as collection_loan_ever_30,
	(greatest(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24,0) > 60)::int as collection_loan_ever_60,
	(greatest(dpd_soft.max_late_pmt1,dpd_soft.max_late_pmt2,dpd_soft.max_late_pmt3,dpd_soft.max_late_pmt4,dpd_soft.max_late_pmt5,dpd_soft.max_late_pmt6,dpd_soft.max_late_pmt7,dpd_soft.max_late_pmt8,dpd_soft.max_late_pmt9,dpd_soft.max_late_pmt10,dpd_soft.max_late_pmt11,dpd_soft.max_late_pmt12,dpd_soft.max_late_pmt13,dpd_soft.max_late_pmt14,dpd_soft.max_late_pmt15,dpd_soft.max_late_pmt16,dpd_soft.max_late_pmt17,dpd_soft.max_late_pmt18,dpd_soft.max_late_pmt19,dpd_soft.max_late_pmt20,dpd_soft.max_late_pmt21,dpd_soft.max_late_pmt22,dpd_soft.max_late_pmt23,dpd_soft.max_late_pmt24,0) > 90)::int as collection_loan_ever_90,
	case when dpd_soft.max_late_pmt1 >= 1 then 1 when dpd_soft.max_late_pmt2 >= 1 then 2 when dpd_soft.max_late_pmt3 >= 1 then 3 when dpd_soft.max_late_pmt4 >= 1 then 4 when dpd_soft.max_late_pmt5 >= 1 then 5 when dpd_soft.max_late_pmt6 >= 1 then 6 when dpd_soft.max_late_pmt7 >= 1 then 7 when dpd_soft.max_late_pmt8 >= 1 then 8 when dpd_soft.max_late_pmt9 >= 1 then 9 when dpd_soft.max_late_pmt10 >= 1 then 10 when dpd_soft.max_late_pmt11 >= 1 then 11 when dpd_soft.max_late_pmt12 >= 1 then 12 when dpd_soft.max_late_pmt12 >= 1 then 12 when dpd_soft.max_late_pmt13 >= 1 then 13 when dpd_soft.max_late_pmt14 >= 1 then 14 when dpd_soft.max_late_pmt15 >= 1 then 15 when dpd_soft.max_late_pmt16 >= 1 then 16 when dpd_soft.max_late_pmt17 >= 1 then 17 when dpd_soft.max_late_pmt18 >= 1 then 18 when dpd_soft.max_late_pmt19 >= 1 then 19 when dpd_soft.max_late_pmt20 >= 1 then 20 when dpd_soft.max_late_pmt21 >= 1 then 21 when dpd_soft.max_late_pmt22 >= 1 then 22 when dpd_soft.max_late_pmt23 >= 1 then 23 when dpd_soft.max_late_pmt24 >= 1 then 24 else 0 end as collection_pmt_of_over1,
	case when dpd_soft.max_late_pmt1 >= 1 then 1 when dpd_soft.max_late_pmt2 >= 1 then 2 when dpd_soft.max_late_pmt3 >= 1 then 3 when dpd_soft.max_late_pmt4 >= 1 then 4 when dpd_soft.max_late_pmt5 >= 1 then 5 when dpd_soft.max_late_pmt6 >= 1 then 6 when dpd_soft.max_late_pmt7 >= 1 then 7 when dpd_soft.max_late_pmt8 >= 1 then 8 when dpd_soft.max_late_pmt9 >= 1 then 9 when dpd_soft.max_late_pmt10 >= 1 then 10 when dpd_soft.max_late_pmt11 >= 1 then 11 when dpd_soft.max_late_pmt12 >= 1 then 12 when dpd_soft.max_late_pmt12 >= 1 then 12 when dpd_soft.max_late_pmt13 >= 1 then 13 when dpd_soft.max_late_pmt14 >= 1 then 14 when dpd_soft.max_late_pmt15 >= 1 then 15 when dpd_soft.max_late_pmt16 >= 1 then 16 when dpd_soft.max_late_pmt17 >= 1 then 17 when dpd_soft.max_late_pmt18 >= 1 then 18 when dpd_soft.max_late_pmt19 >= 1 then 19 when dpd_soft.max_late_pmt20 >= 1 then 20 when dpd_soft.max_late_pmt21 >= 1 then 21 when dpd_soft.max_late_pmt22 >= 1 then 22 when dpd_soft.max_late_pmt23 >= 1 then 23 when dpd_soft.max_late_pmt24 >= 1 then 24 else 0 end / lr.number_of_installments as collection_perc_loan_of_over1,
	case when dpd_soft.max_late_pmt1 >= 7 then 1 when dpd_soft.max_late_pmt2 >= 7 then 2 when dpd_soft.max_late_pmt3 >= 7 then 3 when dpd_soft.max_late_pmt4 >= 7 then 4 when dpd_soft.max_late_pmt5 >= 7 then 5 when dpd_soft.max_late_pmt6 >= 7 then 6 when dpd_soft.max_late_pmt7 >= 7 then 7 when dpd_soft.max_late_pmt8 >= 7 then 8 when dpd_soft.max_late_pmt9 >= 7 then 9 when dpd_soft.max_late_pmt10 >= 7 then 10 when dpd_soft.max_late_pmt11 >= 7 then 11 when dpd_soft.max_late_pmt12 >= 7 then 12 when dpd_soft.max_late_pmt12 >= 7 then 12 when dpd_soft.max_late_pmt13 >= 7 then 13 when dpd_soft.max_late_pmt14 >= 7 then 14 when dpd_soft.max_late_pmt15 >= 7 then 15 when dpd_soft.max_late_pmt16 >= 7 then 16 when dpd_soft.max_late_pmt17 >= 7 then 17 when dpd_soft.max_late_pmt18 >= 7 then 18 when dpd_soft.max_late_pmt19 >= 7 then 19 when dpd_soft.max_late_pmt20 >= 7 then 20 when dpd_soft.max_late_pmt21 >= 7 then 21 when dpd_soft.max_late_pmt22 >= 7 then 22 when dpd_soft.max_late_pmt23 >= 7 then 23 when dpd_soft.max_late_pmt24 >= 7 then 24 else 0 end as collection_pmt_of_over7,
	case when dpd_soft.max_late_pmt1 >= 7 then 1 when dpd_soft.max_late_pmt2 >= 7 then 2 when dpd_soft.max_late_pmt3 >= 7 then 3 when dpd_soft.max_late_pmt4 >= 7 then 4 when dpd_soft.max_late_pmt5 >= 7 then 5 when dpd_soft.max_late_pmt6 >= 7 then 6 when dpd_soft.max_late_pmt7 >= 7 then 7 when dpd_soft.max_late_pmt8 >= 7 then 8 when dpd_soft.max_late_pmt9 >= 7 then 9 when dpd_soft.max_late_pmt10 >= 7 then 10 when dpd_soft.max_late_pmt11 >= 7 then 11 when dpd_soft.max_late_pmt12 >= 7 then 12 when dpd_soft.max_late_pmt12 >= 7 then 12 when dpd_soft.max_late_pmt13 >= 7 then 13 when dpd_soft.max_late_pmt14 >= 7 then 14 when dpd_soft.max_late_pmt15 >= 7 then 15 when dpd_soft.max_late_pmt16 >= 7 then 16 when dpd_soft.max_late_pmt17 >= 7 then 17 when dpd_soft.max_late_pmt18 >= 7 then 18 when dpd_soft.max_late_pmt19 >= 7 then 19 when dpd_soft.max_late_pmt20 >= 7 then 20 when dpd_soft.max_late_pmt21 >= 7 then 21 when dpd_soft.max_late_pmt22 >= 7 then 22 when dpd_soft.max_late_pmt23 >= 7 then 23 when dpd_soft.max_late_pmt24 >= 7 then 24 else 0 end / lr.number_of_installments as collection_perc_loan_of_over7,
	case when dpd_soft.max_late_pmt1 >= 15 then 1 when dpd_soft.max_late_pmt2 >= 15 then 2 when dpd_soft.max_late_pmt3 >= 15 then 3 when dpd_soft.max_late_pmt4 >= 15 then 4 when dpd_soft.max_late_pmt5 >= 15 then 5 when dpd_soft.max_late_pmt6 >= 15 then 6 when dpd_soft.max_late_pmt7 >= 15 then 7 when dpd_soft.max_late_pmt8 >= 15 then 8 when dpd_soft.max_late_pmt9 >= 15 then 9 when dpd_soft.max_late_pmt10 >= 15 then 10 when dpd_soft.max_late_pmt11 >= 15 then 11 when dpd_soft.max_late_pmt12 >= 15 then 12 when dpd_soft.max_late_pmt12 >= 15 then 12 when dpd_soft.max_late_pmt13 >= 15 then 13 when dpd_soft.max_late_pmt14 >= 15 then 14 when dpd_soft.max_late_pmt15 >= 15 then 15 when dpd_soft.max_late_pmt16 >= 15 then 16 when dpd_soft.max_late_pmt17 >= 15 then 17 when dpd_soft.max_late_pmt18 >= 15 then 18 when dpd_soft.max_late_pmt19 >= 15 then 19 when dpd_soft.max_late_pmt20 >= 15 then 20 when dpd_soft.max_late_pmt21 >= 15 then 21 when dpd_soft.max_late_pmt22 >= 15 then 22 when dpd_soft.max_late_pmt23 >= 15 then 23 when dpd_soft.max_late_pmt24 >= 15 then 24 else 0 end as collection_pmt_of_over15,
	case when dpd_soft.max_late_pmt1 >= 15 then 1 when dpd_soft.max_late_pmt2 >= 15 then 2 when dpd_soft.max_late_pmt3 >= 15 then 3 when dpd_soft.max_late_pmt4 >= 15 then 4 when dpd_soft.max_late_pmt5 >= 15 then 5 when dpd_soft.max_late_pmt6 >= 15 then 6 when dpd_soft.max_late_pmt7 >= 15 then 7 when dpd_soft.max_late_pmt8 >= 15 then 8 when dpd_soft.max_late_pmt9 >= 15 then 9 when dpd_soft.max_late_pmt10 >= 15 then 10 when dpd_soft.max_late_pmt11 >= 15 then 11 when dpd_soft.max_late_pmt12 >= 15 then 12 when dpd_soft.max_late_pmt12 >= 15 then 12 when dpd_soft.max_late_pmt13 >= 15 then 13 when dpd_soft.max_late_pmt14 >= 15 then 14 when dpd_soft.max_late_pmt15 >= 15 then 15 when dpd_soft.max_late_pmt16 >= 15 then 16 when dpd_soft.max_late_pmt17 >= 15 then 17 when dpd_soft.max_late_pmt18 >= 15 then 18 when dpd_soft.max_late_pmt19 >= 15 then 19 when dpd_soft.max_late_pmt20 >= 15 then 20 when dpd_soft.max_late_pmt21 >= 15 then 21 when dpd_soft.max_late_pmt22 >= 15 then 22 when dpd_soft.max_late_pmt23 >= 15 then 23 when dpd_soft.max_late_pmt24 >= 15 then 24 else 0 end / lr.number_of_installments as collection_perc_loan_of_over15,
	case when dpd_soft.max_late_pmt1 >= 30 then 1 when dpd_soft.max_late_pmt2 >= 30 then 2 when dpd_soft.max_late_pmt3 >= 30 then 3 when dpd_soft.max_late_pmt4 >= 30 then 4 when dpd_soft.max_late_pmt5 >= 30 then 5 when dpd_soft.max_late_pmt6 >= 30 then 6 when dpd_soft.max_late_pmt7 >= 30 then 7 when dpd_soft.max_late_pmt8 >= 30 then 8 when dpd_soft.max_late_pmt9 >= 30 then 9 when dpd_soft.max_late_pmt10 >= 30 then 10 when dpd_soft.max_late_pmt11 >= 30 then 11 when dpd_soft.max_late_pmt12 >= 30 then 12 when dpd_soft.max_late_pmt12 >= 30 then 12 when dpd_soft.max_late_pmt13 >= 30 then 13 when dpd_soft.max_late_pmt14 >= 30 then 14 when dpd_soft.max_late_pmt15 >= 30 then 15 when dpd_soft.max_late_pmt16 >= 30 then 16 when dpd_soft.max_late_pmt17 >= 30 then 17 when dpd_soft.max_late_pmt18 >= 30 then 18 when dpd_soft.max_late_pmt19 >= 30 then 19 when dpd_soft.max_late_pmt20 >= 30 then 20 when dpd_soft.max_late_pmt21 >= 30 then 21 when dpd_soft.max_late_pmt22 >= 30 then 22 when dpd_soft.max_late_pmt23 >= 30 then 23 when dpd_soft.max_late_pmt24 >= 30 then 24 else 0 end as collection_pmt_of_over30,
	case when dpd_soft.max_late_pmt1 >= 30 then 1 when dpd_soft.max_late_pmt2 >= 30 then 2 when dpd_soft.max_late_pmt3 >= 30 then 3 when dpd_soft.max_late_pmt4 >= 30 then 4 when dpd_soft.max_late_pmt5 >= 30 then 5 when dpd_soft.max_late_pmt6 >= 30 then 6 when dpd_soft.max_late_pmt7 >= 30 then 7 when dpd_soft.max_late_pmt8 >= 30 then 8 when dpd_soft.max_late_pmt9 >= 30 then 9 when dpd_soft.max_late_pmt10 >= 30 then 10 when dpd_soft.max_late_pmt11 >= 30 then 11 when dpd_soft.max_late_pmt12 >= 30 then 12 when dpd_soft.max_late_pmt12 >= 30 then 12 when dpd_soft.max_late_pmt13 >= 30 then 13 when dpd_soft.max_late_pmt14 >= 30 then 14 when dpd_soft.max_late_pmt15 >= 30 then 15 when dpd_soft.max_late_pmt16 >= 30 then 16 when dpd_soft.max_late_pmt17 >= 30 then 17 when dpd_soft.max_late_pmt18 >= 30 then 18 when dpd_soft.max_late_pmt19 >= 30 then 19 when dpd_soft.max_late_pmt20 >= 30 then 20 when dpd_soft.max_late_pmt21 >= 30 then 21 when dpd_soft.max_late_pmt22 >= 30 then 22 when dpd_soft.max_late_pmt23 >= 30 then 23 when dpd_soft.max_late_pmt24 >= 30 then 24 else 0 end / lr.number_of_installments as collection_perc_loan_of_over30,
	case when dpd_soft.max_late_pmt1 >= 60 then 1 when dpd_soft.max_late_pmt2 >= 60 then 2 when dpd_soft.max_late_pmt3 >= 60 then 3 when dpd_soft.max_late_pmt4 >= 60 then 4 when dpd_soft.max_late_pmt5 >= 60 then 5 when dpd_soft.max_late_pmt6 >= 60 then 6 when dpd_soft.max_late_pmt7 >= 60 then 7 when dpd_soft.max_late_pmt8 >= 60 then 8 when dpd_soft.max_late_pmt9 >= 60 then 9 when dpd_soft.max_late_pmt10 >= 60 then 10 when dpd_soft.max_late_pmt11 >= 60 then 11 when dpd_soft.max_late_pmt12 >= 60 then 12 when dpd_soft.max_late_pmt12 >= 60 then 12 when dpd_soft.max_late_pmt13 >= 60 then 13 when dpd_soft.max_late_pmt14 >= 60 then 14 when dpd_soft.max_late_pmt15 >= 60 then 15 when dpd_soft.max_late_pmt16 >= 60 then 16 when dpd_soft.max_late_pmt17 >= 60 then 17 when dpd_soft.max_late_pmt18 >= 60 then 18 when dpd_soft.max_late_pmt19 >= 60 then 19 when dpd_soft.max_late_pmt20 >= 60 then 20 when dpd_soft.max_late_pmt21 >= 60 then 21 when dpd_soft.max_late_pmt22 >= 60 then 22 when dpd_soft.max_late_pmt23 >= 60 then 23 when dpd_soft.max_late_pmt24 >= 60 then 24 else 0 end as collection_pmt_of_over60,
	case when dpd_soft.max_late_pmt1 >= 60 then 1 when dpd_soft.max_late_pmt2 >= 60 then 2 when dpd_soft.max_late_pmt3 >= 60 then 3 when dpd_soft.max_late_pmt4 >= 60 then 4 when dpd_soft.max_late_pmt5 >= 60 then 5 when dpd_soft.max_late_pmt6 >= 60 then 6 when dpd_soft.max_late_pmt7 >= 60 then 7 when dpd_soft.max_late_pmt8 >= 60 then 8 when dpd_soft.max_late_pmt9 >= 60 then 9 when dpd_soft.max_late_pmt10 >= 60 then 10 when dpd_soft.max_late_pmt11 >= 60 then 11 when dpd_soft.max_late_pmt12 >= 60 then 12 when dpd_soft.max_late_pmt12 >= 60 then 12 when dpd_soft.max_late_pmt13 >= 60 then 13 when dpd_soft.max_late_pmt14 >= 60 then 14 when dpd_soft.max_late_pmt15 >= 60 then 15 when dpd_soft.max_late_pmt16 >= 60 then 16 when dpd_soft.max_late_pmt17 >= 60 then 17 when dpd_soft.max_late_pmt18 >= 60 then 18 when dpd_soft.max_late_pmt19 >= 60 then 19 when dpd_soft.max_late_pmt20 >= 60 then 20 when dpd_soft.max_late_pmt21 >= 60 then 21 when dpd_soft.max_late_pmt22 >= 60 then 22 when dpd_soft.max_late_pmt23 >= 60 then 23 when dpd_soft.max_late_pmt24 >= 60 then 24 else 0 end / lr.number_of_installments as collection_perc_loan_of_over60,
	(coalesce(dpd_soft.max_late_pmt1,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt2,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt3,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt4,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt5,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt6,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt7,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt8,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt9,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt10,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt11,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt12,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt13,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt14,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt15,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt16,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt17,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt18,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt19,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt20,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt21,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt22,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt23,-1) = 0)::int + (coalesce(dpd_soft.max_late_pmt24,-1) = 0)::int as collection_count_pmts_paid_on_time,
	(coalesce(dpd_soft.max_late_pmt1,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt2,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt3,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt4,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt5,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt6,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt7,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt8,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt9,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt10,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt11,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt12,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt13,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt14,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt15,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt16,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt17,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt18,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt19,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt20,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt21,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt22,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt23,0) < 0)::int + (coalesce(dpd_soft.max_late_pmt24,0) < 0)::int as collection_count_pmts_paid_in_advance,
	(coalesce(dpd_soft.max_late_pmt1,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt2,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt3,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt4,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt5,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt6,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt7,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt8,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt9,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt10,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt11,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt12,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt13,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt14,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt15,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt16,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt17,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt18,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt19,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt20,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt21,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt22,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt23,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt24,0) > 0)::int - late_inst as collection_count_pmt_paid_late,
	(((coalesce(dpd_soft.max_late_pmt1,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt2,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt3,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt4,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt5,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt6,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt7,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt8,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt9,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt10,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt11,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt12,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt13,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt14,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt15,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt16,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt17,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt18,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt19,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt20,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt21,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt22,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt23,0) > 0)::int + (coalesce(dpd_soft.max_late_pmt24,0) > 0)::int - late_inst) / lr.number_of_installments >= 0.5)::int as collection_recurring_late_payments,
	count_paga_fora_de_ordem as collection_pmts_paid_out_of_order,
--PMT3 SOFT
	greatest(pmt3_orig_soft.max_late_pmt1,pmt3_orig_soft.max_late_pmt2,pmt3_orig_soft.max_late_pmt3) as collection_ever_by_pmt3_soft,
	(greatest(pmt3_orig_soft.max_late_pmt1,pmt3_orig_soft.max_late_pmt2,pmt3_orig_soft.max_late_pmt3) > 30)::int as collection_ever_30_pmt3_soft,
	(greatest(pmt3_orig_soft.max_late_pmt1,pmt3_orig_soft.max_late_pmt2,pmt3_orig_soft.max_late_pmt3) > 60)::int as collection_ever_60_pmt3_soft,
	case coalesce(pmt3_orig_soft.ultparcpaga,-1) when -1 then pmt3_orig_soft.max_late_pmt1 when 1 then coalesce(pmt3_orig_soft.max_late_pmt2,0) when 2 then coalesce(pmt3_orig_soft.max_late_pmt3,0) else 0 end as collection_over_by_pmt3_soft,
	(case coalesce(pmt3_orig_soft.ultparcpaga,-1) when -1 then pmt3_orig_soft.max_late_pmt1 when 1 then coalesce(pmt3_orig_soft.max_late_pmt2,0) when 2 then coalesce(pmt3_orig_soft.max_late_pmt3,0) else 0 end > 30)::int as collection_over_30_pmt3_soft,
	(case coalesce(pmt3_orig_soft.ultparcpaga,-1) when -1 then pmt3_orig_soft.max_late_pmt1 when 1 then coalesce(pmt3_orig_soft.max_late_pmt2,0) when 2 then coalesce(pmt3_orig_soft.max_late_pmt3,0) else 0 end > 60)::int as collection_over_60_pmt3_soft,
	original_p_and_i - pmt3_orig_soft.total_pago as collection_ead_pmt3_soft,
	pmt3_orig_soft.total_pago as collection_amount_paid_pmt3_soft,
--PMT6 SOFT
	greatest(pmt6_orig_soft.max_late_pmt1,pmt6_orig_soft.max_late_pmt2,pmt6_orig_soft.max_late_pmt3,pmt6_orig_soft.max_late_pmt4,pmt6_orig_soft.max_late_pmt5,pmt6_orig_soft.max_late_pmt6) as collection_ever_by_pmt6_soft,
	(greatest(pmt6_orig_soft.max_late_pmt1,pmt6_orig_soft.max_late_pmt2,pmt6_orig_soft.max_late_pmt3,pmt6_orig_soft.max_late_pmt4,pmt6_orig_soft.max_late_pmt5,pmt6_orig_soft.max_late_pmt6) > 30)::int as collection_ever_30_pmt6_soft,
	(greatest(pmt6_orig_soft.max_late_pmt1,pmt6_orig_soft.max_late_pmt2,pmt6_orig_soft.max_late_pmt3,pmt6_orig_soft.max_late_pmt4,pmt6_orig_soft.max_late_pmt5,pmt6_orig_soft.max_late_pmt6) > 60)::int as collection_ever_60_pmt6_soft,
	case coalesce(pmt6_orig_soft.ultparcpaga,-1) when -1 then pmt6_orig_soft.max_late_pmt1 when 1 then coalesce(pmt6_orig_soft.max_late_pmt2,0) when 2 then coalesce(pmt6_orig_soft.max_late_pmt3,0) when 3 then coalesce(pmt6_orig_soft.max_late_pmt4,0) when 4 then coalesce(pmt6_orig_soft.max_late_pmt5,0) when 5 then coalesce(pmt6_orig_soft.max_late_pmt6,0) else 0 end as collection_over_by_pmt6_soft,
	(case coalesce(pmt6_orig_soft.ultparcpaga,-1) when -1 then pmt6_orig_soft.max_late_pmt1 when 1 then coalesce(pmt6_orig_soft.max_late_pmt2,0) when 2 then coalesce(pmt6_orig_soft.max_late_pmt3,0) when 3 then coalesce(pmt6_orig_soft.max_late_pmt4,0) when 4 then coalesce(pmt6_orig_soft.max_late_pmt5,0) when 5 then coalesce(pmt6_orig_soft.max_late_pmt6,0) else 0 end > 30)::int as collection_over_30_pmt6_soft,
	(case coalesce(pmt6_orig_soft.ultparcpaga,-1) when -1 then pmt6_orig_soft.max_late_pmt1 when 1 then coalesce(pmt6_orig_soft.max_late_pmt2,0) when 2 then coalesce(pmt6_orig_soft.max_late_pmt3,0) when 3 then coalesce(pmt6_orig_soft.max_late_pmt4,0) when 4 then coalesce(pmt6_orig_soft.max_late_pmt5,0) when 5 then coalesce(pmt6_orig_soft.max_late_pmt6,0) else 0 end > 60)::int as collection_over_60_pmt6_soft,
	original_p_and_i - pmt6_orig_soft.total_pago as collection_ead_pmt6_soft,
	pmt6_orig_soft.total_pago as collection_amount_paid_pmt6_soft,
--PMT6 AGGRESSIVE
	greatest(pmt6_orig_aggressive.max_late_pmt1,pmt6_orig_aggressive.max_late_pmt2,pmt6_orig_aggressive.max_late_pmt3,pmt6_orig_aggressive.max_late_pmt4,pmt6_orig_aggressive.max_late_pmt5,pmt6_orig_aggressive.max_late_pmt6) as collection_ever_by_pmt6_aggressive,
	(greatest(pmt6_orig_aggressive.max_late_pmt1,pmt6_orig_aggressive.max_late_pmt2,pmt6_orig_aggressive.max_late_pmt3,pmt6_orig_aggressive.max_late_pmt4,pmt6_orig_aggressive.max_late_pmt5,pmt6_orig_aggressive.max_late_pmt6) > 30)::int as collection_ever_30_pmt6_aggressive,
	(greatest(pmt6_orig_aggressive.max_late_pmt1,pmt6_orig_aggressive.max_late_pmt2,pmt6_orig_aggressive.max_late_pmt3,pmt6_orig_aggressive.max_late_pmt4,pmt6_orig_aggressive.max_late_pmt5,pmt6_orig_aggressive.max_late_pmt6) > 60)::int as collection_ever_60_pmt6_aggressive,
	case coalesce(pmt6_orig_aggressive.ultparcpaga,-1) when -1 then pmt6_orig_aggressive.max_late_pmt1 when 1 then coalesce(pmt6_orig_aggressive.max_late_pmt2,0) when 2 then coalesce(pmt6_orig_aggressive.max_late_pmt3,0) when 3 then coalesce(pmt6_orig_aggressive.max_late_pmt4,0) when 4 then coalesce(pmt6_orig_aggressive.max_late_pmt5,0) when 5 then coalesce(pmt6_orig_aggressive.max_late_pmt6,0) else 0 end as collection_over_by_pmt6_aggressive,
	(case coalesce(pmt6_orig_aggressive.ultparcpaga,-1) when -1 then pmt6_orig_aggressive.max_late_pmt1 when 1 then coalesce(pmt6_orig_aggressive.max_late_pmt2,0) when 2 then coalesce(pmt6_orig_aggressive.max_late_pmt3,0) when 3 then coalesce(pmt6_orig_aggressive.max_late_pmt4,0) when 4 then coalesce(pmt6_orig_aggressive.max_late_pmt5,0) when 5 then coalesce(pmt6_orig_aggressive.max_late_pmt6,0) else 0 end > 30)::int as collection_over_30_pmt6_aggressive,
	(case coalesce(pmt6_orig_aggressive.ultparcpaga,-1) when -1 then pmt6_orig_aggressive.max_late_pmt1 when 1 then coalesce(pmt6_orig_aggressive.max_late_pmt2,0) when 2 then coalesce(pmt6_orig_aggressive.max_late_pmt3,0) when 3 then coalesce(pmt6_orig_aggressive.max_late_pmt4,0) when 4 then coalesce(pmt6_orig_aggressive.max_late_pmt5,0) when 5 then coalesce(pmt6_orig_aggressive.max_late_pmt6,0) else 0 end > 60)::int as collection_over_60_pmt6_aggressive,
	original_p_and_i - pmt6_orig_aggressive.total_pago as collection_ead_pmt6_aggressive,
	pmt6_orig_aggressive.total_pago as collection_amount_paid_pmt6_aggressive,
--PMT50% AGGRESSIVE
	greatest(pmt50pct_orig_aggressive.max_late_pmt1,pmt50pct_orig_aggressive.max_late_pmt2,pmt50pct_orig_aggressive.max_late_pmt3,pmt50pct_orig_aggressive.max_late_pmt4,pmt50pct_orig_aggressive.max_late_pmt5,pmt50pct_orig_aggressive.max_late_pmt6) as collection_ever_by_pmt50pct_aggressive,
	(greatest(pmt50pct_orig_aggressive.max_late_pmt1,pmt50pct_orig_aggressive.max_late_pmt2,pmt50pct_orig_aggressive.max_late_pmt3,pmt50pct_orig_aggressive.max_late_pmt4,pmt50pct_orig_aggressive.max_late_pmt5,pmt50pct_orig_aggressive.max_late_pmt6) > 30)::int as collection_ever_30_pmt50pct_aggressive,
	(greatest(pmt50pct_orig_aggressive.max_late_pmt1,pmt50pct_orig_aggressive.max_late_pmt2,pmt50pct_orig_aggressive.max_late_pmt3,pmt50pct_orig_aggressive.max_late_pmt4,pmt50pct_orig_aggressive.max_late_pmt5,pmt50pct_orig_aggressive.max_late_pmt6) > 60)::int as collection_ever_60_pmt50pct_aggressive,
	case coalesce(pmt50pct_orig_aggressive.ultparcpaga,-1) when -1 then pmt50pct_orig_aggressive.max_late_pmt1 when 1 then coalesce(pmt50pct_orig_aggressive.max_late_pmt2,0) when 2 then coalesce(pmt50pct_orig_aggressive.max_late_pmt3,0) when 3 then coalesce(pmt50pct_orig_aggressive.max_late_pmt4,0) when 4 then coalesce(pmt50pct_orig_aggressive.max_late_pmt5,0) when 5 then coalesce(pmt50pct_orig_aggressive.max_late_pmt6,0) when 6 then coalesce(pmt50pct_orig_aggressive.max_late_pmt7,0) when 7 then coalesce(pmt50pct_orig_aggressive.max_late_pmt8,0) when 8 then coalesce(pmt50pct_orig_aggressive.max_late_pmt9,0) when 9 then coalesce(pmt50pct_orig_aggressive.max_late_pmt10,0) when 10 then coalesce(pmt50pct_orig_aggressive.max_late_pmt11,0) when 11 then coalesce(pmt50pct_orig_aggressive.max_late_pmt12,0) else 0 end as collection_over_by_pmt50pct_aggressive,
	(case coalesce(pmt50pct_orig_aggressive.ultparcpaga,-1) when -1 then pmt50pct_orig_aggressive.max_late_pmt1 when 1 then coalesce(pmt50pct_orig_aggressive.max_late_pmt2,0) when 2 then coalesce(pmt50pct_orig_aggressive.max_late_pmt3,0) when 3 then coalesce(pmt50pct_orig_aggressive.max_late_pmt4,0) when 4 then coalesce(pmt50pct_orig_aggressive.max_late_pmt5,0) when 5 then coalesce(pmt50pct_orig_aggressive.max_late_pmt6,0) when 6 then coalesce(pmt50pct_orig_aggressive.max_late_pmt7,0) when 7 then coalesce(pmt50pct_orig_aggressive.max_late_pmt8,0) when 8 then coalesce(pmt50pct_orig_aggressive.max_late_pmt9,0) when 9 then coalesce(pmt50pct_orig_aggressive.max_late_pmt10,0) when 10 then coalesce(pmt50pct_orig_aggressive.max_late_pmt11,0) when 11 then coalesce(pmt50pct_orig_aggressive.max_late_pmt12,0) else 0 end > 30)::int as collection_over_30_pmt50pct_aggressive,
	(case coalesce(pmt50pct_orig_aggressive.ultparcpaga,-1) when -1 then pmt50pct_orig_aggressive.max_late_pmt1 when 1 then coalesce(pmt50pct_orig_aggressive.max_late_pmt2,0) when 2 then coalesce(pmt50pct_orig_aggressive.max_late_pmt3,0) when 3 then coalesce(pmt50pct_orig_aggressive.max_late_pmt4,0) when 4 then coalesce(pmt50pct_orig_aggressive.max_late_pmt5,0) when 5 then coalesce(pmt50pct_orig_aggressive.max_late_pmt6,0)  when 6 then coalesce(pmt50pct_orig_aggressive.max_late_pmt7,0) when 7 then coalesce(pmt50pct_orig_aggressive.max_late_pmt8,0) when 8 then coalesce(pmt50pct_orig_aggressive.max_late_pmt9,0) when 9 then coalesce(pmt50pct_orig_aggressive.max_late_pmt10,0) when 10 then coalesce(pmt50pct_orig_aggressive.max_late_pmt11,0) when 11 then coalesce(pmt50pct_orig_aggressive.max_late_pmt12,0) else 0 end > 60)::int as collection_over_60_pmt50pct_aggressive,
	original_p_and_i - pmt50pct_orig_aggressive.total_pago as collection_ead_pmt50pct_aggressive,
	pmt50pct_orig_aggressive.total_pago as collection_amount_paid_pmt50pct_aggressive,
--Promessas pagamento
	coalesce(total_promessas,0) as collection_payment_promises_total,
	coalesce(total_promessas_cumpridas,0) as collection_payment_promises_fulfilled,
	coalesce(total_promessas_quebradas,0) as collection_payment_promises_broken,
	coalesce(total_promessas_canceladas,0) as collection_payment_promises_cancelled,
	coalesce(total_promessas_ativas,0) as collection_payment_promises_active,
--Historico negativacao
	coalesce(total_negativacoes,0) as collection_total_npr,
	coalesce(total_negativacoes_com_erro,0) as collection_npr_w_error,
	coalesce(total_negativacoes_sem_erro,0) as collection_npr_wo_error,
	coalesce(total_negativacoes_empresa,0) as collection_total_company_npr,
	coalesce(total_negativacoes_avalistas,0) as collection_total_guarantor_npr,
	coalesce(total_negativacoes_empresa_sem_erro,0) as collection_company_wo_error_npr,
	coalesce(total_negativacoes_avalistas_sem_erro,0) as collection_guarantor_wo_error_npr,
	coalesce(total_negativacao_parcelas,0) as collection_total_pmt_npr,
	coalesce(total_negativacao_parcelas_sem_erro,0) as collection_pmt_wo_error_npr,
--------------------------SERASA
	n_cnpjs_relacionados as experian_number_of_related_companies,
	n_cpfs_relacionados as experian_number_of_related_shareholders,
	is_shareholder as experian_is_shareholder,
	count_socios as experian_count_shareholders,	
--CONSULTAS SERASA EMPRESAS
	consulta_factoring as experian_report_recently_taken_by_factoring,
--------------------------SIGNERS
	MajorSigner_Age as signers_MajorShareholder_Age, 
	MajorSigner_Revenue as signers_MajorShareholder_Revenue,
	MajorSigner_civil_status as signers_MajorShareholder_Maritial_Status,
	MajorSigner_gender as signers_MajorShareholdergender,
	SolicSignersCPF_Age as signers_RequesterSigner_Age,
	SolicSignersCPF_Revenue as signers_RequesterSigner_Revenue,
	SolicSignerCPF_civil_status as signers_RequesterSigner_Maritial_Status,
	SolicSignerCPF_gender as signers_RequesterSigner_gender,
	OldestSh_Age as signers_OldestSh_Age,
	OldestSh_Revenue as signers_OldestSh_Revenue,
	OldestSh_civil_status as signers_OldestSh_Maritial_status,
	OldestSh_gender as signers_OldestSh_gender,
	AdminSigner_Age as signers_AdminSigner_Age,
	AdminSigner_Revenue as signers_AdminSigner_Revenue,
	AdminSigner_civil_status as signers_AdminSigner_Maritial_status,
	AdminSigner_gender as signers_AdminSigner_gender,
	avg_age_partners as signers_avg_age_partners,
	avg_revenue_partners as signers_avg_revenue_partners,
	male_partners as signers_male_shareholders,
	female_partners as signers_female_shareholders,
	count_socios_admin as signers_number_of_shareholders,
	count_avalistas as signers_number_of_guarantors,
	count_total as signers_total_shs_and_guarantors,
--------------------------CHECKLIST
	social_contract_confirmed as checklist_social_contract_confirmed,
	company_address_confirmed as checklist_company_address_confirmed,
	bank_statements_confirmed as checklist_bank_statements_confirmed,
	company_account_confirmed as checklist_company_account_confirmed,
	personal_rg as checklist_shareholder_rg,
	personal_cpf as checklist_shareholder_cpf,
	proof_personal_address as checklist_personal_address_confirmed,
	proof_income as checklist_shareholder_income_confirmed,
	spouse_info as checklist_shareholder_spouse_info,
	proved_informed_revenue as checklist_proved_informed_revenue,
	positive_balance_account as checklist_positive_balance_account,
	low_negative_balance as checklist_low_negative_balance,
	largest_cashflow_company_account as checklist_largest_cashflow_company_account,
	continuous_income as checklist_continuous_income,
	income_unrelated_government as checklist_income_unrelated_government,
	diversified_clients as checklist_diversified_clients,
	only_company_expenses as checklist_only_company_expenses,
	different_personal_company_address as checklist_different_personal_company_address,
	dilligent_credit_simulation as checklist_dilligent_credit_simulation,
	requested_lessthan_offered as checklist_requested_lessthan_offered,
	company_usedto_credit as checklist_company_usedto_credit,
	partner_has_assets as checklist_partner_has_assets,
	low_personal_debt as checklist_low_personal_debt,
	percent_documentation as checklist_percent_documentation,
	score_documentation as checklist_score,
--------------------------TAGS
	app_auto as tags_approved_automatically,
	offer_auto as tags_offered_automatically,
	interest_rate_test1 as tags_interest_rate_test_scope,
--------------------------REJECT REASON
	case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then coalesce("Structure complexity",0) else "Structure complexity" end as reject_reason_Structure_complexity,
	case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then coalesce("Bad payment record (Experian/SCR)",0) else "Bad payment record (Experian/SCR)" end as reject_reason_bad_pmt_record_experian_scr,
	case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then coalesce("Low income (bank stmts)",0) else "Low income (bank stmts)" end as reject_reason_low_income_bnk_stmts,
	case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then coalesce("Bad payment record (bank stmts)",0) else "Bad payment record (bank stmts)" end as reject_reason_bad_pmt_record_bnk_stmts,
	case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then coalesce("Policy: low Experian score",0) else "Policy: low Experian score" end as reject_reason_policy_low_experian_score,
	case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then coalesce("Income descrease",0) else "Income descrease" end as reject_reason_income_decrease,
	case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then coalesce("Fraud suspicion",0) else "Fraud suspicion" end as reject_reason_fraud_suspicion,
	case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then coalesce("Policy: social contract change",0) else "Policy: social contract change" end as reject_reason_policy_social_contract_change,
	case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then coalesce("Low margin",0) else "Low margin" end as reject_reason_low_margin,
	case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then coalesce("Policy: company activity",0) else "Policy: company activity" end as reject_reason_policy_company_activity,
	case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then coalesce("Policy: money use",0) else "Policy: money use" end as reject_reason_policy_money_use,
	case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then coalesce("Info/consent denial",0) else "Info/consent denial" end as reject_reason_info_consent_denial,
	case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then coalesce("High indebtness due to recent borrow (bank stmts)",0) else "High indebtness due to recent borrow (bank stmts)" end as reject_reason_high_debt_recent_borrow,
	case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then coalesce("Low LT score",0) else "Low LT score" end as reject_reason_low_LT_score,
	case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then coalesce("Policy: shareholder change",0) else "Policy: shareholder change" end as reject_reason_policy_recent_shareholder_change,
	case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then coalesce("High indebtness due to low income (bank stmts)",0) else "High indebtness due to low income (bank stmts)" end as reject_reason_high_debt_low_income,
	case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then coalesce("Justice issues",0) else "Justice issues" end as reject_reason_justice_issues,
	case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then coalesce("No bank account for company",0) else "No bank account for company" end as reject_reason_lack_bank_account,
	case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then coalesce("Documentation issues",0) else "Documentation issues" end as reject_reason_documentation_issues,
	case when to_char(lr.date_inserted,'yyyy')::int >= 2018 then coalesce("Policy: less than 1 yo",0) else "Policy: less than 1 yo" end as reject_reason_policy_less_1yo,
--------------------------PARECER DE CREDITO
	"Alçada 1 - data" as credit_underwrite_A1_date,
	"Alçada 1 - analista" as credit_underwrite_A1_analyst,
	"Alçada 1 - tipo" as credit_underwrite_A1_type,
	"Alçada 1 - status" as credit_underwrite_A1_status,
	"Alçada 1 - confiança" as credit_underwrite_A1_trust_score,
	"Alçada 1 - limite" as credit_underwrite_A1_value,
	"Alçada 1 - taxa" as credit_underwrite_A1_interest_rate,
	"Alçada 1 - prazo" as credit_underwrite_A1_term
--------------------------
from direct_prospects dp
join clients c on c.client_id = dp.client_id
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join (select o.* ,count_neg from offers o join (select direct_prospect_id,min(offer_id) as min_id,count(*) as count_neg from offers group by 1) as initial_off on initial_off.min_id = o.offer_id) as initialoff on initialoff.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
left join loan_infos li on li.loan_info_id = lr.loan_request_id
left join bank_accounts ba on ba.bank_account_id = lr.bank_account_id
left join lucas_leal.geofusion_data gd on gd.pk = dp.state || dp.city
left join backtests.s4_s6_bizu_20190923 as bktest on bktest.lead_id = dp.direct_prospect_id
left join (select distinct identificacao from parceiros_parceiro) pp on pp.identificacao = replace(dp.utm_source,'#/','')
--------------------------TABLE PARCELAS
left join(select lr.loan_request_id,
		count(*) filter (where fp.numero_ajustado <> fp.numero and fp.pago_em is not null) as count_paga_fora_de_ordem,	
		count(*) as current_term,
		min(fp.pago_em) as first_paid_inst_paid_on,
		min(fp.pago_em) filter (where fp.numero_ajustado = 2) as second_paid_inst_paid_on,
		sum(fp.pago) filter (where fp.status like 'Pago%') as total_paid_amount, 
		sum(fp.cobrado) filter (where fp.status = 'Ativo' and fp.vencimento_em < current_date) as total_late_amount, 
		max(fp.cobrado) filter (where fp.status = 'Ativo') as pmt_atual,
		sum(fp.cobrado) as curr_p_and_i,
		max(original_p_and_i) as original_p_and_i,
		sum(fp.cobrado) filter (where fp.pago_em is null) as EAD, 
		count(*) filter (where fp.status like 'Pago%') as paid_inst,
		count(*) filter (where fp.status = 'Ativo' and fp.vencimento_em < current_date) as late_inst, 
		count(*) filter (where fp.vencimento_em < current_date) as due_inst,	
		count(*) filter (where fp.vencimento_em >= current_date) as to_come_inst,	
		min(fp.vencimento_em) filter (where fp.pago_em is null) as current_inst_due_to,
		sum(fp.cobrado) filter (where pago_em is null and current_date - fp.vencimento_em > 180) as total_amount_over_180,
		max(original_1st_pmt_due_to) as original_1st_pmt_due_to,
		max(original_2nd_pmt_due_to) as original_2nd_pmt_due_to,
		max(ParcSubst) as primeira_parcela_renegociada,
		min(fp.numero_ajustado) filter (where fp.pago_em is null) as parc_em_aberto
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	--TABELA PARA BUSCAR A DATA DE PAGAMENTO DAS PARCELAS NA ORDEM EM QUE ELAS OCORRERAM
	join (select row_number() over (partition by fpp.emprestimo_id order by fp.pago_em,fp.vencimento_em) as numero_ajustado,fp.* 
		from financeiro_parcela fp 
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id 
		where fp.status not in ('Cancelada','EmEspera')) fp on fp.plano_id = fpp.id
	--TABELA PARA BUSCAR A PARCELA E O VENCIMENTO DO PLANO QUE FOI SUBSTITUIDO
	left join(select fpp.emprestimo_id, min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada' and fpp.status not in ('Cancelado','EmEspera')
		group by 1) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	--TABELA PARA BUSCAR INFORMAÇÕES DO PLANO ORIGINAL
	join(select fpp.emprestimo_id, 
			sum(fp.cobrado) as original_p_and_i, 
			max(fp.vencimento_em) filter (where fp.numero = 1) as original_1st_pmt_due_to, 
			max(fp.vencimento_em) filter (where fp.numero = 2) as original_2nd_pmt_due_to
		from financeiro_planopagamento fpp  
		join (select emprestimo_id, min(id) as plano_id from financeiro_planopagamento group by 1) as fpp_orig on fpp_orig.plano_id = fpp.id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		group by 1) vencimentos on vencimentos.emprestimo_id = lr.loan_request_id
	where fp.status not in ('Cancelada','EmEspera')
	group by 1) as parcelas on parcelas.loan_request_id = lr.loan_request_id
--------------------------TABLE PLANO PAGAMENTO
left join(select lr.loan_request_id, 
		max(case when fpp.status = 'Pago' then 1 else 0 end) as fully_paid,
		count(*) filter (where fpp.parcelas_qtd = 1 and fpp.status = 'Pago') as antecipou_emprestimo,
		count(*) - 1 as count_renegotiation,
		count(*) filter (where fpp.status not in ('Cancelado','EmEspera')) - 1 as count_unbroken_renegotiations,
		count(*) filter (where fpp.status = 'Cancelado') as count_broken_renegotiation,
		min(fpp.inicio_em) filter (where fpp.inicio_em > lr.loan_date + interval '5 days') as data_primeira_renegociacao
	from loan_requests lr  
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	group by 1) as planopagamento on planopagamento.loan_request_id = lr.loan_request_id
--------------------------TABLE DPD SOFT
left join(select lr.loan_request_id,
		max((case when fp.numero_ajustado <> 1 then null else (case when ParcSubst = 1 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt1_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt1_venc else null end) else (case when fp.pago_em is null then current_date - pmt1_venc else fp.pago_em - pmt1_venc end) end) end) end)) as max_late_pmt1,
		max((case when fp.numero_ajustado <> 2 then null else (case when ParcSubst = 2 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt2_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt2_venc else null end) else (case when fp.pago_em is null then current_date - pmt2_venc else fp.pago_em - pmt2_venc end) end) end) end)) as max_late_pmt2,
		max((case when fp.numero_ajustado <> 3 then null else (case when ParcSubst = 3 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt3_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt3_venc else null end) else (case when fp.pago_em is null then current_date - pmt3_venc else fp.pago_em - pmt3_venc end) end) end) end)) as max_late_pmt3,
		max((case when fp.numero_ajustado <> 4 then null else (case when ParcSubst = 4 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt4_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt4_venc else null end) else (case when fp.pago_em is null then current_date - pmt4_venc else fp.pago_em - pmt4_venc end) end) end) end)) as max_late_pmt4,
		max((case when fp.numero_ajustado <> 5 then null else (case when ParcSubst = 5 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt5_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt5_venc else null end) else (case when fp.pago_em is null then current_date - pmt5_venc else fp.pago_em - pmt5_venc end) end) end) end)) as max_late_pmt5,
		max((case when fp.numero_ajustado <> 6 then null else (case when ParcSubst = 6 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt6_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt6_venc else null end) else (case when fp.pago_em is null then current_date - pmt6_venc else fp.pago_em - pmt6_venc end) end) end) end)) as max_late_pmt6,
		max((case when fp.numero_ajustado <> 7 then null else (case when ParcSubst = 7 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt7_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt7_venc else null end) else (case when fp.pago_em is null then current_date - pmt7_venc else fp.pago_em - pmt7_venc end) end) end) end)) as max_late_pmt7,
		max((case when fp.numero_ajustado <> 8 then null else (case when ParcSubst = 8 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt8_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt8_venc else null end) else (case when fp.pago_em is null then current_date - pmt8_venc else fp.pago_em - pmt8_venc end) end) end) end)) as max_late_pmt8,
		max((case when fp.numero_ajustado <> 9 then null else (case when ParcSubst = 9 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt9_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt9_venc else null end) else (case when fp.pago_em is null then current_date - pmt9_venc else fp.pago_em - pmt9_venc end) end) end) end)) as max_late_pmt9,
		max((case when fp.numero_ajustado <> 10 then null else (case when ParcSubst = 10 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt10_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt10_venc else null end) else (case when fp.pago_em is null then current_date - pmt10_venc else fp.pago_em - pmt10_venc end) end) end) end)) as max_late_pmt10,
		max((case when fp.numero_ajustado <> 11 then null else (case when ParcSubst = 11 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt11_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt11_venc else null end) else (case when fp.pago_em is null then current_date - pmt11_venc else fp.pago_em - pmt11_venc end) end) end) end)) as max_late_pmt11,
		max((case when fp.numero_ajustado <> 12 then null else (case when ParcSubst = 12 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt12_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt12_venc else null end) else (case when fp.pago_em is null then current_date - pmt12_venc else fp.pago_em - pmt12_venc end) end) end) end)) as max_late_pmt12,
		max((case when fp.numero_ajustado <> 13 then null else (case when ParcSubst = 13 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt13_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt13_venc else null end) else (case when fp.pago_em is null then current_date - pmt13_venc else fp.pago_em - pmt13_venc end) end) end) end)) as max_late_pmt13,
		max((case when fp.numero_ajustado <> 14 then null else (case when ParcSubst = 14 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt14_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt14_venc else null end) else (case when fp.pago_em is null then current_date - pmt14_venc else fp.pago_em - pmt14_venc end) end) end) end)) as max_late_pmt14,
		max((case when fp.numero_ajustado <> 15 then null else (case when ParcSubst = 15 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt15_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt15_venc else null end) else (case when fp.pago_em is null then current_date - pmt15_venc else fp.pago_em - pmt15_venc end) end) end) end)) as max_late_pmt15,
		max((case when fp.numero_ajustado <> 16 then null else (case when ParcSubst = 16 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt16_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt16_venc else null end) else (case when fp.pago_em is null then current_date - pmt16_venc else fp.pago_em - pmt16_venc end) end) end) end)) as max_late_pmt16,
		max((case when fp.numero_ajustado <> 17 then null else (case when ParcSubst = 17 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt17_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt17_venc else null end) else (case when fp.pago_em is null then current_date - pmt17_venc else fp.pago_em - pmt17_venc end) end) end) end)) as max_late_pmt17,
		max((case when fp.numero_ajustado <> 18 then null else (case when ParcSubst = 18 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt18_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt18_venc else null end) else (case when fp.pago_em is null then current_date - pmt18_venc else fp.pago_em - pmt18_venc end) end) end) end)) as max_late_pmt18,
		max((case when fp.numero_ajustado <> 19 then null else (case when ParcSubst = 19 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt19_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt19_venc else null end) else (case when fp.pago_em is null then current_date - pmt19_venc else fp.pago_em - pmt19_venc end) end) end) end)) as max_late_pmt19,
		max((case when fp.numero_ajustado <> 20 then null else (case when ParcSubst = 20 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt20_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt20_venc else null end) else (case when fp.pago_em is null then current_date - pmt20_venc else fp.pago_em - pmt20_venc end) end) end) end)) as max_late_pmt20,
		max((case when fp.numero_ajustado <> 21 then null else (case when ParcSubst = 21 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt21_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt21_venc else null end) else (case when fp.pago_em is null then current_date - pmt21_venc else fp.pago_em - pmt21_venc end) end) end) end)) as max_late_pmt21,
		max((case when fp.numero_ajustado <> 22 then null else (case when ParcSubst = 22 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt22_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt22_venc else null end) else (case when fp.pago_em is null then current_date - pmt22_venc else fp.pago_em - pmt22_venc end) end) end) end)) as max_late_pmt22,
		max((case when fp.numero_ajustado <> 23 then null else (case when ParcSubst = 23 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt23_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt23_venc else null end) else (case when fp.pago_em is null then current_date - pmt23_venc else fp.pago_em - pmt23_venc end) end) end) end)) as max_late_pmt23,
		max((case when fp.numero_ajustado <> 24 then null else (case when ParcSubst = 24 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt24_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt24_venc else null end) else (case when fp.pago_em is null then current_date - pmt24_venc else fp.pago_em - pmt24_venc end) end) end) end)) as max_late_pmt24
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	--TABELA PARA BUSCAR A DATA DE PAGAMENTO DAS PARCELAS NA ORDEM EM QUE ELAS OCORRERAM
	join (select row_number() over (partition by fpp.emprestimo_id order by fp.pago_em,fp.vencimento_em) as numero_ajustado,fp.* 
		from financeiro_parcela fp 
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id 
		where fp.status not in ('Cancelada','EmEspera')) fp on fp.plano_id = fpp.id
	--TABELA PARA BUSCAR A PARCELA E O VENCIMENTO DO PLANO QUE FOI SUBSTITUIDO
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada' and fpp.status not in ('Cancelado','EmEspera')
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	--TABELA PARA BUSCAR A DATA DE VENCIMENTO DAS PARCELAS NA ORDEM EM QUE ELAS FORAM CONTRATADAS
	join(select fpp.emprestimo_id,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 2 and fpp.id = min_plano.min_id) as pmt2_orig_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 1 and fpp.id = min_plano.min_id) as pmt1_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 2 and fp.status not in ('Cancelada','EmEspera')) as pmt2_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 3 and fp.status not in ('Cancelada','EmEspera')) as pmt3_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 4 and fp.status not in ('Cancelada','EmEspera')) as pmt4_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 5 and fp.status not in ('Cancelada','EmEspera')) as pmt5_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 6 and fp.status not in ('Cancelada','EmEspera')) as pmt6_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 7 and fp.status not in ('Cancelada','EmEspera')) as pmt7_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 8 and fp.status not in ('Cancelada','EmEspera')) as pmt8_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 9 and fp.status not in ('Cancelada','EmEspera')) as pmt9_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 10 and fp.status not in ('Cancelada','EmEspera')) as pmt10_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 11 and fp.status not in ('Cancelada','EmEspera')) as pmt11_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 12 and fp.status not in ('Cancelada','EmEspera')) as pmt12_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 13 and fp.status not in ('Cancelada','EmEspera')) as pmt13_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 14 and fp.status not in ('Cancelada','EmEspera')) as pmt14_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 15 and fp.status not in ('Cancelada','EmEspera')) as pmt15_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 16 and fp.status not in ('Cancelada','EmEspera')) as pmt16_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 17 and fp.status not in ('Cancelada','EmEspera')) as pmt17_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 18 and fp.status not in ('Cancelada','EmEspera')) as pmt18_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 19 and fp.status not in ('Cancelada','EmEspera')) as pmt19_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 20 and fp.status not in ('Cancelada','EmEspera')) as pmt20_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 21 and fp.status not in ('Cancelada','EmEspera')) as pmt21_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 22 and fp.status not in ('Cancelada','EmEspera')) as pmt22_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 23 and fp.status not in ('Cancelada','EmEspera')) as pmt23_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 24 and fp.status not in ('Cancelada','EmEspera')) as pmt24_venc,
			sum(fp.cobrado) filter (where fpp.id = min_plano.min_id) as original_p_and_i
		from financeiro_planopagamento fpp
		join (select row_number() over (partition by fpp.emprestimo_id order by fp.vencimento_em) as numero_ajustado,fp.* from financeiro_parcela fp join financeiro_planopagamento fpp on fpp.id = fp.plano_id where fp.status not in ('Cancelada','EmEspera')) as fp on fp.plano_id = fpp.id
		join (select emprestimo_id,min(id) as min_id from financeiro_planopagamento group by 1) as min_plano on min_plano.emprestimo_id = fpp.emprestimo_id
		group by 1) vencimentos on vencimentos.emprestimo_id = lr.loan_request_id
	where fp.status not in ('Cancelada','EmEspera')
	group by 1) as DPD_Soft on DPD_Soft.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO PMT 3 PLANO ORIGINAL SOFT
left join(select lr.loan_request_id,
		max(fp.numero_ajustado) filter (where fp.pago_em <= coalesce(max_venc3p,max_venc3m)) as ultparcpaga,
		sum(fp.pago) filter (where fp.pago_em <= coalesce(max_venc3p,max_venc3m)) as total_pago,
		max((case when fp.numero_ajustado <> 1 then null else (case when ParcSubst = 1 and coalesce(ignorar,1) > 0 then (case when fp.pago_em is null or fp.pago_em > coalesce(max_venc3p,max_venc3m) then greatest(least(coalesce(max_venc3p,max_venc3m),current_date) - vencparcsubst,0) else fp.pago_em - VencParcSubst end) else (case when least(coalesce(max_venc3p,max_venc3m),current_date) < pmt1_venc then (case when fp.pago_em < least(coalesce(max_venc3p,max_venc3m),current_date) then fp.pago_em - pmt1_venc else null end) else (case when fp.pago_em is null then least(coalesce(max_venc3p,max_venc3m),current_date) - pmt1_venc else least(coalesce(max_venc3p,max_venc3m),fp.pago_em) - pmt1_venc end) end) end) end)) as max_late_pmt1,
		max((case when fp.numero_ajustado <> 2 then null else (case when ParcSubst = 2 and coalesce(ignorar,1) > 0 then (case when fp.pago_em is null or fp.pago_em > coalesce(max_venc3p,max_venc3m) then greatest(least(coalesce(max_venc3p,max_venc3m),current_date) - vencparcsubst,0) else fp.pago_em - VencParcSubst end) else (case when least(coalesce(max_venc3p,max_venc3m),current_date) < pmt2_venc then (case when fp.pago_em < least(coalesce(max_venc3p,max_venc3m),current_date) then fp.pago_em - pmt2_venc else null end) else (case when fp.pago_em is null then least(coalesce(max_venc3p,max_venc3m),current_date) - pmt2_venc else least(coalesce(max_venc3p,max_venc3m),fp.pago_em) - pmt2_venc end) end) end) end)) as max_late_pmt2,	
		max((case when fp.numero_ajustado <> 3 then null else (case when ParcSubst = 3 and coalesce(ignorar,1) > 0 then (case when fp.pago_em is null or fp.pago_em > coalesce(max_venc3p,max_venc3m) then greatest(least(coalesce(max_venc3p,max_venc3m),current_date) - vencparcsubst,0) else fp.pago_em - VencParcSubst end) else (case when least(coalesce(max_venc3p,max_venc3m),current_date) < pmt3_venc then (case when fp.pago_em < least(coalesce(max_venc3p,max_venc3m),current_date) then fp.pago_em - pmt3_venc else null end) else (case when fp.pago_em is null then least(coalesce(max_venc3p,max_venc3m),current_date) - pmt3_venc else least(coalesce(max_venc3p,max_venc3m),fp.pago_em) - pmt3_venc end) end) end) end)) as max_late_pmt3	
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	--TABELA PARA BUSCAR A PARCELA E O VENCIMENTO DO PLANO QUE FOI SUBSTITUIDO
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada' and fp.numero <= 3
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	--TABELA PARA INFORMAR OS PLANOS DE PAGAMENTO QUE DEVEM SER IGNORADOS (NÃO TIVERAM NENHUMA PARCELA PAGA E NÃO SÃO O PRIMEIRO PLANO)
	left join(select fpp.id,count(*) filter (where fp.status <> 'Cancelada') as ignorar
		from financeiro_planopagamento fpp
		join (select emprestimo_id,min(id) as min_id from financeiro_planopagamento group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		where fpp.id <> t1.min_id
		group by 1) as ignorados on ignorados.id = t2.plano_id
	--TABELA PARA BUSCAR A DATA DA FOTOGRAFIA
	join(select fpp.emprestimo_id, 
			case when min_id is null then (lr.loan_date + interval '3 months')::date end as max_venc3M , 
			max(fp.vencimento_em) filter (where fp.numero <= 3 and fpp.id = t1.min_id) as max_venc3P,
			count(*) filter (where fpp.status = 'Pago') as quitado
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, min(fpp.id) filter (where fp.numero = 3 and fpp.status <> 'EmEspera') as min_id
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1,2) as max_venc_3M on max_venc_3M.emprestimo_id = fpp.emprestimo_id 
	--TABELA PARA BUSCAR A DATA DE PAGAMENTO DAS PARCELAS NA ORDEM EM QUE ELAS OCORRERAM
	join (select row_number() over (partition by fpp.emprestimo_id order by fp.pago_em,fp.vencimento_em) as numero_ajustado,fp.* from financeiro_parcela fp join financeiro_planopagamento fpp on fpp.id = fp.plano_id where fp.status not in ('Cancelada','EmEspera')) fp on fp.plano_id = fpp.id
	--TABELA PARA BUSCAR A DATA DE VENCIMENTO DAS PARCELAS NA ORDEM EM QUE ELAS FORAM CONTRATADAS
	join(select fpp.emprestimo_id,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 1 and fpp.id = min_plano.min_id) as pmt1_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 2 and fp.status not in ('Cancelada','EmEspera')) as pmt2_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 3 and fp.status not in ('Cancelada','EmEspera')) as pmt3_venc
		from financeiro_planopagamento fpp
		join (select row_number() over (partition by fpp.emprestimo_id order by fp.vencimento_em) as numero_ajustado,fp.* from financeiro_parcela fp join financeiro_planopagamento fpp on fpp.id = fp.plano_id where fp.status not in ('Cancelada','EmEspera')) as fp on fp.plano_id = fpp.id
		join (select emprestimo_id,min(id) as min_id from financeiro_planopagamento group by 1) as min_plano on min_plano.emprestimo_id = fpp.emprestimo_id
		group by 1) vencimentos on vencimentos.emprestimo_id = lr.loan_request_id
	where fp.status not in ('Cancelada','EmEspera') and (coalesce(max_venc3p,max_venc3m) <= current_date or quitado > 0)
	group by 1) as PMT3_orig_soft on PMT3_orig_soft.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO PMT 6 PLANO ORIGINAL SOFT
left join(select lr.loan_request_id,
		max(fp.numero_ajustado) filter (where fp.pago_em <= coalesce(max_venc6p,max_venc6m)) as ultparcpaga,
		sum(fp.pago) filter (where fp.pago_em <= coalesce(max_venc6p,max_venc6m)) as total_pago,
		max((case when fp.numero_ajustado <> 1 then null else (case when ParcSubst = 1 and coalesce(ignorar,1) > 0 then (case when fp.pago_em is null or fp.pago_em > coalesce(max_venc6p,max_venc6m) then greatest(least(coalesce(max_venc6p,max_venc6m),current_date) - vencparcsubst,0) else fp.pago_em - VencParcSubst end) else (case when least(coalesce(max_venc6p,max_venc6m),current_date) < pmt1_venc then (case when fp.pago_em < least(coalesce(max_venc6p,max_venc6m),current_date) then fp.pago_em - pmt1_venc else null end) else (case when fp.pago_em is null then least(coalesce(max_venc6p,max_venc6m),current_date) - pmt1_venc else least(coalesce(max_venc6p,max_venc6m),fp.pago_em) - pmt1_venc end) end) end) end)) as max_late_pmt1,
		max((case when fp.numero_ajustado <> 2 then null else (case when ParcSubst = 2 and coalesce(ignorar,1) > 0 then (case when fp.pago_em is null or fp.pago_em > coalesce(max_venc6p,max_venc6m) then greatest(least(coalesce(max_venc6p,max_venc6m),current_date) - vencparcsubst,0) else fp.pago_em - VencParcSubst end) else (case when least(coalesce(max_venc6p,max_venc6m),current_date) < pmt2_venc then (case when fp.pago_em < least(coalesce(max_venc6p,max_venc6m),current_date) then fp.pago_em - pmt2_venc else null end) else (case when fp.pago_em is null then least(coalesce(max_venc6p,max_venc6m),current_date) - pmt2_venc else least(coalesce(max_venc6p,max_venc6m),fp.pago_em) - pmt2_venc end) end) end) end)) as max_late_pmt2,	
		max((case when fp.numero_ajustado <> 3 then null else (case when ParcSubst = 3 and coalesce(ignorar,1) > 0 then (case when fp.pago_em is null or fp.pago_em > coalesce(max_venc6p,max_venc6m) then greatest(least(coalesce(max_venc6p,max_venc6m),current_date) - vencparcsubst,0) else fp.pago_em - VencParcSubst end) else (case when least(coalesce(max_venc6p,max_venc6m),current_date) < pmt3_venc then (case when fp.pago_em < least(coalesce(max_venc6p,max_venc6m),current_date) then fp.pago_em - pmt3_venc else null end) else (case when fp.pago_em is null then least(coalesce(max_venc6p,max_venc6m),current_date) - pmt3_venc else least(coalesce(max_venc6p,max_venc6m),fp.pago_em) - pmt3_venc end) end) end) end)) as max_late_pmt3,	
		max((case when fp.numero_ajustado <> 4 then null else (case when ParcSubst = 4 and coalesce(ignorar,1) > 0 then (case when fp.pago_em is null or fp.pago_em > coalesce(max_venc6p,max_venc6m) then greatest(least(coalesce(max_venc6p,max_venc6m),current_date) - vencparcsubst,0) else fp.pago_em - VencParcSubst end) else (case when least(coalesce(max_venc6p,max_venc6m),current_date) < pmt4_venc then (case when fp.pago_em < least(coalesce(max_venc6p,max_venc6m),current_date) then fp.pago_em - pmt4_venc else null end) else (case when fp.pago_em is null then least(coalesce(max_venc6p,max_venc6m),current_date) - pmt4_venc else least(coalesce(max_venc6p,max_venc6m),fp.pago_em) - pmt4_venc end) end) end) end)) as max_late_pmt4,
		max((case when fp.numero_ajustado <> 5 then null else (case when ParcSubst = 5 and coalesce(ignorar,1) > 0 then (case when fp.pago_em is null or fp.pago_em > coalesce(max_venc6p,max_venc6m) then greatest(least(coalesce(max_venc6p,max_venc6m),current_date) - vencparcsubst,0) else fp.pago_em - VencParcSubst end) else (case when least(coalesce(max_venc6p,max_venc6m),current_date) < pmt5_venc then (case when fp.pago_em < least(coalesce(max_venc6p,max_venc6m),current_date) then fp.pago_em - pmt5_venc else null end) else (case when fp.pago_em is null then least(coalesce(max_venc6p,max_venc6m),current_date) - pmt5_venc else least(coalesce(max_venc6p,max_venc6m),fp.pago_em) - pmt5_venc end) end) end) end)) as max_late_pmt5,	
		max((case when fp.numero_ajustado <> 6 then null else (case when ParcSubst = 6 and coalesce(ignorar,1) > 0 then (case when fp.pago_em is null or fp.pago_em > coalesce(max_venc6p,max_venc6m) then greatest(least(coalesce(max_venc6p,max_venc6m),current_date) - vencparcsubst,0) else fp.pago_em - VencParcSubst end) else (case when least(coalesce(max_venc6p,max_venc6m),current_date) < pmt6_venc then (case when fp.pago_em < least(coalesce(max_venc6p,max_venc6m),current_date) then fp.pago_em - pmt6_venc else null end) else (case when fp.pago_em is null then least(coalesce(max_venc6p,max_venc6m),current_date) - pmt6_venc else least(coalesce(max_venc6p,max_venc6m),fp.pago_em) - pmt6_venc end) end) end) end)) as max_late_pmt6	
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	--TABELA PARA BUSCAR A PARCELA E O VENCIMENTO DO PLANO QUE FOI SUBSTITUIDO
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada' and fp.numero <= 6
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	--TABELA PARA INFORMAR OS PLANOS DE PAGAMENTO QUE DEVEM SER IGNORADOS (NÃO TIVERAM NENHUMA PARCELA PAGA E NÃO SÃO O PRIMEIRO PLANO)
	left join(select fpp.id,count(*) filter (where fp.status <> 'Cancelada') as ignorar
		from financeiro_planopagamento fpp
		join (select emprestimo_id,min(id) as min_id from financeiro_planopagamento group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		where fpp.id <> t1.min_id
		group by 1) as ignorados on ignorados.id = t2.plano_id
	--TABELA PARA BUSCAR A DATA DA FOTOGRAFIA
	join(select fpp.emprestimo_id, 
			case when min_id is null then (lr.loan_date + interval '6 months')::date end as max_venc6M , 
			max(fp.vencimento_em) filter (where fp.numero <= 6 and fpp.id = t1.min_id) as max_venc6P,
			count(*) filter (where fpp.status = 'Pago') as quitado
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, min(fpp.id) filter (where fp.numero = 6 and fpp.status <> 'EmEspera') as min_id
			from financeiro_planopagamento fpp
			join financeiro_parcela fp on fp.plano_id = fpp.id
			group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1,2) as max_venc_6M on max_venc_6M.emprestimo_id = fpp.emprestimo_id 
	--TABELA PARA BUSCAR A DATA DE PAGAMENTO DAS PARCELAS NA ORDEM EM QUE ELAS OCORRERAM
	join (select row_number() over (partition by fpp.emprestimo_id order by fp.pago_em,fp.vencimento_em) as numero_ajustado,fp.* from financeiro_parcela fp join financeiro_planopagamento fpp on fpp.id = fp.plano_id where fp.status not in ('Cancelada','EmEspera')) fp on fp.plano_id = fpp.id
	--TABELA PARA BUSCAR A DATA DE VENCIMENTO DAS PARCELAS NA ORDEM EM QUE ELAS FORAM CONTRATADAS
	join(select fpp.emprestimo_id,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 1 and fpp.id = min_plano.min_id) as pmt1_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 2 and fp.status not in ('Cancelada','EmEspera')) as pmt2_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 3 and fp.status not in ('Cancelada','EmEspera')) as pmt3_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 4 and fp.status not in ('Cancelada','EmEspera')) as pmt4_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 5 and fp.status not in ('Cancelada','EmEspera')) as pmt5_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 6 and fp.status not in ('Cancelada','EmEspera')) as pmt6_venc
		from financeiro_planopagamento fpp
		join (select row_number() over (partition by fpp.emprestimo_id order by fp.vencimento_em) as numero_ajustado,fp.* from financeiro_parcela fp join financeiro_planopagamento fpp on fpp.id = fp.plano_id where fp.status not in ('Cancelada','EmEspera')) as fp on fp.plano_id = fpp.id
		join (select emprestimo_id,min(id) as min_id from financeiro_planopagamento group by 1) as min_plano on min_plano.emprestimo_id = fpp.emprestimo_id
		group by 1) vencimentos on vencimentos.emprestimo_id = lr.loan_request_id
	where fp.status not in ('Cancelada','EmEspera') and (coalesce(max_venc6p,max_venc6m) <= current_date or quitado > 0)
	group by 1) as PMT6_orig_soft on PMT6_orig_soft.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO MOB 3 SOFT
left join(select lr.loan_request_id,
		max(fp.numero_ajustado) filter (where fp.pago_em <= (to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date) as ultparcpaga,
		sum(fp.pago) filter (where fp.pago_em <= (to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date) as total_pago,
		max((case when fp.numero_ajustado <> 1 then null else (case when ParcSubst = 1 and coalesce(ignorar,1) > 0 then (case when fp.pago_em is null or fp.pago_em > (to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date then greatest(least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,current_date) - vencparcsubst,0) else fp.pago_em - VencParcSubst end) else (case when least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,current_date) < pmt1_venc then (case when fp.pago_em < least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,current_date) then fp.pago_em - pmt1_venc else null end) else (case when fp.pago_em is null then least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,current_date) - pmt1_venc else least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,fp.pago_em) - pmt1_venc end) end) end) end)) as max_late_pmt1,
		max((case when fp.numero_ajustado <> 2 then null else (case when ParcSubst = 2 and coalesce(ignorar,1) > 0 then (case when fp.pago_em is null or fp.pago_em > (to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date then greatest(least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,current_date) - vencparcsubst,0) else fp.pago_em - VencParcSubst end) else (case when least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,current_date) < pmt2_venc then (case when fp.pago_em < least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,current_date) then fp.pago_em - pmt2_venc else null end) else (case when fp.pago_em is null then least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,current_date) - pmt2_venc else least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,fp.pago_em) - pmt2_venc end) end) end) end)) as max_late_pmt2,	
		max((case when fp.numero_ajustado <> 3 then null else (case when ParcSubst = 3 and coalesce(ignorar,1) > 0 then (case when fp.pago_em is null or fp.pago_em > (to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date then greatest(least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,current_date) - vencparcsubst,0) else fp.pago_em - VencParcSubst end) else (case when least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,current_date) < pmt3_venc then (case when fp.pago_em < least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,current_date) then fp.pago_em - pmt3_venc else null end) else (case when fp.pago_em is null then least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,current_date) - pmt3_venc else least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,fp.pago_em) - pmt3_venc end) end) end) end)) as max_late_pmt3,	
		max((case when fp.numero_ajustado <> 4 then null else (case when ParcSubst = 4 and coalesce(ignorar,1) > 0 then (case when fp.pago_em is null or fp.pago_em > (to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date then greatest(least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,current_date) - vencparcsubst,0) else fp.pago_em - VencParcSubst end) else (case when least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,current_date) < pmt4_venc then (case when fp.pago_em < least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,current_date) then fp.pago_em - pmt4_venc else null end) else (case when fp.pago_em is null then least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,current_date) - pmt4_venc else least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,fp.pago_em) - pmt4_venc end) end) end) end)) as max_late_pmt4,	
		max((case when fp.numero_ajustado <> 5 then null else (case when ParcSubst = 5 and coalesce(ignorar,1) > 0 then (case when fp.pago_em is null or fp.pago_em > (to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date then greatest(least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,current_date) - vencparcsubst,0) else fp.pago_em - VencParcSubst end) else (case when least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,current_date) < pmt5_venc then (case when fp.pago_em < least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,current_date) then fp.pago_em - pmt5_venc else null end) else (case when fp.pago_em is null then least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,current_date) - pmt5_venc else least((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date,fp.pago_em) - pmt5_venc end) end) end) end)) as max_late_pmt5	
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	--TABELA PARA BUSCAR A PARCELA E O VENCIMENTO DO PLANO QUE FOI SUBSTITUIDO
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		join loan_requests lr on lr.loan_request_id = fpp.emprestimo_id
		where fp.status = 'Cancelada' and fp.vencimento_em <= (to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	--TABELA PARA INFORMAR OS PLANOS DE PAGAMENTO QUE DEVEM SER IGNORADOS (NÃO TIVERAM NENHUMA PARCELA PAGA E NÃO SÃO O PRIMEIRO PLANO)
	left join(select fpp.id,count(*) filter (where fp.status <> 'Cancelada') as ignorar
		from financeiro_planopagamento fpp
		join (select emprestimo_id,min(id) as min_id from financeiro_planopagamento group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		where fpp.id <> t1.min_id
		group by 1) as ignorados on ignorados.id = t2.plano_id
	--TABELA PARA BUSCAR A DATA DE PAGAMENTO DAS PARCELAS NA ORDEM EM QUE ELAS OCORRERAM
	join (select row_number() over (partition by fpp.emprestimo_id order by fp.pago_em,fp.vencimento_em) as numero_ajustado,fp.* from financeiro_parcela fp join financeiro_planopagamento fpp on fpp.id = fp.plano_id where fp.status not in ('Cancelada','EmEspera')) fp on fp.plano_id = fpp.id
	--TABELA PARA BUSCAR A DATA DE VENCIMENTO DAS PARCELAS NA ORDEM EM QUE ELAS FORAM CONTRATADAS
	join(select fpp.emprestimo_id,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 1 and fpp.id = min_plano.min_id) as pmt1_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 2 and fp.status not in ('Cancelada','EmEspera')) as pmt2_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 3 and fp.status not in ('Cancelada','EmEspera')) as pmt3_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 4 and fp.status not in ('Cancelada','EmEspera')) as pmt4_venc,
			min(fp.vencimento_em) filter (where fp.numero_ajustado = 5 and fp.status not in ('Cancelada','EmEspera')) as pmt5_venc,
			count(*) filter (where fpp.status = 'Pago') as quitado
		from financeiro_planopagamento fpp
		join (select row_number() over (partition by fpp.emprestimo_id order by fp.vencimento_em) as numero_ajustado,fp.* from financeiro_parcela fp join financeiro_planopagamento fpp on fpp.id = fp.plano_id where fp.status not in ('Cancelada','EmEspera')) as fp on fp.plano_id = fpp.id
		join (select emprestimo_id,min(id) as min_id from financeiro_planopagamento group by 1) as min_plano on min_plano.emprestimo_id = fpp.emprestimo_id
		group by 1) vencimentos on vencimentos.emprestimo_id = lr.loan_request_id
	where fp.status not in ('Cancelada','EmEspera') and ((to_date(to_char(lr.loan_date + interval '4 months','Mon/yy'),'Mon/yy') - interval '1 day')::date <= current_date or quitado > 0)
	group by 1) as mob3_orig_soft on mob3_orig_soft.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO PMT 6 PLANO ORIGINAL AGGRESSIVE
left join(select lr.loan_request_id,
		max(fp.numero_ajustado) filter (where fp.pago_em <= coalesce(pmt6_venc,max_venc6m)) as ultparcpaga,
		sum(fp.pago) filter (where fp.pago_em <= coalesce(pmt6_venc,max_venc6m)) as total_pago,
		max((case when fp.numero_ajustado <> 1 then null else (case when least(coalesce(pmt6_venc,max_venc6m),current_date) < pmt1_venc then (case when fp.pago_em < least(coalesce(pmt6_venc,max_venc6m),current_date) then fp.pago_em - pmt1_venc else null end) else (case when fp.pago_em is null then least(coalesce(pmt6_venc,max_venc6m),current_date) - pmt1_venc else least(coalesce(pmt6_venc,max_venc6m),fp.pago_em) - pmt1_venc end) end) end)) as max_late_pmt1,
		max((case when fp.numero_ajustado <> 2 then null else (case when least(coalesce(pmt6_venc,max_venc6m),current_date) < pmt2_venc then (case when fp.pago_em < least(coalesce(pmt6_venc,max_venc6m),current_date) then fp.pago_em - pmt2_venc else null end) else (case when fp.pago_em is null then least(coalesce(pmt6_venc,max_venc6m),current_date) - pmt2_venc else least(coalesce(pmt6_venc,max_venc6m),fp.pago_em) - pmt2_venc end) end) end)) as max_late_pmt2,
		max((case when fp.numero_ajustado <> 3 then null else (case when least(coalesce(pmt6_venc,max_venc6m),current_date) < pmt3_venc then (case when fp.pago_em < least(coalesce(pmt6_venc,max_venc6m),current_date) then fp.pago_em - pmt3_venc else null end) else (case when fp.pago_em is null then least(coalesce(pmt6_venc,max_venc6m),current_date) - pmt3_venc else least(coalesce(pmt6_venc,max_venc6m),fp.pago_em) - pmt3_venc end) end) end)) as max_late_pmt3,
		max((case when fp.numero_ajustado <> 4 then null else (case when least(coalesce(pmt6_venc,max_venc6m),current_date) < pmt4_venc then (case when fp.pago_em < least(coalesce(pmt6_venc,max_venc6m),current_date) then fp.pago_em - pmt4_venc else null end) else (case when fp.pago_em is null then least(coalesce(pmt6_venc,max_venc6m),current_date) - pmt4_venc else least(coalesce(pmt6_venc,max_venc6m),fp.pago_em) - pmt4_venc end) end) end)) as max_late_pmt4,
		max((case when fp.numero_ajustado <> 5 then null else (case when least(coalesce(pmt6_venc,max_venc6m),current_date) < pmt5_venc then (case when fp.pago_em < least(coalesce(pmt6_venc,max_venc6m),current_date) then fp.pago_em - pmt5_venc else null end) else (case when fp.pago_em is null then least(coalesce(pmt6_venc,max_venc6m),current_date) - pmt5_venc else least(coalesce(pmt6_venc,max_venc6m),fp.pago_em) - pmt5_venc end) end) end)) as max_late_pmt5,
		max((case when fp.numero_ajustado <> 6 then null else (case when least(coalesce(pmt6_venc,max_venc6m),current_date) < pmt6_venc then (case when fp.pago_em < least(coalesce(pmt6_venc,max_venc6m),current_date) then fp.pago_em - pmt6_venc else null end) else (case when fp.pago_em is null then least(coalesce(pmt6_venc,max_venc6m),current_date) - pmt6_venc else least(coalesce(pmt6_venc,max_venc6m),fp.pago_em) - pmt6_venc end) end) end)) as max_late_pmt6
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	--TABELA PARA BUSCAR A DATA DE PAGAMENTO DAS PARCELAS NA ORDEM EM QUE ELAS OCORRERAM
	join (select row_number() over (partition by fpp.emprestimo_id order by fp.pago_em,fp.vencimento_em) as numero_ajustado,fpp.emprestimo_id,fp.* from financeiro_parcela fp join financeiro_planopagamento fpp on fpp.id = fp.plano_id where fp.status not in ('Cancelada','EmEspera')) fp on fp.plano_id = fpp.id
	--TABELA PARA BUSCAR A DATA DE VENCIMENTO DAS PARCELAS -> SEMPRE 1o PLANO DE PAGAMENTO
	join(select fpp.emprestimo_id, 
			(to_date(to_char(lr.loan_date + interval '7 months','Mon/yy'),'Mon/yy') - interval '1 day')::date as max_venc6M, 
			count(*) filter (where fpp.status = 'Pago') as quitado,
			min(fp.vencimento_em) filter (where fp.numero = 1 and fpp.id = t1.min_id) as pmt1_venc,
			min(fp.vencimento_em) filter (where fp.numero = 2 and fpp.id = t1.min_id) as pmt2_venc,
			min(fp.vencimento_em) filter (where fp.numero = 3 and fpp.id = t1.min_id) as pmt3_venc,
			min(fp.vencimento_em) filter (where fp.numero = 4 and fpp.id = t1.min_id) as pmt4_venc,
			min(fp.vencimento_em) filter (where fp.numero = 5 and fpp.id = t1.min_id) as pmt5_venc,
			min(fp.vencimento_em) filter (where fp.numero = 6 and fpp.id = t1.min_id) as pmt6_venc
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, min(id) as min_id from financeiro_planopagamento group by 1) as t1 on t1.emprestimo_id = lr.loan_request_id
		group by 1,2) as max_venc_6M on max_venc_6M.emprestimo_id = fpp.emprestimo_id 
	where fp.status not in ('Cancelada','EmEspera') and (coalesce(pmt6_venc,max_venc6m) <= current_date or quitado > 0)
	group by 1) as PMT6_orig_aggressive on PMT6_orig_aggressive.loan_request_id = lr.loan_request_id
--------------------------TABLE FINANCEIRO PMT 50% PLANO ORIGINAL AGGRESSIVE
left join(select lr.loan_request_id,
	max(fp.numero_ajustado) filter (where fp.pago_em <= max_venc50pctP) as ultparcpaga,
	sum(fp.pago) filter (where fp.pago_em <= max_venc50pctP) as total_pago,
	max((case when fp.numero_ajustado <> 1 then null else (case when least(max_venc50pctP,current_date) < pmt1_venc then (case when fp.pago_em < least(max_venc50pctP,current_date) then fp.pago_em - pmt1_venc else null end) else (case when fp.pago_em is null then least(max_venc50pctP,current_date) - pmt1_venc else least(max_venc50pctP,fp.pago_em) - pmt1_venc end) end) end)) as max_late_pmt1,	
	max((case when fp.numero_ajustado <> 2 then null else (case when least(max_venc50pctP,current_date) < pmt2_venc then (case when fp.pago_em < least(max_venc50pctP,current_date) then fp.pago_em - pmt2_venc else null end) else (case when fp.pago_em is null then least(max_venc50pctP,current_date) - pmt2_venc else least(max_venc50pctP,fp.pago_em) - pmt2_venc end) end) end)) as max_late_pmt2,	
	max((case when fp.numero_ajustado <> 3 then null else (case when least(max_venc50pctP,current_date) < pmt3_venc then (case when fp.pago_em < least(max_venc50pctP,current_date) then fp.pago_em - pmt3_venc else null end) else (case when fp.pago_em is null then least(max_venc50pctP,current_date) - pmt3_venc else least(max_venc50pctP,fp.pago_em) - pmt3_venc end) end) end)) as max_late_pmt3,	
	max((case when fp.numero_ajustado <> 4 then null else (case when least(max_venc50pctP,current_date) < pmt4_venc then (case when fp.pago_em < least(max_venc50pctP,current_date) then fp.pago_em - pmt4_venc else null end) else (case when fp.pago_em is null then least(max_venc50pctP,current_date) - pmt4_venc else least(max_venc50pctP,fp.pago_em) - pmt4_venc end) end) end)) as max_late_pmt4,	
	max((case when fp.numero_ajustado <> 5 then null else (case when least(max_venc50pctP,current_date) < pmt5_venc then (case when fp.pago_em < least(max_venc50pctP,current_date) then fp.pago_em - pmt5_venc else null end) else (case when fp.pago_em is null then least(max_venc50pctP,current_date) - pmt5_venc else least(max_venc50pctP,fp.pago_em) - pmt5_venc end) end) end)) as max_late_pmt5,	
	max((case when fp.numero_ajustado <> 6 then null else (case when least(max_venc50pctP,current_date) < pmt6_venc then (case when fp.pago_em < least(max_venc50pctP,current_date) then fp.pago_em - pmt6_venc else null end) else (case when fp.pago_em is null then least(max_venc50pctP,current_date) - pmt6_venc else least(max_venc50pctP,fp.pago_em) - pmt6_venc end) end) end)) as max_late_pmt6,	
	max((case when fp.numero_ajustado <> 7 then null else (case when least(max_venc50pctP,current_date) < pmt7_venc then (case when fp.pago_em < least(max_venc50pctP,current_date) then fp.pago_em - pmt7_venc else null end) else (case when fp.pago_em is null then least(max_venc50pctP,current_date) - pmt7_venc else least(max_venc50pctP,fp.pago_em) - pmt7_venc end) end) end)) as max_late_pmt7,	
	max((case when fp.numero_ajustado <> 8 then null else (case when least(max_venc50pctP,current_date) < pmt8_venc then (case when fp.pago_em < least(max_venc50pctP,current_date) then fp.pago_em - pmt8_venc else null end) else (case when fp.pago_em is null then least(max_venc50pctP,current_date) - pmt8_venc else least(max_venc50pctP,fp.pago_em) - pmt8_venc end) end) end)) as max_late_pmt8,	
	max((case when fp.numero_ajustado <> 9 then null else (case when least(max_venc50pctP,current_date) < pmt9_venc then (case when fp.pago_em < least(max_venc50pctP,current_date) then fp.pago_em - pmt9_venc else null end) else (case when fp.pago_em is null then least(max_venc50pctP,current_date) - pmt9_venc else least(max_venc50pctP,fp.pago_em) - pmt9_venc end) end) end)) as max_late_pmt9,	
	max((case when fp.numero_ajustado <> 10 then null else (case when least(max_venc50pctP,current_date) < pmt10_venc then (case when fp.pago_em < least(max_venc50pctP,current_date) then fp.pago_em - pmt10_venc else null end) else (case when fp.pago_em is null then least(max_venc50pctP,current_date) - pmt10_venc else least(max_venc50pctP,fp.pago_em) - pmt10_venc end) end) end)) as max_late_pmt10,	
	max((case when fp.numero_ajustado <> 11 then null else (case when least(max_venc50pctP,current_date) < pmt11_venc then (case when fp.pago_em < least(max_venc50pctP,current_date) then fp.pago_em - pmt11_venc else null end) else (case when fp.pago_em is null then least(max_venc50pctP,current_date) - pmt11_venc else least(max_venc50pctP,fp.pago_em) - pmt11_venc end) end) end)) as max_late_pmt11,	
	max((case when fp.numero_ajustado <> 12 then null else (case when least(max_venc50pctP,current_date) < pmt12_venc then (case when fp.pago_em < least(max_venc50pctP,current_date) then fp.pago_em - pmt12_venc else null end) else (case when fp.pago_em is null then least(max_venc50pctP,current_date) - pmt12_venc else least(max_venc50pctP,fp.pago_em) - pmt12_venc end) end) end)) as max_late_pmt12	
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	--TABELA PARA BUSCAR A DATA DE PAGAMENTO DAS PARCELAS NA ORDEM EM QUE ELAS OCORRERAM
	join (select row_number() over (partition by fpp.emprestimo_id order by fp.pago_em,fp.vencimento_em) as numero_ajustado,fpp.emprestimo_id,fp.* from financeiro_parcela fp join financeiro_planopagamento fpp on fpp.id = fp.plano_id where fp.status not in ('Cancelada','EmEspera')) fp on fp.plano_id = fpp.id
	--TABELA PARA BUSCAR A DATA DE VENCIMENTO DAS PARCELAS -> SEMPRE 1o PLANO DE PAGAMENTO
	join(select fpp.emprestimo_id,
			count(*) filter (where fpp.status = 'Pago') as quitado,
			max(fp.vencimento_em) filter (where fp.numero = floor(fpp.parcelas_qtd/2) and fpp.id = t1.min_id) as max_venc50pctP,
			min(fp.vencimento_em) filter (where fp.numero = 1 and fpp.id = t1.min_id) as pmt1_venc,
			min(fp.vencimento_em) filter (where fp.numero = 2 and fpp.id = t1.min_id) as pmt2_venc,
			min(fp.vencimento_em) filter (where fp.numero = 3 and fpp.id = t1.min_id) as pmt3_venc,
			min(fp.vencimento_em) filter (where fp.numero = 4 and fpp.id = t1.min_id) as pmt4_venc,
			min(fp.vencimento_em) filter (where fp.numero = 5 and fpp.id = t1.min_id) as pmt5_venc,
			min(fp.vencimento_em) filter (where fp.numero = 6 and fpp.id = t1.min_id) as pmt6_venc,
			min(fp.vencimento_em) filter (where fp.numero = 7 and fpp.id = t1.min_id) as pmt7_venc,
			min(fp.vencimento_em) filter (where fp.numero = 8 and fpp.id = t1.min_id) as pmt8_venc,
			min(fp.vencimento_em) filter (where fp.numero = 9 and fpp.id = t1.min_id) as pmt9_venc,
			min(fp.vencimento_em) filter (where fp.numero = 10 and fpp.id = t1.min_id) as pmt10_venc,
			min(fp.vencimento_em) filter (where fp.numero = 11 and fpp.id = t1.min_id) as pmt11_venc,
			min(fp.vencimento_em) filter (where fp.numero = 12 and fpp.id = t1.min_id) as pmt12_venc
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		join financeiro_parcela fp on fp.plano_id = fpp.id
		join(select emprestimo_id, min(id) as min_id from financeiro_planopagamento group by 1) as t1 on t1.emprestimo_id = fpp.emprestimo_id
		group by 1) as t2 on t2.emprestimo_id = fpp.emprestimo_id 
	where fp.status not in ('Cancelada','EmEspera') and (max_venc50pctP <= current_date or quitado > 0)
	group by 1) as pmt50pct_orig_aggressive on pmt50pct_orig_aggressive.loan_request_id = lr.loan_request_id
--------------------------TABELA PROMESSA DE PAGAMENTO
left join(select cr.emprestimo_id,
		count(*) as total_promessas,
		count(*) filter (where cpp.status = 'Cumprida') as total_promessas_cumpridas,
		count(*) filter (where cpp.status = 'Quebrada') as total_promessas_quebradas,
		count(*) filter (where cpp.status = 'Cancelada') as total_promessas_canceladas,
		count(*) filter (where cpp.status = 'Ativa') as total_promessas_ativas
	from public.cobranca_regua cr
	join public.cobranca_promessapagamento cpp on cpp.regua_id = cr.id
	group by 1) as promessas on promessas.emprestimo_id = lr.loan_request_id
--------------------------TABELA HISTORICO DE NEGATIVACAO
left join(select cr.emprestimo_id,
		count(*) as total_negativacoes,
		count(*) filter (where sh.has_error = true) as total_negativacoes_com_erro,
		count(*) filter (where sh.has_error = false) as total_negativacoes_sem_erro,
		count(*) filter (where sh.company_spoiled = true) as total_negativacoes_empresa,
		count(*) filter (where sh.guarantor_spoiled = true) as total_negativacoes_avalistas,
		count(*) filter (where sh.company_spoiled = true and sh.has_error = false) as total_negativacoes_empresa_sem_erro,
		count(*) filter (where sh.guarantor_spoiled = true and sh.has_error = false) as total_negativacoes_avalistas_sem_erro,
		sum(sh.number_of_installments_spoiled) as total_negativacao_parcelas,
		sum(sh.number_of_installments_spoiled) filter (where sh.has_error = false) as total_negativacao_parcelas_sem_erro	
	from public.cobranca_regua cr
	join public.spoil_history sh on sh.collection_bar_id = cr.id
	group by 1) as historico_negativacao on historico_negativacao.emprestimo_id = lr.loan_request_id
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id,(data->>'score')::float as serasa_coleta,("data"->>'pd')::float as pd_serasa
	from credito_coleta cc
	join(select dp.direct_prospect_id,dp.cnpj,max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 second' * id) as max_date_serasa
		from credito_coleta cc
		join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
		join offers o on o.direct_prospect_id = dp.direct_prospect_id
		join loan_requests lr on lr.offer_id = o.offer_id 
		where lr.status in ('ACCEPTED','REJECTED') and to_date(cc.data->'datetime'->>'data','yyyymmdd') <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date) and tipo in ('Serasa','RelatoSocios','RelatoBasico')
		group by 1,2) as t1 on left(t1.cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date_serasa and tipo in ('Serasa','RelatoSocios','RelatoBasico')) as SerasaScore on SerasaScore.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE CREDITA
left join (select cc.origem_id,
		cc."data"->'customer'->>'email' as credita_email,
		cc."data"->'customer'->'mobile'->>'areaCode' as credita_area_code,
		cc."data"->'customer'->'mobile'->>'regionCode' as credita_region_code,
		cc."data"->'customer'->'mobile'->>'phoneNumber' as credita_phone_number,
		cc."data"->'customer'->'document'->>'documentID' as credita_cpf,
		cc."data"->'customer'->'document'->>'documentCountry' as credita_country,
		(cc."data"->'customer'->>'companyScore')::float as credita_score_mei,
		cc."data"->>'leadType' as credita_lead_type,
		cc."data"->'loanOffer'->>'creditLineName' as credita_credit_line_name
	from credito_coleta cc
	join offers o on o.direct_prospect_id = cc.origem_id
	join loan_requests lr on lr.offer_id = o.offer_id 
	where lr.status in ('ACCEPTED','REJECTED') and cc.tipo = 'Credita') as Credita on Credita.origem_id = dp.direct_prospect_id
--------------------------TABLE SERASA SOCIOS
left join (select direct_prospect_id, case when count(cpf_socios) = 0 then null else count(*) filter (where cpf = cpf_socios) end is_shareholder,case when count(cpf_socios) = 0 then null else count(*) end count_socios
	from(select t1.direct_prospect_id, t1.cpf, case when data->>'acionistas' = '{}' then null else data->'acionistas'->jsonb_object_keys(data->'acionistas')->>'cpf' end as cpf_socios
		from credito_coleta cc
		join(select dp.direct_prospect_id,dp.cnpj,dp.cpf, 
				max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) as max_date
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			join offers o on o.direct_prospect_id = dp.direct_prospect_id
			join loan_requests lr on lr.offer_id = o.offer_id 
			where lr.status in ('ACCEPTED','REJECTED') and to_date(cc.data->'datetime'->>'data','yyyymmdd') <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date) and tipo in ('Serasa','RelatoSocios','RelatoBasico')
			group by 1,2,3) as t1 on tipo in ('Serasa','RelatoSocios','RelatoBasico') and left(t1.cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id = max_date) as t2
		group by 1) as SerasaCPF on SerasaCPF.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE SERASA CONSULTA FACTORING SERASA
left join (select direct_prospect_id,sum(case when position('INVEST DIR' in consultas) > 0 or 
		position('CREDITORIOS' in consultas) > 0 or 
		position('FUNDO DE INVE' in consultas) > 0 or 
		position('SECURITIZADORA' in consultas) > 0 or 
		position('FACTORING'in consultas) > 0 or 
		position('FOMENTO'in consultas) > 0 or 
		position('FIDC' in consultas) > 0 or 
		position('NOVA S R M' in consultas) > 0 or 
		position('RED AS' in consultas) > 0 or 
		position('DEL MONTE SERVICOS' in consultas) > 0 or 
		position('SERVICOS FINANCEIROS' in consultas) > 0 then 1 else 0 end) as consulta_factoring
	from(--Serasa
		select t1.direct_prospect_id,cc.data->'consulta_empresas'->jsonb_object_keys(data->'consulta_empresas')->>'nome' as consultas
			from credito_coleta cc
			join(select dp.direct_prospect_id,dp.cnpj, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) as max_date
				from credito_coleta cc
				join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
				join offers o on o.direct_prospect_id = dp.direct_prospect_id
				join loan_requests lr on lr.offer_id = o.offer_id 
				where lr.status in ('ACCEPTED','REJECTED') and to_date(cc.data->'datetime'->>'data','yyyymmdd') <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date) and tipo in ('Serasa','RelatoSocios','RelatoBasico')
				group by 1,2) as t1 on tipo in ('Serasa','RelatoSocios','RelatoBasico') and left(t1.cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 second' * id = max_date
	union--SPC
		select t1.direct_prospect_id,cc.data->'consulta_spc'->jsonb_object_keys(data->'consulta_spc')->>'nome' as consultas
			from credito_coleta cc
			join(select dp.direct_prospect_id,dp.cnpj, max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')+ interval '1 second' * id) as max_date
				from credito_coleta cc
				join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
				join offers o on o.direct_prospect_id = dp.direct_prospect_id
				join loan_requests lr on lr.offer_id = o.offer_id 
				where lr.status in ('ACCEPTED','REJECTED') and to_date(cc.data->'datetime'->>'data','yyyymmdd') <= coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date) and tipo in ('Serasa','RelatoSocios','RelatoBasico')
				group by 1,2) as t1 on tipo in ('Serasa','RelatoSocios','RelatoBasico') and left(t1.cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 second' * id = max_date) as t1
	group by 1) as Consulta on Consulta.direct_prospect_id = dp.direct_prospect_id
-------------------------TABLE CNPJs RELACIONADOS
left join(select direct_prospect_id,count(distinct cnpjs_relacionados) filter (where cnpjs_relacionados <> 'NA') as n_cnpjs_relacionados
	from(select direct_prospect_id, unnest(string_to_array(replace(replace(replace(cnpjs_relacionados::text,'{}','NA'),'{',''),'}',''),',')) as cnpjs_relacionados
		from direct_prospects
		where workflow in ('PedidoAprovado','PedidoRejeitado')) as t1
	where cnpjs_relacionados is not null
	group by 1) as cnpjs_relacionados on cnpjs_relacionados.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE CNPJs RELACIONADOS
left join(select direct_prospect_id,count(distinct cpfs_relacionados) filter (where cpfs_relacionados <> 'NA') as n_cpfs_relacionados
	from(select direct_prospect_id, unnest(string_to_array(replace(replace(replace(cpfs_relacionados::text,'{}','NA'),'{',''),'}',''),',')) as cpfs_relacionados
		from direct_prospects
		where workflow in ('PedidoAprovado','PedidoRejeitado')) as t1
	where cpfs_relacionados is not null
	group by 1) as cpfs_relacionados on cpfs_relacionados.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE CREDITO PARECER
left join (select emprestimo_id,
		min(cp.criado_em)::date as "Alçada 1 - data",
		max(case criado_por 
			when 25 then 'Robson' 
			when 38 then 'Rodrigo Pinho' 
			when 39 then 'Luigi' 
			when 55 then 'Gabriel' 
			when 52 then 'Andre' 
			when 81 then 'Isabel' 
			when 62 then 'Felipe' 
			when 65 then 'Rodrigo Avila' 
			when 67 then 'BizBot' 
			when 36 then 'Giulia' 
			when 100 then 'Italo' 
			when 113 then 'Nathalya' 
			else criado_por::text end) as "Alçada 1 - analista",
		max(cp.tipo) as "Alçada 1 - tipo",
		max(cp.status) as "Alçada 1 - status",
		max(confianca) as "Alçada 1 - confiança",
		max(limite) as "Alçada 1 - limite",
		max(cp.juros)::decimal(10,2) as "Alçada 1 - taxa",
		max(cp.prazo)::decimal(10,2) as "Alçada 1 - prazo"
	from public.credito_parecer cp
	join public.loan_requests lr on lr.loan_request_id = cp.emprestimo_id
	where lr.status in ('ACCEPTED','REJECTED') and cp.tipo = 'Analise'
	group by 1) as cp on cp.emprestimo_id = lr.loan_request_id
--------------------------TABLE SÓCIO MAJORITÁRIO MAIS VELHO
left join (select lrs.loan_request_id,
		extract(year from age(loan_date,s.date_of_birth))::int as MajorSigner_Age, 
		s.revenue as MajorSigner_Revenue,
		s.civil_status as MajorSigner_civil_status,
		s.gender as MajorSigner_gender,
		avg_age_partners,
		avg_revenue_partners,
		male_partners,
		female_partners,
		count_socios_admin,
		count_avalistas,
		count_total
	from signers s
	join loan_requests_signers lrs on lrs.signer_id = s.signer_id
	join(select lrs.loan_request_id, 
			max(signer_percentage) as signer_percentage,
			max(avg_age_partners) as avg_age_partners,
			max(avg_revenue_partners) as avg_revenue_partners,
			max(male_partners) as male_partners,
			max(female_partners) as female_partners,
			max(loan_date) as loan_date,
			min(date_of_birth) as data_nasc,
			max(count_socios) as count_socios_admin,
			max(count_avalistas) as count_avalistas,
			max(count_total) as count_total
		from signers s
		join loan_requests_signers lrs on lrs.signer_id = s.signer_id
		join(select lr.loan_request_id, 
				coalesce(lr.loan_date,lr.date_inserted) as loan_date, 
				max(s.share_percentage) as signer_percentage,
				avg(extract(year from age(coalesce(lr.loan_date,lr.date_inserted),s.date_of_birth))) filter (where s.share_percentage > 0) as avg_age_partners,
				avg(s.revenue) filter (where s.share_percentage > 0) as avg_revenue_partners,
				count(*) filter (where s.gender = 'MALE' and s.share_percentage > 0) as male_partners, 
				count(*) filter (where s.gender = 'FEMALE' and s.share_percentage > 0) as female_partners,
				count(*) filter (where s.share_percentage > 0) as count_socios,
				count(*) filter (where s.guarantor is true) count_avalistas,
				count(*) as count_total
			from signers s
			join loan_requests_signers as lrs on lrs.signer_id = s.signer_id
			join loan_requests lr on lr.loan_request_id = lrs.loan_request_id
			where lr.status in ('ACCEPTED','REJECTED')
			group by 1,2) as t1 on t1.loan_request_id = lrs.loan_request_id and s.share_percentage = t1. signer_percentage
		group by 1) as t2 on t2.loan_request_id = lrs.loan_request_id and s.date_of_birth = t2.data_nasc and s.share_percentage = t2.signer_percentage) as MajSigners on MajSigners.loan_request_id = lr.loan_request_id
--------------------------TABLE SOLICITANTE POR CPF
left join(select lrs.loan_request_id, extract(year from age(coalesce(lr.loan_date,lr.date_inserted,o.date_inserted,dp.opt_in_date),s.date_of_birth))::int as SolicSignersCPF_Age, s.revenue as SolicSignersCPF_Revenue,s.civil_status as SolicSignerCPF_civil_status,s.gender as SolicSignerCPF_gender
	from direct_prospects dp
	join offers o on o.direct_prospect_id = dp.direct_prospect_id
	join loan_requests lr on lr.offer_id = o.offer_id
	join loan_requests_signers lrs on lrs.loan_request_id = lr.loan_request_id
	join signers s on s.signer_id = lrs.signer_id
	where lr.status in ('ACCEPTED','REJECTED') and case when dp.cpf is null then null else case when dp.cpf = s.cpf then 1 else 0 end end = 1) as SolicCPFSigners on SolicCPFSigners.loan_request_id = lr.loan_request_id 
--------------------------TABLE SÓCIO MAIS VELHO
left join(select lrs.loan_request_id,max(extract(year from age(loan_date,s.date_of_birth)))::int as OldestSh_Age, max(s.revenue) as OldestSh_Revenue,max(s.civil_status) as OldestSh_civil_status,max(s.gender) as OldestSh_gender
	from signers s
	join loan_requests_signers lrs on lrs.signer_id = s.signer_id
	join(select lr.loan_request_id, coalesce(lr.loan_date,lr.date_inserted) as loan_date, min(s.date_of_birth) as min_date
		from signers s
		join loan_requests_signers as lrs on lrs.signer_id = s.signer_id
		join loan_requests lr on lr.loan_request_id = lrs.loan_request_id
		where lr.status in ('ACCEPTED','REJECTED') and s.share_percentage > 0
		group by 1,2) as t1 on t1.loan_request_id = lrs.loan_request_id and s.date_of_birth = t1.min_date 
	group by 1) as Signers on Signers.loan_request_id = lr.loan_request_id
--------------------------TABLE ADMNISTRADOR MAIS VELHO
left join(select lrs.loan_request_id, max(extract(year from age(loan_date,s.date_of_birth)))::int as AdminSigner_Age, max(s.revenue) as AdminSigner_Revenue,max(s.civil_status) as AdminSigner_civil_status,max(s.gender) as AdminSigner_gender 
	from signers s
	join loan_requests_signers lrs on lrs.signer_id = s.signer_id
	join(select lrs.loan_request_id,coalesce(lr.loan_date,lr.date_inserted) as loan_date, min(s.date_of_birth) data_nasc
		from signers s
		join loan_requests_signers lrs on lrs.signer_id = s.signer_id
		join loan_requests lr on lr.loan_request_id = lrs.loan_request_id
		where s.administrator = true and lr.status in ('ACCEPTED','REJECTED')
		group by 1,2) as t2 on t2.loan_request_id = lrs.loan_request_id and t2.data_nasc = s.date_of_birth
	group by 1) as AdminSigners on AdminSigners.loan_request_id = lr.loan_request_id
--------------------------TABLE SIGNERS ADDRESS
left join(select dp.direct_prospect_id,max(replace(sa.zip_code,'-','')) as signer_zipcode
	from direct_prospects dp
	join offers o on o.direct_prospect_id = dp.direct_prospect_id
	join loan_requests lr on lr.offer_id = o.offer_id
	join loan_requests_signers lrs on lrs.loan_request_id = lr.loan_request_id
	join signers s on s.signer_id = lrs.signer_id
	join signer_addresses sa on sa.address_id = s.address_id
	where case when dp.zip_code = '' then null else dp.zip_code end is null
	group by 1) as signer_address on signer_address.direct_prospect_id = dp.direct_prospect_id
--------------------------TABLE CHECKLIST
left join(select t2.emprestimo_id,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'valor_pj') as largest_cashflow_company_account,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'faturamento') as proved_informed_revenue,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'empreza_azul') as positive_balance_account,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'fluxo_entradas') as continuous_income,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'saldo_negativo') as low_negative_balance,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'entradas_desvinculadas') as income_unrelated_government,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'diversificacao_clientes') as diversified_clients,
	sum(pontuacao) filter (where tipo = 'Extrato' and campo = 'somente_despesas_empresa') as only_company_expenses,
	sum(pontuacao) filter (where tipo = 'Outros' and campo = 'endereco') as different_personal_company_address,
	sum(pontuacao) filter (where tipo = 'Outros' and campo = 'socio_bens') as partner_has_assets,
	sum(pontuacao) filter (where tipo = 'Outros' and campo = 'empresa_credito') as company_usedto_credit,
	sum(pontuacao) filter (where tipo = 'Outros' and campo = 'solicitou_menos') as requested_lessthan_offered,
	sum(pontuacao) filter (where tipo = 'Outros' and campo = 'endividamento_baixo') as low_personal_debt,
	sum(pontuacao) filter (where tipo = 'Outros' and campo = 'simulacao_diligente') as dilligent_credit_simulation,
	sum(pontuacao) filter (where tipo = 'Empresa' and campo = 'conta_pj') as company_account_confirmed,
	sum(pontuacao) filter (where tipo = 'Empresa' and campo = 'contrato_social') as social_contract_confirmed,
	sum(pontuacao) filter (where tipo = 'Empresa' and campo = 'extratos_bancarios') as bank_statements_confirmed,
	sum(pontuacao) filter (where tipo = 'Empresa' and campo = 'comprovante_endereço') as company_address_confirmed,
	sum(pontuacao) filter (where tipo = 'Física' and campo = 'cpf')/min(num_socios) as personal_cpf,
	sum(pontuacao) filter (where tipo = 'Física' and campo = 'identidade')/min(num_socios) as personal_rg,
	sum(pontuacao) filter (where tipo = 'Física' and campo = 'conjuge_info')/min(num_socios) as spouse_info,
	sum(pontuacao) filter (where tipo = 'Física' and campo = 'comprovante_renda')/min(num_socios) as proof_income,
	sum(pontuacao) filter (where tipo = 'Física' and campo = 'comprovante_endereço')/min(num_socios) as proof_personal_address,	
	sum(pontuacao) filter (where tipo in ('Extrato', 'Outros')) as score_documentation,
	sum(pontuacao) filter (where tipo in ('Física', 'Empresa') and campo not in ('anuencia','email'))/(4+5*min(num_socios)) percent_documentation
	from (select emprestimo_id, tipo,campo, case when pontuacao = '' then null else pontuacao end::double precision from (
		select emprestimo_id,tipo,jsonb_object_keys(formulario) as campo,formulario->jsonb_object_keys(formulario)->>'pontos' as pontuacao
		from credito_checklist cc
		join loan_requests lr on lr.loan_request_id = cc.emprestimo_id
		where lr.status in ('ACCEPTED','REJECTED')) as t1) as t2
	left join (select emprestimo_id, count(*) num_socios from credito_checklist cc join loan_requests lr on lr.loan_request_id = cc.emprestimo_id where lr.status in ('ACCEPTED','REJECTED') and cc.tipo = 'Física' group by 1) as t1 on t1.emprestimo_id = t2.emprestimo_id
	group by 1) as checklist on checklist.emprestimo_id = lr.loan_request_id
--------------------------TABLE MOTIVOS REJEICAO
left join(select lr.loan_request_id,
		max(lr.date_inserted),
		count(*) filter (where mr.nome = 'Complexidade Estrutural') as "Structure complexity",
		count(*) filter (where mr.nome = 'Mau Pagamento ao Mercado (Anotações Restritivas na PF e/ou PJ e/ou SCR)') as "Bad payment record (Experian/SCR)",
		count(*) filter (where mr.nome = 'Baixo Faturamento Comprovado') "Low income (bank stmts)",
		count(*) filter (where mr.nome = 'Mau Pagamento ao Mercado (Extrato)') "Bad payment record (bank stmts)",
		count(*) filter (where mr.nome in ('Política de Crédito (Score Serasa Baixo)','Política de Crédito (Score Serasa Baixo na análise de Documentação )')) as "Policy: low Experian score",
		count(*) filter (where mr.nome = 'Queda do Faturamento') as "Income descrease",
		count(*) filter (where mr.nome = 'Suspeita de Fraude') as "Fraud suspicion",
		count(*) filter (where mr.nome = 'Política de Crédito (Alteração no Contrato Social)') as "Policy: social contract change",
		count(*) filter (where mr.nome = 'Falta de Capacidade de Pagamento') as "Low margin",
		count(*) filter (where mr.nome = 'Política de Crédito (Atividade Fim da Empresa)') as "Policy: company activity",
		count(*) filter (where mr.nome = 'Política de Crédito (Objetivo do Empréstimo)') as "Policy: money use",
		count(*) filter (where mr.nome = 'Negou Informações/Anuência') as "Info/consent denial",
		count(*) filter (where mr.nome = 'Endividamento Elevado (Extrato/Crédito Recente)') as "High indebtness due to recent borrow (bank stmts)",
		count(*) filter (where mr.nome = 'LT Baixo') as "Low LT score",
		count(*) filter (where mr.nome = 'Política de Crédito (Quadro Societário)') as "Policy: shareholder change",
		count(*) filter (where mr.nome = 'Endividamento Elevado (Comprovado Baixo Faturamento)') as "High indebtness due to low income (bank stmts)",
		count(*) filter (where mr.nome = 'Problemas Com a Justiça') as "Justice issues",
		count(*) filter (where mr.nome = 'Empresa não possui conta PJ') as "No bank account for company",
		count(*) filter (where mr.nome = 'Documentação Irregular') as "Documentation issues",
		count(*) filter (where mr.nome = 'Política de Crédito (Empresa com menos de 1 ano de operação)') as "Policy: less than 1 yo"
	--------------------------
	from loan_requests lr
	join loan_requests_motivos_rejeicao lrm on lrm.loanrequest_id = lr.loan_request_id
	join motivos_rejeicao mr on mr.id = lrm.motivosrejeicao_id
	where lr.status in ('ACCEPTED','REJECTED')
	group by 1) as motivos on motivos.loan_request_id = lr.loan_request_id
--------------------------TABLE TAGS
left join (select dp.direct_prospect_id, 
		max(case when t.nome = 'AA<=10-v1' then 1 else 0 end) as app_auto,
		max(case when t.nome = 'Automático' then 1 else 0 end) as offer_auto,
		max(case when t.nome = 'InterestRate:Test1' then 1 else 0 end) as interest_rate_test1
	from direct_prospects dp
	join direct_prospects_tag dpt on dpt.directprospect_id = dp.direct_prospect_id
	join tags t on t.id = dpt.tag_id
	where dp.workflow in ('PedidoAprovado','PedidoRejeitado')
	group by 1) as tags on tags.direct_prospect_id = dp.direct_prospect_id
--------------------------
where lr.status in ('ACCEPTED','REJECTED')



order by 1
limit 650 
offset 0;

